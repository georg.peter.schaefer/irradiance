use proc_macro::TokenStream;
use std::fs;
use std::path::{Path, PathBuf};

use proc_macro2::{Delimiter, Group, Ident, Literal, Punct, Spacing, Span};
use quote::{ToTokens, TokenStreamExt};
use shaderc::{CompilationArtifact, IncludeType, ResolvedInclude, ShaderKind, SourceLanguage};
use syn::parse::{Parse, ParseStream};
use syn::punctuated::Punctuated;
use syn::{parse_macro_input, LitStr, Token};

use crate::reflection::VertexAttributes;

mod reflection;

/// Compiles the specified shader source and provides a type which creates a shader module from the
/// compiled source.
#[proc_macro]
pub fn shader(input: TokenStream) -> TokenStream {
    let shader_definition = parse_macro_input!(input as ShaderDefinition);
    let name = shader_definition.path.to_str().unwrap();
    let root = std::env::var("CARGO_MANIFEST_DIR").unwrap();
    let root = Path::new(&root).to_path_buf();
    let full_path = root.join(&shader_definition.path);
    let parent = full_path.parent().unwrap().to_path_buf();
    let source = fs::read_to_string(full_path)
        .unwrap_or_else(|_| panic!("Failed to read source from {:?}", shader_definition.path));
    let compiler = shaderc::Compiler::new().unwrap();
    let mut compile_options = shaderc::CompileOptions::new().unwrap();
    compile_options.set_include_callback(|include, ty, _src, _| {
        let full_paths = match ty {
            IncludeType::Relative => vec![parent.join(include)],
            IncludeType::Standard => shader_definition
                .include_dirs
                .iter()
                .map(|include_dir| root.join(include_dir).join(include))
                .collect(),
        };
        for possible_path in &full_paths {
            if possible_path.exists() {
                return Ok(ResolvedInclude {
                    resolved_name: possible_path
                        .canonicalize()
                        .unwrap()
                        .to_str()
                        .unwrap()
                        .to_string(),
                    content: fs::read_to_string(possible_path).unwrap(),
                });
            }
        }

        Err("Unable to find file".to_string())
    });
    compile_options.set_source_language(SourceLanguage::HLSL);
    compile_options.set_generate_debug_info();
    let compilation_artifact = match compiler.compile_into_spirv(
        &source,
        shader_definition.ty,
        shader_definition.path.to_str().unwrap(),
        &shader_definition.entry_point,
        Some(&compile_options),
    ) {
        Ok(compilation_artifact) => compilation_artifact,
        Err(e) => panic!("{}", e),
    };
    let vertex_attributes = VertexAttributes::new(compilation_artifact.as_binary());
    let compiled_shader = CompiledShader::from(compilation_artifact);
    quote::quote! {
        #compiled_shader

        #[allow(missing_docs)]
        pub struct Shader;

        impl Shader {
            #[allow(missing_docs)]
            pub fn load(device: std::sync::Arc<irradiance_runtime::gfx::device::Device>) -> Result<std::sync::Arc<irradiance_runtime::gfx::pipeline::ShaderModule>, irradiance_runtime::gfx::GfxError> {
                use irradiance_runtime::gfx::device::DeviceOwned;

                let module = irradiance_runtime::gfx::pipeline::ShaderModule::builder(device, &BINARY).build()?;
                module.set_debug_utils_object_name(#name);
                Ok(module)
            }
        }

        impl Shader {
            #vertex_attributes
        }
    }
        .into()
}

struct ShaderDefinition {
    include_dirs: Vec<PathBuf>,
    entry_point: String,
    ty: ShaderKind,
    path: PathBuf,
}

impl Parse for ShaderDefinition {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        struct Output {
            include_dirs: Option<Vec<PathBuf>>,
            entry_point: Option<String>,
            ty: Option<ShaderKind>,
            path: Option<PathBuf>,
        }
        let mut output = Output {
            include_dirs: None,
            entry_point: None,
            ty: None,
            path: None,
        };

        while !input.is_empty() {
            let ident: Ident = input.parse()?;
            input.parse::<Token![:]>()?;
            let ident = ident.to_string();

            match ident.as_str() {
                "path" => {
                    if output.path.is_some() {
                        panic!("Multiple definitions of `path`");
                    }

                    let path: LitStr = input.parse()?;
                    output.path = Some(path.value().into());
                }
                "ty" => {
                    if output.ty.is_some() {
                        panic!("Multiple definitions of `ty`");
                    }

                    let ty: LitStr = input.parse()?;
                    let ty = match ty.value().as_ref() {
                        "vertex" => ShaderKind::Vertex,
                        "fragment" => ShaderKind::Fragment,
                        "geometry" => ShaderKind::Geometry,
                        "tesselation_control" => ShaderKind::TessControl,
                        "tesselation_evaluation" => ShaderKind::TessEvaluation,
                        "compute" => ShaderKind::Compute,
                        "raygen" => ShaderKind::RayGeneration,
                        "any_hit" => ShaderKind::AnyHit,
                        "closest_hit" => ShaderKind::ClosestHit,
                        "miss" => ShaderKind::Miss,
                        "intersection" => ShaderKind::Intersection,
                        "callable" => ShaderKind::Callable,
                        "task" => ShaderKind::Task,
                        "mesh" => ShaderKind::Mesh,
                        _ => panic!("Unexpected shader type"),
                    };
                    output.ty = Some(ty);
                }
                "entry_point" => {
                    if output.entry_point.is_some() {
                        panic!("Multiple definitions of `entry_point`");
                    }

                    let entry_point: LitStr = input.parse()?;
                    output.entry_point = Some(entry_point.value());
                }
                "include_dirs" => {
                    if output.include_dirs.is_some() {
                        panic!("Multiple definitions of `include_dirs`");
                    }

                    let content;
                    syn::bracketed!(content in input);
                    let include_dirs: Punctuated<LitStr, Token![,]> =
                        content.parse_terminated(|buffer| buffer.parse::<LitStr>())?;
                    let include_dirs = include_dirs
                        .iter()
                        .map(|literal| literal.value().into())
                        .collect();
                    output.include_dirs = Some(include_dirs);
                }
                unhandled => panic!("Unexpected field {:?}", unhandled),
            }

            if !input.is_empty() {
                input.parse::<Token![,]>()?;
            }
        }

        Ok(ShaderDefinition {
            path: output.path.expect("Missing shader path"),
            ty: output.ty.expect("Missing target profile"),
            entry_point: output.entry_point.expect("Missing entry point"),
            include_dirs: output.include_dirs.unwrap_or_default(),
        })
    }
}

struct CompiledShader {
    inner: CompilationArtifact,
}

impl ToTokens for CompiledShader {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        let binary = self.inner.as_binary();

        tokens.append(Ident::new("const", Span::call_site()));
        tokens.append(Ident::new("BINARY", Span::call_site()));
        tokens.append(Punct::new(':', Spacing::Joint));

        let mut array = proc_macro2::TokenStream::new();
        array.append(Ident::new("u32", Span::call_site()));
        array.append(Punct::new(';', Spacing::Joint));
        array.append(Literal::usize_unsuffixed(binary.len()));

        tokens.append(Group::new(Delimiter::Bracket, array));
        tokens.append(Punct::new('=', Spacing::Alone));

        let mut bytes = proc_macro2::TokenStream::new();
        for byte in binary {
            bytes.append(Literal::u32_unsuffixed(*byte));
            bytes.append(Punct::new(',', Spacing::Joint));
        }

        tokens.append(Group::new(Delimiter::Bracket, bytes));
        tokens.append(Punct::new(';', Spacing::Joint));
    }
}

impl From<CompilationArtifact> for CompiledShader {
    fn from(inner: CompilationArtifact) -> Self {
        Self { inner }
    }
}
