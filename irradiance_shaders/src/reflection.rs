use std::collections::HashMap;
use std::str::FromStr;

use itertools::Itertools;
use proc_macro2::TokenStream;
use quote::ToTokens;
use rspirv::binary::parse_words;
use rspirv::dr::{Loader, Module, Operand};
use rspirv::spirv::{Decoration, ExecutionModel, Op, StorageClass};

pub struct VertexAttributes {
    attributes: Vec<(u32, InputType)>,
}

impl VertexAttributes {
    pub fn new(binary: &[u32]) -> Option<Self> {
        let mut loader = Loader::new();
        parse_words(binary, &mut loader).unwrap();
        let module = loader.module();

        if !module
            .entry_points
            .first()
            .unwrap()
            .operands
            .contains(&Operand::ExecutionModel(ExecutionModel::Vertex))
        {
            return None;
        }

        let attributes = Self::describe_vertex_attributes(&module);

        if attributes.is_empty() {
            None
        } else {
            Some(Self { attributes })
        }
    }

    fn describe_vertex_attributes(module: &Module) -> Vec<(u32, InputType)> {
        let locations = Self::find_locations(module);
        let input_types = Self::find_input_types(module);
        let mut input = Vec::new();
        for operand in &module.entry_points.first().unwrap().operands {
            if let Some(id) = operand.id_ref_any() {
                if let (Some(location), Some(ty)) = (locations.get(&id), input_types.get(&id)) {
                    input.push((*location, *ty));
                }
            }
        }
        input
    }

    fn find_locations(module: &Module) -> HashMap<u32, u32> {
        module
            .annotations
            .iter()
            .filter(|instruction| instruction.class.opcode == Op::Decorate)
            .filter(|instruction| {
                instruction.operands[1] == Operand::Decoration(Decoration::Location)
            })
            .map(|instruction| {
                (
                    instruction.operands[0].unwrap_id_ref(),
                    instruction.operands[2].unwrap_literal_int32(),
                )
            })
            .collect()
    }

    fn find_input_types(module: &Module) -> HashMap<u32, InputType> {
        let mut input_types = HashMap::new();
        let mut stack = module
            .types_global_values
            .iter()
            .filter(|instruction| instruction.class.opcode == Op::Variable)
            .filter(|instruction| {
                instruction.operands.first() == Some(&Operand::StorageClass(StorageClass::Input))
            })
            .collect::<Vec<_>>();
        let mut input_id = None;
        while !stack.is_empty() {
            let node = stack.pop().unwrap();
            match node.class.opcode {
                Op::Variable => {
                    input_id = node.result_id;
                    stack.extend(
                        module
                            .types_global_values
                            .iter()
                            .filter(|instruction| instruction.result_id == node.result_type),
                    );
                }
                Op::TypePointer => {
                    stack.extend(module.types_global_values.iter().filter(|instruction| {
                        instruction.result_id == Some(node.operands[1].unwrap_id_ref())
                    }));
                }
                Op::TypeVector => {
                    let vector_type = module
                        .types_global_values
                        .iter()
                        .find(|instruction| {
                            instruction.result_id == Some(node.operands[0].unwrap_id_ref())
                        })
                        .unwrap()
                        .class
                        .opcode;
                    let vector_dim = node.operands[1].unwrap_literal_int32();

                    match (vector_type, vector_dim) {
                        (Op::TypeFloat, 2) => {
                            input_types.insert(input_id.unwrap(), InputType::Vec2);
                        }
                        (Op::TypeFloat, 3) => {
                            input_types.insert(input_id.unwrap(), InputType::Vec3);
                        }
                        (Op::TypeFloat, 4) => {
                            input_types.insert(input_id.unwrap(), InputType::Vec4);
                        }
                        _ => panic!("Unknown vector type"),
                    }
                }
                _ => {}
            }
        }
        input_types
    }
}

impl ToTokens for VertexAttributes {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let mut lines = vec![
            String::from(
                "pub fn vertex_input() -> irradiance_runtime::gfx::pipeline::VertexInput {",
            ),
            String::from("\tirradiance_runtime::gfx::pipeline::VertexInput::builder()"),
        ];
        let stride: u32 = self
            .attributes
            .iter()
            .map(|(_, input_type)| u32::from(*input_type))
            .sum();
        lines.push(format!(
            "\t\t.binding({}, {}, irradiance_runtime::gfx::pipeline::VertexInputRate::Vertex)",
            0, stride
        ));
        let mut offset = 0u32;
        for (location, input_type) in self
            .attributes
            .iter()
            .sorted_by_key(|(location, _)| *location)
        {
            lines.push(format!(
                "\t\t.attribute({}, {}, irradiance_runtime::gfx::image::Format::{}, {})",
                location,
                0,
                input_type.to_format(),
                offset
            ));
            offset += u32::from(*input_type);
        }
        lines.push(String::from("\t\t.build()"));
        lines.push(String::from("}"));

        *tokens = proc_macro2::TokenStream::from_str(lines.join("\n").as_str()).unwrap();
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum InputType {
    Float,
    Vec2,
    Vec3,
    Vec4,
}

impl InputType {
    fn to_format(self) -> &'static str {
        match self {
            InputType::Float => "R32SFloat",
            InputType::Vec2 => "R32G32SFloat",
            InputType::Vec3 => "R32G32B32SFloat",
            InputType::Vec4 => "R32G32B32A32SFloat",
        }
    }
}

impl From<InputType> for u32 {
    fn from(input_type: InputType) -> Self {
        match input_type {
            InputType::Float => 4,
            InputType::Vec2 => 8,
            InputType::Vec3 => 12,
            InputType::Vec4 => 16,
        }
    }
}
