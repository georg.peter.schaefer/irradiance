use std::{
    ffi::OsStr,
    fmt::{Debug, Display},
    fs, io,
    path::PathBuf,
    process::Command,
};

use clap::Parser;
use serde::Serialize;

#[derive(Parser)]
#[clap(
    bin_name = "cargo irradiance-generate",
    about = "cargo-irradiance-generate",
    long_about = "Creates a new irradiance project at <PATH>"
)]
struct Cli {
    #[clap(value_parser, long, help = "Display title for the project")]
    title: String,
    #[clap(value_parser)]
    path: PathBuf,
    #[clap(
        value_parser,
        long,
        help = "Filesystem path to the local engine workspace"
    )]
    local: Option<PathBuf>,
}

impl Cli {
    fn validate(&self) -> Result<(), Error> {
        if self.path.exists() {
            Err(Error::NonEmptyDir(self.path.clone()))
        } else {
            Ok(())
        }
    }

    fn create_workspace(&self) -> Result<(), Error> {
        fs::create_dir(&self.path)?;
        fs::create_dir(&self.path.join("assets"))?;
        Ok(())
    }

    fn create_runtime(&self) -> Result<(), Error> {
        Command::new("cargo")
            .arg("new")
            .arg("--quiet")
            .arg("--lib")
            .arg("--vcs")
            .arg("none")
            .arg(self.runtime_path())
            .status()?;
        self.add_engine_dependency(self.runtime_manifest(), "irradiance_runtime")?;
        self.add_dependency(self.runtime_manifest(), "log")?;
        self.create_runtime_lib()?;
        Ok(())
    }

    fn create_runtime_lib(&self) -> Result<(), Error> {
        let lib = include_str!("lib.rs.in");
        fs::write(self.runtime_path().join("src").join("lib.rs"), lib)?;
        Ok(())
    }

    fn create_editor(&self) -> Result<(), Error> {
        Command::new("cargo")
            .arg("new")
            .arg("--quiet")
            .arg("--bin")
            .arg("--vcs")
            .arg("none")
            .arg(self.editor_path())
            .status()?;
        self.add_engine_dependency(self.editor_manifest(), "irradiance_runtime")?;
        self.add_engine_dependency(self.editor_manifest(), "irradiance_editor")?;
        self.add_runtime_dependency(self.editor_manifest())?;
        self.create_editor_main()?;
        Ok(())
    }

    fn create_editor_main(&self) -> Result<(), Error> {
        let lib = include_str!("editor.rs.in")
            .replace("<TITLE>", &self.title)
            .replace("<RUNTIME_NAME>", &self.runtime_name());
        fs::write(self.editor_path().join("src").join("main.rs"), lib)?;
        Ok(())
    }

    fn create_standalone(&self) -> Result<(), Error> {
        Command::new("cargo")
            .arg("new")
            .arg("--quiet")
            .arg("--bin")
            .arg("--vcs")
            .arg("none")
            .arg(self.standalone_path())
            .status()?;
        self.add_engine_dependency(self.standalone_manifest(), "irradiance_runtime")?;
        self.add_runtime_dependency(self.standalone_manifest())?;
        self.create_standalone_main()?;
        Ok(())
    }

    fn create_standalone_main(&self) -> Result<(), Error> {
        let lib = include_str!("standalone.rs.in")
            .replace("<TITLE>", &self.title)
            .replace("<RUNTIME_NAME>", &self.runtime_name());
        fs::write(self.standalone_path().join("src").join("main.rs"), lib)?;
        Ok(())
    }

    fn create_workspace_manifest(&self) -> Result<(), Error> {
        fs::write(
            self.workspace_manifest(),
            toml::to_string_pretty(&Manifest::workspace(vec![
                self.runtime_name(),
                self.editor_name(),
                self.project_name(),
            ]))
            .expect("valid workspace manifest"),
        )?;
        Ok(())
    }

    fn workspace_manifest(&self) -> PathBuf {
        self.path.join("Cargo.toml")
    }

    fn runtime_manifest(&self) -> PathBuf {
        self.runtime_path().join("Cargo.toml")
    }

    fn editor_manifest(&self) -> PathBuf {
        self.editor_path().join("Cargo.toml")
    }

    fn standalone_manifest(&self) -> PathBuf {
        self.standalone_path().join("Cargo.toml")
    }

    fn runtime_path(&self) -> PathBuf {
        self.path.join(self.runtime_name())
    }

    fn editor_path(&self) -> PathBuf {
        self.path.join(self.editor_name())
    }

    fn standalone_path(&self) -> PathBuf {
        self.path.join(self.project_name())
    }

    fn runtime_name(&self) -> String {
        self.project_name() + "_runtime"
    }

    fn editor_name(&self) -> String {
        self.project_name() + "_editor"
    }

    fn project_name(&self) -> String {
        self.path
            .file_name()
            .and_then(OsStr::to_str)
            .map(str::to_string)
            .expect("valid utf8 file name")
    }

    fn add_engine_dependency(&self, manifest_path: PathBuf, dependency: &str) -> Result<(), Error> {
        if let Some(local) = &self.local {
            Command::new("cargo")
                .arg("add")
                .arg("--quiet")
                .arg("--manifest-path")
                .arg(manifest_path)
                .arg("--path")
                .arg(local.join(dependency))
                .status()?;
        } else {
            Command::new("cargo")
                .arg("add")
                .arg("--quiet")
                .arg("--manifest-path")
                .arg(manifest_path)
                .arg(dependency)
                .status()?;
        }
        Ok(())
    }

    fn add_runtime_dependency(&self, manifest_path: PathBuf) -> Result<(), Error> {
        Command::new("cargo")
            .arg("add")
            .arg("--quiet")
            .arg("--manifest-path")
            .arg(manifest_path)
            .arg("--path")
            .arg(self.path.join(self.runtime_name()))
            .status()?;
        Ok(())
    }

    fn add_dependency(&self, manifest_path: PathBuf, dependency: &str) -> Result<(), Error> {
        Command::new("cargo")
            .arg("add")
            .arg("--quiet")
            .arg("--manifest-path")
            .arg(manifest_path)
            .arg(dependency)
            .status()?;
        Ok(())
    }
}

#[derive(Serialize)]
struct Manifest {
    workspace: Option<Workspace>,
}

impl Manifest {
    fn workspace(members: Vec<String>) -> Self {
        Self {
            workspace: Some(Workspace { members }),
        }
    }
}

#[derive(Serialize)]
struct Workspace {
    members: Vec<String>,
}

enum Error {
    NonEmptyDir(PathBuf),
    IO(io::Error),
}

impl Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(self, f)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::NonEmptyDir(path) => write!(f, " destination {path:?} already exists"),
            Error::IO(e) => write!(f, "{e}"),
        }
    }
}

impl std::error::Error for Error {}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Self::IO(e)
    }
}

fn main() -> Result<(), Error> {
    let cli = Cli::parse();

    cli.validate()?;
    cli.create_workspace()?;
    cli.create_runtime()?;
    cli.create_editor()?;
    cli.create_standalone()?;
    cli.create_workspace_manifest()?;

    Ok(())
}
