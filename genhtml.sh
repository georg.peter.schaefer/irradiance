#!/usr/bin/env bash

rm -rf /tmp/irradiance-coverage/
LLVM_PROFILE_FILE="coverage-%p-%m.profraw" RUSTFLAGS="-C instrument-coverage" cargo +nightly test --target x86_64-unknown-linux-gnu
grcov . --binary-path ./target/x86_64-unknown-linux-gnu/debug/ -s . -t lcov --branch --ignore-not-existing --keep-only "*irradiance*" --ignore "target/*" --ignore "irradiance_gl/*" -o coverage.lcov
find . -type f -name "*.profraw" -exec rm {} \;
genhtml -o /tmp/irradiance-coverage coverage.lcov
xdg-open /tmp/irradiance-coverage/index.html
rm coverage.lcov