use std::collections::hash_map::Keys;
use std::iter::Map;
use std::path::{Path, PathBuf};
use std::{collections::HashMap, fs, sync::Arc};

use approx::relative_ne;
use winit::event::Event;

use crate::core::Frame;
use crate::input::{
    Axis, AxisBinding, Button, ButtonBinding, Gamepad, InputAction, InputActionMap,
    InputActionMaps, InputDevice, Keyboard, Mouse, ScalarBinding, ScalarCompoundBinding,
    ValueAction, Vector2DAxisBinding, Vector2DBinding, Vector2DCompoundBinding,
};
use crate::math::Vec2;
use crate::window::Window;

/// Input.
pub struct Input {
    action_maps: InputActionMaps,
    input_devices: HashMap<String, Box<dyn InputDevice>>,
    path: PathBuf,
}

impl Input {
    /// Creates a new input.
    pub fn new(asset_path: PathBuf, window: Arc<Window>) -> Self {
        let mouse = Box::new(Mouse::new(window));
        let keyboard = Box::new(Keyboard::new());
        let gamepad = Box::new(Gamepad::new());
        let path = asset_path.join("input.actions");
        let action_maps = fs::read(&path)
            .ok()
            .and_then(|bytes| ron::de::from_bytes(&bytes).ok())
            .unwrap_or_default();

        Self {
            input_devices: [
                (mouse.name().to_string(), mouse as Box<dyn InputDevice>),
                (
                    keyboard.name().to_string(),
                    keyboard as Box<dyn InputDevice>,
                ),
                (gamepad.name().to_string(), gamepad as Box<dyn InputDevice>),
            ]
            .into_iter()
            .collect(),
            action_maps,
            path,
        }
    }

    /// Returns an input device by its name.
    pub fn get<D: InputDevice + 'static>(&self, name: impl AsRef<str>) -> &D {
        self.input_devices
            .get(name.as_ref())
            .map(|input_device| input_device.as_any())
            .and_then(|input_device| input_device.downcast_ref::<D>())
            .expect("input device")
    }

    /// Returns a mutable input device by its name.
    pub fn get_mut<D: InputDevice + 'static>(&mut self, name: impl AsRef<str>) -> &mut D {
        self.input_devices
            .get_mut(name.as_ref())
            .map(|input_device| input_device.as_any_mut())
            .and_then(|input_device| input_device.downcast_mut::<D>())
            .expect("input device")
    }

    /// Returns the device names.
    pub fn device_names(&self) -> Keys<'_, String, Box<dyn InputDevice>> {
        self.input_devices.keys()
    }

    /// Returns the names of all buttons of a device.
    pub fn device_button_names(
        &self,
        device_name: impl AsRef<str>,
    ) -> Map<Box<dyn Iterator<Item = &dyn Button> + '_>, fn(&dyn Button) -> &str> {
        self.input_devices
            .get(device_name.as_ref())
            .map(|device| device.buttons())
            .unwrap_or_else(|| Box::new(std::iter::empty()))
            .map(|button| button.name())
    }

    /// Returns the names of all axes of a device.
    pub fn device_axis_names(
        &self,
        device_name: impl AsRef<str>,
    ) -> Map<Box<dyn Iterator<Item = &dyn Axis> + '_>, fn(&dyn Axis) -> &str> {
        self.input_devices
            .get(device_name.as_ref())
            .map(|device| device.axes())
            .unwrap_or_else(|| Box::new(std::iter::empty()))
            .map(|button| button.name())
    }

    /// Returns a button by its path.
    pub fn button(&self, path: impl AsRef<Path>) -> &dyn Button {
        let path = path.as_ref();
        let device = path
            .components()
            .next()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        let button = path
            .components()
            .last()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        self.input_devices
            .get(device)
            .and_then(|input_device| input_device.button(button))
            .expect("button")
    }

    /// Returns an axis by its path.
    pub fn axis(&self, path: impl AsRef<Path>) -> &dyn Axis {
        let path = path.as_ref();
        let device = path
            .components()
            .next()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        let axis = path
            .components()
            .last()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        self.input_devices
            .get(device)
            .and_then(|input_device| input_device.axis(axis))
            .expect("axis")
    }

    /// Returns all actions maps.
    pub fn action_maps(&self) -> &InputActionMaps {
        &self.action_maps
    }

    /// Returns a mutable reference to all action maps.
    pub fn action_maps_mut(&mut self) -> &mut InputActionMaps {
        &mut self.action_maps
    }

    /// Returns an action map.
    pub fn action_map(&self, name: impl AsRef<str>) -> Option<&InputActionMap> {
        self.action_maps.0.get(name.as_ref())
    }

    /// Returns a mutable action map.
    pub fn action_map_mut(&mut self, name: impl AsRef<str>) -> Option<&mut InputActionMap> {
        self.action_maps.0.get_mut(name.as_ref())
    }

    /// Handles input events.
    pub fn on_event(&mut self, event: &Event<()>) {
        for input_device in self.input_devices.values_mut() {
            input_device.on_event(event);
        }
    }

    /// Updates the input.
    pub fn update(&mut self, frame: &mut Frame) {
        for input_device in self.input_devices.values_mut() {
            input_device.update(frame);
        }
    }

    /// Resets the input state.
    pub fn reset(&mut self) {
        for input_device in self.input_devices.values_mut() {
            input_device.reset();
        }
    }

    /// Save changes.
    pub fn save(&self) {
        fs::write(
            &self.path,
            ron::to_string(&self.action_maps).expect("serialized actions"),
        )
        .expect("written actions");
    }
}

impl Input {
    /// Returns the product of an action.
    pub fn action<T>(&self, path: impl AsRef<Path>) -> T
    where
        Self: Actions<T>,
    {
        self.get_action(path)
    }
}

/// Trait for retrieving actions.
pub trait Actions<T> {
    /// Returns the product of an action.
    fn get_action(&self, path: impl AsRef<Path>) -> T;
}

impl Actions<bool> for Input {
    fn get_action(&self, path: impl AsRef<Path>) -> bool {
        let path = path.as_ref();
        let action_map = path
            .components()
            .next()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        let action = path
            .components()
            .last()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        match &self.action_maps.0[action_map].0[action] {
            InputAction::Button(action) => action
                .bindings
                .values()
                .flat_map(|binding| &binding.button)
                .map(|button| self.button(button))
                .any(|button| {
                    if action.down {
                        button.down()
                    } else {
                        button.pressed()
                    }
                }),
            _ => panic!("Not a button action"),
        }
    }
}

impl Actions<f32> for Input {
    fn get_action(&self, path: impl AsRef<Path>) -> f32 {
        let path = path.as_ref();
        let action_map = path
            .components()
            .next()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        let action = path
            .components()
            .last()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        match &self.action_maps.0[action_map].0[action] {
            InputAction::Value(ValueAction::Scalar(action)) => {
                let mut value = 0.0;
                for binding in action.bindings.values() {
                    match binding {
                        ScalarBinding::Axis(AxisBinding { axis: Some(axis) }) => {
                            let axis = self.axis(axis);
                            if relative_ne!(axis.get(), 0.0) {
                                value = axis.get();
                                break;
                            }
                        }
                        ScalarBinding::Compound(ScalarCompoundBinding {
                            min: ButtonBinding { button: Some(min) },
                            max: ButtonBinding { button: Some(max) },
                        }) => {
                            let min = self.button(min).down();
                            let max = self.button(max).down();

                            if min {
                                value -= 1.0;
                            }
                            if max {
                                value += 1.0;
                            }
                            if min || max {
                                break;
                            }
                        }
                        _ => {}
                    }
                }
                value
            }
            _ => panic!("Not a scalar value action"),
        }
    }
}

impl Actions<Vec2> for Input {
    fn get_action(&self, path: impl AsRef<Path>) -> Vec2 {
        let path = path.as_ref();
        let action_map = path
            .components()
            .next()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        let action = path
            .components()
            .last()
            .unwrap()
            .as_os_str()
            .to_str()
            .unwrap();
        match &self.action_maps.0[action_map].0[action] {
            InputAction::Value(ValueAction::Vector2D(action)) => {
                let mut value = Vec2::zero();
                for binding in action.bindings.values() {
                    match binding {
                        Vector2DBinding::Axis(Vector2DAxisBinding {
                            horizontal:
                                AxisBinding {
                                    axis: Some(horizontal),
                                },
                            vertical:
                                AxisBinding {
                                    axis: Some(vertical),
                                },
                        }) => {
                            let horizontal = self.axis(horizontal);
                            let vertical = self.axis(vertical);
                            if relative_ne!(horizontal.get(), 0.0)
                                || relative_ne!(vertical.get(), 0.0)
                            {
                                value[0] = horizontal.get();
                                value[1] = vertical.get();
                                break;
                            }
                        }
                        Vector2DBinding::Compound(Vector2DCompoundBinding {
                            up: ButtonBinding { button: Some(up) },
                            down: ButtonBinding { button: Some(down) },
                            left: ButtonBinding { button: Some(left) },
                            right:
                                ButtonBinding {
                                    button: Some(right),
                                },
                        }) => {
                            let up = self.button(up).down();
                            let down = self.button(down).down();
                            let left = self.button(left).down();
                            let right = self.button(right).down();

                            if up {
                                value[1] += 1.0;
                            }
                            if down {
                                value[1] -= 1.0;
                            }
                            if left {
                                value[0] -= 1.0;
                            }
                            if right {
                                value[0] += 1.0;
                            }

                            if up || down || left || right {
                                break;
                            }
                        }
                        _ => {}
                    }
                }

                if action.normalize && relative_ne!(value, Vec2::zero()) {
                    value = value.normalize();
                }

                value
            }
            _ => panic!("Not a scalar value action"),
        }
    }
}
