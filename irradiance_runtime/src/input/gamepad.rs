use std::any::Any;
use std::collections::HashMap;

use gilrs::{EventType, Gilrs};
use winit::event::Event;

use crate::core::Frame;
use crate::input::{Axis, Button, InputDevice};
use crate::math::Vec2;

/// Gamepad input device.
pub struct Gamepad {
    pressed_threshold: f32,
    axes: Vec<GamepadAxis>,
    axis_names: HashMap<String, usize>,
    axis_enums: HashMap<gilrs::Axis, usize>,
    buttons: Vec<GamepadButton>,
    button_names: HashMap<String, usize>,
    button_enums: HashMap<gilrs::Button, usize>,
    gilrs: Gilrs,
}

impl Gamepad {
    pub(crate) fn new() -> Self {
        Self {
            gilrs: Gilrs::new().unwrap(),
            button_enums: [
                (gilrs::Button::South, 0),
                (gilrs::Button::East, 1),
                (gilrs::Button::North, 2),
                (gilrs::Button::West, 3),
                (gilrs::Button::LeftTrigger, 4),
                (gilrs::Button::LeftTrigger2, 5),
                (gilrs::Button::RightTrigger, 6),
                (gilrs::Button::RightTrigger2, 7),
                (gilrs::Button::Select, 8),
                (gilrs::Button::Start, 9),
                (gilrs::Button::LeftThumb, 10),
                (gilrs::Button::RightThumb, 11),
                (gilrs::Button::DPadUp, 12),
                (gilrs::Button::DPadDown, 13),
                (gilrs::Button::DPadLeft, 14),
                (gilrs::Button::DPadRight, 15),
            ]
            .into_iter()
            .collect(),
            button_names: [
                ("South".to_string(), 0),
                ("East".to_string(), 1),
                ("North".to_string(), 2),
                ("West".to_string(), 3),
                ("LeftTrigger".to_string(), 4),
                ("LeftTrigger2".to_string(), 5),
                ("RightTrigger".to_string(), 6),
                ("RightTrigger2".to_string(), 7),
                ("Select".to_string(), 8),
                ("Start".to_string(), 9),
                ("LeftThumb".to_string(), 10),
                ("RightThumb".to_string(), 11),
                ("DPadUp".to_string(), 12),
                ("DPadDown".to_string(), 13),
                ("DPadLeft".to_string(), 14),
                ("DPadRight".to_string(), 15),
            ]
            .into_iter()
            .collect(),
            buttons: vec![
                GamepadButton::new("South"),
                GamepadButton::new("East"),
                GamepadButton::new("North"),
                GamepadButton::new("West"),
                GamepadButton::new("LeftTrigger"),
                GamepadButton::new("LeftTrigger2"),
                GamepadButton::new("RightTrigger"),
                GamepadButton::new("RightTrigger2"),
                GamepadButton::new("Select"),
                GamepadButton::new("Start"),
                GamepadButton::new("LeftThumb"),
                GamepadButton::new("RightThumb"),
                GamepadButton::new("DPadUp"),
                GamepadButton::new("DPadDown"),
                GamepadButton::new("DPadLeft"),
                GamepadButton::new("DPadRight"),
            ],
            axis_enums: [
                (gilrs::Axis::LeftStickX, 0),
                (gilrs::Axis::LeftStickY, 1),
                (gilrs::Axis::RightStickX, 2),
                (gilrs::Axis::RightStickY, 3),
            ]
            .into_iter()
            .collect(),
            axis_names: [
                ("LeftStickX".to_string(), 0),
                ("LeftStickY".to_string(), 1),
                ("RightStickX".to_string(), 2),
                ("RightStickY".to_string(), 3),
            ]
            .into_iter()
            .collect(),
            axes: vec![
                GamepadAxis::new("LeftStickX"),
                GamepadAxis::new("LeftStickY"),
                GamepadAxis::new("RightStickX"),
                GamepadAxis::new("RightStick>"),
            ],
            pressed_threshold: 0.5,
        }
    }
}

impl InputDevice for Gamepad {
    fn name(&self) -> &str {
        "Gamepad"
    }

    fn button(&self, name: &str) -> Option<&dyn Button> {
        self.button_names
            .get(name)
            .and_then(|index| self.buttons.get(*index))
            .map(|button| button as &dyn Button)
    }

    fn buttons(&self) -> Box<dyn Iterator<Item = &dyn Button> + '_> {
        Box::new(self.buttons.iter().map(|button| button as &dyn Button))
    }

    fn axis(&self, name: &str) -> Option<&dyn Axis> {
        self.axis_names
            .get(name)
            .and_then(|index| self.axes.get(*index))
            .map(|axis| axis as &dyn Axis)
    }

    fn axes(&self) -> Box<dyn Iterator<Item = &dyn Axis> + '_> {
        Box::new(self.axes.iter().map(|axis| axis as &dyn Axis))
    }

    fn on_event(&mut self, _event: &Event<()>) {}

    fn update(&mut self, frame: &mut Frame) {
        while let Some(gilrs::Event { event, .. }) = self.gilrs.next_event() {
            match event {
                EventType::ButtonPressed(button, _) => {
                    if let Some(mut button) = self
                        .button_enums
                        .get(&button)
                        .and_then(|index| self.buttons.get_mut(*index))
                    {
                        button.down = true;
                        button.down_time = 0.0;
                    }
                }
                EventType::ButtonReleased(button, _) => {
                    if let Some(mut button) = self
                        .button_enums
                        .get(&button)
                        .and_then(|index| self.buttons.get_mut(*index))
                    {
                        button.down = false;
                        button.pressed = button.down_time <= self.pressed_threshold;
                    }
                }
                EventType::AxisChanged(axis, value, _) => {
                    if let Some(mut axis) = self
                        .axis_enums
                        .get(&axis)
                        .and_then(|index| self.axes.get_mut(*index))
                    {
                        axis.value = value;
                    }
                }
                _ => {}
            }
        }

        for mut button in self.buttons.iter_mut().filter(|button| button.down) {
            button.down_time += frame.time().delta();
        }

        let left_stick = Vec2::new(self.axes[0].value, self.axes[1].value);
        if left_stick.length() >= 1.0 {
            let left_stick = left_stick.normalize();
            self.axes[0].value = left_stick[0];
            self.axes[1].value = left_stick[1];
        }

        let right_stick = Vec2::new(self.axes[2].value, self.axes[3].value);
        if right_stick.length() >= 1.0 {
            let right_stick = right_stick.normalize();
            self.axes[2].value = right_stick[0];
            self.axes[3].value = right_stick[1];
        }
    }

    fn reset(&mut self) {
        for button in &mut self.buttons {
            button.pressed = false;
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

struct GamepadButton {
    down_time: f32,
    pressed: bool,
    down: bool,
    name: String,
}

impl GamepadButton {
    fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            down: false,
            pressed: false,
            down_time: 0.0,
        }
    }
}

impl Button for GamepadButton {
    fn name(&self) -> &str {
        &self.name
    }

    fn down(&self) -> bool {
        self.down
    }

    fn up(&self) -> bool {
        !self.down
    }

    fn pressed(&self) -> bool {
        self.pressed
    }
}

struct GamepadAxis {
    value: f32,
    name: String,
}

impl GamepadAxis {
    fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            value: 0.0,
        }
    }
}

impl Axis for GamepadAxis {
    fn name(&self) -> &str {
        &self.name
    }

    fn get(&self) -> f32 {
        self.value
    }
}
