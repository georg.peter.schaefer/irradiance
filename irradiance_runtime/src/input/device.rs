use std::any::Any;
use winit::event::Event;

use crate::core::Frame;

/// A trait describing an input device.
pub trait InputDevice {
    /// Returns the name of the input device.
    fn name(&self) -> &str;
    /// Returns a button by its name.
    fn button(&self, name: &str) -> Option<&dyn Button>;
    /// Returns an iterator to all available buttons.
    fn buttons(&self) -> Box<dyn Iterator<Item = &dyn Button> + '_>;
    /// Returns an axis by its name.
    fn axis(&self, name: &str) -> Option<&dyn Axis>;
    /// Returns an iterator to all available axis.
    fn axes(&self) -> Box<dyn Iterator<Item = &dyn Axis> + '_>;
    /// Handles input events.
    fn on_event(&mut self, event: &Event<()>);
    /// Updates the input device.
    fn update(&mut self, frame: &mut Frame);
    /// Resets the input device.
    fn reset(&mut self);
    /// Returns as any.
    fn as_any(&self) -> &dyn Any;
    /// Returns as mutable any.
    fn as_any_mut(&mut self) -> &mut dyn Any;
}

/// A trait describing an input device button.
pub trait Button {
    /// Returns the buttons name.
    fn name(&self) -> &str;
    /// Returns if the button is down.
    fn down(&self) -> bool;
    /// Returns if the button is up.
    fn up(&self) -> bool;
    /// Returns if the button has been pressed.
    fn pressed(&self) -> bool;
}

/// A trait describing an input device axis.
pub trait Axis {
    /// Returns the axis name.
    fn name(&self) -> &str;
    /// Returns the axis value.
    fn get(&self) -> f32;
}
