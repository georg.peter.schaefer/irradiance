//! Input handling.

pub use action::AxisBinding;
pub use action::Binding;
pub use action::ButtonAction;
pub use action::ButtonBinding;
pub use action::InputAction;
pub use action::InputActionMap;
pub use action::InputActionMaps;
pub use action::ScalarAction;
pub use action::ScalarBinding;
pub use action::ScalarCompoundBinding;
pub use action::ValueAction;
pub use action::Vector2DAction;
pub use action::Vector2DAxisBinding;
pub use action::Vector2DBinding;
pub use action::Vector2DCompoundBinding;
pub use device::Axis;
pub use device::Button;
pub use device::InputDevice;
pub use gamepad::Gamepad;
pub use keyboard::Keyboard;
pub use mouse::Mouse;
pub use private::Input;

mod action;
mod device;
mod gamepad;
mod keyboard;
mod mouse;
mod private;
