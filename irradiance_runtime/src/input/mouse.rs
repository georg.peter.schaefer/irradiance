use crate::core::Frame;
use irradiance_runtime::window::Window;
use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;
use winit::event::{DeviceEvent, ElementState, Event, WindowEvent};
use winit::window::CursorGrabMode;

use crate::input::{Axis, Button, InputDevice};
use crate::math::Vec2;

/// Mouse input device.
pub struct Mouse {
    offset: Vec2,
    continuous: bool,
    pressed_threshold: f32,
    axes: Vec<MouseAxis>,
    axis_names: HashMap<String, usize>,
    buttons: Vec<MouseButton>,
    button_names: HashMap<String, usize>,
    button_enums: HashMap<winit::event::MouseButton, usize>,
    window: Arc<Window>,
}

impl Mouse {
    pub(crate) fn new(window: Arc<Window>) -> Self {
        Self {
            window,
            button_enums: [
                (winit::event::MouseButton::Left, 0),
                (winit::event::MouseButton::Middle, 1),
                (winit::event::MouseButton::Right, 2),
            ]
            .into_iter()
            .collect(),
            button_names: [
                ("Left".to_string(), 0),
                ("Middle".to_string(), 1),
                ("Right".to_string(), 2),
            ]
            .into_iter()
            .collect(),
            buttons: vec![
                MouseButton::new("Left"),
                MouseButton::new("Middle"),
                MouseButton::new("Right"),
            ],
            axis_names: [
                ("Horizontal".to_string(), 0),
                ("Vertical".to_string(), 1),
                ("HorizontalDelta".to_string(), 2),
                ("VerticalDelta".to_string(), 3),
            ]
            .into_iter()
            .collect(),
            axes: vec![
                MouseAxis::new("Horizontal"),
                MouseAxis::new("Vertical"),
                MouseAxis::new("HorizontalDelta"),
                MouseAxis::new("VerticalDelta"),
            ],
            pressed_threshold: 0.5,
            continuous: false,
            offset: Vec2::zero(),
        }
    }

    /// Returns if the mouse motion is continuous.
    pub fn continuous(&self) -> bool {
        self.continuous
    }

    /// Sets continuous mouse motion.
    pub fn set_continuous(&mut self, continuous: bool) {
        self.continuous = continuous;
        self.window.inner().set_cursor_visible(!continuous);
        self.window
            .inner()
            .set_cursor_grab(
                continuous
                    .then_some(CursorGrabMode::Confined)
                    .unwrap_or(CursorGrabMode::None),
            )
            .expect("cursor grab");
    }

    /// Sets the offset.
    pub fn set_offset(&mut self, offset: Vec2) {
        self.offset = offset;
    }
}

impl InputDevice for Mouse {
    fn name(&self) -> &str {
        "Mouse"
    }

    fn button(&self, name: &str) -> Option<&dyn Button> {
        self.button_names
            .get(name)
            .and_then(|index| self.buttons.get(*index))
            .map(|button| button as &dyn Button)
    }

    fn buttons(&self) -> Box<dyn Iterator<Item = &dyn Button> + '_> {
        Box::new(self.buttons.iter().map(|button| button as &dyn Button))
    }

    fn axis(&self, name: &str) -> Option<&dyn Axis> {
        self.axis_names
            .get(name)
            .and_then(|index| self.axes.get(*index))
            .map(|axis| axis as &dyn Axis)
    }

    fn axes(&self) -> Box<dyn Iterator<Item = &dyn Axis> + '_> {
        Box::new(self.axes.iter().map(|axis| axis as &dyn Axis))
    }

    fn on_event(&mut self, event: &Event<()>) {
        match event {
            Event::DeviceEvent {
                event: DeviceEvent::MouseMotion { delta: (x, y) },
                ..
            } => {
                self.handle_mouse_motion(*x as _, *y as _);
            }
            Event::WindowEvent {
                event: WindowEvent::CursorMoved { position, .. },
                ..
            } => {
                self.handle_cursor_moved(position.x as _, position.y as _);
            }
            Event::WindowEvent {
                event: WindowEvent::MouseInput { state, button, .. },
                ..
            } => {
                self.handle_mouse_input(state, button);
            }
            _ => {}
        }
    }

    fn update(&mut self, frame: &mut Frame) {
        for mut button in self.buttons.iter_mut().filter(|button| button.down) {
            button.down_time += frame.time().delta();
        }
    }

    fn reset(&mut self) {
        self.axes[2].value = 0.0;
        self.axes[3].value = 0.0;
        self.buttons[0].pressed = false;
        self.buttons[1].pressed = false;
        self.buttons[2].pressed = false;
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

impl Mouse {
    fn handle_mouse_motion(&mut self, x: f32, y: f32) {
        let resolution = self.window.resolution();
        self.axes[2].value = x / (0.5 * resolution[0] as f32);
        self.axes[3].value = -y / (0.5 * resolution[1] as f32);
    }

    fn handle_cursor_moved(&mut self, x: f32, y: f32) {
        self.axes[0].value = x - self.offset[0];
        self.axes[1].value = y - self.offset[1];
    }

    fn handle_mouse_input(&mut self, state: &ElementState, button: &winit::event::MouseButton) {
        if let Some(mut button) = self
            .button_enums
            .get(button)
            .and_then(|index| self.buttons.get_mut(*index))
        {
            if *state == ElementState::Pressed {
                button.down = true;
                button.down_time = 0.0;
            } else {
                button.down = false;
                button.pressed = button.down_time <= self.pressed_threshold;
            }
        }
    }
}

struct MouseButton {
    down_time: f32,
    pressed: bool,
    down: bool,
    name: String,
}

impl MouseButton {
    fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            down: false,
            pressed: false,
            down_time: 0.0,
        }
    }
}

impl Button for MouseButton {
    fn name(&self) -> &str {
        &self.name
    }

    fn down(&self) -> bool {
        self.down
    }

    fn up(&self) -> bool {
        !self.down
    }

    fn pressed(&self) -> bool {
        self.pressed
    }
}

struct MouseAxis {
    value: f32,
    name: String,
}

impl MouseAxis {
    fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            value: 0.0,
        }
    }
}

impl Axis for MouseAxis {
    fn name(&self) -> &str {
        &self.name
    }

    fn get(&self) -> f32 {
        self.value
    }
}
