use crate::core::Frame;
use crate::input::{Axis, Button, InputDevice};
use std::any::Any;
use std::collections::HashMap;
use winit::event::{DeviceEvent, ElementState, Event, KeyboardInput, VirtualKeyCode};

/// Keyboard input device.
pub struct Keyboard {
    pressed_threshold: f32,
    keys: Vec<KeyboardKey>,
    key_names: HashMap<String, usize>,
    key_enums: HashMap<VirtualKeyCode, usize>,
}

macro_rules! impl_new {
    ($($key:ident),+) => {
        Self {
            key_enums: [$(VirtualKeyCode::$key),+].into_iter()
                .enumerate()
                .map(|(index, key)| (key, index))
                .collect(),
            key_names: [$(stringify!($key)),+].into_iter()
                .enumerate()
                .map(|(index, name)| (name.to_string(), index))
                .collect(),
            keys: vec![$(KeyboardKey::new(stringify!($key))),+],
            pressed_threshold: 0.5
        }
    };
}

impl Keyboard {
    pub(crate) fn new() -> Self {
        impl_new! {
            Key1,
            Key2,
            Key3,
            Key4,
            Key5,
            Key6,
            Key7,
            Key8,
            Key9,
            Key0,
            A,
            B,
            C,
            D,
            E,
            F,
            G,
            H,
            I,
            J,
            K,
            L,
            M,
            N,
            O,
            P,
            Q,
            R,
            S,
            T,
            U,
            V,
            W,
            X,
            Y,
            Z,
            Escape,
            F1,
            F2,
            F3,
            F4,
            F5,
            F6,
            F7,
            F8,
            F9,
            F10,
            F11,
            F12,
            F13,
            F14,
            F15,
            F16,
            F17,
            F18,
            F19,
            F20,
            F21,
            F22,
            F23,
            F24,
            Snapshot,
            Scroll,
            Pause,
            Insert,
            Home,
            Delete,
            End,
            PageDown,
            PageUp,
            Left,
            Up,
            Right,
            Down,
            Back,
            Return,
            Space,
            Compose,
            Caret,
            Numlock,
            Numpad0,
            Numpad1,
            Numpad2,
            Numpad3,
            Numpad4,
            Numpad5,
            Numpad6,
            Numpad7,
            Numpad8,
            Numpad9,
            NumpadAdd,
            NumpadDivide,
            NumpadDecimal,
            NumpadComma,
            NumpadEnter,
            NumpadEquals,
            NumpadMultiply,
            NumpadSubtract,
            AbntC1,
            AbntC2,
            Apostrophe,
            Apps,
            Asterisk,
            At,
            Ax,
            Backslash,
            Calculator,
            Capital,
            Colon,
            Comma,
            Convert,
            Equals,
            Grave,
            Kana,
            Kanji,
            LAlt,
            LBracket,
            LControl,
            LShift,
            LWin,
            Mail,
            MediaSelect,
            MediaStop,
            Minus,
            Mute,
            MyComputer,
            NavigateForward,
            NavigateBackward,
            NextTrack,
            NoConvert,
            OEM102,
            Period,
            PlayPause,
            Plus,
            Power,
            PrevTrack,
            RAlt,
            RBracket,
            RControl,
            RShift,
            RWin,
            Semicolon,
            Slash,
            Sleep,
            Stop,
            Sysrq,
            Tab,
            Underline,
            Unlabeled,
            VolumeDown,
            VolumeUp,
            Wake,
            WebBack,
            WebFavorites,
            WebForward,
            WebHome,
            WebRefresh,
            WebSearch,
            WebStop,
            Yen,
            Copy,
            Paste,
            Cut
        }
    }
}

impl InputDevice for Keyboard {
    fn name(&self) -> &str {
        "Keyboard"
    }

    fn button(&self, name: &str) -> Option<&dyn Button> {
        self.key_names
            .get(name)
            .and_then(|index| self.keys.get(*index))
            .map(|key| key as &dyn Button)
    }

    fn buttons(&self) -> Box<dyn Iterator<Item = &dyn Button> + '_> {
        Box::new(self.keys.iter().map(|key| key as &dyn Button))
    }

    fn axis(&self, _name: &str) -> Option<&dyn Axis> {
        None
    }

    fn axes(&self) -> Box<dyn Iterator<Item = &dyn Axis> + '_> {
        Box::new(std::iter::empty())
    }

    fn on_event(&mut self, event: &Event<()>) {
        match event {
            Event::DeviceEvent {
                event:
                    DeviceEvent::Key(KeyboardInput {
                        state,
                        virtual_keycode: Some(virtual_keycode),
                        ..
                    }),
                ..
            } => {
                self.handle_key(state, virtual_keycode);
            }
            _ => {}
        }
    }

    fn update(&mut self, frame: &mut Frame) {
        for mut key in self.keys.iter_mut().filter(|key| key.down) {
            key.down_time += frame.time().delta();
        }
    }

    fn reset(&mut self) {
        for mut key in &mut self.keys {
            key.pressed = false;
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

impl Keyboard {
    fn handle_key(&mut self, state: &ElementState, virtual_keycode: &VirtualKeyCode) {
        if let Some(mut key) = self
            .key_enums
            .get(virtual_keycode)
            .and_then(|index| self.keys.get_mut(*index))
        {
            if *state == ElementState::Pressed {
                key.down = true;
                key.down_time = 0.0;
            } else {
                key.down = false;
                key.pressed = key.down_time <= self.pressed_threshold;
            }
        }
    }
}

struct KeyboardKey {
    down_time: f32,
    pressed: bool,
    down: bool,
    name: String,
}

impl KeyboardKey {
    fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            down: false,
            pressed: false,
            down_time: 0.0,
        }
    }
}

impl Button for KeyboardKey {
    fn name(&self) -> &str {
        &self.name
    }

    fn down(&self) -> bool {
        self.down
    }

    fn up(&self) -> bool {
        !self.down
    }

    fn pressed(&self) -> bool {
        self.pressed
    }
}
