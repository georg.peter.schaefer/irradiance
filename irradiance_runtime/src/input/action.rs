use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::path::PathBuf;

use serde::{Deserialize, Serialize};

/// Input action maps.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct InputActionMaps(pub HashMap<String, InputActionMap>);

/// Input action map.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct InputActionMap(pub HashMap<String, InputAction>);

/// Input action.
#[derive(Clone, Serialize, Deserialize)]
pub enum InputAction {
    /// A button action.
    Button(ButtonAction),
    /// A value action.
    Value(ValueAction),
}

impl InputAction {
    /// Returns if an action contains a binding.
    pub fn contains_binding(&self, binding: impl AsRef<str>) -> bool {
        match self {
            InputAction::Button(button_action) => {
                button_action.bindings.contains_key(binding.as_ref())
            }
            InputAction::Value(ValueAction::Scalar(scalar_action)) => {
                scalar_action.bindings.contains_key(binding.as_ref())
            }
            InputAction::Value(ValueAction::Vector2D(vector2d_action)) => {
                vector2d_action.bindings.contains_key(binding.as_ref())
            }
        }
    }

    /// Returns a binding.
    pub fn binding(&self, binding: impl AsRef<str>) -> Option<Binding> {
        match self {
            InputAction::Button(button_action) => button_action
                .bindings
                .get(binding.as_ref())
                .map(|binding| Binding::Button(binding.clone())),
            InputAction::Value(ValueAction::Scalar(scalar_action)) => scalar_action
                .bindings
                .get(binding.as_ref())
                .and_then(|binding| {
                    if let ScalarBinding::Axis(axis) = binding {
                        Some(Binding::Axis(axis.clone()))
                    } else {
                        None
                    }
                }),
            _ => None,
        }
    }

    /// Sets a binding.
    pub fn set_binding(&mut self, name: impl Into<String>, binding: Binding) {
        match (self, binding) {
            (InputAction::Button(button_action), Binding::Button(binding)) => {
                button_action.bindings.insert(name.into(), binding);
            }
            (InputAction::Value(ValueAction::Scalar(scalar_action)), Binding::Axis(binding)) => {
                scalar_action
                    .bindings
                    .insert(name.into(), ScalarBinding::Axis(binding));
            }
            _ => {}
        }
    }

    /// Returns a sub binding.
    pub fn sub_binding(
        &self,
        binding: impl AsRef<str>,
        sub_binding: impl AsRef<str>,
    ) -> Option<Binding> {
        match self {
            InputAction::Value(ValueAction::Scalar(scalar_action)) => scalar_action
                .bindings
                .get(binding.as_ref())
                .and_then(|binding| {
                    if let ScalarBinding::Compound(binding) = binding {
                        match sub_binding.as_ref() {
                            "Min" => Some(Binding::Button(binding.min.clone())),
                            "Max" => Some(Binding::Button(binding.max.clone())),
                            _ => None,
                        }
                    } else {
                        None
                    }
                }),
            InputAction::Value(ValueAction::Vector2D(vector2d_action)) => vector2d_action
                .bindings
                .get(binding.as_ref())
                .and_then(|binding| match binding {
                    Vector2DBinding::Axis(binding) => match sub_binding.as_ref() {
                        "Horizontal" => Some(Binding::Axis(binding.horizontal.clone())),
                        "Vertical" => Some(Binding::Axis(binding.vertical.clone())),
                        _ => None,
                    },
                    Vector2DBinding::Compound(binding) => match sub_binding.as_ref() {
                        "Up" => Some(Binding::Button(binding.up.clone())),
                        "Down" => Some(Binding::Button(binding.down.clone())),
                        "Left" => Some(Binding::Button(binding.left.clone())),
                        "Right" => Some(Binding::Button(binding.right.clone())),
                        _ => None,
                    },
                }),
            _ => None,
        }
    }

    /// Sets a sub binding.
    pub fn set_sub_binding(
        &mut self,
        name: impl AsRef<str>,
        sub_name: impl AsRef<str>,
        binding: Binding,
    ) {
        match (self, binding) {
            (InputAction::Value(ValueAction::Scalar(scalar_action)), Binding::Button(binding)) => {
                if let Some(ScalarBinding::Compound(compound_binding)) =
                    scalar_action.bindings.get_mut(name.as_ref())
                {
                    match sub_name.as_ref() {
                        "Min" => compound_binding.min = binding,
                        "Max" => compound_binding.max = binding,
                        _ => {}
                    }
                }
            }
            (
                InputAction::Value(ValueAction::Vector2D(vector2d_action)),
                Binding::Axis(binding),
            ) => {
                if let Some(Vector2DBinding::Axis(axis_binding)) =
                    vector2d_action.bindings.get_mut(name.as_ref())
                {
                    match sub_name.as_ref() {
                        "Horizontal" => axis_binding.horizontal = binding,
                        "Vertical" => axis_binding.vertical = binding,
                        _ => {}
                    }
                }
            }
            (
                InputAction::Value(ValueAction::Vector2D(vector2d_action)),
                Binding::Button(binding),
            ) => {
                if let Some(Vector2DBinding::Compound(compound_binding)) =
                    vector2d_action.bindings.get_mut(name.as_ref())
                {
                    match sub_name.as_ref() {
                        "Up" => compound_binding.up = binding,
                        "Down" => compound_binding.down = binding,
                        "Left" => compound_binding.left = binding,
                        "Right" => compound_binding.right = binding,
                        _ => {}
                    }
                }
            }
            _ => {}
        }
    }
}

impl Default for InputAction {
    fn default() -> Self {
        Self::Button(ButtonAction::default())
    }
}

impl PartialEq for InputAction {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (InputAction::Button(_), InputAction::Button(_)) => true,
            (InputAction::Value(_), InputAction::Value(_)) => true,
            _ => false,
        }
    }
}

impl Eq for InputAction {}

impl Display for InputAction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            InputAction::Button(_) => write!(f, "Button"),
            InputAction::Value(_) => write!(f, "Value"),
        }
    }
}

/// A button action.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct ButtonAction {
    /// Indicates if the action triggers as long as the button is down.
    pub down: bool,
    /// Bindings for the action.
    pub bindings: HashMap<String, ButtonBinding>,
}

/// A value action.
#[derive(Clone, Serialize, Deserialize)]
pub enum ValueAction {
    /// Action producing a scalar value.
    Scalar(ScalarAction),
    /// Action producing a 2d vector.
    Vector2D(Vector2DAction),
}

impl Default for ValueAction {
    fn default() -> Self {
        Self::Vector2D(Vector2DAction::default())
    }
}

impl PartialEq for ValueAction {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (ValueAction::Scalar(_), ValueAction::Scalar(_)) => true,
            (ValueAction::Vector2D(_), ValueAction::Vector2D(_)) => true,
            _ => false,
        }
    }
}

impl Eq for ValueAction {}

impl Display for ValueAction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ValueAction::Scalar(_) => write!(f, "Scalar"),
            ValueAction::Vector2D(_) => write!(f, "2D Vector"),
        }
    }
}

/// A scalar action.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct ScalarAction {
    /// Normalize the produced value.
    pub normalize: bool,
    /// Bindings for the action.
    pub bindings: HashMap<String, ScalarBinding>,
}

/// A scalar action.
#[derive(Clone, Serialize, Deserialize)]
pub enum ScalarBinding {
    /// Axis binding.
    Axis(AxisBinding),
    /// Compound binding.
    Compound(ScalarCompoundBinding),
}

impl Default for ScalarBinding {
    fn default() -> Self {
        Self::Axis(AxisBinding::default())
    }
}

/// A scalar compound binding.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct ScalarCompoundBinding {
    /// Button binding producing the minimum value.
    pub min: ButtonBinding,
    /// Button binding producing the maximum value.
    pub max: ButtonBinding,
}

/// A 2d vector action.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct Vector2DAction {
    /// Normalize the produced value.
    pub normalize: bool,
    /// Bindings for the action.
    pub bindings: HashMap<String, Vector2DBinding>,
}

/// A 2d vector action.
#[derive(Clone, Serialize, Deserialize)]
pub enum Vector2DBinding {
    /// Axis binding.
    Axis(Vector2DAxisBinding),
    /// Compound binding.
    Compound(Vector2DCompoundBinding),
}

impl Default for Vector2DBinding {
    fn default() -> Self {
        Self::Axis(Vector2DAxisBinding::default())
    }
}

/// A 2d vector axis binding.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct Vector2DAxisBinding {
    /// Horizontal axis binding.
    pub horizontal: AxisBinding,
    /// Vertical axis binding.
    pub vertical: AxisBinding,
}

/// A 2d vector compound binding.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct Vector2DCompoundBinding {
    /// Up binding.
    pub up: ButtonBinding,
    /// Down binding.
    pub down: ButtonBinding,
    /// Left binding.
    pub left: ButtonBinding,
    /// Right binding.
    pub right: ButtonBinding,
}

/// A binding.
#[derive(Clone, Serialize, Deserialize)]
pub enum Binding {
    /// Button binding.
    Button(ButtonBinding),
    /// Axis binding.
    Axis(AxisBinding),
}

impl Binding {
    /// Returns the path of the binding.
    pub fn path(&self) -> Option<&PathBuf> {
        match self {
            Binding::Button(binding) => binding.button.as_ref(),
            Binding::Axis(binding) => binding.axis.as_ref(),
        }
    }

    /// Sets the path of a binding.
    pub fn set_path(&mut self, path: Option<PathBuf>) {
        match self {
            Binding::Button(binding) => binding.button = path,
            Binding::Axis(binding) => binding.axis = path,
        }
    }
}

/// A button binding.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct ButtonBinding {
    /// Button binding.
    pub button: Option<PathBuf>,
}

/// An axis binding.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct AxisBinding {
    /// Axis binding.
    pub axis: Option<PathBuf>,
}
