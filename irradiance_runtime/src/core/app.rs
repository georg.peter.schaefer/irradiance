use std::sync::Arc;

use winit::{event::Event, event_loop::ControlFlow};

use crate::{
    gfx::{
        command_buffer::CommandBuffer,
        image::{ImageAspects, ImageLayout, ImageSubresourceRange},
        queue::{CommandBufferSubmit, SemaphoreSubmit, Submit},
        sync::{
            AccessMask, Fence, FenceStatus, ImageMemoryBarrier, PipelineBarrier, PipelineStages,
            Semaphore,
        },
        GfxError, GraphicsContext,
    },
    window::Window,
};

use super::{Engine, Frame, Time};

/// The application.
pub struct App<E> {
    time: Time,
    rendering_finished: Arc<Semaphore>,
    image_available: Arc<Semaphore>,
    in_flight: Arc<Fence>,
    graphics_context: Arc<GraphicsContext>,
    window: Arc<Window>,
    engine: E,
}

impl<E: Engine + 'static> App<E> {
    /// Runs the application.
    pub fn run(engine: E) -> ! {
        let mut app = App::new(engine);

        app.window.take_event_loop().run(move |event, _, flow| {
            *flow = ControlFlow::Poll;

            match event {
                Event::MainEventsCleared => {
                    app.update();
                }
                Event::LoopDestroyed => {
                    app.engine.assets().wait();
                    app.graphics_context.device().wait().expect("idle device");
                }
                event => app.on_event(&event),
            }

            if app.engine.should_close() {
                *flow = ControlFlow::Exit;
            }
        });
    }

    fn new(engine: E) -> Self {
        let window = engine.window().clone();
        let graphics_context = engine.graphics_context().clone();
        let in_flight = Fence::builder(graphics_context.device().clone())
            .status(FenceStatus { signaled: true })
            .build()
            .unwrap();
        let image_available = Semaphore::builder(graphics_context.device().clone())
            .build()
            .unwrap();
        let rendering_finished = Semaphore::builder(graphics_context.device().clone())
            .build()
            .unwrap();

        Self {
            engine,
            window,
            graphics_context,
            in_flight,
            image_available,
            rendering_finished,
            time: Time::new(),
        }
    }

    fn on_event(&mut self, event: &Event<()>) {
        self.engine.on_event(event)
    }

    fn update(&mut self) {
        self.time.update();

        if let Some(mut frame) = self.begin_frame() {
            self.engine.update(&mut frame);
            self.end_frame(frame);
        } else {
            self.graphics_context.recreate_swapchain().unwrap();
        }
    }

    fn begin_frame(&self) -> Option<Frame> {
        self.in_flight.wait().unwrap();
        self.in_flight.reset().unwrap();

        let swapchain_image_index = match self
            .graphics_context
            .swapchain()
            .unwrap()
            .acquire_next_image(u64::MAX, Some(&self.image_available), None)
        {
            Ok((image_index, _)) => image_index,
            Err(GfxError::OutOfDate) => {
                return None;
            }
            Err(e) => {
                panic!("{e}")
            }
        };
        let swapchain_image = self.graphics_context.swapchain().unwrap().image_views()
            [swapchain_image_index as usize]
            .clone();

        let mut builder = CommandBuffer::primary(
            self.graphics_context.device().clone(),
            self.graphics_context.command_pool().unwrap(),
        );

        builder.pipeline_barrier(
            PipelineBarrier::builder()
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(swapchain_image.image().clone())
                        .src_stage_mask(PipelineStages {
                            top_of_pipe: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask::none())
                        .dst_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::Undefined)
                        .new_layout(ImageLayout::ColorAttachmentOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .build(),
        );

        Some(Frame::new(
            self.time,
            builder,
            swapchain_image_index,
            swapchain_image,
        ))
    }

    fn end_frame(&self, mut frame: Frame) {
        let swapchain_image = frame.swapchain_image();
        let command_buffer = frame
            .builder_mut()
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(swapchain_image.image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                bottom_of_pipe: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask::none())
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::PresentSrc)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .build()
            .unwrap();

        self.graphics_context
            .graphics_queue()
            .submit(
                Submit::builder()
                    .wait_semaphore(
                        SemaphoreSubmit::builder(self.image_available.clone())
                            .stages(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .build(),
                    )
                    .command_buffer(CommandBufferSubmit::builder(command_buffer).build())
                    .signal_semaphore(
                        SemaphoreSubmit::builder(self.rendering_finished.clone()).build(),
                    )
                    .build(),
                Some(self.in_flight.clone()),
            )
            .unwrap();

        match self.graphics_context.present_queue().unwrap().present(
            vec![&self.rendering_finished],
            self.graphics_context.swapchain().unwrap(),
            frame.swapchain_image_index,
        ) {
            Err(GfxError::OutOfDate) => {
                self.graphics_context.recreate_swapchain().unwrap();
            }
            result => result.unwrap(),
        }
    }
}
