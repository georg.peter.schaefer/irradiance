use std::{sync::Arc, time::Instant};

use crate::gfx::{command_buffer::CommandBufferBuilder, image::ImageView};

/// The current frame.
#[derive(Debug)]
pub struct Frame {
    time: Time,
    builder: CommandBufferBuilder,
    pub(crate) swapchain_image_index: u32,
    swapchain_image: Arc<ImageView>,
}

impl Frame {
    /// Creates a new frame.
    pub fn new(
        time: Time,
        builder: CommandBufferBuilder,
        swapchain_image_index: u32,
        swapchain_image: Arc<ImageView>,
    ) -> Self {
        Self {
            time,
            builder,
            swapchain_image_index,
            swapchain_image,
        }
    }

    /// Returns the current frame time.
    pub fn time(&self) -> Time {
        self.time
    }

    /// Returns the command buffer builder for the current frame.
    pub fn builder_mut(&mut self) -> &mut CommandBufferBuilder {
        &mut self.builder
    }

    /// Returns the swapchain image of the current frame.
    pub fn swapchain_image(&self) -> Arc<ImageView> {
        self.swapchain_image.clone()
    }

    /// Returns the swapchain image.
    pub fn set_swapchain_image(&mut self, swapchain_image: Arc<ImageView>) {
        self.swapchain_image = swapchain_image;
    }
}

/// Current frame time.
#[derive(Copy, Clone, Debug)]
pub struct Time {
    earlier: Instant,
    elapsed: f32,
    delta: f32,
}

impl Time {
    /// Creates a new frame time.
    pub fn new() -> Self {
        Self {
            earlier: Instant::now(),
            elapsed: 0.0,
            delta: 0.0,
        }
    }

    /// Returns the elapsed time since the appliocation has been started in seconds.
    pub fn elapsed(&self) -> f32 {
        self.elapsed
    }

    /// Returns the time delta since the last frame in seconds.
    pub fn delta(&self) -> f32 {
        self.delta
    }

    pub(crate) fn update(&mut self) {
        let now = Instant::now();
        self.delta = now.duration_since(self.earlier).as_secs_f32();
        self.elapsed += self.delta;
        self.earlier = now;
    }
}
