use std::sync::Arc;

use winit::event::Event;

use crate::animation::Animations;
use crate::audio::Audio;
use crate::core::Config;
use crate::physics::Physics;
use crate::{
    asset::Assets, ecs::ComponentTypes, gfx::GraphicsContext, input::Input,
    rendering::pbr::BufPbrPipeline, scene::Scenes, window::Window,
};

use super::{Frame, Screens};

/// Common engine trait.
pub trait Engine {
    /// Returns the window.
    fn window(&self) -> &Arc<Window>;
    /// Returns the input.
    fn input(&self) -> &Input;
    /// Returns a mutable reference to the input.
    fn input_mut(&mut self) -> &mut Input;
    /// Returns the component type registry.
    fn component_types(&self) -> &ComponentTypes;
    /// Returns the graphics context.
    fn graphics_context(&self) -> &Arc<GraphicsContext>;
    /// Returns the assets.
    fn assets(&self) -> &Arc<Assets>;
    /// Returns the physically based rendering pipeline.
    fn buf_pbr_pipeline(&self) -> &Arc<BufPbrPipeline>;
    /// Returns the animation system.
    fn animations(&self) -> &Animations;
    /// Returns the audio system.
    fn audio(&self) -> &Audio;
    /// Returns the audio system.
    fn audio_mut(&mut self) -> &mut Audio;
    /// Returns the screens.
    fn screens(&self) -> &Screens;
    /// Returns the scenes.
    fn scenes(&self) -> &Scenes;
    /// Returns the physics system.
    fn physics(&self) -> &Physics;
    /// Returns the physics system.
    fn physics_mut(&mut self) -> &mut Physics;
    /// Returns if the application should close.
    fn should_close(&self) -> bool;
    /// Closes the application.
    fn close(&mut self);
    /// Handles events.
    fn on_event(&mut self, event: &Event<()>);
    /// Calculates the current frame.
    fn update(&mut self, frame: &mut Frame);
    /// Returns the config.
    fn config(&self) -> &Config;
    /// Returns a mutable reference to the config.
    fn config_mut(&mut self) -> &mut Config;
}
