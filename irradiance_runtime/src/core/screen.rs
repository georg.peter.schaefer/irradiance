//! Types and traits for game state [`Screen`] handling.

use std::{
    any::Any,
    cell::{Ref, RefCell, RefMut},
    collections::HashMap,
    ops::Deref,
    sync::Arc,
};

use log::{debug, warn};

use crate::{core::Engine, ecs::Entities};

use super::Frame;

/// The mode of a screen.
#[derive(Debug, Eq, PartialEq)]
pub enum Mode {
    /// Covers all screens under this screen.
    ///
    /// Lower screens are not updated and not drawn.
    Opaque,

    /// Overlays all screens under this screen.
    ///
    /// Lower screens are not updated but drawn.
    Modal,

    /// Overlays all screens under this screen.
    ///
    /// Lower screens are updated and drawn.
    Overlay,
}

/// Describes a screen.
/// TODO: Error handling.
pub trait Screen {
    /// Returns the name of the screen.
    fn name(&self) -> &'static str;
    /// Returns the mode of the screen.
    fn mode(&self) -> Mode;
    /// Returns the entities.
    fn entities(&self) -> Option<Arc<Entities>> {
        None
    }
    /// Handles on entering the screen.
    fn on_enter(&mut self, engine: &mut dyn Engine);
    /// Updates the screen.
    fn update(&mut self, engine: &mut dyn Engine, frame: &mut Frame);
    /// Draws the screen.
    fn draw(&mut self, engine: &mut dyn Engine, frame: &mut Frame) {
        let swapchain_image = frame.swapchain_image();
        engine
            .buf_pbr_pipeline()
            .draw(frame.builder_mut(), swapchain_image, &Entities::new())
            .expect("valid frame");
    }
    /// Handles on leaving the screen.
    fn on_leave(&mut self, engine: &mut dyn Engine);
    /// Returns the screen as any.
    fn as_any(&self) -> &dyn Any;
    /// Returns the screen as any mut.
    fn as_any_mut(&mut self) -> &mut dyn Any;
}

/// Extension trait for screens containing non object safe functions.
pub trait ScreenExt: Screen {
    /// Creates a new screen.
    fn new(engine: &dyn Engine) -> Self;
}

/// Stack based state machine for screens.
#[derive(Default)]
pub struct Screens {
    screens: HashMap<&'static str, Box<RefCell<dyn Screen>>>,
    queue: RefCell<Vec<StackOp>>,
    stack: RefCell<Vec<&'static str>>,
}

impl Screens {
    /// Starts building screens.
    pub fn builder() -> ScreensBuilder {
        ScreensBuilder {
            screens: vec![],
            first: None,
        }
    }

    pub(crate) fn new(
        screens: Vec<(&'static str, Box<RefCell<dyn Screen>>)>,
        first: Option<&'static str>,
    ) -> Self {
        let screens = Self {
            screens: screens.into_iter().collect(),
            queue: Default::default(),
            stack: Default::default(),
        };
        if let Some(first) = first {
            screens.push(first);
        }
        screens
    }

    /// Returns the screen names.
    pub fn names(&self) -> Vec<&'static str> {
        self.screens
            .keys()
            .filter(|screen| !screen.starts_with("__internal"))
            .cloned()
            .collect()
    }

    /// Returns a screen.
    pub fn get<S: Screen + 'static>(&self, screen: &'static str) -> Option<Ref<'_, S>> {
        self.screens
            .get(screen)
            .map(|screen| screen.deref().borrow())
            .map(|screen| Ref::filter_map(screen, |screen| screen.as_any().downcast_ref::<S>()))
            .and_then(|screen| screen.ok())
    }

    /// Returns a screen as mutable reference.
    pub fn get_mut<S: Screen + 'static>(&self, screen: &'static str) -> Option<RefMut<'_, S>> {
        self.screens
            .get(screen)
            .map(|screen| screen.deref().borrow_mut())
            .map(|screen| {
                RefMut::filter_map(screen, |screen| screen.as_any_mut().downcast_mut::<S>())
            })
            .and_then(|screen| screen.ok())
    }

    /// Updates the current frame.
    pub fn update(&self, engine: &mut dyn Engine, frame: &mut Frame) {
        for screen in self.screens() {
            screen.borrow_mut().update(engine, frame);
        }
    }

    /// Draws the current visible frame.
    pub fn draw(&self, engine: &mut dyn Engine, frame: &mut Frame) {
        for screen in self.visible_screens() {
            screen.borrow_mut().draw(engine, frame);
        }
    }

    /// Pushes a new screen onto the stack.
    pub fn push(&self, screen: &'static str) {
        if self.screens.contains_key(screen) {
            self.queue.borrow_mut().push(StackOp::Push(screen));
        } else {
            warn!("Trying to push unknown screen '{screen}'");
        }
    }

    /// Pops a screen from the stack.
    pub fn pop(&self) {
        self.queue.borrow_mut().push(StackOp::Pop);
    }

    /// Pops all screens from the stack.
    pub fn pop_all(&self) {
        self.queue.borrow_mut().push(StackOp::PopAll);
    }

    /// Commits queued screen operations.
    pub fn commit(&self, engine: &mut dyn Engine) {
        for op in self.queue.borrow_mut().drain(0..) {
            match op {
                StackOp::Push(screen) => {
                    self._push(screen, engine);
                }
                StackOp::Pop => {
                    self._pop(engine);
                }
                StackOp::PopAll => {
                    self._pop_all(engine);
                }
            }
        }
        self.queue.borrow_mut().clear();
    }

    fn _push(&self, screen: &'static str, engine: &mut dyn Engine) {
        if self.screens[screen].deref().borrow().mode() == Mode::Opaque {
            for screen in self.screens().rev() {
                debug!("Leaving screen '{}'", screen.deref().borrow().name());
                screen.borrow_mut().on_leave(engine);
            }
        }

        self.stack.borrow_mut().push(screen);

        debug!("Entering screen '{}'", screen);
        self.screens[screen].borrow_mut().on_enter(engine);
    }

    fn _pop(&self, engine: &mut dyn Engine) {
        let mut occluded = false;
        let frame_length = {
            let frame = self.screens();

            for screen in frame.rev() {
                debug!("Leaving screen '{}'", screen.deref().borrow().name());
                screen.borrow_mut().on_leave(engine);
                if screen.borrow().mode() == Mode::Opaque {
                    occluded = true;
                }
            }

            self.screens().len()
        };
        let stack_length = self.stack.borrow().len();

        self.stack
            .borrow_mut()
            .truncate(stack_length - frame_length);

        for screen in self.screens() {
            if occluded {
                debug!("Entering screen '{}'", screen.deref().borrow().name());
                screen.borrow_mut().on_enter(engine);
            }
        }
    }

    fn _pop_all(&self, engine: &mut dyn Engine) {
        while !self.stack.borrow().is_empty() {
            self._pop(engine);
        }
    }

    #[allow(clippy::unnecessary_to_owned)]
    fn screens(
        &self,
    ) -> impl Iterator<Item = &Box<RefCell<dyn Screen>>> + ExactSizeIterator + DoubleEndedIterator + '_
    {
        let occluded = self
            .stack
            .borrow()
            .iter()
            .rposition(|screen| {
                matches!(
                    self.screens[screen].deref().borrow().mode(),
                    Mode::Opaque | Mode::Modal
                )
            })
            .unwrap_or(0);
        self.stack.borrow()[occluded..]
            .to_owned()
            .into_iter()
            .map(|screen| &self.screens[screen])
    }

    #[allow(clippy::unnecessary_to_owned)]
    fn visible_screens(
        &self,
    ) -> impl Iterator<Item = &Box<RefCell<dyn Screen>>> + ExactSizeIterator + DoubleEndedIterator + '_
    {
        let occluded = self
            .stack
            .borrow()
            .iter()
            .rposition(|screen| self.screens[screen].deref().borrow().mode() == Mode::Opaque)
            .unwrap_or(0);
        self.stack.borrow()[occluded..]
            .to_owned()
            .into_iter()
            .map(|screen| &self.screens[screen])
    }
}

#[derive(Clone)]
enum StackOp {
    Push(&'static str),
    Pop,
    PopAll,
}

type ScreenFactory = dyn FnOnce(&dyn Engine) -> Box<RefCell<dyn Screen>>;

/// Type for building screens.
pub struct ScreensBuilder {
    first: Option<&'static str>,
    screens: Vec<Box<ScreenFactory>>,
}

impl ScreensBuilder {
    /// Adds a screen.
    pub fn screen<S: ScreenExt + 'static>(mut self) -> Self {
        self.screens
            .push(Box::new(|engine| Box::new(RefCell::new(S::new(engine)))));
        self
    }

    /// Sets the first screen.
    pub fn first(mut self, screen: &'static str) -> Self {
        self.first = Some(screen);
        self
    }

    /// Builds the screens.
    pub fn build(self, engine: &dyn Engine) -> Screens {
        Screens::new(
            self.screens
                .into_iter()
                .map(|screen| {
                    let screen = screen(engine);
                    let name = screen.borrow().name();
                    (name, screen)
                })
                .collect(),
            self.first,
        )
    }
}
