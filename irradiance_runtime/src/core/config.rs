/// Engine runtime configuration.
#[derive(Copy, Clone, Default)]
pub struct Config {
    /// Physics debug render.
    pub physics_debug: bool,
}
