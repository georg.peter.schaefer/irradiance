use std::{path::PathBuf, rc::Rc, sync::Arc};

use chrono::Local;
use fern::{colors::ColoredLevelConfig, Dispatch};
use log::LevelFilter;
use winit::event::{Event, WindowEvent};

use crate::animation::Animations;
use crate::audio::Audio;
use crate::core::Config;
use crate::gfx::physical_device::PhysicalDeviceFeatures;
use crate::gfx::Extent2D;
use crate::physics::Physics;
use crate::{
    asset::{Assets, FilesystemSource},
    ecs::{ComponentExt, ComponentTypes, ComponentTypesBuilder},
    gfx::{instance::InstanceExtensions, physical_device::DeviceExtensions, GraphicsContext},
    input::Input,
    rendering::pbr::BufPbrPipeline,
    scene::Scenes,
    window::Window,
};

use super::{screen::ScreensBuilder, Engine, Frame, ScreenExt, Screens};

/// The engine.
pub struct Standalone {
    config: Config,
    should_close: bool,
    physics: Physics,
    scenes: Scenes,
    screens: Rc<Screens>,
    audio: Audio,
    animations: Animations,
    buf_pbr_pipeline: Arc<BufPbrPipeline>,
    assets: Arc<Assets>,
    graphics_context: Arc<GraphicsContext>,
    component_types: ComponentTypes,
    input: Input,
    window: Arc<Window>,
}

impl Standalone {
    /// Starts building a new standlone engine.
    pub fn builder() -> StandaloneBuilder {
        StandaloneBuilder {
            title: "irradiance".to_owned(),
            assets: None,
            component_types: ComponentTypes::builder(),
            screens: Screens::builder(),
        }
    }
}

impl Engine for Standalone {
    fn window(&self) -> &Arc<Window> {
        &self.window
    }

    fn input(&self) -> &Input {
        &self.input
    }

    fn input_mut(&mut self) -> &mut Input {
        &mut self.input
    }

    fn component_types(&self) -> &ComponentTypes {
        &self.component_types
    }

    fn graphics_context(&self) -> &Arc<GraphicsContext> {
        &self.graphics_context
    }

    fn assets(&self) -> &Arc<Assets> {
        &self.assets
    }

    fn buf_pbr_pipeline(&self) -> &Arc<BufPbrPipeline> {
        &self.buf_pbr_pipeline
    }

    fn animations(&self) -> &Animations {
        &self.animations
    }

    fn audio(&self) -> &Audio {
        &self.audio
    }

    fn audio_mut(&mut self) -> &mut Audio {
        &mut self.audio
    }

    fn screens(&self) -> &Screens {
        &self.screens
    }

    fn scenes(&self) -> &Scenes {
        &self.scenes
    }

    fn physics(&self) -> &Physics {
        &self.physics
    }

    fn physics_mut(&mut self) -> &mut Physics {
        &mut self.physics
    }

    fn should_close(&self) -> bool {
        self.should_close
    }

    fn close(&mut self) {
        self.should_close = true;
    }

    fn on_event(&mut self, event: &Event<()>) {
        if let Event::WindowEvent {
            event: WindowEvent::CloseRequested,
            ..
        } = event
        {
            self.should_close = true;
        }

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                self.should_close = true;
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(size),
                ..
            } => {
                let extent = Extent2D {
                    width: size.width,
                    height: size.height,
                };
                self.buf_pbr_pipeline = self.buf_pbr_pipeline.clone().resize(extent);
            }
            _ => {}
        }

        self.input.on_event(event);
    }

    fn update(&mut self, frame: &mut Frame) {
        let screens = self.screens.clone();

        self.input.update(frame);
        screens.commit(self);
        screens.update(self, frame);
        screens.draw(self, frame);

        self.input.reset();
    }

    fn config(&self) -> &Config {
        &self.config
    }

    fn config_mut(&mut self) -> &mut Config {
        &mut self.config
    }
}

/// Type for building a standalone engine.
pub struct StandaloneBuilder {
    screens: ScreensBuilder,
    component_types: ComponentTypesBuilder,
    assets: Option<PathBuf>,
    title: String,
}

impl StandaloneBuilder {
    /// Sets the title.
    pub fn title(mut self, title: impl Into<String>) -> Self {
        self.title = title.into();
        self
    }

    /// Sets the assets path.
    pub fn assets(mut self, path: impl Into<PathBuf>) -> Self {
        self.assets = Some(path.into());
        self
    }

    /// Registers a component type.
    pub fn component<C: ComponentExt>(mut self) -> Self {
        self.component_types = self.component_types.component::<C>();
        self
    }

    /// Adds a screen.
    pub fn screen<S: ScreenExt + 'static>(mut self) -> Self {
        self.screens = self.screens.screen::<S>();
        self
    }

    /// Builds the engine.
    pub fn build(self, first_screen: &'static str) -> Standalone {
        Self::init_log();

        let window = Window::builder()
            .title(&self.title)
            .fullscreen()
            .build()
            .expect("window");
        let input = Input::new(self.assets.clone().expect("asset path"), window.clone());
        let component_types = self.component_types.build();
        let graphics_context = Self::create_graphics_context(window.clone());
        let assets = Assets::builder()
            .source(
                FilesystemSource::new(self.assets.clone().expect("assets path"))
                    .expect("filesystem source"),
            )
            .build();
        let buf_pbr_pipeline =
            BufPbrPipeline::from_window(&window, graphics_context.clone(), assets.clone());
        let screens = Default::default();
        let scenes = Default::default();
        let physics = Physics::new(graphics_context.clone(), assets.clone());

        let mut standalone = Standalone {
            config: Default::default(),
            window,
            input,
            component_types,
            graphics_context,
            assets,
            buf_pbr_pipeline,
            animations: Animations::default(),
            audio: Audio::new(),
            screens,
            scenes,
            physics,
            should_close: false,
        };

        standalone.screens = Rc::new(self.screens.first(first_screen).build(&standalone));
        standalone.scenes = Scenes::new(self.assets.expect("assets path"), &standalone);

        standalone
    }

    fn create_graphics_context(window: Arc<Window>) -> Arc<GraphicsContext> {
        #[cfg(target_os = "linux")]
        let instance_extensions = InstanceExtensions {
            khr_surface: true,
            khr_xlib_surface: true,
            ..Default::default()
        };
        #[cfg(target_os = "windows")]
        let instance_extensions = InstanceExtensions {
            khr_surface: true,
            khr_win32_surface: true,
            ..Default::default()
        };

        GraphicsContext::with_swapchain(window)
            .instance_extensions(instance_extensions)
            .device_extensions(DeviceExtensions {
                khr_swapchain: true,
                khr_maintenance1: true,
                ..Default::default()
            })
            .physical_device_features(PhysicalDeviceFeatures {
                depth_clamp: true,
                shader_image_gather_extended: true,
                wide_lines: true,
                sampler_anisotropy: true,
                ..Default::default()
            })
            .build()
            .unwrap()
    }

    fn init_log() {
        let colors = ColoredLevelConfig::new();
        Dispatch::new()
            .level(LevelFilter::Debug)
            .chain(
                Dispatch::new()
                    .format(move |out, message, record| {
                        out.finish(format_args!(
                            "{}[{}][{}] {}",
                            Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                            record.target(),
                            colors.color(record.level()),
                            message
                        ))
                    })
                    .chain(std::io::stdout()),
            )
            .apply()
            .expect("initialized log");
    }
}
