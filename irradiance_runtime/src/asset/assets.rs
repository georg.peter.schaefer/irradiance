use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};

use log::debug;

use crate::asset::private::{Asset, AssetRef, Handle, HandleAbstract};
use crate::asset::source::Source;
use crate::gfx::GraphicsContext;

type Cache = HashMap<PathBuf, Arc<dyn HandleAbstract>>;

/// Type for managing [`Asset`](super::Asset)s.
#[derive(Debug)]
pub struct Assets {
    cache: Arc<Mutex<Cache>>,
    sources: Vec<Arc<dyn Source>>,
}

impl Assets {
    /// Starts building a new [asset manager](Assets).
    pub fn builder() -> AssetsBuilder {
        AssetsBuilder { sources: vec![] }
    }

    /// Reads an [`Asset`](super::Asset).
    ///
    /// The [`Asset`](super::Asset) is read from the first [`Source`](super::Source) that contains
    /// it.
    ///
    /// # Errors
    /// This function will return an [`std::io::Error`] if `path` does not point to an asset.
    pub fn read<A, P>(
        self: &Arc<Self>,
        graphics_context: Arc<GraphicsContext>,
        path: P,
    ) -> Result<AssetRef<A>, std::io::Error>
    where
        A: Asset,
        P: AsRef<Path>,
    {
        self.do_read(graphics_context, path, A::try_from_bytes)
    }

    fn do_read<A, P, F>(
        self: &Arc<Self>,
        graphics_context: Arc<GraphicsContext>,
        path: P,
        f: F,
    ) -> Result<AssetRef<A>, std::io::Error>
    where
        A: Asset,
        P: AsRef<Path>,
        F: FnOnce(Arc<Assets>, Arc<GraphicsContext>, &Path, Vec<u8>) -> Result<A, A::Error>
            + Send
            + 'static,
    {
        let path = path.as_ref().to_path_buf();
        let mut cache = self.cache.lock().unwrap();
        cache
            .get(&path)
            .map(|handle| Result::<Arc<dyn HandleAbstract>, std::io::Error>::Ok(handle.clone()))
            .or_else(|| Some(self.new_entry(graphics_context, &mut cache, &path, f)))
            .map(Self::as_asset_ref)
            .unwrap()
    }

    fn new_entry<A, F>(
        self: &Arc<Self>,
        graphics_context: Arc<GraphicsContext>,
        cache: &mut Cache,
        path: &PathBuf,
        f: F,
    ) -> Result<Arc<dyn HandleAbstract>, std::io::Error>
    where
        A: Asset,
        F: FnOnce(Arc<Assets>, Arc<GraphicsContext>, &Path, Vec<u8>) -> Result<A, A::Error>
            + Send
            + 'static,
    {
        let source = self.source(path)?;
        let handle = Handle::<A>::new(path.clone());
        let result = Ok(handle.clone() as Arc<dyn HandleAbstract>);
        cache.insert(path.clone(), handle.clone());

        let assets = self.clone();
        let path = path.clone();
        rayon::spawn(move || {
            log::debug!("Start loading asset {:?}", path);
            let result = f(assets, graphics_context, &path, source.read(&path).unwrap());
            unsafe {
                handle.init(result);
            }
            log::debug!("Finished loading asset {:?}", path)
        });

        result
    }

    fn as_asset_ref<A>(
        result: Result<Arc<dyn HandleAbstract>, std::io::Error>,
    ) -> Result<AssetRef<A>, std::io::Error>
    where
        A: Asset,
    {
        let handle = result?;
        if let Ok(handle) = handle.clone().as_any().downcast::<Handle<A>>() {
            Ok(AssetRef::new(handle))
        } else {
            Err(std::io::Error::new(
                std::io::ErrorKind::NotFound,
                format!("{:?}", handle.path()),
            ))
        }
    }

    /// Writes all in memory changes to assets to the apropriate source.
    pub fn write_all(&self) -> Result<(), std::io::Error> {
        let cache = self.cache.lock().unwrap();
        for (path, handle) in cache.iter() {
            let source = self.source(path)?;
            if let Some(bytes) = unsafe { handle.to_bytes() } {
                debug!("Saving asset {path:?}");
                source.write(path, bytes)?;
            }
        }
        Ok(())
    }

    /// Removes an asset from the cache.
    pub fn remove(&self, path: impl AsRef<Path>) {
        let mut cache = self.cache.lock().unwrap();
        if let Some(handle) = cache.remove(path.as_ref()) {
            unsafe {
                handle.set_none();
            }
        }
    }

    /// Returns if the path points to an asset.
    pub fn contains(&self, path: impl AsRef<Path>) -> bool {
        self.sources
            .iter()
            .any(|source| source.contains(path.as_ref()))
    }

    /// Renames an asset.
    pub fn rename(&self, from: impl AsRef<Path>, to: impl AsRef<Path>) {
        let mut cache = self.cache.lock().unwrap();
        if let Some(handle) = cache.remove(from.as_ref()) {
            let to = to.as_ref().to_path_buf();
            handle.set_path(to.clone());
            cache.insert(to, handle);
            unsafe {
                cache.values().for_each(|handle| handle.mark_unsaved());
            }
        }
    }

    /// Waits for all cached handles.
    pub fn wait(&self) {
        let cache = self.cache.lock().unwrap();
        for (_, handle) in cache.iter() {
            handle.wait();
        }
    }

    fn source(&self, path: &PathBuf) -> Result<Arc<dyn Source>, std::io::Error> {
        self.sources
            .iter()
            .find(|source| source.contains(path))
            .cloned()
            .ok_or_else(|| std::io::Error::new(std::io::ErrorKind::NotFound, format!("{:?}", path)))
    }
}

/// Type for building an [asset manager](Assets).
pub struct AssetsBuilder {
    sources: Vec<Arc<dyn Source>>,
}

impl AssetsBuilder {
    /// Adds a [`Source`](super::Source).
    pub fn source(mut self, source: Arc<impl Source>) -> AssetsBuilder {
        self.sources.push(source);
        self
    }

    /// Builds the [asset manager](Assets).
    pub fn build(self) -> Arc<Assets> {
        Arc::new(Assets {
            cache: Arc::new(Mutex::new(Default::default())),
            sources: self.sources,
        })
    }
}

// #[cfg(test)]
// mod tests {
//     use std::path::PathBuf;
//     use std::sync::Arc;

//     use crate::asset::assets::Assets;
//     use crate::asset::source::FilesystemSource;
//     use crate::asset::test::{OtherAsset, TestAsset};

//     fn create_assets() -> Arc<Assets> {
//         Assets::new()
//             .source(FilesystemSource::new("rsc/test/asset").unwrap())
//             .build()
//     }

//     #[test]
//     fn read_not_found() {
//         let assets = create_assets();
//         let result = assets.read::<TestAsset, _>("not.found").err();

//         assert!(result.is_some());
//         assert_eq!(std::io::ErrorKind::NotFound, result.unwrap().kind());
//     }

//     #[test]
//     fn read_different_type() {
//         let assets = create_assets();
//         let result = assets.read::<TestAsset, _>("test").ok();

//         assert!(result.is_some());
//         result.unwrap().unwrap();

//         let result = assets.read::<OtherAsset, _>("test").err();
//         assert!(result.is_some());
//         assert_eq!(std::io::ErrorKind::NotFound, result.unwrap().kind());
//     }

//     #[test]
//     fn read() {
//         let assets = create_assets();
//         let result = assets.read::<TestAsset, _>("test").ok();

//         assert!(result.is_some());
//         let result = result.unwrap();
//         assert_eq!(Some(PathBuf::from("test")), result.path());
//         assert_eq!("test", result.unwrap().value);
//     }

//     #[test]
//     fn read_cached() {
//         let assets = create_assets();
//         let result = assets.read::<TestAsset, _>("test").ok();

//         assert!(result.is_some());
//         result.unwrap().unwrap();

//         let result = assets.read::<TestAsset, _>("test").ok();
//         assert!(result.is_some());
//         assert!(result.unwrap().ok().is_some())
//     }
// }
