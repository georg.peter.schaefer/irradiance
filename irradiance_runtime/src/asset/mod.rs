//! Asset management.
//!
//! [`Assets`] is a sort-of-collection of assets. Providing non-blocking operations for reading
//! assets.
//!
//! # Implementing an `Asset`
//! An asset type needs to implement [`Asset`]. The trait requires a fallible conversion function
//! from a bytes vector.
//!
//! ```
//! use std::path::Path;
//! use std::sync::Arc;
//! use irradiance_runtime::asset::{Asset, Assets, FilesystemSource};
//! use irradiance_runtime::gfx::GraphicsContext;
//! #[derive(Debug)]
//! struct Text {
//!     value: String
//! }
//!
//! impl Asset for Text {
//!     type Error = std::io::Error;
//!
//!     fn try_from_bytes(
//!         assets: Arc<Assets>,
//!         graphics_context: Arc<GraphicsContext>,
//!         path: &Path,
//!         bytes: Vec<u8>
//!     ) -> Result<Self, Self::Error> {
//!         Ok(Self {
//!             value: String::from_utf8(bytes)
//!                 .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))?
//!         })
//!     }
//! }
//! ```
//!
//! # Read and access
//! Reading an asset returns a smart reference to the asset. This [smart reference](AssetRef)
//! provides access to the maybe ready asset over [`std::option::Option`] or direct blocking access.
//! ```
//! # use std::path::Path;
//! # use std::sync::Arc;
//! # use irradiance_runtime::asset::{Asset, Assets, FilesystemSource};
//! # use irradiance_runtime::gfx::GraphicsContext;
//! # #[derive(Debug)]
//! # struct Text {
//! #     value: String
//! # }
//! #
//! # impl Asset for Text {
//! #     type Error = std::io::Error;
//! #
//! #     fn try_from_bytes(
//! #        assets: Arc<Assets>,
//! #        graphics_context: Arc<GraphicsContext>,
//! #        path: &Path,
//! #        bytes: Vec<u8>
//! #     ) -> Result<Self, Self::Error> {
//! #         Ok(Self {
//! #             value: String::from_utf8(bytes)
//! #                 .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))?
//! #         })
//! #     }
//! # }
//! #
//! let assets = Assets::new()
//!     .source(FilesystemSource::new("rsc/test/asset").unwrap())
//!     .build();
//! let text = assets.read::<Text, _>("test").unwrap();
//!
//! // Access the asset with an Option
//! if let Some(text) = text.ok() {
//!     // Access asset
//! }
//!
//! // Access the error
//! if let Some(e) = text.err() {
//!     // Access error
//! }
//!
//! // Blocking access
//! text.unwrap();
//! // The same for error
//! // text.unwrap_err();
//! ```
//!
//! # Sources
//! An [asset manager](Assets) can read assets from different sources. A type from which assets can
//! be read needs to implement [`Source`].
//!
//! A source for reading assets from the os filesystem is already implemented.
//!
//! ```
//! use irradiance_runtime::asset::{Assets, FilesystemSource};
//!
//! Assets::new()
//!     .source(FilesystemSource::new("rsc/test/asset").unwrap())
//!     .build();
//! ```

pub use assets::Assets;
pub use assets::AssetsBuilder;
pub use private::Asset;
pub use private::AssetRef;
pub use source::FilesystemSource;
pub use source::Source;

mod assets;
mod private;
mod source;

#[cfg(test)]
pub mod test {
    use std::path::Path;
    use std::sync::Arc;

    use crate::asset::assets::Assets;
    use crate::asset::private::Asset;
    use crate::gfx::GraphicsContext;

    #[derive(Debug)]
    pub struct TestAsset {
        pub value: String,
    }

    impl Asset for TestAsset {
        type Error = std::io::Error;

        fn try_from_bytes(
            _assets: Arc<Assets>,
            _graphics_context: Arc<GraphicsContext>,
            _path: &Path,
            bytes: Vec<u8>,
        ) -> Result<Self, Self::Error> {
            Ok(Self {
                value: String::from_utf8(bytes)
                    .map_err(|e| Self::Error::new(std::io::ErrorKind::InvalidData, e))?,
            })
        }
    }

    #[derive(Debug)]
    pub struct OtherAsset;

    impl Asset for OtherAsset {
        type Error = std::io::Error;

        fn try_from_bytes(
            _assets: Arc<Assets>,
            _graphics_context: Arc<GraphicsContext>,
            _path: &Path,
            _bytes: Vec<u8>,
        ) -> Result<Self, Self::Error> {
            Ok(Self {})
        }
    }
}
