use std::fmt::Debug;
use std::path::{Path, PathBuf};
use std::sync::Arc;

/// A source that assets can be read from.
///
/// Types implementing `Source` can be used by [`Assets`](super::Assets) to read assets.
pub trait Source: Debug + Send + Sync + 'static {
    /// Returns `true` if the `Source` contains the asset on the give `path`.
    fn contains(&self, path: &Path) -> bool;

    /// Read an asset into a bytes vector.
    ///
    /// # Errors
    /// This function will return an error if `path` does not point to an asset contained in this
    /// `Source`.
    fn read(&self, path: &Path) -> Result<Vec<u8>, std::io::Error>;

    /// Writes an asset.
    ///
    /// # Errors
    /// This functions reteurns an error if something goes wrong when writing the asset.
    fn write(&self, path: &Path, contents: Vec<u8>) -> Result<(), std::io::Error>;
}

/// A source that can read assets from the os filesystem.
#[derive(Debug)]
pub struct FilesystemSource {
    root: PathBuf,
}

impl FilesystemSource {
    /// Constructs a new `FilesystemSource` that can read assets from the filesystem at `root`.
    pub fn new(root: impl AsRef<Path>) -> Result<Arc<Self>, std::io::Error> {
        Ok(Arc::new(Self {
            root: root.as_ref().canonicalize()?,
        }))
    }

    fn absolute(&self, path: &Path) -> PathBuf {
        self.root.clone().join(path)
    }
}

impl Source for FilesystemSource {
    fn contains(&self, path: &Path) -> bool {
        self.absolute(path).is_file()
    }

    fn read(&self, path: &Path) -> Result<Vec<u8>, std::io::Error> {
        std::fs::read(self.absolute(path))
    }

    fn write(&self, path: &Path, contents: Vec<u8>) -> Result<(), std::io::Error> {
        std::fs::write(self.absolute(path), contents)
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use crate::asset::source::{FilesystemSource, Source};

    #[test]
    fn base_not_found() {
        let result = FilesystemSource::new("does.not.exist").err().unwrap();

        assert_eq!(std::io::ErrorKind::NotFound, result.kind());
    }

    #[test]
    fn does_not_contain() {
        let source = FilesystemSource::new("rsc/test/asset").unwrap();

        assert!(!source.contains(Path::new("does.not.exists")));
    }

    #[test]
    fn contains() {
        let source = FilesystemSource::new("rsc/test/asset").unwrap();

        assert!(source.contains(Path::new("test")));
    }

    #[test]
    fn read_does_not_exist() {
        let source = FilesystemSource::new("rsc/test/asset").unwrap();
        let result = source.read(Path::new("does.not.exist")).err().unwrap();

        assert_eq!(std::io::ErrorKind::NotFound, result.kind());
    }

    #[test]
    fn read() {
        let source = FilesystemSource::new("rsc/test/asset").unwrap();
        let result = String::from_utf8(source.read(Path::new("test")).ok().unwrap()).unwrap();

        assert_eq!("test", result);
    }
}
