use std::any::Any;
use std::cell::{Cell, UnsafeCell};
use std::fmt::{Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::path::{Path, PathBuf};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use serde::{Serialize, Serializer};

use crate::asset::assets::Assets;
use crate::gfx::GraphicsContext;

/// An asset type.
///
/// Types implementing `Asset` are able to be managed with an instance of [`Assets`](super::Assets).
///
/// # Implementing `Asset`
/// When implementing `Asset`, you need to provide the fallible conversion function
/// `try_from_bytes`, which constructs an `Asset` from a vector of bytes.
///
/// ```
/// use std::path::Path;
/// use std::sync::Arc;
/// use irradiance_runtime::asset::{Asset, Assets};
/// use irradiance_runtime::gfx::GraphicsContext;
///
/// #[derive(Debug)]
/// struct Text {
///     value: String
/// }
///
/// impl Asset for Text {
///     type Error = std::io::Error;
///
///     fn try_from_bytes(
///         _assets: Arc<Assets>,
///         _graphics_context: Arc<GraphicsContext>,
///         _path: &Path,
///         bytes: Vec<u8>,
///     ) -> Result<Self, Self::Error> {
///         Ok(Self {
///             value: String::from_utf8(bytes)
///                 .map_err(|e| Self::Error::new(std::io::ErrorKind::InvalidData, e))?,
///         })
///     }
/// }
/// ```
pub trait Asset: Debug + Sized + 'static {
    /// The type returned in the event of a conversion error.
    type Error: std::error::Error + Send + Sync + 'static;

    /// Performs the conversion.
    #[allow(unused)]
    fn try_from_bytes(
        assets: Arc<Assets>,
        graphics_context: Arc<GraphicsContext>,
        path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, Self::Error>;

    /// Converts an asset to bytes.
    fn to_bytes(&self) -> Option<Vec<u8>> {
        None
    }
}

pub trait HandleAbstract: Debug + Send + Sync {
    fn path(&self) -> &Path;
    fn set_path(&self, path: PathBuf);
    unsafe fn set_none(&self);
    unsafe fn mark_unsaved(&self);
    unsafe fn to_bytes(&self) -> Option<Vec<u8>>;
    fn wait(&self);
    fn as_any(self: Arc<Self>) -> Arc<dyn Any + Send + Sync + 'static>;
}

pub struct Handle<A>
where
    A: Asset,
{
    path: UnsafeCell<PathBuf>,
    unsaved: Cell<bool>,
    ready: AtomicBool,
    read_result: UnsafeCell<Option<Result<A, A::Error>>>,
}

impl<A> Handle<A>
where
    A: Asset,
{
    pub fn new(path: PathBuf) -> Arc<Self> {
        Arc::new(Self {
            path: UnsafeCell::new(path),
            unsaved: Cell::new(false),
            ready: false.into(),
            read_result: UnsafeCell::new(None),
        })
    }

    pub unsafe fn init(&self, read_result: Result<A, A::Error>) {
        debug_assert!(!self.ready.load(Ordering::Relaxed));
        *self.read_result.get() = Some(read_result);
        self.ready.store(true, Ordering::Relaxed);
    }

    unsafe fn replace(&self, asset: A) {
        *self.read_result.get() = Some(Ok(asset));
    }

    fn get(&self) -> Option<&Result<A, A::Error>> {
        if self.ready.load(Ordering::Relaxed) {
            unsafe { &*self.read_result.get() }.as_ref()
        } else {
            None
        }
    }
}

impl<A> HandleAbstract for Handle<A>
where
    A: Asset,
{
    fn path(&self) -> &Path {
        unsafe { &*self.path.get() }
    }

    fn set_path(&self, path: PathBuf) {
        *unsafe { &mut *self.path.get() } = path;
    }

    unsafe fn set_none(&self) {
        self.ready.store(false, Ordering::Relaxed);
        *self.read_result.get() = None;
    }

    unsafe fn mark_unsaved(&self) {
        self.unsaved.replace(true);
    }

    unsafe fn to_bytes(&self) -> Option<Vec<u8>> {
        if let Some(asset) = self
            .get()
            .and_then(|result| result.as_ref().ok())
            .filter(|_| self.unsaved.get())
        {
            self.unsaved.replace(false);
            return asset.to_bytes();
        }
        None
    }

    fn wait(&self) {
        while self.get().is_none() {
            std::hint::spin_loop();
        }
    }

    fn as_any(self: Arc<Self>) -> Arc<dyn Any + Send + Sync + 'static> {
        self
    }
}

impl<A> Eq for Handle<A> where A: Asset {}

impl<A> PartialEq for Handle<A>
where
    A: Asset,
{
    fn eq(&self, other: &Self) -> bool {
        PartialEq::eq(self.path(), other.path())
    }
}

impl<A> Ord for Handle<A>
where
    A: Asset,
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        Ord::cmp(self.path(), other.path())
    }
}

impl<A> PartialOrd for Handle<A>
where
    A: Asset,
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        PartialOrd::partial_cmp(self.path(), other.path())
    }
}

impl<A> Hash for Handle<A>
where
    A: Asset,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        Hash::hash(self.path(), state)
    }
}

impl<A> Debug for Handle<A>
where
    A: Asset,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(self, f)
    }
}

impl<A> Display for Handle<A>
where
    A: Asset,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.path())
    }
}

unsafe impl<A> Send for Handle<A> where A: Asset {}

unsafe impl<A> Sync for Handle<A> where A: Asset {}

/// A pointer to an `Asset`.
///
/// The type `AssetRef<A>` provides shared ownership of an asset of type `A`.
///
/// Assets in games are usually not small and there are many of them. Therefore, reading an `Asset`
/// via [`Assets`](super::Assets) will be performed asynchronously. `AssetRef<A>` provides
/// operations to access an `Asset` when it is available.
pub struct AssetRef<A>
where
    A: Asset,
{
    handle: Option<Arc<Handle<A>>>,
}

impl<A> AssetRef<A>
where
    A: Asset,
{
    /// Creates a pointer to a pre read asset.
    pub fn new_pre_read(read_result: Result<A, A::Error>) -> Self {
        let handle = Handle::new("".into());
        unsafe {
            handle.init(read_result);
        }

        Self {
            handle: Some(handle),
        }
    }

    pub(super) fn new(handle: Arc<Handle<A>>) -> Self {
        Self {
            handle: Some(handle),
        }
    }

    /// Returns the path to the `Asset`.
    pub fn path(&self) -> Option<PathBuf> {
        self.handle
            .as_ref()
            .map(|handle| handle.path().to_path_buf())
    }

    /// Returns `true` if the `Asset` is fully read and no error has occurred.
    pub fn is_ok(&self) -> bool {
        self.ok().is_some()
    }

    /// Returns `true` if an error occurred while reading the `Asset`.
    pub fn is_err(&self) -> bool {
        self.err().is_some()
    }

    /// Returns an [`Option`](std::option::Option) of the `Asset`.
    ///
    /// The returned value is [`Some`](std::option::Option::Some) only if the `Asset` is fully read
    /// and no error has occurred.
    pub fn ok(&self) -> Option<&A> {
        if let Some(handle) = self.handle.as_ref() {
            if let Some(Ok(asset)) = handle.get() {
                return Some(asset);
            }
        }
        None
    }

    /// Returns an [`Option`](std::option::Option) of the error.
    ///
    /// The returned value is [`Some`](std::option::Option::Some) only if an error has occurred
    /// while reading the `Asset`.
    pub fn err(&self) -> Option<&A::Error> {
        if let Some(handle) = self.handle.as_ref() {
            if let Some(Err(err)) = handle.get() {
                return Some(err);
            }
        }
        None
    }

    /// Returns the `Asset`, blocking the current thread until it is able to do so.
    ///
    /// # Panics
    /// This function panics when called on an empty `AssetRef`.
    pub fn unwrap(&self) -> &A {
        while self.handle.as_ref().unwrap().get().is_none() {
            std::hint::spin_loop();
        }
        self.handle
            .as_ref()
            .unwrap()
            .get()
            .unwrap()
            .as_ref()
            .unwrap()
    }

    /// Replaces the `Asset` behind this reference.
    ///
    /// The replacement happens in place. All references of the old asset point to the new asset.
    ///
    /// # Safety
    /// This function is only safe when used in a mutually exclusive context.
    pub unsafe fn replace(&self, asset: A) {
        if let Some(handle) = &self.handle {
            handle.replace(asset);
            handle.unsaved.replace(true);
        }
    }

    /// Marks the [`Asset`] as unsaved.
    ///
    /// # Safety
    /// This function is only safe when used in a mutually exclusive context.
    pub unsafe fn mark_unsaved(&self) {
        if let Some(handle) = &self.handle {
            handle.unsaved.replace(true);
        }
    }

    /// Returns the error, blocking the current thread until it is able to do so.
    ///
    /// # Panics
    /// This function panics when called on an empty `AssetRef`.
    pub fn unwrap_err(&self) -> &A::Error {
        while self.handle.as_ref().unwrap().get().is_none() {
            std::hint::spin_loop();
        }
        self.handle
            .as_ref()
            .unwrap()
            .get()
            .unwrap()
            .as_ref()
            .unwrap_err()
    }
}

impl<A> Clone for AssetRef<A>
where
    A: Asset,
{
    fn clone(&self) -> Self {
        Self {
            handle: self.handle.clone(),
        }
    }
}

impl<A> Eq for AssetRef<A> where A: Asset {}

impl<A> PartialEq for AssetRef<A>
where
    A: Asset,
{
    fn eq(&self, other: &Self) -> bool {
        PartialEq::eq(&self.handle, &other.handle)
    }
}

impl<A> Ord for AssetRef<A>
where
    A: Asset,
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        Ord::cmp(&self.handle, &other.handle)
    }
}

impl<A> PartialOrd for AssetRef<A>
where
    A: Asset,
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        PartialOrd::partial_cmp(&self.handle, &other.handle)
    }
}

impl<A> Hash for AssetRef<A>
where
    A: Asset,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        if let Some(handle) = self.handle.as_ref() {
            Hash::hash(&handle, state)
        } else {
            Hash::hash(&PathBuf::default(), state)
        }
    }
}

impl<A> Debug for AssetRef<A>
where
    A: Asset,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(self, f)
    }
}

impl<A> Display for AssetRef<A>
where
    A: Asset,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(handle) = self.handle.as_ref() {
            Display::fmt(handle, f)
        } else {
            Ok(())
        }
    }
}

impl<A> Default for AssetRef<A>
where
    A: Asset,
{
    fn default() -> Self {
        Self { handle: None }
    }
}

impl<A> Serialize for AssetRef<A>
where
    A: Asset,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        if let Some(path) = self.path() {
            path.serialize(serializer)
        } else {
            "".serialize(serializer)
        }
    }
}

#[cfg(test)]
mod tests {
    use std::cmp::Ordering;
    use std::collections::hash_map::DefaultHasher;
    use std::hash::{Hash, Hasher};
    use std::io::ErrorKind;
    use std::path::Path;

    use serde_test::assert_ser_tokens;
    use serde_test::Token;

    use crate::asset::private::{AssetRef, Handle, HandleAbstract};
    use crate::asset::test::TestAsset;

    fn create_asset_ref() -> AssetRef<TestAsset> {
        AssetRef::new(unsafe {
            let handle = Handle::new("test".into());
            handle.init(Ok(TestAsset {
                value: "test".into(),
            }));
            handle
        })
    }

    fn create_asset_ref_error() -> AssetRef<TestAsset> {
        AssetRef::new(unsafe {
            let handle = Handle::<TestAsset>::new("test".into());
            handle.init(Err(std::io::Error::from(ErrorKind::NotFound)));
            handle
        })
    }

    #[test]
    fn handle_path() {
        let handle = Handle::<TestAsset>::new("test".into());

        assert_eq!(Path::new("test"), handle.path())
    }

    #[test]
    fn handle_not_ready() {
        let handle = Handle::<TestAsset>::new("test".into());

        assert!(matches!(handle.get(), None));
    }

    #[test]
    fn handle_ready() {
        let handle = Handle::new("test".into());
        unsafe {
            handle.init(Ok(TestAsset {
                value: "test".into(),
            }));
        }

        assert!(matches!(
            handle.get(),
            Some(Ok(test_asset)) if test_asset.value == "test"
        ));
    }

    #[test]
    fn asset_ref_is_not_ok_empty() {
        let asset = AssetRef::<TestAsset>::default();

        assert!(!asset.is_ok());
    }

    #[test]
    fn asset_ref_is_not_ok() {
        let asset = AssetRef::new(Handle::<TestAsset>::new("test".into()));

        assert!(!asset.is_ok());
    }

    #[test]
    fn asset_ref_is_ok() {
        let asset = create_asset_ref();

        assert!(asset.is_ok());
    }

    #[test]
    fn asset_ref_is_not_err_empty() {
        let asset = AssetRef::<TestAsset>::default();

        assert!(!asset.is_err());
    }

    #[test]
    fn asset_ref_is_not_err() {
        let asset = AssetRef::new(Handle::<TestAsset>::new("test".into()));

        assert!(!asset.is_err());
    }

    #[test]
    fn asset_ref_is_err() {
        let asset = create_asset_ref_error();

        assert!(asset.is_err());
    }

    #[test]
    fn asset_ref_ok_empty() {
        let asset = AssetRef::<TestAsset>::default();

        assert!(asset.ok().is_none());
    }

    #[test]
    fn asset_ref_ok_not_ready() {
        let asset = AssetRef::new(Handle::<TestAsset>::new("test".into()));

        assert!(asset.ok().is_none());
    }

    #[test]
    fn asset_ref_ok_err() {
        let asset = create_asset_ref_error();

        assert!(asset.ok().is_none());
    }

    #[test]
    fn asset_ref_ok() {
        let asset = create_asset_ref();

        assert!(matches!(asset.ok(), Some(asset) if asset.value == "test"));
    }

    #[test]
    fn asset_ref_err_empty() {
        let asset = AssetRef::<TestAsset>::default();

        assert!(asset.err().is_none());
    }

    #[test]
    fn asset_ref_err_not_ready() {
        let asset = AssetRef::new(Handle::<TestAsset>::new("test".into()));

        assert!(asset.err().is_none());
    }

    #[test]
    fn asset_ref_err_ok() {
        let asset = create_asset_ref();

        assert!(asset.err().is_none());
    }

    #[test]
    fn asset_ref_err() {
        let asset = create_asset_ref_error();

        assert!(matches!(asset.err(), Some(err) if err.kind() == ErrorKind::NotFound));
    }

    #[test]
    #[should_panic]
    fn asset_ref_unwrap_empty() {
        let asset = AssetRef::<TestAsset>::default();

        asset.unwrap();
    }

    #[test]
    fn asset_ref_unwrap() {
        let asset = create_asset_ref();

        assert_eq!(asset.unwrap().value, "test");
    }

    #[test]
    #[should_panic]
    fn asset_ref_unwrap_err_empty() {
        let asset = AssetRef::<TestAsset>::default();

        asset.unwrap_err();
    }

    #[test]
    fn asset_ref_unwrap_err() {
        let asset = create_asset_ref_error();

        assert_eq!(asset.unwrap_err().kind(), ErrorKind::NotFound);
    }

    #[test]
    fn asset_ref_not_eq() {
        let asset = create_asset_ref();
        let other = AssetRef::<TestAsset>::default();

        assert_ne!(asset, other);
    }

    #[test]
    fn asset_ref_eq() {
        let asset = create_asset_ref();
        let other = create_asset_ref();

        assert_eq!(asset, other)
    }

    #[test]
    fn asset_ref_cmp_eq() {
        let asset = create_asset_ref();
        let other = create_asset_ref();

        assert_eq!(Ordering::Equal, asset.cmp(&other));
    }

    #[test]
    fn asset_ref_cmp_less() {
        let asset = create_asset_ref();
        let other = AssetRef::<TestAsset>::default();

        assert_eq!(Ordering::Less, other.cmp(&asset));
    }

    #[test]
    fn asset_ref_cmp_greater() {
        let asset = create_asset_ref();
        let other = AssetRef::<TestAsset>::default();

        assert_eq!(Ordering::Greater, asset.cmp(&other));
    }

    #[test]
    fn asset_ref_hash() {
        let asset = create_asset_ref();
        let mut asset_hasher = DefaultHasher::new();
        let path = Path::new("test");
        let mut path_hasher = DefaultHasher::new();

        asset.hash(&mut asset_hasher);
        path.hash(&mut path_hasher);

        assert_eq!(path_hasher.finish(), asset_hasher.finish());
    }

    #[test]
    fn asset_ref_hash_empty() {
        let asset = AssetRef::<TestAsset>::default();
        let mut asset_hasher = DefaultHasher::new();
        let path = Path::new("");
        let mut path_hasher = DefaultHasher::new();

        asset.hash(&mut asset_hasher);
        path.hash(&mut path_hasher);

        assert_eq!(path_hasher.finish(), asset_hasher.finish());
    }

    #[test]
    fn asset_ref_hash_not_eq() {
        let asset = create_asset_ref();
        let mut asset_hasher = DefaultHasher::new();
        let other = AssetRef::<TestAsset>::default();
        let mut other_hasher = DefaultHasher::new();

        asset.hash(&mut asset_hasher);
        other.hash(&mut other_hasher);

        assert_ne!(other_hasher.finish(), asset_hasher.finish());
    }

    #[test]
    fn asset_ref_debug() {
        assert_eq!("\"test\"", format!("{:?}", create_asset_ref()))
    }

    #[test]
    fn asset_ref_serialize() {
        let asset = create_asset_ref();

        assert_ser_tokens(&asset, &[Token::Str("test")]);
    }
}
