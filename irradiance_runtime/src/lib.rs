#![warn(missing_docs)]

//! Runtime library of the irradiance engine.

#[cfg(test)]
#[macro_use]
extern crate approx;
pub extern crate approx;
pub extern crate rodio;
pub extern crate ron;
extern crate self as irradiance_runtime;
pub extern crate winit;

pub use irradiance_derive::Component;
pub use irradiance_shaders::shader;

pub mod animation;
pub mod asset;
pub mod audio;
pub mod core;
pub mod ecs;
pub mod gfx;
pub mod input;
pub mod math;
pub mod physics;
pub mod rendering;
pub mod scene;
pub mod window;
