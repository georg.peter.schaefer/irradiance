//! Window creation.

pub use private::Window;
pub use private::WindowBuilder;
pub use private::WindowCreationError;

mod private;
