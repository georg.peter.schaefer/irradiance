use std::cell::{Cell, RefCell, UnsafeCell};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::sync::Arc;

use winit::dpi::{LogicalPosition, PhysicalPosition, PhysicalSize, Position, Size};
use winit::error::OsError;
use winit::event_loop::EventLoop;
use winit::window::{Fullscreen, Icon, Window as Inner};

use crate::math::Vec2;

/// A window object.
#[derive(Debug)]
pub struct Window {
    unsaved: Cell<bool>,
    title: RefCell<String>,
    inner: Inner,
    event_loop: UnsafeCell<Option<EventLoop<()>>>,
}

impl Window {
    /// Starts building a new window.
    pub fn builder() -> WindowBuilder {
        WindowBuilder {
            height: 768,
            width: 1024,
            maximized: false,
            fullscreen: false,
            title: "Window".to_string(),
        }
    }

    #[doc(hidden)]
    pub fn inner(&self) -> &Inner {
        &self.inner
    }

    /// Sets the title.
    pub fn set_title(&self, title: &str) {
        *self.title.borrow_mut() = title.to_string();
        self.update_title();
    }

    /// Sets the unsaved marker.
    pub fn set_unsaved(&self, unsaved: bool) {
        if self.unsaved.get() != unsaved {
            self.unsaved.replace(unsaved);
            self.update_title();
        }
    }

    fn update_title(&self) {
        let title = if self.unsaved.get() {
            format!("• {}", self.title.borrow())
        } else {
            self.title.borrow().to_owned()
        };
        self.inner.set_title(&title);
    }

    /// Sets the window maximized.
    pub fn set_maximized(&self, maximized: bool) {
        self.inner.set_maximized(maximized);
    }

    /// Returns the resolution.
    pub fn resolution(&self) -> [u32; 2] {
        self.inner.inner_size().into()
    }

    /// Sets the resolution.
    pub fn set_resolution(&self, resolution: [u32; 2]) {
        self.inner
            .set_inner_size(Size::Physical(PhysicalSize::from(resolution)));
    }

    /// Sets the window icon.
    pub fn set_icon(&self, bytes: &[u8]) {
        let image_buffer = image::load_from_memory(bytes).unwrap().to_rgba8();
        self.inner.set_window_icon(Some(
            Icon::from_rgba(
                image_buffer.as_raw().clone(),
                image_buffer.width(),
                image_buffer.height(),
            )
            .unwrap(),
        ));
    }

    /// Sets the cursor position.
    pub fn set_cursor_position(&self, position: Vec2) {
        self.inner
            .set_cursor_position(Position::Logical(LogicalPosition::new(
                position[0] as _,
                position[1] as _,
            )))
            .expect("set cursor position");
    }

    #[doc(hidden)]
    pub fn event_loop(&self) -> &EventLoop<()> {
        unsafe { &*self.event_loop.get() }.as_ref().unwrap()
    }

    #[doc(hidden)]
    pub fn take_event_loop(&self) -> EventLoop<()> {
        unsafe { &mut *self.event_loop.get() }.take().unwrap()
    }
}

/// Type for building a [`Window`].
pub struct WindowBuilder {
    height: u32,
    width: u32,
    maximized: bool,
    fullscreen: bool,
    title: String,
}

impl WindowBuilder {
    /// Sets the title.
    pub fn title(&mut self, title: &str) -> &mut Self {
        self.title = title.to_string();
        self
    }

    /// Sets the window to fullscreen.
    pub fn fullscreen(&mut self) -> &mut Self {
        self.fullscreen = true;
        self
    }

    /// Maximizes the window.
    pub fn maximized(&mut self) -> &mut Self {
        self.maximized = true;
        self
    }

    /// Sets the width.
    pub fn width(&mut self, width: u32) -> &mut Self {
        self.width = width;
        self
    }

    /// Sets the height.
    pub fn height(&mut self, height: u32) -> &mut Self {
        self.height = height;
        self
    }

    /// Builds the [`Window`].
    ///
    /// # Errors
    /// This function returns an error if the window creation fails.
    pub fn build(&self) -> Result<Arc<Window>, WindowCreationError> {
        let event_loop = EventLoop::new();
        let inner = winit::window::WindowBuilder::new()
            .with_title(&self.title)
            .with_inner_size(Size::Physical(PhysicalSize::new(self.width, self.height)))
            .with_fullscreen(self.fullscreen.then(|| Fullscreen::Borderless(None)))
            .with_maximized(self.maximized)
            .build(&event_loop)?;

        let monitor = inner.current_monitor().unwrap();
        let size = monitor.size();
        let position = monitor.position();
        inner.set_outer_position(Position::Physical(PhysicalPosition::new(
            position.x + ((size.width - self.width) / 2) as i32,
            position.y + ((size.height - self.height) / 2) as i32,
        )));

        Ok(Arc::new(Window {
            unsaved: Cell::new(false),
            title: RefCell::new(self.title.clone()),
            inner,
            event_loop: UnsafeCell::new(Some(event_loop)),
        }))
    }
}

/// Error which can occur when creating a [`Window`].
#[derive(Debug)]
pub struct WindowCreationError {
    inner: OsError,
}

impl Display for WindowCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Window creation has failed")
    }
}

impl std::error::Error for WindowCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(&self.inner)
    }
}

#[doc(hidden)]
impl From<OsError> for WindowCreationError {
    fn from(e: OsError) -> Self {
        Self { inner: e }
    }
}
