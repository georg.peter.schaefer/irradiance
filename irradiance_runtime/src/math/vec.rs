//! Generic vector type and operations.
use std::fmt::Formatter;
use std::marker::PhantomData;
use std::ops::{
    Add, AddAssign, Deref, DerefMut, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub,
    SubAssign,
};

use approx::relative_ne;
use approx::{AbsDiffEq, RelativeEq};
use num::traits::NumAssign;
use num::{Float, Num, Signed};
use rapier3d::math::{Real, Vector};
use serde::de::{Error, SeqAccess, Visitor};
use serde::ser::SerializeSeq;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use crate::math::mat::TMat;
use crate::math::quat::TQuat;

/// Trait for the kind of vector.
///
/// Indicates whether a vector is a row or a column vector.
pub trait Kind: Copy + Clone + PartialEq {}

/// Indicates that the [`vector`](TVec) is a row vector.
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Row;

impl Kind for Row {}

/// Indicates that the [`vector`](TVec) is a column vector.
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Column;

impl Kind for Column {}

/// A generic, n-dimensional vector type.
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
#[repr(C)]
pub struct TVec<K, T, const N: usize>
where
    K: Kind,
    T: Num,
{
    phantom_data: PhantomData<K>,
    components: [T; N],
}

impl<K, T, const N: usize> TVec<K, T, N>
where
    K: Kind,
    T: Num + Copy,
{
    /// Creates a vector with all components set to 0.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec3;
    ///
    /// assert_relative_eq!(Vec3::zero(), Vec3::new(0.0, 0.0, 0.0));
    /// ```
    pub fn zero() -> Self {
        Self {
            phantom_data: Default::default(),
            components: [T::zero(); N],
        }
    }
}

impl<K, T, const N: usize> TVec<K, T, N>
where
    K: Kind,
    T: Signed,
{
    /// Returns a vector with the absolute value for each component.
    pub fn abs(mut self) -> Self {
        for i in 0..N {
            self[i] = self[i].abs();
        }
        self
    }
}

impl<K, T> TVec<K, T, 2>
where
    K: Kind,
    T: Num,
{
    /// Creates a new vector with components `x` and `y`.
    pub fn new(x: T, y: T) -> Self {
        Self {
            phantom_data: Default::default(),
            components: [x, y],
        }
    }
}

impl<K, T> TVec<K, T, 3>
where
    K: Kind,
    T: Num,
{
    /// Creates a new vector with components `x`, `y` and `z`.
    pub fn new(x: T, y: T, z: T) -> Self {
        Self {
            phantom_data: Default::default(),
            components: [x, y, z],
        }
    }
}

impl<K, T> TVec<K, T, 3>
where
    K: Kind,
    T: Num + Neg<Output = T>,
{
    /// Creates the forward vector.
    pub fn forward() -> Self {
        Self::new(T::zero(), T::zero(), -T::one())
    }

    /// Creates the backward vector.
    pub fn backward() -> Self {
        Self::new(T::zero(), T::zero(), T::one())
    }

    /// Creates the up vector.
    pub fn up() -> Self {
        Self::new(T::zero(), T::one(), T::zero())
    }

    /// Creates the down vector.
    pub fn down() -> Self {
        Self::new(T::zero(), -T::one(), T::zero())
    }

    /// Creates the left vector.
    pub fn left() -> Self {
        Self::new(-T::one(), T::zero(), T::zero())
    }

    /// Creates the right vector.
    pub fn right() -> Self {
        Self::new(T::one(), T::zero(), T::zero())
    }
}

impl<K, T> TVec<K, T, 4>
where
    K: Kind,
    T: Num,
{
    /// Creates a new vector with components `x`, `y`, `z` and `w`.
    pub fn new(x: T, y: T, z: T, w: T) -> Self {
        Self {
            phantom_data: Default::default(),
            components: [x, y, z, w],
        }
    }
}

impl<T, const N: usize> TVec<Row, T, N>
where
    T: Num,
{
    /// Transposes a row vector into a column vector.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec3;
    /// use irradiance_runtime::math::vec::{TVec, Row};
    ///
    /// let v = Vec3::new(1.0, 2.0, 3.0);
    /// assert_relative_eq!(Vec3::transpose(v), TVec::<Row, f32, 3>::new(1.0, 2.0, 3.0));
    /// ```
    pub fn transpose(self) -> TVec<Column, T, N> {
        TVec {
            phantom_data: PhantomData::default(),
            components: self.components,
        }
    }
}

impl<T, const N: usize> TVec<Column, T, N>
where
    T: Num,
{
    /// Transposes a column vector into a row vector.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec3;
    /// use irradiance_runtime::math::vec::{TVec, Row};
    ///
    /// let v = TVec::<Row, f32, 3>::new(1.0, 2.0, 3.0);
    /// assert_relative_eq!(TVec::<Row, _, 3>::transpose(v), Vec3::new(1.0, 2.0, 3.0));
    /// ```
    pub fn transpose(self) -> TVec<Row, T, N> {
        TVec {
            phantom_data: PhantomData::default(),
            components: self.components,
        }
    }
}

impl<K, T> TVec<K, T, 3>
where
    K: Kind,
    T: NumAssign + Copy,
{
    /// Calculates the cross product between two vectors.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec3;
    ///
    /// let v = Vec3::new(1.0, 2.0, 3.0);
    /// let w = Vec3::new(-7.0, 8.0, 9.0);
    /// assert_relative_eq!(Vec3::cross(v, w), Vec3::new(-6.0, -30.0, 22.0));
    /// ```
    pub fn cross(self, rhs: Self) -> Self {
        TVec::<K, T, 3>::new(
            self[1] * rhs[2] - self[2] * rhs[1],
            self[2] * rhs[0] - self[0] * rhs[2],
            self[0] * rhs[1] - self[1] * rhs[0],
        )
    }
}

impl<K, T, const N: usize> TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    /// Calculates the dot product of two vectors.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec3;
    ///
    /// let v = Vec3::new(1.0, 2.0, 3.0);
    /// let w = Vec3::new(0.5, 0.1, 1.5);
    /// assert_relative_eq!(Vec3::dot(v, w), 5.2);
    /// ```
    pub fn dot(self, rhs: Self) -> T {
        let mut result = T::zero();
        for i in 0..N {
            result += self[i] * rhs[i];
        }
        result
    }

    /// Caluclates the squared length of a vector.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec2;
    ///
    /// let vec = Vec2::new(4.0, 3.0);
    /// assert_relative_eq!(vec.length2(), 25.0);
    /// ```
    pub fn length2(&self) -> T
    where
        K: Kind,
        T: NumAssign + Copy,
    {
        Self::dot(*self, *self)
    }

    /// Calculates the squared distance between two vectors.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec2;
    ///
    /// let v = Vec2::new(5.0, 13.5);
    /// let w = Vec2::new(2.0, 9.5);
    /// assert_relative_eq!(Vec2::distance2(v, w), 25.0);
    /// ```
    pub fn distance2(self, rhs: Self) -> T
    where
        K: Kind,
        T: NumAssign + Copy,
    {
        (self - rhs).length2()
    }
}

impl<K, T, const N: usize> TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Float,
    Self: RelativeEq,
{
    /// Calculates the length of a vector.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec2;
    ///
    /// let vec = Vec2::new(4.0, 3.0);
    /// assert_relative_eq!(vec.length(), 5.0);
    /// ```
    pub fn length(&self) -> T
    where
        K: Kind,
        T: NumAssign + Float,
    {
        self.length2().sqrt()
    }

    /// Calculates the distance between two vectors.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec2;
    ///
    /// let v = Vec2::new(5.0, 13.5);
    /// let w = Vec2::new(2.0, 9.5);
    /// assert_relative_eq!(Vec2::distance(v, w), 5.0);
    /// ```
    pub fn distance(self, rhs: Self) -> T
    where
        K: Kind,
        T: NumAssign + Float,
    {
        (self - rhs).length()
    }

    /// Normalizes a vector.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Vec3;
    ///
    /// let v = Vec3::new(1.0, 2.0, 2.0);
    /// assert_relative_eq!(Vec3::normalize(v), Vec3::new(1.0 / 3.0, 2.0 / 3.0, 2.0 / 3.0));
    /// ```
    pub fn normalize(self) -> TVec<K, T, N>
    where
        K: Kind,
        T: NumAssign + Float,
    {
        if relative_ne!(self, Self::zero()) {
            self / self.length()
        } else {
            self
        }
    }

    /// Linearly interpolates between two vectors.
    pub fn lerp(self, rhs: Self, blend_factor: T) -> Self {
        self * (T::one() - blend_factor) + rhs * blend_factor
    }
}

impl<K, T, const N: usize> TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy + Default + Float,
{
    /// Calculates the minimum of each component of two vectors.
    pub fn min(&self, rhs: &Self) -> Self {
        let mut result = Self::default();
        for i in 0..N {
            result[i] = self[i].min(rhs[i])
        }
        result
    }

    /// Calculates the maximum of each component of two vectors.
    pub fn max(&self, rhs: &Self) -> Self {
        let mut result = Self::default();
        for i in 0..N {
            result[i] = self[i].max(rhs[i])
        }
        result
    }
}

impl<K, T, const N: usize> Default for TVec<K, T, N>
where
    K: Kind,
    T: Num + Copy + Default,
{
    fn default() -> Self {
        Self {
            phantom_data: Default::default(),
            components: [T::default(); N],
        }
    }
}

impl<I, K, T, const N: usize> Index<I> for TVec<K, T, N>
where
    I: std::slice::SliceIndex<[T]>,
    K: Kind,
    T: Num,
{
    type Output = I::Output;

    fn index(&self, index: I) -> &Self::Output {
        Index::index(&**self, index)
    }
}

impl<I, K, T, const N: usize> IndexMut<I> for TVec<K, T, N>
where
    I: std::slice::SliceIndex<[T]>,
    K: Kind,
    T: Num,
{
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        IndexMut::index_mut(&mut **self, index)
    }
}

impl<K, T, const N: usize> Add for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    type Output = Self;

    fn add(mut self, rhs: Self) -> Self::Output {
        self += rhs;
        self
    }
}

impl<K, T, const N: usize> AddAssign for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    fn add_assign(&mut self, rhs: Self) {
        for i in 0..N {
            self.components[i] += rhs.components[i];
        }
    }
}

impl<K, T, const N: usize> Sub for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    type Output = Self;

    fn sub(mut self, rhs: Self) -> Self::Output {
        self -= rhs;
        self
    }
}

impl<K, T, const N: usize> SubAssign for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    fn sub_assign(&mut self, rhs: Self) {
        for i in 0..N {
            self.components[i] -= rhs.components[i];
        }
    }
}

impl<K, T, const N: usize> Mul for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    type Output = Self;

    fn mul(mut self, rhs: Self) -> Self::Output {
        self *= rhs;
        self
    }
}

impl<K, T, const N: usize> MulAssign for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    fn mul_assign(&mut self, rhs: Self) {
        for i in 0..N {
            self.components[i] *= rhs.components[i];
        }
    }
}

impl<K, T, const N: usize> Mul<T> for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    type Output = Self;

    fn mul(mut self, rhs: T) -> Self::Output {
        self *= rhs;
        self
    }
}

impl<K, T, const N: usize> MulAssign<T> for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    fn mul_assign(&mut self, rhs: T) {
        for i in 0..N {
            self.components[i] *= rhs;
        }
    }
}

impl<T, const N: usize> Mul<TVec<Column, T, N>> for TVec<Row, T, N>
where
    T: NumAssign + Copy,
{
    type Output = T;

    fn mul(self, rhs: TVec<Column, T, N>) -> Self::Output {
        let mut result = T::zero();
        for i in 0..N {
            result += self[i] * rhs[i];
        }
        result
    }
}

macro_rules! impl_scalar_mul_vec {
    ($type:ty) => {
        impl<K, const N: usize> Mul<TVec<K, $type, N>> for $type
        where
            K: Kind,
        {
            type Output = TVec<K, $type, N>;

            fn mul(self, rhs: TVec<K, $type, N>) -> Self::Output {
                rhs * self
            }
        }
    };
}

impl_scalar_mul_vec!(i8);
impl_scalar_mul_vec!(i16);
impl_scalar_mul_vec!(i32);
impl_scalar_mul_vec!(i64);
impl_scalar_mul_vec!(i128);
impl_scalar_mul_vec!(isize);
impl_scalar_mul_vec!(u8);
impl_scalar_mul_vec!(u16);
impl_scalar_mul_vec!(u32);
impl_scalar_mul_vec!(u64);
impl_scalar_mul_vec!(u128);
impl_scalar_mul_vec!(usize);
impl_scalar_mul_vec!(f32);
impl_scalar_mul_vec!(f64);

impl<K, T, const N: usize> Div for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    type Output = Self;

    fn div(mut self, rhs: Self) -> Self::Output {
        self /= rhs;
        self
    }
}

impl<K, T, const N: usize> DivAssign for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    fn div_assign(&mut self, rhs: Self) {
        for i in 0..N {
            self.components[i] /= rhs.components[i];
        }
    }
}

impl<K, T, const N: usize> Div<T> for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    type Output = Self;

    fn div(mut self, rhs: T) -> Self::Output {
        self /= rhs;
        self
    }
}

impl<K, T, const N: usize> DivAssign<T> for TVec<K, T, N>
where
    K: Kind,
    T: NumAssign + Copy,
{
    fn div_assign(&mut self, rhs: T) {
        for i in 0..N {
            self.components[i] /= rhs;
        }
    }
}

impl<K, T, const N: usize> Neg for TVec<K, T, N>
where
    K: Kind,
    T: Num + Neg<Output = T> + Copy,
{
    type Output = Self;

    fn neg(mut self) -> Self::Output {
        for component in &mut self.components {
            *component = -*component;
        }
        self
    }
}

impl<K, T, const N: usize> From<[T; N]> for TVec<K, T, N>
where
    K: Kind,
    T: Num,
{
    fn from(components: [T; N]) -> Self {
        Self {
            phantom_data: Default::default(),
            components,
        }
    }
}

impl<K, T, const N: usize> From<TVec<K, T, N>> for [T; N]
where
    K: Kind,
    T: Num,
{
    fn from(v: TVec<K, T, N>) -> Self {
        v.components
    }
}

impl<K, T> From<TVec<K, T, 2>> for TVec<K, T, 3>
where
    K: Kind,
    T: Num + Copy,
{
    fn from(from: TVec<K, T, 2>) -> Self {
        TVec::<K, T, 3>::new(from[0], from[1], T::one())
    }
}

impl<K, T> From<TVec<K, T, 3>> for TVec<K, T, 4>
where
    K: Kind,
    T: Num + Copy,
{
    fn from(from: TVec<K, T, 3>) -> Self {
        TVec::<K, T, 4>::new(from[0], from[1], from[2], T::one())
    }
}

impl<T> From<TQuat<T>> for TVec<Column, T, 3>
where
    T: Float + NumAssign + Copy,
{
    fn from(quat: TQuat<T>) -> Self {
        let m = TMat::from(quat);
        (m * TVec::<Column, T, 4>::new(T::zero(), T::zero(), -T::one(), T::one())).into()
    }
}

impl<K, T> From<TVec<K, T, 4>> for TVec<K, T, 3>
where
    K: Kind,
    T: Num + Copy,
{
    fn from(from: TVec<K, T, 4>) -> Self {
        Self::new(from[0], from[1], from[2])
    }
}

impl From<TVec<Column, f32, 3>> for Vector<Real> {
    fn from(vec: TVec<Column, f32, 3>) -> Self {
        Vector::new(vec[0], vec[1], vec[2])
    }
}

impl From<Vector<Real>> for TVec<Column, f32, 3> {
    fn from(vec: Vector<Real>) -> Self {
        TVec::<Column, f32, 3>::new(vec.x, vec.y, vec.z)
    }
}

impl<K, T, const N: usize> Deref for TVec<K, T, N>
where
    K: Kind,
    T: Num,
{
    type Target = [T];

    fn deref(&self) -> &Self::Target {
        &self.components
    }
}

impl<K, T, const N: usize> DerefMut for TVec<K, T, N>
where
    K: Kind,
    T: Num,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.components
    }
}

impl<K, T, const N: usize> AbsDiffEq for TVec<K, T, N>
where
    T: AbsDiffEq + Float,
    T::Epsilon: Copy,
    K: Kind,
{
    type Epsilon = T::Epsilon;

    fn default_epsilon() -> Self::Epsilon {
        T::default_epsilon()
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        self.components
            .iter()
            .zip(&other.components)
            .all(|(lhs, rhs)| T::abs_diff_eq(lhs, rhs, epsilon))
    }
}

impl<K, T, const N: usize> RelativeEq for TVec<K, T, N>
where
    T: RelativeEq + Float,
    T::Epsilon: Copy,
    K: Kind,
{
    fn default_max_relative() -> T::Epsilon {
        T::default_max_relative()
    }

    fn relative_eq(&self, other: &Self, epsilon: T::Epsilon, max_relative: T::Epsilon) -> bool {
        self.components
            .iter()
            .zip(&other.components)
            .all(|(lhs, rhs)| T::relative_eq(lhs, rhs, epsilon, max_relative))
    }
}

impl<K, T, const N: usize> Serialize for TVec<K, T, N>
where
    K: Kind,
    T: Num + Serialize,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut sequence = serializer.serialize_seq(Some(N))?;
        for i in 0..N {
            sequence.serialize_element(&self.components[i])?;
        }
        sequence.end()
    }
}

impl<'de, K, T, const N: usize> Deserialize<'de> for TVec<K, T, N>
where
    K: Kind,
    T: Num + Copy + Deserialize<'de>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct ArrayVisitor<T, const N: usize> {
            marker: PhantomData<fn() -> [T; N]>,
        }

        impl<T, const N: usize> ArrayVisitor<T, N>
        where
            T: Num,
        {
            fn new() -> Self {
                Self {
                    marker: Default::default(),
                }
            }
        }

        impl<'de, T, const N: usize> Visitor<'de> for ArrayVisitor<T, N>
        where
            T: Num + Copy + Deserialize<'de>,
        {
            type Value = [T; N];

            fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
                write!(formatter, "[{}; {}]", std::any::type_name::<T>(), N)
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: SeqAccess<'de>,
            {
                let mut array = [T::zero(); N];

                for (i, component) in array.iter_mut().enumerate() {
                    if let Some(value) = seq.next_element()? {
                        *component = value;
                    } else {
                        return Err(A::Error::invalid_length(
                            i + 1,
                            &format!("expected array of length {}", N).as_str(),
                        ));
                    }
                }

                Ok(array)
            }
        }

        Ok(Self::from(
            deserializer.deserialize_seq(ArrayVisitor::new())?,
        ))
    }
}

#[cfg(test)]
mod tests {
    use crate::math::vec::{Row, TVec};
    use crate::math::{IVec2, Vec2, Vec3, Vec4};

    #[test]
    fn zero() {
        let vec = Vec3::zero();
        assert_relative_eq!(vec[0], 0.0);
        assert_relative_eq!(vec[1], 0.0);
        assert_relative_eq!(vec[2], 0.0);
    }

    #[test]
    fn vec2_new() {
        let result = Vec2::new(1.2, 0.9);
        assert_relative_eq!(result[0], 1.2);
        assert_relative_eq!(result[1], 0.9);
    }

    #[test]
    fn vec3_new() {
        let result = Vec3::new(1.2, 0.9, 0.4);
        assert_relative_eq!(result[0], 1.2);
        assert_relative_eq!(result[1], 0.9);
        assert_relative_eq!(result[2], 0.4);
    }

    #[test]
    fn vec4_new() {
        let result = Vec4::new(1.2, 0.9, 0.4, 1.9);
        assert_relative_eq!(result[0], 1.2);
        assert_relative_eq!(result[1], 0.9);
        assert_relative_eq!(result[2], 0.4);
        assert_relative_eq!(result[3], 1.9);
    }

    #[test]
    fn transpose_row_vector() {
        let v = Vec3::new(1.0, 2.0, 3.0);
        assert_relative_eq!(Vec3::transpose(v), TVec::<Row, f32, 3>::new(1.0, 2.0, 3.0));
    }

    #[test]
    fn transpose_component_vector() {
        let v = TVec::<Row, f32, 3>::new(1.0, 2.0, 3.0);
        assert_relative_eq!(TVec::<Row, _, 3>::transpose(v), Vec3::new(1.0, 2.0, 3.0));
    }

    #[test]
    fn cross() {
        let v = Vec3::new(1.0, 2.0, 3.0);
        let w = Vec3::new(-7.0, 8.0, 9.0);
        assert_relative_eq!(Vec3::cross(v, w), Vec3::new(-6.0, -30.0, 22.0));
    }

    #[test]
    fn dot() {
        let v = Vec3::new(1.0, 2.0, 3.0);
        let w = Vec3::new(0.5, 0.1, 1.5);
        assert_relative_eq!(Vec3::dot(v, w), 5.2);
        assert_relative_eq!(Vec3::transpose(v) * w, 5.2);
    }

    #[test]
    fn length2() {
        let vec = IVec2::new(4, 6);
        assert_eq!(vec.length2(), 52);
    }

    #[test]
    fn length() {
        let vec = Vec4::new(1.0, 1.0, 1.0, 1.0);
        assert_relative_eq!(vec.length(), 2.0);
    }

    #[test]
    fn distance2() {
        let v = Vec2::new(5.0, 13.5);
        let w = Vec2::new(2.0, 9.5);
        assert_relative_eq!(Vec2::distance2(v, w), 25.0);
    }

    #[test]
    fn distance() {
        let v = Vec2::new(5.0, 13.5);
        let w = Vec2::new(2.0, 9.5);
        assert_relative_eq!(Vec2::distance(v, w), 5.0);
    }

    #[test]
    fn normalize() {
        let v = Vec3::new(1.0, 2.0, 2.0);
        assert_relative_eq!(
            Vec3::normalize(v),
            Vec3::new(1.0 / 3.0, 2.0 / 3.0, 2.0 / 3.0)
        );
    }

    #[test]
    fn index_mut() {
        let mut vec = Vec3::new(0.1, 0.2, 0.3);
        assert_relative_eq!(vec, Vec3::new(0.1, 0.2, 0.3));
        vec[1] = 0.4;
        assert_relative_eq!(vec, Vec3::new(0.1, 0.4, 0.3));
    }

    #[test]
    fn add() {
        let mut vec = Vec3::new(1.2, 0.9, 0.4);
        vec += Vec3::new(0.1, 0.1, 0.1);
        assert_relative_eq!(vec, Vec3::new(1.3, 1.0, 0.5));
        assert_relative_eq!(vec, Vec3::new(1.2, 0.9, 0.4) + Vec3::new(0.1, 0.1, 0.1));
    }

    #[test]
    fn sub() {
        let mut vec = Vec2::new(1.2, 0.9);
        vec -= Vec2::new(0.2, 0.2);
        assert_relative_eq!(vec, Vec2::new(1.0, 0.7));
        assert_relative_eq!(vec, Vec2::new(1.2, 0.9) - Vec2::new(0.2, 0.2));
    }

    #[test]
    fn mul_vec() {
        let mut vec = Vec4::new(1.2, 0.9, 0.4, 1.9);
        vec *= Vec4::new(0.5, 2.0, 0.2, 0.1);
        assert_relative_eq!(vec, Vec4::new(0.6, 1.8, 0.08, 0.19));
        assert_relative_eq!(
            vec,
            Vec4::new(1.2, 0.9, 0.4, 1.9) * Vec4::new(0.5, 2.0, 0.2, 0.1)
        );
    }

    #[test]
    fn mul_scalar() {
        let mut vec = Vec2::new(0.5, 1.5);
        vec *= 3.0;
        assert_relative_eq!(vec, Vec2::new(1.5, 4.5));
        assert_relative_eq!(vec, Vec2::new(0.5, 1.5) * 3.0);
        assert_relative_eq!(vec, 3.0 * Vec2::new(0.5, 1.5));
    }

    #[test]
    fn div_vec() {
        let mut vec = Vec3::new(0.9, 0.3, 0.6);
        vec /= Vec3::new(3.0, 0.3, 2.0);
        assert_relative_eq!(vec, Vec3::new(0.3, 1.0, 0.3));
        assert_relative_eq!(vec, Vec3::new(0.9, 0.3, 0.6) / Vec3::new(3.0, 0.3, 2.0));
    }

    #[test]
    fn div_scalar() {
        let mut vec = Vec2::new(0.5, 1.5);
        vec /= 0.5;
        assert_relative_eq!(vec, Vec2::new(1.0, 3.0));
        assert_relative_eq!(vec, Vec2::new(0.5, 1.5) / 0.5);
    }

    #[test]
    fn from_array() {
        let result: TVec<Row, i32, 6> = [0, 1, 2, 3, 4, 5].into();
        assert_eq!(result[0], 0);
        assert_eq!(result[1], 1);
        assert_eq!(result[2], 2);
        assert_eq!(result[3], 3);
        assert_eq!(result[4], 4);
        assert_eq!(result[5], 5);
    }
}
