//! Generic vector math types and operations.
use crate::math::mat::TMat;
use crate::math::quat::TQuat;
use crate::math::vec::{Column, TVec};

pub mod mat;
pub mod projection;
pub mod quat;
pub mod vec;

/// Type alias for 2d floating point column vector.
pub type Vec2 = TVec<Column, f32, 2>;

/// Type alias for 3d floating point column vector.
pub type Vec3 = TVec<Column, f32, 3>;

/// Type alias for 4d floating point column vector.
pub type Vec4 = TVec<Column, f32, 4>;

/// Type alias for 2d signed integer column vector.
pub type IVec2 = TVec<Column, i32, 2>;

/// Type alias for 3d signed integer column vector.
pub type IVec3 = TVec<Column, i32, 3>;

/// Type alias for 4d signed integer column vector.
pub type IVec4 = TVec<Column, i32, 4>;

/// Type alias for 2d unsigned integer column vector.
pub type UVec2 = TVec<Column, u32, 2>;

/// Type alias for 3d unsigned integer column vector.
pub type UVec3 = TVec<Column, u32, 3>;

/// Type alias for 4d unsigned integer column vector.
pub type UVec4 = TVec<Column, u32, 4>;

/// Type alias for 2x2 floating point matrix;
pub type Mat2 = TMat<f32, 2, 2>;

/// Type alias for 3x3 floating point matrix;
pub type Mat3 = TMat<f32, 3, 3>;

/// Type alias for 4x4 floating point matrix;
pub type Mat4 = TMat<f32, 4, 4>;

/// Type alias for 2x3 floating point matrix;
pub type Mat2x3 = TMat<f32, 2, 3>;

/// Type alias for 3x2 floating point matrix;
pub type Mat3x2 = TMat<f32, 3, 2>;

/// Type alias for 2x4 floating point matrix;
pub type Mat2x4 = TMat<f32, 2, 4>;

/// Type alias for 4x2 floating point matrix;
pub type Mat4x2 = TMat<f32, 4, 2>;

/// Type alias for 3x4 floating point matrix;
pub type Mat3x4 = TMat<f32, 3, 4>;

/// Type alias for 4x3 floating point matrix;
pub type Mat4x3 = TMat<f32, 4, 3>;

/// Type alias for an `f64` quaternion.
pub type Quat = TQuat<f32>;
