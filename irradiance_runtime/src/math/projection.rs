//! Utilities for constructing projection matrices.

use num::Float;

use crate::math::mat::TMat;
use crate::math::vec::{Column, TVec};

/// Represents the parameters for a perspective projection matrix based on the field of view y,
/// aspect ratio and near and far plane.
pub struct PerspectiveFov<T>
where
    T: Float,
{
    fov_y: T,
    aspect_ratio: T,
    near: T,
    far: T,
}

impl<T> PerspectiveFov<T>
where
    T: Float,
{
    /// Creates a new perspective field of view.
    pub fn new(fov_y: T, aspect_ratio: T, near: T, far: T) -> Self {
        Self {
            fov_y,
            aspect_ratio,
            near,
            far,
        }
    }
}

impl<T> From<PerspectiveFov<T>> for TMat<T, 4, 4>
where
    T: Float,
{
    fn from(perspective_fov: PerspectiveFov<T>) -> Self {
        let focal_length = T::one() / T::tan(perspective_fov.fov_y / T::from(2).unwrap());
        let aspect_ratio = perspective_fov.aspect_ratio;
        let n = perspective_fov.near;
        let f = perspective_fov.far;
        [
            TVec::<Column, T, 4>::new(focal_length / aspect_ratio, T::zero(), T::zero(), T::zero()),
            TVec::<Column, T, 4>::new(T::zero(), -focal_length, T::zero(), T::zero()),
            TVec::<Column, T, 4>::new(T::zero(), T::zero(), f / (n - f), -T::one()),
            TVec::<Column, T, 4>::new(T::zero(), T::zero(), -(f * n) / (f - n), T::zero()),
        ]
        .into()
    }
}
/// Represents the parameters for an orthogonal projection matrix.
#[derive(Copy, Clone)]
pub struct Ortho<T>
where
    T: Float,
{
    left: T,
    right: T,
    bottom: T,
    top: T,
    near: T,
    far: T,
}

impl<T> Ortho<T>
where
    T: Float,
{
    /// Creates a new Orthographic projection.
    pub fn new(left: T, right: T, bottom: T, top: T, near: T, far: T) -> Self {
        Self {
            left,
            right,
            bottom,
            top,
            near,
            far,
        }
    }
}

impl<T> From<Ortho<T>> for TMat<T, 4, 4>
where
    T: Float,
{
    fn from(ortho: Ortho<T>) -> Self {
        let two = T::one() + T::one();
        let mut result = Self::identity();
        result[0][0] = two / (ortho.right - ortho.left);
        result[1][1] = -two / (ortho.top - ortho.bottom);
        result[2][2] = -T::one() / (ortho.far - ortho.near);
        result[3][0] = -(ortho.right + ortho.left) / (ortho.right - ortho.left);
        result[3][1] = -(ortho.top + ortho.bottom) / (ortho.top - ortho.bottom);
        result[3][2] = -ortho.near / (ortho.far - ortho.near);
        result
    }
}
