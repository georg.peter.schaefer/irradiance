//! Generic quaternion type and operations.

use std::fmt::Formatter;
use std::marker::PhantomData;
use std::ops::{
    Add, AddAssign, Deref, DerefMut, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub,
    SubAssign,
};

use approx::relative_eq;
use approx::{AbsDiffEq, RelativeEq};
use num::traits::NumAssign;
use num::Float;
use serde::de::{Error, SeqAccess, Visitor};
use serde::ser::SerializeSeq;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use crate::math::mat::TMat;
use crate::math::vec::{Column, TVec};

use super::Vec3;

/// A generic quaternion.
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct TQuat<T>
where
    T: Float,
{
    components: [T; 4],
}

impl<T> TQuat<T>
where
    T: NumAssign + Float,
{
    /// Creates a new quaternion with components `w`, `x`, `y` and `z`.
    pub fn new(w: T, x: T, y: T, z: T) -> Self {
        Self {
            components: [x, y, z, w],
        }
    }

    /// Creates a unit quaternion from an `angle` and an `axis`.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::{Quat, Vec3};
    ///
    /// let quat = Quat::from_angle_axis(45.0f32.to_radians(), Vec3::new(1.0, 1.0, 1.0).normalize());
    /// assert_relative_eq!(quat, Quat::new(0.9238795325112867, 0.22094238269039457, 0.22094238269039457, 0.22094238269039457));
    /// ```
    pub fn from_angle_axis(angle: T, axis: TVec<Column, T, 3>) -> Self {
        Self {
            components: [
                axis[0] * T::sin(angle / (T::one() + T::one())),
                axis[1] * T::sin(angle / (T::one() + T::one())),
                axis[2] * T::sin(angle / (T::one() + T::one())),
                T::cos(angle / (T::one() + T::one())),
            ],
        }
    }

    /// Calculates the dot product for two quaternions.
    pub fn dot(self, rhs: Self) -> T {
        self[0] * rhs[0] + self[1] * rhs[1] + self[2] * rhs[2] + self[3] * rhs[3]
    }

    /// Calculates the squared length of a quaternion.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Quat;
    ///
    /// let quat = Quat::new(0.0, 0.0, 3.0, 4.0);
    /// assert_relative_eq!(quat.length2(), 25.0);
    /// ```
    pub fn length2(&self) -> T {
        self[0] * self[0] + self[1] * self[1] + self[2] * self[2] + self[3] * self[3]
    }

    /// Calculates the length of a quaternion.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Quat;
    ///
    /// let quat = Quat::new(0.0, 0.0, 3.0, 4.0);
    /// assert_relative_eq!(quat.length(), 5.0);
    /// ```
    pub fn length(&self) -> T {
        T::sqrt(self.length2())
    }

    /// Normalizes a quaternion.
    ///
    /// # Examples
    /// ```
    /// use approx::assert_relative_eq;
    /// use irradiance_runtime::math::Quat;
    ///
    /// let quat = Quat::new(1.0, 2.0, 3.0, 4.0).normalize();
    /// assert_relative_eq!(quat.length(), 1.0);
    /// ```
    pub fn normalize(self) -> Self {
        self / self.length()
    }

    /// Linearly interpolates between two quaternions.
    pub fn lerp(self, rhs: Self, blend_factor: T) -> Self {
        self * (T::one() - blend_factor) + rhs * blend_factor
    }

    /// Rotate a point by `self`.
    pub fn rotate(self, rhs: TVec<Column, T, 3>) -> TVec<Column, T, 3> {
        (TMat::<T, 4, 4>::from(self) * TVec::<Column, T, 4>::from(rhs)).into()
    }
}

impl<T> TQuat<T>
where
    T: NumAssign + Float + RelativeEq,
{
    /// Returns the angle and axis.
    pub fn to_angle_axis(self) -> TVec<Column, T, 3> {
        let angle = (T::one() + T::one()) * T::acos(self[3]);
        let s = T::sqrt(T::one() - self[3] * self[3]);
        return if relative_eq!(s, T::zero()) {
            TVec::<Column, T, 3>::new(T::one(), T::zero(), T::zero())
        } else {
            TVec::<Column, T, 3>::new(self[0] / s, self[1] / s, self[2] / s)
        } * angle;
    }
}

impl TQuat<f32> {
    /// Returns the euler angles corresponding to this quaternion.
    pub fn to_euler(self) -> Vec3 {
        let first = (2.0 * (self[0] * self[2] - self[3] * self[1]))
            .atan2(self[3] * self[3] - self[0] * self[0] - self[1] * self[1] + self[3] * self[3]);
        let second = (-2.0 * (self[1] * self[2] - self[3] * self[0]))
            .clamp(-1.0, 1.0)
            .asin();
        let third = (2.0 * (self[0] * self[1] + self[3] * self[2]))
            .atan2(self[3] * self[3] - self[0] * self[0] + self[1] * self[1] - self[3] * self[3]);
        Vec3::new(first, second, third)
    }

    /// Returns a quaternion from euler angles.
    pub fn from_euler(euler: Vec3) -> Self {
        let x = Self::from_angle_axis(euler[0], Vec3::new(1.0, 0.0, 0.0));
        let y = Self::from_angle_axis(euler[1], Vec3::new(0.0, 1.0, 0.0));
        let z = Self::from_angle_axis(euler[2], Vec3::new(0.0, 0.0, 1.0));
        (y * x * z).normalize()
    }
}

impl<T> Default for TQuat<T>
where
    T: Float,
{
    fn default() -> Self {
        Self {
            components: [T::zero(), T::zero(), T::zero(), T::one()],
        }
    }
}

impl<T> From<[T; 4]> for TQuat<T>
where
    T: Float,
{
    fn from(components: [T; 4]) -> Self {
        Self { components }
    }
}

impl<T> From<TMat<T, 4, 4>> for TQuat<T>
where
    T: Float + NumAssign,
{
    fn from(m: TMat<T, 4, 4>) -> Self {
        let one = T::one();
        let half = one / (one + one);
        let quarter = half * half;

        let four_x_squared_minus_1 = m[0][0] - m[1][1] - m[2][2];
        let four_y_squared_minus_1 = m[1][1] - m[0][0] - m[2][2];
        let four_z_squared_minus_1 = m[2][2] - m[0][0] - m[1][1];
        let four_w_squared_minus_1 = m[0][0] + m[1][1] + m[2][2];

        let mut biggest_index = 0;
        let mut four_biggest_squared_minus_1 = four_w_squared_minus_1;
        if four_x_squared_minus_1 > four_biggest_squared_minus_1 {
            four_biggest_squared_minus_1 = four_x_squared_minus_1;
            biggest_index = 1;
        }
        if four_y_squared_minus_1 > four_biggest_squared_minus_1 {
            four_biggest_squared_minus_1 = four_y_squared_minus_1;
            biggest_index = 2;
        }
        if four_z_squared_minus_1 > four_biggest_squared_minus_1 {
            four_biggest_squared_minus_1 = four_z_squared_minus_1;
            biggest_index = 3;
        }

        let biggest_val = T::sqrt(four_biggest_squared_minus_1 + one) * half;
        let mult = quarter / biggest_val;

        match biggest_index {
            0 => Self::new(
                biggest_val,
                (m[1][2] - m[2][1]) * mult,
                (m[2][0] - m[0][2]) * mult,
                (m[0][1] - m[1][0]) * mult,
            ),
            1 => Self::new(
                (m[1][2] - m[2][1]) * mult,
                biggest_val,
                (m[0][1] + m[1][0]) * mult,
                (m[2][0] + m[0][2]) * mult,
            ),
            2 => Self::new(
                (m[2][0] - m[0][2]) * mult,
                (m[0][1] + m[1][0]) * mult,
                biggest_val,
                (m[1][2] + m[2][1]) * mult,
            ),
            3 => Self::new(
                (m[0][1] - m[1][0]) * mult,
                (m[2][0] + m[0][2]) * mult,
                (m[1][2] + m[2][1]) * mult,
                biggest_val,
            ),
            _ => Self::new(one, T::zero(), T::zero(), T::zero()),
        }
    }
}

impl<I, T> Index<I> for TQuat<T>
where
    I: std::slice::SliceIndex<[T]>,
    T: Float,
{
    type Output = I::Output;

    fn index(&self, index: I) -> &Self::Output {
        Index::index(&**self, index)
    }
}

impl<I, T> IndexMut<I> for TQuat<T>
where
    I: std::slice::SliceIndex<[T]>,
    T: Float,
{
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        IndexMut::index_mut(&mut **self, index)
    }
}

impl<T> Neg for TQuat<T>
where
    T: NumAssign + Float,
{
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self {
            components: [-self[0], -self[1], -self[2], -self[3]],
        }
    }
}

impl<T> Add for TQuat<T>
where
    T: NumAssign + Float,
{
    type Output = Self;

    fn add(mut self, rhs: Self) -> Self::Output {
        self += rhs;
        self
    }
}

impl<T> AddAssign for TQuat<T>
where
    T: NumAssign + Float,
{
    fn add_assign(&mut self, rhs: Self) {
        self.components[0] += rhs.components[0];
        self.components[1] += rhs.components[1];
        self.components[2] += rhs.components[2];
        self.components[3] += rhs.components[3];
    }
}

impl<T> Sub for TQuat<T>
where
    T: NumAssign + Float,
{
    type Output = Self;

    fn sub(mut self, rhs: Self) -> Self::Output {
        self -= rhs;
        self
    }
}

impl<T> SubAssign for TQuat<T>
where
    T: NumAssign + Float,
{
    fn sub_assign(&mut self, rhs: Self) {
        self.components[0] -= rhs.components[0];
        self.components[1] -= rhs.components[1];
        self.components[2] -= rhs.components[2];
        self.components[3] -= rhs.components[3];
    }
}

impl<T> Mul for TQuat<T>
where
    T: NumAssign + Float + Mul<TVec<Column, T, 3>, Output = TVec<Column, T, 3>>,
{
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        let lhs_ijk = TVec::<Column, T, 3>::new(self[0], self[1], self[2]);
        let rhs_ijk = TVec::<Column, T, 3>::new(rhs[0], rhs[1], rhs[2]);
        let ijk =
            self[3] * rhs_ijk + rhs[3] * lhs_ijk + TVec::<Column, T, 3>::cross(lhs_ijk, rhs_ijk);
        Self {
            components: [
                ijk[0],
                ijk[1],
                ijk[2],
                self[3] * rhs[3] - TVec::dot(lhs_ijk, rhs_ijk),
            ],
        }
    }
}

impl<T> Mul<T> for TQuat<T>
where
    T: NumAssign + Float,
{
    type Output = Self;

    fn mul(mut self, rhs: T) -> Self::Output {
        self *= rhs;
        self
    }
}

impl<T> MulAssign<T> for TQuat<T>
where
    T: NumAssign + Float,
{
    fn mul_assign(&mut self, rhs: T) {
        self[0] *= rhs;
        self[1] *= rhs;
        self[2] *= rhs;
        self[3] *= rhs;
    }
}

impl Mul<TQuat<f32>> for f32 {
    type Output = TQuat<f32>;

    fn mul(self, mut rhs: TQuat<f32>) -> Self::Output {
        rhs *= self;
        rhs
    }
}

impl Mul<TQuat<f64>> for f64 {
    type Output = TQuat<f64>;

    fn mul(self, mut rhs: TQuat<f64>) -> Self::Output {
        rhs *= self;
        rhs
    }
}

impl<T> Div<T> for TQuat<T>
where
    T: NumAssign + Float,
{
    type Output = Self;

    fn div(mut self, rhs: T) -> Self::Output {
        self /= rhs;
        self
    }
}

impl<T> DivAssign<T> for TQuat<T>
where
    T: NumAssign + Float,
{
    fn div_assign(&mut self, rhs: T) {
        self.components[0] /= rhs;
        self.components[1] /= rhs;
        self.components[2] /= rhs;
        self.components[3] /= rhs;
    }
}

impl<T> Deref for TQuat<T>
where
    T: Float,
{
    type Target = [T];

    fn deref(&self) -> &Self::Target {
        &self.components
    }
}

impl<T> DerefMut for TQuat<T>
where
    T: Float,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.components
    }
}

impl<T> AbsDiffEq for TQuat<T>
where
    T: AbsDiffEq + Float,
    T::Epsilon: Copy,
{
    type Epsilon = T::Epsilon;

    fn default_epsilon() -> Self::Epsilon {
        T::default_epsilon()
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        T::abs_diff_eq(&self[0], &other[0], epsilon)
            && T::abs_diff_eq(&self[1], &other[1], epsilon)
            && T::abs_diff_eq(&self[2], &other[2], epsilon)
            && T::abs_diff_eq(&self[3], &other[3], epsilon)
    }
}

impl<T> RelativeEq for TQuat<T>
where
    T: RelativeEq + Float,
    T::Epsilon: Copy,
{
    fn default_max_relative() -> Self::Epsilon {
        T::default_max_relative()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        T::relative_eq(&self[0], &other[0], epsilon, max_relative)
            && T::relative_eq(&self[1], &other[1], epsilon, max_relative)
            && T::relative_eq(&self[2], &other[2], epsilon, max_relative)
            && T::relative_eq(&self[3], &other[3], epsilon, max_relative)
    }
}

impl<T> Serialize for TQuat<T>
where
    T: Float + Serialize,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut sequence = serializer.serialize_seq(Some(4))?;
        for i in 0..4 {
            sequence.serialize_element(&self.components[i])?;
        }
        sequence.end()
    }
}

impl<'de, T> Deserialize<'de> for TQuat<T>
where
    T: Float + Copy + Deserialize<'de>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct ArrayVisitor<T> {
            marker: PhantomData<fn() -> [T; 4]>,
        }

        impl<T> ArrayVisitor<T>
        where
            T: Float,
        {
            fn new() -> Self {
                Self {
                    marker: Default::default(),
                }
            }
        }

        impl<'de, T> Visitor<'de> for ArrayVisitor<T>
        where
            T: Float + Copy + Deserialize<'de>,
        {
            type Value = [T; 4];

            fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
                write!(formatter, "[{}; 4]", std::any::type_name::<T>())
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: SeqAccess<'de>,
            {
                let mut array = [T::zero(); 4];

                for (i, component) in array.iter_mut().enumerate() {
                    if let Some(value) = seq.next_element()? {
                        *component = value;
                    } else {
                        return Err(A::Error::invalid_length(
                            i + 1,
                            &"expected array of length 4",
                        ));
                    }
                }

                Ok(array)
            }
        }

        Ok(Self::from(
            deserializer.deserialize_seq(ArrayVisitor::new())?,
        ))
    }
}

#[cfg(test)]
mod tests {
    use crate::math::{Mat4, Quat, Vec3};

    #[test]
    fn new() {
        let quat = Quat::new(1.0, 2.0, 3.0, 4.0);
        assert_relative_eq!(quat[3], 1.0);
        assert_relative_eq!(quat[0], 2.0);
        assert_relative_eq!(quat[1], 3.0);
        assert_relative_eq!(quat[2], 4.0);
    }

    #[test]
    fn from_angle_axis() {
        let quat =
            Quat::from_angle_axis(45.0f32.to_radians(), Vec3::new(1.0, 1.0, 1.0).normalize());
        assert_relative_eq!(
            quat,
            Quat::new(
                0.9238795325112867,
                0.22094238269039457,
                0.22094238269039457,
                0.22094238269039457
            )
        );
    }

    #[test]
    fn length2() {
        let quat = Quat::new(0.0, 0.0, 3.0, 4.0);
        assert_relative_eq!(quat.length2(), 25.0);
    }

    #[test]
    fn length() {
        let quat = Quat::new(0.0, 0.0, 3.0, 4.0);
        assert_relative_eq!(quat.length(), 5.0);
    }

    #[test]
    fn normalize() {
        let quat = Quat::new(1.0, 2.0, 3.0, 4.0).normalize();
        assert_relative_eq!(quat.length(), 1.0);
    }

    #[test]
    fn from_array() {
        let quat: Quat = [1.0, 2.0, 3.0, 4.0].into();
        assert_relative_eq!(quat, Quat::new(4.0, 1.0, 2.0, 3.0));
    }

    #[test]
    fn from_mat4() {
        let m = Mat4::new(
            0.5792280, -0.5792280, 0.5735765, 0.0, 0.8122618, 0.4694510, -0.3461886, 0.0,
            -0.0687439, 0.6664163, 0.7424039, 0.0, 0.0, 0.0, 0.0, 1.0,
        );
        let quat = Quat::from(m);
        assert_relative_eq!(
            quat,
            Quat::new(0.8353267, -0.30305654, -0.19223627, -0.41645074)
        );
    }

    #[test]
    fn index_mut() {
        let mut quat = Quat::new(4.0, 1.0, 2.0, 3.0);
        quat[1] = 42.0;
        assert_relative_eq!(quat, Quat::new(4.0, 1.0, 42.0, 3.0));
    }

    #[test]
    fn add() {
        let mut quat = Quat::new(1.0, 2.0, 3.0, 4.0);
        quat += Quat::new(2.0, 3.0, 4.0, 5.0);
        assert_relative_eq!(quat, Quat::new(3.0, 5.0, 7.0, 9.0));
        assert_relative_eq!(
            quat,
            Quat::new(1.0, 2.0, 3.0, 4.0) + Quat::new(2.0, 3.0, 4.0, 5.0)
        );
    }

    #[test]
    fn sub() {
        let mut quat = Quat::new(3.0, 5.0, 7.0, 9.0);
        quat -= Quat::new(2.0, 3.0, 4.0, 5.0);
        assert_relative_eq!(quat, Quat::new(1.0, 2.0, 3.0, 4.0));
        assert_relative_eq!(
            quat,
            Quat::new(3.0, 5.0, 7.0, 9.0) - Quat::new(2.0, 3.0, 4.0, 5.0)
        );
    }

    #[test]
    fn mul() {
        let q0 = Quat::new(1.0, 2.0, 3.0, 4.0);
        let q1 = Quat::new(2.0, 3.0, 4.0, 5.0);
        assert_relative_eq!(q0 * q1, Quat::new(-36.0, 6.0, 12.0, 12.0));
    }

    #[test]
    fn mul_scalar() {
        let mut quat = Quat::new(1.0, 2.0, 3.0, 4.0);
        quat *= 0.5;
        assert_relative_eq!(quat, Quat::new(0.5, 1.0, 1.5, 2.0));
        assert_relative_eq!(quat, Quat::new(1.0, 2.0, 3.0, 4.0) * 0.5);
        assert_relative_eq!(quat, 0.5 * Quat::new(1.0, 2.0, 3.0, 4.0));
    }

    #[test]
    fn div_scalar() {
        let mut quat = Quat::new(1.0, 2.0, 3.0, 4.0);
        quat /= 2.0;
        assert_relative_eq!(quat, Quat::new(0.5, 1.0, 1.5, 2.0));
        assert_relative_eq!(quat, Quat::new(1.0, 2.0, 3.0, 4.0) / 2.0);
    }
}
