use std::sync::Arc;

use approx::relative_eq;

use crate::animation::Pose;
use crate::core::Engine;
use crate::ecs::{Facade, Transform};
use crate::math::Vec2;

/// Animation blend space.
#[derive(Clone)]
pub struct BlendSpace {
    value_provider: Arc<dyn Fn(&dyn Engine, Facade) -> Vec2>,
    samples: Vec<Sample>,
    max: Vec2,
    min: Vec2,
}

impl BlendSpace {
    /// Starts building a new blend space.
    pub fn builder() -> BlendSpaceBuilder {
        BlendSpaceBuilder {
            value_provider: None,
            samples: vec![],
            max: None,
            min: None,
        }
    }
}

#[derive(Clone)]
struct Sample {
    pose: Arc<dyn Pose>,
    value: Vec2,
}

/// Type for building a [`BlendSpace`].
pub struct BlendSpaceBuilder {
    value_provider: Option<Arc<dyn Fn(&dyn Engine, Facade) -> Vec2>>,
    samples: Vec<Sample>,
    max: Option<Vec2>,
    min: Option<Vec2>,
}

impl BlendSpaceBuilder {
    /// Sets the value range.
    pub fn range(mut self, min: Vec2, max: Vec2) -> Self {
        self.min = Some(min);
        self.max = Some(max);
        self
    }

    /// Adds pose generation at a sample point.
    pub fn sample(mut self, value: Vec2, pose: impl Pose + 'static) -> Self {
        self.samples.push(Sample {
            pose: Arc::new(pose),
            value,
        });
        self
    }

    /// Sets the value provider.
    pub fn value_provider(
        mut self,
        value_provider: impl Fn(&dyn Engine, Facade) -> Vec2 + 'static,
    ) -> Self {
        self.value_provider = Some(Arc::new(value_provider));
        self
    }

    /// Builds the blend space.
    pub fn build(self) -> BlendSpace {
        BlendSpace {
            value_provider: self.value_provider.unwrap(),
            samples: self.samples,
            max: self.max.unwrap(),
            min: self.min.unwrap(),
        }
    }
}

impl Pose for BlendSpace {
    fn duration(&self, engine: &dyn Engine, entity: Facade) -> f32 {
        let value = self.value(engine, entity.clone());
        let (top_left, top_right, bottom_left, bottom_right) = self.samples(value);
        let blend_factor = Self::blend_factor(bottom_left.value, top_right.value, value);
        let x0 = Self::lerp(
            top_left.pose.duration(engine, entity.clone()),
            top_right.pose.duration(engine, entity.clone()),
            blend_factor[0],
        );
        let x1 = Self::lerp(
            bottom_left.pose.duration(engine, entity.clone()),
            bottom_right.pose.duration(engine, entity.clone()),
            blend_factor[0],
        );
        Self::lerp(x0, x1, blend_factor[1])
    }

    fn pose(
        &self,
        engine: &dyn Engine,
        entity: Facade,
        local_transform: Vec<Transform>,
        elapsed_time: f32,
    ) -> Vec<Transform> {
        let duration = self.duration(engine, entity.clone());
        let value = self.value(engine, entity.clone());
        let (top_left, top_right, bottom_left, bottom_right) = self.samples(value);
        let blend_factor = Self::blend_factor(bottom_left.value, top_right.value, value);
        let x0 = Self::blend_local_transforms(
            top_left.pose.pose(
                engine,
                entity.clone(),
                local_transform.clone(),
                elapsed_time * top_left.pose.duration(engine, entity.clone()) / duration,
            ),
            top_right.pose.pose(
                engine,
                entity.clone(),
                local_transform.clone(),
                elapsed_time * top_right.pose.duration(engine, entity.clone()) / duration,
            ),
            blend_factor[0],
        );
        let x1 = Self::blend_local_transforms(
            bottom_left.pose.pose(
                engine,
                entity.clone(),
                local_transform.clone(),
                elapsed_time * bottom_left.pose.duration(engine, entity.clone()) / duration,
            ),
            bottom_right.pose.pose(
                engine,
                entity.clone(),
                local_transform.clone(),
                elapsed_time * bottom_right.pose.duration(engine, entity.clone()) / duration,
            ),
            blend_factor[0],
        );
        Self::blend_local_transforms(x0, x1, blend_factor[1])
    }
}

impl BlendSpace {
    fn value(&self, engine: &dyn Engine, entity: Facade) -> Vec2 {
        let value = (self.value_provider)(engine, entity.clone());
        let value = Vec2::new(
            value[0].clamp(self.min[0], self.max[0]),
            value[1].clamp(self.min[1], self.max[1]),
        );
        value
    }

    fn samples(&self, value: Vec2) -> (&Sample, &Sample, &Sample, &Sample) {
        let top_left = self
            .samples
            .iter()
            .filter(|sample| {
                Self::relative_leq(sample.value[0], value[0])
                    && Self::relative_geq(sample.value[1], value[1])
            })
            .min_by(|a, b| {
                (a.value - value)
                    .length2()
                    .partial_cmp(&(b.value - value).length2())
                    .unwrap()
            })
            .unwrap();
        let top_right = self
            .samples
            .iter()
            .filter(|sample| {
                Self::relative_geq(sample.value[0], value[0])
                    && Self::relative_geq(sample.value[1], value[1])
            })
            .min_by(|a, b| {
                (a.value - value)
                    .length2()
                    .partial_cmp(&(b.value - value).length2())
                    .unwrap()
            })
            .unwrap();
        let bottom_left = self
            .samples
            .iter()
            .filter(|sample| {
                Self::relative_leq(sample.value[0], value[0])
                    && Self::relative_leq(sample.value[1], value[1])
            })
            .min_by(|a, b| {
                (a.value - value)
                    .length2()
                    .partial_cmp(&(b.value - value).length2())
                    .unwrap()
            })
            .unwrap();
        let bottom_right = self
            .samples
            .iter()
            .filter(|sample| {
                Self::relative_geq(sample.value[0], value[0])
                    && Self::relative_leq(sample.value[1], value[1])
            })
            .min_by(|a, b| {
                (a.value - value)
                    .length2()
                    .partial_cmp(&(b.value - value).length2())
                    .unwrap()
            })
            .unwrap();
        (top_left, top_right, bottom_left, bottom_right)
    }

    fn relative_leq(x: f32, y: f32) -> bool {
        relative_eq!(x, y) || x < y
    }

    fn relative_geq(x: f32, y: f32) -> bool {
        relative_eq!(x, y) || x > y
    }

    fn blend_factor(min: Vec2, max: Vec2, value: Vec2) -> Vec2 {
        let x = if relative_eq!(min[0], max[0]) {
            0.0
        } else {
            (value[0] - min[0]) / (max[0] - min[0])
        };
        let y = if relative_eq!(min[1], max[1]) {
            0.0
        } else {
            (value[1] - min[1]) / (max[1] - min[1])
        };
        Vec2::new(x, y)
    }

    fn lerp(a: f32, b: f32, c: f32) -> f32 {
        a + (b - a) * c
    }

    fn blend_local_transforms(
        mut first_local_transforms: Vec<Transform>,
        second_local_transforms: Vec<Transform>,
        blend_factor: f32,
    ) -> Vec<Transform> {
        for (first_local_transform, second_local_transform) in first_local_transforms
            .iter_mut()
            .zip(second_local_transforms)
        {
            first_local_transform.set_position(
                first_local_transform
                    .position()
                    .lerp(second_local_transform.position(), blend_factor),
            );
            let first_orientation = first_local_transform.orientation();
            let mut second_orientation = second_local_transform.orientation();
            if first_orientation.dot(second_orientation) < 0.0 {
                second_orientation = -second_orientation;
            }
            first_local_transform.set_orientation(
                first_orientation
                    .lerp(second_orientation, blend_factor)
                    .normalize(),
            );
            first_local_transform.set_scale(
                first_local_transform
                    .scale()
                    .lerp(second_local_transform.scale(), blend_factor),
            );
        }
        first_local_transforms
    }
}
