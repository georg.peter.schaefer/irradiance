use crate::core::Engine;
use crate::ecs::{Facade, Transform};

/// A trait for something that can generate a pose.
pub trait Pose {
    /// Returns the duration.
    fn duration(&self, engine: &dyn Engine, entity: Facade) -> f32;
    /// Calculates the pose for a point in time.
    fn pose(
        &self,
        engine: &dyn Engine,
        entity: Facade,
        local_transforms: Vec<Transform>,
        elapsed_time: f32,
    ) -> Vec<Transform>;
}
