//! Skeletal animation.

pub use animation::Animation;
pub use animation::AnimationCreationError;
pub use animation::Channel;
pub use animation::Sample;
pub use animation::Samples;
pub use animations::Animations;
pub use blend_space::BlendSpace;
pub use blend_space::BlendSpaceBuilder;
pub use graph::AnimationGraph;
pub use graph::AnimationGraphBuilder;
pub use graph::Node;
pub use pose::Pose;

mod animation;
mod animations;
mod blend_space;
mod graph;
mod pose;
