use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::path::Path;
use std::sync::Arc;

use crate::animation::Pose;
use gltf::animation::util::{ReadOutputs, Rotations};

use crate::asset::{Asset, AssetRef, Assets};
use crate::core::Engine;
use crate::ecs::{Facade, Transform};
use crate::gfx::GraphicsContext;
use crate::math::{Quat, Vec3};

/// An animation.
#[derive(Debug)]
pub struct Animation {
    /// Animation channels.
    pub channels: Vec<Channel>,
    /// End of the animation.
    pub end: f32,
    /// Start of the animation.
    pub start: f32,
}

impl Asset for Animation {
    type Error = AnimationCreationError;

    fn try_from_bytes(
        _assets: Arc<Assets>,
        _graphics_context: Arc<GraphicsContext>,
        _path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, Self::Error> {
        let (document, buffers, _) = gltf::import_slice(&bytes).unwrap();
        let joints = document
            .skins()
            .next()
            .ok_or(AnimationCreationError::MissingSkin)?
            .joints();
        let animation = document
            .animations()
            .next()
            .ok_or(AnimationCreationError::MissingAnimation)?;

        let mut channels = Vec::new();
        for channel in animation.channels() {
            if let Some(joint) = joints
                .clone()
                .position(|joint| joint.index() == channel.target().node().index())
            {
                let reader = channel.reader(|buffer| Some(&buffers[buffer.index()].0));
                let inputs = reader
                    .read_inputs()
                    .ok_or(AnimationCreationError::MissingInputs)?;
                let samples = match reader
                    .read_outputs()
                    .ok_or(AnimationCreationError::MissingOutputs)?
                {
                    ReadOutputs::Translations(translations) => {
                        let mut samples = Vec::new();
                        for (input, translation) in inputs.zip(translations) {
                            samples.push(Sample {
                                input,
                                output: Vec3::from(translation),
                            });
                        }
                        Samples::Translations(samples)
                    }
                    ReadOutputs::Rotations(Rotations::F32(rotations)) => {
                        let mut samples = Vec::new();
                        for (input, rotation) in inputs.zip(rotations) {
                            samples.push(Sample {
                                input,
                                output: Quat::from(rotation),
                            });
                        }
                        Samples::Rotations(samples)
                    }
                    ReadOutputs::Scales(scales) => {
                        let mut samples = Vec::new();
                        for (input, scale) in inputs.zip(scales) {
                            samples.push(Sample {
                                input,
                                output: Vec3::from(scale),
                            });
                        }
                        Samples::Scales(samples)
                    }
                    _ => {
                        return Err(AnimationCreationError::UnknownOutput);
                    }
                };
                channels.push(Channel { joint, samples });
            }
        }

        let start = channels
            .iter()
            .flat_map(|channel| channel.samples.inputs().into_iter())
            .min_by(|a, b| a.partial_cmp(b).expect("non NaN"))
            .expect("start time");
        let end = channels
            .iter()
            .flat_map(|channel| channel.samples.inputs().into_iter())
            .max_by(|a, b| a.partial_cmp(b).expect("non NaN"))
            .expect("end time");

        Ok(Self {
            channels,
            end,
            start,
        })
    }
}

/// Animation channel.
#[derive(Debug)]
pub struct Channel {
    /// Samples.
    pub samples: Samples,
    /// Index of the joint.
    pub joint: usize,
}

/// Animation samples.
#[derive(Debug)]
pub enum Samples {
    /// Translation samples.
    Translations(Vec<Sample<Vec3>>),
    /// Rotation samples.
    Rotations(Vec<Sample<Quat>>),
    /// Orientation samples.
    Scales(Vec<Sample<Vec3>>),
}

impl Samples {
    fn inputs(&self) -> Vec<f32> {
        match self {
            Samples::Translations(translations) => {
                translations.iter().map(|sample| sample.input).collect()
            }
            Samples::Rotations(rotations) => rotations.iter().map(|sample| sample.input).collect(),
            Samples::Scales(scales) => scales.iter().map(|sample| sample.input).collect(),
        }
    }
}

/// Animation sample.
#[derive(Debug)]
pub struct Sample<T: Debug> {
    /// Sample output.
    pub output: T,
    /// Sample input.
    pub input: f32,
}

/// Error that can occur while reading an animation.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum AnimationCreationError {
    /// Missing skin.
    MissingSkin,
    /// Missing animation.
    MissingAnimation,
    /// Missing inputs.
    MissingInputs,
    /// Missing outputs.
    MissingOutputs,
    /// Unknown output.
    UnknownOutput,
    /// Missing joint in skin for channel.
    UnknownChannel,
}

impl Display for AnimationCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            AnimationCreationError::MissingSkin => write!(f, "Missing skin"),
            AnimationCreationError::MissingAnimation => write!(f, "Missing animation"),
            AnimationCreationError::MissingInputs => write!(f, "Missing inputs"),
            AnimationCreationError::MissingOutputs => write!(f, "Missing outputs"),
            AnimationCreationError::UnknownOutput => write!(f, "Unknown output"),
            AnimationCreationError::UnknownChannel => {
                write!(f, "Missing joint in skin for channel")
            }
        }
    }
}

impl Error for AnimationCreationError {}

impl Pose for Animation {
    fn duration(&self, _engine: &dyn Engine, _entity: Facade) -> f32 {
        self.end - self.start
    }

    fn pose(
        &self,
        _engine: &dyn Engine,
        _entity: Facade,
        mut local_transforms: Vec<Transform>,
        elapsed_time: f32,
    ) -> Vec<Transform> {
        for channel in &self.channels {
            match &channel.samples {
                Samples::Translations(translations) => {
                    for i in 0..translations.len() - 1 {
                        if elapsed_time >= translations[i].input
                            && elapsed_time <= translations[i + 1].input
                        {
                            let blend_factor = (elapsed_time - translations[i].input)
                                / (translations[i + 1].input - translations[i].input);
                            local_transforms[channel.joint].set_position(
                                translations[i]
                                    .output
                                    .lerp(translations[i + 1].output, blend_factor),
                            );
                        }
                    }
                }
                Samples::Rotations(rotations) => {
                    for i in 0..rotations.len() - 1 {
                        if elapsed_time >= rotations[i].input
                            && elapsed_time <= rotations[i + 1].input
                        {
                            let blend_factor = (elapsed_time - rotations[i].input)
                                / (rotations[i + 1].input - rotations[i].input);
                            let a = rotations[i].output;
                            let mut b = rotations[i + 1].output;
                            if a.dot(b) < 0.0 {
                                b = -b;
                            }
                            local_transforms[channel.joint]
                                .set_orientation(a.lerp(b, blend_factor).normalize());
                        }
                    }
                }
                Samples::Scales(scales) => {
                    for i in 0..scales.len() - 1 {
                        if elapsed_time >= scales[i].input && elapsed_time <= scales[i + 1].input {
                            let blend_factor = (elapsed_time - scales[i].input)
                                / (scales[i + 1].input - scales[i].input);
                            local_transforms[channel.joint].set_scale(
                                scales[i].output.lerp(scales[i + 1].output, blend_factor),
                            );
                        }
                    }
                }
            }
        }
        local_transforms
    }
}

impl Pose for AssetRef<Animation> {
    fn duration(&self, engine: &dyn Engine, entity: Facade) -> f32 {
        self.unwrap().duration(engine, entity)
    }

    fn pose(
        &self,
        engine: &dyn Engine,
        entity: Facade,
        local_transform: Vec<Transform>,
        elapsed_time: f32,
    ) -> Vec<Transform> {
        self.unwrap()
            .pose(engine, entity, local_transform, elapsed_time)
    }
}
