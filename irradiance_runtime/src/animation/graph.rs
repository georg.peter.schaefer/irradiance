use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use std::sync::Arc;

use crate::animation::Pose;
use crate::core::Engine;
use crate::ecs::Facade;

/// An animation state machine.
#[derive(Clone, Debug)]
pub struct AnimationGraph {
    /// Nodes in the graph.
    pub nodes: HashMap<String, Node>,
    /// Elapsed time.
    pub elapsed_time: f32,
    /// Previous node.
    pub previous: String,
    /// Current node.
    pub current: String,
    /// Is the current node finished.
    pub finished: bool,
}

impl AnimationGraph {
    /// Starts building a new animation graph.
    pub fn builder() -> AnimationGraphBuilder {
        AnimationGraphBuilder {
            nodes: Default::default(),
            start: None,
        }
    }
}

unsafe impl Send for AnimationGraph {}
unsafe impl Sync for AnimationGraph {}

/// Type for building an animation graph.
pub struct AnimationGraphBuilder {
    nodes: HashMap<String, Node>,
    start: Option<String>,
}

impl AnimationGraphBuilder {
    /// Adds a node with a pose generator.
    pub fn node(
        mut self,
        name: impl Into<String>,
        root_animation: bool,
        pose: impl Pose + 'static,
    ) -> Self {
        let name = name.into();
        if self.start.is_none() {
            self.start = Some(name.clone());
        }
        self.nodes
            .insert(name.clone(), Node::new(name, root_animation, pose));
        self
    }

    /// Adds a transition between two nodes.
    pub fn transition(
        mut self,
        from: impl AsRef<str>,
        to: impl Into<String>,
        can_transition: impl Fn(&dyn Engine, Facade) -> bool + 'static,
    ) -> Self {
        let to = to.into();
        self.nodes
            .get_mut(from.as_ref())
            .expect("node")
            .transitions
            .insert(to, Arc::new(can_transition));
        self
    }

    /// Builds the animation graph.
    pub fn build(self) -> AnimationGraph {
        AnimationGraph {
            nodes: self.nodes,
            elapsed_time: 0.0,
            previous: "".to_string(),
            current: self.start.expect("start node"),
            finished: false,
        }
    }
}

/// A node in an animation graph.
#[derive(Clone)]
pub struct Node {
    /// Transitions from a node to others.
    pub transitions: HashMap<String, Arc<dyn Fn(&dyn Engine, Facade) -> bool>>,
    /// The pose generator.
    pub pose: Arc<dyn Pose>,
    /// Is this a root animation.
    pub root_animation: bool,
    /// The name of the node.
    pub name: String,
}

impl Node {
    fn new(name: String, root_animation: bool, pose: impl Pose + 'static) -> Self {
        Self {
            name,
            root_animation,
            pose: Arc::new(pose),
            transitions: Default::default(),
        }
    }
}

impl Debug for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Node")
    }
}
