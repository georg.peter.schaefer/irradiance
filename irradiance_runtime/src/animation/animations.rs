use crate::core::{Engine, Time};
use crate::ecs::Entities;
use crate::gfx::buffer::{Buffer, BufferUsage};
use crate::gfx::GraphicsContext;
use crate::math::{Mat4, Vec3};
use crate::rendering::skinned_mesh::Joint;
use crate::rendering::{AnimatedMesh, SkinnedMesh};

/// Animation system.
#[derive(Default)]
pub struct Animations;

impl Animations {
    /// Updates all animations.
    pub fn update(&self, engine: &dyn Engine, time: Time, entities: &Entities) {
        for (entity, mut animated_mesh) in entities.components::<(&mut AnimatedMesh,)>() {
            if let Some(skinned_mesh) = animated_mesh.mesh().clone().ok() {
                Self::create_joint_buffer_if_required(
                    engine.graphics_context(),
                    &mut animated_mesh,
                );

                let local_transforms = skinned_mesh
                    .joints()
                    .iter()
                    .map(|joint| joint.transform)
                    .collect::<Vec<_>>();

                if animated_mesh.root_translation.is_none() {
                    let (root, _) = Self::root(&skinned_mesh);
                    animated_mesh.root_translation = Some(local_transforms[root].position());
                }

                let local_transforms = animated_mesh
                    .animation_graph
                    .as_mut()
                    .map(|animation_graph| {
                        if let Some(to) = animation_graph.nodes[&animation_graph.current]
                            .transitions
                            .iter()
                            .find(|(_, can_transition)| can_transition(engine, entity.clone()))
                            .map(|(to, _)| to.clone())
                        {
                            log::info!("from: {} to: {}", animation_graph.current, to);
                            animation_graph.previous = animation_graph.current.clone();
                            animation_graph.current = to;
                            animation_graph.elapsed_time = 0.0;
                        }

                        let pose = animation_graph.nodes[&animation_graph.current].pose.clone();

                        animation_graph.finished = false;
                        animation_graph.elapsed_time += time.delta();
                        if animation_graph.elapsed_time + time.delta()
                            >= pose.duration(engine, entity.clone())
                        {
                            animation_graph.finished = true;
                        }
                        if animation_graph.elapsed_time > pose.duration(engine, entity.clone()) {
                            animation_graph.elapsed_time -= pose.duration(engine, entity.clone());
                        }

                        pose.pose(
                            engine,
                            entity.clone(),
                            local_transforms.clone(),
                            animation_graph.elapsed_time,
                        )
                    })
                    .unwrap_or(local_transforms)
                    .into_iter()
                    .map(Mat4::from)
                    .collect();

                animated_mesh.last_root_translation = animated_mesh.root_translation;
                animated_mesh.root_translation =
                    Some(Self::root_translation(&animated_mesh, &local_transforms));

                let root_animation = animated_mesh
                    .animation_graph
                    .as_ref()
                    .map(|animation_graph| {
                        animation_graph.nodes[&animation_graph.current].root_animation
                    })
                    .unwrap_or_default();

                Self::update_joint_buffer(&animated_mesh, local_transforms, root_animation);
            }
        }
        unsafe { entities.commit() };
    }

    fn create_joint_buffer_if_required(
        graphics_context: &GraphicsContext,
        animated_mesh: &mut AnimatedMesh,
    ) {
        if Self::requires_joint_buffer_creation(animated_mesh) {
            animated_mesh.set_joint_buffer(
                Buffer::from_data(
                    graphics_context.device().clone(),
                    std::iter::repeat(Mat4::identity())
                        .take(animated_mesh.mesh().unwrap().joints().len())
                        .collect::<Vec<_>>(),
                )
                .usage(BufferUsage {
                    storage_buffer: true,
                    ..Default::default()
                })
                .build()
                .expect("joint buffer"),
            )
        }
    }

    fn requires_joint_buffer_creation(animated_mesh: &mut AnimatedMesh) -> bool {
        animated_mesh.joint_buffer().is_none()
            || animated_mesh
                .joint_buffer()
                .map(|joint_buffer| {
                    joint_buffer.size() as usize / std::mem::size_of::<Mat4>()
                        != animated_mesh.mesh().unwrap().joints().len()
                })
                .unwrap_or_default()
    }

    fn update_joint_buffer(
        animated_mesh: &AnimatedMesh,
        local_transforms: Vec<Mat4>,
        root_animation: bool,
    ) {
        let mesh = animated_mesh.mesh().unwrap();
        let root = Self::root(mesh);
        let mut transforms = vec![Mat4::identity(); mesh.joints().len()];
        let mut current_transforms = vec![Mat4::identity(); mesh.joints().len()];
        let mut stack = vec![root];
        while let Some((index, joint)) = stack.pop() {
            let parent_transform = joint
                .parent
                .map(|index| transforms[index])
                .unwrap_or_default();
            if root_animation && index == root.0 {
                let mut transform = local_transforms[index];
                transform[3] = joint.transform.position().into();
                transforms[index] = transform;
            } else {
                transforms[index] = parent_transform * local_transforms[index];
            }
            current_transforms[index] = transforms[index] * joint.inverse_bind_matrix;
            stack.extend(
                mesh.joints()
                    .iter()
                    .enumerate()
                    .filter(|(_, child)| child.parent == Some(index)),
            );
        }
        animated_mesh
            .joint_buffer()
            .unwrap()
            .write(current_transforms);
    }

    fn root_translation(animated_mesh: &AnimatedMesh, local_transforms: &Vec<Mat4>) -> Vec3 {
        let mesh = animated_mesh.mesh().unwrap();
        let (root, _) = Self::root(mesh);
        local_transforms[root][3].into()
    }

    fn root(mesh: &SkinnedMesh) -> (usize, &Joint) {
        mesh.joints()
            .iter()
            .enumerate()
            .find(|(_, joint)| joint.parent.is_none())
            .unwrap()
    }
}
