use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

use crate::math::Vec3;
use crate::physics::Shape;
use crate::Component;

/// Rigid body component.
#[derive(Clone, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct RigidBody {
    /// Offset of the collider shape in local space.
    pub offset: Vec3,
    /// Is the rigid body a sensor.
    #[serde(default)]
    pub sensor: bool,
    /// Collider shape.
    pub shape: Shape,
    /// Type of the rigid body.
    pub body_type: RigidBodyType,
}

impl RigidBody {
    /// Creates a new rigid body.
    pub fn new(shape: Shape) -> Self {
        Self {
            offset: Default::default(),
            sensor: false,
            shape,
            body_type: Default::default(),
        }
    }
}

/// The type of a rigid body, which indicates the way it is affected by forces.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Default, Serialize, Deserialize)]
#[serde(tag = "type_name")]
pub enum RigidBodyType {
    /// Can be affected by all external forces.
    #[default]
    Dynamic,
    /// Cannot be affected by external forces.
    Fixed,
    /// Cannot be affected by external forces but can be controlled by the user at the position
    /// level. Other [`Dynamic`] rigid bodies can be affected by a [`Kinematic`] rigid body.
    Kinematic,
}

impl Display for RigidBodyType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            RigidBodyType::Dynamic => write!(f, "Dynamic"),
            RigidBodyType::Fixed => write!(f, "Fixed"),
            RigidBodyType::Kinematic => write!(f, "Kinematic"),
        }
    }
}

impl From<RigidBodyType> for rapier3d::prelude::RigidBodyType {
    fn from(rigid_body_type: RigidBodyType) -> Self {
        match rigid_body_type {
            RigidBodyType::Dynamic => Self::Dynamic,
            RigidBodyType::Fixed => Self::Fixed,
            RigidBodyType::Kinematic => Self::KinematicPositionBased,
        }
    }
}
