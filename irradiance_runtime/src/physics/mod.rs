//! Physics integration.

pub use private::Hit;
pub use private::Physics;
pub use rigid_body::RigidBody;
pub use rigid_body::RigidBodyType;
pub use shape::Shape;

mod physics_debug_pass;
mod private;
mod rigid_body;
pub mod shape;
