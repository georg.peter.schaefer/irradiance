use std::sync::Arc;

use rapier3d::math::{Point, Real};
use rapier3d::pipeline::DebugRenderObject;
use rapier3d::prelude::DebugRenderBackend;

use crate::ecs::{Entities, Transform};
use crate::gfx::buffer::{Buffer, BufferUsage};
use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::device::Device;
use crate::gfx::image::{Format, ImageAspects, ImageLayout, ImageSubresourceRange, ImageView};
use crate::gfx::pipeline::{
    ColorBlendAttachmentState, CompareOp, CullMode, DynamicState, FrontFace, GraphicsPipeline,
    PipelineLayout, PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
};
use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::GraphicsContext;
use crate::gfx::{Extent2D, GfxError, Rect2D};
use crate::math::{Mat4, Vec3, Vec4};
use crate::physics::physics_debug_pass::shaders::PushConstants;
use crate::rendering::pbr::GBuffer;
use crate::rendering::Camera;

pub struct PhysicsDebugPass {
    vertices: Vec<Vertex>,
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl PhysicsDebugPass {
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let pipeline_layout = Self::create_pipeline_layout(graphics_context.device().clone())?;
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            pipeline_layout.clone(),
            graphics_context.swapchain().unwrap().image_format(),
        )?;

        Ok(Self {
            graphics_context,
            pipeline_layout,
            graphics_pipeline,
            vertices: Default::default(),
        })
    }

    fn create_pipeline_layout(device: Arc<Device>) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
        format: Format,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .attribute(1, 0, Format::R32G32B32A32SFloat, 12)
                    .build(),
            )
            .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::LineList)
            .line_width(2.0)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::None)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::LessOrEqual)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(format)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    pub fn draw(
        &mut self,
        builder: &mut CommandBufferBuilder,
        g_buffer: &GBuffer,
        final_image: Arc<ImageView>,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Physics Debug Pass", Vec4::new(0.5, 0.5, 1.0, 1.0));

        if let Some((Some(camera), Some(camera_transform))) = entities
            .active::<Camera>()
            .map(|camera| (camera.get::<Camera>(), camera.get::<Transform>()))
        {
            let vertex_count = self.vertices.len() as u32;
            if vertex_count > 0 {
                builder.pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(g_buffer.depth_buffer().image().clone())
                                .src_stage_mask(PipelineStages {
                                    early_fragment_tests: true,
                                    ..Default::default()
                                })
                                .src_access_mask(AccessMask {
                                    depth_stencil_attachment_read: true,
                                    depth_stencil_attachment_write: true,
                                    ..Default::default()
                                })
                                .dst_stage_mask(PipelineStages {
                                    early_fragment_tests: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    depth_stencil_attachment_read: true,
                                    depth_stencil_attachment_write: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::DepthAttachmentOptimal)
                                .new_layout(ImageLayout::DepthAttachmentOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            depth: true,
                                            ..Default::default()
                                        })
                                        .build(),
                                )
                                .build(),
                        )
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(final_image.image().clone())
                                .src_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .src_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .dst_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::ColorAttachmentOptimal)
                                .new_layout(ImageLayout::ColorAttachmentOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                );
                let vertex_buffer = Buffer::from_data(
                    self.graphics_context.device().clone(),
                    self.vertices.drain(0..).collect(),
                )
                .usage(BufferUsage {
                    vertex_buffer: true,
                    ..Default::default()
                })
                .build()?;

                builder
                    .bind_graphics_pipeline(self.graphics_pipeline.clone())
                    .bind_vertex_buffer(vertex_buffer)
                    .push_constants(
                        self.pipeline_layout.clone(),
                        ShaderStages {
                            vertex: true,
                            ..Default::default()
                        },
                        0,
                        PushConstants {
                            projection: camera.projection(
                                g_buffer.extent().width as f32 / g_buffer.extent().height as f32,
                            ),
                            view: Mat4::from(*camera_transform).inverse(),
                        },
                    )
                    .set_viewport(
                        0,
                        vec![Viewport {
                            x: 0.0,
                            y: 0.0,
                            width: g_buffer.extent().width as _,
                            height: g_buffer.extent().height as _,
                            min_depth: 0.0,
                            max_depth: 1.0,
                        }],
                    )
                    .set_scissor(
                        0,
                        vec![Rect2D {
                            offset: Default::default(),
                            extent: Extent2D {
                                width: g_buffer.extent().width,
                                height: g_buffer.extent().height,
                            },
                        }],
                    )
                    .begin_rendering(self.create_render_pass(g_buffer, final_image))
                    .draw(vertex_count, 1, 0, 0)
                    .end_rendering();
            }
        }

        builder.end_label();

        Ok(())
    }

    fn create_render_pass(&self, g_buffer: &GBuffer, final_image: Arc<ImageView>) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: final_image.image().extent().into(),
            })
            .color_attachment(
                Attachment::builder(final_image)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .depth_attachment(
                Attachment::builder(g_buffer.depth_buffer().clone())
                    .image_layout(ImageLayout::DepthAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .build()
    }
}

impl DebugRenderBackend for PhysicsDebugPass {
    fn draw_line(
        &mut self,
        _object: DebugRenderObject,
        a: Point<Real>,
        b: Point<Real>,
        color: [f32; 4],
    ) {
        self.vertices.push(Vertex {
            position: Vec3::new(a.x, a.y, a.z),
            color: Vec4::new(color[0], color[1], color[2], color[3]),
        });
        self.vertices.push(Vertex {
            position: Vec3::new(b.x, b.y, b.z),
            color: Vec4::new(color[0], color[1], color[2], color[3]),
        });
    }
}

#[allow(unused)]
#[repr(C)]
struct Vertex {
    position: Vec3,
    color: Vec4,
}

mod shaders {
    use crate::math::Mat4;

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub projection: Mat4,
        pub view: Mat4,
    }
}

mod vertex_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/physics/debug.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod fragment_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/physics/debug.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
