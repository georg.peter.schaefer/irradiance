//! Collision shapes.

use std::fmt::{Display, Formatter};
use std::path::PathBuf;
use std::sync::Arc;

use approx::relative_eq;
use rapier3d::prelude::*;
use serde::Deserialize;
use serde::Serialize;

use crate::asset::Assets;
use crate::gfx::GraphicsContext;
use crate::math::Vec3;

/// Collider shapes.
#[derive(Clone, Serialize, Deserialize)]
#[serde(tag = "type_name")]
pub enum Shape {
    /// Ball shape.
    Ball(Ball),
    /// Cuboid shape.
    Cuboid(Cuboid),
    /// Cylinder shape.
    Cylinder(Cylinder),
    /// Capsule shape.
    Capsule(Capsule),
    /// Mesh shape.
    Mesh(Mesh),
}

impl Shape {
    /// Creates a ball shape.
    pub fn ball(radius: f32) -> Self {
        Self::Ball(Ball::new(radius))
    }

    /// Creates a cuboids shape.
    pub fn cuboid(half_extent: Vec3) -> Self {
        Self::Cuboid(Cuboid::new(half_extent))
    }

    /// Creates a cylinder.
    pub fn cylinder(half_height: f32, radius: f32) -> Self {
        Self::Cylinder(Cylinder::new(half_height, radius))
    }

    /// Creates a capsule.
    pub fn capsule(half_height: f32, radius: f32) -> Self {
        Self::Capsule(Capsule::new(half_height, radius))
    }

    /// Creates a mesh.
    pub fn mesh(path: PathBuf) -> Self {
        Self::Mesh(Mesh::new(path))
    }

    /// Constructs a shared shape.
    pub fn into_shared_shape(
        &self,
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
    ) -> SharedShape {
        match self {
            Shape::Ball(ball) => SharedShape::ball(ball.radius),
            Shape::Cuboid(cuboid) => SharedShape::cuboid(
                cuboid.half_extent[0],
                cuboid.half_extent[1],
                cuboid.half_extent[2],
            ),
            Shape::Cylinder(cylinder) => {
                SharedShape::cylinder(cylinder.half_height, cylinder.radius)
            }
            Shape::Capsule(capsule) => SharedShape::capsule_y(capsule.half_height, capsule.radius),
            Shape::Mesh(mesh) => {
                let mesh = assets
                    .read::<crate::rendering::Mesh, _>(graphics_context, &mesh.path)
                    .unwrap();
                SharedShape::trimesh(
                    mesh.unwrap()
                        .vertices()
                        .iter()
                        .map(|vertex| {
                            Point::new(vertex.position[0], vertex.position[1], vertex.position[2])
                        })
                        .collect(),
                    mesh.unwrap()
                        .indices()
                        .chunks(3)
                        .map(|indices| [indices[0], indices[1], indices[2]])
                        .collect(),
                )
            }
        }
    }
}

impl PartialEq for Shape {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Shape::Ball(_), Shape::Ball(_)) => true,
            (Shape::Cuboid(_), Shape::Cuboid(_)) => true,
            (Shape::Cylinder(_), Shape::Cylinder(_)) => true,
            (Shape::Capsule(_), Shape::Capsule(_)) => true,
            (Shape::Mesh(_), Shape::Mesh(_)) => true,
            _ => false,
        }
    }
}

impl PartialEq<&SharedShape> for &Shape {
    fn eq(&self, other: &&SharedShape) -> bool {
        match (self, other.as_typed_shape()) {
            (Shape::Ball(ball), TypedShape::Ball(other)) => relative_eq!(ball.radius, other.radius),
            (Shape::Cuboid(cuboid), TypedShape::Cuboid(other)) => relative_eq!(
                cuboid.half_extent,
                Vec3::new(
                    other.half_extents.x,
                    other.half_extents.y,
                    other.half_extents.z,
                )
            ),
            (Shape::Cylinder(cylinder), TypedShape::Cylinder(other)) => {
                relative_eq!(cylinder.half_height, other.half_height)
                    && relative_eq!(cylinder.radius, other.radius)
            }
            (Shape::Capsule(capsule), TypedShape::Capsule(other)) => {
                relative_eq!(capsule.half_height, other.segment.length() * 0.5)
                    && relative_eq!(capsule.radius, other.radius)
            }
            (Shape::Mesh(_), TypedShape::TriMesh(_)) => true,
            _ => false,
        }
    }
}

impl PartialEq<&Shape> for &SharedShape {
    fn eq(&self, other: &&Shape) -> bool {
        (&other).eq(&self)
    }
}

impl Display for Shape {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Shape::Ball(_) => write!(f, "Ball"),
            Shape::Cuboid(_) => write!(f, "Cuboid"),
            Shape::Cylinder(_) => write!(f, "Cylinder"),
            Shape::Capsule(_) => write!(f, "Capsule"),
            Shape::Mesh(_) => write!(f, "Mesh"),
        }
    }
}

/// Ball shape.
#[derive(Copy, Clone, Serialize, Deserialize)]
pub struct Ball {
    /// Radius.
    pub radius: f32,
}

impl Ball {
    /// Creates a new ball.
    pub fn new(radius: f32) -> Self {
        Self { radius }
    }
}

impl Default for Ball {
    fn default() -> Self {
        Self { radius: 1.0 }
    }
}

/// Cuboid shape.
#[derive(Copy, Clone, Serialize, Deserialize)]
pub struct Cuboid {
    /// Half extent.
    pub half_extent: Vec3,
}

impl Cuboid {
    /// Creates a new cuboid shape.
    pub fn new(half_extent: Vec3) -> Self {
        Self { half_extent }
    }
}

impl Default for Cuboid {
    fn default() -> Self {
        Self {
            half_extent: Vec3::new(1.0, 1.0, 1.0),
        }
    }
}

/// Cylinder shape.
#[derive(Copy, Clone, Serialize, Deserialize)]
pub struct Cylinder {
    /// Half height.
    pub half_height: f32,
    /// Radius.
    pub radius: f32,
}

impl Cylinder {
    /// Creates a new cylinder.
    pub fn new(half_height: f32, radius: f32) -> Self {
        Self {
            half_height,
            radius,
        }
    }
}

impl Default for Cylinder {
    fn default() -> Self {
        Self {
            half_height: 1.0,
            radius: 1.0,
        }
    }
}

/// Capsule shape.
#[derive(Copy, Clone, Serialize, Deserialize)]
pub struct Capsule {
    /// Half height.
    pub half_height: f32,
    /// Radius.
    pub radius: f32,
}

impl Capsule {
    /// Creates a new capsule.
    pub fn new(half_height: f32, radius: f32) -> Self {
        Self {
            half_height,
            radius,
        }
    }
}

impl Default for Capsule {
    fn default() -> Self {
        Self {
            half_height: 1.0,
            radius: 1.0,
        }
    }
}

/// Mesh shape.
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct Mesh {
    /// Mesh path.
    pub path: PathBuf,
}

impl Mesh {
    /// Creates a new mesh.
    pub fn new(path: PathBuf) -> Self {
        Self { path }
    }
}
