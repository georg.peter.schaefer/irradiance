use std::collections::HashMap;
use std::sync::Arc;

use rapier3d::na::Vector3;
use rapier3d::parry::bounding_volume::BoundingVolume;
use rapier3d::parry::query::{DefaultQueryDispatcher, PersistentQueryDispatcher};
use rapier3d::prelude::*;

use crate::asset::Assets;
use crate::core::Config;
use crate::ecs::{Entities, Facade, Transform};
use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::image::ImageView;
use crate::gfx::GfxError;
use crate::gfx::GraphicsContext;
use crate::math::{Quat, Vec2, Vec3};
use crate::physics::physics_debug_pass::PhysicsDebugPass;
use crate::rendering::pbr::BufPbrPipeline;

/// Physics system.
pub struct Physics {
    debug_render_pipeline: DebugRenderPipeline,
    physics_debug_pass: PhysicsDebugPass,
    rigid_bodies: HashMap<String, RigidBodyHandle>,
    event_handler: (),
    physics_hooks: (),
    ccd_solver: CCDSolver,
    multibody_joint_set: MultibodyJointSet,
    impulse_joint_set: ImpulseJointSet,
    collider_set: ColliderSet,
    rigid_body_set: RigidBodySet,
    narrow_phase: NarrowPhase,
    broad_phase: BroadPhase,
    island_manager: IslandManager,
    integration_parameters: IntegrationParameters,
    gravity: Vector3<f32>,
    query_pipeline: QueryPipeline,
    physics_pipeline: PhysicsPipeline,
    assets: Arc<Assets>,
    graphics_context: Arc<GraphicsContext>,
}

impl Physics {
    /// Creates a new physics system
    pub fn new(graphics_context: Arc<GraphicsContext>, assets: Arc<Assets>) -> Self {
        Self {
            debug_render_pipeline: DebugRenderPipeline::new(
                Default::default(),
                DebugRenderMode::default() | DebugRenderMode::CONTACTS,
            ),
            physics_debug_pass: PhysicsDebugPass::new(graphics_context.clone()).unwrap(),
            rigid_bodies: Default::default(),
            event_handler: (),
            physics_hooks: (),
            ccd_solver: CCDSolver::new(),
            multibody_joint_set: MultibodyJointSet::new(),
            impulse_joint_set: ImpulseJointSet::new(),
            collider_set: ColliderSet::new(),
            rigid_body_set: RigidBodySet::new(),
            narrow_phase: NarrowPhase::new(),
            broad_phase: BroadPhase::new(),
            island_manager: IslandManager::new(),
            integration_parameters: Default::default(),
            gravity: vector![0.0, -9.81, 0.0],
            query_pipeline: QueryPipeline::new(),
            physics_pipeline: PhysicsPipeline::new(),
            assets,
            graphics_context,
        }
    }

    /// Updates the physics simulation.
    pub fn update(&mut self, entities: &Entities) {
        self.housekeeping(entities);
        self.synchronize_kinematic_rigid_bodies(entities);
        self.physics_pipeline.step(
            &self.gravity,
            &self.integration_parameters,
            &mut self.island_manager,
            &mut self.broad_phase,
            &mut self.narrow_phase,
            &mut self.rigid_body_set,
            &mut self.collider_set,
            &mut self.impulse_joint_set,
            &mut self.multibody_joint_set,
            &mut self.ccd_solver,
            &self.physics_hooks,
            &self.event_handler,
        );
        self.query_pipeline.update(
            &self.island_manager,
            &self.rigid_body_set,
            &self.collider_set,
        );
        self.synchronize_physics_scene(entities);
    }

    /// Updates the physics system without stepping the simulation.
    pub fn update_no_step(&mut self, entities: &Entities) {
        self.housekeeping(entities);
        self.rigid_body_set
            .propagate_modified_body_positions_to_colliders(&mut self.collider_set);
        self.synchronize_scene_physics(entities);
    }

    fn housekeeping(&mut self, entities: &Entities) {
        self.retain_rigid_bodies(entities);
        self.insert_or_update_rigid_bodies(entities);
    }

    fn retain_rigid_bodies(&mut self, entities: &Entities) {
        self.rigid_bodies.retain(|entity, handle| {
            if entities.has::<super::RigidBody>(entity) {
                true
            } else {
                self.rigid_body_set.remove(
                    *handle,
                    &mut self.island_manager,
                    &mut self.collider_set,
                    &mut self.impulse_joint_set,
                    &mut self.multibody_joint_set,
                    true,
                );
                false
            }
        });
    }

    fn insert_or_update_rigid_bodies(&mut self, entities: &Entities) {
        for (entity, rigid_body, transform) in
            entities.components::<(&super::RigidBody, &Transform)>()
        {
            if self.rigid_bodies.contains_key(entity.id()) {
                self.update_rigid_body(&entity, &rigid_body);
            } else {
                self.insert_rigid_body(&entity, &rigid_body, &transform);
            }
        }
    }

    fn update_rigid_body(&mut self, entity: &Facade, rigid_body: &super::RigidBody) {
        let inner_rigid_body = &mut self.rigid_body_set[self.rigid_bodies[entity.id()]];
        let inner_collider = &mut self.collider_set[inner_rigid_body.colliders()[0]];

        if RigidBodyType::from(rigid_body.body_type) != inner_rigid_body.body_type() {
            inner_rigid_body.set_body_type(rigid_body.body_type.into());
        }
        if &rigid_body.shape != inner_collider.shared_shape() {
            inner_collider.set_shape(
                rigid_body
                    .shape
                    .into_shared_shape(self.graphics_context.clone(), self.assets.clone()),
            );
        }
        if rigid_body.sensor != inner_collider.is_sensor() {
            inner_collider.set_sensor(rigid_body.sensor);
        }
    }

    fn insert_rigid_body(
        &mut self,
        entity: &Facade,
        rigid_body: &super::RigidBody,
        transform: &Transform,
    ) {
        let handle = self.rigid_body_set.insert(
            RigidBodyBuilder::new(rigid_body.body_type.into()).position((*transform).into()),
        );
        self.collider_set.insert_with_parent(
            ColliderBuilder::new(
                rigid_body
                    .shape
                    .into_shared_shape(self.graphics_context.clone(), self.assets.clone()),
            )
            .sensor(rigid_body.sensor)
            .active_collision_types(ActiveCollisionTypes::all())
            .position(rigid_body.offset.into()),
            handle,
            &mut self.rigid_body_set,
        );
        self.rigid_bodies.insert(entity.id().to_string(), handle);
    }

    fn synchronize_kinematic_rigid_bodies(&mut self, entities: &Entities) {
        for (entity, transform, _) in entities
            .components::<(&Transform, &super::RigidBody)>()
            .filter(|(_, _, rigid_body)| rigid_body.body_type == super::RigidBodyType::Kinematic)
        {
            let inner_rigid_body = &mut self.rigid_body_set[self.rigid_bodies[entity.id()]];
            inner_rigid_body.set_next_kinematic_position((*transform).into());
        }
    }

    fn synchronize_physics_scene(&mut self, entities: &Entities) {
        for (entity, mut transform, _) in entities
            .components::<(&mut Transform, &super::RigidBody)>()
            .filter(|(_, _, rigid_body)| rigid_body.body_type == super::RigidBodyType::Dynamic)
        {
            let inner_rigid_body = &self.rigid_body_set[self.rigid_bodies[entity.id()]];
            transform.set_position(inner_rigid_body.position().into());
            transform.set_orientation(inner_rigid_body.position().into());
        }
    }

    fn synchronize_scene_physics(&mut self, entities: &Entities) {
        for (entity, transform, rigid_body) in
            entities.components::<(&Transform, &super::RigidBody)>()
        {
            let inner_rigid_body = &mut self.rigid_body_set[self.rigid_bodies[entity.id()]];
            let inner_collider = &mut self.collider_set[inner_rigid_body.colliders()[0]];
            inner_rigid_body.set_position((*transform).into(), true);
            let mut position = *inner_rigid_body.position();
            position.append_translation_mut(
                &position
                    .transform_vector(&vector![
                        rigid_body.offset[0],
                        rigid_body.offset[1],
                        rigid_body.offset[2]
                    ])
                    .into(),
            );
            inner_collider.set_position(position);
        }
    }

    /// Draws the physics scene.
    pub fn draw(
        &mut self,
        config: Config,
        buf_pbr_pipeline: Arc<BufPbrPipeline>,
        builder: &mut CommandBufferBuilder,
        final_image: Arc<ImageView>,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        if config.physics_debug {
            self.debug_render_pipeline.render(
                &mut self.physics_debug_pass,
                &self.rigid_body_set,
                &self.collider_set,
                &self.impulse_joint_set,
                &self.multibody_joint_set,
                &self.narrow_phase,
            );
            self.physics_debug_pass.draw(
                builder,
                buf_pbr_pipeline.g_buffer(),
                final_image,
                entities,
            )?;
        }

        Ok(())
    }

    /// Returns the dimensions of a rigid body.
    pub fn dimensions(&self, entity: &Facade) -> Option<Vec2> {
        self.rigid_bodies
            .get(entity.id())
            .map(|handle| &self.collider_set[self.rigid_body_set[*handle].colliders()[0]])
            .map(|collider| collider.compute_aabb().extents())
            .map(|extent| {
                let up_extent = Vector::<Real>::new(0.0, 1.0, 0.0).dot(&extent);
                let side_extent = (extent - Vector::<Real>::new(0.0, 1.0, 0.0) * up_extent).norm();
                Vec2::new(side_extent, up_extent)
            })
    }

    /// Casts the shape of an entity into the scene.
    pub fn cast_shape(
        &self,
        position: Vec3,
        orientation: Quat,
        shape: super::Shape,
        direction: Vec3,
        max_toi: f32,
        exclude_entity: Option<&Facade>,
    ) -> Option<Hit> {
        let shape = shape.into_shared_shape(self.graphics_context.clone(), self.assets.clone());
        self.query_pipeline
            .cast_shape(
                &self.rigid_body_set,
                &self.collider_set,
                &Transform::new(position, orientation, Vec3::default()).into(),
                &vector![direction[0], direction[1], direction[2]],
                shape.as_ref(),
                max_toi,
                false,
                self.query_filter(exclude_entity),
            )
            .map(|(handle, hit)| Hit {
                normal2: Vec3::new(hit.normal2[0], hit.normal2[1], hit.normal2[2]),
                normal1: Vec3::new(hit.normal1[0], hit.normal1[1], hit.normal1[2]),
                witness2: Vec3::new(hit.witness2[0], hit.witness2[1], hit.witness2[2]),
                witness1: Vec3::new(hit.witness1[0], hit.witness1[1], hit.witness1[2]),
                toi: hit.toi,
                entity: self.entity(handle),
            })
    }

    /// Casts a ray into the scene.
    pub fn cast_ray(
        &self,
        position: Vec3,
        direction: Vec3,
        max_toi: f32,
        exclude_entity: Option<&Facade>,
    ) -> Option<(String, f32)> {
        let ray = Ray::new(
            Point::new(position[0], position[1], position[2]),
            direction.into(),
        );
        self.query_pipeline
            .cast_ray(
                &self.rigid_body_set,
                &self.collider_set,
                &ray,
                max_toi,
                true,
                self.query_filter(exclude_entity),
            )
            .map(|(handle, toi)| (self.entity(handle), toi))
    }

    fn query_filter(&self, exclude_entity: Option<&Facade>) -> QueryFilter {
        exclude_entity
            .and_then(|entity| self.rigid_bodies.get(entity.id()))
            .map(|handle| QueryFilter::new().exclude_rigid_body(*handle))
            .unwrap_or(QueryFilter::default())
            .exclude_sensors()
    }

    fn entity(&self, handle: ColliderHandle) -> String {
        let handle = self.collider_set[handle]
            .parent()
            .expect("rigid body handle");
        self.rigid_bodies
            .iter()
            .find(|(_, rigid_body_handle)| handle == **rigid_body_handle)
            .expect("entity")
            .0
            .clone()
    }

    /// Applies an impulse to an entity.
    pub fn apply_impulse(&mut self, position: Vec3, impulse: Vec3, entity: &Facade) {
        if let Some(rigid_body) = self
            .rigid_bodies
            .get(entity.id())
            .map(|handle| &mut self.rigid_body_set[*handle])
        {
            rigid_body.apply_impulse_at_point(
                impulse.into(),
                Point::new(position[0], position[1], position[2]),
                true,
            );
        }
    }

    /// Calculates the offset that needs to be applied to `entity` to resolve penetrations.
    pub fn penetration_offset(&self, entity: &Facade) -> Vec3 {
        let mut offset = Vector::<Real>::zeros();

        if let Some(rigid_body) = self
            .rigid_bodies
            .get(entity.id())
            .map(|rigid_body| &self.rigid_body_set[*rigid_body])
        {
            let entity_collider_handle = rigid_body.colliders()[0];
            for contact_pair in self
                .narrow_phase
                .contacts_with(entity_collider_handle)
                .filter(|contact_pair| contact_pair.has_any_active_contact)
            {
                for manifold in &contact_pair.manifolds {
                    for contact_point in &manifold.points {
                        if contact_point.dist < 0.0 {
                            let (mut entity_contact_point, contact_normal, other_contact_point) =
                                self.contact_info(
                                    entity_collider_handle,
                                    contact_pair,
                                    manifold,
                                    contact_point,
                                );
                            entity_contact_point += offset;
                            let actual_distance =
                                (entity_contact_point - other_contact_point).magnitude();
                            if actual_distance > 1.0e-5 {
                                offset += contact_normal * -contact_point.dist;
                            }
                        }
                    }
                }
            }
        }

        offset.into()
    }

    /// Returns if `entity` intersects `other`.
    pub fn intersecting(&self, entity: &Facade, other: &Facade) -> bool {
        if let Some((entity_rigid_body, other_rigid_body)) = self
            .rigid_bodies
            .get(entity.id())
            .map(|rigid_body| &self.rigid_body_set[*rigid_body])
            .zip(
                self.rigid_bodies
                    .get(other.id())
                    .map(|rigid_body| &self.rigid_body_set[*rigid_body]),
            )
        {
            return self
                .narrow_phase
                .intersection_pair(
                    entity_rigid_body.colliders()[0],
                    other_rigid_body.colliders()[0],
                )
                .unwrap_or_default();
        }

        false
    }

    /// Checks if an entity at `transform` is grounded.
    pub fn is_grounded(
        &self,
        entity: &Facade,
        position: Vec3,
        orientation: Quat,
        offset: Vec2,
    ) -> bool {
        let mut grounded = false;

        if let Some(entity_collider) = self
            .rigid_bodies
            .get(entity.id())
            .map(|handle| &self.collider_set[self.rigid_body_set[*handle].colliders()[0]])
        {
            let prediction = offset[1] * 1.1;
            let dispatcher = DefaultQueryDispatcher;
            let filter = QueryFilter::new().exclude_rigid_body(self.rigid_bodies[entity.id()]);
            let collider_position =
                Isometry::from(Transform::new(position, orientation, Vec3::default()));
            let entity_aabb = entity_collider
                .shape()
                .compute_aabb(&collider_position)
                .loosened(prediction);

            self.query_pipeline.colliders_with_aabb_intersecting_aabb(
                &entity_aabb,
                |other_collider_handle| {
                    let other_collider = &self.collider_set[*other_collider_handle];
                    if filter.test(&self.rigid_body_set, *other_collider_handle, other_collider) {
                        let mut manifolds: Vec<ContactManifold> = Default::default();
                        let pos12 = collider_position.inv_mul(other_collider.position());
                        let _ = dispatcher.contact_manifolds(
                            &pos12,
                            entity_collider.shape(),
                            other_collider.shape(),
                            prediction,
                            &mut manifolds,
                            &mut None,
                        );

                        for manifold in &manifolds {
                            let normal = collider_position * manifold.local_n1;

                            if normal.dot(&Vector::new(0.0, 1.0, 0.0)) <= -2.0e-1 {
                                for contact_point in &manifold.points {
                                    if contact_point.dist <= prediction {
                                        grounded = true;
                                        return false;
                                    }
                                }
                            }
                        }
                    }

                    true
                },
            );
        }

        grounded
    }

    fn contact_info(
        &self,
        entity_collider_handle: ColliderHandle,
        contact_pair: &ContactPair,
        manifold: &ContactManifold,
        contact_point: &TrackedContact<ContactData>,
    ) -> (Point<Real>, Vector<Real>, Point<Real>) {
        if contact_pair.collider1 == entity_collider_handle {
            let entity_collider = &self.collider_set[entity_collider_handle];
            let other_collider = &self.collider_set[contact_pair.collider2];
            (
                entity_collider.position() * contact_point.local_p1,
                other_collider.position() * manifold.local_n2,
                other_collider.position() * contact_point.local_p2,
            )
        } else {
            let entity_collider = &self.collider_set[entity_collider_handle];
            let other_collider = &self.collider_set[contact_pair.collider1];
            (
                entity_collider.position() * contact_point.local_p2,
                other_collider.position() * manifold.local_n1,
                other_collider.position() * contact_point.local_p1,
            )
        }
    }
}

/// Casting result.
#[allow(missing_docs)]
pub struct Hit {
    pub normal2: Vec3,
    pub normal1: Vec3,
    pub witness2: Vec3,
    pub witness1: Vec3,
    pub toi: f32,
    pub entity: String,
}

impl From<Vec3> for Isometry<Real> {
    fn from(position: Vec3) -> Self {
        Isometry::translation(position[0], position[1], position[2])
    }
}

impl From<Isometry<Real>> for Vec3 {
    fn from(isometry: Isometry<Real>) -> Self {
        Vec3::new(
            isometry.translation.x,
            isometry.translation.y,
            isometry.translation.z,
        )
    }
}

impl From<&Isometry<Real>> for Vec3 {
    fn from(isometry: &Isometry<Real>) -> Self {
        (*isometry).into()
    }
}

impl From<Isometry<Real>> for Quat {
    fn from(isometry: Isometry<Real>) -> Self {
        Quat::new(
            isometry.rotation.w,
            isometry.rotation.i,
            isometry.rotation.j,
            isometry.rotation.k,
        )
    }
}

impl From<&Isometry<Real>> for Quat {
    fn from(isometry: &Isometry<Real>) -> Self {
        (*isometry).into()
    }
}
