//! Audio playback and spatial audio.

pub use private::Audio;
pub use source::AudioSource;

mod private;
mod source;
