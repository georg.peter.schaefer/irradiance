use std::fmt::{Debug, Formatter};
use std::io::Cursor;
use std::path::Path;
use std::sync::Arc;
use std::time::Duration;

use rodio::decoder::DecoderError;
use rodio::source::Buffered;
use rodio::{Decoder, Source};

use crate::asset::{Asset, Assets};
use crate::gfx::GraphicsContext;

/// An audio source.
#[derive(Clone)]
pub struct AudioSource {
    pub(crate) inner: Buffered<Decoder<Cursor<Vec<u8>>>>,
}

impl Debug for AudioSource {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("AudioSource").finish()
    }
}

impl Asset for AudioSource {
    type Error = DecoderError;

    fn try_from_bytes(
        _assets: Arc<Assets>,
        _graphics_context: Arc<GraphicsContext>,
        _path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, Self::Error> {
        Ok(Self {
            inner: Decoder::new(Cursor::new(bytes))?.buffered(),
        })
    }
}

impl Iterator for AudioSource {
    type Item = i16;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

impl Source for AudioSource {
    fn current_frame_len(&self) -> Option<usize> {
        self.inner.current_frame_len()
    }

    fn channels(&self) -> u16 {
        self.inner.channels()
    }

    fn sample_rate(&self) -> u32 {
        self.inner.sample_rate()
    }

    fn total_duration(&self) -> Option<Duration> {
        self.inner.total_duration()
    }
}
