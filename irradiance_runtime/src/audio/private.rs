use std::collections::HashMap;

use rodio::{OutputStream, OutputStreamHandle, Sample, Sink, Source, SpatialSink};

use irradiance_runtime::ecs::Transform;

use crate::ecs::{Entities, Facade};
use crate::math::{Quat, Vec3};

/// Audio system.
pub struct Audio {
    sinks: HashMap<String, SpatialSink>,
    output_stream_handle: OutputStreamHandle,
    _output_stream: OutputStream,
}

impl Audio {
    /// Creates a new audio system.
    pub fn new() -> Self {
        let (_output_stream, output_stream_handle) =
            OutputStream::try_default().expect("output stream on default audio device");
        Self {
            sinks: Default::default(),
            _output_stream,
            output_stream_handle,
        }
    }

    /// Plays an [`AudioSource`] returning a new [`Sink`] for controlling the playback.
    pub fn play<S: Source + Send + 'static>(&self, source: S) -> Sink
    where
        <S as Iterator>::Item: Sample + Send,
    {
        let sink = Sink::try_new(&self.output_stream_handle).expect("sink");
        sink.append(source);
        sink
    }

    /// Plays an audio source at an entities position.
    pub fn play_at<S: Source + Send + 'static>(&mut self, entity: &Facade, source: S)
    where
        <S as Iterator>::Item: Sample + Send,
    {
        if let Some(transform) = entity.get::<Transform>() {
            self.sinks
                .entry(entity.id().to_owned())
                .or_insert_with(|| {
                    SpatialSink::try_new(
                        &self.output_stream_handle,
                        transform.position().into(),
                        Vec3::zero().into(),
                        Vec3::zero().into(),
                    )
                    .expect("sink")
                })
                .append(source);
        }
    }

    /// Stops the currently playing sound.
    pub fn stop(&self, entity: &Facade) {
        if let Some(sink) = self.sinks.get(entity.id()) {
            sink.stop()
        }
    }

    /// Stops all playing sounds.
    pub fn stop_all(&mut self) {
        self.sinks.clear();
    }

    /// Returns if a sound is playing for an entity.
    pub fn is_playing(&self, entity: &Facade) -> bool {
        self.sinks
            .get(entity.id())
            .map(|sink| !sink.empty())
            .unwrap_or_default()
    }

    /// Updates the positions of the spatial audio.
    pub fn update(&self, listener_position: Vec3, listener_orientation: Quat, entities: &Entities) {
        let left = listener_orientation.rotate(Vec3::left());
        let left_ear_offset = left * 0.1;
        let right_ear_offset = -left_ear_offset;
        let height_offset = Vec3::up() * 1.8;
        for (entity, transform) in entities.components::<(&Transform,)>() {
            if let Some(sink) = self.sinks.get(entity.id()) {
                let center = listener_position + height_offset;
                let attenuation = 10.0;
                let left_ear_position = center / attenuation + left_ear_offset;
                let right_ear_position = center / attenuation + right_ear_offset;
                let emitter_position = transform.position() / attenuation;
                sink.set_left_ear_position(left_ear_position.into());
                sink.set_right_ear_position(right_ear_position.into());
                sink.set_emitter_position(emitter_position.into());
            }
        }
    }
}
