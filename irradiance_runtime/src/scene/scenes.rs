use std::{collections::HashMap, fs, path::PathBuf};

use crate::core::Engine;

use super::Scene;

/// Scene manager.
#[derive(Default)]
pub struct Scenes {
    scenes: HashMap<&'static str, Scene>,
    asset_path: PathBuf,
}

impl Scenes {
    /// Creates a new scene manager.
    pub fn new(asset_path: PathBuf, engine: &dyn Engine) -> Self {
        if !asset_path.join("scenes").exists() {
            fs::create_dir(asset_path.join("scenes")).expect("scenes directory created");
        }

        Self {
            asset_path: asset_path.clone(),
            scenes: engine
                .screens()
                .names()
                .into_iter()
                .map(|screen| (screen, Scene::read(screen, &asset_path, engine)))
                .collect(),
        }
    }

    /// Returns a scene by it's name.
    pub fn get(&self, name: &'static str) -> Option<&Scene> {
        self.scenes.get(name)
    }

    /// Saves the scenes.
    pub fn save(&self) {
        for scene in self.scenes.values() {
            scene.write(&self.asset_path);
        }
    }
}
