use std::{
    fmt::{self, Debug, Formatter},
    fs,
    path::Path,
    sync::Arc,
};

use ron::{ser::PrettyConfig, Value};
use serde::{ser::SerializeStruct, Serialize};

use crate::{core::Engine, ecs::Entities};

/// A scene.
pub struct Scene {
    entities: Arc<Entities>,
    screen: String,
}

impl Scene {
    pub(crate) fn read(screen: &'static str, asset_path: &Path, engine: &dyn Engine) -> Self {
        let path = asset_path
            .join("scenes")
            .join(screen)
            .with_extension("scene");
        if !path.exists() {
            fs::write(
                &path,
                ron::to_string(&de::Scene {
                    entities: Default::default(),
                })
                .expect("serialized scene"),
            )
            .expect("create scene file");
        }

        let scene: de::Scene =
            ron::de::from_bytes(&fs::read(path).expect("read scene")).expect("scene");
        let entities = Entities::new();
        for (id, mut components) in scene.entities.into_iter() {
            let entity = entities.create(id).expect("entity");

            while let Some(Value::Map(mut component)) = components.pop() {
                let type_name = match component
                    .remove(&Value::String("type_name".to_owned()))
                    .expect("type name")
                {
                    Value::String(type_name) => type_name,
                    _ => panic!("missing type name"),
                };

                if let Some(component) =
                    engine
                        .component_types()
                        .deserialize(&type_name, Value::Map(component), engine)
                {
                    entity.insert_dyn(component);
                }
            }
        }

        unsafe {
            entities.commit();
        }

        Scene {
            screen: screen.to_owned(),
            entities,
        }
    }

    pub(crate) fn write(&self, asset_path: &Path) {
        let path = asset_path
            .join("scenes")
            .join(&self.screen)
            .with_extension("scene");
        fs::write(
            path,
            ron::ser::to_string_pretty(&self, PrettyConfig::default()).expect("serialized scene"),
        )
        .expect("written scene")
    }

    /// Returns the entities.
    pub fn entities(&self) -> &Arc<Entities> {
        &self.entities
    }
}

impl Debug for Scene {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("Scene")
            .field("screen", &self.screen)
            .finish()
    }
}

impl Serialize for Scene {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut state = serializer.serialize_struct("Scene", 1)?;
        state.serialize_field("entities", self.entities.as_ref())?;
        state.end()
    }
}

mod de {
    use std::collections::HashMap;

    use ron::Value;
    use serde::{Deserialize, Serialize};

    #[derive(Serialize, Deserialize)]
    pub struct Scene {
        pub entities: HashMap<String, Vec<Value>>,
    }
}
