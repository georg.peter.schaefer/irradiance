//! Managing scenes.

pub use private::Scene;
pub use scenes::Scenes;

mod private;
mod scenes;
