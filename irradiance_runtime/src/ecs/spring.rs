pub use serde::Serialize;

use crate::math::Vec3;
pub use crate::Component;

/// Spring component.
#[derive(Clone, Debug, Default, Component, Serialize)]
#[serde(tag = "type_name")]
pub struct Spring {
    /// Current velocity.
    #[serde(skip_serializing, skip_deserializing)]
    pub velocity: Vec3,
    /// Dampening factor of the resulting spring force.
    pub dampening: f32,
    /// Desired distance between this entity and the target entity.
    pub distance: f32,
    /// Stiffness of the spring.
    pub stiffness: f32,
    /// Mass of this entity.
    pub mass: f32,
    /// Offset that is added to the target entities position.
    pub offset: Vec3,
    /// Target entity.
    pub target: String,
}
