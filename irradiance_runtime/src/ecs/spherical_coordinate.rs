use std::f32::consts;

use irradiance_derive::Component;
use irradiance_runtime::math::Vec3;
use serde::Serialize;

/// Spherical coordinate.
#[derive(Copy, Clone, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct SphericalCoordinate {
    azimuth: f32,
    inclination: f32,
    radius: f32,
}

impl SphericalCoordinate {
    /// Creates a new spherical coordinate.
    pub fn new(radius: f32, inclination: f32, azimuth: f32) -> Self {
        Self {
            radius,
            inclination,
            azimuth,
        }
    }

    /// Creates a new spherical cooridinate pointing forward in cartesian coordinates.
    pub fn forward(radius: f32) -> Self {
        Self::new(radius, consts::FRAC_PI_2, consts::PI)
    }

    /// Returns the radius.
    pub fn radius(&self) -> f32 {
        self.radius
    }

    /// Sets the radius.
    pub fn set_radius(&mut self, radius: f32) {
        self.radius = radius;
    }

    /// Returns the inclination.
    pub fn inclination(&self) -> f32 {
        self.inclination
    }

    /// Sets the inclination.
    pub fn set_inclination(&mut self, inclination: f32) {
        self.inclination = inclination;
    }

    /// Returns the azimuth.
    pub fn azimuth(&self) -> f32 {
        self.azimuth
    }

    /// Sets the azimuth.
    pub fn set_azimuth(&mut self, azimuth: f32) {
        self.azimuth = azimuth;
    }

    /// Converts the spherical coordinate into a cartesian coordinate.
    pub fn to_cartesian(self) -> Vec3 {
        Vec3::new(
            self.radius * self.azimuth.sin() * self.inclination.sin(),
            self.radius * self.inclination.cos(),
            self.radius * self.azimuth.cos() * self.inclination.sin(),
        )
    }
}
