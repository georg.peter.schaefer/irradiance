use crate::core::{Engine, Time};
use crate::ecs::{Entities, SphericalCoordinate, Spring, Transform};
use crate::math::{Mat4, Vec3};
use crate::physics::{RigidBody, RigidBodyType};

/// Spring system.
#[derive(Default)]
pub struct Springs;

impl Springs {
    /// Updates spring components.
    pub fn update(&self, engine: &dyn Engine, time: Time, entities: &Entities) {
        for (entity, _, spring) in entities.components::<(&Transform, &Spring)>() {
            if entity.get::<SphericalCoordinate>().is_none() {
                entity.insert(SphericalCoordinate::new(
                    spring.distance,
                    std::f32::consts::FRAC_PI_2,
                    0.0,
                ));
            }
        }
        unsafe {
            entities.commit();
        }

        for (entity, mut transform, mut spring, spherical_coordinate) in
            entities.components::<(&mut Transform, &mut Spring, &mut SphericalCoordinate)>()
        {
            if let Some(target) = entities
                .get(&spring.target)
                .filter(|entity| entity.get::<Transform>().is_some())
            {
                let target_transform = target.get::<Transform>().expect("target transform");
                let target_position = target_transform.position()
                    + spring.offset
                    + spherical_coordinate.to_cartesian();

                let force = spring.stiffness * (target_position - transform.position())
                    - spring.dampening * spring.velocity;
                let acceleration = force / spring.mass;
                let guessed_velocity = spring.velocity + time.delta() * acceleration;
                let guessed_position = transform.position() + time.delta() * guessed_velocity;

                let force = spring.stiffness * (target_position - guessed_position)
                    - spring.dampening * guessed_velocity;
                let acceleration = force / spring.mass;
                let velocity = guessed_velocity + time.delta() * acceleration;
                spring.velocity = 0.5 * (guessed_velocity + velocity);

                let mut translation_remaining = time.delta() * spring.velocity;
                let mut translation = Vec3::zero();

                if let Some(rigid_body) = entity
                    .get::<RigidBody>()
                    .filter(|rigid_body| rigid_body.body_type == RigidBodyType::Kinematic)
                {
                    let offset = 0.05;
                    let mut max_iter = 20;
                    while translation_remaining.length() >= 1.0e-5 {
                        if max_iter == 0 {
                            break;
                        } else {
                            max_iter -= 1;
                        }

                        if let Some(hit) = engine.physics().cast_shape(
                            transform.position() + translation,
                            transform.orientation(),
                            rigid_body.shape.clone(),
                            translation_remaining.normalize(),
                            translation_remaining.length() + offset,
                            Some(&entity),
                        ) {
                            let allowed_distance = (hit.toi
                                - (-hit.normal1.dot(translation_remaining)) * offset)
                                .max(0.0);
                            let allowed_translation =
                                translation_remaining.normalize() * allowed_distance;
                            translation += allowed_translation;
                            translation_remaining -= allowed_translation;

                            translation_remaining -=
                                hit.normal1.dot(translation_remaining) * hit.normal1;
                        } else {
                            translation += translation_remaining;
                            break;
                        }
                    }
                } else {
                    translation = translation_remaining;
                }

                let position = transform.position() + translation;
                transform.set_position(position);
                transform.set_orientation(
                    Mat4::look_at(
                        position,
                        target_transform.position() + spring.offset,
                        Vec3::new(0.0, 1.0, 0.0),
                    )
                    .inverse()
                    .into(),
                );
            }
        }
        unsafe {
            entities.commit();
        }
    }
}
