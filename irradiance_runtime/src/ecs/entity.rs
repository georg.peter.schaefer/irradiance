use crate::ecs::component::{Component, Ref, RefMut};
use crate::ecs::entities::Entities;
use crate::ecs::Active;
use std::any::TypeId;
use std::fmt::{Debug, Formatter};
use std::sync::Arc;

/// An entity's facade.
///
/// Gives access to an entity's components and lets you add new components or remove existing ones.
#[derive(Clone)]
pub struct Facade<'a> {
    id: String,
    entities: &'a Entities,
}

impl<'a> Facade<'a> {
    pub(super) fn new(id: String, entities: &'a Entities) -> Self {
        Self { id, entities }
    }

    /// Returns the entity's id.
    pub fn id(&self) -> &str {
        &self.id
    }

    /// Returns a immutable reference to the component of type `C` if the entity contains one.
    pub fn get<C>(&self) -> Option<Ref<'a, C>>
    where
        C: Component,
    {
        self.entities.get_component::<C>(&self.id).map(Ref::new)
    }

    /// Returns a mutable reference to the component of type `C` if the entity contains one.
    pub fn get_mut<C>(&self) -> Option<RefMut<'a, C>>
    where
        C: Component + Clone,
    {
        self.entities
            .get_component::<C>(&self.id)
            .map(|component| RefMut::new(self.entities, self.id.clone(), (*component).clone()))
    }

    #[doc(hidden)]
    pub fn get_dyn(&self, component_type: TypeId) -> Option<Arc<dyn Component>> {
        self.entities.get_component_dyn(&self.id, &component_type)
    }

    /// Returns the `TypeId`s if the components.
    pub fn types(&self) -> Vec<TypeId> {
        self.entities.get_component_types(&self.id)
    }

    /// Activates a component.
    pub fn activate<C: Component + Active>(&self) {
        self.entities.activate_component::<C>(self.id.clone());
    }

    /// Adds a new component to the entity.
    pub fn insert<C>(&self, component: C)
    where
        C: Component,
    {
        self.entities.insert_component(self.id.clone(), component);
    }

    #[doc(hidden)]
    pub fn insert_dyn(&self, component: Arc<dyn Component>) {
        self.entities
            .insert_component_dyn(self.id.clone(), component);
    }

    /// Removes the component of type `C`.
    pub fn remove<C>(&self)
    where
        C: Component,
    {
        self.entities.remove_component::<C>(self.id.clone());
    }

    #[doc(hidden)]
    pub fn remove_by_type(&self, component_type: TypeId) {
        self.entities
            .remove_component_by_type(self.id.clone(), component_type);
    }
}

impl<'a> Debug for Facade<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Facade<'a>").field("id", &self.id).finish()
    }
}

impl<'a> Eq for Facade<'a> {}

impl<'a> PartialEq for Facade<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}
