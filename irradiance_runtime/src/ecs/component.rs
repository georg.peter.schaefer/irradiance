use std::any::Any;
use std::fmt::{Debug, Display, Formatter};
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};
use std::sync::Arc;

use erased_serde::{serialize_trait_object, Serialize};

use crate::ecs::Entities;

/// A component type.
///
/// Types implementing `Component` can be used to compose an entity.
///
/// # Implementing `Component`
/// You can derive `Component` with `#[derive(Component)]`.
///
/// ```
/// use irradiance_runtime::Component;
///
/// #[derive(Component)]
/// struct Health {
///     health: f32,
/// }
/// ```
pub trait Component: Serialize + Send + Sync + 'static {
    #[doc(hidden)]
    fn as_any(self: Arc<Self>) -> Arc<dyn Any + Send + Sync + 'static>;
    /// Duplicates a component.
    fn duplicate(&self) -> Arc<dyn Component>;
}

serialize_trait_object!(Component);

/// Immutable reference to a component of an entity.
pub struct Ref<'a, C>
where
    C: Component,
{
    inner: Arc<C>,
    _phantom: PhantomData<&'a C>,
}

impl<'a, C> Ref<'a, C>
where
    C: Component,
{
    pub(super) fn new(inner: Arc<C>) -> Self {
        Self {
            inner,
            _phantom: Default::default(),
        }
    }
}

impl<'a, C> Deref for Ref<'a, C>
where
    C: Component,
{
    type Target = C;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<'a, C> Debug for Ref<'a, C>
where
    C: Component + Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&**self, f)
    }
}

impl<'a, C> Display for Ref<'a, C>
where
    C: Component + Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&**self, f)
    }
}

/// Mutable reference to a component of an entity.
///
/// Modifications made through this mutable reference get enqueued for application when the
/// reference is dropped. The modifications become only visible after
/// [`commit`](super::Entities::commit) has been invoked.
pub struct RefMut<'a, C>
where
    C: Component + Clone,
{
    inner: C,
    id: String,
    entities: &'a Entities,
}

impl<'a, C> RefMut<'a, C>
where
    C: Component + Clone,
{
    pub(super) fn new(entities: &'a Entities, id: String, inner: C) -> Self {
        Self {
            inner,
            id,
            entities,
        }
    }
}

impl<'a, C> Drop for RefMut<'a, C>
where
    C: Component + Clone,
{
    fn drop(&mut self) {
        self.entities
            .insert_component(self.id.clone(), self.inner.clone());
    }
}

impl<'a, C> Deref for RefMut<'a, C>
where
    C: Component + Clone,
{
    type Target = C;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<'a, C> DerefMut for RefMut<'a, C>
where
    C: Component + Clone,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl<'a, C> Debug for RefMut<'a, C>
where
    C: Component + Clone + Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&**self, f)
    }
}

impl<'a, C> Display for RefMut<'a, C>
where
    C: Component + Clone + Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&**self, f)
    }
}

/// Marker trait for components where only one instance can be active.
pub trait Active: Component {}
