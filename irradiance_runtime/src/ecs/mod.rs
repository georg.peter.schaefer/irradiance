//! Entity component system.
//!
//! Entities are the game objects and consist of components. Components hold the data that describe
//! the game. The logic which processes this data resides in systems. Systems iterate over the
//! entities that they are interested in and update the components accordingly.
//!
//! # Implementing a `Component`
//! A component type needs to implement [`Component`]. You can derive [`Component`] with
//! `#[derive(Component)]`.
//!
//! ```
//! use irradiance_runtime::Component;
//!
//! #[derive(Component)]
//! struct Damage {
//!     damage: f32,
//! }
//! ```
//!
//! # Creating an entity
//! An entity is created through the [entity managers](Entities) `create` function.
//!
//! ```
//! use irradiance_runtime::ecs::Entities;
//!
//! let entities = Entities::new();
//! let entity = entities.create("enemy").unwrap();
//! ```
//!
//! This returns a [`Facade`] for the entity which provides functions for accessing and manipulating
//! the entity. An entity is an association between the entity's id and its components. The
//! [`Facade`] acts as a proxy to provide a type like interface, even though entities have no
//! declared type.
//!
//! ```
//! use irradiance_runtime::Component;
//! use irradiance_runtime::ecs::Entities;
//!
//! #[derive(Component, Clone)]
//! struct Attributes {
//!     strength: u32,
//!     intelligence: u32
//! }
//!
//! let entities = Entities::new();
//! let player = entities.create("player").unwrap();
//!
//! // Adding a component to an entity
//! player.insert(Attributes { strength: 10, intelligence: 18 });
//!
//! // Removing a component
//! player.remove::<Attributes>();
//!
//! // Immutable access to a component
//! player.get::<Attributes>();
//!
//! // Mutable access
//! player.get_mut::<Attributes>()
//!     .map(|mut attributes| attributes.intelligence -= 2);
//! ```
//!
//! # Thread Safety
//! All operations on entities, even creating them are thread safe. Component insertion and removal
//! is deferred until [`commit`](Entities::commit) is called. This should only happen at the end of
//! a frame from the main thread. Otherwise no safety guarantees can be given.
//!
//! This is why components need to implement `Clone` if you want to modify them through a
//! [`RefMut`]. The component is cloned and re-inserted when the mutable reference goes out of
//! scope.
//!
//! # Iterating over entities
//! When implementing a system, you are most likely only interested in processing entities that have
//! a certain subset of components. [`components`](Entities::components) lets you iterate
//! over all entities pre-filtered for the components you are interested in.
//!
//! ```
//! use irradiance_runtime::Component;
//! use irradiance_runtime::ecs::Entities;
//!
//! #[derive(Component, Clone)]
//! struct Transform;
//!
//! #[derive(Component, Clone)]
//! struct StaticMesh;
//!
//! let entities = Entities::new();
//!
//! for (entity, transform) in entities.components::<(&Transform,)>() {
//!     // Iterating over all entities that have a Transform component
//! }
//!
//! for (entity, mut transform, static_mesh) in entities.components::<(&mut Transform, &StaticMesh)>() {
//!     // Iterating over all entities that have a Transform and StaticMesh component
//!     // The Transform component is mutable
//! }
//! ```

pub use component::Active;
pub use component::Component;
pub use component::Ref;
pub use component::RefMut;
pub use entities::Entities;
pub use entities::EntityCreationError;
pub use entities::Iter;
pub use entity::Facade;
pub use extension::ComponentExt;
pub use extension::ComponentTypes;
pub use extension::ComponentTypesBuilder;
pub use spherical_coordinate::SphericalCoordinate;
pub use spring::Spring;
pub use springs::Springs;
pub use transform::Transform;

mod component;
mod entities;
mod entity;
mod extension;
mod spherical_coordinate;
mod spring;
mod springs;
mod transform;
