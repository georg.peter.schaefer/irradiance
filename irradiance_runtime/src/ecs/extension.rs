use std::{collections::HashMap, sync::Arc};

use ron::Value;

use crate::ecs::Spring;
use crate::physics::RigidBody;
use crate::rendering::{AnimatedMesh, CascadedShadow, OmnidirectionalShadow, PointLight};
use crate::{
    core::Engine,
    rendering::{Camera, DirectionalLight, StaticMesh},
};

use super::{Component, SphericalCoordinate, Transform};

/// Extension trait for components.
pub trait ComponentExt: Sized + Component + Clone {
    /// Returns the type name.
    fn type_name() -> &'static str;
    /// Deserializes a component.
    fn from_str(value: Value, engine: &dyn Engine) -> Option<Arc<dyn Component>>;
}

type Deserializer = dyn Fn(Value, &dyn Engine) -> Option<Arc<dyn Component>>;

/// Component type registry.
pub struct ComponentTypes {
    deserializer: Vec<Box<Deserializer>>,
    types: HashMap<&'static str, usize>,
}

impl ComponentTypes {
    /// Starts building a new component type registry.
    pub fn builder() -> ComponentTypesBuilder {
        ComponentTypesBuilder {
            deserializer: Default::default(),
            types: Default::default(),
        }
        .component::<Transform>()
        .component::<SphericalCoordinate>()
        .component::<Spring>()
        .component::<Camera>()
        .component::<DirectionalLight>()
        .component::<PointLight>()
        .component::<StaticMesh>()
        .component::<AnimatedMesh>()
        .component::<CascadedShadow>()
        .component::<OmnidirectionalShadow>()
        .component::<RigidBody>()
    }

    /// Deserializes a component.
    pub fn deserialize(
        &self,
        type_name: &str,
        value: Value,
        engine: &dyn Engine,
    ) -> Option<Arc<dyn Component>> {
        self.types
            .get(type_name)
            .and_then(|index| self.deserializer[*index](value, engine))
    }
}

/// Type for building component type registration.
pub struct ComponentTypesBuilder {
    deserializer: Vec<Box<Deserializer>>,
    types: HashMap<&'static str, usize>,
}

impl ComponentTypesBuilder {
    /// Registers a component.
    pub fn component<C: ComponentExt>(mut self) -> Self {
        let index = self.types.len();
        self.types.insert(C::type_name(), index);
        self.deserializer.push(Box::new(|serialized, engine| {
            C::from_str(serialized, engine)
        }));
        self
    }

    /// Builds the component type registry.
    pub fn build(self) -> ComponentTypes {
        ComponentTypes {
            deserializer: self.deserializer,
            types: self.types,
        }
    }
}
