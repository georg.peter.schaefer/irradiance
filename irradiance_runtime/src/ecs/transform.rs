use rapier3d::prelude::*;

use serde::Serialize;

use crate::math::{Mat4, Quat, Vec3, Vec4};
use crate::Component;

/// Entity transformation.
#[derive(Copy, Clone, Debug, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct Transform {
    position: Vec3,
    orientation: Quat,
    scale: Vec3,
}

impl Transform {
    /// Creates a new transform component.
    pub fn new(position: Vec3, orientation: Quat, scale: Vec3) -> Self {
        Self {
            position,
            orientation,
            scale,
        }
    }

    /// Returns the position.
    pub fn position(&self) -> Vec3 {
        self.position
    }

    /// Sets the position.
    pub fn set_position(&mut self, position: Vec3) {
        self.position = position;
    }

    /// Returns the orientation.
    pub fn orientation(&self) -> Quat {
        self.orientation
    }

    /// Sets the orientation.
    pub fn set_orientation(&mut self, orientation: Quat) {
        self.orientation = orientation;
    }

    /// Returns the scale.
    pub fn scale(&self) -> Vec3 {
        self.scale
    }

    /// Sets the scale.
    pub fn set_scale(&mut self, scale: Vec3) {
        self.scale = scale;
    }
}

impl Default for Transform {
    fn default() -> Self {
        Self {
            position: Default::default(),
            orientation: Default::default(),
            scale: Vec3::new(1.0, 1.0, 1.0),
        }
    }
}

impl From<Transform> for Mat4 {
    fn from(transform: Transform) -> Self {
        let scale = Mat4::from([
            Vec4::new(transform.scale[0], 0.0, 0.0, 0.0),
            Vec4::new(0.0, transform.scale[1], 0.0, 0.0),
            Vec4::new(0.0, 0.0, transform.scale[2], 0.0),
            Vec4::new(0.0, 0.0, 0.0, 1.0),
        ]);
        let rotation = Mat4::from(transform.orientation);
        let mut model = rotation * scale;

        model[3][0] = transform.position[0];
        model[3][1] = transform.position[1];
        model[3][2] = transform.position[2];
        model
    }
}

impl From<Transform> for Isometry<Real> {
    fn from(transform: Transform) -> Self {
        let angle_axis = transform.orientation.to_angle_axis();
        Isometry::new(
            vector![
                transform.position[0],
                transform.position[1],
                transform.position[2]
            ],
            vector![angle_axis[0], angle_axis[1], angle_axis[2]],
        )
    }
}
