use std::any::TypeId;
use std::cell::UnsafeCell;
use std::collections::HashMap;
use std::fmt::{Debug, Display, Formatter};
use std::marker::PhantomData;
use std::sync::{Arc, Mutex};

use serde::ser::SerializeMap;
use serde::Serialize;

use crate::ecs::entity::Facade;
use crate::ecs::{Active, Component};
use crate::scene::Scene;

type Components = HashMap<TypeId, Arc<dyn Component>>;

/// Type for managing [entities](super::Facade) and [components](super::Component).
pub struct Entities {
    insert_components: Mutex<Vec<(String, Arc<dyn Component>)>>,
    delete_components: Mutex<Vec<(String, TypeId)>>,
    insert_entities: Mutex<Vec<String>>,
    delete_entities: Mutex<Vec<String>>,
    active: Mutex<HashMap<TypeId, String>>,
    components: UnsafeCell<HashMap<String, Components>>,
}

impl Entities {
    /// Creates a new entity manager.
    pub fn new() -> Arc<Self> {
        Arc::new(Self {
            insert_components: Default::default(),
            delete_components: Default::default(),
            insert_entities: Default::default(),
            delete_entities: Default::default(),
            active: Default::default(),
            components: Default::default(),
        })
    }

    /// Creates a new entity manager from a scene.
    pub fn from_scene(scene: &Scene) -> Arc<Self> {
        Arc::new(Self {
            insert_components: Default::default(),
            delete_components: Default::default(),
            insert_entities: Default::default(),
            delete_entities: Default::default(),
            active: Default::default(),
            components: UnsafeCell::new(
                unsafe { &*scene.entities().components.get() }
                    .clone()
                    .into_iter()
                    .filter(|(entity, _)| !entity.starts_with("__internal"))
                    .collect(),
            ),
        })
    }

    /// Returns teh entity ids.
    pub fn all(&self) -> Vec<Facade<'_>> {
        let mut all = unsafe { &*self.components.get() }
            .keys()
            .cloned()
            .filter(|entity| !entity.starts_with("__internal"))
            .map(|id| Facade::new(id, self))
            .collect::<Vec<_>>();
        all.sort_by_key(|facade| facade.id().to_owned());
        all
    }

    /// Returns a [`Facade`](super::Facade) for the entity with `id`.
    pub fn get(&self, id: impl AsRef<str>) -> Option<Facade> {
        unsafe {
            (*self.components.get())
                .get(id.as_ref())
                .map(|_| Facade::new(id.as_ref().to_string(), self))
        }
    }

    /// Creates a new entity with `id`.
    pub fn create(&self, id: impl AsRef<str>) -> Result<Facade, EntityCreationError> {
        if unsafe { &mut *self.components.get() }
            .get(id.as_ref())
            .is_some()
        {
            Err(EntityCreationError {
                id: id.as_ref().to_string(),
            })
        } else {
            self.insert_entities
                .lock()
                .unwrap()
                .push(id.as_ref().to_string());
            Ok(Facade::new(id.as_ref().to_string(), self))
        }
    }

    /// Removes all components of an entity.
    pub fn remove(&self, facade: &Facade) {
        self.delete_entities
            .lock()
            .unwrap()
            .push(facade.id().to_owned());
    }

    /// Returns if an entity with [`id`] has a component of type [`C`].
    pub fn has<C: Component>(&self, id: impl AsRef<str>) -> bool {
        self.get(id.as_ref())
            .and_then(|entity| entity.get::<C>())
            .is_some()
    }

    /// Commits all deferred operations.
    ///
    /// # Safety
    /// It is safe to call this function at the end of a frame.
    pub unsafe fn commit(&self) {
        let components = &mut *self.components.get();

        for id in self.delete_entities.lock().unwrap().drain(0..) {
            components.remove(&id);
        }

        for id in self.insert_entities.lock().unwrap().drain(0..) {
            components.insert(id, Default::default());
        }

        let mut active = self.active.lock().unwrap();
        for (id, type_id) in self.delete_components.lock().unwrap().drain(0..) {
            components
                .get_mut(&id)
                .and_then(|components| components.remove(&type_id));
            if active.get(&type_id) == Some(&id) {
                active.remove(&type_id);
            }
        }

        for (id, component) in self.insert_components.lock().unwrap().drain(0..) {
            components.entry(id).and_modify(|components| {
                components.insert((&*component.clone().as_any()).type_id(), component.clone());
            });
        }
    }

    /// Returns an iterator to entities and their components matching a subset of components.
    pub fn components<'a, Components>(&'a self) -> Iter<Components::Item>
    where
        Components: ToComponentsIter<'a>,
    {
        Components::to_iter(self)
    }

    /// Returns a [`Facade`](super::Facade) for the entity with the active component of type `C`.
    pub fn active<C: Component + Active>(&self) -> Option<Facade> {
        let active = self.active.lock().unwrap();
        active.get(&TypeId::of::<C>()).and_then(|id| self.get(id))
    }

    pub(super) fn get_component<C>(&self, id: &String) -> Option<Arc<C>>
    where
        C: Component,
    {
        self.get_component_dyn(id, &TypeId::of::<C>())
            .map(|component| component.as_any())
            .and_then(|component| component.downcast::<C>().ok())
    }

    pub(super) fn get_component_dyn(
        &self,
        id: &String,
        component_type: &TypeId,
    ) -> Option<Arc<dyn Component>> {
        unsafe { &*self.components.get() }
            .get(id)
            .and_then(|components| components.get(component_type))
            .cloned()
    }

    pub(super) fn activate_component<C: Component + Active>(&self, id: String) {
        let mut active = self.active.lock().unwrap();
        active.insert(TypeId::of::<C>(), id);
    }

    pub(super) fn insert_component<C>(&self, id: String, component: C)
    where
        C: Component,
    {
        self.insert_component_dyn(id, Arc::new(component));
    }

    pub(super) fn insert_component_dyn(&self, id: String, component: Arc<dyn Component>) {
        self.insert_components.lock().unwrap().push((id, component));
    }

    pub(super) fn remove_component<C>(&self, id: String)
    where
        C: Component,
    {
        self.remove_component_by_type(id, TypeId::of::<C>());
    }

    pub(super) fn remove_component_by_type(&self, id: String, component_type: TypeId) {
        self.delete_components
            .lock()
            .unwrap()
            .push((id, component_type));
    }

    pub(super) fn get_component_types(&self, id: &String) -> Vec<TypeId> {
        unsafe { &*self.components.get() }
            .get(id)
            .map(|components| components.keys().cloned().collect::<Vec<_>>())
            .unwrap_or_default()
    }
}

unsafe impl Send for Entities {}
unsafe impl Sync for Entities {}

impl Serialize for Entities {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(unsafe { &*self.components.get() }.len()))?;
        for (entity, components) in unsafe { &*self.components.get() }.iter() {
            let components = components
                .values()
                .map(|component| component.as_ref())
                .collect::<Vec<_>>();
            map.serialize_entry(entity, &components)?;
        }
        map.end()
    }
}

/// The error type for entity creation.
#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct EntityCreationError {
    id: String,
}

impl Display for EntityCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "entity with id '{}' already exists", self.id)
    }
}

impl std::error::Error for EntityCreationError {}

/// Mixed mutability component iterator.
pub struct Iter<'a, Comps> {
    inner: Box<dyn Iterator<Item = Comps> + 'a>,
    _phantom: PhantomData<&'a Comps>,
}

impl<'a, Comps> Iter<'a, Comps> {
    fn new(inner: Box<dyn Iterator<Item = Comps> + 'a>) -> Self {
        Self {
            inner,
            _phantom: Default::default(),
        }
    }
}

impl<'a, Comps> Iterator for Iter<'a, Comps> {
    type Item = Comps;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

pub trait ToComponentsIter<'a>: 'a {
    type Item;

    fn to_iter(entities: &'a Entities) -> Iter<'a, Self::Item> {
        Iter::new(Box::new(
            unsafe { &*entities.components.get() }
                .keys()
                .flat_map(|id| entities.get(id))
                .filter(Self::filter)
                .map(Self::to_item),
        ))
    }

    fn filter(entity: &Facade<'a>) -> bool;

    fn to_item(entity: Facade<'a>) -> Self::Item;
}

macro_rules! impl_to_components_iter_tuple {
        ({$($name:ident)+}, {$($mutability:ty)+}, {$($ref:tt)+}, {$($accessor:ident)+}) => {
        impl<'a, $($name: Component + Clone),+> ToComponentsIter<'a> for ($($mutability,)+) {
            type Item = (Facade<'a>, $(super::$ref<'a, $name>,)+);

            fn filter(entity: &Facade<'a>) -> bool {
                $(entity.get::<$name>().is_some())&&+
            }

            fn to_item(entity: Facade<'a>) -> Self::Item {
                (entity.clone(), $(entity.$accessor::<$name>().unwrap(),)+)
            }
        }
    };
}

impl_to_components_iter_tuple! { { A }, { &'a A }, { Ref }, { get } }
impl_to_components_iter_tuple! { { A }, { &'a mut A }, { RefMut }, { get_mut } }
impl_to_components_iter_tuple! { { A B }, { &'a A &'a B }, { Ref Ref }, { get get } }
impl_to_components_iter_tuple! { { A B }, { &'a A &'a mut B }, { Ref RefMut }, { get get_mut } }
impl_to_components_iter_tuple! { { A B }, { &'a mut A &'a B }, { RefMut Ref }, { get_mut get } }
impl_to_components_iter_tuple! { { A B }, { &'a mut A &'a mut B }, { RefMut RefMut }, { get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C }, { &'a A &'a B &'a C }, { Ref Ref Ref }, { get get get } }
impl_to_components_iter_tuple! { { A B C }, { &'a A &'a B &'a mut C }, { Ref Ref RefMut }, { get get get_mut } }
impl_to_components_iter_tuple! { { A B C }, { &'a A &'a mut B &'a C }, { Ref RefMut Ref }, { get get_mut get } }
impl_to_components_iter_tuple! { { A B C }, { &'a A &'a mut B &'a mut C }, { Ref RefMut RefMut }, { get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C }, { &'a mut A &'a B &'a C }, { RefMut Ref Ref }, { get_mut get get } }
impl_to_components_iter_tuple! { { A B C }, { &'a mut A &'a B &'a mut C }, { RefMut Ref RefMut }, { get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C }, { &'a mut A &'a mut B &'a C }, { RefMut RefMut Ref }, { get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C }, { &'a mut A &'a mut B &'a mut C }, { RefMut RefMut RefMut }, { get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D }, { &'a A &'a B &'a C &'a D }, { Ref Ref Ref Ref }, { get get get get } }
impl_to_components_iter_tuple! { { A B C D }, { &'a A &'a B &'a C &'a mut D }, { Ref Ref Ref RefMut }, { get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D }, { &'a A &'a B &'a mut C &'a D }, { Ref Ref RefMut Ref }, { get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D }, { &'a A &'a B &'a mut C &'a mut D }, { Ref Ref RefMut RefMut }, { get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D }, { &'a A &'a mut B &'a C &'a D }, { Ref RefMut Ref Ref }, { get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D }, { &'a A &'a mut B &'a C &'a mut D }, { Ref RefMut Ref RefMut }, { get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D }, { &'a A &'a mut B &'a mut C &'a D }, { Ref RefMut RefMut Ref }, { get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D }, { &'a A &'a mut B &'a mut C &'a mut D }, { Ref RefMut RefMut RefMut }, { get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D }, { &'a mut A &'a B &'a C &'a D }, { RefMut Ref Ref Ref }, { get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D }, { &'a mut A &'a B &'a C &'a mut D }, { RefMut Ref Ref RefMut }, { get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D }, { &'a mut A &'a B &'a mut C &'a D }, { RefMut Ref RefMut Ref }, { get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D }, { &'a mut A &'a B &'a mut C &'a mut D }, { RefMut Ref RefMut RefMut }, { get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D }, { &'a mut A &'a mut B &'a C &'a D }, { RefMut RefMut Ref Ref }, { get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D }, { &'a mut A &'a mut B &'a C &'a mut D }, { RefMut RefMut Ref RefMut }, { get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D }, { &'a mut A &'a mut B &'a mut C &'a D }, { RefMut RefMut RefMut Ref }, { get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D }, { &'a mut A &'a mut B &'a mut C &'a mut D }, { RefMut RefMut RefMut RefMut }, { get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a B &'a C &'a D &'a E }, { Ref Ref Ref Ref Ref }, { get get get get get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a B &'a C &'a D &'a mut E }, { Ref Ref Ref Ref RefMut }, { get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a B &'a C &'a mut D &'a E }, { Ref Ref Ref RefMut Ref }, { get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a B &'a C &'a mut D &'a mut E }, { Ref Ref Ref RefMut RefMut }, { get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a B &'a mut C &'a D &'a E }, { Ref Ref RefMut Ref Ref }, { get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a B &'a mut C &'a D &'a mut E }, { Ref Ref RefMut Ref RefMut }, { get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a B &'a mut C &'a mut D &'a E }, { Ref Ref RefMut RefMut Ref }, { get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a B &'a mut C &'a mut D &'a mut E }, { Ref Ref RefMut RefMut RefMut }, { get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a mut B &'a C &'a D &'a E }, { Ref RefMut Ref Ref Ref }, { get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a mut B &'a C &'a D &'a mut E }, { Ref RefMut Ref Ref RefMut }, { get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a mut B &'a C &'a mut D &'a E }, { Ref RefMut Ref RefMut Ref }, { get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a mut B &'a C &'a mut D &'a mut E }, { Ref RefMut Ref RefMut RefMut }, { get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a mut B &'a mut C &'a D &'a E }, { Ref RefMut RefMut Ref Ref }, { get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a mut B &'a mut C &'a D &'a mut E }, { Ref RefMut RefMut Ref RefMut }, { get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a mut B &'a mut C &'a mut D &'a E }, { Ref RefMut RefMut RefMut Ref }, { get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E }, { Ref RefMut RefMut RefMut RefMut }, { get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a B &'a C &'a D &'a E }, { RefMut Ref Ref Ref Ref }, { get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a B &'a C &'a D &'a mut E }, { RefMut Ref Ref Ref RefMut }, { get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a B &'a C &'a mut D &'a E }, { RefMut Ref Ref RefMut Ref }, { get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a B &'a C &'a mut D &'a mut E }, { RefMut Ref Ref RefMut RefMut }, { get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a B &'a mut C &'a D &'a E }, { RefMut Ref RefMut Ref Ref }, { get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a B &'a mut C &'a D &'a mut E }, { RefMut Ref RefMut Ref RefMut }, { get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a B &'a mut C &'a mut D &'a E }, { RefMut Ref RefMut RefMut Ref }, { get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E }, { RefMut Ref RefMut RefMut RefMut }, { get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a mut B &'a C &'a D &'a E }, { RefMut RefMut Ref Ref Ref }, { get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a mut B &'a C &'a D &'a mut E }, { RefMut RefMut Ref Ref RefMut }, { get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a mut B &'a C &'a mut D &'a E }, { RefMut RefMut Ref RefMut Ref }, { get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E }, { RefMut RefMut Ref RefMut RefMut }, { get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a mut B &'a mut C &'a D &'a E }, { RefMut RefMut RefMut Ref Ref }, { get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E }, { RefMut RefMut RefMut Ref RefMut }, { get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E }, { RefMut RefMut RefMut RefMut Ref }, { get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E }, { RefMut RefMut RefMut RefMut RefMut }, { get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a C &'a D &'a E &'a F }, { Ref Ref Ref Ref Ref Ref }, { get get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a C &'a D &'a E &'a mut F }, { Ref Ref Ref Ref Ref RefMut }, { get get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a C &'a D &'a mut E &'a F }, { Ref Ref Ref Ref RefMut Ref }, { get get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a C &'a D &'a mut E &'a mut F }, { Ref Ref Ref Ref RefMut RefMut }, { get get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a C &'a mut D &'a E &'a F }, { Ref Ref Ref RefMut Ref Ref }, { get get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a C &'a mut D &'a E &'a mut F }, { Ref Ref Ref RefMut Ref RefMut }, { get get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a F }, { Ref Ref Ref RefMut RefMut Ref }, { get get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a mut F }, { Ref Ref Ref RefMut RefMut RefMut }, { get get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a mut C &'a D &'a E &'a F }, { Ref Ref RefMut Ref Ref Ref }, { get get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a mut C &'a D &'a E &'a mut F }, { Ref Ref RefMut Ref Ref RefMut }, { get get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a F }, { Ref Ref RefMut Ref RefMut Ref }, { get get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a mut F }, { Ref Ref RefMut Ref RefMut RefMut }, { get get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a F }, { Ref Ref RefMut RefMut Ref Ref }, { get get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a mut F }, { Ref Ref RefMut RefMut Ref RefMut }, { get get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a F }, { Ref Ref RefMut RefMut RefMut Ref }, { get get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a mut F }, { Ref Ref RefMut RefMut RefMut RefMut }, { get get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a C &'a D &'a E &'a F }, { Ref RefMut Ref Ref Ref Ref }, { get get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a C &'a D &'a E &'a mut F }, { Ref RefMut Ref Ref Ref RefMut }, { get get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a F }, { Ref RefMut Ref Ref RefMut Ref }, { get get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a mut F }, { Ref RefMut Ref Ref RefMut RefMut }, { get get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a F }, { Ref RefMut Ref RefMut Ref Ref }, { get get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a mut F }, { Ref RefMut Ref RefMut Ref RefMut }, { get get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a F }, { Ref RefMut Ref RefMut RefMut Ref }, { get get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a mut F }, { Ref RefMut Ref RefMut RefMut RefMut }, { get get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a F }, { Ref RefMut RefMut Ref Ref Ref }, { get get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a mut F }, { Ref RefMut RefMut Ref Ref RefMut }, { get get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a F }, { Ref RefMut RefMut Ref RefMut Ref }, { get get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a mut F }, { Ref RefMut RefMut Ref RefMut RefMut }, { get get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a F }, { Ref RefMut RefMut RefMut Ref Ref }, { get get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a mut F }, { Ref RefMut RefMut RefMut Ref RefMut }, { get get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a F }, { Ref RefMut RefMut RefMut RefMut Ref }, { get get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F }, { Ref RefMut RefMut RefMut RefMut RefMut }, { get get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a C &'a D &'a E &'a F }, { RefMut Ref Ref Ref Ref Ref }, { get_mut get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a C &'a D &'a E &'a mut F }, { RefMut Ref Ref Ref Ref RefMut }, { get_mut get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a F }, { RefMut Ref Ref Ref RefMut Ref }, { get_mut get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a mut F }, { RefMut Ref Ref Ref RefMut RefMut }, { get_mut get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a F }, { RefMut Ref Ref RefMut Ref Ref }, { get_mut get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a mut F }, { RefMut Ref Ref RefMut Ref RefMut }, { get_mut get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a F }, { RefMut Ref Ref RefMut RefMut Ref }, { get_mut get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a mut F }, { RefMut Ref Ref RefMut RefMut RefMut }, { get_mut get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a F }, { RefMut Ref RefMut Ref Ref Ref }, { get_mut get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a mut F }, { RefMut Ref RefMut Ref Ref RefMut }, { get_mut get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a F }, { RefMut Ref RefMut Ref RefMut Ref }, { get_mut get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a mut F }, { RefMut Ref RefMut Ref RefMut RefMut }, { get_mut get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a F }, { RefMut Ref RefMut RefMut Ref Ref }, { get_mut get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a mut F }, { RefMut Ref RefMut RefMut Ref RefMut }, { get_mut get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a F }, { RefMut Ref RefMut RefMut RefMut Ref }, { get_mut get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a mut F }, { RefMut Ref RefMut RefMut RefMut RefMut }, { get_mut get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a F }, { RefMut RefMut Ref Ref Ref Ref }, { get_mut get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a mut F }, { RefMut RefMut Ref Ref Ref RefMut }, { get_mut get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a F }, { RefMut RefMut Ref Ref RefMut Ref }, { get_mut get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a mut F }, { RefMut RefMut Ref Ref RefMut RefMut }, { get_mut get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a F }, { RefMut RefMut Ref RefMut Ref Ref }, { get_mut get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a mut F }, { RefMut RefMut Ref RefMut Ref RefMut }, { get_mut get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a F }, { RefMut RefMut Ref RefMut RefMut Ref }, { get_mut get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a mut F }, { RefMut RefMut Ref RefMut RefMut RefMut }, { get_mut get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a F }, { RefMut RefMut RefMut Ref Ref Ref }, { get_mut get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a mut F }, { RefMut RefMut RefMut Ref Ref RefMut }, { get_mut get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a F }, { RefMut RefMut RefMut Ref RefMut Ref }, { get_mut get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a mut F }, { RefMut RefMut RefMut Ref RefMut RefMut }, { get_mut get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a F }, { RefMut RefMut RefMut RefMut Ref Ref }, { get_mut get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a mut F }, { RefMut RefMut RefMut RefMut Ref RefMut }, { get_mut get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a F }, { RefMut RefMut RefMut RefMut RefMut Ref }, { get_mut get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F }, { RefMut RefMut RefMut RefMut RefMut RefMut }, { get_mut get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a D &'a E &'a F &'a G }, { Ref Ref Ref Ref Ref Ref Ref }, { get get get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a D &'a E &'a F &'a mut G }, { Ref Ref Ref Ref Ref Ref RefMut }, { get get get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a D &'a E &'a mut F &'a G }, { Ref Ref Ref Ref Ref RefMut Ref }, { get get get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a D &'a E &'a mut F &'a mut G }, { Ref Ref Ref Ref Ref RefMut RefMut }, { get get get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a D &'a mut E &'a F &'a G }, { Ref Ref Ref Ref RefMut Ref Ref }, { get get get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a D &'a mut E &'a F &'a mut G }, { Ref Ref Ref Ref RefMut Ref RefMut }, { get get get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a D &'a mut E &'a mut F &'a G }, { Ref Ref Ref Ref RefMut RefMut Ref }, { get get get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a D &'a mut E &'a mut F &'a mut G }, { Ref Ref Ref Ref RefMut RefMut RefMut }, { get get get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a mut D &'a E &'a F &'a G }, { Ref Ref Ref RefMut Ref Ref Ref }, { get get get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a mut D &'a E &'a F &'a mut G }, { Ref Ref Ref RefMut Ref Ref RefMut }, { get get get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a mut D &'a E &'a mut F &'a G }, { Ref Ref Ref RefMut Ref RefMut Ref }, { get get get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a mut D &'a E &'a mut F &'a mut G }, { Ref Ref Ref RefMut Ref RefMut RefMut }, { get get get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a F &'a G }, { Ref Ref Ref RefMut RefMut Ref Ref }, { get get get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a F &'a mut G }, { Ref Ref Ref RefMut RefMut Ref RefMut }, { get get get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a G }, { Ref Ref Ref RefMut RefMut RefMut Ref }, { get get get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a mut G }, { Ref Ref Ref RefMut RefMut RefMut RefMut }, { get get get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a D &'a E &'a F &'a G }, { Ref Ref RefMut Ref Ref Ref Ref }, { get get get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a D &'a E &'a F &'a mut G }, { Ref Ref RefMut Ref Ref Ref RefMut }, { get get get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a D &'a E &'a mut F &'a G }, { Ref Ref RefMut Ref Ref RefMut Ref }, { get get get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a D &'a E &'a mut F &'a mut G }, { Ref Ref RefMut Ref Ref RefMut RefMut }, { get get get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a F &'a G }, { Ref Ref RefMut Ref RefMut Ref Ref }, { get get get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a F &'a mut G }, { Ref Ref RefMut Ref RefMut Ref RefMut }, { get get get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a G }, { Ref Ref RefMut Ref RefMut RefMut Ref }, { get get get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a mut G }, { Ref Ref RefMut Ref RefMut RefMut RefMut }, { get get get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a F &'a G }, { Ref Ref RefMut RefMut Ref Ref Ref }, { get get get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a F &'a mut G }, { Ref Ref RefMut RefMut Ref Ref RefMut }, { get get get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a G }, { Ref Ref RefMut RefMut Ref RefMut Ref }, { get get get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a mut G }, { Ref Ref RefMut RefMut Ref RefMut RefMut }, { get get get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a G }, { Ref Ref RefMut RefMut RefMut Ref Ref }, { get get get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a mut G }, { Ref Ref RefMut RefMut RefMut Ref RefMut }, { get get get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a G }, { Ref Ref RefMut RefMut RefMut RefMut Ref }, { get get get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G }, { Ref Ref RefMut RefMut RefMut RefMut RefMut }, { get get get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a D &'a E &'a F &'a G }, { Ref RefMut Ref Ref Ref Ref Ref }, { get get_mut get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a D &'a E &'a F &'a mut G }, { Ref RefMut Ref Ref Ref Ref RefMut }, { get get_mut get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a D &'a E &'a mut F &'a G }, { Ref RefMut Ref Ref Ref RefMut Ref }, { get get_mut get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a D &'a E &'a mut F &'a mut G }, { Ref RefMut Ref Ref Ref RefMut RefMut }, { get get_mut get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a F &'a G }, { Ref RefMut Ref Ref RefMut Ref Ref }, { get get_mut get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a F &'a mut G }, { Ref RefMut Ref Ref RefMut Ref RefMut }, { get get_mut get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a G }, { Ref RefMut Ref Ref RefMut RefMut Ref }, { get get_mut get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a mut G }, { Ref RefMut Ref Ref RefMut RefMut RefMut }, { get get_mut get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a F &'a G }, { Ref RefMut Ref RefMut Ref Ref Ref }, { get get_mut get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a F &'a mut G }, { Ref RefMut Ref RefMut Ref Ref RefMut }, { get get_mut get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a G }, { Ref RefMut Ref RefMut Ref RefMut Ref }, { get get_mut get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a mut G }, { Ref RefMut Ref RefMut Ref RefMut RefMut }, { get get_mut get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a G }, { Ref RefMut Ref RefMut RefMut Ref Ref }, { get get_mut get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a mut G }, { Ref RefMut Ref RefMut RefMut Ref RefMut }, { get get_mut get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a G }, { Ref RefMut Ref RefMut RefMut RefMut Ref }, { get get_mut get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a mut G }, { Ref RefMut Ref RefMut RefMut RefMut RefMut }, { get get_mut get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a F &'a G }, { Ref RefMut RefMut Ref Ref Ref Ref }, { get get_mut get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a F &'a mut G }, { Ref RefMut RefMut Ref Ref Ref RefMut }, { get get_mut get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a G }, { Ref RefMut RefMut Ref Ref RefMut Ref }, { get get_mut get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a mut G }, { Ref RefMut RefMut Ref Ref RefMut RefMut }, { get get_mut get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a G }, { Ref RefMut RefMut Ref RefMut Ref Ref }, { get get_mut get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a mut G }, { Ref RefMut RefMut Ref RefMut Ref RefMut }, { get get_mut get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a G }, { Ref RefMut RefMut Ref RefMut RefMut Ref }, { get get_mut get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a mut G }, { Ref RefMut RefMut Ref RefMut RefMut RefMut }, { get get_mut get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a G }, { Ref RefMut RefMut RefMut Ref Ref Ref }, { get get_mut get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a mut G }, { Ref RefMut RefMut RefMut Ref Ref RefMut }, { get get_mut get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a G }, { Ref RefMut RefMut RefMut Ref RefMut Ref }, { get get_mut get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a mut G }, { Ref RefMut RefMut RefMut Ref RefMut RefMut }, { get get_mut get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a G }, { Ref RefMut RefMut RefMut RefMut Ref Ref }, { get get_mut get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a mut G }, { Ref RefMut RefMut RefMut RefMut Ref RefMut }, { get get_mut get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a G }, { Ref RefMut RefMut RefMut RefMut RefMut Ref }, { get get_mut get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G }, { Ref RefMut RefMut RefMut RefMut RefMut RefMut }, { get get_mut get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a D &'a E &'a F &'a G }, { RefMut Ref Ref Ref Ref Ref Ref }, { get_mut get get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a D &'a E &'a F &'a mut G }, { RefMut Ref Ref Ref Ref Ref RefMut }, { get_mut get get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a D &'a E &'a mut F &'a G }, { RefMut Ref Ref Ref Ref RefMut Ref }, { get_mut get get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a D &'a E &'a mut F &'a mut G }, { RefMut Ref Ref Ref Ref RefMut RefMut }, { get_mut get get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a F &'a G }, { RefMut Ref Ref Ref RefMut Ref Ref }, { get_mut get get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a F &'a mut G }, { RefMut Ref Ref Ref RefMut Ref RefMut }, { get_mut get get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a mut F &'a G }, { RefMut Ref Ref Ref RefMut RefMut Ref }, { get_mut get get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a mut F &'a mut G }, { RefMut Ref Ref Ref RefMut RefMut RefMut }, { get_mut get get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a F &'a G }, { RefMut Ref Ref RefMut Ref Ref Ref }, { get_mut get get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a F &'a mut G }, { RefMut Ref Ref RefMut Ref Ref RefMut }, { get_mut get get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a mut F &'a G }, { RefMut Ref Ref RefMut Ref RefMut Ref }, { get_mut get get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a mut F &'a mut G }, { RefMut Ref Ref RefMut Ref RefMut RefMut }, { get_mut get get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a F &'a G }, { RefMut Ref Ref RefMut RefMut Ref Ref }, { get_mut get get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a F &'a mut G }, { RefMut Ref Ref RefMut RefMut Ref RefMut }, { get_mut get get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a G }, { RefMut Ref Ref RefMut RefMut RefMut Ref }, { get_mut get get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a mut G }, { RefMut Ref Ref RefMut RefMut RefMut RefMut }, { get_mut get get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a F &'a G }, { RefMut Ref RefMut Ref Ref Ref Ref }, { get_mut get get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a F &'a mut G }, { RefMut Ref RefMut Ref Ref Ref RefMut }, { get_mut get get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a mut F &'a G }, { RefMut Ref RefMut Ref Ref RefMut Ref }, { get_mut get get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a mut F &'a mut G }, { RefMut Ref RefMut Ref Ref RefMut RefMut }, { get_mut get get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a F &'a G }, { RefMut Ref RefMut Ref RefMut Ref Ref }, { get_mut get get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a F &'a mut G }, { RefMut Ref RefMut Ref RefMut Ref RefMut }, { get_mut get get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a G }, { RefMut Ref RefMut Ref RefMut RefMut Ref }, { get_mut get get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a mut G }, { RefMut Ref RefMut Ref RefMut RefMut RefMut }, { get_mut get get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a F &'a G }, { RefMut Ref RefMut RefMut Ref Ref Ref }, { get_mut get get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a F &'a mut G }, { RefMut Ref RefMut RefMut Ref Ref RefMut }, { get_mut get get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a G }, { RefMut Ref RefMut RefMut Ref RefMut Ref }, { get_mut get get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a mut G }, { RefMut Ref RefMut RefMut Ref RefMut RefMut }, { get_mut get get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a G }, { RefMut Ref RefMut RefMut RefMut Ref Ref }, { get_mut get get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a mut G }, { RefMut Ref RefMut RefMut RefMut Ref RefMut }, { get_mut get get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a G }, { RefMut Ref RefMut RefMut RefMut RefMut Ref }, { get_mut get get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G }, { RefMut Ref RefMut RefMut RefMut RefMut RefMut }, { get_mut get get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a F &'a G }, { RefMut RefMut Ref Ref Ref Ref Ref }, { get_mut get_mut get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a F &'a mut G }, { RefMut RefMut Ref Ref Ref Ref RefMut }, { get_mut get_mut get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a mut F &'a G }, { RefMut RefMut Ref Ref Ref RefMut Ref }, { get_mut get_mut get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a mut F &'a mut G }, { RefMut RefMut Ref Ref Ref RefMut RefMut }, { get_mut get_mut get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a F &'a G }, { RefMut RefMut Ref Ref RefMut Ref Ref }, { get_mut get_mut get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a F &'a mut G }, { RefMut RefMut Ref Ref RefMut Ref RefMut }, { get_mut get_mut get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a G }, { RefMut RefMut Ref Ref RefMut RefMut Ref }, { get_mut get_mut get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a mut G }, { RefMut RefMut Ref Ref RefMut RefMut RefMut }, { get_mut get_mut get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a F &'a G }, { RefMut RefMut Ref RefMut Ref Ref Ref }, { get_mut get_mut get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a F &'a mut G }, { RefMut RefMut Ref RefMut Ref Ref RefMut }, { get_mut get_mut get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a G }, { RefMut RefMut Ref RefMut Ref RefMut Ref }, { get_mut get_mut get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a mut G }, { RefMut RefMut Ref RefMut Ref RefMut RefMut }, { get_mut get_mut get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a G }, { RefMut RefMut Ref RefMut RefMut Ref Ref }, { get_mut get_mut get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a mut G }, { RefMut RefMut Ref RefMut RefMut Ref RefMut }, { get_mut get_mut get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a G }, { RefMut RefMut Ref RefMut RefMut RefMut Ref }, { get_mut get_mut get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a mut G }, { RefMut RefMut Ref RefMut RefMut RefMut RefMut }, { get_mut get_mut get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a F &'a G }, { RefMut RefMut RefMut Ref Ref Ref Ref }, { get_mut get_mut get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a F &'a mut G }, { RefMut RefMut RefMut Ref Ref Ref RefMut }, { get_mut get_mut get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a G }, { RefMut RefMut RefMut Ref Ref RefMut Ref }, { get_mut get_mut get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a mut G }, { RefMut RefMut RefMut Ref Ref RefMut RefMut }, { get_mut get_mut get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a G }, { RefMut RefMut RefMut Ref RefMut Ref Ref }, { get_mut get_mut get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a mut G }, { RefMut RefMut RefMut Ref RefMut Ref RefMut }, { get_mut get_mut get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a G }, { RefMut RefMut RefMut Ref RefMut RefMut Ref }, { get_mut get_mut get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a mut G }, { RefMut RefMut RefMut Ref RefMut RefMut RefMut }, { get_mut get_mut get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a G }, { RefMut RefMut RefMut RefMut Ref Ref Ref }, { get_mut get_mut get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a mut G }, { RefMut RefMut RefMut RefMut Ref Ref RefMut }, { get_mut get_mut get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a G }, { RefMut RefMut RefMut RefMut Ref RefMut Ref }, { get_mut get_mut get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a mut G }, { RefMut RefMut RefMut RefMut Ref RefMut RefMut }, { get_mut get_mut get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a G }, { RefMut RefMut RefMut RefMut RefMut Ref Ref }, { get_mut get_mut get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a mut G }, { RefMut RefMut RefMut RefMut RefMut Ref RefMut }, { get_mut get_mut get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a G }, { RefMut RefMut RefMut RefMut RefMut RefMut Ref }, { get_mut get_mut get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G }, { RefMut RefMut RefMut RefMut RefMut RefMut RefMut }, { get_mut get_mut get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a E &'a F &'a G &'a H }, { Ref Ref Ref Ref Ref Ref Ref Ref }, { get get get get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a E &'a F &'a G &'a mut H }, { Ref Ref Ref Ref Ref Ref Ref RefMut }, { get get get get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a E &'a F &'a mut G &'a H }, { Ref Ref Ref Ref Ref Ref RefMut Ref }, { get get get get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a E &'a F &'a mut G &'a mut H }, { Ref Ref Ref Ref Ref Ref RefMut RefMut }, { get get get get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a E &'a mut F &'a G &'a H }, { Ref Ref Ref Ref Ref RefMut Ref Ref }, { get get get get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a E &'a mut F &'a G &'a mut H }, { Ref Ref Ref Ref Ref RefMut Ref RefMut }, { get get get get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a E &'a mut F &'a mut G &'a H }, { Ref Ref Ref Ref Ref RefMut RefMut Ref }, { get get get get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a E &'a mut F &'a mut G &'a mut H }, { Ref Ref Ref Ref Ref RefMut RefMut RefMut }, { get get get get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a mut E &'a F &'a G &'a H }, { Ref Ref Ref Ref RefMut Ref Ref Ref }, { get get get get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a mut E &'a F &'a G &'a mut H }, { Ref Ref Ref Ref RefMut Ref Ref RefMut }, { get get get get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a mut E &'a F &'a mut G &'a H }, { Ref Ref Ref Ref RefMut Ref RefMut Ref }, { get get get get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a mut E &'a F &'a mut G &'a mut H }, { Ref Ref Ref Ref RefMut Ref RefMut RefMut }, { get get get get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a mut E &'a mut F &'a G &'a H }, { Ref Ref Ref Ref RefMut RefMut Ref Ref }, { get get get get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a mut E &'a mut F &'a G &'a mut H }, { Ref Ref Ref Ref RefMut RefMut Ref RefMut }, { get get get get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a mut E &'a mut F &'a mut G &'a H }, { Ref Ref Ref Ref RefMut RefMut RefMut Ref }, { get get get get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a D &'a mut E &'a mut F &'a mut G &'a mut H }, { Ref Ref Ref Ref RefMut RefMut RefMut RefMut }, { get get get get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a E &'a F &'a G &'a H }, { Ref Ref Ref RefMut Ref Ref Ref Ref }, { get get get get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a E &'a F &'a G &'a mut H }, { Ref Ref Ref RefMut Ref Ref Ref RefMut }, { get get get get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a E &'a F &'a mut G &'a H }, { Ref Ref Ref RefMut Ref Ref RefMut Ref }, { get get get get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a E &'a F &'a mut G &'a mut H }, { Ref Ref Ref RefMut Ref Ref RefMut RefMut }, { get get get get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a E &'a mut F &'a G &'a H }, { Ref Ref Ref RefMut Ref RefMut Ref Ref }, { get get get get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a E &'a mut F &'a G &'a mut H }, { Ref Ref Ref RefMut Ref RefMut Ref RefMut }, { get get get get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a E &'a mut F &'a mut G &'a H }, { Ref Ref Ref RefMut Ref RefMut RefMut Ref }, { get get get get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a E &'a mut F &'a mut G &'a mut H }, { Ref Ref Ref RefMut Ref RefMut RefMut RefMut }, { get get get get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a F &'a G &'a H }, { Ref Ref Ref RefMut RefMut Ref Ref Ref }, { get get get get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a F &'a G &'a mut H }, { Ref Ref Ref RefMut RefMut Ref Ref RefMut }, { get get get get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a F &'a mut G &'a H }, { Ref Ref Ref RefMut RefMut Ref RefMut Ref }, { get get get get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a F &'a mut G &'a mut H }, { Ref Ref Ref RefMut RefMut Ref RefMut RefMut }, { get get get get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a G &'a H }, { Ref Ref Ref RefMut RefMut RefMut Ref Ref }, { get get get get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a G &'a mut H }, { Ref Ref Ref RefMut RefMut RefMut Ref RefMut }, { get get get get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a mut G &'a H }, { Ref Ref Ref RefMut RefMut RefMut RefMut Ref }, { get get get get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a mut G &'a mut H }, { Ref Ref Ref RefMut RefMut RefMut RefMut RefMut }, { get get get get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a E &'a F &'a G &'a H }, { Ref Ref RefMut Ref Ref Ref Ref Ref }, { get get get_mut get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a E &'a F &'a G &'a mut H }, { Ref Ref RefMut Ref Ref Ref Ref RefMut }, { get get get_mut get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a E &'a F &'a mut G &'a H }, { Ref Ref RefMut Ref Ref Ref RefMut Ref }, { get get get_mut get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a E &'a F &'a mut G &'a mut H }, { Ref Ref RefMut Ref Ref Ref RefMut RefMut }, { get get get_mut get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a E &'a mut F &'a G &'a H }, { Ref Ref RefMut Ref Ref RefMut Ref Ref }, { get get get_mut get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a E &'a mut F &'a G &'a mut H }, { Ref Ref RefMut Ref Ref RefMut Ref RefMut }, { get get get_mut get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a E &'a mut F &'a mut G &'a H }, { Ref Ref RefMut Ref Ref RefMut RefMut Ref }, { get get get_mut get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a E &'a mut F &'a mut G &'a mut H }, { Ref Ref RefMut Ref Ref RefMut RefMut RefMut }, { get get get_mut get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a F &'a G &'a H }, { Ref Ref RefMut Ref RefMut Ref Ref Ref }, { get get get_mut get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a F &'a G &'a mut H }, { Ref Ref RefMut Ref RefMut Ref Ref RefMut }, { get get get_mut get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a F &'a mut G &'a H }, { Ref Ref RefMut Ref RefMut Ref RefMut Ref }, { get get get_mut get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a F &'a mut G &'a mut H }, { Ref Ref RefMut Ref RefMut Ref RefMut RefMut }, { get get get_mut get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a G &'a H }, { Ref Ref RefMut Ref RefMut RefMut Ref Ref }, { get get get_mut get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a G &'a mut H }, { Ref Ref RefMut Ref RefMut RefMut Ref RefMut }, { get get get_mut get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a mut G &'a H }, { Ref Ref RefMut Ref RefMut RefMut RefMut Ref }, { get get get_mut get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a mut G &'a mut H }, { Ref Ref RefMut Ref RefMut RefMut RefMut RefMut }, { get get get_mut get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a F &'a G &'a H }, { Ref Ref RefMut RefMut Ref Ref Ref Ref }, { get get get_mut get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a F &'a G &'a mut H }, { Ref Ref RefMut RefMut Ref Ref Ref RefMut }, { get get get_mut get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a F &'a mut G &'a H }, { Ref Ref RefMut RefMut Ref Ref RefMut Ref }, { get get get_mut get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a F &'a mut G &'a mut H }, { Ref Ref RefMut RefMut Ref Ref RefMut RefMut }, { get get get_mut get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a G &'a H }, { Ref Ref RefMut RefMut Ref RefMut Ref Ref }, { get get get_mut get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a G &'a mut H }, { Ref Ref RefMut RefMut Ref RefMut Ref RefMut }, { get get get_mut get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a mut G &'a H }, { Ref Ref RefMut RefMut Ref RefMut RefMut Ref }, { get get get_mut get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a mut G &'a mut H }, { Ref Ref RefMut RefMut Ref RefMut RefMut RefMut }, { get get get_mut get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a G &'a H }, { Ref Ref RefMut RefMut RefMut Ref Ref Ref }, { get get get_mut get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a G &'a mut H }, { Ref Ref RefMut RefMut RefMut Ref Ref RefMut }, { get get get_mut get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a mut G &'a H }, { Ref Ref RefMut RefMut RefMut Ref RefMut Ref }, { get get get_mut get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a mut G &'a mut H }, { Ref Ref RefMut RefMut RefMut Ref RefMut RefMut }, { get get get_mut get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a G &'a H }, { Ref Ref RefMut RefMut RefMut RefMut Ref Ref }, { get get get_mut get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a G &'a mut H }, { Ref Ref RefMut RefMut RefMut RefMut Ref RefMut }, { get get get_mut get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G &'a H }, { Ref Ref RefMut RefMut RefMut RefMut RefMut Ref }, { get get get_mut get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G &'a mut H }, { Ref Ref RefMut RefMut RefMut RefMut RefMut RefMut }, { get get get_mut get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a E &'a F &'a G &'a H }, { Ref RefMut Ref Ref Ref Ref Ref Ref }, { get get_mut get get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a E &'a F &'a G &'a mut H }, { Ref RefMut Ref Ref Ref Ref Ref RefMut }, { get get_mut get get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a E &'a F &'a mut G &'a H }, { Ref RefMut Ref Ref Ref Ref RefMut Ref }, { get get_mut get get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a E &'a F &'a mut G &'a mut H }, { Ref RefMut Ref Ref Ref Ref RefMut RefMut }, { get get_mut get get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a E &'a mut F &'a G &'a H }, { Ref RefMut Ref Ref Ref RefMut Ref Ref }, { get get_mut get get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a E &'a mut F &'a G &'a mut H }, { Ref RefMut Ref Ref Ref RefMut Ref RefMut }, { get get_mut get get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a E &'a mut F &'a mut G &'a H }, { Ref RefMut Ref Ref Ref RefMut RefMut Ref }, { get get_mut get get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a E &'a mut F &'a mut G &'a mut H }, { Ref RefMut Ref Ref Ref RefMut RefMut RefMut }, { get get_mut get get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a F &'a G &'a H }, { Ref RefMut Ref Ref RefMut Ref Ref Ref }, { get get_mut get get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a F &'a G &'a mut H }, { Ref RefMut Ref Ref RefMut Ref Ref RefMut }, { get get_mut get get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a F &'a mut G &'a H }, { Ref RefMut Ref Ref RefMut Ref RefMut Ref }, { get get_mut get get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a F &'a mut G &'a mut H }, { Ref RefMut Ref Ref RefMut Ref RefMut RefMut }, { get get_mut get get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a G &'a H }, { Ref RefMut Ref Ref RefMut RefMut Ref Ref }, { get get_mut get get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a G &'a mut H }, { Ref RefMut Ref Ref RefMut RefMut Ref RefMut }, { get get_mut get get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a mut G &'a H }, { Ref RefMut Ref Ref RefMut RefMut RefMut Ref }, { get get_mut get get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a mut G &'a mut H }, { Ref RefMut Ref Ref RefMut RefMut RefMut RefMut }, { get get_mut get get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a F &'a G &'a H }, { Ref RefMut Ref RefMut Ref Ref Ref Ref }, { get get_mut get get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a F &'a G &'a mut H }, { Ref RefMut Ref RefMut Ref Ref Ref RefMut }, { get get_mut get get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a F &'a mut G &'a H }, { Ref RefMut Ref RefMut Ref Ref RefMut Ref }, { get get_mut get get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a F &'a mut G &'a mut H }, { Ref RefMut Ref RefMut Ref Ref RefMut RefMut }, { get get_mut get get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a G &'a H }, { Ref RefMut Ref RefMut Ref RefMut Ref Ref }, { get get_mut get get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a G &'a mut H }, { Ref RefMut Ref RefMut Ref RefMut Ref RefMut }, { get get_mut get get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a mut G &'a H }, { Ref RefMut Ref RefMut Ref RefMut RefMut Ref }, { get get_mut get get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a mut G &'a mut H }, { Ref RefMut Ref RefMut Ref RefMut RefMut RefMut }, { get get_mut get get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a G &'a H }, { Ref RefMut Ref RefMut RefMut Ref Ref Ref }, { get get_mut get get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a G &'a mut H }, { Ref RefMut Ref RefMut RefMut Ref Ref RefMut }, { get get_mut get get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a mut G &'a H }, { Ref RefMut Ref RefMut RefMut Ref RefMut Ref }, { get get_mut get get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a mut G &'a mut H }, { Ref RefMut Ref RefMut RefMut Ref RefMut RefMut }, { get get_mut get get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a G &'a H }, { Ref RefMut Ref RefMut RefMut RefMut Ref Ref }, { get get_mut get get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a G &'a mut H }, { Ref RefMut Ref RefMut RefMut RefMut Ref RefMut }, { get get_mut get get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a mut G &'a H }, { Ref RefMut Ref RefMut RefMut RefMut RefMut Ref }, { get get_mut get get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a mut G &'a mut H }, { Ref RefMut Ref RefMut RefMut RefMut RefMut RefMut }, { get get_mut get get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a F &'a G &'a H }, { Ref RefMut RefMut Ref Ref Ref Ref Ref }, { get get_mut get_mut get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a F &'a G &'a mut H }, { Ref RefMut RefMut Ref Ref Ref Ref RefMut }, { get get_mut get_mut get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a F &'a mut G &'a H }, { Ref RefMut RefMut Ref Ref Ref RefMut Ref }, { get get_mut get_mut get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a F &'a mut G &'a mut H }, { Ref RefMut RefMut Ref Ref Ref RefMut RefMut }, { get get_mut get_mut get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a G &'a H }, { Ref RefMut RefMut Ref Ref RefMut Ref Ref }, { get get_mut get_mut get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a G &'a mut H }, { Ref RefMut RefMut Ref Ref RefMut Ref RefMut }, { get get_mut get_mut get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a mut G &'a H }, { Ref RefMut RefMut Ref Ref RefMut RefMut Ref }, { get get_mut get_mut get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a mut G &'a mut H }, { Ref RefMut RefMut Ref Ref RefMut RefMut RefMut }, { get get_mut get_mut get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a G &'a H }, { Ref RefMut RefMut Ref RefMut Ref Ref Ref }, { get get_mut get_mut get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a G &'a mut H }, { Ref RefMut RefMut Ref RefMut Ref Ref RefMut }, { get get_mut get_mut get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a mut G &'a H }, { Ref RefMut RefMut Ref RefMut Ref RefMut Ref }, { get get_mut get_mut get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a mut G &'a mut H }, { Ref RefMut RefMut Ref RefMut Ref RefMut RefMut }, { get get_mut get_mut get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a G &'a H }, { Ref RefMut RefMut Ref RefMut RefMut Ref Ref }, { get get_mut get_mut get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a G &'a mut H }, { Ref RefMut RefMut Ref RefMut RefMut Ref RefMut }, { get get_mut get_mut get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a mut G &'a H }, { Ref RefMut RefMut Ref RefMut RefMut RefMut Ref }, { get get_mut get_mut get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a mut G &'a mut H }, { Ref RefMut RefMut Ref RefMut RefMut RefMut RefMut }, { get get_mut get_mut get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a G &'a H }, { Ref RefMut RefMut RefMut Ref Ref Ref Ref }, { get get_mut get_mut get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a G &'a mut H }, { Ref RefMut RefMut RefMut Ref Ref Ref RefMut }, { get get_mut get_mut get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a mut G &'a H }, { Ref RefMut RefMut RefMut Ref Ref RefMut Ref }, { get get_mut get_mut get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a mut G &'a mut H }, { Ref RefMut RefMut RefMut Ref Ref RefMut RefMut }, { get get_mut get_mut get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a G &'a H }, { Ref RefMut RefMut RefMut Ref RefMut Ref Ref }, { get get_mut get_mut get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a G &'a mut H }, { Ref RefMut RefMut RefMut Ref RefMut Ref RefMut }, { get get_mut get_mut get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a mut G &'a H }, { Ref RefMut RefMut RefMut Ref RefMut RefMut Ref }, { get get_mut get_mut get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a mut G &'a mut H }, { Ref RefMut RefMut RefMut Ref RefMut RefMut RefMut }, { get get_mut get_mut get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a G &'a H }, { Ref RefMut RefMut RefMut RefMut Ref Ref Ref }, { get get_mut get_mut get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a G &'a mut H }, { Ref RefMut RefMut RefMut RefMut Ref Ref RefMut }, { get get_mut get_mut get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a mut G &'a H }, { Ref RefMut RefMut RefMut RefMut Ref RefMut Ref }, { get get_mut get_mut get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a mut G &'a mut H }, { Ref RefMut RefMut RefMut RefMut Ref RefMut RefMut }, { get get_mut get_mut get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a G &'a H }, { Ref RefMut RefMut RefMut RefMut RefMut Ref Ref }, { get get_mut get_mut get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a G &'a mut H }, { Ref RefMut RefMut RefMut RefMut RefMut Ref RefMut }, { get get_mut get_mut get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G &'a H }, { Ref RefMut RefMut RefMut RefMut RefMut RefMut Ref }, { get get_mut get_mut get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G &'a mut H }, { Ref RefMut RefMut RefMut RefMut RefMut RefMut RefMut }, { get get_mut get_mut get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a E &'a F &'a G &'a H }, { RefMut Ref Ref Ref Ref Ref Ref Ref }, { get_mut get get get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a E &'a F &'a G &'a mut H }, { RefMut Ref Ref Ref Ref Ref Ref RefMut }, { get_mut get get get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a E &'a F &'a mut G &'a H }, { RefMut Ref Ref Ref Ref Ref RefMut Ref }, { get_mut get get get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a E &'a F &'a mut G &'a mut H }, { RefMut Ref Ref Ref Ref Ref RefMut RefMut }, { get_mut get get get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a E &'a mut F &'a G &'a H }, { RefMut Ref Ref Ref Ref RefMut Ref Ref }, { get_mut get get get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a E &'a mut F &'a G &'a mut H }, { RefMut Ref Ref Ref Ref RefMut Ref RefMut }, { get_mut get get get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a E &'a mut F &'a mut G &'a H }, { RefMut Ref Ref Ref Ref RefMut RefMut Ref }, { get_mut get get get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a E &'a mut F &'a mut G &'a mut H }, { RefMut Ref Ref Ref Ref RefMut RefMut RefMut }, { get_mut get get get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a F &'a G &'a H }, { RefMut Ref Ref Ref RefMut Ref Ref Ref }, { get_mut get get get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a F &'a G &'a mut H }, { RefMut Ref Ref Ref RefMut Ref Ref RefMut }, { get_mut get get get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a F &'a mut G &'a H }, { RefMut Ref Ref Ref RefMut Ref RefMut Ref }, { get_mut get get get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a F &'a mut G &'a mut H }, { RefMut Ref Ref Ref RefMut Ref RefMut RefMut }, { get_mut get get get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a mut F &'a G &'a H }, { RefMut Ref Ref Ref RefMut RefMut Ref Ref }, { get_mut get get get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a mut F &'a G &'a mut H }, { RefMut Ref Ref Ref RefMut RefMut Ref RefMut }, { get_mut get get get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a mut F &'a mut G &'a H }, { RefMut Ref Ref Ref RefMut RefMut RefMut Ref }, { get_mut get get get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a D &'a mut E &'a mut F &'a mut G &'a mut H }, { RefMut Ref Ref Ref RefMut RefMut RefMut RefMut }, { get_mut get get get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a F &'a G &'a H }, { RefMut Ref Ref RefMut Ref Ref Ref Ref }, { get_mut get get get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a F &'a G &'a mut H }, { RefMut Ref Ref RefMut Ref Ref Ref RefMut }, { get_mut get get get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a F &'a mut G &'a H }, { RefMut Ref Ref RefMut Ref Ref RefMut Ref }, { get_mut get get get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a F &'a mut G &'a mut H }, { RefMut Ref Ref RefMut Ref Ref RefMut RefMut }, { get_mut get get get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a mut F &'a G &'a H }, { RefMut Ref Ref RefMut Ref RefMut Ref Ref }, { get_mut get get get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a mut F &'a G &'a mut H }, { RefMut Ref Ref RefMut Ref RefMut Ref RefMut }, { get_mut get get get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a mut F &'a mut G &'a H }, { RefMut Ref Ref RefMut Ref RefMut RefMut Ref }, { get_mut get get get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a E &'a mut F &'a mut G &'a mut H }, { RefMut Ref Ref RefMut Ref RefMut RefMut RefMut }, { get_mut get get get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a F &'a G &'a H }, { RefMut Ref Ref RefMut RefMut Ref Ref Ref }, { get_mut get get get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a F &'a G &'a mut H }, { RefMut Ref Ref RefMut RefMut Ref Ref RefMut }, { get_mut get get get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a F &'a mut G &'a H }, { RefMut Ref Ref RefMut RefMut Ref RefMut Ref }, { get_mut get get get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a F &'a mut G &'a mut H }, { RefMut Ref Ref RefMut RefMut Ref RefMut RefMut }, { get_mut get get get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a G &'a H }, { RefMut Ref Ref RefMut RefMut RefMut Ref Ref }, { get_mut get get get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a G &'a mut H }, { RefMut Ref Ref RefMut RefMut RefMut Ref RefMut }, { get_mut get get get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a mut G &'a H }, { RefMut Ref Ref RefMut RefMut RefMut RefMut Ref }, { get_mut get get get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a C &'a mut D &'a mut E &'a mut F &'a mut G &'a mut H }, { RefMut Ref Ref RefMut RefMut RefMut RefMut RefMut }, { get_mut get get get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a F &'a G &'a H }, { RefMut Ref RefMut Ref Ref Ref Ref Ref }, { get_mut get get_mut get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a F &'a G &'a mut H }, { RefMut Ref RefMut Ref Ref Ref Ref RefMut }, { get_mut get get_mut get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a F &'a mut G &'a H }, { RefMut Ref RefMut Ref Ref Ref RefMut Ref }, { get_mut get get_mut get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a F &'a mut G &'a mut H }, { RefMut Ref RefMut Ref Ref Ref RefMut RefMut }, { get_mut get get_mut get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a mut F &'a G &'a H }, { RefMut Ref RefMut Ref Ref RefMut Ref Ref }, { get_mut get get_mut get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a mut F &'a G &'a mut H }, { RefMut Ref RefMut Ref Ref RefMut Ref RefMut }, { get_mut get get_mut get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a mut F &'a mut G &'a H }, { RefMut Ref RefMut Ref Ref RefMut RefMut Ref }, { get_mut get get_mut get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a E &'a mut F &'a mut G &'a mut H }, { RefMut Ref RefMut Ref Ref RefMut RefMut RefMut }, { get_mut get get_mut get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a F &'a G &'a H }, { RefMut Ref RefMut Ref RefMut Ref Ref Ref }, { get_mut get get_mut get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a F &'a G &'a mut H }, { RefMut Ref RefMut Ref RefMut Ref Ref RefMut }, { get_mut get get_mut get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a F &'a mut G &'a H }, { RefMut Ref RefMut Ref RefMut Ref RefMut Ref }, { get_mut get get_mut get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a F &'a mut G &'a mut H }, { RefMut Ref RefMut Ref RefMut Ref RefMut RefMut }, { get_mut get get_mut get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a G &'a H }, { RefMut Ref RefMut Ref RefMut RefMut Ref Ref }, { get_mut get get_mut get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a G &'a mut H }, { RefMut Ref RefMut Ref RefMut RefMut Ref RefMut }, { get_mut get get_mut get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a mut G &'a H }, { RefMut Ref RefMut Ref RefMut RefMut RefMut Ref }, { get_mut get get_mut get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a D &'a mut E &'a mut F &'a mut G &'a mut H }, { RefMut Ref RefMut Ref RefMut RefMut RefMut RefMut }, { get_mut get get_mut get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a F &'a G &'a H }, { RefMut Ref RefMut RefMut Ref Ref Ref Ref }, { get_mut get get_mut get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a F &'a G &'a mut H }, { RefMut Ref RefMut RefMut Ref Ref Ref RefMut }, { get_mut get get_mut get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a F &'a mut G &'a H }, { RefMut Ref RefMut RefMut Ref Ref RefMut Ref }, { get_mut get get_mut get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a F &'a mut G &'a mut H }, { RefMut Ref RefMut RefMut Ref Ref RefMut RefMut }, { get_mut get get_mut get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a G &'a H }, { RefMut Ref RefMut RefMut Ref RefMut Ref Ref }, { get_mut get get_mut get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a G &'a mut H }, { RefMut Ref RefMut RefMut Ref RefMut Ref RefMut }, { get_mut get get_mut get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a mut G &'a H }, { RefMut Ref RefMut RefMut Ref RefMut RefMut Ref }, { get_mut get get_mut get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a E &'a mut F &'a mut G &'a mut H }, { RefMut Ref RefMut RefMut Ref RefMut RefMut RefMut }, { get_mut get get_mut get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a G &'a H }, { RefMut Ref RefMut RefMut RefMut Ref Ref Ref }, { get_mut get get_mut get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a G &'a mut H }, { RefMut Ref RefMut RefMut RefMut Ref Ref RefMut }, { get_mut get get_mut get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a mut G &'a H }, { RefMut Ref RefMut RefMut RefMut Ref RefMut Ref }, { get_mut get get_mut get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a F &'a mut G &'a mut H }, { RefMut Ref RefMut RefMut RefMut Ref RefMut RefMut }, { get_mut get get_mut get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a G &'a H }, { RefMut Ref RefMut RefMut RefMut RefMut Ref Ref }, { get_mut get get_mut get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a G &'a mut H }, { RefMut Ref RefMut RefMut RefMut RefMut Ref RefMut }, { get_mut get get_mut get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G &'a H }, { RefMut Ref RefMut RefMut RefMut RefMut RefMut Ref }, { get_mut get get_mut get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G &'a mut H }, { RefMut Ref RefMut RefMut RefMut RefMut RefMut RefMut }, { get_mut get get_mut get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a F &'a G &'a H }, { RefMut RefMut Ref Ref Ref Ref Ref Ref }, { get_mut get_mut get get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a F &'a G &'a mut H }, { RefMut RefMut Ref Ref Ref Ref Ref RefMut }, { get_mut get_mut get get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a F &'a mut G &'a H }, { RefMut RefMut Ref Ref Ref Ref RefMut Ref }, { get_mut get_mut get get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a F &'a mut G &'a mut H }, { RefMut RefMut Ref Ref Ref Ref RefMut RefMut }, { get_mut get_mut get get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a mut F &'a G &'a H }, { RefMut RefMut Ref Ref Ref RefMut Ref Ref }, { get_mut get_mut get get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a mut F &'a G &'a mut H }, { RefMut RefMut Ref Ref Ref RefMut Ref RefMut }, { get_mut get_mut get get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a mut F &'a mut G &'a H }, { RefMut RefMut Ref Ref Ref RefMut RefMut Ref }, { get_mut get_mut get get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a E &'a mut F &'a mut G &'a mut H }, { RefMut RefMut Ref Ref Ref RefMut RefMut RefMut }, { get_mut get_mut get get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a F &'a G &'a H }, { RefMut RefMut Ref Ref RefMut Ref Ref Ref }, { get_mut get_mut get get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a F &'a G &'a mut H }, { RefMut RefMut Ref Ref RefMut Ref Ref RefMut }, { get_mut get_mut get get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a F &'a mut G &'a H }, { RefMut RefMut Ref Ref RefMut Ref RefMut Ref }, { get_mut get_mut get get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a F &'a mut G &'a mut H }, { RefMut RefMut Ref Ref RefMut Ref RefMut RefMut }, { get_mut get_mut get get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a G &'a H }, { RefMut RefMut Ref Ref RefMut RefMut Ref Ref }, { get_mut get_mut get get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a G &'a mut H }, { RefMut RefMut Ref Ref RefMut RefMut Ref RefMut }, { get_mut get_mut get get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a mut G &'a H }, { RefMut RefMut Ref Ref RefMut RefMut RefMut Ref }, { get_mut get_mut get get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a D &'a mut E &'a mut F &'a mut G &'a mut H }, { RefMut RefMut Ref Ref RefMut RefMut RefMut RefMut }, { get_mut get_mut get get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a F &'a G &'a H }, { RefMut RefMut Ref RefMut Ref Ref Ref Ref }, { get_mut get_mut get get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a F &'a G &'a mut H }, { RefMut RefMut Ref RefMut Ref Ref Ref RefMut }, { get_mut get_mut get get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a F &'a mut G &'a H }, { RefMut RefMut Ref RefMut Ref Ref RefMut Ref }, { get_mut get_mut get get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a F &'a mut G &'a mut H }, { RefMut RefMut Ref RefMut Ref Ref RefMut RefMut }, { get_mut get_mut get get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a G &'a H }, { RefMut RefMut Ref RefMut Ref RefMut Ref Ref }, { get_mut get_mut get get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a G &'a mut H }, { RefMut RefMut Ref RefMut Ref RefMut Ref RefMut }, { get_mut get_mut get get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a mut G &'a H }, { RefMut RefMut Ref RefMut Ref RefMut RefMut Ref }, { get_mut get_mut get get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a E &'a mut F &'a mut G &'a mut H }, { RefMut RefMut Ref RefMut Ref RefMut RefMut RefMut }, { get_mut get_mut get get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a G &'a H }, { RefMut RefMut Ref RefMut RefMut Ref Ref Ref }, { get_mut get_mut get get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a G &'a mut H }, { RefMut RefMut Ref RefMut RefMut Ref Ref RefMut }, { get_mut get_mut get get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a mut G &'a H }, { RefMut RefMut Ref RefMut RefMut Ref RefMut Ref }, { get_mut get_mut get get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a F &'a mut G &'a mut H }, { RefMut RefMut Ref RefMut RefMut Ref RefMut RefMut }, { get_mut get_mut get get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a G &'a H }, { RefMut RefMut Ref RefMut RefMut RefMut Ref Ref }, { get_mut get_mut get get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a G &'a mut H }, { RefMut RefMut Ref RefMut RefMut RefMut Ref RefMut }, { get_mut get_mut get get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a mut G &'a H }, { RefMut RefMut Ref RefMut RefMut RefMut RefMut Ref }, { get_mut get_mut get get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a C &'a mut D &'a mut E &'a mut F &'a mut G &'a mut H }, { RefMut RefMut Ref RefMut RefMut RefMut RefMut RefMut }, { get_mut get_mut get get_mut get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a F &'a G &'a H }, { RefMut RefMut RefMut Ref Ref Ref Ref Ref }, { get_mut get_mut get_mut get get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a F &'a G &'a mut H }, { RefMut RefMut RefMut Ref Ref Ref Ref RefMut }, { get_mut get_mut get_mut get get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a F &'a mut G &'a H }, { RefMut RefMut RefMut Ref Ref Ref RefMut Ref }, { get_mut get_mut get_mut get get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a F &'a mut G &'a mut H }, { RefMut RefMut RefMut Ref Ref Ref RefMut RefMut }, { get_mut get_mut get_mut get get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a G &'a H }, { RefMut RefMut RefMut Ref Ref RefMut Ref Ref }, { get_mut get_mut get_mut get get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a G &'a mut H }, { RefMut RefMut RefMut Ref Ref RefMut Ref RefMut }, { get_mut get_mut get_mut get get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a mut G &'a H }, { RefMut RefMut RefMut Ref Ref RefMut RefMut Ref }, { get_mut get_mut get_mut get get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a E &'a mut F &'a mut G &'a mut H }, { RefMut RefMut RefMut Ref Ref RefMut RefMut RefMut }, { get_mut get_mut get_mut get get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a G &'a H }, { RefMut RefMut RefMut Ref RefMut Ref Ref Ref }, { get_mut get_mut get_mut get get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a G &'a mut H }, { RefMut RefMut RefMut Ref RefMut Ref Ref RefMut }, { get_mut get_mut get_mut get get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a mut G &'a H }, { RefMut RefMut RefMut Ref RefMut Ref RefMut Ref }, { get_mut get_mut get_mut get get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a F &'a mut G &'a mut H }, { RefMut RefMut RefMut Ref RefMut Ref RefMut RefMut }, { get_mut get_mut get_mut get get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a G &'a H }, { RefMut RefMut RefMut Ref RefMut RefMut Ref Ref }, { get_mut get_mut get_mut get get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a G &'a mut H }, { RefMut RefMut RefMut Ref RefMut RefMut Ref RefMut }, { get_mut get_mut get_mut get get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a mut G &'a H }, { RefMut RefMut RefMut Ref RefMut RefMut RefMut Ref }, { get_mut get_mut get_mut get get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a D &'a mut E &'a mut F &'a mut G &'a mut H }, { RefMut RefMut RefMut Ref RefMut RefMut RefMut RefMut }, { get_mut get_mut get_mut get get_mut get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a G &'a H }, { RefMut RefMut RefMut RefMut Ref Ref Ref Ref }, { get_mut get_mut get_mut get_mut get get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a G &'a mut H }, { RefMut RefMut RefMut RefMut Ref Ref Ref RefMut }, { get_mut get_mut get_mut get_mut get get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a mut G &'a H }, { RefMut RefMut RefMut RefMut Ref Ref RefMut Ref }, { get_mut get_mut get_mut get_mut get get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a F &'a mut G &'a mut H }, { RefMut RefMut RefMut RefMut Ref Ref RefMut RefMut }, { get_mut get_mut get_mut get_mut get get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a G &'a H }, { RefMut RefMut RefMut RefMut Ref RefMut Ref Ref }, { get_mut get_mut get_mut get_mut get get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a G &'a mut H }, { RefMut RefMut RefMut RefMut Ref RefMut Ref RefMut }, { get_mut get_mut get_mut get_mut get get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a mut G &'a H }, { RefMut RefMut RefMut RefMut Ref RefMut RefMut Ref }, { get_mut get_mut get_mut get_mut get get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a E &'a mut F &'a mut G &'a mut H }, { RefMut RefMut RefMut RefMut Ref RefMut RefMut RefMut }, { get_mut get_mut get_mut get_mut get get_mut get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a G &'a H }, { RefMut RefMut RefMut RefMut RefMut Ref Ref Ref }, { get_mut get_mut get_mut get_mut get_mut get get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a G &'a mut H }, { RefMut RefMut RefMut RefMut RefMut Ref Ref RefMut }, { get_mut get_mut get_mut get_mut get_mut get get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a mut G &'a H }, { RefMut RefMut RefMut RefMut RefMut Ref RefMut Ref }, { get_mut get_mut get_mut get_mut get_mut get get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a F &'a mut G &'a mut H }, { RefMut RefMut RefMut RefMut RefMut Ref RefMut RefMut }, { get_mut get_mut get_mut get_mut get_mut get get_mut get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a G &'a H }, { RefMut RefMut RefMut RefMut RefMut RefMut Ref Ref }, { get_mut get_mut get_mut get_mut get_mut get_mut get get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a G &'a mut H }, { RefMut RefMut RefMut RefMut RefMut RefMut Ref RefMut }, { get_mut get_mut get_mut get_mut get_mut get_mut get get_mut } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G &'a H }, { RefMut RefMut RefMut RefMut RefMut RefMut RefMut Ref }, { get_mut get_mut get_mut get_mut get_mut get_mut get_mut get } }
impl_to_components_iter_tuple! { { A B C D E F G H }, { &'a mut A &'a mut B &'a mut C &'a mut D &'a mut E &'a mut F &'a mut G &'a mut H }, { RefMut RefMut RefMut RefMut RefMut RefMut RefMut RefMut }, { get_mut get_mut get_mut get_mut get_mut get_mut get_mut get_mut } }

#[cfg(test)]
mod tests {
    use crate::ecs::{Entities, SphericalCoordinate};
    use irradiance_runtime::ecs::EntityCreationError;

    #[test]
    fn entity_creation_error_display() {
        let error = EntityCreationError { id: "test".into() };

        assert_eq!("entity with id 'test' already exists", format!("{}", error));
    }

    #[test]
    fn commit_order() {
        let entities = Entities::new();
        let entity = entities.create("entity").expect("entity");

        entity.insert(SphericalCoordinate::new(0.5, 1.0, 0.1));
        entity.insert(SphericalCoordinate::new(0.1, 1.0, 0.5));

        unsafe {
            entities.commit();
        }

        let component = entity
            .get::<SphericalCoordinate>()
            .expect("spherical coordinate");
        assert_relative_eq!(0.1, component.radius());
        assert_relative_eq!(1.0, component.inclination());
        assert_relative_eq!(0.5, component.azimuth());
    }
}
