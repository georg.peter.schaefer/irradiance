//! Graphics api.
//!
//! The main type for the graphics API is [`Context`]. It initializes the underlying graphics API,
//! selects a suitable [`PhysicalDevice`](physical_device::PhysicalDevice) and creates the
//! [`Device`](device::Device) and required [`Queues`](queue::Queue).
//!
//! The [`Context`] also provides resource management for memory, and pools.
//!
//! # Creating a `Context`
//! When creating a [`Context`] you can specify required
//! [instance extensions](instance::InstanceExtensions),
//! [device extensions](physical_device::DeviceExtensions) and
//! [device features](physical_device::PhysicalDeviceFeatures).
//!
//! ```no_run
//! use irradiance_runtime::gfx::GraphicsContext;
//! use irradiance_runtime::window::Window;
//! use irradiance_runtime::gfx::instance::InstanceExtensions;
//! use irradiance_runtime::gfx::physical_device::{DeviceExtensions, PhysicalDeviceFeatures};
//!
//! let window = Window::new().build().unwrap();
//! let context = GraphicsContext::with_swapchain(window.clone())
//!     .instance_extensions(InstanceExtensions {
//!         ext_debug_utils: true,
//!         ..Default::default()
//!     })
//!     .device_extensions(DeviceExtensions {
//!         khr_maintenance1: true,
//!         ..Default::default()
//!     })
//!     .physical_device_features(PhysicalDeviceFeatures {
//!         robust_buffer_access: true,
//!         ..Default::default()
//!     })
//!     .build()
//!     .unwrap();
//! ```

pub use context::GraphicsContext;
pub use context::GraphicsContextBuilder;
pub use error::GfxError;
pub use extent::Extent2D;
pub use extent::Extent3D;
pub use offset::Offset2D;
pub use offset::Offset3D;
pub use rect::Rect2D;
pub use version::Version;

pub mod buffer;
pub mod command_buffer;
mod context;
pub mod device;
mod error;
mod extent;
pub mod image;
pub mod instance;
mod macros;
pub mod memory;
mod offset;
pub mod physical_device;
pub mod pipeline;
pub mod queue;
mod rect;
pub mod render_pass;
pub mod sync;
mod version;
pub mod wsi;

/// Trait for types that have a gfx handle.
pub trait Handle {
    /// The handle type.
    type Type: ash::vk::Handle;

    /// Returns the object type.
    fn object_type(&self) -> ObjectType;

    /// Returns the handle.
    fn handle(&self) -> Self::Type;

    /// Returns the handle as raw `u64`.
    fn as_raw(&self) -> u64 {
        use ash::vk::Handle;

        self.handle().as_raw()
    }
}

/// Vulkan object types.
#[allow(missing_docs)]
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum ObjectType {
    Unknown,
    Instance,
    PhysicalDevice,
    Device,
    Queue,
    Semaphore,
    CommandBuffer,
    Fence,
    DeviceMemory,
    Buffer,
    Image,
    Event,
    QueryPool,
    BufferView,
    ImageView,
    ShaderModule,
    PipelineCache,
    PipelineLayout,
    RenderPass,
    Pipeline,
    DescriptorSetLayout,
    Sampler,
    DescriptorPool,
    DescriptorSet,
    Framebuffer,
    CommandPool,
    Surface,
    Swapchain,
}

impl From<ObjectType> for ash::vk::ObjectType {
    fn from(object_type: ObjectType) -> Self {
        match object_type {
            ObjectType::Unknown => ash::vk::ObjectType::UNKNOWN,
            ObjectType::Instance => ash::vk::ObjectType::INSTANCE,
            ObjectType::PhysicalDevice => ash::vk::ObjectType::PHYSICAL_DEVICE,
            ObjectType::Device => ash::vk::ObjectType::DEVICE,
            ObjectType::Queue => ash::vk::ObjectType::QUEUE,
            ObjectType::Semaphore => ash::vk::ObjectType::SEMAPHORE,
            ObjectType::CommandBuffer => ash::vk::ObjectType::COMMAND_BUFFER,
            ObjectType::Fence => ash::vk::ObjectType::FENCE,
            ObjectType::DeviceMemory => ash::vk::ObjectType::DEVICE_MEMORY,
            ObjectType::Buffer => ash::vk::ObjectType::BUFFER,
            ObjectType::Image => ash::vk::ObjectType::IMAGE,
            ObjectType::Event => ash::vk::ObjectType::EVENT,
            ObjectType::QueryPool => ash::vk::ObjectType::QUERY_POOL,
            ObjectType::BufferView => ash::vk::ObjectType::BUFFER_VIEW,
            ObjectType::ImageView => ash::vk::ObjectType::IMAGE_VIEW,
            ObjectType::ShaderModule => ash::vk::ObjectType::SHADER_MODULE,
            ObjectType::PipelineCache => ash::vk::ObjectType::PIPELINE_CACHE,
            ObjectType::PipelineLayout => ash::vk::ObjectType::PIPELINE_LAYOUT,
            ObjectType::RenderPass => ash::vk::ObjectType::RENDER_PASS,
            ObjectType::Pipeline => ash::vk::ObjectType::PIPELINE,
            ObjectType::DescriptorSetLayout => ash::vk::ObjectType::DESCRIPTOR_SET_LAYOUT,
            ObjectType::Sampler => ash::vk::ObjectType::SAMPLER,
            ObjectType::DescriptorPool => ash::vk::ObjectType::DESCRIPTOR_POOL,
            ObjectType::DescriptorSet => ash::vk::ObjectType::DESCRIPTOR_SET,
            ObjectType::Framebuffer => ash::vk::ObjectType::FRAMEBUFFER,
            ObjectType::CommandPool => ash::vk::ObjectType::COMMAND_POOL,
            ObjectType::Surface => ash::vk::ObjectType::SURFACE_KHR,
            ObjectType::Swapchain => ash::vk::ObjectType::SWAPCHAIN_KHR,
        }
    }
}
