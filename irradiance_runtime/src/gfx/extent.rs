/// A two dimensional extent.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Default)]
pub struct Extent2D {
    /// The width of the extent.
    pub width: u32,
    /// The height of the extent.
    pub height: u32,
}

impl From<Extent3D> for Extent2D {
    fn from(extent: Extent3D) -> Self {
        Self {
            width: extent.width,
            height: extent.height,
        }
    }
}

#[doc(hidden)]
impl From<ash::vk::Extent2D> for Extent2D {
    fn from(extent: ash::vk::Extent2D) -> Self {
        Self {
            width: extent.width,
            height: extent.height,
        }
    }
}

#[doc(hidden)]
impl From<Extent2D> for ash::vk::Extent2D {
    fn from(extent: Extent2D) -> Self {
        Self {
            width: extent.width,
            height: extent.height,
        }
    }
}

/// A three dimensional extent.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Default)]
pub struct Extent3D {
    /// The width of the extent.
    pub width: u32,
    /// The height of the extent.
    pub height: u32,
    /// The depth of the extent
    pub depth: u32,
}

impl From<Extent2D> for Extent3D {
    fn from(extent: Extent2D) -> Self {
        Self {
            width: extent.width,
            height: extent.height,
            depth: 1,
        }
    }
}

#[doc(hidden)]
impl From<ash::vk::Extent3D> for Extent3D {
    fn from(extent: ash::vk::Extent3D) -> Self {
        Self {
            width: extent.width,
            height: extent.height,
            depth: extent.depth,
        }
    }
}

#[doc(hidden)]
impl From<Extent3D> for ash::vk::Extent3D {
    fn from(extent: Extent3D) -> Self {
        Self {
            width: extent.width,
            height: extent.height,
            depth: extent.depth,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::{Extent2D, Extent3D};

    #[test]
    fn from_extent2d() {
        assert_eq!(
            Extent2D {
                width: 1024,
                height: 768
            },
            Extent2D::from(ash::vk::Extent2D::builder().width(1024).height(768).build())
        );
    }

    #[test]
    fn to_extent2d() {
        assert_eq!(
            ash::vk::Extent2D::builder().width(1024).height(768).build(),
            ash::vk::Extent2D::from(Extent2D {
                width: 1024,
                height: 768
            })
        );
    }

    #[test]
    fn from_extent3d() {
        assert_eq!(
            Extent3D {
                width: 1024,
                height: 768,
                depth: 8
            },
            Extent3D::from(
                ash::vk::Extent3D::builder()
                    .width(1024)
                    .height(768)
                    .depth(8)
                    .build()
            )
        );
    }

    #[test]
    fn to_extent3d() {
        assert_eq!(
            ash::vk::Extent3D::builder()
                .width(1024)
                .height(768)
                .depth(8)
                .build(),
            ash::vk::Extent3D::from(Extent3D {
                width: 1024,
                height: 768,
                depth: 8
            })
        );
    }

    #[test]
    fn extent2d_from_extent3d() {
        assert_eq!(
            Extent2D {
                width: 1024,
                height: 768
            },
            Extent2D::from(Extent3D {
                width: 1024,
                height: 768,
                depth: 8
            })
        );
    }

    #[test]
    fn extent3d_from_extent2d() {
        assert_eq!(
            Extent3D {
                width: 1024,
                height: 768,
                depth: 1
            },
            Extent3D::from(Extent2D {
                width: 1024,
                height: 768
            })
        );
    }
}
