use std::fmt::{Debug, Display, Formatter};

/// A semantic version number.
///
/// `Version` is a representation of `major.minor.patch`.
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Version {
    /// Major version.
    pub major: u32,
    /// Minor version.
    pub minor: u32,
    /// Patch version.
    pub patch: u32,
}

impl Default for Version {
    fn default() -> Self {
        Self {
            major: 0,
            minor: 1,
            patch: 0,
        }
    }
}

impl Debug for Version {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(self, f)
    }
}

impl From<(u32, u32, u32)> for Version {
    fn from((major, minor, patch): (u32, u32, u32)) -> Self {
        Self {
            major,
            minor,
            patch,
        }
    }
}

impl From<u32> for Version {
    fn from(version: u32) -> Self {
        Self {
            major: ash::vk::api_version_major(version),
            minor: ash::vk::api_version_minor(version),
            patch: ash::vk::api_version_patch(version),
        }
    }
}

impl From<Version> for u32 {
    fn from(version: Version) -> Self {
        ash::vk::make_api_version(0, version.major, version.minor, version.patch)
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::version::Version;

    #[test]
    fn default() {
        assert_eq!(
            Version {
                major: 0,
                minor: 1,
                patch: 0
            },
            Default::default()
        );
    }

    #[test]
    fn display() {
        assert_eq!(
            "1.2.6",
            format!(
                "{}",
                Version {
                    major: 1,
                    minor: 2,
                    patch: 6
                }
            )
        );
    }

    #[test]
    fn from_tuple() {
        assert_eq!(
            Version {
                major: 1,
                minor: 0,
                patch: 22
            },
            (1, 0, 22).into()
        )
    }

    #[test]
    fn from_u32() {
        assert_eq!(
            Version {
                major: 3,
                minor: 42,
                patch: 9
            },
            ash::vk::make_api_version(0, 3, 42, 9).into()
        )
    }

    #[test]
    fn from_version() {
        assert_eq!(
            ash::vk::make_api_version(0, 3, 42, 9),
            u32::from(Version {
                major: 3,
                minor: 42,
                patch: 9
            })
        )
    }
}
