//! Buffer

pub use buffer_usage::BufferUsage;
pub use private::Buffer;
pub use private::BufferBuilder;

mod buffer_usage;
mod private;
