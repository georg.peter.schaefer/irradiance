use crate::impl_flag_set;

impl_flag_set! {
    BufferUsage,
    doc = "Allowed buffer usage",
    (VK_BUFFER_USAGE_TRANSFER_SRC_BIT, transfer_src),
    (VK_BUFFER_USAGE_TRANSFER_DST_BIT, transfer_dst),
    (VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT, uniform_texel_buffer),
    (VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT, storage_texel_buffer),
    (VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, uniform_buffer),
    (VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, storage_buffer),
    (VK_BUFFER_USAGE_INDEX_BUFFER_BIT, index_buffer),
    (VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, vertex_buffer),
    (VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT, indirect_buffer),
    (VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, shader_device_address),
    (VK_BUFFER_USAGE_VIDEO_DECODE_SRC_BIT_KHR, video_decode_src),
    (VK_BUFFER_USAGE_VIDEO_DECODE_DST_BIT_KHR, video_decode_dst),
    (VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_BUFFER_BIT_EXT, transform_feedback_buffer),
    (VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_COUNTER_BUFFER_BIT_EXT, transform_feedback_counter_buffer),
    (VK_BUFFER_USAGE_CONDITIONAL_RENDERING_BIT_EXT, conditional_rendering),
    (VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR, acceleration_structure_build_input_read_only),
    (VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR, acceleration_structure_storage),
    (VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR, shader_binding_table),
    (VK_BUFFER_USAGE_VIDEO_ENCODE_DST_BIT_KHR, video_encode_dst),
    (VK_BUFFER_USAGE_VIDEO_ENCODE_SRC_BIT_KHR, video_encode_src)
}

#[doc(hidden)]
impl From<BufferUsage> for ash::vk::BufferUsageFlags {
    fn from(buffer_usage: BufferUsage) -> Self {
        let mut flags = ash::vk::BufferUsageFlags::empty();
        if buffer_usage.transfer_src {
            flags |= ash::vk::BufferUsageFlags::TRANSFER_SRC;
        }
        if buffer_usage.transfer_dst {
            flags |= ash::vk::BufferUsageFlags::TRANSFER_DST;
        }
        if buffer_usage.uniform_texel_buffer {
            flags |= ash::vk::BufferUsageFlags::UNIFORM_TEXEL_BUFFER;
        }
        if buffer_usage.storage_texel_buffer {
            flags |= ash::vk::BufferUsageFlags::STORAGE_TEXEL_BUFFER;
        }
        if buffer_usage.uniform_buffer {
            flags |= ash::vk::BufferUsageFlags::UNIFORM_BUFFER;
        }
        if buffer_usage.storage_buffer {
            flags |= ash::vk::BufferUsageFlags::STORAGE_BUFFER;
        }
        if buffer_usage.index_buffer {
            flags |= ash::vk::BufferUsageFlags::INDEX_BUFFER;
        }
        if buffer_usage.vertex_buffer {
            flags |= ash::vk::BufferUsageFlags::VERTEX_BUFFER;
        }
        if buffer_usage.indirect_buffer {
            flags |= ash::vk::BufferUsageFlags::INDIRECT_BUFFER;
        }
        if buffer_usage.shader_device_address {
            flags |= ash::vk::BufferUsageFlags::SHADER_DEVICE_ADDRESS;
        }
        if buffer_usage.video_decode_src {
            flags |= ash::vk::BufferUsageFlags::VIDEO_DECODE_SRC_KHR;
        }
        if buffer_usage.video_decode_dst {
            flags |= ash::vk::BufferUsageFlags::VIDEO_DECODE_DST_KHR;
        }
        if buffer_usage.transform_feedback_buffer {
            flags |= ash::vk::BufferUsageFlags::TRANSFORM_FEEDBACK_BUFFER_EXT;
        }
        if buffer_usage.transform_feedback_counter_buffer {
            flags |= ash::vk::BufferUsageFlags::TRANSFORM_FEEDBACK_COUNTER_BUFFER_EXT;
        }
        if buffer_usage.conditional_rendering {
            flags |= ash::vk::BufferUsageFlags::CONDITIONAL_RENDERING_EXT;
        }
        if buffer_usage.acceleration_structure_build_input_read_only {
            flags |= ash::vk::BufferUsageFlags::ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_KHR;
        }
        if buffer_usage.acceleration_structure_storage {
            flags |= ash::vk::BufferUsageFlags::ACCELERATION_STRUCTURE_STORAGE_KHR;
        }
        if buffer_usage.shader_binding_table {
            flags |= ash::vk::BufferUsageFlags::SHADER_BINDING_TABLE_KHR;
        }
        if buffer_usage.video_encode_src {
            flags |= ash::vk::BufferUsageFlags::VIDEO_ENCODE_SRC_KHR;
        }
        if buffer_usage.video_encode_dst {
            flags |= ash::vk::BufferUsageFlags::VIDEO_ENCODE_DST_KHR;
        }
        flags
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::buffer::BufferUsage;

    #[test]
    fn default() {
        assert_eq!(
            BufferUsage {
                transfer_src: false,
                transfer_dst: false,
                uniform_texel_buffer: false,
                storage_texel_buffer: false,
                uniform_buffer: false,
                storage_buffer: false,
                index_buffer: false,
                vertex_buffer: false,
                indirect_buffer: false,
                shader_device_address: false,
                video_decode_src: false,
                video_decode_dst: false,
                transform_feedback_buffer: false,
                transform_feedback_counter_buffer: false,
                conditional_rendering: false,
                acceleration_structure_build_input_read_only: false,
                acceleration_structure_storage: false,
                shader_binding_table: false,
                video_encode_dst: false,
                video_encode_src: false
            },
            Default::default()
        );
    }

    #[test]
    fn none() {
        assert_eq!(BufferUsage::default(), BufferUsage::none());
    }

    #[test]
    fn is_not_a_subset() {
        let a = BufferUsage {
            transfer_src: true,
            storage_texel_buffer: true,
            ..Default::default()
        };
        let b = BufferUsage {
            transfer_src: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn is_subset() {
        let a = BufferUsage {
            transfer_src: true,
            storage_texel_buffer: true,
            ..Default::default()
        };
        let b = BufferUsage {
            transfer_src: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn is_not_a_superset() {
        let a = BufferUsage {
            transfer_src: true,
            storage_texel_buffer: true,
            ..Default::default()
        };
        let b = BufferUsage {
            transfer_src: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn is_superset() {
        let a = BufferUsage {
            transfer_src: true,
            storage_texel_buffer: true,
            ..Default::default()
        };
        let b = BufferUsage {
            transfer_src: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn difference() {
        let a = BufferUsage {
            transfer_src: true,
            storage_texel_buffer: true,
            ..Default::default()
        };
        let b = BufferUsage {
            transfer_src: true,
            ..Default::default()
        };
        assert_eq!(
            BufferUsage {
                storage_texel_buffer: true,
                ..Default::default()
            },
            a.difference(&b)
        );
    }

    #[test]
    fn intersection() {
        let a = BufferUsage {
            transfer_src: true,
            storage_texel_buffer: true,
            ..Default::default()
        };
        let b = BufferUsage {
            transfer_src: true,
            ..Default::default()
        };
        assert_eq!(
            BufferUsage {
                transfer_src: true,
                ..Default::default()
            },
            a.intersection(&b)
        );
    }

    #[test]
    fn union() {
        let a = BufferUsage {
            transfer_src: true,
            storage_texel_buffer: true,
            ..Default::default()
        };
        let b = BufferUsage {
            transfer_src: true,
            shader_binding_table: true,
            ..Default::default()
        };
        assert_eq!(
            BufferUsage {
                transfer_src: true,
                storage_texel_buffer: true,
                shader_binding_table: true,
                ..Default::default()
            },
            a.union(&b)
        );
    }

    #[test]
    fn to_image_usage() {
        assert_eq!(
            ash::vk::BufferUsageFlags::TRANSFER_SRC
                | ash::vk::BufferUsageFlags::UNIFORM_TEXEL_BUFFER
                | ash::vk::BufferUsageFlags::UNIFORM_BUFFER
                | ash::vk::BufferUsageFlags::INDEX_BUFFER
                | ash::vk::BufferUsageFlags::INDIRECT_BUFFER
                | ash::vk::BufferUsageFlags::VIDEO_DECODE_SRC_KHR
                | ash::vk::BufferUsageFlags::TRANSFORM_FEEDBACK_BUFFER_EXT
                | ash::vk::BufferUsageFlags::CONDITIONAL_RENDERING_EXT
                | ash::vk::BufferUsageFlags::ACCELERATION_STRUCTURE_STORAGE_KHR
                | ash::vk::BufferUsageFlags::VIDEO_ENCODE_DST_KHR,
            ash::vk::BufferUsageFlags::from(BufferUsage {
                transfer_src: true,
                transfer_dst: false,
                uniform_texel_buffer: true,
                storage_texel_buffer: false,
                uniform_buffer: true,
                storage_buffer: false,
                index_buffer: true,
                vertex_buffer: false,
                indirect_buffer: true,
                shader_device_address: false,
                video_decode_src: true,
                video_decode_dst: false,
                transform_feedback_buffer: true,
                transform_feedback_counter_buffer: false,
                conditional_rendering: true,
                acceleration_structure_build_input_read_only: false,
                acceleration_structure_storage: true,
                shader_binding_table: false,
                video_encode_dst: true,
                video_encode_src: false
            })
        );
        assert_eq!(
            ash::vk::BufferUsageFlags::TRANSFER_DST
                | ash::vk::BufferUsageFlags::STORAGE_TEXEL_BUFFER
                | ash::vk::BufferUsageFlags::STORAGE_BUFFER
                | ash::vk::BufferUsageFlags::VERTEX_BUFFER
                | ash::vk::BufferUsageFlags::SHADER_DEVICE_ADDRESS
                | ash::vk::BufferUsageFlags::VIDEO_DECODE_DST_KHR
                | ash::vk::BufferUsageFlags::TRANSFORM_FEEDBACK_COUNTER_BUFFER_EXT
                | ash::vk::BufferUsageFlags::ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_KHR
                | ash::vk::BufferUsageFlags::SHADER_BINDING_TABLE_KHR
                | ash::vk::BufferUsageFlags::VIDEO_ENCODE_SRC_KHR,
            ash::vk::BufferUsageFlags::from(BufferUsage {
                transfer_src: false,
                transfer_dst: true,
                uniform_texel_buffer: false,
                storage_texel_buffer: true,
                uniform_buffer: false,
                storage_buffer: true,
                index_buffer: false,
                vertex_buffer: true,
                indirect_buffer: false,
                shader_device_address: true,
                video_decode_src: false,
                video_decode_dst: true,
                transform_feedback_buffer: false,
                transform_feedback_counter_buffer: true,
                conditional_rendering: false,
                acceleration_structure_build_input_read_only: true,
                acceleration_structure_storage: false,
                shader_binding_table: true,
                video_encode_dst: false,
                video_encode_src: true
            })
        );
    }
}
