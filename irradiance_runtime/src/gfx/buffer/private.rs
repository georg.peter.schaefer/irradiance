use ash::vk;
use std::sync::Arc;

use crate::gfx::buffer::BufferUsage;
use crate::gfx::command_buffer::{BufferCopy, BufferImageCopy, CommandBuffer, CommandPool};
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{Image, ImageAspects, ImageLayout, ImageSubresourceRange};
use crate::gfx::memory::{Allocation, MemoryLocation, MemoryRequirements};
use crate::gfx::queue::{CommandBufferSubmit, Queue, Submit};
use crate::gfx::sync::{
    AccessMask, BufferMemoryBarrier, ImageMemoryBarrier, PipelineBarrier, PipelineStages,
    SharingMode,
};
use crate::gfx::{GfxError, Handle, ObjectType};

/// A buffer object.
#[derive(Debug)]
pub struct Buffer {
    size: u64,
    allocation: Arc<Allocation>,
    handle: ash::vk::Buffer,
    device: Arc<Device>,
}

impl Buffer {
    /// Starts building a buffer from `data`.
    pub fn from_data<T>(device: Arc<Device>, data: Vec<T>) -> BufferBuilder<T> {
        BufferBuilder {
            data: Some(data),
            size: None,
            sharing_mode: SharingMode::Exclusive,
            usage: Default::default(),
            device,
        }
    }

    /// Starts building a buffer with `size`.
    pub fn with_size(device: Arc<Device>, size: u64) -> BufferBuilder<()> {
        BufferBuilder {
            data: None,
            size: Some(size),
            sharing_mode: SharingMode::Exclusive,
            usage: Default::default(),
            device,
        }
    }

    /// Returns the size of the buffer.
    pub fn size(&self) -> u64 {
        self.size
    }

    /// Copies the buffer to an image.
    pub fn copy_to_image(
        self: &Arc<Self>,
        queue: Arc<Queue>,
        command_pool: Arc<CommandPool>,
        image: Arc<Image>,
        buffer_image_copy: BufferImageCopy,
    ) -> Result<(), GfxError> {
        queue.submit(
            Submit::builder()
                .command_buffer(
                    CommandBufferSubmit::builder(self.create_copy_to_image_command_buffer(
                        command_pool,
                        image,
                        buffer_image_copy,
                    )?)
                    .build(),
                )
                .build(),
            None,
        )
    }

    fn create_copy_to_image_command_buffer(
        self: &Arc<Self>,
        command_pool: Arc<CommandPool>,
        image: Arc<Image>,
        buffer_image_copy: BufferImageCopy,
    ) -> Result<Arc<CommandBuffer>, GfxError> {
        CommandBuffer::primary(self.device.clone(), command_pool)
            .pipeline_barrier(self.create_pre_copy_to_image_barrier(image.clone()))
            .copy_buffer_to_image(
                self.clone(),
                image.clone(),
                ImageLayout::TransferDstOptimal,
                vec![buffer_image_copy],
            )
            .pipeline_barrier(self.create_post_copy_to_image_barrier(image))
            .build()
    }

    fn create_pre_copy_to_image_barrier(self: &Arc<Self>, image: Arc<Image>) -> PipelineBarrier {
        PipelineBarrier::builder()
            .image_memory_barrier(
                ImageMemoryBarrier::builder(image)
                    .src_stage_mask(PipelineStages::none())
                    .src_access_mask(AccessMask::none())
                    .dst_stage_mask(PipelineStages {
                        transfer: true,
                        ..Default::default()
                    })
                    .dst_access_mask(AccessMask {
                        transfer_write: true,
                        ..Default::default()
                    })
                    .old_layout(ImageLayout::Undefined)
                    .new_layout(ImageLayout::TransferDstOptimal)
                    .subresource_range(
                        ImageSubresourceRange::builder()
                            .aspect_mask(ImageAspects {
                                color: true,
                                ..Default::default()
                            })
                            .build(),
                    )
                    .build(),
            )
            .build()
    }

    fn create_post_copy_to_image_barrier(self: &Arc<Self>, image: Arc<Image>) -> PipelineBarrier {
        PipelineBarrier::builder()
            .image_memory_barrier(
                ImageMemoryBarrier::builder(image)
                    .src_stage_mask(PipelineStages {
                        transfer: true,
                        ..Default::default()
                    })
                    .src_access_mask(AccessMask {
                        transfer_write: true,
                        ..Default::default()
                    })
                    .dst_stage_mask(PipelineStages {
                        fragment_shader: true,
                        ..Default::default()
                    })
                    .dst_access_mask(AccessMask {
                        shader_sampled_read: true,
                        ..Default::default()
                    })
                    .old_layout(ImageLayout::TransferDstOptimal)
                    .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                    .subresource_range(
                        ImageSubresourceRange::builder()
                            .aspect_mask(ImageAspects {
                                color: true,
                                ..Default::default()
                            })
                            .build(),
                    )
                    .build(),
            )
            .build()
    }

    /// Copies the buffer to a buffer.
    pub fn copy_to_buffer(
        self: &Arc<Self>,
        queue: Arc<Queue>,
        command_pool: Arc<CommandPool>,
        buffer: Arc<Buffer>,
        dst_stage_mask: PipelineStages,
        dst_access_mask: AccessMask,
    ) -> Result<(), GfxError> {
        queue.submit(
            Submit::builder()
                .command_buffer(
                    CommandBufferSubmit::builder(self.create_copy_to_buffer_command_buffer(
                        command_pool,
                        buffer,
                        dst_stage_mask,
                        dst_access_mask,
                    )?)
                    .build(),
                )
                .build(),
            None,
        )
    }

    fn create_copy_to_buffer_command_buffer(
        self: &Arc<Self>,
        command_pool: Arc<CommandPool>,
        buffer: Arc<Buffer>,
        dst_stage_mask: PipelineStages,
        dst_access_mask: AccessMask,
    ) -> Result<Arc<CommandBuffer>, GfxError> {
        CommandBuffer::primary(self.device.clone(), command_pool)
            .copy_buffer(
                self.clone(),
                buffer.clone(),
                vec![BufferCopy::builder().size(self.size()).build()],
            )
            .pipeline_barrier(self.create_post_copy_to_buffer_barrier(
                buffer,
                dst_stage_mask,
                dst_access_mask,
            ))
            .build()
    }

    fn create_post_copy_to_buffer_barrier(
        self: &Arc<Self>,
        buffer: Arc<Buffer>,
        dst_stage_mask: PipelineStages,
        dst_access_mask: AccessMask,
    ) -> PipelineBarrier {
        PipelineBarrier::builder()
            .buffer_memory_barrier(
                BufferMemoryBarrier::builder(buffer.clone())
                    .src_stage_mask(PipelineStages {
                        transfer: true,
                        ..Default::default()
                    })
                    .src_access_mask(AccessMask {
                        transfer_write: true,
                        ..Default::default()
                    })
                    .dst_stage_mask(dst_stage_mask)
                    .dst_access_mask(dst_access_mask)
                    .size(buffer.size())
                    .build(),
            )
            .build()
    }

    /// Reads the content of the buffer if it is host visible.
    pub fn read<T: Sized + Default>(&self) -> Option<T> {
        unsafe {
            self.allocation
                .inner()
                .mapped_ptr()
                .map(|ptr| std::ptr::read(ptr.as_ptr() as *const T))
        }
    }

    /// Writes the content of the buffer if it is host visible.
    pub fn write<T: Sized>(&self, data: Vec<T>) {
        unsafe {
            if let Some(ptr) = self.allocation.inner().mapped_ptr() {
                std::ptr::copy_nonoverlapping(data.as_ptr(), ptr.as_ptr() as *mut T, data.len());
                let offset = (self.allocation.inner().offset()
                    / self
                        .device
                        .physical_device()
                        .properties()
                        .limits
                        .non_coherent_atom_size)
                    * self
                        .device
                        .physical_device()
                        .properties()
                        .limits
                        .non_coherent_atom_size;
                self.device
                    .inner()
                    .flush_mapped_memory_ranges(&[vk::MappedMemoryRange::builder()
                        .memory(self.allocation.inner().memory())
                        .offset(offset)
                        .size(self.allocation.inner().size())
                        .build()])
                    .expect("flushed memory");
            }
        }
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        unsafe {
            self.device.inner().destroy_buffer(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for Buffer {
    type Type = ash::vk::Buffer;

    fn object_type(&self) -> ObjectType {
        ObjectType::Buffer
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for Buffer {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`Buffer`].
pub struct BufferBuilder<T> {
    data: Option<Vec<T>>,
    size: Option<u64>,
    sharing_mode: SharingMode,
    usage: BufferUsage,
    device: Arc<Device>,
}

impl<T> BufferBuilder<T> {
    /// Sets the buffer usage.
    pub fn usage(&mut self, usage: BufferUsage) -> &mut Self {
        self.usage = usage;
        self
    }

    /// Sets the sharing mode.
    pub fn sharing_mode(&mut self, sharing_mode: SharingMode) -> &mut Self {
        self.sharing_mode = sharing_mode;
        self
    }

    /// Builds the [`Buffer`].
    ///
    /// # Errors
    /// This function returns an error if creating the buffer fails.
    pub fn build(&mut self) -> Result<Arc<Buffer>, GfxError> {
        let size = match (&self.size, &self.data) {
            (Some(size), None) => *size,
            (None, Some(data)) => (std::mem::size_of::<T>() * data.len()) as u64,
            _ => 0,
        };
        let create_info = ash::vk::BufferCreateInfo::builder()
            .size(size)
            .usage(self.usage.into())
            .sharing_mode(self.sharing_mode.clone().into())
            .queue_family_indices(self.sharing_mode.as_ref())
            .build();

        let handle = unsafe { self.device.inner().create_buffer(&create_info, None) }?;

        let memory_requirements = MemoryRequirements::from(unsafe {
            self.device.inner().get_buffer_memory_requirements(handle)
        });
        let memory_location = match (&self.size, &self.data) {
            (Some(_), None) => MemoryLocation::GpuOnly,
            (None, Some(_)) => MemoryLocation::CpuToGpu,
            _ => MemoryLocation::Unknown,
        };
        let allocation = self.device.allocator().allocate(
            self.device.clone(),
            "buffer",
            memory_requirements,
            memory_location,
            true,
        )?;

        unsafe {
            self.device.inner().bind_buffer_memory(
                handle,
                allocation.memory().handle(),
                allocation.offset(),
            )?;
            if let Some(data) = &self.data {
                allocation
                    .inner()
                    .mapped_ptr()
                    .unwrap()
                    .as_ptr()
                    .copy_from_nonoverlapping(data.as_ptr() as _, size as _);

                // Flushing sub ranges not working properly. The range needs to a multiple of a
                // physical device limit, which is currently not the case. We just risk having
                // data not fully being written to the memory for now.
                // self.device.inner().flush_mapped_memory_ranges(&[
                //     ash::vk::MappedMemoryRange::builder()
                //         .memory(allocation.memory().handle())
                //         .offset(allocation.offset())
                //         .size(allocation.size())
                //         .build(),
                // ])?;
            }
        }

        Ok(Arc::new(Buffer {
            size,
            allocation,
            handle,
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::buffer::{Buffer, BufferUsage};
    use crate::gfx::device::Device;
    use crate::gfx::sync::SharingMode;
    use crate::physical_device;

    #[test]
    fn build_buffer_err() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();

        let result = Buffer::with_size(device.clone(), 0)
            .usage(BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .build();

        assert!(result.is_err())
    }

    #[test]
    fn build_buffer_from_data() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();

        let result = Buffer::from_data(device.clone(), vec![1.0, 2.0, 3.0, 4.0, 5.0, 6.0])
            .usage(BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .build();

        assert!(result.is_ok());
        assert_eq!(48, result.unwrap().size());
    }

    #[test]
    fn build_buffer_with_size() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();

        let result = Buffer::with_size(device.clone(), 48)
            .usage(BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .build();

        assert!(result.is_ok());
        assert_eq!(48, result.unwrap().size());
    }
}
