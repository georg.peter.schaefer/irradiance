use crate::gfx::image::Format;

/// Swapchain format-color space pair.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct SurfaceFormat {
    /// [`Format`] that is compatible with the [`Surface`](super::Surface).
    pub format: Format,
    /// Presentation [`ColorSpace`] that is compatible with the [`Surface`](super::Surface).
    pub color_space: ColorSpace,
}

#[doc(hidden)]
impl From<ash::vk::SurfaceFormatKHR> for SurfaceFormat {
    fn from(surface_format: ash::vk::SurfaceFormatKHR) -> Self {
        Self {
            format: surface_format.format.into(),
            color_space: surface_format.color_space.into(),
        }
    }
}

/// Supported color space of the presentation engine.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum ColorSpace {
    /// The sRGB color space.
    SrgbNonLinear,
    /// The Display-P3 color space to be displayed using an sRGB-like EOTF.
    DisplayP3NonLinear,
    /// The extended sRGB color space to be displayed using a linear EOTF.
    ExtendedSrgbLinear,
    /// The Display-P3 color space to be displayed using a linear EOTF.
    DisplayP3Linear,
    /// The DCI-P3 color space to be displayed using the DCI-P3 EOTF.
    DciP3NonLinear,
    /// The BT709 color space to be displayed using a linear EOTF.
    Bt709Linear,
    /// The BT709 color space to be displayed using the SMPTE 170M EOTF.
    Bt709NonLinear,
    /// The BT2020 color space to be displayed using a linear EOTF.
    Bt2020Linear,
    /// The HDR10 (BT2020 color) space to be displayed using the SMPTE ST2084 Perceptual Quantizer
    /// (PQ) EOTF.
    Hdr10St2084,
    /// The Dolby Vision (BT2020 color space), proprietary encoding, to be displayed using the SMPTE
    /// ST2084 EOTF.
    DolbyVision,
    /// The HDR10 (BT2020 color space) to be displayed using the Hybrid Log Gamma (HLG) EOTF.
    Hdr10Hlg,
    /// The AdobeRGB color space to be displayed using a linear EOTF.
    AdobeRgbLinear,
    /// The AdobeRGB color space to be displayed using the Gamma 2.2 EOTF.
    AdobeRgbNonLinear,
    /// Color components are used “as is”
    PassThrough,
    /// The extended sRGB color space to be displayed using an sRGB EOTF.
    ExtendedSrgbNonLinear,
    /// The display’s native color space.
    DisplayNative,
}

#[doc(hidden)]
impl From<ash::vk::ColorSpaceKHR> for ColorSpace {
    fn from(color_space: ash::vk::ColorSpaceKHR) -> Self {
        match color_space {
            ash::vk::ColorSpaceKHR::SRGB_NONLINEAR => Self::SrgbNonLinear,
            ash::vk::ColorSpaceKHR::DISPLAY_P3_NONLINEAR_EXT => Self::DisplayP3NonLinear,
            ash::vk::ColorSpaceKHR::EXTENDED_SRGB_LINEAR_EXT => Self::ExtendedSrgbLinear,
            ash::vk::ColorSpaceKHR::DISPLAY_P3_LINEAR_EXT => Self::DisplayP3Linear,
            ash::vk::ColorSpaceKHR::DCI_P3_NONLINEAR_EXT => Self::DciP3NonLinear,
            ash::vk::ColorSpaceKHR::BT709_LINEAR_EXT => Self::Bt709Linear,
            ash::vk::ColorSpaceKHR::BT709_NONLINEAR_EXT => Self::Bt709NonLinear,
            ash::vk::ColorSpaceKHR::BT2020_LINEAR_EXT => Self::Bt2020Linear,
            ash::vk::ColorSpaceKHR::HDR10_ST2084_EXT => Self::Hdr10St2084,
            ash::vk::ColorSpaceKHR::DOLBYVISION_EXT => Self::DolbyVision,
            ash::vk::ColorSpaceKHR::HDR10_HLG_EXT => Self::Hdr10Hlg,
            ash::vk::ColorSpaceKHR::ADOBERGB_LINEAR_EXT => Self::AdobeRgbLinear,
            ash::vk::ColorSpaceKHR::ADOBERGB_NONLINEAR_EXT => Self::AdobeRgbNonLinear,
            ash::vk::ColorSpaceKHR::PASS_THROUGH_EXT => Self::PassThrough,
            ash::vk::ColorSpaceKHR::EXTENDED_SRGB_NONLINEAR_EXT => Self::ExtendedSrgbNonLinear,
            ash::vk::ColorSpaceKHR::DISPLAY_NATIVE_AMD => Self::DisplayNative,
            _ => unreachable!("Unrecognized 'ColorSpace'"),
        }
    }
}

impl From<ColorSpace> for ash::vk::ColorSpaceKHR {
    fn from(color_space: ColorSpace) -> Self {
        match color_space {
            ColorSpace::SrgbNonLinear => Self::SRGB_NONLINEAR,
            ColorSpace::DisplayP3NonLinear => Self::DISPLAY_P3_NONLINEAR_EXT,
            ColorSpace::ExtendedSrgbLinear => Self::EXTENDED_SRGB_LINEAR_EXT,
            ColorSpace::DisplayP3Linear => Self::DISPLAY_P3_LINEAR_EXT,
            ColorSpace::DciP3NonLinear => Self::DCI_P3_NONLINEAR_EXT,
            ColorSpace::Bt709Linear => Self::BT709_LINEAR_EXT,
            ColorSpace::Bt709NonLinear => Self::BT709_NONLINEAR_EXT,
            ColorSpace::Bt2020Linear => Self::BT2020_LINEAR_EXT,
            ColorSpace::Hdr10St2084 => Self::HDR10_ST2084_EXT,
            ColorSpace::DolbyVision => Self::DOLBYVISION_EXT,
            ColorSpace::Hdr10Hlg => Self::HDR10_HLG_EXT,
            ColorSpace::AdobeRgbLinear => Self::ADOBERGB_LINEAR_EXT,
            ColorSpace::AdobeRgbNonLinear => Self::ADOBERGB_NONLINEAR_EXT,
            ColorSpace::PassThrough => Self::PASS_THROUGH_EXT,
            ColorSpace::ExtendedSrgbNonLinear => Self::EXTENDED_SRGB_NONLINEAR_EXT,
            ColorSpace::DisplayNative => Self::DISPLAY_NATIVE_AMD,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::Format;
    use crate::gfx::wsi::format::SurfaceFormat;
    use crate::gfx::wsi::ColorSpace;

    #[test]
    fn from_surface_format() {
        let surface_format = SurfaceFormat::from(
            ash::vk::SurfaceFormatKHR::builder()
                .format(ash::vk::Format::R8G8B8A8_SRGB)
                .color_space(ash::vk::ColorSpaceKHR::SRGB_NONLINEAR)
                .build(),
        );

        assert_eq!(Format::R8G8B8A8Srgb, surface_format.format);
        assert_eq!(ColorSpace::SrgbNonLinear, surface_format.color_space);
    }

    #[test]
    fn from_color_space() {
        assert_eq!(
            ColorSpace::SrgbNonLinear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::SRGB_NONLINEAR)
        );
        assert_eq!(
            ColorSpace::DisplayP3NonLinear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::DISPLAY_P3_NONLINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::ExtendedSrgbLinear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::EXTENDED_SRGB_LINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::DisplayP3Linear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::DISPLAY_P3_LINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::DciP3NonLinear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::DCI_P3_NONLINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::Bt709Linear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::BT709_LINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::Bt709NonLinear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::BT709_NONLINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::Bt2020Linear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::BT2020_LINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::Hdr10St2084,
            ColorSpace::from(ash::vk::ColorSpaceKHR::HDR10_ST2084_EXT)
        );
        assert_eq!(
            ColorSpace::DolbyVision,
            ColorSpace::from(ash::vk::ColorSpaceKHR::DOLBYVISION_EXT)
        );
        assert_eq!(
            ColorSpace::Hdr10Hlg,
            ColorSpace::from(ash::vk::ColorSpaceKHR::HDR10_HLG_EXT)
        );
        assert_eq!(
            ColorSpace::AdobeRgbLinear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::ADOBERGB_LINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::AdobeRgbNonLinear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::ADOBERGB_NONLINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::PassThrough,
            ColorSpace::from(ash::vk::ColorSpaceKHR::PASS_THROUGH_EXT)
        );
        assert_eq!(
            ColorSpace::ExtendedSrgbNonLinear,
            ColorSpace::from(ash::vk::ColorSpaceKHR::EXTENDED_SRGB_NONLINEAR_EXT)
        );
        assert_eq!(
            ColorSpace::DisplayNative,
            ColorSpace::from(ash::vk::ColorSpaceKHR::DISPLAY_NATIVE_AMD)
        );
    }

    #[test]
    fn to_color_space() {
        assert_eq!(
            ash::vk::ColorSpaceKHR::SRGB_NONLINEAR,
            ash::vk::ColorSpaceKHR::from(ColorSpace::SrgbNonLinear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::DISPLAY_P3_NONLINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::DisplayP3NonLinear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::EXTENDED_SRGB_LINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::ExtendedSrgbLinear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::DISPLAY_P3_LINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::DisplayP3Linear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::DCI_P3_NONLINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::DciP3NonLinear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::BT709_LINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::Bt709Linear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::BT709_NONLINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::Bt709NonLinear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::BT2020_LINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::Bt2020Linear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::HDR10_ST2084_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::Hdr10St2084)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::DOLBYVISION_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::DolbyVision)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::HDR10_HLG_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::Hdr10Hlg)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::ADOBERGB_LINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::AdobeRgbLinear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::ADOBERGB_NONLINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::AdobeRgbNonLinear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::PASS_THROUGH_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::PassThrough)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::EXTENDED_SRGB_NONLINEAR_EXT,
            ash::vk::ColorSpaceKHR::from(ColorSpace::ExtendedSrgbNonLinear)
        );
        assert_eq!(
            ash::vk::ColorSpaceKHR::DISPLAY_NATIVE_AMD,
            ash::vk::ColorSpaceKHR::from(ColorSpace::DisplayNative)
        );
    }
}
