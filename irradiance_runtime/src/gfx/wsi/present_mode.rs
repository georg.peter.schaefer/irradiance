/// Presentation mode supported for a [`Surface`](super::Surface).
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum PresentMode {
    /// The presentation engine does not wait for a vertical blanking period to update the current
    /// image, meaning this mode may result in visible tearing.
    Immediate,
    /// The presentation engine waits for the next vertical blanking period to update the current
    /// image.
    Mailbox,
    /// The presentation engine waits for the next vertical blanking period to update the current
    /// image.
    Fifo,
    /// The presentation engine generally waits for the next vertical blanking period to update the
    /// current image.
    FifoRelaxed,
    /// The presentation engine and application have concurrent access to a single image, which is
    /// referred to as a shared presentable image. The presentation engine is only required to
    /// update the current image after a new presentation request is received.
    SharedDemandRefresh,
    /// The presentation engine and application have concurrent access to a single image, which is
    /// referred to as a shared presentable image. The presentation engine periodically updates the
    /// current image on its regular refresh cycle.
    SharedContinuousRefresh,
}

#[doc(hidden)]
impl From<ash::vk::PresentModeKHR> for PresentMode {
    fn from(present_mode: ash::vk::PresentModeKHR) -> Self {
        match present_mode {
            ash::vk::PresentModeKHR::IMMEDIATE => Self::Immediate,
            ash::vk::PresentModeKHR::MAILBOX => Self::Mailbox,
            ash::vk::PresentModeKHR::FIFO => Self::Fifo,
            ash::vk::PresentModeKHR::FIFO_RELAXED => Self::FifoRelaxed,
            ash::vk::PresentModeKHR::SHARED_DEMAND_REFRESH => Self::SharedDemandRefresh,
            ash::vk::PresentModeKHR::SHARED_CONTINUOUS_REFRESH => Self::SharedContinuousRefresh,
            _ => unreachable!("Unrecognized 'PresentMode'"),
        }
    }
}

#[doc(hidden)]
impl From<PresentMode> for ash::vk::PresentModeKHR {
    fn from(present_mode: PresentMode) -> Self {
        match present_mode {
            PresentMode::Immediate => Self::IMMEDIATE,
            PresentMode::Mailbox => Self::MAILBOX,
            PresentMode::Fifo => Self::FIFO,
            PresentMode::FifoRelaxed => Self::FIFO_RELAXED,
            PresentMode::SharedDemandRefresh => Self::SHARED_DEMAND_REFRESH,
            PresentMode::SharedContinuousRefresh => Self::SHARED_CONTINUOUS_REFRESH,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::wsi::present_mode::PresentMode;

    #[test]
    fn from_present_mode() {
        assert_eq!(
            PresentMode::Immediate,
            PresentMode::from(ash::vk::PresentModeKHR::IMMEDIATE)
        );
        assert_eq!(
            PresentMode::Mailbox,
            PresentMode::from(ash::vk::PresentModeKHR::MAILBOX)
        );
        assert_eq!(
            PresentMode::Fifo,
            PresentMode::from(ash::vk::PresentModeKHR::FIFO)
        );
        assert_eq!(
            PresentMode::FifoRelaxed,
            PresentMode::from(ash::vk::PresentModeKHR::FIFO_RELAXED)
        );
        assert_eq!(
            PresentMode::SharedDemandRefresh,
            PresentMode::from(ash::vk::PresentModeKHR::SHARED_DEMAND_REFRESH)
        );
        assert_eq!(
            PresentMode::SharedContinuousRefresh,
            PresentMode::from(ash::vk::PresentModeKHR::SHARED_CONTINUOUS_REFRESH)
        );
    }

    #[test]
    fn to_present_mode() {
        assert_eq!(
            ash::vk::PresentModeKHR::IMMEDIATE,
            ash::vk::PresentModeKHR::from(PresentMode::Immediate)
        );
        assert_eq!(
            ash::vk::PresentModeKHR::MAILBOX,
            ash::vk::PresentModeKHR::from(PresentMode::Mailbox)
        );
        assert_eq!(
            ash::vk::PresentModeKHR::FIFO,
            ash::vk::PresentModeKHR::from(PresentMode::Fifo)
        );
        assert_eq!(
            ash::vk::PresentModeKHR::FIFO_RELAXED,
            ash::vk::PresentModeKHR::from(PresentMode::FifoRelaxed)
        );
        assert_eq!(
            ash::vk::PresentModeKHR::SHARED_DEMAND_REFRESH,
            ash::vk::PresentModeKHR::from(PresentMode::SharedDemandRefresh)
        );
        assert_eq!(
            ash::vk::PresentModeKHR::SHARED_CONTINUOUS_REFRESH,
            ash::vk::PresentModeKHR::from(PresentMode::SharedContinuousRefresh)
        );
    }
}
