use std::fmt::{Debug, Formatter};
use std::sync::Arc;

use crate::gfx::instance::Instance;
use crate::gfx::{GfxError, Handle, ObjectType};
use crate::window::Window;

/// Abstraction for a native platform surface or window object.
pub struct Surface {
    handle: ash::vk::SurfaceKHR,
    loader: ash::extensions::khr::Surface,
}

impl Surface {
    /// Starts building a new [`Surface`].
    pub fn builder(instance: Arc<Instance>, window: Arc<Window>) -> SurfaceBuilder {
        SurfaceBuilder { window, instance }
    }

    pub(crate) fn loader(&self) -> &ash::extensions::khr::Surface {
        &self.loader
    }
}

impl Drop for Surface {
    fn drop(&mut self) {
        unsafe { self.loader.destroy_surface(self.handle, None) };
    }
}

impl Debug for Surface {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Surface")
            .field("handle", &self.handle)
            .finish()
    }
}

#[doc(hidden)]
impl Handle for Surface {
    type Type = ash::vk::SurfaceKHR;

    fn object_type(&self) -> ObjectType {
        ObjectType::Surface
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

/// Type for building a [`Surface`]
#[derive(Debug)]
pub struct SurfaceBuilder {
    #[allow(unused)]
    window: Arc<Window>,
    instance: Arc<Instance>,
}

impl SurfaceBuilder {
    /// Builds the [`Surface`].
    ///
    /// # Errors
    /// This function returns an error if the [`Surface`] creation fails.
    pub fn build(&mut self) -> Result<Arc<Surface>, GfxError> {
        let loader =
            ash::extensions::khr::Surface::new(self.instance.entry(), self.instance.inner());
        Ok(Arc::new(Surface {
            handle: self.create_surface()?,
            loader,
        }))
    }

    #[cfg(target_os = "linux")]
    fn create_surface(&self) -> Result<ash::vk::SurfaceKHR, GfxError> {
        use winit::platform::unix::WindowExtUnix;

        let loader =
            ash::extensions::khr::XlibSurface::new(self.instance.entry(), self.instance.inner());
        let display = self.window.inner().xlib_display().unwrap();
        let window = self.window.inner().xlib_window().unwrap();
        let create_info = ash::vk::XlibSurfaceCreateInfoKHR::builder()
            .window(window as ash::vk::Window)
            .dpy(display as *mut ash::vk::Display)
            .build();
        Ok(unsafe { loader.create_xlib_surface(&create_info, None) }?)
    }

    #[cfg(target_os = "windows")]
    fn create_surface(&self) -> Result<ash::vk::SurfaceKHR, GfxError> {
        use winit::platform::windows::WindowExtWindows;

        let loader =
            ash::extensions::khr::Win32Surface::new(self.instance.entry(), self.instance.inner());
        let hinstance = self.window.inner().hinstance();
        let hwnd = self.window.inner().hwnd();
        let create_info = ash::vk::Win32SurfaceCreateInfoKHR::builder()
            .hinstance(hinstance as _)
            .hwnd(hwnd as _)
            .build();
        Ok(unsafe { loader.create_win32_surface(&create_info, None) }?)
    }
}
