use crate::gfx::wsi::SurfaceTransforms;
use crate::gfx::Extent2D;

/// Capabilities of a [`Surface`](crate::gfx::wsi::Surface).
#[derive(Copy, Clone, Eq, PartialEq, Debug, Default)]
pub struct SurfaceCapabilities {
    /// The minimum number of images the specified device supports for a swapchain created for the surface.
    pub min_image_count: u32,
    /// The maximum number of images the specified device supports for a swapchain created for the surface.
    pub max_image_count: u32,
    /// The current width and height of the surface.
    pub current_extent: Extent2D,
    /// The smallest valid swapchain extent for the surface on the specified device.
    pub min_image_extent: Extent2D,
    /// The largest valid swapchain extent for the surface on the specified device.
    pub max_image_extent: Extent2D,
    /// The presentation transforms supported for the surface on the specified device. At least one
    /// bit will be set.
    pub supported_transforms: SurfaceTransforms,
    /// The surface’s current transform relative to the presentation engine’s natural orientation.
    pub current_transforms: SurfaceTransforms,
}

#[doc(hidden)]
impl From<ash::vk::SurfaceCapabilitiesKHR> for SurfaceCapabilities {
    fn from(surface_capabilities: ash::vk::SurfaceCapabilitiesKHR) -> Self {
        Self {
            min_image_count: surface_capabilities.min_image_count,
            max_image_count: surface_capabilities.max_image_count,
            current_extent: surface_capabilities.current_extent.into(),
            min_image_extent: surface_capabilities.min_image_extent.into(),
            max_image_extent: surface_capabilities.max_image_extent.into(),
            supported_transforms: surface_capabilities.supported_transforms.into(),
            current_transforms: surface_capabilities.current_transform.into(),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::wsi::{SurfaceCapabilities, SurfaceTransforms};
    use crate::gfx::Extent2D;

    #[test]
    fn from_surface_capabilities() {
        assert_eq!(
            SurfaceCapabilities {
                min_image_count: 1,
                max_image_count: 3,
                current_extent: Extent2D {
                    width: 1024,
                    height: 768
                },
                min_image_extent: Extent2D {
                    width: 640,
                    height: 480
                },
                max_image_extent: Extent2D {
                    width: 1920,
                    height: 1080
                },
                supported_transforms: SurfaceTransforms {
                    inherit: true,
                    ..Default::default()
                },
                current_transforms: SurfaceTransforms {
                    horizontal_mirror: true,
                    ..Default::default()
                }
            },
            SurfaceCapabilities::from(
                ash::vk::SurfaceCapabilitiesKHR::builder()
                    .min_image_count(1)
                    .max_image_count(3)
                    .current_extent(ash::vk::Extent2D::builder().width(1024).height(768).build())
                    .min_image_extent(ash::vk::Extent2D::builder().width(640).height(480).build())
                    .max_image_extent(
                        ash::vk::Extent2D::builder()
                            .width(1920)
                            .height(1080)
                            .build()
                    )
                    .supported_transforms(ash::vk::SurfaceTransformFlagsKHR::INHERIT)
                    .current_transform(ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR)
                    .build()
            )
        )
    }
}
