//! Window system integration.

pub use capabilities::SurfaceCapabilities;
pub use format::ColorSpace;
pub use format::SurfaceFormat;
pub use present_mode::PresentMode;
pub use surface::Surface;
pub use surface::SurfaceBuilder;
pub use surface_transforms::SurfaceTransforms;
pub use swapchain::Swapchain;
pub use swapchain::SwapchainBuilder;

mod capabilities;
mod format;
mod present_mode;
mod surface;
mod surface_transforms;
mod swapchain;
