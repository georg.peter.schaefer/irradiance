use std::fmt::{Debug, Formatter};
use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{Format, Image, ImageUsage, ImageView, ImageViewType};
use crate::gfx::sync::{Fence, Semaphore, SharingMode};
use crate::gfx::wsi::{ColorSpace, PresentMode, Surface, SurfaceTransforms};
use crate::gfx::{Extent2D, GfxError, Handle, ObjectType};

/// Provides the ability to present rendering results to a surface.
pub struct Swapchain {
    image_views: Vec<Arc<ImageView>>,
    images: Vec<Arc<Image>>,
    image_extent: Extent2D,
    image_format: Format,
    handle: ash::vk::SwapchainKHR,
    loader: ash::extensions::khr::Swapchain,
    device: Arc<Device>,
}

impl Swapchain {
    /// Starts building a new `Swapchain`.
    pub fn builder(surface: Arc<Surface>, device: Arc<Device>) -> SwapchainBuilder {
        SwapchainBuilder {
            old_swapchain: None,
            clipped: false,
            present_mode: PresentMode::Fifo,
            pre_transform: Default::default(),
            image_sharing_mode: SharingMode::Exclusive,
            image_usage: Default::default(),
            image_extent: Default::default(),
            image_color_space: ColorSpace::SrgbNonLinear,
            image_format: Format::B8G8R8A8Srgb,
            min_image_count: 0,
            device,
            surface,
        }
    }

    /// Returns the format for the swapchain images.
    pub fn image_format(&self) -> Format {
        self.image_format
    }

    /// Returns the extent for the swapchain images.
    pub fn image_extent(&self) -> Extent2D {
        self.image_extent
    }

    /// Returns the [`Swapchain`]s images.
    pub fn images(&self) -> &Vec<Arc<Image>> {
        &self.images
    }

    /// Returns the [`Swapchain`]s image views.
    pub fn image_views(&self) -> &Vec<Arc<ImageView>> {
        &self.image_views
    }

    pub(crate) fn loader(&self) -> &ash::extensions::khr::Swapchain {
        &self.loader
    }

    /// Acquires the next image from the [`Swapchain`].
    ///
    /// # Errors
    /// This function returns an error if acquiring the next image fails.
    pub fn acquire_next_image(
        &self,
        timeout: u64,
        semaphore: Option<&Semaphore>,
        fence: Option<&Fence>,
    ) -> Result<(u32, bool), GfxError> {
        unsafe {
            Ok(self.loader.acquire_next_image(
                self.handle,
                timeout,
                semaphore
                    .map(|semaphore| semaphore.handle())
                    .unwrap_or(ash::vk::Semaphore::null()),
                fence
                    .map(|fence| fence.handle())
                    .unwrap_or(ash::vk::Fence::null()),
            )?)
        }
    }
}

impl Drop for Swapchain {
    fn drop(&mut self) {
        unsafe { self.loader.destroy_swapchain(self.handle, None) };
    }
}

impl Debug for Swapchain {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Swapchain")
            .field("handle", &self.handle)
            .finish()
    }
}

#[doc(hidden)]
impl Handle for Swapchain {
    type Type = ash::vk::SwapchainKHR;

    fn object_type(&self) -> ObjectType {
        ObjectType::Swapchain
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for Swapchain {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Errors which can occur when creating a [`Swapchain`].
#[derive(Debug)]
pub struct SwapchainBuilder {
    old_swapchain: Option<Arc<Swapchain>>,
    clipped: bool,
    present_mode: PresentMode,
    pre_transform: SurfaceTransforms,
    image_sharing_mode: SharingMode,
    image_usage: ImageUsage,
    image_extent: Extent2D,
    image_color_space: ColorSpace,
    image_format: Format,
    min_image_count: u32,
    device: Arc<Device>,
    surface: Arc<Surface>,
}

impl SwapchainBuilder {
    /// Sets the minimum number of images the [`Swapchain`] will be created with.
    pub fn min_image_count(&mut self, min_image_count: u32) -> &mut Self {
        self.min_image_count = min_image_count;
        self
    }

    /// Sets the [`Format`] for the [`Swapchain`] images.
    pub fn image_format(&mut self, image_format: Format) -> &mut Self {
        self.image_format = image_format;
        self
    }

    /// Sets the [`ColorSpace`] for the [`Swapchain`].
    pub fn image_color_space(&mut self, image_color_space: ColorSpace) -> &mut Self {
        self.image_color_space = image_color_space;
        self
    }

    /// Sets the extent for the [`Swapchain`] images.
    pub fn image_extent(&mut self, image_extent: Extent2D) -> &mut Self {
        self.image_extent = image_extent;
        self
    }

    /// Sets the [`ImageUsage`] for the [`Swapchain`] images will be created with.
    pub fn image_usage(&mut self, image_usage: ImageUsage) -> &mut Self {
        self.image_usage = image_usage;
        self
    }

    /// Sets the [`SharingMode`] for the [`Swapchain`] images.
    pub fn image_sharing_mode(&mut self, image_sharing_mode: SharingMode) -> &mut Self {
        self.image_sharing_mode = image_sharing_mode;
        self
    }

    /// Sets the [`pre transform`](SurfaceTransforms) for the [`Swapchain`].
    pub fn pre_transform(&mut self, pre_transform: SurfaceTransforms) -> &mut Self {
        self.pre_transform = pre_transform;
        self
    }

    /// Sets the [`PresentMode`] for the [`Swapchain`].
    pub fn present_mode(&mut self, present_mode: PresentMode) -> &mut Self {
        self.present_mode = present_mode;
        self
    }

    /// Sets if the implementation is allowed to discard rendering operations that affect regions of
    /// the [`Surface`] that are not visible.
    pub fn clipped(&mut self, clipped: bool) -> &mut Self {
        self.clipped = clipped;
        self
    }

    /// Sets the old [`Swapchain`].
    pub fn old_swapchain(&mut self, old_swapchain: Arc<Swapchain>) -> &mut Self {
        self.old_swapchain = Some(old_swapchain);
        self
    }

    /// Builds the [`Swapchain`].
    ///
    /// # Errors
    /// This function returns an error if the swapchain creation fails.
    pub fn build(&mut self) -> Result<Arc<Swapchain>, GfxError> {
        let instance = self.device.physical_device().instance().inner();
        let device = self.device.inner();
        let loader = ash::extensions::khr::Swapchain::new(instance, device);

        let mut builder = ash::vk::SwapchainCreateInfoKHR::builder()
            .surface(self.surface.handle())
            .min_image_count(self.min_image_count)
            .image_format(self.image_format.into())
            .image_color_space(self.image_color_space.into())
            .image_extent(self.image_extent.into())
            .image_array_layers(1)
            .image_usage(self.image_usage.into())
            .image_sharing_mode(self.image_sharing_mode.clone().into())
            .queue_family_indices(self.image_sharing_mode.as_ref())
            .pre_transform(self.pre_transform.into())
            .composite_alpha(ash::vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(self.present_mode.into())
            .clipped(self.clipped);
        if let Some(old_swapchain) = &self.old_swapchain {
            builder = builder.old_swapchain(old_swapchain.handle());
        }
        let create_info = builder.build();

        let handle = unsafe { loader.create_swapchain(&create_info, None) }?;
        let images = unsafe { loader.get_swapchain_images(handle) }?
            .into_iter()
            .map(|handle| {
                Image::from_handle(
                    self.device.clone(),
                    handle,
                    self.image_format,
                    self.image_extent.into(),
                    1,
                    self.image_usage,
                    self.image_sharing_mode.clone(),
                )
            })
            .collect::<Vec<_>>();
        let mut image_views = Vec::new();
        for image in &images {
            image_views.push(
                ImageView::builder(self.device.clone(), image.clone())
                    .view_type(ImageViewType::Type2D)
                    .build()?,
            );
        }

        Ok(Arc::new(Swapchain {
            image_views,
            images,
            image_extent: self.image_extent,
            image_format: self.image_format,
            device: self.device.clone(),
            loader,
            handle,
        }))
    }
}
