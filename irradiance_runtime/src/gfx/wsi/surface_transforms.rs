use crate::impl_flag_set;

impl_flag_set! {
    SurfaceTransforms,
    doc = "Presentation transforms",
    (VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR, identity),
    (VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR, rotate90),
    (VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR, rotate180),
    (VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR, rotate270),
    (VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR, horizontal_mirror),
    (VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR, horizontal_mirror_rotate90),
    (VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR, horizontal_mirror_rotate180),
    (VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR, horizontal_mirror_rotate270),
    (VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR, inherit)
}

#[doc(hidden)]
impl From<ash::vk::SurfaceTransformFlagsKHR> for SurfaceTransforms {
    fn from(surface_transform_flags: ash::vk::SurfaceTransformFlagsKHR) -> Self {
        Self {
            identity: surface_transform_flags & ash::vk::SurfaceTransformFlagsKHR::IDENTITY
                == ash::vk::SurfaceTransformFlagsKHR::IDENTITY,
            rotate90: surface_transform_flags & ash::vk::SurfaceTransformFlagsKHR::ROTATE_90
                == ash::vk::SurfaceTransformFlagsKHR::ROTATE_90,
            rotate180: surface_transform_flags & ash::vk::SurfaceTransformFlagsKHR::ROTATE_180
                == ash::vk::SurfaceTransformFlagsKHR::ROTATE_180,
            rotate270: surface_transform_flags & ash::vk::SurfaceTransformFlagsKHR::ROTATE_270
                == ash::vk::SurfaceTransformFlagsKHR::ROTATE_270,
            horizontal_mirror: surface_transform_flags
                & ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR
                == ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR,
            horizontal_mirror_rotate90: surface_transform_flags
                & ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_90
                == ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_90,
            horizontal_mirror_rotate180: surface_transform_flags
                & ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_180
                == ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_180,
            horizontal_mirror_rotate270: surface_transform_flags
                & ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_270
                == ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_270,
            inherit: surface_transform_flags & ash::vk::SurfaceTransformFlagsKHR::INHERIT
                == ash::vk::SurfaceTransformFlagsKHR::INHERIT,
        }
    }
}

#[doc(hidden)]
impl From<SurfaceTransforms> for ash::vk::SurfaceTransformFlagsKHR {
    fn from(surface_transforms: SurfaceTransforms) -> Self {
        let mut surface_transform_flags = ash::vk::SurfaceTransformFlagsKHR::empty();
        if surface_transforms.identity {
            surface_transform_flags |= ash::vk::SurfaceTransformFlagsKHR::IDENTITY;
        }
        if surface_transforms.rotate90 {
            surface_transform_flags |= ash::vk::SurfaceTransformFlagsKHR::ROTATE_90;
        }
        if surface_transforms.rotate180 {
            surface_transform_flags |= ash::vk::SurfaceTransformFlagsKHR::ROTATE_180;
        }
        if surface_transforms.rotate270 {
            surface_transform_flags |= ash::vk::SurfaceTransformFlagsKHR::ROTATE_270;
        }
        if surface_transforms.horizontal_mirror {
            surface_transform_flags |= ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR;
        }
        if surface_transforms.horizontal_mirror_rotate90 {
            surface_transform_flags |=
                ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_90;
        }
        if surface_transforms.horizontal_mirror_rotate180 {
            surface_transform_flags |=
                ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_180;
        }
        if surface_transforms.horizontal_mirror_rotate270 {
            surface_transform_flags |=
                ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_270;
        }
        if surface_transforms.inherit {
            surface_transform_flags |= ash::vk::SurfaceTransformFlagsKHR::INHERIT;
        }
        surface_transform_flags
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::wsi::SurfaceTransforms;

    #[test]
    fn default() {
        assert_eq!(
            SurfaceTransforms {
                identity: false,
                rotate90: false,
                rotate180: false,
                rotate270: false,
                horizontal_mirror: false,
                horizontal_mirror_rotate90: false,
                horizontal_mirror_rotate180: false,
                horizontal_mirror_rotate270: false,
                inherit: false
            },
            Default::default()
        );
    }

    #[test]
    fn none() {
        assert_eq!(SurfaceTransforms::default(), SurfaceTransforms::none());
    }

    #[test]
    fn is_not_a_subset() {
        let a = SurfaceTransforms {
            rotate270: true,
            horizontal_mirror_rotate90: true,
            ..Default::default()
        };
        let b = SurfaceTransforms {
            rotate270: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn is_subset() {
        let a = SurfaceTransforms {
            rotate270: true,
            horizontal_mirror_rotate90: true,
            ..Default::default()
        };
        let b = SurfaceTransforms {
            rotate270: true,
            ..Default::default()
        };
        assert!(b.is_subset(&a));
    }

    #[test]
    fn is_not_a_superset() {
        let a = SurfaceTransforms {
            rotate270: true,
            horizontal_mirror_rotate90: true,
            ..Default::default()
        };
        let b = SurfaceTransforms {
            rotate270: true,
            ..Default::default()
        };
        assert!(!b.is_superset(&a));
    }

    #[test]
    fn is_superset() {
        let a = SurfaceTransforms {
            rotate270: true,
            horizontal_mirror_rotate90: true,
            ..Default::default()
        };
        let b = SurfaceTransforms {
            rotate270: true,
            ..Default::default()
        };
        assert!(a.is_superset(&b));
    }

    #[test]
    fn difference() {
        let a = SurfaceTransforms {
            rotate270: true,
            horizontal_mirror_rotate90: true,
            ..Default::default()
        };
        let b = SurfaceTransforms {
            rotate270: true,
            ..Default::default()
        };
        assert_eq!(
            SurfaceTransforms {
                horizontal_mirror_rotate90: true,
                ..Default::default()
            },
            a.difference(&b)
        );
    }

    #[test]
    fn intersection() {
        let a = SurfaceTransforms {
            rotate270: true,
            horizontal_mirror_rotate90: true,
            ..Default::default()
        };
        let b = SurfaceTransforms {
            rotate270: true,
            ..Default::default()
        };
        assert_eq!(
            SurfaceTransforms {
                rotate270: true,
                ..Default::default()
            },
            a.intersection(&b)
        );
    }

    #[test]
    fn union() {
        let a = SurfaceTransforms {
            rotate270: true,
            horizontal_mirror_rotate90: true,
            ..Default::default()
        };
        let b = SurfaceTransforms {
            identity: true,
            rotate270: true,
            ..Default::default()
        };
        assert_eq!(
            SurfaceTransforms {
                identity: true,
                rotate270: true,
                horizontal_mirror_rotate90: true,
                ..Default::default()
            },
            a.union(&b)
        );
    }

    #[test]
    fn from_surface_transform() {
        assert_eq!(
            SurfaceTransforms {
                identity: true,
                rotate180: true,
                horizontal_mirror: true,
                horizontal_mirror_rotate180: true,
                inherit: true,
                ..Default::default()
            },
            SurfaceTransforms::from(
                ash::vk::SurfaceTransformFlagsKHR::IDENTITY
                    | ash::vk::SurfaceTransformFlagsKHR::ROTATE_180
                    | ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR
                    | ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_180
                    | ash::vk::SurfaceTransformFlagsKHR::INHERIT
            )
        );
        assert_eq!(
            SurfaceTransforms {
                rotate90: true,
                rotate270: true,
                horizontal_mirror_rotate90: true,
                horizontal_mirror_rotate270: true,
                ..Default::default()
            },
            SurfaceTransforms::from(
                ash::vk::SurfaceTransformFlagsKHR::ROTATE_90
                    | ash::vk::SurfaceTransformFlagsKHR::ROTATE_270
                    | ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_90
                    | ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_270
            )
        );
    }

    #[test]
    fn to_surface_transform() {
        assert_eq!(
            ash::vk::SurfaceTransformFlagsKHR::IDENTITY
                | ash::vk::SurfaceTransformFlagsKHR::ROTATE_180
                | ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR
                | ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_180
                | ash::vk::SurfaceTransformFlagsKHR::INHERIT,
            ash::vk::SurfaceTransformFlagsKHR::from(SurfaceTransforms {
                identity: true,
                rotate180: true,
                horizontal_mirror: true,
                horizontal_mirror_rotate180: true,
                inherit: true,
                ..Default::default()
            })
        );
        assert_eq!(
            ash::vk::SurfaceTransformFlagsKHR::ROTATE_90
                | ash::vk::SurfaceTransformFlagsKHR::ROTATE_270
                | ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_90
                | ash::vk::SurfaceTransformFlagsKHR::HORIZONTAL_MIRROR_ROTATE_270,
            ash::vk::SurfaceTransformFlagsKHR::from(SurfaceTransforms {
                rotate90: true,
                rotate270: true,
                horizontal_mirror_rotate90: true,
                horizontal_mirror_rotate270: true,
                ..Default::default()
            })
        );
    }
}
