/// Behavior of sampling with texture coordinates outside an image.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum SamplerAddressMode {
    /// The repeat wrap mode will be used.
    Repeat,
    /// The mirrored repeat wrap mode will be used.
    MirroredRepeat,
    /// The clamp to edge wrap mode will be used.
    ClampToEdge,
    /// The clamp to border wrap mode will be used.
    ClampToBorder,
    /// The mirror clamp to edge wrap mode will be used.
    MirrorClampToEdge,
}

#[doc(hidden)]
impl From<SamplerAddressMode> for ash::vk::SamplerAddressMode {
    fn from(sampler_address_mode: SamplerAddressMode) -> Self {
        match sampler_address_mode {
            SamplerAddressMode::Repeat => ash::vk::SamplerAddressMode::REPEAT,
            SamplerAddressMode::MirroredRepeat => ash::vk::SamplerAddressMode::MIRRORED_REPEAT,
            SamplerAddressMode::ClampToEdge => ash::vk::SamplerAddressMode::CLAMP_TO_EDGE,
            SamplerAddressMode::ClampToBorder => ash::vk::SamplerAddressMode::CLAMP_TO_BORDER,
            SamplerAddressMode::MirrorClampToEdge => {
                ash::vk::SamplerAddressMode::MIRROR_CLAMP_TO_EDGE
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::SamplerAddressMode;

    #[test]
    fn to_sampler_address_mode() {
        assert_eq!(
            ash::vk::SamplerAddressMode::REPEAT,
            ash::vk::SamplerAddressMode::from(SamplerAddressMode::Repeat)
        );
        assert_eq!(
            ash::vk::SamplerAddressMode::MIRRORED_REPEAT,
            ash::vk::SamplerAddressMode::from(SamplerAddressMode::MirroredRepeat)
        );
        assert_eq!(
            ash::vk::SamplerAddressMode::CLAMP_TO_EDGE,
            ash::vk::SamplerAddressMode::from(SamplerAddressMode::ClampToEdge)
        );
        assert_eq!(
            ash::vk::SamplerAddressMode::CLAMP_TO_BORDER,
            ash::vk::SamplerAddressMode::from(SamplerAddressMode::ClampToBorder)
        );
        assert_eq!(
            ash::vk::SamplerAddressMode::MIRROR_CLAMP_TO_EDGE,
            ash::vk::SamplerAddressMode::from(SamplerAddressMode::MirrorClampToEdge)
        );
    }
}
