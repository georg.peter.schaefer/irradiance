use crate::impl_flag_set;

impl_flag_set! {
    ImageCreateFlags,
    doc = "Additional parameters of an image",
    (VK_IMAGE_CREATE_SPARSE_BINDING_BIT, sparse_binding),
    (VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT, sparse_residency),
    (VK_IMAGE_CREATE_SPARSE_ALIASED_BIT, sparse_aliased),
    (VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT, mutable_format),
    (VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT, cube_compatible)
}

#[doc(hidden)]
impl From<ImageCreateFlags> for ash::vk::ImageCreateFlags {
    fn from(image_create_flags: ImageCreateFlags) -> Self {
        let mut flags = ash::vk::ImageCreateFlags::empty();
        if image_create_flags.sparse_binding {
            flags |= ash::vk::ImageCreateFlags::SPARSE_BINDING;
        }
        if image_create_flags.sparse_residency {
            flags |= ash::vk::ImageCreateFlags::SPARSE_RESIDENCY;
        }
        if image_create_flags.sparse_aliased {
            flags |= ash::vk::ImageCreateFlags::SPARSE_ALIASED;
        }
        if image_create_flags.mutable_format {
            flags |= ash::vk::ImageCreateFlags::MUTABLE_FORMAT;
        }
        if image_create_flags.cube_compatible {
            flags |= ash::vk::ImageCreateFlags::CUBE_COMPATIBLE;
        }
        flags
    }
}
