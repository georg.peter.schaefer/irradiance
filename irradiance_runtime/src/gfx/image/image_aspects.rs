use crate::impl_flag_set;

impl_flag_set! {
    ImageAspects,
    doc = "Aspects of an image.",
    (VK_IMAGE_ASPECT_COLOR_BIT, color),
    (VK_IMAGE_ASPECT_DEPTH_BIT, depth),
    (VK_IMAGE_ASPECT_STENCIL_BIT, stencil),
    (VK_IMAGE_ASPECT_METADATA_BIT, metadata),
    (VK_IMAGE_ASPECT_PLANE_0_BIT, plane_0),
    (VK_IMAGE_ASPECT_PLANE_1_BIT, plane_1),
    (VK_IMAGE_ASPECT_PLANE_2_BIT, plane_2),
    (VK_IMAGE_ASPECT_MEMORY_PLANE_0_BIT_EXT, memory_plane_0),
    (VK_IMAGE_ASPECT_MEMORY_PLANE_1_BIT_EXT, memory_plane_1),
    (VK_IMAGE_ASPECT_MEMORY_PLANE_2_BIT_EXT, memory_plane_2),
    (VK_IMAGE_ASPECT_MEMORY_PLANE_3_BIT_EXT, memory_plane_3)
}

#[doc(hidden)]
impl From<ImageAspects> for ash::vk::ImageAspectFlags {
    fn from(image_aspects: ImageAspects) -> Self {
        let mut flags = ash::vk::ImageAspectFlags::empty();
        if image_aspects.color {
            flags |= ash::vk::ImageAspectFlags::COLOR;
        }
        if image_aspects.depth {
            flags |= ash::vk::ImageAspectFlags::DEPTH;
        }
        if image_aspects.stencil {
            flags |= ash::vk::ImageAspectFlags::STENCIL;
        }
        if image_aspects.metadata {
            flags |= ash::vk::ImageAspectFlags::METADATA;
        }
        if image_aspects.plane_0 {
            flags |= ash::vk::ImageAspectFlags::PLANE_0;
        }
        if image_aspects.plane_1 {
            flags |= ash::vk::ImageAspectFlags::PLANE_1;
        }
        if image_aspects.plane_2 {
            flags |= ash::vk::ImageAspectFlags::PLANE_2;
        }
        if image_aspects.memory_plane_0 {
            flags |= ash::vk::ImageAspectFlags::MEMORY_PLANE_0_EXT;
        }
        if image_aspects.memory_plane_1 {
            flags |= ash::vk::ImageAspectFlags::MEMORY_PLANE_1_EXT;
        }
        if image_aspects.memory_plane_2 {
            flags |= ash::vk::ImageAspectFlags::MEMORY_PLANE_2_EXT;
        }
        if image_aspects.memory_plane_3 {
            flags |= ash::vk::ImageAspectFlags::MEMORY_PLANE_3_EXT;
        }
        flags
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::ImageAspects;

    #[test]
    fn to_image_aspect_flags() {
        assert_eq!(
            ash::vk::ImageAspectFlags::COLOR
                | ash::vk::ImageAspectFlags::STENCIL
                | ash::vk::ImageAspectFlags::PLANE_0
                | ash::vk::ImageAspectFlags::PLANE_2
                | ash::vk::ImageAspectFlags::MEMORY_PLANE_1_EXT
                | ash::vk::ImageAspectFlags::MEMORY_PLANE_3_EXT,
            ash::vk::ImageAspectFlags::from(ImageAspects {
                color: true,
                depth: false,
                stencil: true,
                metadata: false,
                plane_0: true,
                plane_1: false,
                plane_2: true,
                memory_plane_0: false,
                memory_plane_1: true,
                memory_plane_2: false,
                memory_plane_3: true
            })
        );
        assert_eq!(
            ash::vk::ImageAspectFlags::DEPTH
                | ash::vk::ImageAspectFlags::METADATA
                | ash::vk::ImageAspectFlags::PLANE_1
                | ash::vk::ImageAspectFlags::MEMORY_PLANE_0_EXT
                | ash::vk::ImageAspectFlags::MEMORY_PLANE_2_EXT,
            ash::vk::ImageAspectFlags::from(ImageAspects {
                color: false,
                depth: true,
                stencil: false,
                metadata: true,
                plane_0: false,
                plane_1: true,
                plane_2: false,
                memory_plane_0: true,
                memory_plane_1: false,
                memory_plane_2: true,
                memory_plane_3: false
            })
        );
    }
}
