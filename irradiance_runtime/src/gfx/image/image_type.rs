/// Image type.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum ImageType {
    /// One-dimensional image.
    Type1D,
    /// Two-dimensional image.
    Type2D,
    /// Third-dimensional image.
    Type3D,
}

#[doc(hidden)]
impl From<ImageType> for ash::vk::ImageType {
    fn from(image_type: ImageType) -> Self {
        match image_type {
            ImageType::Type1D => ash::vk::ImageType::TYPE_1D,
            ImageType::Type2D => ash::vk::ImageType::TYPE_2D,
            ImageType::Type3D => ash::vk::ImageType::TYPE_3D,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::ImageType;

    #[test]
    fn to_image_type() {
        assert_eq!(
            ash::vk::ImageType::TYPE_1D,
            ash::vk::ImageType::from(ImageType::Type1D)
        );
        assert_eq!(
            ash::vk::ImageType::TYPE_2D,
            ash::vk::ImageType::from(ImageType::Type2D)
        );
        assert_eq!(
            ash::vk::ImageType::TYPE_3D,
            ash::vk::ImageType::from(ImageType::Type3D)
        );
    }
}
