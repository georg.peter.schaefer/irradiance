use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{BorderColor, Filter, SamplerAddressMode, SamplerMipMapMode};
use crate::gfx::pipeline::CompareOp;
use crate::gfx::{GfxError, Handle, ObjectType};

/// An image sampler which is used by the implementation to read image data and apply filtering and
/// other transformations for the shader.
#[derive(Debug)]
pub struct Sampler {
    handle: ash::vk::Sampler,
    device: Arc<Device>,
}

impl Sampler {
    /// Starts building a new sampler.
    pub fn builder(device: Arc<Device>) -> SamplerBuilder {
        SamplerBuilder {
            mag_filter: Filter::Linear,
            min_filter: Filter::Linear,
            mip_map_mode: SamplerMipMapMode::Linear,
            address_mode_u: SamplerAddressMode::Repeat,
            address_mode_v: SamplerAddressMode::Repeat,
            address_mode_w: SamplerAddressMode::Repeat,
            mip_lod_bias: 0.0,
            anisotropy_enabled: false,
            max_anisotropy: 1.0,
            compare_enable: false,
            compare_op: CompareOp::Always,
            min_lod: 0.0,
            max_lod: 1.0,
            border_color: BorderColor::FloatTransparentBlack,
            unnormalized_coordinates: false,
            device,
        }
    }
}

impl Drop for Sampler {
    fn drop(&mut self) {
        unsafe {
            self.device.inner().destroy_sampler(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for Sampler {
    type Type = ash::vk::Sampler;

    fn object_type(&self) -> ObjectType {
        ObjectType::Sampler
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for Sampler {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`Sampler`].
#[derive(Debug)]
pub struct SamplerBuilder {
    mag_filter: Filter,
    min_filter: Filter,
    mip_map_mode: SamplerMipMapMode,
    address_mode_u: SamplerAddressMode,
    address_mode_v: SamplerAddressMode,
    address_mode_w: SamplerAddressMode,
    mip_lod_bias: f32,
    anisotropy_enabled: bool,
    max_anisotropy: f32,
    compare_enable: bool,
    compare_op: CompareOp,
    min_lod: f32,
    max_lod: f32,
    border_color: BorderColor,
    unnormalized_coordinates: bool,
    device: Arc<Device>,
}

impl SamplerBuilder {
    /// Sets the mag filter.
    pub fn mag_filter(&mut self, mag_filter: Filter) -> &mut Self {
        self.mag_filter = mag_filter;
        self
    }

    /// Sets the min filter.
    pub fn min_filter(&mut self, min_filter: Filter) -> &mut Self {
        self.min_filter = min_filter;
        self
    }

    /// Sets the mip map mode.
    pub fn mip_map_mode(&mut self, mip_map_mode: SamplerMipMapMode) -> &mut Self {
        self.mip_map_mode = mip_map_mode;
        self
    }

    /// Sets the address mode u.
    pub fn address_mode_u(&mut self, address_mode_u: SamplerAddressMode) -> &mut Self {
        self.address_mode_u = address_mode_u;
        self
    }

    /// Sets the address mode v.
    pub fn address_mode_v(&mut self, address_mode_v: SamplerAddressMode) -> &mut Self {
        self.address_mode_v = address_mode_v;
        self
    }

    /// Sets the address mode w.
    pub fn address_mode_w(&mut self, address_mode_w: SamplerAddressMode) -> &mut Self {
        self.address_mode_w = address_mode_w;
        self
    }

    /// Sets the mip lod bias.
    pub fn mip_lod_bias(&mut self, mip_lod_bias: f32) -> &mut Self {
        self.mip_lod_bias = mip_lod_bias;
        self
    }

    /// Sets the maximum anisotropy.
    pub fn max_anisotropy(&mut self, max_anisotropy: f32) -> &mut Self {
        self.max_anisotropy = max_anisotropy;
        self.anisotropy_enabled = true;
        self
    }

    /// Sets the compare operation.
    pub fn compare_op(&mut self, compare_op: CompareOp) -> &mut Self {
        self.compare_op = compare_op;
        self.compare_enable = true;
        self
    }

    /// Sets the minimum level of detail.
    pub fn min_lod(&mut self, min_lod: f32) -> &mut Self {
        self.min_lod = min_lod;
        self
    }

    /// Sets the maximum level of detail.
    pub fn max_lod(&mut self, max_lod: f32) -> &mut Self {
        self.max_lod = max_lod;
        self
    }

    /// Sets the border color.
    pub fn border_color(&mut self, border_color: BorderColor) -> &mut Self {
        self.border_color = border_color;
        self
    }

    /// Enables unnormalized coordinates.
    pub fn unnormalized_coordinates(&mut self) -> &mut Self {
        self.unnormalized_coordinates = true;
        self
    }

    /// Builds the [`Sampler`].
    pub fn build(&mut self) -> Result<Arc<Sampler>, GfxError> {
        let create_info = ash::vk::SamplerCreateInfo::builder()
            .min_filter(self.min_filter.into())
            .mag_filter(self.mag_filter.into())
            .mipmap_mode(self.mip_map_mode.into())
            .address_mode_u(self.address_mode_u.into())
            .address_mode_v(self.address_mode_v.into())
            .address_mode_w(self.address_mode_w.into())
            .mip_lod_bias(self.mip_lod_bias)
            .anisotropy_enable(self.anisotropy_enabled)
            .max_anisotropy(self.max_anisotropy)
            .compare_enable(self.compare_enable)
            .compare_op(self.compare_op.into())
            .min_lod(self.min_lod)
            .max_lod(self.max_lod)
            .border_color(self.border_color.into())
            .unnormalized_coordinates(self.unnormalized_coordinates)
            .build();

        let handle = unsafe { self.device.inner().create_sampler(&create_info, None) }?;

        Ok(Arc::new(Sampler {
            handle,
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::image::{BorderColor, Filter, Sampler, SamplerAddressMode, SamplerMipMapMode};
    use crate::gfx::pipeline::CompareOp;
    use crate::physical_device;

    #[test]
    fn build_sampler() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();

        let result = Sampler::builder(device.clone())
            .mag_filter(Filter::Nearest)
            .min_filter(Filter::Nearest)
            .mip_map_mode(SamplerMipMapMode::Nearest)
            .address_mode_u(SamplerAddressMode::Repeat)
            .address_mode_v(SamplerAddressMode::ClampToBorder)
            .address_mode_w(SamplerAddressMode::MirroredRepeat)
            .mip_lod_bias(0.0)
            .max_anisotropy(1.0)
            .compare_op(CompareOp::Never)
            .min_lod(1.0)
            .max_lod(2.3)
            .border_color(BorderColor::FloatOpaqueBlack)
            .unnormalized_coordinates()
            .build();

        assert!(result.is_ok());
    }
}
