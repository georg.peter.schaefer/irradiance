/// Image view types.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum ImageViewType {
    /// One dimensional image view.
    Type1D,
    /// Two dimensional image view.
    Type2D,
    /// Three dimensional image view.
    Type3D,
    /// Cube image view.
    Cube,
    /// One dimensional array image view.
    Type1DArray,
    /// Two dimensional array image view.
    Type2DArray,
    /// Cube array image view.
    CubeArray,
}

#[doc(hidden)]
impl From<ImageViewType> for ash::vk::ImageViewType {
    fn from(image_view_type: ImageViewType) -> Self {
        match image_view_type {
            ImageViewType::Type1D => ash::vk::ImageViewType::TYPE_1D,
            ImageViewType::Type2D => ash::vk::ImageViewType::TYPE_2D,
            ImageViewType::Type3D => ash::vk::ImageViewType::TYPE_3D,
            ImageViewType::Cube => ash::vk::ImageViewType::CUBE,
            ImageViewType::Type1DArray => ash::vk::ImageViewType::TYPE_1D_ARRAY,
            ImageViewType::Type2DArray => ash::vk::ImageViewType::TYPE_2D_ARRAY,
            ImageViewType::CubeArray => ash::vk::ImageViewType::CUBE_ARRAY,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::ImageViewType;

    #[test]
    fn to_image_view_type() {
        assert_eq!(
            ash::vk::ImageViewType::TYPE_1D,
            ash::vk::ImageViewType::from(ImageViewType::Type1D)
        );
        assert_eq!(
            ash::vk::ImageViewType::TYPE_2D,
            ash::vk::ImageViewType::from(ImageViewType::Type2D)
        );
        assert_eq!(
            ash::vk::ImageViewType::TYPE_3D,
            ash::vk::ImageViewType::from(ImageViewType::Type3D)
        );
        assert_eq!(
            ash::vk::ImageViewType::CUBE,
            ash::vk::ImageViewType::from(ImageViewType::Cube)
        );
        assert_eq!(
            ash::vk::ImageViewType::TYPE_1D_ARRAY,
            ash::vk::ImageViewType::from(ImageViewType::Type1DArray)
        );
        assert_eq!(
            ash::vk::ImageViewType::TYPE_2D_ARRAY,
            ash::vk::ImageViewType::from(ImageViewType::Type2DArray)
        );
        assert_eq!(
            ash::vk::ImageViewType::CUBE_ARRAY,
            ash::vk::ImageViewType::from(ImageViewType::CubeArray)
        );
    }
}
