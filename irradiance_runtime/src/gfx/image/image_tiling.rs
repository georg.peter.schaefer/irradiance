/// The tiling arrangement of data in an image.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum ImageTiling {
    /// Optimal tiling (texels are laid out in an implementation-dependent arrangement, for more
    /// efficient memory access).
    Optimal,
    /// Linear tiling (texels are laid out in memory in row-major order, possibly with some padding
    /// on each row).
    Linear,
    /// The image’s tiling is defined by a Linux DRM format modifier.
    DrmFormatModifier,
}

#[doc(hidden)]
impl From<ImageTiling> for ash::vk::ImageTiling {
    fn from(image_tiling: ImageTiling) -> Self {
        match image_tiling {
            ImageTiling::Optimal => ash::vk::ImageTiling::OPTIMAL,
            ImageTiling::Linear => ash::vk::ImageTiling::LINEAR,
            ImageTiling::DrmFormatModifier => ash::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::ImageTiling;

    #[test]
    fn to_image_tiling() {
        assert_eq!(
            ash::vk::ImageTiling::OPTIMAL,
            ash::vk::ImageTiling::from(ImageTiling::Optimal)
        );
        assert_eq!(
            ash::vk::ImageTiling::LINEAR,
            ash::vk::ImageTiling::from(ImageTiling::Linear)
        );
        assert_eq!(
            ash::vk::ImageTiling::DRM_FORMAT_MODIFIER_EXT,
            ash::vk::ImageTiling::from(ImageTiling::DrmFormatModifier)
        );
    }
}
