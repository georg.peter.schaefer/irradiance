/// Border color used for texture lookups.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum BorderColor {
    /// A transparent, floating-point format, black color.
    FloatTransparentBlack,
    /// A transparent, integer format, black color.
    IntTransparentBlack,
    /// An opaque, floating-point format, black color.
    FloatOpaqueBlack,
    /// An opaque, integer format, black color.
    IntOpaqueBlack,
    /// An opaque, floating-point format, white color.
    FloatOpaqueWhite,
    /// An opaque, integer format, white color.
    IntOpaqueWhite,
    /// Custom color data in floating-point format.
    FloatCustom,
    /// Custom color data in integer format.
    IntCustom,
}

#[doc(hidden)]
impl From<BorderColor> for ash::vk::BorderColor {
    fn from(border_color: BorderColor) -> Self {
        match border_color {
            BorderColor::FloatTransparentBlack => ash::vk::BorderColor::FLOAT_TRANSPARENT_BLACK,
            BorderColor::IntTransparentBlack => ash::vk::BorderColor::INT_TRANSPARENT_BLACK,
            BorderColor::FloatOpaqueBlack => ash::vk::BorderColor::FLOAT_OPAQUE_BLACK,
            BorderColor::IntOpaqueBlack => ash::vk::BorderColor::INT_OPAQUE_BLACK,
            BorderColor::FloatOpaqueWhite => ash::vk::BorderColor::FLOAT_OPAQUE_WHITE,
            BorderColor::IntOpaqueWhite => ash::vk::BorderColor::INT_OPAQUE_WHITE,
            BorderColor::FloatCustom => ash::vk::BorderColor::FLOAT_CUSTOM_EXT,
            BorderColor::IntCustom => ash::vk::BorderColor::INT_CUSTOM_EXT,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::BorderColor;

    #[test]
    fn to_border_color() {
        assert_eq!(
            ash::vk::BorderColor::FLOAT_TRANSPARENT_BLACK,
            ash::vk::BorderColor::from(BorderColor::FloatTransparentBlack)
        );
        assert_eq!(
            ash::vk::BorderColor::INT_TRANSPARENT_BLACK,
            ash::vk::BorderColor::from(BorderColor::IntTransparentBlack)
        );
        assert_eq!(
            ash::vk::BorderColor::FLOAT_OPAQUE_BLACK,
            ash::vk::BorderColor::from(BorderColor::FloatOpaqueBlack)
        );
        assert_eq!(
            ash::vk::BorderColor::INT_OPAQUE_BLACK,
            ash::vk::BorderColor::from(BorderColor::IntOpaqueBlack)
        );
        assert_eq!(
            ash::vk::BorderColor::FLOAT_OPAQUE_WHITE,
            ash::vk::BorderColor::from(BorderColor::FloatOpaqueWhite)
        );
        assert_eq!(
            ash::vk::BorderColor::INT_OPAQUE_WHITE,
            ash::vk::BorderColor::from(BorderColor::IntOpaqueWhite)
        );
        assert_eq!(
            ash::vk::BorderColor::FLOAT_CUSTOM_EXT,
            ash::vk::BorderColor::from(BorderColor::FloatCustom)
        );
        assert_eq!(
            ash::vk::BorderColor::INT_CUSTOM_EXT,
            ash::vk::BorderColor::from(BorderColor::IntCustom)
        );
    }
}
