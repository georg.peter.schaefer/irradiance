/// Filters used for texture lookups.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Filter {
    /// Nearest filtering.
    Nearest,
    /// Linear filtering.
    Linear,
    /// Cubic filtering.
    CubicImg,
}

#[doc(hidden)]
impl From<Filter> for ash::vk::Filter {
    fn from(filter: Filter) -> Self {
        match filter {
            Filter::Nearest => ash::vk::Filter::NEAREST,
            Filter::Linear => ash::vk::Filter::LINEAR,
            Filter::CubicImg => ash::vk::Filter::CUBIC_IMG,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::Filter;

    #[test]
    fn to_filter() {
        assert_eq!(
            ash::vk::Filter::NEAREST,
            ash::vk::Filter::from(Filter::Nearest)
        );
        assert_eq!(
            ash::vk::Filter::LINEAR,
            ash::vk::Filter::from(Filter::Linear)
        );
        assert_eq!(
            ash::vk::Filter::CUBIC_IMG,
            ash::vk::Filter::from(Filter::CubicImg)
        );
    }
}
