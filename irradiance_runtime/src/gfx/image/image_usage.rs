use crate::impl_flag_set;

impl_flag_set! {
    ImageUsage,
    doc = "Intended usage of an image.",
    (VK_IMAGE_USAGE_TRANSFER_SRC_BIT, transfer_src),
    (VK_IMAGE_USAGE_TRANSFER_DST_BIT, transfer_dst),
    (VK_IMAGE_USAGE_SAMPLED_BIT, sampled),
    (VK_IMAGE_USAGE_STORAGE_BIT, storage),
    (VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, color_attachment),
    (VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, depth_stencil_attachment),
    (VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT, transient_attachment),
    (VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT, input_attachment),
    (VK_IMAGE_USAGE_VIDEO_DECODE_DST_BIT_KHR, video_decode_dst),
    (VK_IMAGE_USAGE_VIDEO_DECODE_SRC_BIT_KHR, video_decode_src),
    (VK_IMAGE_USAGE_VIDEO_DECODE_DPB_BIT_KHR, video_decode_dpb),
    (VK_IMAGE_USAGE_FRAGMENT_DENSITY_MAP_BIT_EXT, fragment_density_map),
    (VK_IMAGE_USAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR, fragment_shading_rate_attachment),
    (VK_IMAGE_USAGE_VIDEO_ENCODE_DST_BIT_KHR, video_encode_dst),
    (VK_IMAGE_USAGE_VIDEO_ENCODE_SRC_BIT_KHR, video_encode_src),
    (VK_IMAGE_USAGE_VIDEO_ENCODE_DPB_BIT_KHR, video_encode_dpb),
    (VK_IMAGE_USAGE_INVOCATION_MASK_BIT_HUAWEI, invocation_mask)
}

#[doc(hidden)]
impl From<ImageUsage> for ash::vk::ImageUsageFlags {
    fn from(image_usage: ImageUsage) -> Self {
        let mut image_usage_flags = ash::vk::ImageUsageFlags::empty();
        if image_usage.transfer_src {
            image_usage_flags |= ash::vk::ImageUsageFlags::TRANSFER_SRC;
        }
        if image_usage.transfer_dst {
            image_usage_flags |= ash::vk::ImageUsageFlags::TRANSFER_DST;
        }
        if image_usage.sampled {
            image_usage_flags |= ash::vk::ImageUsageFlags::SAMPLED;
        }
        if image_usage.storage {
            image_usage_flags |= ash::vk::ImageUsageFlags::STORAGE;
        }
        if image_usage.color_attachment {
            image_usage_flags |= ash::vk::ImageUsageFlags::COLOR_ATTACHMENT;
        }
        if image_usage.depth_stencil_attachment {
            image_usage_flags |= ash::vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT;
        }
        if image_usage.transient_attachment {
            image_usage_flags |= ash::vk::ImageUsageFlags::TRANSIENT_ATTACHMENT;
        }
        if image_usage.input_attachment {
            image_usage_flags |= ash::vk::ImageUsageFlags::INPUT_ATTACHMENT;
        }
        if image_usage.video_decode_dst {
            image_usage_flags |= ash::vk::ImageUsageFlags::VIDEO_DECODE_DST_KHR;
        }
        if image_usage.video_decode_src {
            image_usage_flags |= ash::vk::ImageUsageFlags::VIDEO_DECODE_SRC_KHR;
        }
        if image_usage.video_decode_dpb {
            image_usage_flags |= ash::vk::ImageUsageFlags::VIDEO_DECODE_DPB_KHR;
        }
        if image_usage.fragment_density_map {
            image_usage_flags |= ash::vk::ImageUsageFlags::FRAGMENT_DENSITY_MAP_EXT;
        }
        if image_usage.fragment_shading_rate_attachment {
            image_usage_flags |= ash::vk::ImageUsageFlags::FRAGMENT_SHADING_RATE_ATTACHMENT_KHR;
        }
        if image_usage.video_encode_dst {
            image_usage_flags |= ash::vk::ImageUsageFlags::VIDEO_ENCODE_DST_KHR;
        }
        if image_usage.video_encode_src {
            image_usage_flags |= ash::vk::ImageUsageFlags::VIDEO_ENCODE_SRC_KHR;
        }
        if image_usage.video_encode_dpb {
            image_usage_flags |= ash::vk::ImageUsageFlags::VIDEO_ENCODE_DPB_KHR;
        }
        if image_usage.invocation_mask {
            image_usage_flags |= ash::vk::ImageUsageFlags::INVOCATION_MASK_HUAWEI;
        }
        image_usage_flags
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::ImageUsage;

    #[test]
    fn default() {
        assert_eq!(
            ImageUsage {
                transfer_src: false,
                transfer_dst: false,
                sampled: false,
                storage: false,
                color_attachment: false,
                depth_stencil_attachment: false,
                transient_attachment: false,
                input_attachment: false,
                video_decode_dst: false,
                video_decode_src: false,
                video_decode_dpb: false,
                fragment_density_map: false,
                fragment_shading_rate_attachment: false,
                video_encode_dst: false,
                video_encode_src: false,
                video_encode_dpb: false,
                invocation_mask: false
            },
            Default::default()
        );
    }

    #[test]
    fn none() {
        assert_eq!(ImageUsage::default(), ImageUsage::none());
    }

    #[test]
    fn is_not_a_subset() {
        let a = ImageUsage {
            transient_attachment: true,
            depth_stencil_attachment: true,
            ..Default::default()
        };
        let b = ImageUsage {
            transient_attachment: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn is_subset() {
        let a = ImageUsage {
            transient_attachment: true,
            depth_stencil_attachment: true,
            ..Default::default()
        };
        let b = ImageUsage {
            transient_attachment: true,
            ..Default::default()
        };
        assert!(b.is_subset(&a));
    }

    #[test]
    fn is_not_a_superset() {
        let a = ImageUsage {
            transient_attachment: true,
            depth_stencil_attachment: true,
            ..Default::default()
        };
        let b = ImageUsage {
            transient_attachment: true,
            ..Default::default()
        };
        assert!(!b.is_superset(&a));
    }

    #[test]
    fn is_superset() {
        let a = ImageUsage {
            transient_attachment: true,
            depth_stencil_attachment: true,
            ..Default::default()
        };
        let b = ImageUsage {
            transient_attachment: true,
            ..Default::default()
        };
        assert!(a.is_superset(&b));
    }

    #[test]
    fn difference() {
        let a = ImageUsage {
            transient_attachment: true,
            depth_stencil_attachment: true,
            ..Default::default()
        };
        let b = ImageUsage {
            transient_attachment: true,
            ..Default::default()
        };
        assert_eq!(
            ImageUsage {
                depth_stencil_attachment: true,
                ..Default::default()
            },
            a.difference(&b)
        );
    }

    #[test]
    fn intersection() {
        let a = ImageUsage {
            transient_attachment: true,
            depth_stencil_attachment: true,
            ..Default::default()
        };
        let b = ImageUsage {
            transient_attachment: true,
            ..Default::default()
        };
        assert_eq!(
            ImageUsage {
                transient_attachment: true,
                ..Default::default()
            },
            a.intersection(&b)
        );
    }

    #[test]
    fn union() {
        let a = ImageUsage {
            transient_attachment: true,
            depth_stencil_attachment: true,
            ..Default::default()
        };
        let b = ImageUsage {
            transient_attachment: true,
            input_attachment: true,
            ..Default::default()
        };
        assert_eq!(
            ImageUsage {
                transient_attachment: true,
                depth_stencil_attachment: true,
                input_attachment: true,
                ..Default::default()
            },
            a.union(&b)
        );
    }

    #[test]
    fn to_image_usage() {
        assert_eq!(
            ash::vk::ImageUsageFlags::TRANSFER_SRC
                | ash::vk::ImageUsageFlags::SAMPLED
                | ash::vk::ImageUsageFlags::COLOR_ATTACHMENT
                | ash::vk::ImageUsageFlags::TRANSIENT_ATTACHMENT
                | ash::vk::ImageUsageFlags::VIDEO_DECODE_DST_KHR
                | ash::vk::ImageUsageFlags::VIDEO_DECODE_DPB_KHR
                | ash::vk::ImageUsageFlags::FRAGMENT_SHADING_RATE_ATTACHMENT_KHR
                | ash::vk::ImageUsageFlags::VIDEO_ENCODE_SRC_KHR
                | ash::vk::ImageUsageFlags::INVOCATION_MASK_HUAWEI,
            ash::vk::ImageUsageFlags::from(ImageUsage {
                transfer_src: true,
                sampled: true,
                color_attachment: true,
                transient_attachment: true,
                video_decode_dst: true,
                video_decode_dpb: true,
                fragment_shading_rate_attachment: true,
                video_encode_src: true,
                invocation_mask: true,
                ..Default::default()
            })
        );
        assert_eq!(
            ash::vk::ImageUsageFlags::TRANSFER_DST
                | ash::vk::ImageUsageFlags::STORAGE
                | ash::vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT
                | ash::vk::ImageUsageFlags::INPUT_ATTACHMENT
                | ash::vk::ImageUsageFlags::VIDEO_DECODE_SRC_KHR
                | ash::vk::ImageUsageFlags::FRAGMENT_DENSITY_MAP_EXT
                | ash::vk::ImageUsageFlags::VIDEO_ENCODE_DST_KHR
                | ash::vk::ImageUsageFlags::VIDEO_ENCODE_DPB_KHR,
            ash::vk::ImageUsageFlags::from(ImageUsage {
                transfer_dst: true,
                storage: true,
                depth_stencil_attachment: true,
                input_attachment: true,
                video_decode_src: true,
                fragment_density_map: true,
                video_encode_dst: true,
                video_encode_dpb: true,
                ..Default::default()
            })
        );
    }
}
