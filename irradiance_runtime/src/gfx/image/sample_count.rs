/// Sample counts supported for an image used for storage operations.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum SampleCount {
    /// An image with one sample per pixel.
    Sample1,
    /// An image with 2 samples per pixel.
    Sample2,
    /// An image with 4 samples per pixel.
    Sample4,
    /// An image with 8 samples per pixel.
    Sample8,
    /// An image with 16 samples per pixel.
    Sample16,
    /// An image with 32 samples per pixel.
    Sample32,
    /// An image with 64 samples per pixel.
    Sample64,
}

#[doc(hidden)]
impl From<SampleCount> for ash::vk::SampleCountFlags {
    fn from(sample_count: SampleCount) -> Self {
        match sample_count {
            SampleCount::Sample1 => ash::vk::SampleCountFlags::TYPE_1,
            SampleCount::Sample2 => ash::vk::SampleCountFlags::TYPE_2,
            SampleCount::Sample4 => ash::vk::SampleCountFlags::TYPE_4,
            SampleCount::Sample8 => ash::vk::SampleCountFlags::TYPE_8,
            SampleCount::Sample16 => ash::vk::SampleCountFlags::TYPE_16,
            SampleCount::Sample32 => ash::vk::SampleCountFlags::TYPE_32,
            SampleCount::Sample64 => ash::vk::SampleCountFlags::TYPE_64,
        }
    }
}

#[cfg(test)]
mod tests {
    use irradiance_runtime::gfx::image::SampleCount;

    #[test]
    fn to_sample_count_flags() {
        assert_eq!(
            ash::vk::SampleCountFlags::TYPE_1,
            ash::vk::SampleCountFlags::from(SampleCount::Sample1)
        );
        assert_eq!(
            ash::vk::SampleCountFlags::TYPE_2,
            ash::vk::SampleCountFlags::from(SampleCount::Sample2)
        );
        assert_eq!(
            ash::vk::SampleCountFlags::TYPE_4,
            ash::vk::SampleCountFlags::from(SampleCount::Sample4)
        );
        assert_eq!(
            ash::vk::SampleCountFlags::TYPE_8,
            ash::vk::SampleCountFlags::from(SampleCount::Sample8)
        );
        assert_eq!(
            ash::vk::SampleCountFlags::TYPE_16,
            ash::vk::SampleCountFlags::from(SampleCount::Sample16)
        );
        assert_eq!(
            ash::vk::SampleCountFlags::TYPE_32,
            ash::vk::SampleCountFlags::from(SampleCount::Sample32)
        );
        assert_eq!(
            ash::vk::SampleCountFlags::TYPE_64,
            ash::vk::SampleCountFlags::from(SampleCount::Sample64)
        );
    }
}
