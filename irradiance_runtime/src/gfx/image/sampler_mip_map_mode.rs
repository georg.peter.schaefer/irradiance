/// Mipmap mode used for texture lookups
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum SamplerMipMapMode {
    /// Nearest filtering.
    Nearest,
    /// Linear filtering.
    Linear,
}

#[doc(hidden)]
impl From<SamplerMipMapMode> for ash::vk::SamplerMipmapMode {
    fn from(sampler_mip_map_mode: SamplerMipMapMode) -> Self {
        match sampler_mip_map_mode {
            SamplerMipMapMode::Nearest => ash::vk::SamplerMipmapMode::NEAREST,
            SamplerMipMapMode::Linear => ash::vk::SamplerMipmapMode::LINEAR,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::SamplerMipMapMode;

    #[test]
    fn to_sampler_mip_map_mode() {
        assert_eq!(
            ash::vk::SamplerMipmapMode::NEAREST,
            ash::vk::SamplerMipmapMode::from(SamplerMipMapMode::Nearest)
        );
        assert_eq!(
            ash::vk::SamplerMipmapMode::LINEAR,
            ash::vk::SamplerMipmapMode::from(SamplerMipMapMode::Linear)
        );
    }
}
