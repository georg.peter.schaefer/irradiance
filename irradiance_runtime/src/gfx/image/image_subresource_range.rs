use crate::gfx::image::ImageAspects;

/// Image subresource range.
#[derive(Copy, Clone, Debug, Default)]
pub struct ImageSubresourceRange {
    layer_count: u32,
    base_array_layer: u32,
    level_count: u32,
    base_mip_level: u32,
    aspect_mask: ImageAspects,
}

impl ImageSubresourceRange {
    /// Starts building a new image subresource range.
    pub fn builder() -> ImageSubresourceRangeBuilder {
        ImageSubresourceRangeBuilder {
            layer_count: 1,
            base_array_layer: 0,
            level_count: 1,
            base_mip_level: 0,
            aspect_mask: Default::default(),
        }
    }
}

#[doc(hidden)]
impl From<ImageSubresourceRange> for ash::vk::ImageSubresourceRange {
    fn from(image_subresource_range: ImageSubresourceRange) -> Self {
        ash::vk::ImageSubresourceRange::builder()
            .aspect_mask(image_subresource_range.aspect_mask.into())
            .base_mip_level(image_subresource_range.base_mip_level)
            .level_count(image_subresource_range.level_count)
            .base_array_layer(image_subresource_range.base_array_layer)
            .layer_count(image_subresource_range.layer_count)
            .build()
    }
}

/// Type for building a [`ImageSubresourceRange`].
#[derive(Debug)]
pub struct ImageSubresourceRangeBuilder {
    layer_count: u32,
    base_array_layer: u32,
    level_count: u32,
    base_mip_level: u32,
    aspect_mask: ImageAspects,
}

impl ImageSubresourceRangeBuilder {
    /// Sets the aspect mask.
    pub fn aspect_mask(&mut self, aspect_mask: ImageAspects) -> &mut Self {
        self.aspect_mask = aspect_mask;
        self
    }

    /// Sets the base mip level.
    pub fn base_mip_level(&mut self, base_mip_level: u32) -> &mut Self {
        self.base_mip_level = base_mip_level;
        self
    }

    /// Sets the level count.
    pub fn level_count(&mut self, level_count: u32) -> &mut Self {
        self.level_count = level_count;
        self
    }

    /// Sets the base array layer.
    pub fn base_array_layer(&mut self, base_array_layer: u32) -> &mut Self {
        self.base_array_layer = base_array_layer;
        self
    }

    /// Sets the layer count.
    pub fn layer_count(&mut self, layer_count: u32) -> &mut Self {
        self.layer_count = layer_count;
        self
    }

    /// Builds the [`ImageSubresourceRange`].
    pub fn build(&mut self) -> ImageSubresourceRange {
        ImageSubresourceRange {
            layer_count: self.layer_count,
            base_array_layer: self.base_array_layer,
            level_count: self.level_count,
            base_mip_level: self.base_mip_level,
            aspect_mask: self.aspect_mask,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::{ImageAspects, ImageSubresourceRange};

    #[test]
    fn build_image_subresource_range() {
        let image_subresource_range = ImageSubresourceRange::builder()
            .aspect_mask(ImageAspects {
                color: true,
                ..Default::default()
            })
            .base_mip_level(0)
            .level_count(8)
            .base_array_layer(1)
            .layer_count(3)
            .build();

        assert_eq!(
            ImageAspects {
                color: true,
                ..Default::default()
            },
            image_subresource_range.aspect_mask
        );
        assert_eq!(0, image_subresource_range.base_mip_level);
        assert_eq!(8, image_subresource_range.level_count);
        assert_eq!(1, image_subresource_range.base_array_layer);
        assert_eq!(3, image_subresource_range.layer_count);
    }

    #[test]
    fn to_image_subresource_range() {
        let image_subresource_range = ash::vk::ImageSubresourceRange::from(
            ImageSubresourceRange::builder()
                .aspect_mask(ImageAspects {
                    color: true,
                    ..Default::default()
                })
                .base_mip_level(0)
                .level_count(8)
                .base_array_layer(1)
                .layer_count(3)
                .build(),
        );

        assert_eq!(
            ash::vk::ImageAspectFlags::COLOR,
            image_subresource_range.aspect_mask
        );
        assert_eq!(0, image_subresource_range.base_mip_level);
        assert_eq!(8, image_subresource_range.level_count);
        assert_eq!(1, image_subresource_range.base_array_layer);
        assert_eq!(3, image_subresource_range.layer_count);
    }
}
