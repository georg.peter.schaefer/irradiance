use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::image_view_type::ImageViewType;
use crate::gfx::image::{Image, ImageAspects, ImageSubresourceRange};
use crate::gfx::{GfxError, Handle, ObjectType};

/// View for accessing an [`Image`] by pipeline shaders.
#[derive(Debug)]
pub struct ImageView {
    image: Arc<Image>,
    handle: ash::vk::ImageView,
    device: Arc<Device>,
}

impl ImageView {
    /// Starts building a new image view.
    pub fn builder(device: Arc<Device>, image: Arc<Image>) -> ImageViewBuilder {
        ImageViewBuilder {
            subresource_range: ImageSubresourceRange::builder()
                .aspect_mask(ImageAspects {
                    color: true,
                    ..Default::default()
                })
                .base_mip_level(0)
                .level_count(1)
                .base_array_layer(0)
                .layer_count(1)
                .build(),
            view_type: ImageViewType::Type2D,
            image,
            device,
        }
    }

    /// Returns the [`Image`].
    pub fn image(&self) -> &Arc<Image> {
        &self.image
    }
}

impl Drop for ImageView {
    fn drop(&mut self) {
        unsafe {
            self.device.inner().destroy_image_view(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for ImageView {
    type Type = ash::vk::ImageView;

    fn object_type(&self) -> ObjectType {
        ObjectType::ImageView
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for ImageView {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building an [`ImageView`].
#[derive(Debug)]
pub struct ImageViewBuilder {
    subresource_range: ImageSubresourceRange,
    view_type: ImageViewType,
    image: Arc<Image>,
    device: Arc<Device>,
}

impl ImageViewBuilder {
    /// Sets the [`ImageViewType`].
    pub fn view_type(&mut self, view_type: ImageViewType) -> &mut Self {
        self.view_type = view_type;
        self
    }

    /// Sets the [`ImageSubresourceRange`].
    pub fn subresource_range(&mut self, subresource_range: ImageSubresourceRange) -> &mut Self {
        self.subresource_range = subresource_range;
        self
    }

    /// Builds the [`ImageView`].
    ///
    /// # Errors
    /// This function returns an error if the image view creation fails.
    pub fn build(&mut self) -> Result<Arc<ImageView>, GfxError> {
        let create_info = ash::vk::ImageViewCreateInfo::builder()
            .image(self.image.handle())
            .view_type(self.view_type.into())
            .format(self.image.format().into())
            .components(
                ash::vk::ComponentMapping::builder()
                    .r(ash::vk::ComponentSwizzle::R)
                    .g(ash::vk::ComponentSwizzle::G)
                    .b(ash::vk::ComponentSwizzle::B)
                    .a(ash::vk::ComponentSwizzle::A)
                    .build(),
            )
            .subresource_range(self.subresource_range.into())
            .build();

        let handle = unsafe { self.device.inner().create_image_view(&create_info, None) }?;

        Ok(Arc::new(ImageView {
            image: self.image.clone(),
            handle,
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::image::{
        Format, Image, ImageLayout, ImageTiling, ImageType, ImageUsage, ImageView, ImageViewType,
        SampleCount,
    };
    use crate::gfx::sync::SharingMode;
    use crate::gfx::{Extent3D, Handle};
    use crate::physical_device;

    #[test]
    fn build_image_view() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let image = Image::builder(device.clone())
            .image_type(ImageType::Type2D)
            .format(Format::R8G8B8A8Srgb)
            .extent(Extent3D {
                width: 1024,
                height: 1024,
                depth: 1,
            })
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCount::Sample1)
            .tiling(ImageTiling::Optimal)
            .usage(ImageUsage {
                sampled: true,
                transfer_dst: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .initial_layout(ImageLayout::Undefined)
            .build()
            .unwrap();

        let result = ImageView::builder(device.clone(), image.clone())
            .view_type(ImageViewType::Type2D)
            .build();

        assert!(result.is_ok());

        assert_eq!(image.handle(), result.unwrap().image().handle());
    }
}
