use crate::gfx::image::ImageAspects;

/// Image subresource layers.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct ImageSubresourceLayers {
    layer_count: u32,
    base_array_layer: u32,
    mip_level: u32,
    image_aspects: ImageAspects,
}

impl ImageSubresourceLayers {
    /// Starts building new image subresource layers.
    pub fn builder() -> ImageSubresourceLayersBuilder {
        ImageSubresourceLayersBuilder {
            layer_count: 1,
            base_array_layer: 0,
            mip_level: 0,
            image_aspects: Default::default(),
        }
    }
}

#[doc(hidden)]
impl From<ImageSubresourceLayers> for ash::vk::ImageSubresourceLayers {
    fn from(image_subresource_layers: ImageSubresourceLayers) -> Self {
        ash::vk::ImageSubresourceLayers::builder()
            .aspect_mask(image_subresource_layers.image_aspects.into())
            .mip_level(image_subresource_layers.mip_level)
            .base_array_layer(image_subresource_layers.base_array_layer)
            .layer_count(image_subresource_layers.layer_count)
            .build()
    }
}

/// Type for building [`ImageSubresourceLayers`].
#[derive(Debug)]
pub struct ImageSubresourceLayersBuilder {
    layer_count: u32,
    base_array_layer: u32,
    mip_level: u32,
    image_aspects: ImageAspects,
}

impl ImageSubresourceLayersBuilder {
    /// Sets the image aspects.
    pub fn image_aspects(&mut self, image_aspects: ImageAspects) -> &mut Self {
        self.image_aspects = image_aspects;
        self
    }

    /// Sets the mip level.
    pub fn mip_level(&mut self, mip_level: u32) -> &mut Self {
        self.mip_level = mip_level;
        self
    }

    /// Sets the base array layer.
    pub fn base_array_layer(&mut self, base_array_layer: u32) -> &mut Self {
        self.base_array_layer = base_array_layer;
        self
    }

    /// Sets the layer count.
    pub fn layer_count(&mut self, layer_count: u32) -> &mut Self {
        self.layer_count = layer_count;
        self
    }

    /// Builds the [`ImageSubresourceLayers`].
    pub fn build(&mut self) -> ImageSubresourceLayers {
        ImageSubresourceLayers {
            layer_count: self.layer_count,
            base_array_layer: self.base_array_layer,
            mip_level: self.mip_level,
            image_aspects: self.image_aspects,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::{ImageAspects, ImageSubresourceLayers};

    #[test]
    fn to_image_subresource_layers() {
        let image_subresource_layers = ash::vk::ImageSubresourceLayers::from(
            ImageSubresourceLayers::builder()
                .image_aspects(ImageAspects {
                    color: true,
                    ..Default::default()
                })
                .mip_level(1)
                .base_array_layer(0)
                .layer_count(1)
                .build(),
        );

        assert_eq!(
            ash::vk::ImageAspectFlags::COLOR,
            image_subresource_layers.aspect_mask
        );
        assert_eq!(1, image_subresource_layers.mip_level);
        assert_eq!(0, image_subresource_layers.base_array_layer);
        assert_eq!(1, image_subresource_layers.layer_count);
    }
}
