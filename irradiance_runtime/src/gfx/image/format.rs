/// Available image formats.
#[allow(missing_docs)]
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Format {
    Undefined,
    R4G4UNormPack8,
    R4G4B4A4UNormPack16,
    B4G4R4A4UNormPack16,
    R5G6B5UNormPack16,
    B5G6R5UNormPack16,
    R5G5B5A1UNormPack16,
    B5G5R5A1UNormPack16,
    A1R5G5B5UNormPack16,
    R8UNorm,
    R8SNorm,
    R8UScaled,
    R8SScaled,
    R8UInt,
    R8SInt,
    R8Srgb,
    R8G8UNorm,
    R8G8SNorm,
    R8G8UScaled,
    R8G8SScaled,
    R8G8UInt,
    R8G8SInt,
    R8G8Srgb,
    R8G8B8UNorm,
    R8G8B8SNorm,
    R8G8B8UScaled,
    R8G8B8SScaled,
    R8G8B8UInt,
    R8G8B8SInt,
    R8G8B8Srgb,
    B8G8R8UNorm,
    B8G8R8SNorm,
    B8G8R8UScaled,
    B8G8R8SScaled,
    B8G8R8UInt,
    B8G8R8SInt,
    B8G8R8Srgb,
    R8G8B8A8UNorm,
    R8G8B8A8SNorm,
    R8G8B8A8UScaled,
    R8G8B8A8SScaled,
    R8G8B8A8UInt,
    R8G8B8A8SInt,
    R8G8B8A8Srgb,
    B8G8R8A8UNorm,
    B8G8R8A8SNorm,
    B8G8R8A8UScaled,
    B8G8R8A8SScaled,
    B8G8R8A8UInt,
    B8G8R8A8SInt,
    B8G8R8A8Srgb,
    A8B8G8R8UNormPack32,
    A8B8G8R8SNormPack32,
    A8B8G8R8UScaledPack32,
    A8B8G8R8SScaledPack32,
    A8B8G8R8UIntPack32,
    A8B8G8R8SIntPack32,
    A8B8G8R8SrgbPack32,
    A2R10G10B10UNormPack32,
    A2R10G10B10SNormPack32,
    A2R10G10B10UScaledPack32,
    A2R10G10B10SScaledPack32,
    A2R10G10B10UIntPack32,
    A2R10G10B10SIntPack32,
    A2B10G10R10UNormPack32,
    A2B10G10R10SNormPack32,
    A2B10G10R10UScaledPack32,
    A2B10G10R10SScaledPack32,
    A2B10G10R10UIntPack32,
    A2B10G10R10SIntPack32,
    R16UNorm,
    R16SNorm,
    R16UScaled,
    R16SScaled,
    R16UInt,
    R16SInt,
    R16SFloat,
    R16G16UNorm,
    R16G16SNorm,
    R16G16UScaled,
    R16G16SScaled,
    R16G16UInt,
    R16G16SInt,
    R16G16SFloat,
    R16G16B16UNorm,
    R16G16B16SNorm,
    R16G16B16UScaled,
    R16G16B16SScaled,
    R16G16B16UInt,
    R16G16B16SInt,
    R16G16B16SFloat,
    R16G16B16A16UNorm,
    R16G16B16A16SNorm,
    R16G16B16A16UScaled,
    R16G16B16A16SScaled,
    R16G16B16A16UInt,
    R16G16B16A16SInt,
    R16G16B16A16SFloat,
    R32UInt,
    R32SInt,
    R32SFloat,
    R32G32UInt,
    R32G32SInt,
    R32G32SFloat,
    R32G32B32UInt,
    R32G32B32SInt,
    R32G32B32SFloat,
    R32G32B32A32UInt,
    R32G32B32A32SInt,
    R32G32B32A32SFloat,
    R64UInt,
    R64SInt,
    R64SFloat,
    R64G64UInt,
    R64B64SInt,
    R64B64SFloat,
    R64G64B64UInt,
    R64G64B64SInt,
    R64G64B64SFloat,
    R64G64B64A64UInt,
    R64G64B64A64SInt,
    R64G64B64A64SFloat,
    B10G11R11UFloatPack32,
    E5B9G9R9UFloatPack32,
    D16UNorm,
    X8D24UNormPack32,
    D32SFloat,
    S8UInt,
    D16UNormS8UInt,
    D24UNormS8UInt,
    D32SFloatS8UInt,
    BC1RgbUNormBlock,
    BC1RgbSrgbBlock,
    BC1RgbaUNormBlock,
    BC1RgbaSrgbBlock,
    BC2UNormBlock,
    BC2SrgbBlock,
    BC3UNormBlock,
    BC3SrgbBlock,
    BC4UNormBlock,
    BC4SNormBlock,
    BC5UNormBlock,
    BC5SNormBlock,
    BC6HUFloatBlock,
    BC6HSFloatBlock,
    BC7UNormBlock,
    BC7SrgbBlock,
    ETC2R8G8B8UNormBlock,
    ETC2R8G8B8SrgbBlock,
    ETC2R8G8B8A1UNormBlock,
    ETC2R8G8B8A1SrgbBlock,
    ETC2R8G8B8A8UNormBlock,
    ETC2R8G8B8A8SrgbBlock,
    EACR11UNormBlock,
    EACR11SNormBlock,
    EACR11G11UNormBlock,
    EACR11G11SNormBlock,
    ASTC4x4UNormBlock,
    ASTC4x4SrgbBlock,
    ASTC5x4UNormBlock,
    ASTC5x4SrgbBlock,
    ASTC5x5UNormBlock,
    ASTC5x5SrgbBlock,
    ASTC6x5UNormBlock,
    ASTC6x5SrgbBlock,
    ASTC6x6UNormBlock,
    ASTC6x6SrgbBlock,
    ASTC8x5UNormBlock,
    ASTC8x5SrgbBlock,
    ASTC8x6UNormBlock,
    ASTC8x6SrgbBlock,
    ASTC8x8UNormBlock,
    ASTC8x8SrgbBlock,
    ASTC10x5UNormBlock,
    ASTC10x5SrgbBlock,
    ASTC10x6UNormBlock,
    ASTC10x6SrgbBlock,
    ASTC10x8UNormBlock,
    ASTC10x8SrgbBlock,
    ASTC10x10UNormBlock,
    ASTC10x10SrgbBlock,
    ASTC12x10UNormBlock,
    ASTC12x10SrgbBlock,
    ASTC12x12UNormBlock,
    ASTC12x12SrgbBlock,
    G8B8G8R8422UNorm,
    B8G8R8G8422UNorm,
    G8B8R83Plane420UNorm,
    G8B8R82Plane420UNorm,
    G8B8R83Plane422UNorm,
    G8B8R82Plane422UNorm,
    G8B8R83Plane444UNorm,
    R10X6UNormPack16,
    R10X6G10X6UNorm2Pack16,
    R10X6G10X6B10X6A10X6UNorm4Pack16,
    G10X6B10X6G10X6R10X6422UNorm4Pack16,
    B10X6G10X6R10X6G10X6422UNorm4Pack16,
    G10X6B10X6R10X63Plane420UNorm3Pack16,
    G10X6B10X6R10X62Plane420UNorm3Pack16,
    G10X6B10X6R10X63Plane422UNorm3Pack16,
    G10X6B10X6R10X62Plane422UNorm3Pack16,
    G10X6B10X6R10X63Plane444UNorm3Pack16,
    R12X4UNormPack16,
    R12X4G12X4UNorm2Pack16,
    R12X4G12X4B12X4A12X4UNorm4Pack16,
    G12X4B12X4G12X4R12X4422UNorm4Pack16,
    B12X4G12X4R12X4G12X4422UNorm4Pack16,
    G12X4B12X4R12X43Plane420UNorm3Pack16,
    G12X4B12X4R12X42Plane420UNorm3Pack16,
    G12X4B12X4R12X43Plane422UNorm3Pack16,
    G12X4B12X4R12X42Plane422UNorm3Pack16,
    G12X4B12X4R12X43Plane444UNorm3Pack16,
    G16B16G16R16422UNorm,
    B16G16R16G16422UNorm,
    G16B16R163Plane420UNorm,
    G16B16R162Plane420UNorm,
    G16B16R163Plane422UNorm,
    G16B16R162Plane422UNorm,
    G16B16R163Plane444UNorm,
    G8B8R82Plane444UNorm,
    G10X6B10X6R10X62Plane444UNorm3Pack16,
    G12X4B12X4R12X42Plane444UNorm3Pack16,
    G16B16R162Plane444UNorm,
    A4R4G4B4UNormPack16,
    A4B4G4R4UNormPack16,
    ASTC4x4SFloatBlock,
    ASTC5x4SFloatBlock,
    ASTC5x5SFloatBlock,
    ASTC6x5SFloatBlock,
    ASTC6x6SFloatBlock,
    ASTC8x5SFloatBlock,
    ASTC8x6SFloatBlock,
    ASTC8x8SFloatBlock,
    ASTC10x5SFloatBlock,
    ASTC10x6SFloatBlock,
    ASTC10x8SFloatBlock,
    ASTC10x10SFloatBlock,
    ASTC12x10SFloatBlock,
    ASTC12x12SFloatBlock,
    PVRTC12BPPUNormBlockImg,
    PVRTC14BPPUNormBlockImg,
    PVRTC22BPPUNormBlockImg,
    PVRTC24BPPUNormBlockImg,
    PVRTC12BPPSrgbBlockImg,
    PVRTC14BPPSrgbBlockImg,
    PVRTC22BPPSrgbBlockImg,
    PVRTC24BPPSrgbBlockImg,
}

#[doc(hidden)]
impl From<ash::vk::Format> for Format {
    fn from(format: ash::vk::Format) -> Self {
        match format {
            ash::vk::Format::UNDEFINED => Format::Undefined,
            ash::vk::Format::R4G4_UNORM_PACK8 => Format::R4G4UNormPack8,
            ash::vk::Format::R4G4B4A4_UNORM_PACK16 => Format::R4G4B4A4UNormPack16,
            ash::vk::Format::B4G4R4A4_UNORM_PACK16 => Format::B4G4R4A4UNormPack16,
            ash::vk::Format::R5G6B5_UNORM_PACK16 => Format::R5G6B5UNormPack16,
            ash::vk::Format::B5G6R5_UNORM_PACK16 => Format::B5G6R5UNormPack16,
            ash::vk::Format::R5G5B5A1_UNORM_PACK16 => Format::R5G5B5A1UNormPack16,
            ash::vk::Format::B5G5R5A1_UNORM_PACK16 => Format::B5G5R5A1UNormPack16,
            ash::vk::Format::A1R5G5B5_UNORM_PACK16 => Format::A1R5G5B5UNormPack16,
            ash::vk::Format::R8_UNORM => Format::R8UNorm,
            ash::vk::Format::R8_SNORM => Format::R8SNorm,
            ash::vk::Format::R8_USCALED => Format::R8UScaled,
            ash::vk::Format::R8_SSCALED => Format::R8SScaled,
            ash::vk::Format::R8_UINT => Format::R8UInt,
            ash::vk::Format::R8_SINT => Format::R8SInt,
            ash::vk::Format::R8_SRGB => Format::R8Srgb,
            ash::vk::Format::R8G8_UNORM => Format::R8G8UNorm,
            ash::vk::Format::R8G8_SNORM => Format::R8G8SNorm,
            ash::vk::Format::R8G8_USCALED => Format::R8G8UScaled,
            ash::vk::Format::R8G8_SSCALED => Format::R8G8SScaled,
            ash::vk::Format::R8G8_UINT => Format::R8G8UInt,
            ash::vk::Format::R8G8_SINT => Format::R8G8SInt,
            ash::vk::Format::R8G8_SRGB => Format::R8G8Srgb,
            ash::vk::Format::R8G8B8_UNORM => Format::R8G8B8UNorm,
            ash::vk::Format::R8G8B8_SNORM => Format::R8G8B8SNorm,
            ash::vk::Format::R8G8B8_USCALED => Format::R8G8B8UScaled,
            ash::vk::Format::R8G8B8_SSCALED => Format::R8G8B8SScaled,
            ash::vk::Format::R8G8B8_UINT => Format::R8G8B8UInt,
            ash::vk::Format::R8G8B8_SINT => Format::R8G8B8SInt,
            ash::vk::Format::R8G8B8_SRGB => Format::R8G8B8Srgb,
            ash::vk::Format::B8G8R8_UNORM => Format::B8G8R8UNorm,
            ash::vk::Format::B8G8R8_SNORM => Format::B8G8R8SNorm,
            ash::vk::Format::B8G8R8_USCALED => Format::B8G8R8UScaled,
            ash::vk::Format::B8G8R8_SSCALED => Format::B8G8R8SScaled,
            ash::vk::Format::B8G8R8_UINT => Format::B8G8R8UInt,
            ash::vk::Format::B8G8R8_SINT => Format::B8G8R8SInt,
            ash::vk::Format::B8G8R8_SRGB => Format::B8G8R8Srgb,
            ash::vk::Format::R8G8B8A8_UNORM => Format::R8G8B8A8UNorm,
            ash::vk::Format::R8G8B8A8_SNORM => Format::R8G8B8A8SNorm,
            ash::vk::Format::R8G8B8A8_USCALED => Format::R8G8B8A8UScaled,
            ash::vk::Format::R8G8B8A8_SSCALED => Format::R8G8B8A8SScaled,
            ash::vk::Format::R8G8B8A8_UINT => Format::R8G8B8A8UInt,
            ash::vk::Format::R8G8B8A8_SINT => Format::R8G8B8A8SInt,
            ash::vk::Format::R8G8B8A8_SRGB => Format::R8G8B8A8Srgb,
            ash::vk::Format::B8G8R8A8_UNORM => Format::B8G8R8A8UNorm,
            ash::vk::Format::B8G8R8A8_SNORM => Format::B8G8R8A8SNorm,
            ash::vk::Format::B8G8R8A8_USCALED => Format::B8G8R8A8UScaled,
            ash::vk::Format::B8G8R8A8_SSCALED => Format::B8G8R8A8SScaled,
            ash::vk::Format::B8G8R8A8_UINT => Format::B8G8R8A8UInt,
            ash::vk::Format::B8G8R8A8_SINT => Format::B8G8R8A8SInt,
            ash::vk::Format::B8G8R8A8_SRGB => Format::B8G8R8A8Srgb,
            ash::vk::Format::A8B8G8R8_UNORM_PACK32 => Format::A8B8G8R8UNormPack32,
            ash::vk::Format::A8B8G8R8_SNORM_PACK32 => Format::A8B8G8R8SNormPack32,
            ash::vk::Format::A8B8G8R8_USCALED_PACK32 => Format::A8B8G8R8UScaledPack32,
            ash::vk::Format::A8B8G8R8_SSCALED_PACK32 => Format::A8B8G8R8SScaledPack32,
            ash::vk::Format::A8B8G8R8_UINT_PACK32 => Format::A8B8G8R8UIntPack32,
            ash::vk::Format::A8B8G8R8_SINT_PACK32 => Format::A8B8G8R8SIntPack32,
            ash::vk::Format::A8B8G8R8_SRGB_PACK32 => Format::A8B8G8R8SrgbPack32,
            ash::vk::Format::A2R10G10B10_UNORM_PACK32 => Format::A2R10G10B10UNormPack32,
            ash::vk::Format::A2R10G10B10_SNORM_PACK32 => Format::A2R10G10B10SNormPack32,
            ash::vk::Format::A2R10G10B10_USCALED_PACK32 => Format::A2R10G10B10UScaledPack32,
            ash::vk::Format::A2R10G10B10_SSCALED_PACK32 => Format::A2R10G10B10SScaledPack32,
            ash::vk::Format::A2R10G10B10_UINT_PACK32 => Format::A2R10G10B10UIntPack32,
            ash::vk::Format::A2R10G10B10_SINT_PACK32 => Format::A2R10G10B10SIntPack32,
            ash::vk::Format::A2B10G10R10_UNORM_PACK32 => Format::A2B10G10R10UNormPack32,
            ash::vk::Format::A2B10G10R10_SNORM_PACK32 => Format::A2B10G10R10SNormPack32,
            ash::vk::Format::A2B10G10R10_USCALED_PACK32 => Format::A2B10G10R10UScaledPack32,
            ash::vk::Format::A2B10G10R10_SSCALED_PACK32 => Format::A2B10G10R10SScaledPack32,
            ash::vk::Format::A2B10G10R10_UINT_PACK32 => Format::A2B10G10R10UIntPack32,
            ash::vk::Format::A2B10G10R10_SINT_PACK32 => Format::A2B10G10R10SIntPack32,
            ash::vk::Format::R16_UNORM => Format::R16UNorm,
            ash::vk::Format::R16_SNORM => Format::R16SNorm,
            ash::vk::Format::R16_USCALED => Format::R16UScaled,
            ash::vk::Format::R16_SSCALED => Format::R16SScaled,
            ash::vk::Format::R16_UINT => Format::R16UInt,
            ash::vk::Format::R16_SINT => Format::R16SInt,
            ash::vk::Format::R16_SFLOAT => Format::R16SFloat,
            ash::vk::Format::R16G16_UNORM => Format::R16G16UNorm,
            ash::vk::Format::R16G16_SNORM => Format::R16G16SNorm,
            ash::vk::Format::R16G16_USCALED => Format::R16G16UScaled,
            ash::vk::Format::R16G16_SSCALED => Format::R16G16SScaled,
            ash::vk::Format::R16G16_UINT => Format::R16G16UInt,
            ash::vk::Format::R16G16_SINT => Format::R16G16SInt,
            ash::vk::Format::R16G16_SFLOAT => Format::R16G16SFloat,
            ash::vk::Format::R16G16B16_UNORM => Format::R16G16B16UNorm,
            ash::vk::Format::R16G16B16_SNORM => Format::R16G16B16SNorm,
            ash::vk::Format::R16G16B16_USCALED => Format::R16G16B16UScaled,
            ash::vk::Format::R16G16B16_SSCALED => Format::R16G16B16SScaled,
            ash::vk::Format::R16G16B16_UINT => Format::R16G16B16UInt,
            ash::vk::Format::R16G16B16_SINT => Format::R16G16B16SInt,
            ash::vk::Format::R16G16B16_SFLOAT => Format::R16G16B16SFloat,
            ash::vk::Format::R16G16B16A16_UNORM => Format::R16G16B16A16UNorm,
            ash::vk::Format::R16G16B16A16_SNORM => Format::R16G16B16A16SNorm,
            ash::vk::Format::R16G16B16A16_USCALED => Format::R16G16B16A16UScaled,
            ash::vk::Format::R16G16B16A16_SSCALED => Format::R16G16B16A16SScaled,
            ash::vk::Format::R16G16B16A16_UINT => Format::R16G16B16A16UInt,
            ash::vk::Format::R16G16B16A16_SINT => Format::R16G16B16A16SInt,
            ash::vk::Format::R16G16B16A16_SFLOAT => Format::R16G16B16A16SFloat,
            ash::vk::Format::R32_UINT => Format::R32UInt,
            ash::vk::Format::R32_SINT => Format::R32SInt,
            ash::vk::Format::R32_SFLOAT => Format::R32SFloat,
            ash::vk::Format::R32G32_UINT => Format::R32G32UInt,
            ash::vk::Format::R32G32_SINT => Format::R32G32SInt,
            ash::vk::Format::R32G32_SFLOAT => Format::R32G32SFloat,
            ash::vk::Format::R32G32B32_UINT => Format::R32G32B32UInt,
            ash::vk::Format::R32G32B32_SINT => Format::R32G32B32SInt,
            ash::vk::Format::R32G32B32_SFLOAT => Format::R32G32B32SFloat,
            ash::vk::Format::R32G32B32A32_UINT => Format::R32G32B32A32UInt,
            ash::vk::Format::R32G32B32A32_SINT => Format::R32G32B32A32SInt,
            ash::vk::Format::R32G32B32A32_SFLOAT => Format::R32G32B32A32SFloat,
            ash::vk::Format::R64_UINT => Format::R64UInt,
            ash::vk::Format::R64_SINT => Format::R64SInt,
            ash::vk::Format::R64_SFLOAT => Format::R64SFloat,
            ash::vk::Format::R64G64_UINT => Format::R64G64UInt,
            ash::vk::Format::R64G64_SINT => Format::R64B64SInt,
            ash::vk::Format::R64G64_SFLOAT => Format::R64B64SFloat,
            ash::vk::Format::R64G64B64_UINT => Format::R64G64B64UInt,
            ash::vk::Format::R64G64B64_SINT => Format::R64G64B64SInt,
            ash::vk::Format::R64G64B64_SFLOAT => Format::R64G64B64SFloat,
            ash::vk::Format::R64G64B64A64_UINT => Format::R64G64B64A64UInt,
            ash::vk::Format::R64G64B64A64_SINT => Format::R64G64B64A64SInt,
            ash::vk::Format::R64G64B64A64_SFLOAT => Format::R64G64B64A64SFloat,
            ash::vk::Format::B10G11R11_UFLOAT_PACK32 => Format::B10G11R11UFloatPack32,
            ash::vk::Format::E5B9G9R9_UFLOAT_PACK32 => Format::E5B9G9R9UFloatPack32,
            ash::vk::Format::D16_UNORM => Format::D16UNorm,
            ash::vk::Format::X8_D24_UNORM_PACK32 => Format::X8D24UNormPack32,
            ash::vk::Format::D32_SFLOAT => Format::D32SFloat,
            ash::vk::Format::S8_UINT => Format::S8UInt,
            ash::vk::Format::D16_UNORM_S8_UINT => Format::D16UNormS8UInt,
            ash::vk::Format::D24_UNORM_S8_UINT => Format::D24UNormS8UInt,
            ash::vk::Format::D32_SFLOAT_S8_UINT => Format::D32SFloatS8UInt,
            ash::vk::Format::BC1_RGB_UNORM_BLOCK => Format::BC1RgbUNormBlock,
            ash::vk::Format::BC1_RGB_SRGB_BLOCK => Format::BC1RgbSrgbBlock,
            ash::vk::Format::BC1_RGBA_UNORM_BLOCK => Format::BC1RgbaUNormBlock,
            ash::vk::Format::BC1_RGBA_SRGB_BLOCK => Format::BC1RgbaSrgbBlock,
            ash::vk::Format::BC2_UNORM_BLOCK => Format::BC2UNormBlock,
            ash::vk::Format::BC2_SRGB_BLOCK => Format::BC2SrgbBlock,
            ash::vk::Format::BC3_UNORM_BLOCK => Format::BC3UNormBlock,
            ash::vk::Format::BC3_SRGB_BLOCK => Format::BC3SrgbBlock,
            ash::vk::Format::BC4_UNORM_BLOCK => Format::BC4UNormBlock,
            ash::vk::Format::BC4_SNORM_BLOCK => Format::BC4SNormBlock,
            ash::vk::Format::BC5_UNORM_BLOCK => Format::BC5UNormBlock,
            ash::vk::Format::BC5_SNORM_BLOCK => Format::BC5SNormBlock,
            ash::vk::Format::BC6H_UFLOAT_BLOCK => Format::BC6HUFloatBlock,
            ash::vk::Format::BC6H_SFLOAT_BLOCK => Format::BC6HSFloatBlock,
            ash::vk::Format::BC7_UNORM_BLOCK => Format::BC7UNormBlock,
            ash::vk::Format::BC7_SRGB_BLOCK => Format::BC7SrgbBlock,
            ash::vk::Format::ETC2_R8G8B8_UNORM_BLOCK => Format::ETC2R8G8B8UNormBlock,
            ash::vk::Format::ETC2_R8G8B8_SRGB_BLOCK => Format::ETC2R8G8B8SrgbBlock,
            ash::vk::Format::ETC2_R8G8B8A1_UNORM_BLOCK => Format::ETC2R8G8B8A1UNormBlock,
            ash::vk::Format::ETC2_R8G8B8A1_SRGB_BLOCK => Format::ETC2R8G8B8A1SrgbBlock,
            ash::vk::Format::ETC2_R8G8B8A8_UNORM_BLOCK => Format::ETC2R8G8B8A8UNormBlock,
            ash::vk::Format::ETC2_R8G8B8A8_SRGB_BLOCK => Format::ETC2R8G8B8A8SrgbBlock,
            ash::vk::Format::EAC_R11_UNORM_BLOCK => Format::EACR11UNormBlock,
            ash::vk::Format::EAC_R11_SNORM_BLOCK => Format::EACR11SNormBlock,
            ash::vk::Format::EAC_R11G11_UNORM_BLOCK => Format::EACR11G11UNormBlock,
            ash::vk::Format::EAC_R11G11_SNORM_BLOCK => Format::EACR11G11SNormBlock,
            ash::vk::Format::ASTC_4X4_UNORM_BLOCK => Format::ASTC4x4UNormBlock,
            ash::vk::Format::ASTC_4X4_SRGB_BLOCK => Format::ASTC4x4SrgbBlock,
            ash::vk::Format::ASTC_5X4_UNORM_BLOCK => Format::ASTC5x4UNormBlock,
            ash::vk::Format::ASTC_5X4_SRGB_BLOCK => Format::ASTC5x4SrgbBlock,
            ash::vk::Format::ASTC_5X5_UNORM_BLOCK => Format::ASTC5x5UNormBlock,
            ash::vk::Format::ASTC_5X5_SRGB_BLOCK => Format::ASTC5x5SrgbBlock,
            ash::vk::Format::ASTC_6X5_UNORM_BLOCK => Format::ASTC6x5UNormBlock,
            ash::vk::Format::ASTC_6X5_SRGB_BLOCK => Format::ASTC6x5SrgbBlock,
            ash::vk::Format::ASTC_6X6_UNORM_BLOCK => Format::ASTC6x6UNormBlock,
            ash::vk::Format::ASTC_6X6_SRGB_BLOCK => Format::ASTC6x6SrgbBlock,
            ash::vk::Format::ASTC_8X5_UNORM_BLOCK => Format::ASTC8x5UNormBlock,
            ash::vk::Format::ASTC_8X5_SRGB_BLOCK => Format::ASTC8x5SrgbBlock,
            ash::vk::Format::ASTC_8X6_UNORM_BLOCK => Format::ASTC8x6UNormBlock,
            ash::vk::Format::ASTC_8X6_SRGB_BLOCK => Format::ASTC8x6SrgbBlock,
            ash::vk::Format::ASTC_8X8_UNORM_BLOCK => Format::ASTC8x8UNormBlock,
            ash::vk::Format::ASTC_8X8_SRGB_BLOCK => Format::ASTC8x8SrgbBlock,
            ash::vk::Format::ASTC_10X5_UNORM_BLOCK => Format::ASTC10x5UNormBlock,
            ash::vk::Format::ASTC_10X5_SRGB_BLOCK => Format::ASTC10x5SrgbBlock,
            ash::vk::Format::ASTC_10X6_UNORM_BLOCK => Format::ASTC10x6UNormBlock,
            ash::vk::Format::ASTC_10X6_SRGB_BLOCK => Format::ASTC10x6SrgbBlock,
            ash::vk::Format::ASTC_10X8_UNORM_BLOCK => Format::ASTC10x8UNormBlock,
            ash::vk::Format::ASTC_10X8_SRGB_BLOCK => Format::ASTC10x8SrgbBlock,
            ash::vk::Format::ASTC_10X10_UNORM_BLOCK => Format::ASTC10x10UNormBlock,
            ash::vk::Format::ASTC_10X10_SRGB_BLOCK => Format::ASTC10x10SrgbBlock,
            ash::vk::Format::ASTC_12X10_UNORM_BLOCK => Format::ASTC12x10UNormBlock,
            ash::vk::Format::ASTC_12X10_SRGB_BLOCK => Format::ASTC12x10SrgbBlock,
            ash::vk::Format::ASTC_12X12_UNORM_BLOCK => Format::ASTC12x12UNormBlock,
            ash::vk::Format::ASTC_12X12_SRGB_BLOCK => Format::ASTC12x12SrgbBlock,
            ash::vk::Format::G8B8G8R8_422_UNORM => Format::G8B8G8R8422UNorm,
            ash::vk::Format::B8G8R8G8_422_UNORM => Format::B8G8R8G8422UNorm,
            ash::vk::Format::G8_B8_R8_3PLANE_420_UNORM => Format::G8B8R83Plane420UNorm,
            ash::vk::Format::G8_B8R8_2PLANE_420_UNORM => Format::G8B8R82Plane420UNorm,
            ash::vk::Format::G8_B8_R8_3PLANE_422_UNORM => Format::G8B8R83Plane422UNorm,
            ash::vk::Format::G8_B8R8_2PLANE_422_UNORM => Format::G8B8R82Plane422UNorm,
            ash::vk::Format::G8_B8_R8_3PLANE_444_UNORM => Format::G8B8R83Plane444UNorm,
            ash::vk::Format::R10X6_UNORM_PACK16 => Format::R10X6UNormPack16,
            ash::vk::Format::R10X6G10X6_UNORM_2PACK16 => Format::R10X6G10X6UNorm2Pack16,
            ash::vk::Format::R10X6G10X6B10X6A10X6_UNORM_4PACK16 => {
                Format::R10X6G10X6B10X6A10X6UNorm4Pack16
            }
            ash::vk::Format::G10X6B10X6G10X6R10X6_422_UNORM_4PACK16 => {
                Format::G10X6B10X6G10X6R10X6422UNorm4Pack16
            }
            ash::vk::Format::B10X6G10X6R10X6G10X6_422_UNORM_4PACK16 => {
                Format::B10X6G10X6R10X6G10X6422UNorm4Pack16
            }
            ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16 => {
                Format::G10X6B10X6R10X63Plane420UNorm3Pack16
            }
            ash::vk::Format::G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16 => {
                Format::G10X6B10X6R10X62Plane420UNorm3Pack16
            }
            ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16 => {
                Format::G10X6B10X6R10X63Plane422UNorm3Pack16
            }
            ash::vk::Format::G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16 => {
                Format::G10X6B10X6R10X62Plane422UNorm3Pack16
            }
            ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16 => {
                Format::G10X6B10X6R10X63Plane444UNorm3Pack16
            }
            ash::vk::Format::R12X4_UNORM_PACK16 => Format::R12X4UNormPack16,
            ash::vk::Format::R12X4G12X4_UNORM_2PACK16 => Format::R12X4G12X4UNorm2Pack16,
            ash::vk::Format::R12X4G12X4B12X4A12X4_UNORM_4PACK16 => {
                Format::R12X4G12X4B12X4A12X4UNorm4Pack16
            }
            ash::vk::Format::G12X4B12X4G12X4R12X4_422_UNORM_4PACK16 => {
                Format::G12X4B12X4G12X4R12X4422UNorm4Pack16
            }
            ash::vk::Format::B12X4G12X4R12X4G12X4_422_UNORM_4PACK16 => {
                Format::B12X4G12X4R12X4G12X4422UNorm4Pack16
            }
            ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16 => {
                Format::G12X4B12X4R12X43Plane420UNorm3Pack16
            }
            ash::vk::Format::G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16 => {
                Format::G12X4B12X4R12X42Plane420UNorm3Pack16
            }
            ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16 => {
                Format::G12X4B12X4R12X43Plane422UNorm3Pack16
            }
            ash::vk::Format::G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16 => {
                Format::G12X4B12X4R12X42Plane422UNorm3Pack16
            }
            ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16 => {
                Format::G12X4B12X4R12X43Plane444UNorm3Pack16
            }
            ash::vk::Format::G16B16G16R16_422_UNORM => Format::G16B16G16R16422UNorm,
            ash::vk::Format::B16G16R16G16_422_UNORM => Format::B16G16R16G16422UNorm,
            ash::vk::Format::G16_B16_R16_3PLANE_420_UNORM => Format::G16B16R163Plane420UNorm,
            ash::vk::Format::G16_B16R16_2PLANE_420_UNORM => Format::G16B16R162Plane420UNorm,
            ash::vk::Format::G16_B16_R16_3PLANE_422_UNORM => Format::G16B16R163Plane422UNorm,
            ash::vk::Format::G16_B16R16_2PLANE_422_UNORM => Format::G16B16R162Plane422UNorm,
            ash::vk::Format::G16_B16_R16_3PLANE_444_UNORM => Format::G16B16R163Plane444UNorm,
            ash::vk::Format::G8_B8R8_2PLANE_444_UNORM => Format::G8B8R82Plane444UNorm,
            ash::vk::Format::G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16 => {
                Format::G10X6B10X6R10X62Plane444UNorm3Pack16
            }
            ash::vk::Format::G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16 => {
                Format::G12X4B12X4R12X42Plane444UNorm3Pack16
            }
            ash::vk::Format::G16_B16R16_2PLANE_444_UNORM => Format::G16B16R162Plane444UNorm,
            ash::vk::Format::A4R4G4B4_UNORM_PACK16 => Format::A4R4G4B4UNormPack16,
            ash::vk::Format::A4B4G4R4_UNORM_PACK16 => Format::A4B4G4R4UNormPack16,
            ash::vk::Format::ASTC_4X4_SFLOAT_BLOCK => Format::ASTC4x4SFloatBlock,
            ash::vk::Format::ASTC_5X4_SFLOAT_BLOCK => Format::ASTC5x4SFloatBlock,
            ash::vk::Format::ASTC_5X5_SFLOAT_BLOCK => Format::ASTC5x5SFloatBlock,
            ash::vk::Format::ASTC_6X5_SFLOAT_BLOCK => Format::ASTC6x5SFloatBlock,
            ash::vk::Format::ASTC_6X6_SFLOAT_BLOCK => Format::ASTC6x6SFloatBlock,
            ash::vk::Format::ASTC_8X5_SFLOAT_BLOCK => Format::ASTC8x5SFloatBlock,
            ash::vk::Format::ASTC_8X6_SFLOAT_BLOCK => Format::ASTC8x6SFloatBlock,
            ash::vk::Format::ASTC_8X8_SFLOAT_BLOCK => Format::ASTC8x8SFloatBlock,
            ash::vk::Format::ASTC_10X5_SFLOAT_BLOCK => Format::ASTC10x5SFloatBlock,
            ash::vk::Format::ASTC_10X6_SFLOAT_BLOCK => Format::ASTC10x6SFloatBlock,
            ash::vk::Format::ASTC_10X8_SFLOAT_BLOCK => Format::ASTC10x8SFloatBlock,
            ash::vk::Format::ASTC_10X10_SFLOAT_BLOCK => Format::ASTC10x10SFloatBlock,
            ash::vk::Format::ASTC_12X10_SFLOAT_BLOCK => Format::ASTC12x10SFloatBlock,
            ash::vk::Format::ASTC_12X12_SFLOAT_BLOCK => Format::ASTC12x12SFloatBlock,
            ash::vk::Format::PVRTC1_2BPP_UNORM_BLOCK_IMG => Format::PVRTC12BPPUNormBlockImg,
            ash::vk::Format::PVRTC1_4BPP_UNORM_BLOCK_IMG => Format::PVRTC14BPPUNormBlockImg,
            ash::vk::Format::PVRTC2_2BPP_UNORM_BLOCK_IMG => Format::PVRTC22BPPUNormBlockImg,
            ash::vk::Format::PVRTC2_4BPP_UNORM_BLOCK_IMG => Format::PVRTC24BPPUNormBlockImg,
            ash::vk::Format::PVRTC1_2BPP_SRGB_BLOCK_IMG => Format::PVRTC12BPPSrgbBlockImg,
            ash::vk::Format::PVRTC1_4BPP_SRGB_BLOCK_IMG => Format::PVRTC14BPPSrgbBlockImg,
            ash::vk::Format::PVRTC2_2BPP_SRGB_BLOCK_IMG => Format::PVRTC22BPPSrgbBlockImg,
            ash::vk::Format::PVRTC2_4BPP_SRGB_BLOCK_IMG => Format::PVRTC24BPPSrgbBlockImg,
            _ => unreachable!("Unrecognized 'Format'"),
        }
    }
}

#[doc(hidden)]
impl From<Format> for ash::vk::Format {
    fn from(format: Format) -> Self {
        match format {
            Format::Undefined => ash::vk::Format::UNDEFINED,
            Format::R4G4UNormPack8 => ash::vk::Format::R4G4_UNORM_PACK8,
            Format::R4G4B4A4UNormPack16 => ash::vk::Format::R4G4B4A4_UNORM_PACK16,
            Format::B4G4R4A4UNormPack16 => ash::vk::Format::B4G4R4A4_UNORM_PACK16,
            Format::R5G6B5UNormPack16 => ash::vk::Format::R5G6B5_UNORM_PACK16,
            Format::B5G6R5UNormPack16 => ash::vk::Format::B5G6R5_UNORM_PACK16,
            Format::R5G5B5A1UNormPack16 => ash::vk::Format::R5G5B5A1_UNORM_PACK16,
            Format::B5G5R5A1UNormPack16 => ash::vk::Format::B5G5R5A1_UNORM_PACK16,
            Format::A1R5G5B5UNormPack16 => ash::vk::Format::A1R5G5B5_UNORM_PACK16,
            Format::R8UNorm => ash::vk::Format::R8_UNORM,
            Format::R8SNorm => ash::vk::Format::R8_SNORM,
            Format::R8UScaled => ash::vk::Format::R8_USCALED,
            Format::R8SScaled => ash::vk::Format::R8_SSCALED,
            Format::R8UInt => ash::vk::Format::R8_UINT,
            Format::R8SInt => ash::vk::Format::R8_SINT,
            Format::R8Srgb => ash::vk::Format::R8_SRGB,
            Format::R8G8UNorm => ash::vk::Format::R8G8_UNORM,
            Format::R8G8SNorm => ash::vk::Format::R8G8_SNORM,
            Format::R8G8UScaled => ash::vk::Format::R8G8_USCALED,
            Format::R8G8SScaled => ash::vk::Format::R8G8_SSCALED,
            Format::R8G8UInt => ash::vk::Format::R8G8_UINT,
            Format::R8G8SInt => ash::vk::Format::R8G8_SINT,
            Format::R8G8Srgb => ash::vk::Format::R8G8_SRGB,
            Format::R8G8B8UNorm => ash::vk::Format::R8G8B8_UNORM,
            Format::R8G8B8SNorm => ash::vk::Format::R8G8B8_SNORM,
            Format::R8G8B8UScaled => ash::vk::Format::R8G8B8_USCALED,
            Format::R8G8B8SScaled => ash::vk::Format::R8G8B8_SSCALED,
            Format::R8G8B8UInt => ash::vk::Format::R8G8B8_UINT,
            Format::R8G8B8SInt => ash::vk::Format::R8G8B8_SINT,
            Format::R8G8B8Srgb => ash::vk::Format::R8G8B8_SRGB,
            Format::B8G8R8UNorm => ash::vk::Format::B8G8R8_UNORM,
            Format::B8G8R8SNorm => ash::vk::Format::B8G8R8_SNORM,
            Format::B8G8R8UScaled => ash::vk::Format::B8G8R8_USCALED,
            Format::B8G8R8SScaled => ash::vk::Format::B8G8R8_SSCALED,
            Format::B8G8R8UInt => ash::vk::Format::B8G8R8_UINT,
            Format::B8G8R8SInt => ash::vk::Format::B8G8R8_SINT,
            Format::B8G8R8Srgb => ash::vk::Format::B8G8R8_SRGB,
            Format::R8G8B8A8UNorm => ash::vk::Format::R8G8B8A8_UNORM,
            Format::R8G8B8A8SNorm => ash::vk::Format::R8G8B8A8_SNORM,
            Format::R8G8B8A8UScaled => ash::vk::Format::R8G8B8A8_USCALED,
            Format::R8G8B8A8SScaled => ash::vk::Format::R8G8B8A8_SSCALED,
            Format::R8G8B8A8UInt => ash::vk::Format::R8G8B8A8_UINT,
            Format::R8G8B8A8SInt => ash::vk::Format::R8G8B8A8_SINT,
            Format::R8G8B8A8Srgb => ash::vk::Format::R8G8B8A8_SRGB,
            Format::B8G8R8A8UNorm => ash::vk::Format::B8G8R8A8_UNORM,
            Format::B8G8R8A8SNorm => ash::vk::Format::B8G8R8A8_SNORM,
            Format::B8G8R8A8UScaled => ash::vk::Format::B8G8R8A8_USCALED,
            Format::B8G8R8A8SScaled => ash::vk::Format::B8G8R8A8_SSCALED,
            Format::B8G8R8A8UInt => ash::vk::Format::B8G8R8A8_UINT,
            Format::B8G8R8A8SInt => ash::vk::Format::B8G8R8A8_SINT,
            Format::B8G8R8A8Srgb => ash::vk::Format::B8G8R8A8_SRGB,
            Format::A8B8G8R8UNormPack32 => ash::vk::Format::A8B8G8R8_UNORM_PACK32,
            Format::A8B8G8R8SNormPack32 => ash::vk::Format::A8B8G8R8_SNORM_PACK32,
            Format::A8B8G8R8UScaledPack32 => ash::vk::Format::A8B8G8R8_USCALED_PACK32,
            Format::A8B8G8R8SScaledPack32 => ash::vk::Format::A8B8G8R8_SSCALED_PACK32,
            Format::A8B8G8R8UIntPack32 => ash::vk::Format::A8B8G8R8_UINT_PACK32,
            Format::A8B8G8R8SIntPack32 => ash::vk::Format::A8B8G8R8_SINT_PACK32,
            Format::A8B8G8R8SrgbPack32 => ash::vk::Format::A8B8G8R8_SRGB_PACK32,
            Format::A2R10G10B10UNormPack32 => ash::vk::Format::A2R10G10B10_UNORM_PACK32,
            Format::A2R10G10B10SNormPack32 => ash::vk::Format::A2R10G10B10_SNORM_PACK32,
            Format::A2R10G10B10UScaledPack32 => ash::vk::Format::A2R10G10B10_USCALED_PACK32,
            Format::A2R10G10B10SScaledPack32 => ash::vk::Format::A2R10G10B10_SSCALED_PACK32,
            Format::A2R10G10B10UIntPack32 => ash::vk::Format::A2R10G10B10_UINT_PACK32,
            Format::A2R10G10B10SIntPack32 => ash::vk::Format::A2R10G10B10_SINT_PACK32,
            Format::A2B10G10R10UNormPack32 => ash::vk::Format::A2B10G10R10_UNORM_PACK32,
            Format::A2B10G10R10SNormPack32 => ash::vk::Format::A2B10G10R10_SNORM_PACK32,
            Format::A2B10G10R10UScaledPack32 => ash::vk::Format::A2B10G10R10_SSCALED_PACK32,
            Format::A2B10G10R10SScaledPack32 => ash::vk::Format::A2B10G10R10_SSCALED_PACK32,
            Format::A2B10G10R10UIntPack32 => ash::vk::Format::A2B10G10R10_UINT_PACK32,
            Format::A2B10G10R10SIntPack32 => ash::vk::Format::A2B10G10R10_SINT_PACK32,
            Format::R16UNorm => ash::vk::Format::R16_UNORM,
            Format::R16SNorm => ash::vk::Format::R16_SNORM,
            Format::R16UScaled => ash::vk::Format::R16_USCALED,
            Format::R16SScaled => ash::vk::Format::R16_SSCALED,
            Format::R16UInt => ash::vk::Format::R16_UINT,
            Format::R16SInt => ash::vk::Format::R16_SINT,
            Format::R16SFloat => ash::vk::Format::R16_SFLOAT,
            Format::R16G16UNorm => ash::vk::Format::R16G16_UNORM,
            Format::R16G16SNorm => ash::vk::Format::R16G16_SNORM,
            Format::R16G16UScaled => ash::vk::Format::R16G16_USCALED,
            Format::R16G16SScaled => ash::vk::Format::R16G16_SSCALED,
            Format::R16G16UInt => ash::vk::Format::R16G16_UINT,
            Format::R16G16SInt => ash::vk::Format::R16G16_SINT,
            Format::R16G16SFloat => ash::vk::Format::R16G16B16_UNORM,
            Format::R16G16B16UNorm => ash::vk::Format::R16G16B16_UNORM,
            Format::R16G16B16SNorm => ash::vk::Format::R16G16B16_SNORM,
            Format::R16G16B16UScaled => ash::vk::Format::R16G16B16_SSCALED,
            Format::R16G16B16SScaled => ash::vk::Format::R16G16B16_SSCALED,
            Format::R16G16B16UInt => ash::vk::Format::R16G16B16_UINT,
            Format::R16G16B16SInt => ash::vk::Format::R16G16B16_SFLOAT,
            Format::R16G16B16SFloat => ash::vk::Format::R16G16B16_SFLOAT,
            Format::R16G16B16A16UNorm => ash::vk::Format::R16G16B16A16_UNORM,
            Format::R16G16B16A16SNorm => ash::vk::Format::R16G16B16A16_SNORM,
            Format::R16G16B16A16UScaled => ash::vk::Format::R16G16B16A16_USCALED,
            Format::R16G16B16A16SScaled => ash::vk::Format::R16G16B16A16_SSCALED,
            Format::R16G16B16A16UInt => ash::vk::Format::R16G16B16A16_UINT,
            Format::R16G16B16A16SInt => ash::vk::Format::R16G16B16A16_SINT,
            Format::R16G16B16A16SFloat => ash::vk::Format::R16G16B16A16_SFLOAT,
            Format::R32UInt => ash::vk::Format::R32_UINT,
            Format::R32SInt => ash::vk::Format::R32_SINT,
            Format::R32SFloat => ash::vk::Format::R32_SFLOAT,
            Format::R32G32UInt => ash::vk::Format::R32G32_UINT,
            Format::R32G32SInt => ash::vk::Format::R32G32_SINT,
            Format::R32G32SFloat => ash::vk::Format::R32G32_SFLOAT,
            Format::R32G32B32UInt => ash::vk::Format::R32G32B32_UINT,
            Format::R32G32B32SInt => ash::vk::Format::R32G32B32_SINT,
            Format::R32G32B32SFloat => ash::vk::Format::R32G32B32_SFLOAT,
            Format::R32G32B32A32UInt => ash::vk::Format::R32G32B32A32_UINT,
            Format::R32G32B32A32SInt => ash::vk::Format::R32G32B32A32_SINT,
            Format::R32G32B32A32SFloat => ash::vk::Format::R32G32B32A32_SFLOAT,
            Format::R64UInt => ash::vk::Format::R64_UINT,
            Format::R64SInt => ash::vk::Format::R64_SINT,
            Format::R64SFloat => ash::vk::Format::R64_SFLOAT,
            Format::R64G64UInt => ash::vk::Format::R64G64_UINT,
            Format::R64B64SInt => ash::vk::Format::R64G64_SINT,
            Format::R64B64SFloat => ash::vk::Format::R64G64_SFLOAT,
            Format::R64G64B64UInt => ash::vk::Format::R64G64B64_UINT,
            Format::R64G64B64SInt => ash::vk::Format::R64G64B64_SINT,
            Format::R64G64B64SFloat => ash::vk::Format::R64G64B64_SFLOAT,
            Format::R64G64B64A64UInt => ash::vk::Format::R64G64B64A64_UINT,
            Format::R64G64B64A64SInt => ash::vk::Format::R64G64B64A64_SINT,
            Format::R64G64B64A64SFloat => ash::vk::Format::R64G64B64A64_SFLOAT,
            Format::B10G11R11UFloatPack32 => ash::vk::Format::B10G11R11_UFLOAT_PACK32,
            Format::E5B9G9R9UFloatPack32 => ash::vk::Format::E5B9G9R9_UFLOAT_PACK32,
            Format::D16UNorm => ash::vk::Format::D16_UNORM,
            Format::X8D24UNormPack32 => ash::vk::Format::X8_D24_UNORM_PACK32,
            Format::D32SFloat => ash::vk::Format::D32_SFLOAT,
            Format::S8UInt => ash::vk::Format::S8_UINT,
            Format::D16UNormS8UInt => ash::vk::Format::D16_UNORM_S8_UINT,
            Format::D24UNormS8UInt => ash::vk::Format::D24_UNORM_S8_UINT,
            Format::D32SFloatS8UInt => ash::vk::Format::D32_SFLOAT_S8_UINT,
            Format::BC1RgbUNormBlock => ash::vk::Format::BC1_RGB_UNORM_BLOCK,
            Format::BC1RgbSrgbBlock => ash::vk::Format::BC1_RGB_SRGB_BLOCK,
            Format::BC1RgbaUNormBlock => ash::vk::Format::BC1_RGBA_UNORM_BLOCK,
            Format::BC1RgbaSrgbBlock => ash::vk::Format::BC1_RGBA_SRGB_BLOCK,
            Format::BC2UNormBlock => ash::vk::Format::BC2_UNORM_BLOCK,
            Format::BC2SrgbBlock => ash::vk::Format::BC2_SRGB_BLOCK,
            Format::BC3UNormBlock => ash::vk::Format::BC3_UNORM_BLOCK,
            Format::BC3SrgbBlock => ash::vk::Format::BC3_SRGB_BLOCK,
            Format::BC4UNormBlock => ash::vk::Format::BC4_UNORM_BLOCK,
            Format::BC4SNormBlock => ash::vk::Format::BC4_SNORM_BLOCK,
            Format::BC5UNormBlock => ash::vk::Format::BC5_UNORM_BLOCK,
            Format::BC5SNormBlock => ash::vk::Format::BC5_SNORM_BLOCK,
            Format::BC6HUFloatBlock => ash::vk::Format::BC6H_UFLOAT_BLOCK,
            Format::BC6HSFloatBlock => ash::vk::Format::BC6H_SFLOAT_BLOCK,
            Format::BC7UNormBlock => ash::vk::Format::BC7_UNORM_BLOCK,
            Format::BC7SrgbBlock => ash::vk::Format::BC7_SRGB_BLOCK,
            Format::ETC2R8G8B8UNormBlock => ash::vk::Format::ETC2_R8G8B8_UNORM_BLOCK,
            Format::ETC2R8G8B8SrgbBlock => ash::vk::Format::ETC2_R8G8B8_SRGB_BLOCK,
            Format::ETC2R8G8B8A1UNormBlock => ash::vk::Format::ETC2_R8G8B8A1_UNORM_BLOCK,
            Format::ETC2R8G8B8A1SrgbBlock => ash::vk::Format::ETC2_R8G8B8A1_SRGB_BLOCK,
            Format::ETC2R8G8B8A8UNormBlock => ash::vk::Format::ETC2_R8G8B8A8_UNORM_BLOCK,
            Format::ETC2R8G8B8A8SrgbBlock => ash::vk::Format::ETC2_R8G8B8A8_SRGB_BLOCK,
            Format::EACR11UNormBlock => ash::vk::Format::EAC_R11_UNORM_BLOCK,
            Format::EACR11SNormBlock => ash::vk::Format::EAC_R11_SNORM_BLOCK,
            Format::EACR11G11UNormBlock => ash::vk::Format::EAC_R11G11_UNORM_BLOCK,
            Format::EACR11G11SNormBlock => ash::vk::Format::EAC_R11G11_SNORM_BLOCK,
            Format::ASTC4x4UNormBlock => ash::vk::Format::ASTC_4X4_UNORM_BLOCK,
            Format::ASTC4x4SrgbBlock => ash::vk::Format::ASTC_4X4_SRGB_BLOCK,
            Format::ASTC5x4UNormBlock => ash::vk::Format::ASTC_4X4_UNORM_BLOCK,
            Format::ASTC5x4SrgbBlock => ash::vk::Format::ASTC_5X4_SRGB_BLOCK,
            Format::ASTC5x5UNormBlock => ash::vk::Format::ASTC_5X5_UNORM_BLOCK,
            Format::ASTC5x5SrgbBlock => ash::vk::Format::ASTC_5X5_SRGB_BLOCK,
            Format::ASTC6x5UNormBlock => ash::vk::Format::ASTC_6X5_UNORM_BLOCK,
            Format::ASTC6x5SrgbBlock => ash::vk::Format::ASTC_6X5_SRGB_BLOCK,
            Format::ASTC6x6UNormBlock => ash::vk::Format::ASTC_6X6_UNORM_BLOCK,
            Format::ASTC6x6SrgbBlock => ash::vk::Format::ASTC_6X6_UNORM_BLOCK,
            Format::ASTC8x5UNormBlock => ash::vk::Format::ASTC_8X5_UNORM_BLOCK,
            Format::ASTC8x5SrgbBlock => ash::vk::Format::ASTC_8X5_SRGB_BLOCK,
            Format::ASTC8x6UNormBlock => ash::vk::Format::ASTC_8X6_UNORM_BLOCK,
            Format::ASTC8x6SrgbBlock => ash::vk::Format::ASTC_8X6_SRGB_BLOCK,
            Format::ASTC8x8UNormBlock => ash::vk::Format::ASTC_8X8_UNORM_BLOCK,
            Format::ASTC8x8SrgbBlock => ash::vk::Format::ASTC_8X8_SRGB_BLOCK,
            Format::ASTC10x5UNormBlock => ash::vk::Format::ASTC_10X5_UNORM_BLOCK,
            Format::ASTC10x5SrgbBlock => ash::vk::Format::ASTC_10X5_SRGB_BLOCK,
            Format::ASTC10x6UNormBlock => ash::vk::Format::ASTC_10X6_UNORM_BLOCK,
            Format::ASTC10x6SrgbBlock => ash::vk::Format::ASTC_10X6_SRGB_BLOCK,
            Format::ASTC10x8UNormBlock => ash::vk::Format::ASTC_10X8_UNORM_BLOCK,
            Format::ASTC10x8SrgbBlock => ash::vk::Format::ASTC_10X8_SRGB_BLOCK,
            Format::ASTC10x10UNormBlock => ash::vk::Format::ASTC_10X10_UNORM_BLOCK,
            Format::ASTC10x10SrgbBlock => ash::vk::Format::ASTC_10X10_SRGB_BLOCK,
            Format::ASTC12x10UNormBlock => ash::vk::Format::ASTC_12X10_UNORM_BLOCK,
            Format::ASTC12x10SrgbBlock => ash::vk::Format::ASTC_12X10_SRGB_BLOCK,
            Format::ASTC12x12UNormBlock => ash::vk::Format::ASTC_12X12_UNORM_BLOCK,
            Format::ASTC12x12SrgbBlock => ash::vk::Format::ASTC_12X12_SRGB_BLOCK,
            Format::G8B8G8R8422UNorm => ash::vk::Format::G8B8G8R8_422_UNORM,
            Format::B8G8R8G8422UNorm => ash::vk::Format::B8G8R8G8_422_UNORM,
            Format::G8B8R83Plane420UNorm => ash::vk::Format::G8_B8_R8_3PLANE_420_UNORM,
            Format::G8B8R82Plane420UNorm => ash::vk::Format::G8_B8R8_2PLANE_420_UNORM,
            Format::G8B8R83Plane422UNorm => ash::vk::Format::G8_B8_R8_3PLANE_422_UNORM,
            Format::G8B8R82Plane422UNorm => ash::vk::Format::G8_B8R8_2PLANE_422_UNORM,
            Format::G8B8R83Plane444UNorm => ash::vk::Format::G8_B8_R8_3PLANE_444_UNORM,
            Format::R10X6UNormPack16 => ash::vk::Format::R10X6_UNORM_PACK16,
            Format::R10X6G10X6UNorm2Pack16 => ash::vk::Format::R10X6G10X6_UNORM_2PACK16,
            Format::R10X6G10X6B10X6A10X6UNorm4Pack16 => {
                ash::vk::Format::R10X6G10X6B10X6A10X6_UNORM_4PACK16
            }
            Format::G10X6B10X6G10X6R10X6422UNorm4Pack16 => {
                ash::vk::Format::G10X6B10X6G10X6R10X6_422_UNORM_4PACK16
            }
            Format::B10X6G10X6R10X6G10X6422UNorm4Pack16 => {
                ash::vk::Format::B10X6G10X6R10X6G10X6_422_UNORM_4PACK16
            }
            Format::G10X6B10X6R10X63Plane420UNorm3Pack16 => {
                ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16
            }
            Format::G10X6B10X6R10X62Plane420UNorm3Pack16 => {
                ash::vk::Format::G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16
            }
            Format::G10X6B10X6R10X63Plane422UNorm3Pack16 => {
                ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16
            }
            Format::G10X6B10X6R10X62Plane422UNorm3Pack16 => {
                ash::vk::Format::G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16
            }
            Format::G10X6B10X6R10X63Plane444UNorm3Pack16 => {
                ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16
            }
            Format::R12X4UNormPack16 => ash::vk::Format::R12X4_UNORM_PACK16,
            Format::R12X4G12X4UNorm2Pack16 => ash::vk::Format::R12X4G12X4_UNORM_2PACK16,
            Format::R12X4G12X4B12X4A12X4UNorm4Pack16 => {
                ash::vk::Format::R12X4G12X4B12X4A12X4_UNORM_4PACK16
            }
            Format::G12X4B12X4G12X4R12X4422UNorm4Pack16 => {
                ash::vk::Format::G12X4B12X4G12X4R12X4_422_UNORM_4PACK16
            }
            Format::B12X4G12X4R12X4G12X4422UNorm4Pack16 => {
                ash::vk::Format::B12X4G12X4R12X4G12X4_422_UNORM_4PACK16
            }
            Format::G12X4B12X4R12X43Plane420UNorm3Pack16 => {
                ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16
            }
            Format::G12X4B12X4R12X42Plane420UNorm3Pack16 => {
                ash::vk::Format::G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16
            }
            Format::G12X4B12X4R12X43Plane422UNorm3Pack16 => {
                ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16
            }
            Format::G12X4B12X4R12X42Plane422UNorm3Pack16 => {
                ash::vk::Format::G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16
            }
            Format::G12X4B12X4R12X43Plane444UNorm3Pack16 => {
                ash::vk::Format::G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16
            }
            Format::G16B16G16R16422UNorm => ash::vk::Format::G16B16G16R16_422_UNORM,
            Format::B16G16R16G16422UNorm => ash::vk::Format::B16G16R16G16_422_UNORM,
            Format::G16B16R163Plane420UNorm => ash::vk::Format::G16_B16_R16_3PLANE_420_UNORM,
            Format::G16B16R162Plane420UNorm => ash::vk::Format::G16_B16R16_2PLANE_420_UNORM,
            Format::G16B16R163Plane422UNorm => ash::vk::Format::G16_B16_R16_3PLANE_422_UNORM,
            Format::G16B16R162Plane422UNorm => ash::vk::Format::G16_B16R16_2PLANE_422_UNORM,
            Format::G16B16R163Plane444UNorm => ash::vk::Format::G16_B16_R16_3PLANE_444_UNORM,
            Format::G8B8R82Plane444UNorm => ash::vk::Format::G8_B8R8_2PLANE_444_UNORM,
            Format::G10X6B10X6R10X62Plane444UNorm3Pack16 => {
                ash::vk::Format::G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16
            }
            Format::G12X4B12X4R12X42Plane444UNorm3Pack16 => {
                ash::vk::Format::G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16
            }
            Format::G16B16R162Plane444UNorm => ash::vk::Format::G16_B16R16_2PLANE_444_UNORM,
            Format::A4R4G4B4UNormPack16 => ash::vk::Format::A4R4G4B4_UNORM_PACK16,
            Format::A4B4G4R4UNormPack16 => ash::vk::Format::A4B4G4R4_UNORM_PACK16,
            Format::ASTC4x4SFloatBlock => ash::vk::Format::ASTC_4X4_SFLOAT_BLOCK,
            Format::ASTC5x4SFloatBlock => ash::vk::Format::ASTC_5X4_SFLOAT_BLOCK,
            Format::ASTC5x5SFloatBlock => ash::vk::Format::ASTC_5X5_SFLOAT_BLOCK,
            Format::ASTC6x5SFloatBlock => ash::vk::Format::ASTC_6X5_SFLOAT_BLOCK,
            Format::ASTC6x6SFloatBlock => ash::vk::Format::ASTC_6X6_SFLOAT_BLOCK,
            Format::ASTC8x5SFloatBlock => ash::vk::Format::ASTC_8X6_SFLOAT_BLOCK,
            Format::ASTC8x6SFloatBlock => ash::vk::Format::ASTC_8X6_SFLOAT_BLOCK,
            Format::ASTC8x8SFloatBlock => ash::vk::Format::ASTC_8X8_SFLOAT_BLOCK,
            Format::ASTC10x5SFloatBlock => ash::vk::Format::ASTC_10X5_SFLOAT_BLOCK,
            Format::ASTC10x6SFloatBlock => ash::vk::Format::ASTC_10X6_SFLOAT_BLOCK,
            Format::ASTC10x8SFloatBlock => ash::vk::Format::ASTC_10X8_SFLOAT_BLOCK,
            Format::ASTC10x10SFloatBlock => ash::vk::Format::ASTC_10X10_SFLOAT_BLOCK,
            Format::ASTC12x10SFloatBlock => ash::vk::Format::ASTC_12X10_SFLOAT_BLOCK,
            Format::ASTC12x12SFloatBlock => ash::vk::Format::ASTC_12X12_SFLOAT_BLOCK,
            Format::PVRTC12BPPUNormBlockImg => ash::vk::Format::PVRTC1_2BPP_UNORM_BLOCK_IMG,
            Format::PVRTC14BPPUNormBlockImg => ash::vk::Format::PVRTC1_4BPP_UNORM_BLOCK_IMG,
            Format::PVRTC22BPPUNormBlockImg => ash::vk::Format::PVRTC2_2BPP_UNORM_BLOCK_IMG,
            Format::PVRTC24BPPUNormBlockImg => ash::vk::Format::PVRTC2_4BPP_UNORM_BLOCK_IMG,
            Format::PVRTC12BPPSrgbBlockImg => ash::vk::Format::PVRTC1_2BPP_SRGB_BLOCK_IMG,
            Format::PVRTC14BPPSrgbBlockImg => ash::vk::Format::PVRTC1_4BPP_SRGB_BLOCK_IMG,
            Format::PVRTC22BPPSrgbBlockImg => ash::vk::Format::PVRTC2_2BPP_SRGB_BLOCK_IMG,
            Format::PVRTC24BPPSrgbBlockImg => ash::vk::Format::PVRTC2_4BPP_SRGB_BLOCK_IMG,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::Format;

    #[test]
    fn from_format() {
        assert_eq!(Format::Undefined, Format::from(ash::vk::Format::UNDEFINED));
        assert_eq!(
            Format::R4G4UNormPack8,
            Format::from(ash::vk::Format::R4G4_UNORM_PACK8)
        );
        assert_eq!(
            Format::R4G4B4A4UNormPack16,
            Format::from(ash::vk::Format::R4G4B4A4_UNORM_PACK16)
        );
        assert_eq!(
            Format::B4G4R4A4UNormPack16,
            Format::from(ash::vk::Format::B4G4R4A4_UNORM_PACK16)
        );
        assert_eq!(
            Format::R5G6B5UNormPack16,
            Format::from(ash::vk::Format::R5G6B5_UNORM_PACK16)
        );
        assert_eq!(
            Format::B5G6R5UNormPack16,
            Format::from(ash::vk::Format::B5G6R5_UNORM_PACK16)
        );
        assert_eq!(
            Format::R5G5B5A1UNormPack16,
            Format::from(ash::vk::Format::R5G5B5A1_UNORM_PACK16)
        );
        assert_eq!(
            Format::B5G5R5A1UNormPack16,
            Format::from(ash::vk::Format::B5G5R5A1_UNORM_PACK16)
        );
        assert_eq!(
            Format::A1R5G5B5UNormPack16,
            Format::from(ash::vk::Format::A1R5G5B5_UNORM_PACK16)
        );
        assert_eq!(Format::R8UNorm, Format::from(ash::vk::Format::R8_UNORM));
        assert_eq!(Format::R8SNorm, Format::from(ash::vk::Format::R8_SNORM));
        assert_eq!(Format::R8UScaled, Format::from(ash::vk::Format::R8_USCALED));
        assert_eq!(Format::R8SScaled, Format::from(ash::vk::Format::R8_SSCALED));
        assert_eq!(Format::R8UInt, Format::from(ash::vk::Format::R8_UINT));
        assert_eq!(Format::R8SInt, Format::from(ash::vk::Format::R8_SINT));
        assert_eq!(Format::R8Srgb, Format::from(ash::vk::Format::R8_SRGB));
        assert_eq!(Format::R8G8UNorm, Format::from(ash::vk::Format::R8G8_UNORM));
        assert_eq!(Format::R8G8SNorm, Format::from(ash::vk::Format::R8G8_SNORM));
        assert_eq!(
            Format::R8G8UScaled,
            Format::from(ash::vk::Format::R8G8_USCALED)
        );
        assert_eq!(
            Format::R8G8SScaled,
            Format::from(ash::vk::Format::R8G8_SSCALED)
        );
        assert_eq!(Format::R8G8UInt, Format::from(ash::vk::Format::R8G8_UINT));
        assert_eq!(Format::R8G8SInt, Format::from(ash::vk::Format::R8G8_SINT));
        assert_eq!(Format::R8G8Srgb, Format::from(ash::vk::Format::R8G8_SRGB));
        assert_eq!(
            Format::R8G8B8UNorm,
            Format::from(ash::vk::Format::R8G8B8_UNORM)
        );
        assert_eq!(
            Format::R8G8B8SNorm,
            Format::from(ash::vk::Format::R8G8B8_SNORM)
        );
        assert_eq!(
            Format::R8G8B8UScaled,
            Format::from(ash::vk::Format::R8G8B8_USCALED)
        );
        assert_eq!(
            Format::R8G8B8SScaled,
            Format::from(ash::vk::Format::R8G8B8_SSCALED)
        );
        assert_eq!(
            Format::R8G8B8UInt,
            Format::from(ash::vk::Format::R8G8B8_UINT)
        );
        assert_eq!(
            Format::R8G8B8SInt,
            Format::from(ash::vk::Format::R8G8B8_SINT)
        );
        assert_eq!(
            Format::R8G8B8Srgb,
            Format::from(ash::vk::Format::R8G8B8_SRGB)
        );
        assert_eq!(
            Format::B8G8R8UNorm,
            Format::from(ash::vk::Format::B8G8R8_UNORM)
        );
        assert_eq!(
            Format::B8G8R8SNorm,
            Format::from(ash::vk::Format::B8G8R8_SNORM)
        );
        assert_eq!(
            Format::B8G8R8UScaled,
            Format::from(ash::vk::Format::B8G8R8_USCALED)
        );
        assert_eq!(
            Format::B8G8R8SScaled,
            Format::from(ash::vk::Format::B8G8R8_SSCALED)
        );
        assert_eq!(
            Format::B8G8R8UInt,
            Format::from(ash::vk::Format::B8G8R8_UINT)
        );
        assert_eq!(
            Format::B8G8R8SInt,
            Format::from(ash::vk::Format::B8G8R8_SINT)
        );
        assert_eq!(
            Format::B8G8R8Srgb,
            Format::from(ash::vk::Format::B8G8R8_SRGB)
        );
        assert_eq!(
            Format::R8G8B8A8UNorm,
            Format::from(ash::vk::Format::R8G8B8A8_UNORM)
        );
        assert_eq!(
            Format::R8G8B8A8SNorm,
            Format::from(ash::vk::Format::R8G8B8A8_SNORM)
        );
        assert_eq!(
            Format::R8G8B8A8UScaled,
            Format::from(ash::vk::Format::R8G8B8A8_USCALED)
        );
        assert_eq!(
            Format::R8G8B8A8SScaled,
            Format::from(ash::vk::Format::R8G8B8A8_SSCALED)
        );
        assert_eq!(
            Format::R8G8B8A8UInt,
            Format::from(ash::vk::Format::R8G8B8A8_UINT)
        );
        assert_eq!(
            Format::R8G8B8A8SInt,
            Format::from(ash::vk::Format::R8G8B8A8_SINT)
        );
        assert_eq!(
            Format::R8G8B8A8Srgb,
            Format::from(ash::vk::Format::R8G8B8A8_SRGB)
        );
        assert_eq!(
            Format::B8G8R8A8UNorm,
            Format::from(ash::vk::Format::B8G8R8A8_UNORM)
        );
        assert_eq!(
            Format::B8G8R8A8SNorm,
            Format::from(ash::vk::Format::B8G8R8A8_SNORM)
        );
        assert_eq!(
            Format::B8G8R8A8UScaled,
            Format::from(ash::vk::Format::B8G8R8A8_USCALED)
        );
        assert_eq!(
            Format::B8G8R8A8SScaled,
            Format::from(ash::vk::Format::B8G8R8A8_SSCALED)
        );
        assert_eq!(
            Format::B8G8R8A8UInt,
            Format::from(ash::vk::Format::B8G8R8A8_UINT)
        );
        assert_eq!(
            Format::B8G8R8A8SInt,
            Format::from(ash::vk::Format::B8G8R8A8_SINT)
        );
        assert_eq!(
            Format::B8G8R8A8Srgb,
            Format::from(ash::vk::Format::B8G8R8A8_SRGB)
        );
        assert_eq!(
            Format::A8B8G8R8UNormPack32,
            Format::from(ash::vk::Format::A8B8G8R8_UNORM_PACK32)
        );
        assert_eq!(
            Format::A8B8G8R8SNormPack32,
            Format::from(ash::vk::Format::A8B8G8R8_SNORM_PACK32)
        );
        assert_eq!(
            Format::A8B8G8R8UScaledPack32,
            Format::from(ash::vk::Format::A8B8G8R8_USCALED_PACK32)
        );
        assert_eq!(
            Format::A8B8G8R8SScaledPack32,
            Format::from(ash::vk::Format::A8B8G8R8_SSCALED_PACK32)
        );
        assert_eq!(
            Format::A8B8G8R8UIntPack32,
            Format::from(ash::vk::Format::A8B8G8R8_UINT_PACK32)
        );
        assert_eq!(
            Format::A8B8G8R8SIntPack32,
            Format::from(ash::vk::Format::A8B8G8R8_SINT_PACK32)
        );
        assert_eq!(
            Format::A8B8G8R8SrgbPack32,
            Format::from(ash::vk::Format::A8B8G8R8_SRGB_PACK32)
        );
        assert_eq!(
            Format::A2R10G10B10UNormPack32,
            Format::from(ash::vk::Format::A2R10G10B10_UNORM_PACK32)
        );
        assert_eq!(
            Format::A2R10G10B10SNormPack32,
            Format::from(ash::vk::Format::A2R10G10B10_SNORM_PACK32)
        );
        assert_eq!(
            Format::A2R10G10B10UScaledPack32,
            Format::from(ash::vk::Format::A2R10G10B10_USCALED_PACK32)
        );
        assert_eq!(
            Format::A2R10G10B10SScaledPack32,
            Format::from(ash::vk::Format::A2R10G10B10_SSCALED_PACK32)
        );
        assert_eq!(
            Format::A2R10G10B10UIntPack32,
            Format::from(ash::vk::Format::A2R10G10B10_UINT_PACK32)
        );
        assert_eq!(
            Format::A2R10G10B10SIntPack32,
            Format::from(ash::vk::Format::A2R10G10B10_SINT_PACK32)
        );
        assert_eq!(
            Format::A2B10G10R10UNormPack32,
            Format::from(ash::vk::Format::A2B10G10R10_UNORM_PACK32)
        );
        assert_eq!(
            Format::A2B10G10R10SNormPack32,
            Format::from(ash::vk::Format::A2B10G10R10_SNORM_PACK32)
        );
        assert_eq!(
            Format::A2B10G10R10UScaledPack32,
            Format::from(ash::vk::Format::A2B10G10R10_USCALED_PACK32)
        );
        assert_eq!(
            Format::A2B10G10R10SScaledPack32,
            Format::from(ash::vk::Format::A2B10G10R10_SSCALED_PACK32)
        );
        assert_eq!(
            Format::A2B10G10R10UIntPack32,
            Format::from(ash::vk::Format::A2B10G10R10_UINT_PACK32)
        );
        assert_eq!(
            Format::A2B10G10R10SIntPack32,
            Format::from(ash::vk::Format::A2B10G10R10_SINT_PACK32)
        );
        assert_eq!(Format::R16UNorm, Format::from(ash::vk::Format::R16_UNORM));
        assert_eq!(Format::R16SNorm, Format::from(ash::vk::Format::R16_SNORM));
        assert_eq!(
            Format::R16UScaled,
            Format::from(ash::vk::Format::R16_USCALED)
        );
        assert_eq!(
            Format::R16SScaled,
            Format::from(ash::vk::Format::R16_SSCALED)
        );
        assert_eq!(Format::R16UInt, Format::from(ash::vk::Format::R16_UINT));
        assert_eq!(Format::R16SInt, Format::from(ash::vk::Format::R16_SINT));
        assert_eq!(Format::R16SFloat, Format::from(ash::vk::Format::R16_SFLOAT));
        assert_eq!(
            Format::R16G16UNorm,
            Format::from(ash::vk::Format::R16G16_UNORM)
        );
        assert_eq!(
            Format::R16G16SNorm,
            Format::from(ash::vk::Format::R16G16_SNORM)
        );
        assert_eq!(
            Format::R16G16UScaled,
            Format::from(ash::vk::Format::R16G16_USCALED)
        );
        assert_eq!(
            Format::R16G16SScaled,
            Format::from(ash::vk::Format::R16G16_SSCALED)
        );
        assert_eq!(
            Format::R16G16UInt,
            Format::from(ash::vk::Format::R16G16_UINT)
        );
        assert_eq!(
            Format::R16G16SInt,
            Format::from(ash::vk::Format::R16G16_SINT)
        );
        assert_eq!(
            Format::R16G16SFloat,
            Format::from(ash::vk::Format::R16G16_SFLOAT)
        );
        assert_eq!(
            Format::R16G16B16UNorm,
            Format::from(ash::vk::Format::R16G16B16_UNORM)
        );
        assert_eq!(
            Format::R16G16B16SNorm,
            Format::from(ash::vk::Format::R16G16B16_SNORM)
        );
        assert_eq!(
            Format::R16G16B16UScaled,
            Format::from(ash::vk::Format::R16G16B16_USCALED)
        );
        assert_eq!(
            Format::R16G16B16SScaled,
            Format::from(ash::vk::Format::R16G16B16_SSCALED)
        );
        assert_eq!(
            Format::R16G16B16UInt,
            Format::from(ash::vk::Format::R16G16B16_UINT)
        );
        assert_eq!(
            Format::R16G16B16SInt,
            Format::from(ash::vk::Format::R16G16B16_SINT)
        );
        assert_eq!(
            Format::R16G16B16SFloat,
            Format::from(ash::vk::Format::R16G16B16_SFLOAT)
        );
        assert_eq!(
            Format::R16G16B16A16UNorm,
            Format::from(ash::vk::Format::R16G16B16A16_UNORM)
        );
        assert_eq!(
            Format::R16G16B16A16SNorm,
            Format::from(ash::vk::Format::R16G16B16A16_SNORM)
        );
        assert_eq!(
            Format::R16G16B16A16UScaled,
            Format::from(ash::vk::Format::R16G16B16A16_USCALED)
        );
        assert_eq!(
            Format::R16G16B16A16SScaled,
            Format::from(ash::vk::Format::R16G16B16A16_SSCALED)
        );
        assert_eq!(
            Format::R16G16B16A16UInt,
            Format::from(ash::vk::Format::R16G16B16A16_UINT)
        );
        assert_eq!(
            Format::R16G16B16A16SInt,
            Format::from(ash::vk::Format::R16G16B16A16_SINT)
        );
        assert_eq!(
            Format::R16G16B16A16SFloat,
            Format::from(ash::vk::Format::R16G16B16A16_SFLOAT)
        );
        assert_eq!(Format::R32UInt, Format::from(ash::vk::Format::R32_UINT));
        assert_eq!(Format::R32SInt, Format::from(ash::vk::Format::R32_SINT));
        assert_eq!(Format::R32SFloat, Format::from(ash::vk::Format::R32_SFLOAT));
        assert_eq!(
            Format::R32G32UInt,
            Format::from(ash::vk::Format::R32G32_UINT)
        );
        assert_eq!(
            Format::R32G32SInt,
            Format::from(ash::vk::Format::R32G32_SINT)
        );
        assert_eq!(
            Format::R32G32SFloat,
            Format::from(ash::vk::Format::R32G32_SFLOAT)
        );
        assert_eq!(
            Format::R32G32B32UInt,
            Format::from(ash::vk::Format::R32G32B32_UINT)
        );
        assert_eq!(
            Format::R32G32B32SInt,
            Format::from(ash::vk::Format::R32G32B32_SINT)
        );
        assert_eq!(
            Format::R32G32B32SFloat,
            Format::from(ash::vk::Format::R32G32B32_SFLOAT)
        );
        assert_eq!(
            Format::R32G32B32A32UInt,
            Format::from(ash::vk::Format::R32G32B32A32_UINT)
        );
        assert_eq!(
            Format::R32G32B32A32SInt,
            Format::from(ash::vk::Format::R32G32B32A32_SINT)
        );
        assert_eq!(
            Format::R32G32B32A32SFloat,
            Format::from(ash::vk::Format::R32G32B32A32_SFLOAT)
        );
        assert_eq!(Format::R64UInt, Format::from(ash::vk::Format::R64_UINT));
        assert_eq!(Format::R64SInt, Format::from(ash::vk::Format::R64_SINT));
        assert_eq!(Format::R64SFloat, Format::from(ash::vk::Format::R64_SFLOAT));
        assert_eq!(
            Format::R64G64UInt,
            Format::from(ash::vk::Format::R64G64_UINT)
        );
        assert_eq!(
            Format::R64B64SInt,
            Format::from(ash::vk::Format::R64G64_SINT)
        );
        assert_eq!(
            Format::R64B64SFloat,
            Format::from(ash::vk::Format::R64G64_SFLOAT)
        );
        assert_eq!(
            Format::R64G64B64UInt,
            Format::from(ash::vk::Format::R64G64B64_UINT)
        );
        assert_eq!(
            Format::R64G64B64SInt,
            Format::from(ash::vk::Format::R64G64B64_SINT)
        );
        assert_eq!(
            Format::R64G64B64SFloat,
            Format::from(ash::vk::Format::R64G64B64_SFLOAT)
        );
        assert_eq!(
            Format::R64G64B64A64UInt,
            Format::from(ash::vk::Format::R64G64B64A64_UINT)
        );
        assert_eq!(
            Format::R64G64B64A64SInt,
            Format::from(ash::vk::Format::R64G64B64A64_SINT)
        );
        assert_eq!(
            Format::R64G64B64A64SFloat,
            Format::from(ash::vk::Format::R64G64B64A64_SFLOAT)
        );
        assert_eq!(
            Format::B10G11R11UFloatPack32,
            Format::from(ash::vk::Format::B10G11R11_UFLOAT_PACK32)
        );
        assert_eq!(
            Format::E5B9G9R9UFloatPack32,
            Format::from(ash::vk::Format::E5B9G9R9_UFLOAT_PACK32)
        );
        assert_eq!(Format::D16UNorm, Format::from(ash::vk::Format::D16_UNORM));
        assert_eq!(
            Format::X8D24UNormPack32,
            Format::from(ash::vk::Format::X8_D24_UNORM_PACK32)
        );
        assert_eq!(Format::D32SFloat, Format::from(ash::vk::Format::D32_SFLOAT));
        assert_eq!(Format::S8UInt, Format::from(ash::vk::Format::S8_UINT));
        assert_eq!(
            Format::D16UNormS8UInt,
            Format::from(ash::vk::Format::D16_UNORM_S8_UINT)
        );
        assert_eq!(
            Format::D24UNormS8UInt,
            Format::from(ash::vk::Format::D24_UNORM_S8_UINT)
        );
        assert_eq!(
            Format::D32SFloatS8UInt,
            Format::from(ash::vk::Format::D32_SFLOAT_S8_UINT)
        );
        assert_eq!(
            Format::BC1RgbUNormBlock,
            Format::from(ash::vk::Format::BC1_RGB_UNORM_BLOCK)
        );
        assert_eq!(
            Format::BC1RgbSrgbBlock,
            Format::from(ash::vk::Format::BC1_RGB_SRGB_BLOCK)
        );
        assert_eq!(
            Format::BC1RgbaUNormBlock,
            Format::from(ash::vk::Format::BC1_RGBA_UNORM_BLOCK)
        );
        assert_eq!(
            Format::BC1RgbaSrgbBlock,
            Format::from(ash::vk::Format::BC1_RGBA_SRGB_BLOCK)
        );
        assert_eq!(
            Format::BC2UNormBlock,
            Format::from(ash::vk::Format::BC2_UNORM_BLOCK)
        );
        assert_eq!(
            Format::BC2SrgbBlock,
            Format::from(ash::vk::Format::BC2_SRGB_BLOCK)
        );
        assert_eq!(
            Format::BC3UNormBlock,
            Format::from(ash::vk::Format::BC3_UNORM_BLOCK)
        );
        assert_eq!(
            Format::BC3SrgbBlock,
            Format::from(ash::vk::Format::BC3_SRGB_BLOCK)
        );
        assert_eq!(
            Format::BC4UNormBlock,
            Format::from(ash::vk::Format::BC4_UNORM_BLOCK)
        );
        assert_eq!(
            Format::BC4SNormBlock,
            Format::from(ash::vk::Format::BC4_SNORM_BLOCK)
        );
        assert_eq!(
            Format::BC5UNormBlock,
            Format::from(ash::vk::Format::BC5_UNORM_BLOCK)
        );
        assert_eq!(
            Format::BC5SNormBlock,
            Format::from(ash::vk::Format::BC5_SNORM_BLOCK)
        );
        assert_eq!(
            Format::BC6HUFloatBlock,
            Format::from(ash::vk::Format::BC6H_UFLOAT_BLOCK)
        );
        assert_eq!(
            Format::BC6HSFloatBlock,
            Format::from(ash::vk::Format::BC6H_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::BC7UNormBlock,
            Format::from(ash::vk::Format::BC7_UNORM_BLOCK)
        );
        assert_eq!(
            Format::BC7SrgbBlock,
            Format::from(ash::vk::Format::BC7_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ETC2R8G8B8UNormBlock,
            Format::from(ash::vk::Format::ETC2_R8G8B8_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ETC2R8G8B8SrgbBlock,
            Format::from(ash::vk::Format::ETC2_R8G8B8_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ETC2R8G8B8A1UNormBlock,
            Format::from(ash::vk::Format::ETC2_R8G8B8A1_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ETC2R8G8B8A1SrgbBlock,
            Format::from(ash::vk::Format::ETC2_R8G8B8A1_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ETC2R8G8B8A8UNormBlock,
            Format::from(ash::vk::Format::ETC2_R8G8B8A8_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ETC2R8G8B8A8SrgbBlock,
            Format::from(ash::vk::Format::ETC2_R8G8B8A8_SRGB_BLOCK)
        );
        assert_eq!(
            Format::EACR11UNormBlock,
            Format::from(ash::vk::Format::EAC_R11_UNORM_BLOCK)
        );
        assert_eq!(
            Format::EACR11SNormBlock,
            Format::from(ash::vk::Format::EAC_R11_SNORM_BLOCK)
        );
        assert_eq!(
            Format::EACR11G11UNormBlock,
            Format::from(ash::vk::Format::EAC_R11G11_UNORM_BLOCK)
        );
        assert_eq!(
            Format::EACR11G11SNormBlock,
            Format::from(ash::vk::Format::EAC_R11G11_SNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC4x4UNormBlock,
            Format::from(ash::vk::Format::ASTC_4X4_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC4x4SrgbBlock,
            Format::from(ash::vk::Format::ASTC_4X4_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC5x4UNormBlock,
            Format::from(ash::vk::Format::ASTC_5X4_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC5x4SrgbBlock,
            Format::from(ash::vk::Format::ASTC_5X4_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC5x5UNormBlock,
            Format::from(ash::vk::Format::ASTC_5X5_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC5x5SrgbBlock,
            Format::from(ash::vk::Format::ASTC_5X5_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC6x5UNormBlock,
            Format::from(ash::vk::Format::ASTC_6X5_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC6x5SrgbBlock,
            Format::from(ash::vk::Format::ASTC_6X5_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC6x6UNormBlock,
            Format::from(ash::vk::Format::ASTC_6X6_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC6x6SrgbBlock,
            Format::from(ash::vk::Format::ASTC_6X6_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC8x5UNormBlock,
            Format::from(ash::vk::Format::ASTC_8X5_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC8x5SrgbBlock,
            Format::from(ash::vk::Format::ASTC_8X5_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC8x6UNormBlock,
            Format::from(ash::vk::Format::ASTC_8X6_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC8x6SrgbBlock,
            Format::from(ash::vk::Format::ASTC_8X6_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC8x8UNormBlock,
            Format::from(ash::vk::Format::ASTC_8X8_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC8x8SrgbBlock,
            Format::from(ash::vk::Format::ASTC_8X8_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x5UNormBlock,
            Format::from(ash::vk::Format::ASTC_10X5_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x5SrgbBlock,
            Format::from(ash::vk::Format::ASTC_10X5_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x6UNormBlock,
            Format::from(ash::vk::Format::ASTC_10X6_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x6SrgbBlock,
            Format::from(ash::vk::Format::ASTC_10X6_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x8UNormBlock,
            Format::from(ash::vk::Format::ASTC_10X8_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x8SrgbBlock,
            Format::from(ash::vk::Format::ASTC_10X8_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x10UNormBlock,
            Format::from(ash::vk::Format::ASTC_10X10_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x10SrgbBlock,
            Format::from(ash::vk::Format::ASTC_10X10_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC12x10UNormBlock,
            Format::from(ash::vk::Format::ASTC_12X10_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC12x10SrgbBlock,
            Format::from(ash::vk::Format::ASTC_12X10_SRGB_BLOCK)
        );
        assert_eq!(
            Format::ASTC12x12UNormBlock,
            Format::from(ash::vk::Format::ASTC_12X12_UNORM_BLOCK)
        );
        assert_eq!(
            Format::ASTC12x12SrgbBlock,
            Format::from(ash::vk::Format::ASTC_12X12_SRGB_BLOCK)
        );
        assert_eq!(
            Format::G8B8G8R8422UNorm,
            Format::from(ash::vk::Format::G8B8G8R8_422_UNORM)
        );
        assert_eq!(
            Format::B8G8R8G8422UNorm,
            Format::from(ash::vk::Format::B8G8R8G8_422_UNORM)
        );
        assert_eq!(
            Format::G8B8R83Plane420UNorm,
            Format::from(ash::vk::Format::G8_B8_R8_3PLANE_420_UNORM)
        );
        assert_eq!(
            Format::G8B8R82Plane420UNorm,
            Format::from(ash::vk::Format::G8_B8R8_2PLANE_420_UNORM)
        );
        assert_eq!(
            Format::G8B8R83Plane422UNorm,
            Format::from(ash::vk::Format::G8_B8_R8_3PLANE_422_UNORM)
        );
        assert_eq!(
            Format::G8B8R82Plane422UNorm,
            Format::from(ash::vk::Format::G8_B8R8_2PLANE_422_UNORM)
        );
        assert_eq!(
            Format::G8B8R83Plane444UNorm,
            Format::from(ash::vk::Format::G8_B8_R8_3PLANE_444_UNORM)
        );
        assert_eq!(
            Format::R10X6UNormPack16,
            Format::from(ash::vk::Format::R10X6_UNORM_PACK16)
        );
        assert_eq!(
            Format::R10X6G10X6UNorm2Pack16,
            Format::from(ash::vk::Format::R10X6G10X6_UNORM_2PACK16)
        );
        assert_eq!(
            Format::R10X6G10X6B10X6A10X6UNorm4Pack16,
            Format::from(ash::vk::Format::R10X6G10X6B10X6A10X6_UNORM_4PACK16)
        );
        assert_eq!(
            Format::G10X6B10X6G10X6R10X6422UNorm4Pack16,
            Format::from(ash::vk::Format::G10X6B10X6G10X6R10X6_422_UNORM_4PACK16)
        );
        assert_eq!(
            Format::B10X6G10X6R10X6G10X6422UNorm4Pack16,
            Format::from(ash::vk::Format::B10X6G10X6R10X6G10X6_422_UNORM_4PACK16)
        );
        assert_eq!(
            Format::G10X6B10X6R10X63Plane420UNorm3Pack16,
            Format::from(ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G10X6B10X6R10X62Plane420UNorm3Pack16,
            Format::from(ash::vk::Format::G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G10X6B10X6R10X63Plane422UNorm3Pack16,
            Format::from(ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G10X6B10X6R10X62Plane422UNorm3Pack16,
            Format::from(ash::vk::Format::G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G10X6B10X6R10X63Plane444UNorm3Pack16,
            Format::from(ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16)
        );
        assert_eq!(
            Format::R12X4UNormPack16,
            Format::from(ash::vk::Format::R12X4_UNORM_PACK16)
        );
        assert_eq!(
            Format::R12X4G12X4UNorm2Pack16,
            Format::from(ash::vk::Format::R12X4G12X4_UNORM_2PACK16)
        );
        assert_eq!(
            Format::R12X4G12X4B12X4A12X4UNorm4Pack16,
            Format::from(ash::vk::Format::R12X4G12X4B12X4A12X4_UNORM_4PACK16)
        );
        assert_eq!(
            Format::G12X4B12X4G12X4R12X4422UNorm4Pack16,
            Format::from(ash::vk::Format::G12X4B12X4G12X4R12X4_422_UNORM_4PACK16)
        );
        assert_eq!(
            Format::B12X4G12X4R12X4G12X4422UNorm4Pack16,
            Format::from(ash::vk::Format::B12X4G12X4R12X4G12X4_422_UNORM_4PACK16)
        );
        assert_eq!(
            Format::G12X4B12X4R12X43Plane420UNorm3Pack16,
            Format::from(ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G12X4B12X4R12X42Plane420UNorm3Pack16,
            Format::from(ash::vk::Format::G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G12X4B12X4R12X43Plane422UNorm3Pack16,
            Format::from(ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G12X4B12X4R12X42Plane422UNorm3Pack16,
            Format::from(ash::vk::Format::G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G12X4B12X4R12X43Plane444UNorm3Pack16,
            Format::from(ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G16B16G16R16422UNorm,
            Format::from(ash::vk::Format::G16B16G16R16_422_UNORM)
        );
        assert_eq!(
            Format::B16G16R16G16422UNorm,
            Format::from(ash::vk::Format::B16G16R16G16_422_UNORM)
        );
        assert_eq!(
            Format::G16B16R163Plane420UNorm,
            Format::from(ash::vk::Format::G16_B16_R16_3PLANE_420_UNORM)
        );
        assert_eq!(
            Format::G16B16R162Plane420UNorm,
            Format::from(ash::vk::Format::G16_B16R16_2PLANE_420_UNORM)
        );
        assert_eq!(
            Format::G16B16R163Plane422UNorm,
            Format::from(ash::vk::Format::G16_B16_R16_3PLANE_422_UNORM)
        );
        assert_eq!(
            Format::G16B16R162Plane422UNorm,
            Format::from(ash::vk::Format::G16_B16R16_2PLANE_422_UNORM)
        );
        assert_eq!(
            Format::G16B16R163Plane444UNorm,
            Format::from(ash::vk::Format::G16_B16_R16_3PLANE_444_UNORM)
        );
        assert_eq!(
            Format::G8B8R82Plane444UNorm,
            Format::from(ash::vk::Format::G8_B8R8_2PLANE_444_UNORM)
        );
        assert_eq!(
            Format::G10X6B10X6R10X62Plane444UNorm3Pack16,
            Format::from(ash::vk::Format::G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G12X4B12X4R12X42Plane444UNorm3Pack16,
            Format::from(ash::vk::Format::G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16)
        );
        assert_eq!(
            Format::G16B16R162Plane444UNorm,
            Format::from(ash::vk::Format::G16_B16R16_2PLANE_444_UNORM)
        );
        assert_eq!(
            Format::A4R4G4B4UNormPack16,
            Format::from(ash::vk::Format::A4R4G4B4_UNORM_PACK16)
        );
        assert_eq!(
            Format::A4B4G4R4UNormPack16,
            Format::from(ash::vk::Format::A4B4G4R4_UNORM_PACK16)
        );
        assert_eq!(
            Format::ASTC4x4SFloatBlock,
            Format::from(ash::vk::Format::ASTC_4X4_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC5x4SFloatBlock,
            Format::from(ash::vk::Format::ASTC_5X4_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC5x5SFloatBlock,
            Format::from(ash::vk::Format::ASTC_5X5_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC6x5SFloatBlock,
            Format::from(ash::vk::Format::ASTC_6X5_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC6x6SFloatBlock,
            Format::from(ash::vk::Format::ASTC_6X6_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC8x5SFloatBlock,
            Format::from(ash::vk::Format::ASTC_8X5_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC8x6SFloatBlock,
            Format::from(ash::vk::Format::ASTC_8X6_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC8x8SFloatBlock,
            Format::from(ash::vk::Format::ASTC_8X8_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x5SFloatBlock,
            Format::from(ash::vk::Format::ASTC_10X5_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x6SFloatBlock,
            Format::from(ash::vk::Format::ASTC_10X6_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x8SFloatBlock,
            Format::from(ash::vk::Format::ASTC_10X8_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC10x10SFloatBlock,
            Format::from(ash::vk::Format::ASTC_10X10_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC12x10SFloatBlock,
            Format::from(ash::vk::Format::ASTC_12X10_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::ASTC12x12SFloatBlock,
            Format::from(ash::vk::Format::ASTC_12X12_SFLOAT_BLOCK)
        );
        assert_eq!(
            Format::PVRTC12BPPUNormBlockImg,
            Format::from(ash::vk::Format::PVRTC1_2BPP_UNORM_BLOCK_IMG)
        );
        assert_eq!(
            Format::PVRTC14BPPUNormBlockImg,
            Format::from(ash::vk::Format::PVRTC1_4BPP_UNORM_BLOCK_IMG)
        );
        assert_eq!(
            Format::PVRTC22BPPUNormBlockImg,
            Format::from(ash::vk::Format::PVRTC2_2BPP_UNORM_BLOCK_IMG)
        );
        assert_eq!(
            Format::PVRTC24BPPUNormBlockImg,
            Format::from(ash::vk::Format::PVRTC2_4BPP_UNORM_BLOCK_IMG)
        );
        assert_eq!(
            Format::PVRTC12BPPSrgbBlockImg,
            Format::from(ash::vk::Format::PVRTC1_2BPP_SRGB_BLOCK_IMG)
        );
        assert_eq!(
            Format::PVRTC14BPPSrgbBlockImg,
            Format::from(ash::vk::Format::PVRTC1_4BPP_SRGB_BLOCK_IMG)
        );
        assert_eq!(
            Format::PVRTC22BPPSrgbBlockImg,
            Format::from(ash::vk::Format::PVRTC2_2BPP_SRGB_BLOCK_IMG)
        );
        assert_eq!(
            Format::PVRTC24BPPSrgbBlockImg,
            Format::from(ash::vk::Format::PVRTC2_4BPP_SRGB_BLOCK_IMG)
        );
    }

    #[test]
    fn to_format() {
        assert_eq!(
            ash::vk::Format::UNDEFINED,
            ash::vk::Format::from(Format::Undefined)
        );
        assert_eq!(
            ash::vk::Format::R4G4_UNORM_PACK8,
            ash::vk::Format::from(Format::R4G4UNormPack8)
        );
        assert_eq!(
            ash::vk::Format::R4G4B4A4_UNORM_PACK16,
            ash::vk::Format::from(Format::R4G4B4A4UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::B4G4R4A4_UNORM_PACK16,
            ash::vk::Format::from(Format::B4G4R4A4UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::R5G6B5_UNORM_PACK16,
            ash::vk::Format::from(Format::R5G6B5UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::B5G6R5_UNORM_PACK16,
            ash::vk::Format::from(Format::B5G6R5UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::R5G5B5A1_UNORM_PACK16,
            ash::vk::Format::from(Format::R5G5B5A1UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::B5G5R5A1_UNORM_PACK16,
            ash::vk::Format::from(Format::B5G5R5A1UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::A1R5G5B5_UNORM_PACK16,
            ash::vk::Format::from(Format::A1R5G5B5UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::R8_UNORM,
            ash::vk::Format::from(Format::R8UNorm)
        );
        assert_eq!(
            ash::vk::Format::R8_SNORM,
            ash::vk::Format::from(Format::R8SNorm)
        );
        assert_eq!(
            ash::vk::Format::R8_USCALED,
            ash::vk::Format::from(Format::R8UScaled)
        );
        assert_eq!(
            ash::vk::Format::R8_SSCALED,
            ash::vk::Format::from(Format::R8SScaled)
        );
        assert_eq!(
            ash::vk::Format::R8_UINT,
            ash::vk::Format::from(Format::R8UInt)
        );
        assert_eq!(
            ash::vk::Format::R8_SINT,
            ash::vk::Format::from(Format::R8SInt)
        );
        assert_eq!(
            ash::vk::Format::R8_SRGB,
            ash::vk::Format::from(Format::R8Srgb)
        );
        assert_eq!(
            ash::vk::Format::R8G8_UNORM,
            ash::vk::Format::from(Format::R8G8UNorm)
        );
        assert_eq!(
            ash::vk::Format::R8G8_SNORM,
            ash::vk::Format::from(Format::R8G8SNorm)
        );
        assert_eq!(
            ash::vk::Format::R8G8_USCALED,
            ash::vk::Format::from(Format::R8G8UScaled)
        );
        assert_eq!(
            ash::vk::Format::R8G8_SSCALED,
            ash::vk::Format::from(Format::R8G8SScaled)
        );
        assert_eq!(
            ash::vk::Format::R8G8_UINT,
            ash::vk::Format::from(Format::R8G8UInt)
        );
        assert_eq!(
            ash::vk::Format::R8G8_SINT,
            ash::vk::Format::from(Format::R8G8SInt)
        );
        assert_eq!(
            ash::vk::Format::R8G8_SRGB,
            ash::vk::Format::from(Format::R8G8Srgb)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8_UNORM,
            ash::vk::Format::from(Format::R8G8B8UNorm)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8_SNORM,
            ash::vk::Format::from(Format::R8G8B8SNorm)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8_USCALED,
            ash::vk::Format::from(Format::R8G8B8UScaled)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8_SSCALED,
            ash::vk::Format::from(Format::R8G8B8SScaled)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8_UINT,
            ash::vk::Format::from(Format::R8G8B8UInt)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8_SINT,
            ash::vk::Format::from(Format::R8G8B8SInt)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8_SRGB,
            ash::vk::Format::from(Format::R8G8B8Srgb)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8_UNORM,
            ash::vk::Format::from(Format::B8G8R8UNorm)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8_SNORM,
            ash::vk::Format::from(Format::B8G8R8SNorm)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8_USCALED,
            ash::vk::Format::from(Format::B8G8R8UScaled)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8_SSCALED,
            ash::vk::Format::from(Format::B8G8R8SScaled)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8_UINT,
            ash::vk::Format::from(Format::B8G8R8UInt)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8_SINT,
            ash::vk::Format::from(Format::B8G8R8SInt)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8_SRGB,
            ash::vk::Format::from(Format::B8G8R8Srgb)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8A8_UNORM,
            ash::vk::Format::from(Format::R8G8B8A8UNorm)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8A8_SNORM,
            ash::vk::Format::from(Format::R8G8B8A8SNorm)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8A8_USCALED,
            ash::vk::Format::from(Format::R8G8B8A8UScaled)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8A8_SSCALED,
            ash::vk::Format::from(Format::R8G8B8A8SScaled)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8A8_UINT,
            ash::vk::Format::from(Format::R8G8B8A8UInt)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8A8_SINT,
            ash::vk::Format::from(Format::R8G8B8A8SInt)
        );
        assert_eq!(
            ash::vk::Format::R8G8B8A8_SRGB,
            ash::vk::Format::from(Format::R8G8B8A8Srgb)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8A8_UNORM,
            ash::vk::Format::from(Format::B8G8R8A8UNorm)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8A8_SNORM,
            ash::vk::Format::from(Format::B8G8R8A8SNorm)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8A8_USCALED,
            ash::vk::Format::from(Format::B8G8R8A8UScaled)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8A8_SSCALED,
            ash::vk::Format::from(Format::B8G8R8A8SScaled)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8A8_UINT,
            ash::vk::Format::from(Format::B8G8R8A8UInt)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8A8_SINT,
            ash::vk::Format::from(Format::B8G8R8A8SInt)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8A8_SRGB,
            ash::vk::Format::from(Format::B8G8R8A8Srgb)
        );
        assert_eq!(
            ash::vk::Format::A8B8G8R8_UNORM_PACK32,
            ash::vk::Format::from(Format::A8B8G8R8UNormPack32)
        );
        assert_eq!(
            ash::vk::Format::A8B8G8R8_SNORM_PACK32,
            ash::vk::Format::from(Format::A8B8G8R8SNormPack32)
        );
        assert_eq!(
            ash::vk::Format::A8B8G8R8_USCALED_PACK32,
            ash::vk::Format::from(Format::A8B8G8R8UScaledPack32)
        );
        assert_eq!(
            ash::vk::Format::A8B8G8R8_SSCALED_PACK32,
            ash::vk::Format::from(Format::A8B8G8R8SScaledPack32)
        );
        assert_eq!(
            ash::vk::Format::A8B8G8R8_UINT_PACK32,
            ash::vk::Format::from(Format::A8B8G8R8UIntPack32)
        );
        assert_eq!(
            ash::vk::Format::A8B8G8R8_SINT_PACK32,
            ash::vk::Format::from(Format::A8B8G8R8SIntPack32)
        );
        assert_eq!(
            ash::vk::Format::A8B8G8R8_SRGB_PACK32,
            ash::vk::Format::from(Format::A8B8G8R8SrgbPack32)
        );
        assert_eq!(
            ash::vk::Format::A2R10G10B10_UNORM_PACK32,
            ash::vk::Format::from(Format::A2R10G10B10UNormPack32)
        );
        assert_eq!(
            ash::vk::Format::A2R10G10B10_SNORM_PACK32,
            ash::vk::Format::from(Format::A2R10G10B10SNormPack32)
        );
        assert_eq!(
            ash::vk::Format::A2R10G10B10_USCALED_PACK32,
            ash::vk::Format::from(Format::A2R10G10B10UScaledPack32)
        );
        assert_eq!(
            ash::vk::Format::A2R10G10B10_SSCALED_PACK32,
            ash::vk::Format::from(Format::A2R10G10B10SScaledPack32)
        );
        assert_eq!(
            ash::vk::Format::A2R10G10B10_UINT_PACK32,
            ash::vk::Format::from(Format::A2R10G10B10UIntPack32)
        );
        assert_eq!(
            ash::vk::Format::A2R10G10B10_SINT_PACK32,
            ash::vk::Format::from(Format::A2R10G10B10SIntPack32)
        );
        assert_eq!(
            ash::vk::Format::A2B10G10R10_UNORM_PACK32,
            ash::vk::Format::from(Format::A2B10G10R10UNormPack32)
        );
        assert_eq!(
            ash::vk::Format::A2B10G10R10_SNORM_PACK32,
            ash::vk::Format::from(Format::A2B10G10R10SNormPack32)
        );
        assert_eq!(
            ash::vk::Format::A2B10G10R10_SSCALED_PACK32,
            ash::vk::Format::from(Format::A2B10G10R10UScaledPack32)
        );
        assert_eq!(
            ash::vk::Format::A2B10G10R10_SSCALED_PACK32,
            ash::vk::Format::from(Format::A2B10G10R10SScaledPack32)
        );
        assert_eq!(
            ash::vk::Format::A2B10G10R10_UINT_PACK32,
            ash::vk::Format::from(Format::A2B10G10R10UIntPack32)
        );
        assert_eq!(
            ash::vk::Format::A2B10G10R10_SINT_PACK32,
            ash::vk::Format::from(Format::A2B10G10R10SIntPack32)
        );
        assert_eq!(
            ash::vk::Format::R16_UNORM,
            ash::vk::Format::from(Format::R16UNorm)
        );
        assert_eq!(
            ash::vk::Format::R16_SNORM,
            ash::vk::Format::from(Format::R16SNorm)
        );
        assert_eq!(
            ash::vk::Format::R16_USCALED,
            ash::vk::Format::from(Format::R16UScaled)
        );
        assert_eq!(
            ash::vk::Format::R16_SSCALED,
            ash::vk::Format::from(Format::R16SScaled)
        );
        assert_eq!(
            ash::vk::Format::R16_UINT,
            ash::vk::Format::from(Format::R16UInt)
        );
        assert_eq!(
            ash::vk::Format::R16_SINT,
            ash::vk::Format::from(Format::R16SInt)
        );
        assert_eq!(
            ash::vk::Format::R16_SFLOAT,
            ash::vk::Format::from(Format::R16SFloat)
        );
        assert_eq!(
            ash::vk::Format::R16G16_UNORM,
            ash::vk::Format::from(Format::R16G16UNorm)
        );
        assert_eq!(
            ash::vk::Format::R16G16_SNORM,
            ash::vk::Format::from(Format::R16G16SNorm)
        );
        assert_eq!(
            ash::vk::Format::R16G16_USCALED,
            ash::vk::Format::from(Format::R16G16UScaled)
        );
        assert_eq!(
            ash::vk::Format::R16G16_SSCALED,
            ash::vk::Format::from(Format::R16G16SScaled)
        );
        assert_eq!(
            ash::vk::Format::R16G16_UINT,
            ash::vk::Format::from(Format::R16G16UInt)
        );
        assert_eq!(
            ash::vk::Format::R16G16_SINT,
            ash::vk::Format::from(Format::R16G16SInt)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16_UNORM,
            ash::vk::Format::from(Format::R16G16SFloat)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16_UNORM,
            ash::vk::Format::from(Format::R16G16B16UNorm)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16_SNORM,
            ash::vk::Format::from(Format::R16G16B16SNorm)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16_SSCALED,
            ash::vk::Format::from(Format::R16G16B16UScaled)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16_SSCALED,
            ash::vk::Format::from(Format::R16G16B16SScaled)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16_UINT,
            ash::vk::Format::from(Format::R16G16B16UInt)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16_SFLOAT,
            ash::vk::Format::from(Format::R16G16B16SInt)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16_SFLOAT,
            ash::vk::Format::from(Format::R16G16B16SFloat)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16A16_UNORM,
            ash::vk::Format::from(Format::R16G16B16A16UNorm)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16A16_SNORM,
            ash::vk::Format::from(Format::R16G16B16A16SNorm)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16A16_USCALED,
            ash::vk::Format::from(Format::R16G16B16A16UScaled)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16A16_SSCALED,
            ash::vk::Format::from(Format::R16G16B16A16SScaled)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16A16_UINT,
            ash::vk::Format::from(Format::R16G16B16A16UInt)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16A16_SINT,
            ash::vk::Format::from(Format::R16G16B16A16SInt)
        );
        assert_eq!(
            ash::vk::Format::R16G16B16A16_SFLOAT,
            ash::vk::Format::from(Format::R16G16B16A16SFloat)
        );
        assert_eq!(
            ash::vk::Format::R32_UINT,
            ash::vk::Format::from(Format::R32UInt)
        );
        assert_eq!(
            ash::vk::Format::R32_SINT,
            ash::vk::Format::from(Format::R32SInt)
        );
        assert_eq!(
            ash::vk::Format::R32_SFLOAT,
            ash::vk::Format::from(Format::R32SFloat)
        );
        assert_eq!(
            ash::vk::Format::R32G32_UINT,
            ash::vk::Format::from(Format::R32G32UInt)
        );
        assert_eq!(
            ash::vk::Format::R32G32_SINT,
            ash::vk::Format::from(Format::R32G32SInt)
        );
        assert_eq!(
            ash::vk::Format::R32G32_SFLOAT,
            ash::vk::Format::from(Format::R32G32SFloat)
        );
        assert_eq!(
            ash::vk::Format::R32G32B32_UINT,
            ash::vk::Format::from(Format::R32G32B32UInt)
        );
        assert_eq!(
            ash::vk::Format::R32G32B32_SINT,
            ash::vk::Format::from(Format::R32G32B32SInt)
        );
        assert_eq!(
            ash::vk::Format::R32G32B32_SFLOAT,
            ash::vk::Format::from(Format::R32G32B32SFloat)
        );
        assert_eq!(
            ash::vk::Format::R32G32B32A32_UINT,
            ash::vk::Format::from(Format::R32G32B32A32UInt)
        );
        assert_eq!(
            ash::vk::Format::R32G32B32A32_SINT,
            ash::vk::Format::from(Format::R32G32B32A32SInt)
        );
        assert_eq!(
            ash::vk::Format::R32G32B32A32_SFLOAT,
            ash::vk::Format::from(Format::R32G32B32A32SFloat)
        );
        assert_eq!(
            ash::vk::Format::R64_UINT,
            ash::vk::Format::from(Format::R64UInt)
        );
        assert_eq!(
            ash::vk::Format::R64_SINT,
            ash::vk::Format::from(Format::R64SInt)
        );
        assert_eq!(
            ash::vk::Format::R64_SFLOAT,
            ash::vk::Format::from(Format::R64SFloat)
        );
        assert_eq!(
            ash::vk::Format::R64G64_UINT,
            ash::vk::Format::from(Format::R64G64UInt)
        );
        assert_eq!(
            ash::vk::Format::R64G64_SINT,
            ash::vk::Format::from(Format::R64B64SInt)
        );
        assert_eq!(
            ash::vk::Format::R64G64_SFLOAT,
            ash::vk::Format::from(Format::R64B64SFloat)
        );
        assert_eq!(
            ash::vk::Format::R64G64B64_UINT,
            ash::vk::Format::from(Format::R64G64B64UInt)
        );
        assert_eq!(
            ash::vk::Format::R64G64B64_SINT,
            ash::vk::Format::from(Format::R64G64B64SInt)
        );
        assert_eq!(
            ash::vk::Format::R64G64B64_SFLOAT,
            ash::vk::Format::from(Format::R64G64B64SFloat)
        );
        assert_eq!(
            ash::vk::Format::R64G64B64A64_UINT,
            ash::vk::Format::from(Format::R64G64B64A64UInt)
        );
        assert_eq!(
            ash::vk::Format::R64G64B64A64_SINT,
            ash::vk::Format::from(Format::R64G64B64A64SInt)
        );
        assert_eq!(
            ash::vk::Format::R64G64B64A64_SFLOAT,
            ash::vk::Format::from(Format::R64G64B64A64SFloat)
        );
        assert_eq!(
            ash::vk::Format::B10G11R11_UFLOAT_PACK32,
            ash::vk::Format::from(Format::B10G11R11UFloatPack32)
        );
        assert_eq!(
            ash::vk::Format::E5B9G9R9_UFLOAT_PACK32,
            ash::vk::Format::from(Format::E5B9G9R9UFloatPack32)
        );
        assert_eq!(
            ash::vk::Format::D16_UNORM,
            ash::vk::Format::from(Format::D16UNorm)
        );
        assert_eq!(
            ash::vk::Format::X8_D24_UNORM_PACK32,
            ash::vk::Format::from(Format::X8D24UNormPack32)
        );
        assert_eq!(
            ash::vk::Format::D32_SFLOAT,
            ash::vk::Format::from(Format::D32SFloat)
        );
        assert_eq!(
            ash::vk::Format::S8_UINT,
            ash::vk::Format::from(Format::S8UInt)
        );
        assert_eq!(
            ash::vk::Format::D16_UNORM_S8_UINT,
            ash::vk::Format::from(Format::D16UNormS8UInt)
        );
        assert_eq!(
            ash::vk::Format::D24_UNORM_S8_UINT,
            ash::vk::Format::from(Format::D24UNormS8UInt)
        );
        assert_eq!(
            ash::vk::Format::D32_SFLOAT_S8_UINT,
            ash::vk::Format::from(Format::D32SFloatS8UInt)
        );
        assert_eq!(
            ash::vk::Format::BC1_RGB_UNORM_BLOCK,
            ash::vk::Format::from(Format::BC1RgbUNormBlock)
        );
        assert_eq!(
            ash::vk::Format::BC1_RGB_SRGB_BLOCK,
            ash::vk::Format::from(Format::BC1RgbSrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::BC1_RGBA_UNORM_BLOCK,
            ash::vk::Format::from(Format::BC1RgbaUNormBlock)
        );
        assert_eq!(
            ash::vk::Format::BC1_RGBA_SRGB_BLOCK,
            ash::vk::Format::from(Format::BC1RgbaSrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::BC2_UNORM_BLOCK,
            ash::vk::Format::from(Format::BC2UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::BC2_SRGB_BLOCK,
            ash::vk::Format::from(Format::BC2SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::BC3_UNORM_BLOCK,
            ash::vk::Format::from(Format::BC3UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::BC3_SRGB_BLOCK,
            ash::vk::Format::from(Format::BC3SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::BC4_UNORM_BLOCK,
            ash::vk::Format::from(Format::BC4UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::BC4_SNORM_BLOCK,
            ash::vk::Format::from(Format::BC4SNormBlock)
        );
        assert_eq!(
            ash::vk::Format::BC5_UNORM_BLOCK,
            ash::vk::Format::from(Format::BC5UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::BC5_SNORM_BLOCK,
            ash::vk::Format::from(Format::BC5SNormBlock)
        );
        assert_eq!(
            ash::vk::Format::BC6H_UFLOAT_BLOCK,
            ash::vk::Format::from(Format::BC6HUFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::BC6H_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::BC6HSFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::BC7_UNORM_BLOCK,
            ash::vk::Format::from(Format::BC7UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::BC7_SRGB_BLOCK,
            ash::vk::Format::from(Format::BC7SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ETC2_R8G8B8_UNORM_BLOCK,
            ash::vk::Format::from(Format::ETC2R8G8B8UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ETC2_R8G8B8_SRGB_BLOCK,
            ash::vk::Format::from(Format::ETC2R8G8B8SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ETC2_R8G8B8A1_UNORM_BLOCK,
            ash::vk::Format::from(Format::ETC2R8G8B8A1UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ETC2_R8G8B8A1_SRGB_BLOCK,
            ash::vk::Format::from(Format::ETC2R8G8B8A1SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ETC2_R8G8B8A8_UNORM_BLOCK,
            ash::vk::Format::from(Format::ETC2R8G8B8A8UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ETC2_R8G8B8A8_SRGB_BLOCK,
            ash::vk::Format::from(Format::ETC2R8G8B8A8SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::EAC_R11_UNORM_BLOCK,
            ash::vk::Format::from(Format::EACR11UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::EAC_R11_SNORM_BLOCK,
            ash::vk::Format::from(Format::EACR11SNormBlock)
        );
        assert_eq!(
            ash::vk::Format::EAC_R11G11_UNORM_BLOCK,
            ash::vk::Format::from(Format::EACR11G11UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::EAC_R11G11_SNORM_BLOCK,
            ash::vk::Format::from(Format::EACR11G11SNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_4X4_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC4x4UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_4X4_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC4x4SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_4X4_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC5x4UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_5X4_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC5x4SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_5X5_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC5x5UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_5X5_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC5x5SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_6X5_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC6x5UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_6X5_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC6x5SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_6X6_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC6x6UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_6X6_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC6x6SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_8X5_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC8x5UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_8X5_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC8x5SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_8X6_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC8x6UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_8X6_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC8x6SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_8X8_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC8x8UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_8X8_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC8x8SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X5_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC10x5UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X5_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC10x5SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X6_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC10x6UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X6_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC10x6SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X8_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC10x8UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X8_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC10x8SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X10_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC10x10UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X10_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC10x10SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_12X10_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC12x10UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_12X10_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC12x10SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_12X12_UNORM_BLOCK,
            ash::vk::Format::from(Format::ASTC12x12UNormBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_12X12_SRGB_BLOCK,
            ash::vk::Format::from(Format::ASTC12x12SrgbBlock)
        );
        assert_eq!(
            ash::vk::Format::G8B8G8R8_422_UNORM,
            ash::vk::Format::from(Format::G8B8G8R8422UNorm)
        );
        assert_eq!(
            ash::vk::Format::B8G8R8G8_422_UNORM,
            ash::vk::Format::from(Format::B8G8R8G8422UNorm)
        );
        assert_eq!(
            ash::vk::Format::G8_B8_R8_3PLANE_420_UNORM,
            ash::vk::Format::from(Format::G8B8R83Plane420UNorm)
        );
        assert_eq!(
            ash::vk::Format::G8_B8R8_2PLANE_420_UNORM,
            ash::vk::Format::from(Format::G8B8R82Plane420UNorm)
        );
        assert_eq!(
            ash::vk::Format::G8_B8_R8_3PLANE_422_UNORM,
            ash::vk::Format::from(Format::G8B8R83Plane422UNorm)
        );
        assert_eq!(
            ash::vk::Format::G8_B8R8_2PLANE_422_UNORM,
            ash::vk::Format::from(Format::G8B8R82Plane422UNorm)
        );
        assert_eq!(
            ash::vk::Format::G8_B8_R8_3PLANE_444_UNORM,
            ash::vk::Format::from(Format::G8B8R83Plane444UNorm)
        );
        assert_eq!(
            ash::vk::Format::R10X6_UNORM_PACK16,
            ash::vk::Format::from(Format::R10X6UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::R10X6G10X6_UNORM_2PACK16,
            ash::vk::Format::from(Format::R10X6G10X6UNorm2Pack16)
        );
        assert_eq!(
            ash::vk::Format::R10X6G10X6B10X6A10X6_UNORM_4PACK16,
            ash::vk::Format::from(Format::R10X6G10X6B10X6A10X6UNorm4Pack16)
        );
        assert_eq!(
            ash::vk::Format::G10X6B10X6G10X6R10X6_422_UNORM_4PACK16,
            ash::vk::Format::from(Format::G10X6B10X6G10X6R10X6422UNorm4Pack16)
        );
        assert_eq!(
            ash::vk::Format::B10X6G10X6R10X6G10X6_422_UNORM_4PACK16,
            ash::vk::Format::from(Format::B10X6G10X6R10X6G10X6422UNorm4Pack16)
        );
        assert_eq!(
            ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16,
            ash::vk::Format::from(Format::G10X6B10X6R10X63Plane420UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16,
            ash::vk::Format::from(Format::G10X6B10X6R10X62Plane420UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16,
            ash::vk::Format::from(Format::G10X6B10X6R10X63Plane422UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16,
            ash::vk::Format::from(Format::G10X6B10X6R10X62Plane422UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16,
            ash::vk::Format::from(Format::G10X6B10X6R10X63Plane444UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::R12X4_UNORM_PACK16,
            ash::vk::Format::from(Format::R12X4UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::R12X4G12X4_UNORM_2PACK16,
            ash::vk::Format::from(Format::R12X4G12X4UNorm2Pack16)
        );
        assert_eq!(
            ash::vk::Format::R12X4G12X4B12X4A12X4_UNORM_4PACK16,
            ash::vk::Format::from(Format::R12X4G12X4B12X4A12X4UNorm4Pack16)
        );
        assert_eq!(
            ash::vk::Format::G12X4B12X4G12X4R12X4_422_UNORM_4PACK16,
            ash::vk::Format::from(Format::G12X4B12X4G12X4R12X4422UNorm4Pack16)
        );
        assert_eq!(
            ash::vk::Format::B12X4G12X4R12X4G12X4_422_UNORM_4PACK16,
            ash::vk::Format::from(Format::B12X4G12X4R12X4G12X4422UNorm4Pack16)
        );
        assert_eq!(
            ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16,
            ash::vk::Format::from(Format::G12X4B12X4R12X43Plane420UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16,
            ash::vk::Format::from(Format::G12X4B12X4R12X42Plane420UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16,
            ash::vk::Format::from(Format::G12X4B12X4R12X43Plane422UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16,
            ash::vk::Format::from(Format::G12X4B12X4R12X42Plane422UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16,
            ash::vk::Format::from(Format::G12X4B12X4R12X43Plane444UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G16B16G16R16_422_UNORM,
            ash::vk::Format::from(Format::G16B16G16R16422UNorm)
        );
        assert_eq!(
            ash::vk::Format::B16G16R16G16_422_UNORM,
            ash::vk::Format::from(Format::B16G16R16G16422UNorm)
        );
        assert_eq!(
            ash::vk::Format::G16_B16_R16_3PLANE_420_UNORM,
            ash::vk::Format::from(Format::G16B16R163Plane420UNorm)
        );
        assert_eq!(
            ash::vk::Format::G16_B16R16_2PLANE_420_UNORM,
            ash::vk::Format::from(Format::G16B16R162Plane420UNorm)
        );
        assert_eq!(
            ash::vk::Format::G16_B16_R16_3PLANE_422_UNORM,
            ash::vk::Format::from(Format::G16B16R163Plane422UNorm)
        );
        assert_eq!(
            ash::vk::Format::G16_B16R16_2PLANE_422_UNORM,
            ash::vk::Format::from(Format::G16B16R162Plane422UNorm)
        );
        assert_eq!(
            ash::vk::Format::G16_B16_R16_3PLANE_444_UNORM,
            ash::vk::Format::from(Format::G16B16R163Plane444UNorm)
        );
        assert_eq!(
            ash::vk::Format::G8_B8R8_2PLANE_444_UNORM,
            ash::vk::Format::from(Format::G8B8R82Plane444UNorm)
        );
        assert_eq!(
            ash::vk::Format::G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16,
            ash::vk::Format::from(Format::G10X6B10X6R10X62Plane444UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16,
            ash::vk::Format::from(Format::G12X4B12X4R12X42Plane444UNorm3Pack16)
        );
        assert_eq!(
            ash::vk::Format::G16_B16R16_2PLANE_444_UNORM,
            ash::vk::Format::from(Format::G16B16R162Plane444UNorm)
        );
        assert_eq!(
            ash::vk::Format::A4R4G4B4_UNORM_PACK16,
            ash::vk::Format::from(Format::A4R4G4B4UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::A4B4G4R4_UNORM_PACK16,
            ash::vk::Format::from(Format::A4B4G4R4UNormPack16)
        );
        assert_eq!(
            ash::vk::Format::ASTC_4X4_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC4x4SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_5X4_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC5x4SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_5X5_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC5x5SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_6X5_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC6x5SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_6X6_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC6x6SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_8X6_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC8x5SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_8X6_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC8x6SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_8X8_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC8x8SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X5_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC10x5SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X6_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC10x6SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X8_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC10x8SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_10X10_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC10x10SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_12X10_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC12x10SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::ASTC_12X12_SFLOAT_BLOCK,
            ash::vk::Format::from(Format::ASTC12x12SFloatBlock)
        );
        assert_eq!(
            ash::vk::Format::PVRTC1_2BPP_UNORM_BLOCK_IMG,
            ash::vk::Format::from(Format::PVRTC12BPPUNormBlockImg)
        );
        assert_eq!(
            ash::vk::Format::PVRTC1_4BPP_UNORM_BLOCK_IMG,
            ash::vk::Format::from(Format::PVRTC14BPPUNormBlockImg)
        );
        assert_eq!(
            ash::vk::Format::PVRTC2_2BPP_UNORM_BLOCK_IMG,
            ash::vk::Format::from(Format::PVRTC22BPPUNormBlockImg)
        );
        assert_eq!(
            ash::vk::Format::PVRTC2_4BPP_UNORM_BLOCK_IMG,
            ash::vk::Format::from(Format::PVRTC24BPPUNormBlockImg)
        );
        assert_eq!(
            ash::vk::Format::PVRTC1_2BPP_SRGB_BLOCK_IMG,
            ash::vk::Format::from(Format::PVRTC12BPPSrgbBlockImg)
        );
        assert_eq!(
            ash::vk::Format::PVRTC1_4BPP_SRGB_BLOCK_IMG,
            ash::vk::Format::from(Format::PVRTC14BPPSrgbBlockImg)
        );
        assert_eq!(
            ash::vk::Format::PVRTC2_2BPP_SRGB_BLOCK_IMG,
            ash::vk::Format::from(Format::PVRTC22BPPSrgbBlockImg)
        );
        assert_eq!(
            ash::vk::Format::PVRTC2_4BPP_SRGB_BLOCK_IMG,
            ash::vk::Format::from(Format::PVRTC24BPPSrgbBlockImg)
        );
    }
}
