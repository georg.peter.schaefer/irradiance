use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{
    Format, ImageCreateFlags, ImageLayout, ImageTiling, ImageType, ImageUsage, SampleCount,
};
use crate::gfx::memory::{Allocated, Memory, MemoryLocation, MemoryRequirements};
use crate::gfx::sync::SharingMode;
use crate::gfx::{Extent3D, GfxError, Handle, ObjectType};

/// Images represent multidimensional - up to 3 - arrays of data which can be used for various
/// purposes (e.g. attachments, textures), by binding them to a graphics or compute pipeline via
/// descriptor sets, or by directly specifying them as parameters to certain commands.
#[derive(Debug)]
pub struct Image {
    sharing_mode: SharingMode,
    usage: ImageUsage,
    tiling: ImageTiling,
    samples: SampleCount,
    array_layers: u32,
    mip_levels: u32,
    extent: Extent3D,
    format: Format,
    image_type: ImageType,
    image_create_flags: ImageCreateFlags,
    allocated: Option<Allocated>,
    swapchain_owned: bool,
    handle: ash::vk::Image,
    device: Arc<Device>,
}

impl Image {
    pub(crate) fn from_handle(
        device: Arc<Device>,
        handle: ash::vk::Image,
        format: Format,
        extent: Extent3D,
        array_layers: u32,
        usage: ImageUsage,
        sharing_mode: SharingMode,
    ) -> Arc<Self> {
        Arc::new(Self {
            sharing_mode,
            usage,
            tiling: ImageTiling::Optimal,
            samples: SampleCount::Sample1,
            array_layers,
            mip_levels: 1,
            extent,
            format,
            image_type: ImageType::Type2D,
            image_create_flags: Default::default(),
            allocated: None,
            swapchain_owned: true,
            handle,
            device,
        })
    }

    /// Starts building a new image.
    pub fn builder(device: Arc<Device>) -> ImageBuilder {
        ImageBuilder {
            initial_layout: ImageLayout::Undefined,
            sharing_mode: SharingMode::Exclusive,
            usage: Default::default(),
            tiling: ImageTiling::Optimal,
            samples: SampleCount::Sample1,
            array_layers: 1,
            mip_levels: 1,
            extent: Default::default(),
            format: Format::Undefined,
            image_type: ImageType::Type2D,
            image_create_flags: Default::default(),
            exported: false,
            device,
        }
    }

    /// Starts building an image with exported memory.
    pub fn exported(device: Arc<Device>) -> ImageBuilder {
        ImageBuilder {
            initial_layout: ImageLayout::Undefined,
            sharing_mode: SharingMode::Exclusive,
            usage: Default::default(),
            tiling: ImageTiling::Optimal,
            samples: SampleCount::Sample1,
            array_layers: 1,
            mip_levels: 1,
            extent: Default::default(),
            format: Format::Undefined,
            image_type: ImageType::Type2D,
            image_create_flags: Default::default(),
            exported: true,
            device,
        }
    }

    /// Returns the image create flags.
    pub fn image_create_flags(&self) -> ImageCreateFlags {
        self.image_create_flags
    }

    /// Returns the image type.
    pub fn image_type(&self) -> ImageType {
        self.image_type
    }

    /// Returns the image format.
    pub fn format(&self) -> Format {
        self.format
    }

    /// Returns the image extent.
    pub fn extent(&self) -> Extent3D {
        self.extent
    }

    /// Returns the mip levels.
    pub fn mip_levels(&self) -> u32 {
        self.mip_levels
    }

    /// Returns the array layers.
    pub fn array_layers(&self) -> u32 {
        self.array_layers
    }

    /// Returns the samples.
    pub fn samples(&self) -> SampleCount {
        self.samples
    }

    /// Returns the tiling.
    pub fn tiling(&self) -> ImageTiling {
        self.tiling
    }

    /// Returns the image usage.
    pub fn usage(&self) -> ImageUsage {
        self.usage
    }

    /// Returns the sharing mode.
    pub fn sharing_mode(&self) -> SharingMode {
        self.sharing_mode.clone()
    }

    /// Returns the memory.
    pub fn memory(&self) -> Option<Arc<Memory>> {
        match &self.allocated {
            None => None,
            Some(Allocated::Managed(allocation)) => Some(allocation.memory()),
            Some(Allocated::Manual(memory)) => Some(memory.clone()),
        }
    }
}

impl Drop for Image {
    fn drop(&mut self) {
        if !self.swapchain_owned {
            unsafe { self.device.inner().destroy_image(self.handle, None) };
        }
    }
}

#[doc(hidden)]
impl Handle for Image {
    type Type = ash::vk::Image;

    fn object_type(&self) -> ObjectType {
        ObjectType::Image
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for Image {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building an [`Image`].
#[derive(Debug)]
pub struct ImageBuilder {
    initial_layout: ImageLayout,
    sharing_mode: SharingMode,
    usage: ImageUsage,
    tiling: ImageTiling,
    samples: SampleCount,
    array_layers: u32,
    mip_levels: u32,
    extent: Extent3D,
    format: Format,
    image_type: ImageType,
    image_create_flags: ImageCreateFlags,
    exported: bool,
    device: Arc<Device>,
}

impl ImageBuilder {
    /// Sets the [`ImageCreateFlags`].
    pub fn image_create_flags(&mut self, image_create_flags: ImageCreateFlags) -> &mut Self {
        self.image_create_flags = image_create_flags;
        self
    }

    /// Sets the image type.
    pub fn image_type(&mut self, image_type: ImageType) -> &mut Self {
        self.image_type = image_type;
        self
    }

    /// Sets the format.
    pub fn format(&mut self, format: Format) -> &mut Self {
        self.format = format;
        self
    }

    /// Sets the extent.
    pub fn extent(&mut self, extent: Extent3D) -> &mut Self {
        self.extent = extent;
        self
    }

    /// Sets the mip levels.
    pub fn mip_levels(&mut self, mip_levels: u32) -> &mut Self {
        self.mip_levels = mip_levels;
        self
    }

    /// Sets the array layers.
    pub fn array_layers(&mut self, array_levels: u32) -> &mut Self {
        self.array_layers = array_levels;
        self
    }

    /// Sets the samples.
    pub fn samples(&mut self, samples: SampleCount) -> &mut Self {
        self.samples = samples;
        self
    }

    /// Sets the tiling mode.
    pub fn tiling(&mut self, tiling: ImageTiling) -> &mut Self {
        self.tiling = tiling;
        self
    }

    /// Sets the image usage.
    pub fn usage(&mut self, usage: ImageUsage) -> &mut Self {
        self.usage = usage;
        self
    }

    /// Sets the sharing mode.
    pub fn sharing_mode(&mut self, sharing_mode: SharingMode) -> &mut Self {
        self.sharing_mode = sharing_mode;
        self
    }

    /// Sets the initial layout.
    pub fn initial_layout(&mut self, initial_layout: ImageLayout) -> &mut Self {
        self.initial_layout = initial_layout;
        self
    }

    /// Builds the [`Image`].
    ///
    /// # Errors
    /// This function returns an error if the image creation fails.
    pub fn build(&mut self) -> Result<Arc<Image>, GfxError> {
        #[cfg(target_os = "linux")]
        let mut external_memory_create_info = ash::vk::ExternalMemoryImageCreateInfo::builder()
            .handle_types(ash::vk::ExternalMemoryHandleTypeFlags::OPAQUE_FD)
            .build();
        #[cfg(target_os = "windows")]
        let mut external_memory_create_info = ash::vk::ExternalMemoryImageCreateInfo::builder()
            .handle_types(ash::vk::ExternalMemoryHandleTypeFlags::OPAQUE_WIN32)
            .build();
        let mut create_info_builder = ash::vk::ImageCreateInfo::builder()
            .flags(self.image_create_flags.into())
            .image_type(self.image_type.into())
            .format(self.format.into())
            .extent(self.extent.into())
            .mip_levels(self.mip_levels)
            .array_layers(self.array_layers)
            .samples(self.samples.into())
            .tiling(self.tiling.into())
            .usage(self.usage.into())
            .sharing_mode(self.sharing_mode.clone().into())
            .queue_family_indices(self.sharing_mode.as_ref())
            .initial_layout(self.initial_layout.into());

        if self.exported {
            create_info_builder = create_info_builder.push_next(&mut external_memory_create_info);
        }

        let create_info = create_info_builder.build();

        let handle = unsafe { self.device.inner().create_image(&create_info, None) }?;

        let memory_requirements = MemoryRequirements::from(unsafe {
            self.device.inner().get_image_memory_requirements(handle)
        });
        let allocated = if self.exported {
            Allocated::Manual(
                Memory::builder(self.device.clone(), memory_requirements)
                    .exported()
                    .build()?,
            )
        } else {
            Allocated::Managed(self.device.allocator().allocate(
                self.device.clone(),
                "image",
                memory_requirements,
                MemoryLocation::GpuOnly,
                true,
            )?)
        };

        unsafe {
            self.device.inner().bind_image_memory(
                handle,
                allocated.memory().handle(),
                allocated.offset(),
            )?;
        }

        Ok(Arc::new(Image {
            sharing_mode: self.sharing_mode.clone(),
            usage: self.usage,
            tiling: self.tiling,
            samples: self.samples,
            array_layers: self.array_layers,
            mip_levels: self.mip_levels,
            extent: self.extent,
            format: self.format,
            image_type: self.image_type,
            image_create_flags: self.image_create_flags,
            allocated: Some(allocated),
            swapchain_owned: false,
            handle,
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::image::{
        Format, Image, ImageLayout, ImageTiling, ImageType, ImageUsage, SampleCount,
    };
    use crate::gfx::sync::SharingMode;
    use crate::gfx::Extent3D;
    use crate::physical_device;

    #[test]
    fn build_image() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();

        let result = Image::builder(device.clone())
            .image_type(ImageType::Type2D)
            .format(Format::R8G8B8A8Srgb)
            .extent(Extent3D {
                width: 1024,
                height: 1024,
                depth: 1,
            })
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCount::Sample1)
            .tiling(ImageTiling::Optimal)
            .usage(ImageUsage {
                sampled: true,
                transfer_dst: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .initial_layout(ImageLayout::Undefined)
            .build();

        assert!(result.is_ok());

        let image = result.unwrap();

        assert_eq!(ImageType::Type2D, image.image_type());
        assert_eq!(Format::R8G8B8A8Srgb, image.format());
        assert_eq!(
            Extent3D {
                width: 1024,
                height: 1024,
                depth: 1
            },
            image.extent()
        );
        assert_eq!(1, image.mip_levels());
        assert_eq!(1, image.array_layers());
        assert_eq!(SampleCount::Sample1, image.samples());
        assert_eq!(ImageTiling::Optimal, image.tiling());
        assert_eq!(
            ImageUsage {
                sampled: true,
                transfer_dst: true,
                ..Default::default()
            },
            image.usage()
        );
        assert_eq!(SharingMode::Exclusive, image.sharing_mode());
    }
}
