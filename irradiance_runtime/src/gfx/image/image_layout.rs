/// Image layouts.
#[allow(missing_docs)]
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum ImageLayout {
    Undefined,
    General,
    ColorAttachmentOptimal,
    DepthStencilAttachmentOptimal,
    DepthStencilReadOnlyOptimal,
    ShaderReadOnlyOptimal,
    TransferSrcOptimal,
    TransferDstOptimal,
    PreInitialized,
    DepthReadOnlyStencilAttachmentOptimal,
    DepthAttachmentStencilReadOnlyOptimal,
    DepthAttachmentOptimal,
    DepthReadOnlyOptimal,
    StencilAttachmentOptimal,
    StencilReadOnlyOptimal,
    ReadOnlyOptimal,
    AttachmentOptimal,
    PresentSrc,
    VideoDecodeDst,
    VideoDecodeSrc,
    VideoDecodeDpb,
    SharedPresent,
    FragmentDensityMapOptimal,
    FragmentShadingRateAttachmentOptimal,
    VideoEncodeDst,
    VideoEncodeSrc,
    VideoEncodeDpb,
}

#[doc(hidden)]
impl From<ImageLayout> for ash::vk::ImageLayout {
    fn from(image_layout: ImageLayout) -> Self {
        match image_layout {
            ImageLayout::Undefined => ash::vk::ImageLayout::UNDEFINED,
            ImageLayout::General => ash::vk::ImageLayout::GENERAL,
            ImageLayout::ColorAttachmentOptimal => ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
            ImageLayout::DepthStencilAttachmentOptimal => {
                ash::vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL
            }
            ImageLayout::DepthStencilReadOnlyOptimal => {
                ash::vk::ImageLayout::DEPTH_STENCIL_READ_ONLY_OPTIMAL
            }
            ImageLayout::ShaderReadOnlyOptimal => ash::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            ImageLayout::TransferSrcOptimal => ash::vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
            ImageLayout::TransferDstOptimal => ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            ImageLayout::PreInitialized => ash::vk::ImageLayout::PREINITIALIZED,
            ImageLayout::DepthReadOnlyStencilAttachmentOptimal => {
                ash::vk::ImageLayout::DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL
            }
            ImageLayout::DepthAttachmentStencilReadOnlyOptimal => {
                ash::vk::ImageLayout::DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL
            }
            ImageLayout::DepthAttachmentOptimal => ash::vk::ImageLayout::DEPTH_ATTACHMENT_OPTIMAL,
            ImageLayout::DepthReadOnlyOptimal => ash::vk::ImageLayout::DEPTH_READ_ONLY_OPTIMAL,
            ImageLayout::StencilAttachmentOptimal => {
                ash::vk::ImageLayout::STENCIL_ATTACHMENT_OPTIMAL
            }
            ImageLayout::StencilReadOnlyOptimal => ash::vk::ImageLayout::STENCIL_READ_ONLY_OPTIMAL,
            ImageLayout::ReadOnlyOptimal => ash::vk::ImageLayout::READ_ONLY_OPTIMAL,
            ImageLayout::AttachmentOptimal => ash::vk::ImageLayout::ATTACHMENT_OPTIMAL,
            ImageLayout::PresentSrc => ash::vk::ImageLayout::PRESENT_SRC_KHR,
            ImageLayout::VideoDecodeDst => ash::vk::ImageLayout::VIDEO_DECODE_DST_KHR,
            ImageLayout::VideoDecodeSrc => ash::vk::ImageLayout::VIDEO_DECODE_SRC_KHR,
            ImageLayout::VideoDecodeDpb => ash::vk::ImageLayout::VIDEO_DECODE_DPB_KHR,
            ImageLayout::SharedPresent => ash::vk::ImageLayout::SHARED_PRESENT_KHR,
            ImageLayout::FragmentDensityMapOptimal => {
                ash::vk::ImageLayout::FRAGMENT_DENSITY_MAP_OPTIMAL_EXT
            }
            ImageLayout::FragmentShadingRateAttachmentOptimal => {
                ash::vk::ImageLayout::FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR
            }
            ImageLayout::VideoEncodeDst => ash::vk::ImageLayout::VIDEO_ENCODE_DST_KHR,
            ImageLayout::VideoEncodeSrc => ash::vk::ImageLayout::VIDEO_ENCODE_SRC_KHR,
            ImageLayout::VideoEncodeDpb => ash::vk::ImageLayout::VIDEO_ENCODE_DPB_KHR,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::image_layout::ImageLayout;

    #[test]
    fn to_image_layout() {
        assert_eq!(
            ash::vk::ImageLayout::UNDEFINED,
            ash::vk::ImageLayout::from(ImageLayout::Undefined)
        );
        assert_eq!(
            ash::vk::ImageLayout::GENERAL,
            ash::vk::ImageLayout::from(ImageLayout::General)
        );
        assert_eq!(
            ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::ColorAttachmentOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::DepthStencilAttachmentOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::DEPTH_STENCIL_READ_ONLY_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::DepthStencilReadOnlyOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::ShaderReadOnlyOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::TransferSrcOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::TransferDstOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::PREINITIALIZED,
            ash::vk::ImageLayout::from(ImageLayout::PreInitialized)
        );
        assert_eq!(
            ash::vk::ImageLayout::DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::DepthReadOnlyStencilAttachmentOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::DepthAttachmentStencilReadOnlyOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::DEPTH_ATTACHMENT_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::DepthAttachmentOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::DEPTH_READ_ONLY_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::DepthReadOnlyOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::STENCIL_ATTACHMENT_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::StencilAttachmentOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::STENCIL_READ_ONLY_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::StencilReadOnlyOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::READ_ONLY_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::ReadOnlyOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::ATTACHMENT_OPTIMAL,
            ash::vk::ImageLayout::from(ImageLayout::AttachmentOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::PRESENT_SRC_KHR,
            ash::vk::ImageLayout::from(ImageLayout::PresentSrc)
        );
        assert_eq!(
            ash::vk::ImageLayout::VIDEO_DECODE_DST_KHR,
            ash::vk::ImageLayout::from(ImageLayout::VideoDecodeDst)
        );
        assert_eq!(
            ash::vk::ImageLayout::VIDEO_DECODE_SRC_KHR,
            ash::vk::ImageLayout::from(ImageLayout::VideoDecodeSrc)
        );
        assert_eq!(
            ash::vk::ImageLayout::VIDEO_DECODE_DPB_KHR,
            ash::vk::ImageLayout::from(ImageLayout::VideoDecodeDpb)
        );
        assert_eq!(
            ash::vk::ImageLayout::SHARED_PRESENT_KHR,
            ash::vk::ImageLayout::from(ImageLayout::SharedPresent)
        );
        assert_eq!(
            ash::vk::ImageLayout::FRAGMENT_DENSITY_MAP_OPTIMAL_EXT,
            ash::vk::ImageLayout::from(ImageLayout::FragmentDensityMapOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR,
            ash::vk::ImageLayout::from(ImageLayout::FragmentShadingRateAttachmentOptimal)
        );
        assert_eq!(
            ash::vk::ImageLayout::VIDEO_ENCODE_DST_KHR,
            ash::vk::ImageLayout::from(ImageLayout::VideoEncodeDst)
        );
        assert_eq!(
            ash::vk::ImageLayout::VIDEO_ENCODE_SRC_KHR,
            ash::vk::ImageLayout::from(ImageLayout::VideoEncodeSrc)
        );
        assert_eq!(
            ash::vk::ImageLayout::VIDEO_ENCODE_DPB_KHR,
            ash::vk::ImageLayout::from(ImageLayout::VideoEncodeDpb)
        );
    }
}
