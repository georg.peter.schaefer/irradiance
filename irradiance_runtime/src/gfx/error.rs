use ash::LoadingError;
use gpu_allocator::AllocationError;
use std::error::Error;
use std::fmt::{Display, Formatter};

/// Errors which can occur when using the gfx api.
#[derive(Debug)]
pub enum GfxError {
    /// Loading the graphics API has failed.
    LoadingFailed(LoadingError),
    /// No suitable physical device found.
    NoSuitablePhysicalDevice,
    /// A host memory allocation has failed.
    OutOfHostMemory,
    /// A device memory allocation has failed.
    OutOfDeviceMemory,
    /// Initialization of an object could not be completed for implementation-specific reasons.
    InitializationFailed,
    /// The logical or physical device has been lost.
    DeviceLost,
    /// Mapping of a memory object has failed.
    MemoryMapFailed,
    /// A requested layer is not present or could not be loaded.
    LayerNotPresent,
    /// A requested extension is not supported.
    ExtensionNotPresent,
    /// A requested feature is not supported.
    FeatureNotPresent,
    /// The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation-specific reasons.
    IncompatibleDriver,
    /// Too many objects of the type have already been created.
    TooManyObjects,
    /// A requested format is not supported on this device.
    FormatNotSupported,
    /// A pool allocation has failed due to fragmentation of the pool’s memory.
    FragmentedPool,
    /// An unknown error has occurred; either the application has provided invalid input, or an implementation failure has occurred.
    Unknown,
    /// A pool memory allocation has failed.
    OutOfPoolMemory,
    /// An external handle is not a valid handle of the specified type.
    InvalidExternalHandle,
    /// A descriptor pool creation has failed due to fragmentation.
    Fragmentation,
    /// A buffer creation failed because the requested address is not available.
    InvalidOpaqueCaptureAddress,
    /// A surface is no longer available.
    SurfaceLost,
    /// The requested window is already in use by Vulkan or another API in a manner which prevents it from being used again.
    NativeWindowInUse,
    /// A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail.
    OutOfDate,
    /// The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image.
    IncompatibleDisplay,
    /// VK_ERROR_VALIDATION_FAILED_EXT.
    ValidationFailed,
    /// One or more shaders failed to compile or link.
    InvalidShader,
    /// VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT.
    InvalidDrmFormatModifierPlaneLayout,
    /// VK_ERROR_NOT_PERMITTED_EXT.
    NotPermitted,
    /// An operation on a swapchain created with VK_FULL_SCREEN_EXCLUSIVE_APPLICATION_CONTROLLED_EXT failed as it did not have exclusive full-screen access.
    FullScreenExclusiveModeLost,
    /// A device memory allocation has failed.
    AllocationFailed(AllocationError),
}

impl Display for GfxError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            GfxError::LoadingFailed(_) => write!(f, "Loading the graphics API has failed."),
            GfxError::NoSuitablePhysicalDevice => write!(f, "No suitable physical device found"),
            GfxError::OutOfHostMemory => write!(f, "A host memory allocation has failed"),
            GfxError::OutOfDeviceMemory => write!(f, "A device memory allocation has failed"),
            GfxError::InitializationFailed => write!(f, "Initialization of an object could not be completed for implementation-specific reasons"),
            GfxError::DeviceLost => write!(f, "The logical or physical device has been lost"),
            GfxError::MemoryMapFailed => write!(f, "Mapping of a memory object has failed"),
            GfxError::LayerNotPresent => write!(f, "A requested layer is not present or could not be loaded"),
            GfxError::ExtensionNotPresent => write!(f, "A requested extension is not supported"),
            GfxError::FeatureNotPresent => write!(f, "A requested feature is not supported"),
            GfxError::IncompatibleDriver => write!(f, "The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation-specific reasons"),
            GfxError::TooManyObjects => write!(f, "Too many objects of the type have already been created"),
            GfxError::FormatNotSupported => write!(f, "A requested format is not supported on this device"),
            GfxError::FragmentedPool => write!(f, "A pool allocation has failed due to fragmentation of the pool’s memory"),
            GfxError::Unknown => write!(f, "An unknown error has occurred; either the application has provided invalid input, or an implementation failure has occurred"),
            GfxError::OutOfPoolMemory => write!(f, "A pool memory allocation has failed"),
            GfxError::InvalidExternalHandle => write!(f, "An external handle is not a valid handle of the specified type"),
            GfxError::Fragmentation => write!(f, "A descriptor pool creation has failed due to fragmentation"),
            GfxError::InvalidOpaqueCaptureAddress => write!(f, "A buffer creation failed because the requested address is not available"),
            GfxError::SurfaceLost => write!(f, "A surface is no longer available"),
            GfxError::NativeWindowInUse => write!(f, "The requested window is already in use by Vulkan or another API in a manner which prevents it from being used again"),
            GfxError::OutOfDate => write!(f, "A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail"),
            GfxError::IncompatibleDisplay => write!(f, "The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image"),
            GfxError::ValidationFailed => write!(f, "VK_ERROR_VALIDATION_FAILED_EXT"),
            GfxError::InvalidShader => write!(f, "One or more shaders failed to compile or link"),
            GfxError::InvalidDrmFormatModifierPlaneLayout => write!(f, "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT"),
            GfxError::NotPermitted => write!(f, "VK_ERROR_NOT_PERMITTED_EXT"),
            GfxError::FullScreenExclusiveModeLost => write!(f, "An operation on a swapchain created with VK_FULL_SCREEN_EXCLUSIVE_APPLICATION_CONTROLLED_EXT failed as it did not have exclusive full-screen access"),
            GfxError::AllocationFailed(_) => write!(f, "A device memory allocation has failed"),
        }
    }
}

impl std::error::Error for GfxError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            GfxError::LoadingFailed(e) => Some(e),
            GfxError::AllocationFailed(e) => Some(e),
            _ => None,
        }
    }
}

#[doc(hidden)]
impl From<ash::vk::Result> for GfxError {
    fn from(result: ash::vk::Result) -> Self {
        match result {
            ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => Self::OutOfHostMemory,
            ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => Self::OutOfDeviceMemory,
            ash::vk::Result::ERROR_INITIALIZATION_FAILED => Self::InitializationFailed,
            ash::vk::Result::ERROR_DEVICE_LOST => Self::DeviceLost,
            ash::vk::Result::ERROR_MEMORY_MAP_FAILED => Self::MemoryMapFailed,
            ash::vk::Result::ERROR_LAYER_NOT_PRESENT => Self::LayerNotPresent,
            ash::vk::Result::ERROR_EXTENSION_NOT_PRESENT => Self::ExtensionNotPresent,
            ash::vk::Result::ERROR_FEATURE_NOT_PRESENT => Self::FeatureNotPresent,
            ash::vk::Result::ERROR_INCOMPATIBLE_DRIVER => Self::IncompatibleDriver,
            ash::vk::Result::ERROR_TOO_MANY_OBJECTS => Self::TooManyObjects,
            ash::vk::Result::ERROR_FORMAT_NOT_SUPPORTED => Self::FormatNotSupported,
            ash::vk::Result::ERROR_FRAGMENTED_POOL => Self::FragmentedPool,
            ash::vk::Result::ERROR_UNKNOWN => Self::Unknown,
            ash::vk::Result::ERROR_OUT_OF_POOL_MEMORY => Self::OutOfPoolMemory,
            ash::vk::Result::ERROR_INVALID_EXTERNAL_HANDLE => Self::InvalidExternalHandle,
            ash::vk::Result::ERROR_FRAGMENTATION => Self::Fragmentation,
            ash::vk::Result::ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS => {
                Self::InvalidOpaqueCaptureAddress
            }
            ash::vk::Result::ERROR_SURFACE_LOST_KHR => Self::SurfaceLost,
            ash::vk::Result::ERROR_NATIVE_WINDOW_IN_USE_KHR => Self::NativeWindowInUse,
            ash::vk::Result::ERROR_OUT_OF_DATE_KHR => Self::OutOfDate,
            ash::vk::Result::ERROR_INCOMPATIBLE_DISPLAY_KHR => Self::IncompatibleDisplay,
            ash::vk::Result::ERROR_VALIDATION_FAILED_EXT => Self::ValidationFailed,
            ash::vk::Result::ERROR_INVALID_SHADER_NV => Self::InvalidShader,
            ash::vk::Result::ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT => {
                Self::InvalidDrmFormatModifierPlaneLayout
            }
            ash::vk::Result::ERROR_NOT_PERMITTED_KHR => Self::NotPermitted,
            ash::vk::Result::ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT => {
                Self::FullScreenExclusiveModeLost
            }
            _ => unreachable!("Unrecognized 'VkResult'"),
        }
    }
}

#[doc(hidden)]
impl From<LoadingError> for GfxError {
    fn from(e: LoadingError) -> Self {
        Self::LoadingFailed(e)
    }
}

#[doc(hidden)]
impl From<AllocationError> for GfxError {
    fn from(e: AllocationError) -> Self {
        Self::AllocationFailed(e)
    }
}
