use std::cell::UnsafeCell;
use std::collections::HashMap;
use std::sync::{Arc, LockResult, Mutex, MutexGuard};

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::queue::Submit;
use crate::gfx::sync::{ExternallySynchronized, Fence, Semaphore};
use crate::gfx::wsi::Swapchain;
use crate::gfx::{GfxError, Handle, ObjectType};

/// A queue you can submit work to.
#[derive(Debug)]
pub struct Queue {
    submitted: UnsafeCell<Vec<(Submit, Arc<Fence>)>>,
    family_index: u32,
    mutex: Mutex<()>,
    handle: ash::vk::Queue,
    device: Arc<Device>,
}

impl Queue {
    pub(crate) fn new(device: Arc<Device>, handle: ash::vk::Queue, family_index: u32) -> Arc<Self> {
        Arc::new(Self {
            submitted: UnsafeCell::new(vec![]),
            family_index,
            mutex: Mutex::new(()),
            handle,
            device,
        })
    }

    /// Returns the queue family index.
    pub fn family_index(&self) -> u32 {
        self.family_index
    }

    /// Submits [`command buffers`](crate::gfx::command_buffer::CommandBuffer) for execution.
    ///
    /// # Errors
    /// This function returns an error if command buffer submission fails.
    pub fn submit(
        self: &Arc<Self>,
        submit: Submit,
        fence: Option<Arc<Fence>>,
    ) -> Result<(), GfxError> {
        let wait_semaphore_infos = submit
            .wait_semaphores
            .clone()
            .into_iter()
            .map(ash::vk::SemaphoreSubmitInfo::from)
            .collect::<Vec<_>>();
        let command_buffer_infos = submit
            .command_buffers
            .clone()
            .into_iter()
            .map(ash::vk::CommandBufferSubmitInfo::from)
            .collect::<Vec<_>>();
        let signal_semaphore_infos = submit
            .signal_semaphores
            .clone()
            .into_iter()
            .map(ash::vk::SemaphoreSubmitInfo::from)
            .collect::<Vec<_>>();
        let submit_info = ash::vk::SubmitInfo2::builder()
            .wait_semaphore_infos(&wait_semaphore_infos)
            .command_buffer_infos(&command_buffer_infos)
            .signal_semaphore_infos(&signal_semaphore_infos)
            .build();
        let fence = match fence {
            Some(fence) => {
                fence.set_queue(Arc::downgrade(self));
                fence
            }
            None => Fence::builder(self.device.clone()).build()?,
        };

        let _queue_lock = self.lock().unwrap();

        self.push_pending(submit, fence.clone());

        let synchronization2 = ash::extensions::khr::Synchronization2::new(
            self.device.physical_device().instance().inner(),
            self.device.inner(),
        );

        unsafe {
            synchronization2.queue_submit2(self.handle, &[submit_info], fence.handle())?;
        }

        self.remove_executed()?;

        Ok(())
    }

    fn push_pending(&self, submit: Submit, fence: Arc<Fence>) {
        unsafe { &mut *self.submitted.get() }.push((submit, fence))
    }

    fn remove_executed(&self) -> Result<(), GfxError> {
        let mut fence_status = HashMap::new();
        for (_, fence) in unsafe { &mut *self.submitted.get() } {
            fence_status.insert(fence.handle(), fence.is_signaled()?);
        }
        unsafe { &mut *self.submitted.get() }
            .retain(|(_, fence)| !*fence_status.get(&fence.handle()).unwrap());

        Ok(())
    }

    pub(crate) fn remove_fence(&self, fence_to_remove: &Fence) {
        let _guard = self.lock().unwrap();

        unsafe { &mut *self.submitted.get() }
            .retain(|(_, fence)| fence.handle() != fence_to_remove.handle());
    }

    /// Presents an image of a [`Swapchain`].
    ///
    /// # Errors
    /// This function returns an error if presenting the image fails.
    pub fn present(
        &self,
        wait_semaphores: Vec<&Semaphore>,
        swapchain: &Swapchain,
        image_index: u32,
    ) -> Result<(), GfxError> {
        let wait_semaphores = wait_semaphores
            .into_iter()
            .map(Semaphore::handle)
            .collect::<Vec<_>>();
        let swapchains = [swapchain.handle()];
        let image_indices = [image_index];
        let present_info = ash::vk::PresentInfoKHR::builder()
            .wait_semaphores(&wait_semaphores)
            .swapchains(&swapchains)
            .image_indices(&image_indices)
            .build();

        unsafe {
            swapchain
                .loader()
                .queue_present(self.handle, &present_info)?;
        }

        Ok(())
    }
}

#[doc(hidden)]
impl Handle for Queue {
    type Type = ash::vk::Queue;

    fn object_type(&self) -> ObjectType {
        ObjectType::Queue
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl ExternallySynchronized for Queue {
    fn lock(&self) -> LockResult<MutexGuard<()>> {
        self.mutex.lock()
    }
}

impl DeviceOwned for Queue {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::command_buffer::{CommandBuffer, CommandPool};
    use crate::gfx::device::Device;
    use crate::gfx::queue::{CommandBufferSubmit, Submit};
    use crate::gfx::sync::Fence;
    use crate::physical_device;

    #[test]
    fn submit_with_fence() {
        let physical_device = physical_device!();
        let (device, queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let queue = queues[0].clone();
        let command_pool = CommandPool::builder(device.clone())
            .queue_family_index(0)
            .build()
            .unwrap();
        let command_buffer = CommandBuffer::primary(device.clone(), command_pool.clone())
            .build()
            .unwrap();
        let fence = Fence::builder(device.clone()).build().unwrap();

        let result = queue.submit(
            Submit::builder()
                .command_buffer(CommandBufferSubmit::builder(command_buffer.clone()).build())
                .build(),
            Some(fence.clone()),
        );

        assert!(result.is_ok());

        fence.wait().unwrap();
    }

    #[test]
    fn submit() {
        let physical_device = physical_device!();
        let (device, queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let queue = queues[0].clone();
        let command_pool = CommandPool::builder(device.clone())
            .queue_family_index(0)
            .build()
            .unwrap();
        let command_buffer = CommandBuffer::primary(device.clone(), command_pool.clone())
            .build()
            .unwrap();

        let result = queue.submit(
            Submit::builder()
                .command_buffer(CommandBufferSubmit::builder(command_buffer.clone()).build())
                .build(),
            None,
        );

        assert!(result.is_ok());

        device.wait().unwrap();
    }
}
