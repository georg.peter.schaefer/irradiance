//! Queue for submitting work to a device.
//!
//! # Queue creation
//! Queues are created during [`Device`](super::device::Device) creation.

pub use private::Queue;
pub use queue_family_properties::QueueFamilyProperties;
pub use queue_family_properties::QueueFlags;
pub use submit::CommandBufferSubmit;
pub use submit::CommandBufferSubmitBuilder;
pub use submit::SemaphoreSubmit;
pub use submit::SemaphoreSubmitBuilder;
pub use submit::Submit;
pub use submit::SubmitBuilder;

mod private;
mod queue_family_properties;
mod submit;
