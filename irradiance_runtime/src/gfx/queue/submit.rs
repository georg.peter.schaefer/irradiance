use std::sync::Arc;

use crate::gfx::command_buffer::CommandBuffer;
use crate::gfx::sync::{PipelineStages, Semaphore};
use crate::gfx::Handle;

/// Queue submit operation.
#[derive(Clone, Debug)]
pub struct Submit {
    pub(crate) signal_semaphores: Vec<SemaphoreSubmit>,
    pub(crate) command_buffers: Vec<CommandBufferSubmit>,
    pub(crate) wait_semaphores: Vec<SemaphoreSubmit>,
}

impl Submit {
    /// Starts building a new [`Submit`].
    pub fn builder() -> SubmitBuilder {
        SubmitBuilder {
            signal_semaphores: vec![],
            command_buffers: vec![],
            wait_semaphores: vec![],
        }
    }
}

/// Type for building a queue submit operation.
#[derive(Debug)]
pub struct SubmitBuilder {
    signal_semaphores: Vec<SemaphoreSubmit>,
    command_buffers: Vec<CommandBufferSubmit>,
    wait_semaphores: Vec<SemaphoreSubmit>,
}

impl SubmitBuilder {
    /// Adds a [`Semaphore`] to wait on.
    pub fn wait_semaphore(&mut self, wait_semaphore: SemaphoreSubmit) -> &mut Self {
        self.wait_semaphores.push(wait_semaphore);
        self
    }

    /// Adds a [`CommandBuffer`] to submit.
    pub fn command_buffer(&mut self, command_buffer: CommandBufferSubmit) -> &mut Self {
        self.command_buffers.push(command_buffer);
        self
    }

    /// Adds [`Semaphore`] to signal.
    pub fn signal_semaphore(&mut self, signal_semaphore: SemaphoreSubmit) -> &mut Self {
        self.signal_semaphores.push(signal_semaphore);
        self
    }

    /// Builds the queue submit operation.
    pub fn build(&mut self) -> Submit {
        Submit {
            signal_semaphores: self.signal_semaphores.clone(),
            command_buffers: self.command_buffers.clone(),
            wait_semaphores: self.wait_semaphores.clone(),
        }
    }
}

/// Semaphore signal or wait operation.
#[derive(Clone, Debug)]
pub struct SemaphoreSubmit {
    stages: PipelineStages,
    semaphore: Arc<Semaphore>,
}

impl SemaphoreSubmit {
    /// Starts building a new [`Semaphore`] signal or wait operation.
    pub fn builder(semaphore: Arc<Semaphore>) -> SemaphoreSubmitBuilder {
        SemaphoreSubmitBuilder {
            stages: Default::default(),
            semaphore,
        }
    }
}

#[doc(hidden)]
impl From<SemaphoreSubmit> for ash::vk::SemaphoreSubmitInfo {
    fn from(semaphore_submit: SemaphoreSubmit) -> Self {
        ash::vk::SemaphoreSubmitInfo::builder()
            .semaphore(semaphore_submit.semaphore.handle())
            .stage_mask(semaphore_submit.stages.into())
            .build()
    }
}

/// Type for building a semaphore signal or wait operation.
#[derive(Debug)]
pub struct SemaphoreSubmitBuilder {
    stages: PipelineStages,
    semaphore: Arc<Semaphore>,
}

impl SemaphoreSubmitBuilder {
    /// Sets the [`PipelineStages`] to wait on or to signal at.
    pub fn stages(&mut self, stages: PipelineStages) -> &mut Self {
        self.stages = stages;
        self
    }

    /// Builds the semaphore signal or wait operation.
    pub fn build(&mut self) -> SemaphoreSubmit {
        SemaphoreSubmit {
            stages: self.stages,
            semaphore: self.semaphore.clone(),
        }
    }
}

/// A command buffer submission.
#[derive(Clone, Debug)]
pub struct CommandBufferSubmit {
    command_buffer: Arc<CommandBuffer>,
}

impl CommandBufferSubmit {
    /// Starts building a new command buffer submission.
    pub fn builder(command_buffer: Arc<CommandBuffer>) -> CommandBufferSubmitBuilder {
        CommandBufferSubmitBuilder { command_buffer }
    }
}

#[doc(hidden)]
impl From<CommandBufferSubmit> for ash::vk::CommandBufferSubmitInfo {
    fn from(command_buffer_submit: CommandBufferSubmit) -> Self {
        ash::vk::CommandBufferSubmitInfo::builder()
            .command_buffer(command_buffer_submit.command_buffer.handle())
            .build()
    }
}

/// Type for building a command buffer submission.
#[derive(Debug)]
pub struct CommandBufferSubmitBuilder {
    command_buffer: Arc<CommandBuffer>,
}

impl CommandBufferSubmitBuilder {
    /// Builds the command buffer submission.
    pub fn build(&mut self) -> CommandBufferSubmit {
        CommandBufferSubmit {
            command_buffer: self.command_buffer.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use ash::vk::PipelineStageFlags2;

    use crate::gfx::command_buffer::{CommandBuffer, CommandPool};
    use crate::gfx::device::Device;
    use crate::gfx::queue::{CommandBufferSubmit, SemaphoreSubmit, Submit};
    use crate::gfx::sync::{PipelineStages, Semaphore};
    use crate::gfx::Handle;
    use crate::physical_device;

    #[test]
    fn build_submit() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let command_pool = CommandPool::builder(device.clone())
            .queue_family_index(0)
            .build()
            .unwrap();
        let command_buffer = CommandBuffer::primary(device.clone(), command_pool.clone())
            .build()
            .unwrap();
        let semaphore = Semaphore::builder(device.clone()).build().unwrap();

        let submit = Submit::builder()
            .wait_semaphore(
                SemaphoreSubmit::builder(semaphore.clone())
                    .stages(PipelineStages {
                        top_of_pipe: true,
                        ..Default::default()
                    })
                    .build(),
            )
            .command_buffer(CommandBufferSubmit::builder(command_buffer.clone()).build())
            .signal_semaphore(
                SemaphoreSubmit::builder(semaphore.clone())
                    .stages(PipelineStages {
                        bottom_of_pipe: true,
                        ..Default::default()
                    })
                    .build(),
            )
            .build();

        assert_eq!(
            semaphore.handle(),
            submit.wait_semaphores[0].semaphore.handle()
        );
        assert_eq!(
            PipelineStages {
                top_of_pipe: true,
                ..Default::default()
            },
            submit.wait_semaphores[0].stages
        );
        assert_eq!(
            command_buffer.handle(),
            submit.command_buffers[0].command_buffer.handle()
        );
        assert_eq!(
            semaphore.handle(),
            submit.signal_semaphores[0].semaphore.handle()
        );
        assert_eq!(
            PipelineStages {
                bottom_of_pipe: true,
                ..Default::default()
            },
            submit.signal_semaphores[0].stages
        );
    }

    #[test]
    fn semaphore_submit_to_semaphore_submit_info() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let semaphore = Semaphore::builder(device.clone()).build().unwrap();

        let semaphore_submit_info = ash::vk::SemaphoreSubmitInfo::from(
            SemaphoreSubmit::builder(semaphore.clone())
                .stages(PipelineStages {
                    top_of_pipe: true,
                    ..Default::default()
                })
                .build(),
        );

        assert_eq!(semaphore.handle(), semaphore_submit_info.semaphore);
        assert_eq!(
            PipelineStageFlags2::TOP_OF_PIPE,
            semaphore_submit_info.stage_mask
        );
    }

    #[test]
    fn command_buffer_submit_to_command_buffer_info() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let command_pool = CommandPool::builder(device.clone())
            .queue_family_index(0)
            .build()
            .unwrap();
        let command_buffer = CommandBuffer::primary(device.clone(), command_pool.clone())
            .build()
            .unwrap();

        let command_buffer_submit_info = ash::vk::CommandBufferSubmitInfo::from(
            CommandBufferSubmit::builder(command_buffer.clone()).build(),
        );

        assert_eq!(
            command_buffer.handle(),
            command_buffer_submit_info.command_buffer
        );
    }
}
