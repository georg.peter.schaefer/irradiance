use crate::impl_flag_set;

/// The properties of a queues in a family.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct QueueFamilyProperties {
    /// A bitmask indicating the capabilities of queues in this queue family.
    pub queue_flags: QueueFlags,
}

impl From<ash::vk::QueueFamilyProperties2> for QueueFamilyProperties {
    fn from(properties: ash::vk::QueueFamilyProperties2) -> Self {
        Self {
            ..properties.queue_family_properties.into()
        }
    }
}

impl From<ash::vk::QueueFamilyProperties> for QueueFamilyProperties {
    fn from(properties: ash::vk::QueueFamilyProperties) -> Self {
        Self {
            queue_flags: properties.queue_flags.into(),
        }
    }
}

impl_flag_set! {
    QueueFlags,
    doc = "Queue flags.",
    (VK_QUEUE_GRAPHICS_BIT, graphics),
    (VK_QUEUE_COMPUTE_BIT, compute),
    (VK_QUEUE_TRANSFER_BIT, transfer),
    (VK_QUEUE_SPARSE_BINDING_BIT, sparse_binding)
}

#[doc(hidden)]
impl From<ash::vk::QueueFlags> for QueueFlags {
    fn from(flags: ash::vk::QueueFlags) -> Self {
        Self {
            graphics: flags & ash::vk::QueueFlags::GRAPHICS == ash::vk::QueueFlags::GRAPHICS,
            compute: flags & ash::vk::QueueFlags::COMPUTE == ash::vk::QueueFlags::COMPUTE,
            transfer: flags & ash::vk::QueueFlags::TRANSFER == ash::vk::QueueFlags::TRANSFER,
            sparse_binding: flags & ash::vk::QueueFlags::SPARSE_BINDING
                == ash::vk::QueueFlags::SPARSE_BINDING,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::queue::QueueFlags;

    #[test]
    fn default() {
        assert_eq!(
            QueueFlags {
                graphics: false,
                compute: false,
                transfer: false,
                sparse_binding: false
            },
            Default::default()
        );
    }

    #[test]
    fn none() {
        assert_eq!(QueueFlags::default(), QueueFlags::none());
    }

    #[test]
    fn is_not_a_subset() {
        let a = QueueFlags {
            graphics: true,
            transfer: true,
            ..Default::default()
        };
        let b = QueueFlags {
            graphics: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn is_subset() {
        let a = QueueFlags {
            graphics: true,
            transfer: true,
            ..Default::default()
        };
        let b = QueueFlags {
            graphics: true,
            ..Default::default()
        };
        assert!(b.is_subset(&a));
    }

    #[test]
    fn is_not_a_superset() {
        let a = QueueFlags {
            graphics: true,
            transfer: true,
            ..Default::default()
        };
        let b = QueueFlags {
            graphics: true,
            ..Default::default()
        };
        assert!(!b.is_superset(&a));
    }

    #[test]
    fn is_superset() {
        let a = QueueFlags {
            graphics: true,
            transfer: true,
            ..Default::default()
        };
        let b = QueueFlags {
            graphics: true,
            ..Default::default()
        };
        assert!(a.is_superset(&b));
    }

    #[test]
    fn difference() {
        let a = QueueFlags {
            graphics: true,
            transfer: true,
            ..Default::default()
        };
        let b = QueueFlags {
            graphics: true,
            ..Default::default()
        };
        assert_eq!(
            QueueFlags {
                transfer: true,
                ..Default::default()
            },
            a.difference(&b)
        );
    }

    #[test]
    fn intersection() {
        let a = QueueFlags {
            graphics: true,
            transfer: true,
            ..Default::default()
        };
        let b = QueueFlags {
            graphics: true,
            ..Default::default()
        };
        assert_eq!(
            QueueFlags {
                graphics: true,
                ..Default::default()
            },
            a.intersection(&b)
        );
    }

    #[test]
    fn union() {
        let a = QueueFlags {
            graphics: true,
            transfer: true,
            ..Default::default()
        };
        let b = QueueFlags {
            graphics: true,
            sparse_binding: true,
            ..Default::default()
        };
        assert_eq!(
            QueueFlags {
                graphics: true,
                transfer: true,
                sparse_binding: true,
                ..Default::default()
            },
            a.union(&b)
        );
    }
}
