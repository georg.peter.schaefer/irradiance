use std::sync::{LockResult, MutexGuard};

/// Type that needs to be externally synchronized.
pub trait ExternallySynchronized {
    /// Acquires a mutex, blocking the current thread until it is able to do so.
    fn lock(&self) -> LockResult<MutexGuard<()>>;
}
