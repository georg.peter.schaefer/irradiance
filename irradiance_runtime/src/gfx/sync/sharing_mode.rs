/// Buffer and image sharing modes.
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum SharingMode {
    /// Access to any range or image subresource of the object will be exclusive to a single queue
    /// family at a time.
    Exclusive,
    /// Concurrent access to any range or image subresource of the object from multiple queue
    /// families is supported.
    Concurrent(Vec<u32>),
}

#[doc(hidden)]
impl From<SharingMode> for ash::vk::SharingMode {
    fn from(sharing_mode: SharingMode) -> Self {
        match sharing_mode {
            SharingMode::Exclusive => Self::EXCLUSIVE,
            SharingMode::Concurrent(_) => Self::CONCURRENT,
        }
    }
}

#[doc(hidden)]
impl AsRef<[u32]> for SharingMode {
    fn as_ref(&self) -> &[u32] {
        match self {
            SharingMode::Exclusive => &[],
            SharingMode::Concurrent(queue_families) => queue_families,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::sync::SharingMode;

    #[test]
    fn to_sharing_mode() {
        assert_eq!(
            ash::vk::SharingMode::EXCLUSIVE,
            ash::vk::SharingMode::from(SharingMode::Exclusive)
        );
        assert_eq!(
            ash::vk::SharingMode::CONCURRENT,
            ash::vk::SharingMode::from(SharingMode::Concurrent(vec![0]))
        );
    }

    #[test]
    fn as_ref() {
        assert_eq!(Vec::<u32>::new(), SharingMode::Exclusive.as_ref());
        assert_eq!(vec![0], SharingMode::Concurrent(vec![0]).as_ref());
    }
}
