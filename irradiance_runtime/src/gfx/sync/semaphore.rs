use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::{GfxError, Handle, ObjectType};

/// A queue semaphore object.
#[derive(Debug)]
pub struct Semaphore {
    handle: ash::vk::Semaphore,
    device: Arc<Device>,
}

impl Semaphore {
    /// Starts building an new semaphore.
    pub fn builder(device: Arc<Device>) -> SemaphoreBuilder {
        SemaphoreBuilder {
            exported: false,
            device,
        }
    }

    /// Returns the exported handle.
    #[cfg(target_os = "linux")]
    pub fn exported_handle(&self) -> i32 {
        let loader = ash::extensions::khr::ExternalSemaphoreFd::new(
            self.device.physical_device().instance().inner(),
            self.device.inner(),
        );
        let get_info = ash::vk::SemaphoreGetFdInfoKHR::builder()
            .handle_type(ash::vk::ExternalSemaphoreHandleTypeFlags::OPAQUE_FD)
            .semaphore(self.handle)
            .build();
        unsafe { loader.get_semaphore_fd(&get_info).unwrap() }
    }

    /// Returns the exported handle.
    #[cfg(target_os = "windows")]
    pub fn exported_handle(&self) -> *mut std::ffi::c_void {
        let loader = ash::extensions::khr::ExternalSemaphoreWin32::new(
            self.device.physical_device().instance().inner(),
            self.device.inner(),
        );
        let get_info = ash::vk::SemaphoreGetWin32HandleInfoKHR::builder()
            .handle_type(ash::vk::ExternalSemaphoreHandleTypeFlags::OPAQUE_WIN32)
            .semaphore(self.handle)
            .build();
        unsafe { loader.get_semaphore_win32_handle(&get_info).unwrap() }
    }
}

impl Drop for Semaphore {
    fn drop(&mut self) {
        unsafe {
            self.device.inner().destroy_semaphore(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for Semaphore {
    type Type = ash::vk::Semaphore;

    fn object_type(&self) -> ObjectType {
        ObjectType::Semaphore
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for Semaphore {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`Semaphore`].
#[derive(Debug)]
pub struct SemaphoreBuilder {
    exported: bool,
    device: Arc<Device>,
}

impl SemaphoreBuilder {
    /// Exported semaphore.
    pub fn exported(&mut self) -> &mut Self {
        self.exported = true;
        self
    }

    /// Builds the [`Semaphore`].
    ///
    /// # Errors
    /// This function returns an error if the semaphore creation fails.
    pub fn build(&mut self) -> Result<Arc<Semaphore>, GfxError> {
        #[cfg(target_os = "linux")]
        let mut export_create_info = ash::vk::ExportSemaphoreCreateInfo::builder()
            .handle_types(ash::vk::ExternalSemaphoreHandleTypeFlags::OPAQUE_FD)
            .build();
        #[cfg(target_os = "windows")]
        let mut export_create_info = ash::vk::ExportSemaphoreCreateInfo::builder()
            .handle_types(ash::vk::ExternalSemaphoreHandleTypeFlags::OPAQUE_WIN32)
            .build();
        let mut create_info_builder = ash::vk::SemaphoreCreateInfo::builder();
        if self.exported {
            create_info_builder = create_info_builder.push_next(&mut export_create_info);
        }
        let create_info = create_info_builder.build();

        let handle = unsafe { self.device.inner().create_semaphore(&create_info, None) }?;

        Ok(Arc::new(Semaphore {
            handle,
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::sync::Semaphore;
    use crate::physical_device;

    #[test]
    fn build_semaphore() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();

        let result = Semaphore::builder(device.clone()).build();

        assert!(result.is_ok());
    }
}
