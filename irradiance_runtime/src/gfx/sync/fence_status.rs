use crate::impl_flag_set;

impl_flag_set! {
    FenceStatus,
    doc = "Initial fence status.",
    (VK_FENCE_CREATE_SIGNALED_BIT, signaled)
}

#[doc(hidden)]
impl From<FenceStatus> for ash::vk::FenceCreateFlags {
    fn from(fence_status: FenceStatus) -> Self {
        let mut flags = ash::vk::FenceCreateFlags::empty();
        if fence_status.signaled {
            flags |= ash::vk::FenceCreateFlags::SIGNALED;
        }
        flags
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::sync::FenceStatus;

    #[test]
    fn to_fence_create_flags() {
        assert_eq!(
            ash::vk::FenceCreateFlags::SIGNALED,
            ash::vk::FenceCreateFlags::from(FenceStatus { signaled: true })
        );
    }
}
