use crate::impl_flag_set;

impl_flag_set! {
    PipelineStages,
    doc = "Pipeline stages.",
    (VK_PIPELINE_STAGE_2_TOP_OF_PIPE_BIT, top_of_pipe),
    (VK_PIPELINE_STAGE_2_DRAW_INDIRECT_BIT, draw_indirect),
    (VK_PIPELINE_STAGE_2_VERTEX_INPUT_BIT, vertex_input),
    (VK_PIPELINE_STAGE_2_VERTEX_SHADER_BIT, vertex_shader),
    (VK_PIPELINE_STAGE_2_TESSELLATION_CONTROL_SHADER_BIT, tesselation_control_shader),
    (VK_PIPELINE_STAGE_2_TESSELLATION_EVALUATION_SHADER_BIT, tesselation_evaluation_shader),
    (VK_PIPELINE_STAGE_2_GEOMETRY_SHADER_BIT, geoemtry_shader),
    (VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT, fragment_shader),
    (VK_PIPELINE_STAGE_2_EARLY_FRAGMENT_TESTS_BIT, early_fragment_tests),
    (VK_PIPELINE_STAGE_2_LATE_FRAGMENT_TESTS_BIT, late_fragment_tests),
    (VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT, color_attachment_output),
    (VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT, compute_shader),
    (VK_PIPELINE_STAGE_2_ALL_TRANSFER_BIT, all_transfer),
    (VK_PIPELINE_STAGE_2_TRANSFER_BIT, transfer),
    (VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT, bottom_of_pipe),
    (VK_PIPELINE_STAGE_2_HOST_BIT, host),
    (VK_PIPELINE_STAGE_2_ALL_GRAPHICS_BIT, all_graphics),
    (VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT, all_commands),
    (VK_PIPELINE_STAGE_2_COPY_BIT, copy),
    (VK_PIPELINE_STAGE_2_RESOLVE_BIT, resolve),
    (VK_PIPELINE_STAGE_2_BLIT_BIT, blit),
    (VK_PIPELINE_STAGE_2_CLEAR_BIT, clear),
    (VK_PIPELINE_STAGE_2_INDEX_INPUT_BIT, index_input),
    (VK_PIPELINE_STAGE_2_VERTEX_ATTRIBUTE_INPUT_BIT, vertex_attribute_input),
    (VK_PIPELINE_STAGE_2_PRE_RASTERIZATION_SHADERS_BIT, pre_rasterization_shaders),
    (VK_PIPELINE_STAGE_2_VIDEO_DECODE_BIT_KHR, video_decode),
    (VK_PIPELINE_STAGE_2_VIDEO_ENCODE_BIT_KHR, video_encode),
    (VK_PIPELINE_STAGE_2_TRANSFORM_FEEDBACK_BIT_EXT, transform_feedback),
    (VK_PIPELINE_STAGE_2_CONDITIONAL_RENDERING_BIT_EXT, conditional_rendering),
    (VK_PIPELINE_STAGE_2_COMMAND_PREPROCESS_BIT_NV, command_preprocess),
    (VK_PIPELINE_STAGE_2_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR, fragment_shading_rate_attachment),
    (VK_PIPELINE_STAGE_2_SHADING_RATE_IMAGE_BIT_NV, shading_rate_image),
    (VK_PIPELINE_STAGE_2_ACCELERATION_STRUCTURE_BUILD_BIT_KHR, acceleration_structure_build),
    (VK_PIPELINE_STAGE_2_RAY_TRACING_SHADER_BIT_KHR, ray_tracing_shader),
    (VK_PIPELINE_STAGE_2_FRAGMENT_DENSITY_PROCESS_BIT_EXT, fragment_density_process),
    (VK_PIPELINE_STAGE_2_TASK_SHADER_BIT_NV, task_shader),
    (VK_PIPELINE_STAGE_2_MESH_SHADER_BIT_NV, mesh_shader),
    (VK_PIPELINE_STAGE_2_SUBPASS_SHADING_BIT_HUAWEI, subpass_shading),
    (VK_PIPELINE_STAGE_2_INVOCATION_MASK_BIT_HUAWEI, invocation_mask)
}

#[doc(hidden)]
impl From<PipelineStages> for ash::vk::PipelineStageFlags {
    fn from(pipeline_stages: PipelineStages) -> Self {
        let mut flags = ash::vk::PipelineStageFlags::empty();
        if pipeline_stages.top_of_pipe {
            flags |= ash::vk::PipelineStageFlags::TOP_OF_PIPE;
        }
        if pipeline_stages.draw_indirect {
            flags |= ash::vk::PipelineStageFlags::DRAW_INDIRECT;
        }
        if pipeline_stages.vertex_input {
            flags |= ash::vk::PipelineStageFlags::VERTEX_INPUT;
        }
        if pipeline_stages.vertex_shader {
            flags |= ash::vk::PipelineStageFlags::VERTEX_SHADER;
        }
        if pipeline_stages.tesselation_control_shader {
            flags |= ash::vk::PipelineStageFlags::TESSELLATION_CONTROL_SHADER;
        }
        if pipeline_stages.tesselation_evaluation_shader {
            flags |= ash::vk::PipelineStageFlags::TESSELLATION_EVALUATION_SHADER;
        }
        if pipeline_stages.geoemtry_shader {
            flags |= ash::vk::PipelineStageFlags::GEOMETRY_SHADER;
        }
        if pipeline_stages.fragment_shader {
            flags |= ash::vk::PipelineStageFlags::FRAGMENT_SHADER;
        }
        if pipeline_stages.early_fragment_tests {
            flags |= ash::vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS;
        }
        if pipeline_stages.late_fragment_tests {
            flags |= ash::vk::PipelineStageFlags::LATE_FRAGMENT_TESTS;
        }
        if pipeline_stages.color_attachment_output {
            flags |= ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT;
        }
        if pipeline_stages.compute_shader {
            flags |= ash::vk::PipelineStageFlags::COMPUTE_SHADER;
        }
        if pipeline_stages.transfer {
            flags |= ash::vk::PipelineStageFlags::TRANSFER;
        }
        if pipeline_stages.bottom_of_pipe {
            flags |= ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE;
        }
        if pipeline_stages.host {
            flags |= ash::vk::PipelineStageFlags::HOST;
        }
        if pipeline_stages.all_graphics {
            flags |= ash::vk::PipelineStageFlags::ALL_GRAPHICS;
        }
        if pipeline_stages.all_commands {
            flags |= ash::vk::PipelineStageFlags::ALL_COMMANDS;
        }
        if pipeline_stages.transform_feedback {
            flags |= ash::vk::PipelineStageFlags::TRANSFORM_FEEDBACK_EXT;
        }
        if pipeline_stages.conditional_rendering {
            flags |= ash::vk::PipelineStageFlags::CONDITIONAL_RENDERING_EXT;
        }
        if pipeline_stages.command_preprocess {
            flags |= ash::vk::PipelineStageFlags::COMMAND_PREPROCESS_NV;
        }
        if pipeline_stages.fragment_shading_rate_attachment {
            flags |= ash::vk::PipelineStageFlags::FRAGMENT_SHADING_RATE_ATTACHMENT_KHR;
        }
        if pipeline_stages.shading_rate_image {
            flags |= ash::vk::PipelineStageFlags::SHADING_RATE_IMAGE_NV;
        }
        if pipeline_stages.acceleration_structure_build {
            flags |= ash::vk::PipelineStageFlags::ACCELERATION_STRUCTURE_BUILD_KHR;
        }
        if pipeline_stages.ray_tracing_shader {
            flags |= ash::vk::PipelineStageFlags::RAY_TRACING_SHADER_KHR;
        }
        if pipeline_stages.fragment_density_process {
            flags |= ash::vk::PipelineStageFlags::FRAGMENT_DENSITY_PROCESS_EXT;
        }
        if pipeline_stages.task_shader {
            flags |= ash::vk::PipelineStageFlags::TASK_SHADER_NV;
        }
        if pipeline_stages.mesh_shader {
            flags |= ash::vk::PipelineStageFlags::MESH_SHADER_NV;
        }
        flags
    }
}

#[doc(hidden)]
impl From<PipelineStages> for ash::vk::PipelineStageFlags2 {
    fn from(pipeline_stages: PipelineStages) -> Self {
        let mut flags = ash::vk::PipelineStageFlags2::empty();
        if pipeline_stages.top_of_pipe {
            flags |= ash::vk::PipelineStageFlags2::TOP_OF_PIPE;
        }
        if pipeline_stages.draw_indirect {
            flags |= ash::vk::PipelineStageFlags2::DRAW_INDIRECT;
        }
        if pipeline_stages.vertex_input {
            flags |= ash::vk::PipelineStageFlags2::VERTEX_INPUT;
        }
        if pipeline_stages.vertex_shader {
            flags |= ash::vk::PipelineStageFlags2::VERTEX_SHADER;
        }
        if pipeline_stages.tesselation_control_shader {
            flags |= ash::vk::PipelineStageFlags2::TESSELLATION_CONTROL_SHADER;
        }
        if pipeline_stages.tesselation_evaluation_shader {
            flags |= ash::vk::PipelineStageFlags2::TESSELLATION_EVALUATION_SHADER;
        }
        if pipeline_stages.geoemtry_shader {
            flags |= ash::vk::PipelineStageFlags2::GEOMETRY_SHADER;
        }
        if pipeline_stages.fragment_shader {
            flags |= ash::vk::PipelineStageFlags2::FRAGMENT_SHADER;
        }
        if pipeline_stages.early_fragment_tests {
            flags |= ash::vk::PipelineStageFlags2::EARLY_FRAGMENT_TESTS;
        }
        if pipeline_stages.late_fragment_tests {
            flags |= ash::vk::PipelineStageFlags2::LATE_FRAGMENT_TESTS;
        }
        if pipeline_stages.color_attachment_output {
            flags |= ash::vk::PipelineStageFlags2::COLOR_ATTACHMENT_OUTPUT;
        }
        if pipeline_stages.compute_shader {
            flags |= ash::vk::PipelineStageFlags2::COMPUTE_SHADER;
        }
        if pipeline_stages.all_transfer {
            flags |= ash::vk::PipelineStageFlags2::ALL_TRANSFER;
        }
        if pipeline_stages.transfer {
            flags |= ash::vk::PipelineStageFlags2::TRANSFER;
        }
        if pipeline_stages.bottom_of_pipe {
            flags |= ash::vk::PipelineStageFlags2::BOTTOM_OF_PIPE;
        }
        if pipeline_stages.host {
            flags |= ash::vk::PipelineStageFlags2::HOST;
        }
        if pipeline_stages.all_graphics {
            flags |= ash::vk::PipelineStageFlags2::ALL_GRAPHICS;
        }
        if pipeline_stages.all_commands {
            flags |= ash::vk::PipelineStageFlags2::ALL_COMMANDS;
        }
        if pipeline_stages.copy {
            flags |= ash::vk::PipelineStageFlags2::COPY;
        }
        if pipeline_stages.resolve {
            flags |= ash::vk::PipelineStageFlags2::RESOLVE;
        }
        if pipeline_stages.blit {
            flags |= ash::vk::PipelineStageFlags2::BLIT;
        }
        if pipeline_stages.clear {
            flags |= ash::vk::PipelineStageFlags2::CLEAR;
        }
        if pipeline_stages.index_input {
            flags |= ash::vk::PipelineStageFlags2::INDEX_INPUT;
        }
        if pipeline_stages.vertex_attribute_input {
            flags |= ash::vk::PipelineStageFlags2::VERTEX_ATTRIBUTE_INPUT;
        }
        if pipeline_stages.pre_rasterization_shaders {
            flags |= ash::vk::PipelineStageFlags2::PRE_RASTERIZATION_SHADERS;
        }
        if pipeline_stages.video_decode {
            flags |= ash::vk::PipelineStageFlags2::VIDEO_DECODE_KHR;
        }
        if pipeline_stages.video_encode {
            flags |= ash::vk::PipelineStageFlags2::VIDEO_ENCODE_KHR;
        }
        if pipeline_stages.transform_feedback {
            flags |= ash::vk::PipelineStageFlags2::TRANSFORM_FEEDBACK_EXT;
        }
        if pipeline_stages.conditional_rendering {
            flags |= ash::vk::PipelineStageFlags2::CONDITIONAL_RENDERING_EXT;
        }
        if pipeline_stages.command_preprocess {
            flags |= ash::vk::PipelineStageFlags2::COMMAND_PREPROCESS_NV;
        }
        if pipeline_stages.fragment_shading_rate_attachment {
            flags |= ash::vk::PipelineStageFlags2::FRAGMENT_SHADING_RATE_ATTACHMENT_KHR;
        }
        if pipeline_stages.shading_rate_image {
            flags |= ash::vk::PipelineStageFlags2::SHADING_RATE_IMAGE_NV;
        }
        if pipeline_stages.acceleration_structure_build {
            flags |= ash::vk::PipelineStageFlags2::ACCELERATION_STRUCTURE_BUILD_KHR;
        }
        if pipeline_stages.ray_tracing_shader {
            flags |= ash::vk::PipelineStageFlags2::RAY_TRACING_SHADER_KHR;
        }
        if pipeline_stages.fragment_density_process {
            flags |= ash::vk::PipelineStageFlags2::FRAGMENT_DENSITY_PROCESS_EXT;
        }
        if pipeline_stages.task_shader {
            flags |= ash::vk::PipelineStageFlags2::TASK_SHADER_NV;
        }
        if pipeline_stages.mesh_shader {
            flags |= ash::vk::PipelineStageFlags2::MESH_SHADER_NV;
        }
        if pipeline_stages.subpass_shading {
            flags |= ash::vk::PipelineStageFlags2::SUBPASS_SHADING_HUAWEI;
        }
        if pipeline_stages.invocation_mask {
            flags |= ash::vk::PipelineStageFlags2::INVOCATION_MASK_HUAWEI;
        }
        flags
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::sync::PipelineStages;

    #[test]
    fn to_pipeline_stage_flags() {
        assert_eq!(
            ash::vk::PipelineStageFlags::DRAW_INDIRECT
                | ash::vk::PipelineStageFlags::VERTEX_SHADER
                | ash::vk::PipelineStageFlags::TESSELLATION_EVALUATION_SHADER
                | ash::vk::PipelineStageFlags::FRAGMENT_SHADER
                | ash::vk::PipelineStageFlags::LATE_FRAGMENT_TESTS
                | ash::vk::PipelineStageFlags::COMPUTE_SHADER
                | ash::vk::PipelineStageFlags::TRANSFER
                | ash::vk::PipelineStageFlags::HOST
                | ash::vk::PipelineStageFlags::ALL_COMMANDS
                | ash::vk::PipelineStageFlags::TRANSFORM_FEEDBACK_EXT
                | ash::vk::PipelineStageFlags::COMMAND_PREPROCESS_NV
                | ash::vk::PipelineStageFlags::SHADING_RATE_IMAGE_NV
                | ash::vk::PipelineStageFlags::RAY_TRACING_SHADER_KHR
                | ash::vk::PipelineStageFlags::TASK_SHADER_NV,
            ash::vk::PipelineStageFlags::from(PipelineStages {
                top_of_pipe: false,
                draw_indirect: true,
                vertex_input: false,
                vertex_shader: true,
                tesselation_control_shader: false,
                tesselation_evaluation_shader: true,
                geoemtry_shader: false,
                fragment_shader: true,
                early_fragment_tests: false,
                late_fragment_tests: true,
                color_attachment_output: false,
                compute_shader: true,
                all_transfer: false,
                transfer: true,
                bottom_of_pipe: false,
                host: true,
                all_graphics: false,
                all_commands: true,
                copy: false,
                resolve: true,
                blit: false,
                clear: true,
                index_input: false,
                vertex_attribute_input: true,
                pre_rasterization_shaders: false,
                video_decode: true,
                video_encode: false,
                transform_feedback: true,
                conditional_rendering: false,
                command_preprocess: true,
                fragment_shading_rate_attachment: false,
                shading_rate_image: true,
                acceleration_structure_build: false,
                ray_tracing_shader: true,
                fragment_density_process: false,
                task_shader: true,
                mesh_shader: false,
                subpass_shading: true,
                invocation_mask: false
            })
        );
        assert_eq!(
            ash::vk::PipelineStageFlags::TOP_OF_PIPE
                | ash::vk::PipelineStageFlags::VERTEX_INPUT
                | ash::vk::PipelineStageFlags::TESSELLATION_CONTROL_SHADER
                | ash::vk::PipelineStageFlags::GEOMETRY_SHADER
                | ash::vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS
                | ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT
                | ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE
                | ash::vk::PipelineStageFlags::ALL_GRAPHICS
                | ash::vk::PipelineStageFlags::CONDITIONAL_RENDERING_EXT
                | ash::vk::PipelineStageFlags::FRAGMENT_SHADING_RATE_ATTACHMENT_KHR
                | ash::vk::PipelineStageFlags::ACCELERATION_STRUCTURE_BUILD_KHR
                | ash::vk::PipelineStageFlags::FRAGMENT_DENSITY_PROCESS_EXT
                | ash::vk::PipelineStageFlags::MESH_SHADER_NV,
            ash::vk::PipelineStageFlags::from(PipelineStages {
                top_of_pipe: true,
                draw_indirect: false,
                vertex_input: true,
                vertex_shader: false,
                tesselation_control_shader: true,
                tesselation_evaluation_shader: false,
                geoemtry_shader: true,
                fragment_shader: false,
                early_fragment_tests: true,
                late_fragment_tests: false,
                color_attachment_output: true,
                compute_shader: false,
                all_transfer: true,
                transfer: false,
                bottom_of_pipe: true,
                host: false,
                all_graphics: true,
                all_commands: false,
                copy: true,
                resolve: false,
                blit: true,
                clear: false,
                index_input: true,
                vertex_attribute_input: false,
                pre_rasterization_shaders: true,
                video_decode: false,
                video_encode: true,
                transform_feedback: false,
                conditional_rendering: true,
                command_preprocess: false,
                fragment_shading_rate_attachment: true,
                shading_rate_image: false,
                acceleration_structure_build: true,
                ray_tracing_shader: false,
                fragment_density_process: true,
                task_shader: false,
                mesh_shader: true,
                subpass_shading: false,
                invocation_mask: true
            })
        );
    }

    #[test]
    fn to_pipeline_stage_flags2() {
        assert_eq!(
            ash::vk::PipelineStageFlags2::DRAW_INDIRECT
                | ash::vk::PipelineStageFlags2::VERTEX_SHADER
                | ash::vk::PipelineStageFlags2::TESSELLATION_EVALUATION_SHADER
                | ash::vk::PipelineStageFlags2::FRAGMENT_SHADER
                | ash::vk::PipelineStageFlags2::LATE_FRAGMENT_TESTS
                | ash::vk::PipelineStageFlags2::COMPUTE_SHADER
                | ash::vk::PipelineStageFlags2::TRANSFER
                | ash::vk::PipelineStageFlags2::HOST
                | ash::vk::PipelineStageFlags2::ALL_COMMANDS
                | ash::vk::PipelineStageFlags2::RESOLVE
                | ash::vk::PipelineStageFlags2::CLEAR
                | ash::vk::PipelineStageFlags2::VERTEX_ATTRIBUTE_INPUT
                | ash::vk::PipelineStageFlags2::VIDEO_DECODE_KHR
                | ash::vk::PipelineStageFlags2::TRANSFORM_FEEDBACK_EXT
                | ash::vk::PipelineStageFlags2::COMMAND_PREPROCESS_NV
                | ash::vk::PipelineStageFlags2::SHADING_RATE_IMAGE_NV
                | ash::vk::PipelineStageFlags2::RAY_TRACING_SHADER_KHR
                | ash::vk::PipelineStageFlags2::TASK_SHADER_NV
                | ash::vk::PipelineStageFlags2::SUBPASS_SHADING_HUAWEI,
            ash::vk::PipelineStageFlags2::from(PipelineStages {
                top_of_pipe: false,
                draw_indirect: true,
                vertex_input: false,
                vertex_shader: true,
                tesselation_control_shader: false,
                tesselation_evaluation_shader: true,
                geoemtry_shader: false,
                fragment_shader: true,
                early_fragment_tests: false,
                late_fragment_tests: true,
                color_attachment_output: false,
                compute_shader: true,
                all_transfer: false,
                transfer: true,
                bottom_of_pipe: false,
                host: true,
                all_graphics: false,
                all_commands: true,
                copy: false,
                resolve: true,
                blit: false,
                clear: true,
                index_input: false,
                vertex_attribute_input: true,
                pre_rasterization_shaders: false,
                video_decode: true,
                video_encode: false,
                transform_feedback: true,
                conditional_rendering: false,
                command_preprocess: true,
                fragment_shading_rate_attachment: false,
                shading_rate_image: true,
                acceleration_structure_build: false,
                ray_tracing_shader: true,
                fragment_density_process: false,
                task_shader: true,
                mesh_shader: false,
                subpass_shading: true,
                invocation_mask: false
            })
        );
        assert_eq!(
            ash::vk::PipelineStageFlags2::TOP_OF_PIPE
                | ash::vk::PipelineStageFlags2::VERTEX_INPUT
                | ash::vk::PipelineStageFlags2::TESSELLATION_CONTROL_SHADER
                | ash::vk::PipelineStageFlags2::GEOMETRY_SHADER
                | ash::vk::PipelineStageFlags2::EARLY_FRAGMENT_TESTS
                | ash::vk::PipelineStageFlags2::COLOR_ATTACHMENT_OUTPUT
                | ash::vk::PipelineStageFlags2::ALL_TRANSFER
                | ash::vk::PipelineStageFlags2::BOTTOM_OF_PIPE
                | ash::vk::PipelineStageFlags2::ALL_GRAPHICS
                | ash::vk::PipelineStageFlags2::COPY
                | ash::vk::PipelineStageFlags2::BLIT
                | ash::vk::PipelineStageFlags2::INDEX_INPUT
                | ash::vk::PipelineStageFlags2::PRE_RASTERIZATION_SHADERS
                | ash::vk::PipelineStageFlags2::VIDEO_ENCODE_KHR
                | ash::vk::PipelineStageFlags2::CONDITIONAL_RENDERING_EXT
                | ash::vk::PipelineStageFlags2::FRAGMENT_SHADING_RATE_ATTACHMENT_KHR
                | ash::vk::PipelineStageFlags2::ACCELERATION_STRUCTURE_BUILD_KHR
                | ash::vk::PipelineStageFlags2::FRAGMENT_DENSITY_PROCESS_EXT
                | ash::vk::PipelineStageFlags2::MESH_SHADER_NV
                | ash::vk::PipelineStageFlags2::INVOCATION_MASK_HUAWEI,
            ash::vk::PipelineStageFlags2::from(PipelineStages {
                top_of_pipe: true,
                draw_indirect: false,
                vertex_input: true,
                vertex_shader: false,
                tesselation_control_shader: true,
                tesselation_evaluation_shader: false,
                geoemtry_shader: true,
                fragment_shader: false,
                early_fragment_tests: true,
                late_fragment_tests: false,
                color_attachment_output: true,
                compute_shader: false,
                all_transfer: true,
                transfer: false,
                bottom_of_pipe: true,
                host: false,
                all_graphics: true,
                all_commands: false,
                copy: true,
                resolve: false,
                blit: true,
                clear: false,
                index_input: true,
                vertex_attribute_input: false,
                pre_rasterization_shaders: true,
                video_decode: false,
                video_encode: true,
                transform_feedback: false,
                conditional_rendering: true,
                command_preprocess: false,
                fragment_shading_rate_attachment: true,
                shading_rate_image: false,
                acceleration_structure_build: true,
                ray_tracing_shader: false,
                fragment_density_process: true,
                task_shader: false,
                mesh_shader: true,
                subpass_shading: false,
                invocation_mask: true
            })
        );
    }
}
