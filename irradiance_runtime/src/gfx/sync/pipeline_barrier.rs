use std::sync::Arc;

use crate::gfx::buffer::Buffer;
use crate::gfx::image::{Image, ImageLayout, ImageSubresourceRange};
use crate::gfx::sync::{AccessMask, DependencyFlags, PipelineStages};
use crate::gfx::Handle;

/// Dependency information for a synchronization command.
#[derive(Clone, Debug)]
pub struct PipelineBarrier {
    pub(crate) image_memory_barriers: Vec<ImageMemoryBarrier>,
    pub(crate) buffer_memory_barriers: Vec<BufferMemoryBarrier>,
    pub(crate) memory_barriers: Vec<MemoryBarrier>,
    pub(crate) dependency_flags: DependencyFlags,
}

impl PipelineBarrier {
    /// Starts building a new pipeline barrier.
    pub fn builder() -> PipelineBarrierBuilder {
        PipelineBarrierBuilder {
            image_memory_barriers: vec![],
            buffer_memory_barriers: vec![],
            memory_barriers: vec![],
            dependency_flags: Default::default(),
        }
    }
}

/// Type for building a[`PipelineBarrier`].
#[derive(Debug)]
pub struct PipelineBarrierBuilder {
    image_memory_barriers: Vec<ImageMemoryBarrier>,
    buffer_memory_barriers: Vec<BufferMemoryBarrier>,
    memory_barriers: Vec<MemoryBarrier>,
    dependency_flags: DependencyFlags,
}

impl PipelineBarrierBuilder {
    /// Sets the dependency flags.
    pub fn dependency_flags(&mut self, dependency_flags: DependencyFlags) -> &mut Self {
        self.dependency_flags = dependency_flags;
        self
    }

    /// Adds a [`MemoryBarrier`].
    pub fn memory_barrier(&mut self, memory_barrier: MemoryBarrier) -> &mut Self {
        self.memory_barriers.push(memory_barrier);
        self
    }

    /// Adds a [`BufferMemoryBarrier`].
    pub fn buffer_memory_barrier(
        &mut self,
        buffer_memory_barrier: BufferMemoryBarrier,
    ) -> &mut Self {
        self.buffer_memory_barriers.push(buffer_memory_barrier);
        self
    }

    /// Adds a [`ImageMemoryBarrier`].
    pub fn image_memory_barrier(&mut self, image_memory_barrier: ImageMemoryBarrier) -> &mut Self {
        self.image_memory_barriers.push(image_memory_barrier);
        self
    }

    /// Builds the [`PipelineBarrier`].
    pub fn build(&mut self) -> PipelineBarrier {
        PipelineBarrier {
            image_memory_barriers: self.image_memory_barriers.clone(),
            buffer_memory_barriers: self.buffer_memory_barriers.clone(),
            memory_barriers: self.memory_barriers.clone(),
            dependency_flags: self.dependency_flags,
        }
    }
}

/// Global memory barrier.
#[derive(Copy, Clone, Debug)]
pub struct MemoryBarrier {
    dst_access_mask: AccessMask,
    dst_stage_mask: PipelineStages,
    src_access_mask: AccessMask,
    src_stage_mask: PipelineStages,
}

impl MemoryBarrier {
    /// Starts building a new memory barrier.
    pub fn builder() -> MemoryBarrierBuilder {
        MemoryBarrierBuilder {
            dst_access_mask: Default::default(),
            dst_stage_mask: Default::default(),
            src_access_mask: Default::default(),
            src_stage_mask: Default::default(),
        }
    }
}

#[doc(hidden)]
impl From<MemoryBarrier> for ash::vk::MemoryBarrier2 {
    fn from(memory_barrier: MemoryBarrier) -> Self {
        ash::vk::MemoryBarrier2::builder()
            .src_stage_mask(memory_barrier.src_stage_mask.into())
            .src_access_mask(memory_barrier.src_access_mask.into())
            .dst_stage_mask(memory_barrier.dst_stage_mask.into())
            .dst_access_mask(memory_barrier.dst_access_mask.into())
            .build()
    }
}

/// Type for building a [`MemoryBarrier`].
#[derive(Debug)]
pub struct MemoryBarrierBuilder {
    dst_access_mask: AccessMask,
    dst_stage_mask: PipelineStages,
    src_access_mask: AccessMask,
    src_stage_mask: PipelineStages,
}

impl MemoryBarrierBuilder {
    /// Sets the source pipeline stages.
    pub fn src_stage_mask(&mut self, src_stage_mask: PipelineStages) -> &mut Self {
        self.src_stage_mask = src_stage_mask;
        self
    }

    /// Sets the source access mask.
    pub fn src_access_mask(&mut self, src_access_mask: AccessMask) -> &mut Self {
        self.src_access_mask = src_access_mask;
        self
    }

    /// Sets the destination pipeline stages.
    pub fn dst_stage_mask(&mut self, dst_stage_mask: PipelineStages) -> &mut Self {
        self.dst_stage_mask = dst_stage_mask;
        self
    }

    /// Sets the destination access mask.
    pub fn dst_access_mask(&mut self, dst_access_mask: AccessMask) -> &mut Self {
        self.dst_access_mask = dst_access_mask;
        self
    }

    /// Builds the [`MemoryBarrier`].
    pub fn build(&mut self) -> MemoryBarrier {
        MemoryBarrier {
            dst_access_mask: self.dst_access_mask,
            dst_stage_mask: self.dst_stage_mask,
            src_access_mask: self.src_access_mask,
            src_stage_mask: self.src_stage_mask,
        }
    }
}

/// Buffer memory barrier.
#[derive(Clone, Debug)]
pub struct BufferMemoryBarrier {
    size: u64,
    offset: u64,
    buffer: Arc<Buffer>,
    dst_queue_family_index: u32,
    src_queue_family_index: u32,
    dst_access_mask: AccessMask,
    dst_stage_mask: PipelineStages,
    src_access_mask: AccessMask,
    src_stage_mask: PipelineStages,
}

impl BufferMemoryBarrier {
    /// Starts building a new [`BufferMemoryBarrier`].
    pub fn builder(buffer: Arc<Buffer>) -> BufferMemoryBarrierBuilder {
        BufferMemoryBarrierBuilder {
            size: ash::vk::WHOLE_SIZE,
            offset: 0,
            buffer,
            dst_queue_family_index: 0,
            src_queue_family_index: 0,
            dst_access_mask: Default::default(),
            dst_stage_mask: Default::default(),
            src_access_mask: Default::default(),
            src_stage_mask: Default::default(),
        }
    }
}

#[doc(hidden)]
impl From<BufferMemoryBarrier> for ash::vk::BufferMemoryBarrier2 {
    fn from(buffer_memory_barrier: BufferMemoryBarrier) -> Self {
        ash::vk::BufferMemoryBarrier2::builder()
            .src_stage_mask(buffer_memory_barrier.src_stage_mask.into())
            .src_access_mask(buffer_memory_barrier.src_access_mask.into())
            .dst_stage_mask(buffer_memory_barrier.dst_stage_mask.into())
            .dst_access_mask(buffer_memory_barrier.dst_access_mask.into())
            .src_queue_family_index(buffer_memory_barrier.src_queue_family_index)
            .dst_queue_family_index(buffer_memory_barrier.dst_queue_family_index)
            .buffer(buffer_memory_barrier.buffer.handle())
            .offset(buffer_memory_barrier.offset)
            .size(buffer_memory_barrier.size)
            .build()
    }
}

/// Type for building a [`BufferMemoryBarrier`].
#[derive(Debug)]
pub struct BufferMemoryBarrierBuilder {
    size: u64,
    offset: u64,
    buffer: Arc<Buffer>,
    dst_queue_family_index: u32,
    src_queue_family_index: u32,
    dst_access_mask: AccessMask,
    dst_stage_mask: PipelineStages,
    src_access_mask: AccessMask,
    src_stage_mask: PipelineStages,
}

impl BufferMemoryBarrierBuilder {
    /// Sets the source pipeline stages.
    pub fn src_stage_mask(&mut self, src_stage_mask: PipelineStages) -> &mut Self {
        self.src_stage_mask = src_stage_mask;
        self
    }

    /// Sets the source access mask.
    pub fn src_access_mask(&mut self, src_access_mask: AccessMask) -> &mut Self {
        self.src_access_mask = src_access_mask;
        self
    }

    /// Sets the destination pipeline stages.
    pub fn dst_stage_mask(&mut self, dst_stage_mask: PipelineStages) -> &mut Self {
        self.dst_stage_mask = dst_stage_mask;
        self
    }

    /// Sets the destination access mask.
    pub fn dst_access_mask(&mut self, dst_access_mask: AccessMask) -> &mut Self {
        self.dst_access_mask = dst_access_mask;
        self
    }

    /// Sets the source queue family index.
    pub fn src_queue_family_index(&mut self, src_queue_family_index: u32) -> &mut Self {
        self.src_queue_family_index = src_queue_family_index;
        self
    }

    /// Sets the destination queue family index.
    pub fn dst_queue_family_index(&mut self, dst_queue_family_index: u32) -> &mut Self {
        self.dst_queue_family_index = dst_queue_family_index;
        self
    }

    /// Sets the offset of the buffer memory region.
    pub fn offset(&mut self, offset: u64) -> &mut Self {
        self.offset = offset;
        self
    }

    /// Sets the size of the buffer memory region.
    pub fn size(&mut self, size: u64) -> &mut Self {
        self.size = size;
        self
    }

    /// Builds the [`BufferMemoryBarrier`].
    pub fn build(&mut self) -> BufferMemoryBarrier {
        BufferMemoryBarrier {
            size: self.size,
            offset: self.offset,
            buffer: self.buffer.clone(),
            dst_queue_family_index: self.dst_queue_family_index,
            src_queue_family_index: self.src_queue_family_index,
            dst_access_mask: self.dst_access_mask,
            dst_stage_mask: self.dst_stage_mask,
            src_access_mask: self.src_access_mask,
            src_stage_mask: self.src_stage_mask,
        }
    }
}

/// Image memory barrier.
#[derive(Clone, Debug)]
pub struct ImageMemoryBarrier {
    subresource_range: ImageSubresourceRange,
    image: Arc<Image>,
    dst_queue_family_index: u32,
    src_queue_family_index: u32,
    new_layout: ImageLayout,
    old_layout: ImageLayout,
    dst_access_mask: AccessMask,
    dst_stage_mask: PipelineStages,
    src_access_mask: AccessMask,
    src_stage_mask: PipelineStages,
}

impl ImageMemoryBarrier {
    /// Starts building a new [`ImageMemoryBarrier`].
    pub fn builder(image: Arc<Image>) -> ImageMemoryBarrierBuilder {
        ImageMemoryBarrierBuilder {
            subresource_range: Default::default(),
            image,
            dst_queue_family_index: ash::vk::QUEUE_FAMILY_IGNORED,
            src_queue_family_index: ash::vk::QUEUE_FAMILY_IGNORED,
            new_layout: ImageLayout::Undefined,
            old_layout: ImageLayout::Undefined,
            dst_access_mask: Default::default(),
            dst_stage_mask: Default::default(),
            src_access_mask: Default::default(),
            src_stage_mask: Default::default(),
        }
    }
}

#[doc(hidden)]
impl From<ImageMemoryBarrier> for ash::vk::ImageMemoryBarrier2 {
    fn from(image_memory_barrier: ImageMemoryBarrier) -> Self {
        ash::vk::ImageMemoryBarrier2::builder()
            .src_stage_mask(image_memory_barrier.src_stage_mask.into())
            .src_access_mask(image_memory_barrier.src_access_mask.into())
            .dst_stage_mask(image_memory_barrier.dst_stage_mask.into())
            .dst_access_mask(image_memory_barrier.dst_access_mask.into())
            .old_layout(image_memory_barrier.old_layout.into())
            .new_layout(image_memory_barrier.new_layout.into())
            .src_queue_family_index(image_memory_barrier.src_queue_family_index)
            .dst_queue_family_index(image_memory_barrier.dst_queue_family_index)
            .image(image_memory_barrier.image.handle())
            .subresource_range(image_memory_barrier.subresource_range.into())
            .build()
    }
}

/// Type for building an [`ImageMemoryBarrier`].
#[derive(Debug)]
pub struct ImageMemoryBarrierBuilder {
    subresource_range: ImageSubresourceRange,
    image: Arc<Image>,
    dst_queue_family_index: u32,
    src_queue_family_index: u32,
    new_layout: ImageLayout,
    old_layout: ImageLayout,
    dst_access_mask: AccessMask,
    dst_stage_mask: PipelineStages,
    src_access_mask: AccessMask,
    src_stage_mask: PipelineStages,
}

impl ImageMemoryBarrierBuilder {
    /// Sets the source pipeline stages.
    pub fn src_stage_mask(&mut self, src_stage_mask: PipelineStages) -> &mut Self {
        self.src_stage_mask = src_stage_mask;
        self
    }

    /// Sets the source access mask.
    pub fn src_access_mask(&mut self, src_access_mask: AccessMask) -> &mut Self {
        self.src_access_mask = src_access_mask;
        self
    }

    /// Sets the destination pipeline stages.
    pub fn dst_stage_mask(&mut self, dst_stage_mask: PipelineStages) -> &mut Self {
        self.dst_stage_mask = dst_stage_mask;
        self
    }

    /// Sets the destination access mask.
    pub fn dst_access_mask(&mut self, dst_access_mask: AccessMask) -> &mut Self {
        self.dst_access_mask = dst_access_mask;
        self
    }

    /// Sets the old image layout.
    pub fn old_layout(&mut self, old_layout: ImageLayout) -> &mut Self {
        self.old_layout = old_layout;
        self
    }

    /// Sets the new image layout.
    pub fn new_layout(&mut self, new_layout: ImageLayout) -> &mut Self {
        self.new_layout = new_layout;
        self
    }

    /// Sets the source queue family index.
    pub fn src_queue_family_index(&mut self, src_queue_family_index: u32) -> &mut Self {
        self.src_queue_family_index = src_queue_family_index;
        self
    }

    /// Sets the destination queue family index.
    pub fn dst_queue_family_index(&mut self, dst_queue_family_index: u32) -> &mut Self {
        self.dst_queue_family_index = dst_queue_family_index;
        self
    }

    /// Sets the subresource range.
    pub fn subresource_range(&mut self, subresource_range: ImageSubresourceRange) -> &mut Self {
        self.subresource_range = subresource_range;
        self
    }

    /// Builds the [`ImageMemoryBarrier`].
    pub fn build(&self) -> ImageMemoryBarrier {
        ImageMemoryBarrier {
            subresource_range: self.subresource_range,
            image: self.image.clone(),
            dst_queue_family_index: self.dst_queue_family_index,
            src_queue_family_index: self.src_queue_family_index,
            new_layout: self.new_layout,
            old_layout: self.old_layout,
            dst_access_mask: self.dst_access_mask,
            dst_stage_mask: self.dst_stage_mask,
            src_access_mask: self.src_access_mask,
            src_stage_mask: self.src_stage_mask,
        }
    }
}

#[cfg(test)]
mod tests {
    use ash::vk::{AccessFlags2, MemoryBarrier2, PipelineStageFlags2};

    use crate::gfx::buffer::{Buffer, BufferUsage};
    use crate::gfx::device::Device;
    use crate::gfx::image::{
        Format, Image, ImageAspects, ImageLayout, ImageSubresourceRange, ImageTiling, ImageType,
        ImageUsage, SampleCount,
    };
    use crate::gfx::sync::{
        AccessMask, BufferMemoryBarrier, DependencyFlags, ImageMemoryBarrier, MemoryBarrier,
        PipelineBarrier, PipelineStages, SharingMode,
    };
    use crate::gfx::{Extent3D, Handle};
    use crate::physical_device;

    #[test]
    fn build_pipeline_barrier() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let buffer = Buffer::with_size(device.clone(), 24)
            .usage(BufferUsage {
                storage_buffer: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .build()
            .unwrap();
        let image = Image::builder(device.clone())
            .image_type(ImageType::Type2D)
            .format(Format::R8G8B8A8Srgb)
            .extent(Extent3D {
                width: 1024,
                height: 1024,
                depth: 1,
            })
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCount::Sample1)
            .tiling(ImageTiling::Optimal)
            .usage(ImageUsage {
                sampled: true,
                transfer_dst: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .initial_layout(ImageLayout::Undefined)
            .build()
            .unwrap();
        let pipeline_barrier = PipelineBarrier::builder()
            .dependency_flags(DependencyFlags {
                by_region: true,
                ..Default::default()
            })
            .memory_barrier(
                MemoryBarrier::builder()
                    .src_stage_mask(PipelineStages {
                        top_of_pipe: true,
                        ..Default::default()
                    })
                    .src_access_mask(AccessMask {
                        memory_read: true,
                        ..Default::default()
                    })
                    .dst_stage_mask(PipelineStages {
                        color_attachment_output: true,
                        ..Default::default()
                    })
                    .dst_access_mask(AccessMask {
                        color_attachment_write: true,
                        ..Default::default()
                    })
                    .build(),
            )
            .buffer_memory_barrier(
                BufferMemoryBarrier::builder(buffer.clone())
                    .src_stage_mask(PipelineStages {
                        transfer: true,
                        ..Default::default()
                    })
                    .src_access_mask(AccessMask {
                        transfer_write: true,
                        ..Default::default()
                    })
                    .dst_stage_mask(PipelineStages {
                        vertex_shader: true,
                        ..Default::default()
                    })
                    .dst_access_mask(AccessMask {
                        uniform_read: true,
                        ..Default::default()
                    })
                    .src_queue_family_index(0)
                    .dst_queue_family_index(1)
                    .offset(8)
                    .size(16)
                    .build(),
            )
            .image_memory_barrier(
                ImageMemoryBarrier::builder(image.clone())
                    .src_stage_mask(PipelineStages {
                        transfer: true,
                        ..Default::default()
                    })
                    .src_access_mask(AccessMask {
                        transfer_write: true,
                        ..Default::default()
                    })
                    .dst_stage_mask(PipelineStages {
                        fragment_shader: true,
                        ..Default::default()
                    })
                    .dst_access_mask(AccessMask {
                        shader_sampled_read: true,
                        ..Default::default()
                    })
                    .old_layout(ImageLayout::Undefined)
                    .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                    .src_queue_family_index(0)
                    .dst_queue_family_index(1)
                    .subresource_range(
                        ImageSubresourceRange::builder()
                            .aspect_mask(ImageAspects {
                                color: true,
                                ..Default::default()
                            })
                            .base_mip_level(0)
                            .level_count(1)
                            .base_array_layer(0)
                            .layer_count(1)
                            .build(),
                    )
                    .build(),
            )
            .build();

        assert_eq!(
            pipeline_barrier.dependency_flags,
            DependencyFlags {
                by_region: true,
                ..Default::default()
            }
        );
        assert_eq!(
            PipelineStages {
                top_of_pipe: true,
                ..Default::default()
            },
            pipeline_barrier.memory_barriers[0].src_stage_mask
        );
        assert_eq!(
            AccessMask {
                memory_read: true,
                ..Default::default()
            },
            pipeline_barrier.memory_barriers[0].src_access_mask
        );
        assert_eq!(
            PipelineStages {
                color_attachment_output: true,
                ..Default::default()
            },
            pipeline_barrier.memory_barriers[0].dst_stage_mask
        );
        assert_eq!(
            AccessMask {
                color_attachment_write: true,
                ..Default::default()
            },
            pipeline_barrier.memory_barriers[0].dst_access_mask
        );

        assert_eq!(
            PipelineStages {
                transfer: true,
                ..Default::default()
            },
            pipeline_barrier.buffer_memory_barriers[0].src_stage_mask
        );
        assert_eq!(
            AccessMask {
                transfer_write: true,
                ..Default::default()
            },
            pipeline_barrier.buffer_memory_barriers[0].src_access_mask
        );
        assert_eq!(
            PipelineStages {
                vertex_shader: true,
                ..Default::default()
            },
            pipeline_barrier.buffer_memory_barriers[0].dst_stage_mask
        );
        assert_eq!(
            AccessMask {
                uniform_read: true,
                ..Default::default()
            },
            pipeline_barrier.buffer_memory_barriers[0].dst_access_mask
        );
        assert_eq!(
            0,
            pipeline_barrier.buffer_memory_barriers[0].src_queue_family_index
        );
        assert_eq!(
            1,
            pipeline_barrier.buffer_memory_barriers[0].dst_queue_family_index
        );
        assert_eq!(
            buffer.handle(),
            pipeline_barrier.buffer_memory_barriers[0].buffer.handle()
        );
        assert_eq!(8, pipeline_barrier.buffer_memory_barriers[0].offset);
        assert_eq!(16, pipeline_barrier.buffer_memory_barriers[0].size);

        assert_eq!(
            PipelineStages {
                transfer: true,
                ..Default::default()
            },
            pipeline_barrier.image_memory_barriers[0].src_stage_mask
        );
        assert_eq!(
            AccessMask {
                transfer_write: true,
                ..Default::default()
            },
            pipeline_barrier.image_memory_barriers[0].src_access_mask
        );
        assert_eq!(
            PipelineStages {
                fragment_shader: true,
                ..Default::default()
            },
            pipeline_barrier.image_memory_barriers[0].dst_stage_mask
        );
        assert_eq!(
            AccessMask {
                shader_sampled_read: true,
                ..Default::default()
            },
            pipeline_barrier.image_memory_barriers[0].dst_access_mask
        );
        assert_eq!(
            0,
            pipeline_barrier.image_memory_barriers[0].src_queue_family_index
        );
        assert_eq!(
            1,
            pipeline_barrier.image_memory_barriers[0].dst_queue_family_index
        );
        assert_eq!(
            ImageLayout::Undefined,
            pipeline_barrier.image_memory_barriers[0].old_layout
        );
        assert_eq!(
            ImageLayout::ShaderReadOnlyOptimal,
            pipeline_barrier.image_memory_barriers[0].new_layout
        );
        assert_eq!(
            image.handle(),
            pipeline_barrier.image_memory_barriers[0].image.handle()
        );
    }

    #[test]
    fn memory_barrier_to_memory_barrier2() {
        let result = MemoryBarrier2::from(
            MemoryBarrier::builder()
                .src_stage_mask(PipelineStages {
                    top_of_pipe: true,
                    ..Default::default()
                })
                .src_access_mask(AccessMask {
                    memory_read: true,
                    ..Default::default()
                })
                .dst_stage_mask(PipelineStages {
                    color_attachment_output: true,
                    ..Default::default()
                })
                .dst_access_mask(AccessMask {
                    color_attachment_write: true,
                    ..Default::default()
                })
                .build(),
        );

        assert_eq!(PipelineStageFlags2::TOP_OF_PIPE, result.src_stage_mask);
        assert_eq!(AccessFlags2::MEMORY_READ, result.src_access_mask);
        assert_eq!(
            PipelineStageFlags2::COLOR_ATTACHMENT_OUTPUT,
            result.dst_stage_mask
        );
        assert_eq!(AccessFlags2::COLOR_ATTACHMENT_WRITE, result.dst_access_mask);
    }

    #[test]
    fn buffer_memory_barrier_to_buffer_memory_barrier2() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let buffer = Buffer::with_size(device.clone(), 24)
            .usage(BufferUsage {
                storage_buffer: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .build()
            .unwrap();

        let result = ash::vk::BufferMemoryBarrier2::from(
            BufferMemoryBarrier::builder(buffer.clone())
                .src_stage_mask(PipelineStages {
                    transfer: true,
                    ..Default::default()
                })
                .src_access_mask(AccessMask {
                    transfer_write: true,
                    ..Default::default()
                })
                .dst_stage_mask(PipelineStages {
                    vertex_shader: true,
                    ..Default::default()
                })
                .dst_access_mask(AccessMask {
                    uniform_read: true,
                    ..Default::default()
                })
                .src_queue_family_index(0)
                .dst_queue_family_index(1)
                .offset(8)
                .size(16)
                .build(),
        );

        assert_eq!(PipelineStageFlags2::TRANSFER, result.src_stage_mask);
        assert_eq!(AccessFlags2::TRANSFER_WRITE, result.src_access_mask);
        assert_eq!(PipelineStageFlags2::VERTEX_SHADER, result.dst_stage_mask);
        assert_eq!(AccessFlags2::UNIFORM_READ, result.dst_access_mask);
        assert_eq!(0, result.src_queue_family_index);
        assert_eq!(1, result.dst_queue_family_index);
        assert_eq!(buffer.handle(), result.buffer);
        assert_eq!(8, result.offset);
        assert_eq!(16, result.size);
    }

    #[test]
    fn image_memory_barrier_to_image_memory_barrier_2() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let image = Image::builder(device.clone())
            .image_type(ImageType::Type2D)
            .format(Format::R8G8B8A8Srgb)
            .extent(Extent3D {
                width: 1024,
                height: 1024,
                depth: 1,
            })
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCount::Sample1)
            .tiling(ImageTiling::Optimal)
            .usage(ImageUsage {
                sampled: true,
                transfer_dst: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .initial_layout(ImageLayout::Undefined)
            .build()
            .unwrap();

        let result = ash::vk::ImageMemoryBarrier2::from(
            ImageMemoryBarrier::builder(image.clone())
                .src_stage_mask(PipelineStages {
                    transfer: true,
                    ..Default::default()
                })
                .src_access_mask(AccessMask {
                    transfer_write: true,
                    ..Default::default()
                })
                .dst_stage_mask(PipelineStages {
                    fragment_shader: true,
                    ..Default::default()
                })
                .dst_access_mask(AccessMask {
                    shader_sampled_read: true,
                    ..Default::default()
                })
                .old_layout(ImageLayout::Undefined)
                .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                .src_queue_family_index(0)
                .dst_queue_family_index(1)
                .subresource_range(
                    ImageSubresourceRange::builder()
                        .aspect_mask(ImageAspects {
                            color: true,
                            ..Default::default()
                        })
                        .base_mip_level(0)
                        .level_count(1)
                        .base_array_layer(0)
                        .layer_count(1)
                        .build(),
                )
                .build(),
        );

        assert_eq!(PipelineStageFlags2::TRANSFER, result.src_stage_mask);
        assert_eq!(AccessFlags2::TRANSFER_WRITE, result.src_access_mask);
        assert_eq!(PipelineStageFlags2::FRAGMENT_SHADER, result.dst_stage_mask);
        assert_eq!(AccessFlags2::SHADER_SAMPLED_READ, result.dst_access_mask);
        assert_eq!(ash::vk::ImageLayout::UNDEFINED, result.old_layout);
        assert_eq!(
            ash::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            result.new_layout
        );
        assert_eq!(0, result.src_queue_family_index);
        assert_eq!(1, result.dst_queue_family_index);
        assert_eq!(image.handle(), result.image);
        assert_eq!(
            ash::vk::ImageAspectFlags::COLOR,
            result.subresource_range.aspect_mask
        );
        assert_eq!(0, result.subresource_range.base_mip_level);
        assert_eq!(1, result.subresource_range.level_count);
        assert_eq!(0, result.subresource_range.base_array_layer);
        assert_eq!(1, result.subresource_range.layer_count);
    }
}
