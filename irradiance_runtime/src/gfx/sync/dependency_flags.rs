use crate::impl_flag_set;

impl_flag_set! {
    DependencyFlags,
    doc = "How execution and memory dependencies are formed.",
    (VK_DEPENDENCY_BY_REGION_BIT, by_region),
    (VK_DEPENDENCY_DEVICE_GROUP_BIT, device_group),
    (VK_DEPENDENCY_VIEW_LOCAL_BIT, view_local)
}

#[doc(hidden)]
impl From<DependencyFlags> for ash::vk::DependencyFlags {
    fn from(dependency_flags: DependencyFlags) -> Self {
        let mut flags = ash::vk::DependencyFlags::empty();
        if dependency_flags.by_region {
            flags |= ash::vk::DependencyFlags::BY_REGION;
        }
        if dependency_flags.device_group {
            flags |= ash::vk::DependencyFlags::DEVICE_GROUP;
        }
        if dependency_flags.view_local {
            flags |= ash::vk::DependencyFlags::VIEW_LOCAL;
        }
        flags
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::sync::dependency_flags::DependencyFlags;

    #[test]
    fn to_dependency_flags() {
        assert_eq!(
            ash::vk::DependencyFlags::BY_REGION | ash::vk::DependencyFlags::VIEW_LOCAL,
            ash::vk::DependencyFlags::from(DependencyFlags {
                by_region: true,
                device_group: false,
                view_local: true
            })
        );
        assert_eq!(
            ash::vk::DependencyFlags::DEVICE_GROUP,
            ash::vk::DependencyFlags::from(DependencyFlags {
                by_region: false,
                device_group: true,
                view_local: false
            })
        );
    }
}
