use std::cell::UnsafeCell;
use std::sync::{Arc, Weak};

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::queue::Queue;
use crate::gfx::sync::fence_status::FenceStatus;
use crate::gfx::{GfxError, Handle, ObjectType};

/// A fence object.
#[derive(Debug)]
pub struct Fence {
    queue: UnsafeCell<Option<Weak<Queue>>>,
    handle: ash::vk::Fence,
    device: Arc<Device>,
}

impl Fence {
    /// Starts building a new fence.
    pub fn builder(device: Arc<Device>) -> FenceBuilder {
        FenceBuilder {
            status: Default::default(),
            device,
        }
    }

    /// Returns `true` if the fence is signaled.
    ///
    /// # Errors
    /// This function returns an error if querying for the fence status fails.
    pub fn is_signaled(&self) -> Result<bool, GfxError> {
        Ok(unsafe { self.device.inner().get_fence_status(self.handle)? })
    }

    /// Waits for the fence to become signaled.
    ///
    /// # Errors
    /// This function returns an error when waiting for the fence to become signaled fails.
    pub fn wait(&self) -> Result<(), GfxError> {
        unsafe {
            self.device
                .inner()
                .wait_for_fences(&[self.handle], true, u64::MAX)?;
        }

        Ok(())
    }

    /// Resets the fence.
    ///
    /// # Errors
    /// This function returns an error when resetting the fence fails.
    pub fn reset(&self) -> Result<(), GfxError> {
        self.remove_from_queue();

        unsafe {
            self.device.inner().reset_fences(&[self.handle])?;
        }

        Ok(())
    }

    pub(crate) fn set_queue(&self, queue: Weak<Queue>) {
        *unsafe { &mut *self.queue.get() } = Some(queue);
    }

    fn remove_from_queue(&self) {
        if let Some(queue) = unsafe { &mut *self.queue.get() } {
            queue.upgrade().unwrap().remove_fence(self);
        }
    }
}

impl Drop for Fence {
    fn drop(&mut self) {
        unsafe {
            self.device.inner().destroy_fence(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for Fence {
    type Type = ash::vk::Fence;

    fn object_type(&self) -> ObjectType {
        ObjectType::Fence
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for Fence {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`Fence`].
#[derive(Debug)]
pub struct FenceBuilder {
    status: FenceStatus,
    device: Arc<Device>,
}

impl FenceBuilder {
    /// Sets the initial fence status.
    pub fn status(&mut self, status: FenceStatus) -> &mut Self {
        self.status = status;
        self
    }

    /// Builds the [`Fence`].
    ///
    /// # Errors
    /// This function returns an error if the fence creation fails.
    pub fn build(&mut self) -> Result<Arc<Fence>, GfxError> {
        let create_info = ash::vk::FenceCreateInfo::builder()
            .flags(self.status.into())
            .build();

        let handle = unsafe { self.device.inner().create_fence(&create_info, None) }?;

        Ok(Arc::new(Fence {
            queue: UnsafeCell::new(None),
            handle,
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::sync::{Fence, FenceStatus};
    use crate::physical_device;

    #[test]
    fn build_fence() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();

        let result = Fence::builder(device.clone()).build();

        assert!(result.is_ok());
    }

    #[test]
    fn is_not_signaled() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let fence = Fence::builder(device.clone()).build().unwrap();

        assert!(!fence.is_signaled().unwrap());
    }

    #[test]
    fn is_signaled() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let fence = Fence::builder(device.clone())
            .status(FenceStatus { signaled: true })
            .build()
            .unwrap();

        assert!(fence.is_signaled().unwrap());
    }

    #[test]
    fn wait() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let fence = Fence::builder(device.clone())
            .status(FenceStatus { signaled: true })
            .build()
            .unwrap();

        let result = fence.wait();

        assert!(result.is_ok());
    }

    #[test]
    fn reset() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let fence = Fence::builder(device.clone())
            .status(FenceStatus { signaled: true })
            .build()
            .unwrap();

        let result = fence.reset();

        assert!(result.is_ok());
    }
}
