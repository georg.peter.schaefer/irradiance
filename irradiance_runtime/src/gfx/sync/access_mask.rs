use crate::impl_flag_set;

impl_flag_set! {
    AccessMask,
    doc = "Barrier access behaviour.",
    (VK_ACCESS_2_INDIRECT_COMMAND_READ_BIT, indirect_command_read),
    (VK_ACCESS_2_INDEX_READ_BIT, index_read),
    (VK_ACCESS_2_VERTEX_ATTRIBUTE_READ_BIT, vertex_attribute_read),
    (VK_ACCESS_2_UNIFORM_READ_BIT, uniform_read),
    (VK_ACCESS_2_INPUT_ATTACHMENT_READ_BIT, input_attachment_read),
    (VK_ACCESS_2_SHADER_READ_BIT, shader_read),
    (VK_ACCESS_2_SHADER_WRITE_BIT, shader_write),
    (VK_ACCESS_2_COLOR_ATTACHMENT_READ_BIT, color_attachment_read),
    (VK_ACCESS_2_COLOR_ATTACHMENT_WRITE_BIT, color_attachment_write),
    (VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_READ_BIT, depth_stencil_attachment_read),
    (VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, depth_stencil_attachment_write),
    (VK_ACCESS_2_TRANSFER_READ_BIT, transfer_read),
    (VK_ACCESS_2_TRANSFER_WRITE_BIT, transfer_write),
    (VK_ACCESS_2_HOST_READ_BIT, host_read),
    (VK_ACCESS_2_HOST_WRITE_BIT, host_write),
    (VK_ACCESS_2_MEMORY_READ_BIT, memory_read),
    (VK_ACCESS_2_MEMORY_WRITE_BIT, memory_write),
    (VK_ACCESS_2_SHADER_SAMPLED_READ_BIT, shader_sampled_read),
    (VK_ACCESS_2_SHADER_STORAGE_READ_BIT, shader_storage_read),
    (VK_ACCESS_2_SHADER_STORAGE_WRITE_BIT, shader_storage_write),
    (VK_ACCESS_2_VIDEO_DECODE_READ_BIT_KHR, video_decode_read),
    (VK_ACCESS_2_VIDEO_DECODE_WRITE_BIT_KHR, video_decode_write),
    (VK_ACCESS_2_VIDEO_ENCODE_READ_BIT_KHR, video_encode_read),
    (VK_ACCESS_2_VIDEO_ENCODE_WRITE_BIT_KHR, video_encode_write),
    (VK_ACCESS_2_TRANSFORM_FEEDBACK_WRITE_BIT_EXT, transform_feedback_write),
    (VK_ACCESS_2_TRANSFORM_FEEDBACK_COUNTER_READ_BIT_EXT, transform_feedback_counter_read),
    (VK_ACCESS_2_TRANSFORM_FEEDBACK_COUNTER_WRITE_BIT_EXT, transform_feedback_counter_write),
    (VK_ACCESS_2_CONDITIONAL_RENDERING_READ_BIT_EXT, conditional_rendering_read),
    (VK_ACCESS_2_COMMAND_PREPROCESS_READ_BIT_NV, command_preprocess_read),
    (VK_ACCESS_2_COMMAND_PREPROCESS_WRITE_BIT_NV, command_preprocess_write),
    (VK_ACCESS_2_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR, fragment_shading_rate_attachment_read),
    (VK_ACCESS_2_SHADING_RATE_IMAGE_READ_BIT_NV, shading_rate_image_read),
    (VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR, acceleration_structure_read),
    (VK_ACCESS_2_ACCELERATION_STRUCTURE_WRITE_BIT_KHR, acceleration_structure_write),
    (VK_ACCESS_2_FRAGMENT_DENSITY_MAP_READ_BIT_EXT, fragment_density_map_read),
    (VK_ACCESS_2_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT, color_attachment_read_noncoherent),
    (VK_ACCESS_2_INVOCATION_MASK_READ_BIT_HUAWEI, invocation_mask_read)
}

#[doc(hidden)]
impl From<AccessMask> for ash::vk::AccessFlags2 {
    fn from(access_mask: AccessMask) -> Self {
        let mut flags = ash::vk::AccessFlags2::empty();
        if access_mask.indirect_command_read {
            flags |= ash::vk::AccessFlags2::INDIRECT_COMMAND_READ;
        }
        if access_mask.index_read {
            flags |= ash::vk::AccessFlags2::INDEX_READ;
        }
        if access_mask.vertex_attribute_read {
            flags |= ash::vk::AccessFlags2::VERTEX_ATTRIBUTE_READ;
        }
        if access_mask.uniform_read {
            flags |= ash::vk::AccessFlags2::UNIFORM_READ;
        }
        if access_mask.input_attachment_read {
            flags |= ash::vk::AccessFlags2::INPUT_ATTACHMENT_READ;
        }
        if access_mask.shader_read {
            flags |= ash::vk::AccessFlags2::SHADER_READ;
        }
        if access_mask.shader_write {
            flags |= ash::vk::AccessFlags2::SHADER_WRITE;
        }
        if access_mask.color_attachment_read {
            flags |= ash::vk::AccessFlags2::COLOR_ATTACHMENT_READ;
        }
        if access_mask.color_attachment_write {
            flags |= ash::vk::AccessFlags2::COLOR_ATTACHMENT_WRITE;
        }
        if access_mask.depth_stencil_attachment_read {
            flags |= ash::vk::AccessFlags2::DEPTH_STENCIL_ATTACHMENT_READ;
        }
        if access_mask.depth_stencil_attachment_write {
            flags |= ash::vk::AccessFlags2::DEPTH_STENCIL_ATTACHMENT_WRITE;
        }
        if access_mask.transfer_read {
            flags |= ash::vk::AccessFlags2::TRANSFER_READ;
        }
        if access_mask.transfer_write {
            flags |= ash::vk::AccessFlags2::TRANSFER_WRITE;
        }
        if access_mask.host_read {
            flags |= ash::vk::AccessFlags2::HOST_READ;
        }
        if access_mask.host_write {
            flags |= ash::vk::AccessFlags2::HOST_WRITE;
        }
        if access_mask.memory_read {
            flags |= ash::vk::AccessFlags2::MEMORY_READ;
        }
        if access_mask.memory_write {
            flags |= ash::vk::AccessFlags2::MEMORY_WRITE;
        }
        if access_mask.shader_sampled_read {
            flags |= ash::vk::AccessFlags2::SHADER_SAMPLED_READ;
        }
        if access_mask.shader_storage_read {
            flags |= ash::vk::AccessFlags2::SHADER_STORAGE_READ;
        }
        if access_mask.shader_storage_write {
            flags |= ash::vk::AccessFlags2::SHADER_STORAGE_WRITE;
        }
        if access_mask.video_decode_read {
            flags |= ash::vk::AccessFlags2::VIDEO_DECODE_READ_KHR;
        }
        if access_mask.video_decode_write {
            flags |= ash::vk::AccessFlags2::VIDEO_DECODE_WRITE_KHR;
        }
        if access_mask.video_encode_read {
            flags |= ash::vk::AccessFlags2::VIDEO_ENCODE_READ_KHR;
        }
        if access_mask.video_encode_write {
            flags |= ash::vk::AccessFlags2::VIDEO_ENCODE_WRITE_KHR;
        }
        if access_mask.transform_feedback_write {
            flags |= ash::vk::AccessFlags2::TRANSFORM_FEEDBACK_WRITE_EXT;
        }
        if access_mask.transform_feedback_counter_read {
            flags |= ash::vk::AccessFlags2::TRANSFORM_FEEDBACK_COUNTER_READ_EXT;
        }
        if access_mask.transform_feedback_counter_write {
            flags |= ash::vk::AccessFlags2::TRANSFORM_FEEDBACK_COUNTER_WRITE_EXT;
        }
        if access_mask.conditional_rendering_read {
            flags |= ash::vk::AccessFlags2::CONDITIONAL_RENDERING_READ_EXT;
        }
        if access_mask.command_preprocess_read {
            flags |= ash::vk::AccessFlags2::COMMAND_PREPROCESS_READ_NV;
        }
        if access_mask.command_preprocess_write {
            flags |= ash::vk::AccessFlags2::COMMAND_PREPROCESS_WRITE_NV;
        }
        if access_mask.fragment_shading_rate_attachment_read {
            flags |= ash::vk::AccessFlags2::FRAGMENT_SHADING_RATE_ATTACHMENT_READ_KHR;
        }
        if access_mask.shading_rate_image_read {
            flags |= ash::vk::AccessFlags2::SHADING_RATE_IMAGE_READ_NV;
        }
        if access_mask.acceleration_structure_read {
            flags |= ash::vk::AccessFlags2::ACCELERATION_STRUCTURE_READ_KHR;
        }
        if access_mask.acceleration_structure_write {
            flags |= ash::vk::AccessFlags2::ACCELERATION_STRUCTURE_WRITE_KHR;
        }
        if access_mask.fragment_density_map_read {
            flags |= ash::vk::AccessFlags2::FRAGMENT_DENSITY_MAP_READ_EXT;
        }
        if access_mask.color_attachment_read_noncoherent {
            flags |= ash::vk::AccessFlags2::COLOR_ATTACHMENT_READ_NONCOHERENT_EXT;
        }
        if access_mask.invocation_mask_read {
            flags |= ash::vk::AccessFlags2::INVOCATION_MASK_READ_HUAWEI;
        }
        flags
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::sync::AccessMask;

    #[test]
    fn to_access_flags() {
        assert_eq!(
            ash::vk::AccessFlags2::INDEX_READ
                | ash::vk::AccessFlags2::UNIFORM_READ
                | ash::vk::AccessFlags2::SHADER_READ
                | ash::vk::AccessFlags2::COLOR_ATTACHMENT_READ
                | ash::vk::AccessFlags2::DEPTH_STENCIL_ATTACHMENT_READ
                | ash::vk::AccessFlags2::TRANSFER_READ
                | ash::vk::AccessFlags2::HOST_READ
                | ash::vk::AccessFlags2::MEMORY_READ
                | ash::vk::AccessFlags2::SHADER_SAMPLED_READ
                | ash::vk::AccessFlags2::SHADER_STORAGE_WRITE
                | ash::vk::AccessFlags2::VIDEO_DECODE_WRITE_KHR
                | ash::vk::AccessFlags2::VIDEO_ENCODE_WRITE_KHR
                | ash::vk::AccessFlags2::TRANSFORM_FEEDBACK_COUNTER_READ_EXT
                | ash::vk::AccessFlags2::CONDITIONAL_RENDERING_READ_EXT
                | ash::vk::AccessFlags2::COMMAND_PREPROCESS_WRITE_NV
                | ash::vk::AccessFlags2::SHADING_RATE_IMAGE_READ_NV
                | ash::vk::AccessFlags2::ACCELERATION_STRUCTURE_WRITE_KHR
                | ash::vk::AccessFlags2::COLOR_ATTACHMENT_READ_NONCOHERENT_EXT,
            ash::vk::AccessFlags2::from(AccessMask {
                indirect_command_read: false,
                index_read: true,
                vertex_attribute_read: false,
                uniform_read: true,
                input_attachment_read: false,
                shader_read: true,
                shader_write: false,
                color_attachment_read: true,
                color_attachment_write: false,
                depth_stencil_attachment_read: true,
                depth_stencil_attachment_write: false,
                transfer_read: true,
                transfer_write: false,
                host_read: true,
                host_write: false,
                memory_read: true,
                memory_write: false,
                shader_sampled_read: true,
                shader_storage_read: false,
                shader_storage_write: true,
                video_decode_read: false,
                video_decode_write: true,
                video_encode_read: false,
                video_encode_write: true,
                transform_feedback_write: false,
                transform_feedback_counter_read: true,
                transform_feedback_counter_write: false,
                conditional_rendering_read: true,
                command_preprocess_read: false,
                command_preprocess_write: true,
                fragment_shading_rate_attachment_read: false,
                shading_rate_image_read: true,
                acceleration_structure_read: false,
                acceleration_structure_write: true,
                fragment_density_map_read: false,
                color_attachment_read_noncoherent: true,
                invocation_mask_read: false
            })
        );
        assert_eq!(
            ash::vk::AccessFlags2::INDIRECT_COMMAND_READ
                | ash::vk::AccessFlags2::VERTEX_ATTRIBUTE_READ
                | ash::vk::AccessFlags2::INPUT_ATTACHMENT_READ
                | ash::vk::AccessFlags2::SHADER_WRITE
                | ash::vk::AccessFlags2::COLOR_ATTACHMENT_WRITE
                | ash::vk::AccessFlags2::DEPTH_STENCIL_ATTACHMENT_WRITE
                | ash::vk::AccessFlags2::TRANSFER_WRITE
                | ash::vk::AccessFlags2::HOST_WRITE
                | ash::vk::AccessFlags2::MEMORY_WRITE
                | ash::vk::AccessFlags2::SHADER_STORAGE_READ
                | ash::vk::AccessFlags2::VIDEO_DECODE_READ_KHR
                | ash::vk::AccessFlags2::VIDEO_ENCODE_READ_KHR
                | ash::vk::AccessFlags2::TRANSFORM_FEEDBACK_WRITE_EXT
                | ash::vk::AccessFlags2::TRANSFORM_FEEDBACK_COUNTER_WRITE_EXT
                | ash::vk::AccessFlags2::COMMAND_PREPROCESS_READ_NV
                | ash::vk::AccessFlags2::FRAGMENT_SHADING_RATE_ATTACHMENT_READ_KHR
                | ash::vk::AccessFlags2::ACCELERATION_STRUCTURE_READ_KHR
                | ash::vk::AccessFlags2::FRAGMENT_DENSITY_MAP_READ_EXT
                | ash::vk::AccessFlags2::INVOCATION_MASK_READ_HUAWEI,
            ash::vk::AccessFlags2::from(AccessMask {
                indirect_command_read: true,
                index_read: false,
                vertex_attribute_read: true,
                uniform_read: false,
                input_attachment_read: true,
                shader_read: false,
                shader_write: true,
                color_attachment_read: false,
                color_attachment_write: true,
                depth_stencil_attachment_read: false,
                depth_stencil_attachment_write: true,
                transfer_read: false,
                transfer_write: true,
                host_read: false,
                host_write: true,
                memory_read: false,
                memory_write: true,
                shader_sampled_read: false,
                shader_storage_read: true,
                shader_storage_write: false,
                video_decode_read: true,
                video_decode_write: false,
                video_encode_read: true,
                video_encode_write: false,
                transform_feedback_write: true,
                transform_feedback_counter_read: false,
                transform_feedback_counter_write: true,
                conditional_rendering_read: false,
                command_preprocess_read: true,
                command_preprocess_write: false,
                fragment_shading_rate_attachment_read: true,
                shading_rate_image_read: false,
                acceleration_structure_read: true,
                acceleration_structure_write: false,
                fragment_density_map_read: true,
                color_attachment_read_noncoherent: false,
                invocation_mask_read: true
            })
        );
    }
}
