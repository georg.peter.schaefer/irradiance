use crate::gfx::memory::{Allocation, Memory};
use std::sync::Arc;

/// Manual or managed memory allocation.
#[derive(Debug)]
pub enum Allocated {
    /// Managed allocation.
    Managed(Arc<Allocation>),
    /// Manual allocation.
    Manual(Arc<Memory>),
}

impl Allocated {
    /// Returns the memory.
    pub fn memory(&self) -> Arc<Memory> {
        match self {
            Allocated::Managed(allocation) => allocation.memory(),
            Allocated::Manual(memory) => memory.clone(),
        }
    }

    /// Returns the offset.
    pub fn offset(&self) -> u64 {
        match self {
            Allocated::Managed(allocation) => allocation.offset(),
            Allocated::Manual(_) => 0,
        }
    }
}
