use std::sync::{Arc, Mutex};

use gpu_allocator::vulkan::AllocatorCreateDesc;

use crate::gfx::device::Device;
use crate::gfx::memory::{Allocation, MemoryLocation, MemoryRequirements};
use crate::gfx::GfxError;

/// Type for allocating graphics memory.
#[derive(Debug)]
pub struct Allocator {
    inner: Mutex<gpu_allocator::vulkan::Allocator>,
}

impl Allocator {
    pub(crate) fn new(
        instance: ash::Instance,
        physical_device: ash::vk::PhysicalDevice,
        device: ash::Device,
    ) -> Result<Arc<Self>, GfxError> {
        Ok(Arc::new(Self {
            inner: Mutex::new(gpu_allocator::vulkan::Allocator::new(
                &AllocatorCreateDesc {
                    instance,
                    device,
                    physical_device,
                    debug_settings: Default::default(),
                    buffer_device_address: false,
                },
            )?),
        }))
    }

    /// Allocates memory.
    ///
    /// # Errors
    /// This function returns an error if the allocation fails.
    pub fn allocate(
        self: &Arc<Self>,
        device: Arc<Device>,
        name: &str,
        requirements: MemoryRequirements,
        location: MemoryLocation,
        linear: bool,
    ) -> Result<Arc<Allocation>, GfxError> {
        Ok(Arc::new(Allocation {
            inner: Some(self.inner.lock().unwrap().allocate(
                &gpu_allocator::vulkan::AllocationCreateDesc {
                    name,
                    requirements: requirements.into(),
                    location: location.into(),
                    linear,
                },
            )?),
            allocator: self.clone(),
            device,
        }))
    }

    pub(crate) fn free(&self, allocation: &mut Allocation) {
        self.inner
            .lock()
            .unwrap()
            .free(allocation.inner.take().unwrap())
            .unwrap();
    }
}
