/// Memory requirements.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct MemoryRequirements {
    size: u64,
    alignment: u64,
    memory_type_bits: u32,
}

impl MemoryRequirements {
    /// Starts building new memory requirements.
    pub fn builder() -> MemoryRequirementsBuilder {
        MemoryRequirementsBuilder {
            size: 0,
            alignment: 0,
            memory_type_bits: 0,
        }
    }
}

#[doc(hidden)]
impl From<ash::vk::MemoryRequirements> for MemoryRequirements {
    fn from(memory_requirements: ash::vk::MemoryRequirements) -> Self {
        Self {
            size: memory_requirements.size,
            alignment: memory_requirements.alignment,
            memory_type_bits: memory_requirements.memory_type_bits,
        }
    }
}

#[doc(hidden)]
impl From<MemoryRequirements> for ash::vk::MemoryRequirements {
    fn from(memory_requirements: MemoryRequirements) -> Self {
        ash::vk::MemoryRequirements::builder()
            .size(memory_requirements.size)
            .alignment(memory_requirements.alignment)
            .memory_type_bits(memory_requirements.memory_type_bits)
            .build()
    }
}

/// Type for building [`MemoryRequirements`].
#[derive(Debug)]
pub struct MemoryRequirementsBuilder {
    size: u64,
    alignment: u64,
    memory_type_bits: u32,
}

impl MemoryRequirementsBuilder {
    /// Sets the size.
    pub fn size(&mut self, size: u64) -> &mut Self {
        self.size = size;
        self
    }

    /// Sets the alignment.
    pub fn alignment(&mut self, alignment: u64) -> &mut Self {
        self.alignment = alignment;
        self
    }

    /// Sets the memory type bits.
    pub fn memory_type_bits(&mut self, memory_type_bits: u32) -> &mut Self {
        self.memory_type_bits = memory_type_bits;
        self
    }

    /// Builds the [`MemoryRequirements`].
    pub fn build(&mut self) -> MemoryRequirements {
        MemoryRequirements {
            size: self.size,
            alignment: self.alignment,
            memory_type_bits: self.memory_type_bits,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::memory::MemoryRequirements;

    #[test]
    fn from_memory_requirements() {
        assert_eq!(
            MemoryRequirements::builder()
                .size(64)
                .alignment(16)
                .memory_type_bits(42)
                .build(),
            MemoryRequirements::from(
                ash::vk::MemoryRequirements::builder()
                    .size(64)
                    .alignment(16)
                    .memory_type_bits(42)
                    .build()
            )
        );
    }

    #[test]
    fn to_memory_requirements() {
        let result = ash::vk::MemoryRequirements::from(
            MemoryRequirements::builder()
                .size(64)
                .alignment(16)
                .memory_type_bits(42)
                .build(),
        );

        assert_eq!(64, result.size);
        assert_eq!(16, result.alignment);
        assert_eq!(42, result.memory_type_bits);
    }
}
