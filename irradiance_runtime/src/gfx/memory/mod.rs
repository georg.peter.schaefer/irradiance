//! Memory and allocator.

pub use allocated::Allocated;
pub use allocation::Allocation;
pub use allocator::Allocator;
pub use memory_location::MemoryLocation;
pub use memory_requirements::MemoryRequirements;
pub use memory_requirements::MemoryRequirementsBuilder;
pub use private::Memory;
pub use private::MemoryBuilder;

mod allocated;
mod allocation;
mod allocator;
mod memory_location;
mod memory_requirements;
mod private;
