use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::memory::MemoryRequirements;
use crate::gfx::{GfxError, Handle, ObjectType};

/// Device memory.
#[derive(Clone, Debug)]
pub struct Memory {
    size: u64,
    managed: bool,
    handle: ash::vk::DeviceMemory,
    device: Arc<Device>,
}

impl Memory {
    pub(crate) fn managed(device: Arc<Device>, handle: ash::vk::DeviceMemory) -> Arc<Self> {
        Arc::new(Self {
            size: 0,
            managed: true,
            handle,
            device,
        })
    }

    /// Starts building a new memory allocation.
    pub fn builder(device: Arc<Device>, memory_requirements: MemoryRequirements) -> MemoryBuilder {
        MemoryBuilder {
            exported: false,
            memory_requirements,
            device,
        }
    }

    /// Returns the size.
    pub fn size(&self) -> u64 {
        self.size
    }

    /// Returns the shared memory handle.
    #[cfg(target_os = "linux")]
    pub fn exported_handle(&self) -> i32 {
        let loader = ash::extensions::khr::ExternalMemoryFd::new(
            self.device.physical_device().instance().inner(),
            self.device.inner(),
        );
        let get_info = ash::vk::MemoryGetFdInfoKHR::builder()
            .handle_type(ash::vk::ExternalMemoryHandleTypeFlags::OPAQUE_FD)
            .memory(self.handle)
            .build();
        unsafe { loader.get_memory_fd(&get_info).unwrap() }
    }

    /// Returns the shared memory handle.
    #[cfg(target_os = "windows")]
    pub fn exported_handle(&self) -> *mut std::ffi::c_void {
        let loader = ash::extensions::khr::ExternalMemoryWin32::new(
            self.device.physical_device().instance().inner(),
            self.device.inner(),
        );
        let get_info = ash::vk::MemoryGetWin32HandleInfoKHR::builder()
            .handle_type(ash::vk::ExternalMemoryHandleTypeFlags::OPAQUE_WIN32)
            .memory(self.handle)
            .build();
        unsafe { loader.get_memory_win32_handle(&get_info).unwrap() }
    }
}

impl Drop for Memory {
    fn drop(&mut self) {
        if !self.managed {
            unsafe {
                self.device.inner().free_memory(self.handle, None);
            }
        }
    }
}

#[doc(hidden)]
impl Handle for Memory {
    type Type = ash::vk::DeviceMemory;

    fn object_type(&self) -> ObjectType {
        ObjectType::DeviceMemory
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for Memory {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building [`Memory`].
#[derive(Debug)]
pub struct MemoryBuilder {
    exported: bool,
    memory_requirements: MemoryRequirements,
    device: Arc<Device>,
}

impl MemoryBuilder {
    /// Exported memory.
    pub fn exported(&mut self) -> &mut Self {
        self.exported = true;
        self
    }

    /// Builds the [`Memory`].
    pub fn build(&mut self) -> Result<Arc<Memory>, GfxError> {
        let memory_requirements = ash::vk::MemoryRequirements::from(self.memory_requirements);
        let memory_type_index = self.find_memory_type().unwrap();
        #[cfg(target_os = "linux")]
        let mut external_allocation_info = ash::vk::ExportMemoryAllocateInfo::builder()
            .handle_types(ash::vk::ExternalMemoryHandleTypeFlags::OPAQUE_FD)
            .build();
        #[cfg(target_os = "windows")]
        let mut external_allocation_info = ash::vk::ExportMemoryAllocateInfo::builder()
            .handle_types(ash::vk::ExternalMemoryHandleTypeFlags::OPAQUE_WIN32)
            .build();
        let mut allocate_info_builder = ash::vk::MemoryAllocateInfo::builder()
            .allocation_size(memory_requirements.size)
            .memory_type_index(memory_type_index);

        if self.exported {
            allocate_info_builder = allocate_info_builder.push_next(&mut external_allocation_info);
        }

        let allocate_info = allocate_info_builder.build();

        unsafe {
            Ok(Arc::new(Memory {
                size: memory_requirements.size,
                managed: false,
                handle: self.device.inner().allocate_memory(&allocate_info, None)?,
                device: self.device.clone(),
            }))
        }
    }

    fn find_memory_type(&self) -> Option<u32> {
        let memory_requirements = ash::vk::MemoryRequirements::from(self.memory_requirements);
        let physical_device = self.device.physical_device();
        let instance = physical_device.instance();
        let memory_properties = unsafe {
            instance
                .inner()
                .get_physical_device_memory_properties(physical_device.handle())
        };
        for memory_index in 0..memory_properties.memory_type_count {
            let memory_type_bits = 1u32 << memory_index;
            let is_required_memory_type =
                memory_requirements.memory_type_bits & memory_type_bits == memory_type_bits;

            let properties = memory_properties.memory_types[memory_index as usize].property_flags;
            let has_required_properties = properties & ash::vk::MemoryPropertyFlags::DEVICE_LOCAL
                == ash::vk::MemoryPropertyFlags::DEVICE_LOCAL;

            if is_required_memory_type && has_required_properties {
                return Some(memory_index);
            }
        }
        None
    }
}
