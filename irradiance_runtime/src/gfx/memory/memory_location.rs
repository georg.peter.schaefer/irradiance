/// Memory locations.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum MemoryLocation {
    /// The allocated resource is stored in an unknown location.
    Unknown,
    /// Store the allocation in GPU only memory.
    GpuOnly,
    /// Memory useful for uploading data to the GPU.
    CpuToGpu,
    /// Memory useful for reading data back from the GPU.
    GpuToCpu,
}

#[doc(hidden)]
impl From<MemoryLocation> for gpu_allocator::MemoryLocation {
    fn from(memory_location: MemoryLocation) -> Self {
        match memory_location {
            MemoryLocation::Unknown => gpu_allocator::MemoryLocation::Unknown,
            MemoryLocation::GpuOnly => gpu_allocator::MemoryLocation::GpuOnly,
            MemoryLocation::CpuToGpu => gpu_allocator::MemoryLocation::CpuToGpu,
            MemoryLocation::GpuToCpu => gpu_allocator::MemoryLocation::GpuToCpu,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::memory::memory_location::MemoryLocation;

    #[test]
    fn to_memory_location() {
        assert_eq!(
            gpu_allocator::MemoryLocation::Unknown,
            gpu_allocator::MemoryLocation::from(MemoryLocation::Unknown)
        );
        assert_eq!(
            gpu_allocator::MemoryLocation::GpuOnly,
            gpu_allocator::MemoryLocation::from(MemoryLocation::GpuOnly)
        );
        assert_eq!(
            gpu_allocator::MemoryLocation::CpuToGpu,
            gpu_allocator::MemoryLocation::from(MemoryLocation::CpuToGpu)
        );
        assert_eq!(
            gpu_allocator::MemoryLocation::GpuToCpu,
            gpu_allocator::MemoryLocation::from(MemoryLocation::GpuToCpu)
        );
    }
}
