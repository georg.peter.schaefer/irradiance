use std::sync::Arc;

use crate::gfx::device::Device;
use crate::gfx::memory::{Allocator, Memory};

/// A device memory allocation.
#[derive(Debug)]
pub struct Allocation {
    pub(crate) inner: Option<gpu_allocator::vulkan::Allocation>,
    pub(crate) allocator: Arc<Allocator>,
    pub(crate) device: Arc<Device>,
}

impl Allocation {
    /// Returns the memory.
    pub fn memory(&self) -> Arc<Memory> {
        unsafe { Memory::managed(self.device.clone(), self.inner.as_ref().unwrap().memory()) }
    }

    /// Returns the offset.
    pub fn offset(&self) -> u64 {
        self.inner.as_ref().unwrap().offset()
    }

    /// Returns the size.
    pub fn size(&self) -> u64 {
        self.inner.as_ref().unwrap().size()
    }

    #[doc(hidden)]
    pub fn inner(&self) -> &gpu_allocator::vulkan::Allocation {
        self.inner.as_ref().unwrap()
    }
}

impl Drop for Allocation {
    fn drop(&mut self) {
        self.allocator.clone().free(self);
    }
}
