//! Graphics API instance.
//!
//! An [`Instance`] is the main graphics API object.
//!
//! # Creating an `Instance`
//! Creating an [`Instance`] loads the graphics API library ans enables requested
//! [`InstanceExtensions`].
//!
//! ```
//! use irradiance_runtime::gfx::instance::{Instance, ApplicationInfo, InstanceExtensions};
//!
//! Instance::new(
//!     ApplicationInfo::new((1, 3, 0).into())
//!         .application_name("super awesome game")
//! )
//! .extensions(InstanceExtensions {
//!     ext_debug_utils: true,
//!     khr_surface: true,
//!     ..Default::default()
//! })
//! .build();
//! ```

pub use application_info::ApplicationInfo;
pub use extensions::InstanceExtensions;
pub use private::Instance;

mod application_info;
mod extensions;
mod private;
