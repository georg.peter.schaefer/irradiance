use crate::impl_extensions;

impl_extensions! {
    InstanceExtensions,
    doc = "Instance extensions.",
    (VK_KHR_surface, khr_surface),
    (VK_KHR_display, khr_display),
    (VK_KHR_xlib_surface, khr_xlib_surface),
    (VK_KHR_xcb_surface, khr_xcb_surface),
    (VK_KHR_wayland_surface, khr_wayland_surface),
    (VK_KHR_android_surface, khr_android_surface),
    (VK_KHR_win32_surface, khr_win32_surface),
    (VK_EXT_debug_report, ext_debug_report),
    (VK_GGP_stream_descriptor_surface, ggp_stream_descriptor_surface),
    (VK_NV_external_memory_capabilities, nv_external_memory_capabilities),
    (VK_KHR_get_physical_device_properties2, khr_get_physical_device_properties2),
    (VK_EXT_validation_flags, ext_validation_flags),
    (VK_NN_vi_surface, nn_vi_surface),
    (VK_KHR_device_group_creation, khr_device_group_creation),
    (VK_KHR_external_memory_capabilities, khr_external_memory_capabilities),
    (VK_KHR_external_semaphore_capabilities, khr_external_semaphore_capabilities),
    (VK_EXT_direct_mode_display, ext_direct_mode_display),
    (VK_EXT_acquire_xlib_display, ext_acquire_xlib_display),
    (VK_EXT_display_surface_counter, ext_display_surface_counter),
    (VK_EXT_swapchain_colorspace, ext_swapchain_colorspace),
    (VK_KHR_external_fence_capabilities, khr_external_fence_capabilities),
    (VK_KHR_get_surface_capabilities2, khr_get_surface_capabilities2),
    (VK_KHR_get_display_properties2, khr_get_display_properties2),
    (VK_MVK_ios_surface, mvk_ios_surface),
    (VK_MVK_macos_surface, mvk_macos_surface),
    (VK_EXT_debug_utils, ext_debug_utils),
    (VK_FUCHSIA_imagepipe_surface, fuchsia_imagepipe_surface),
    (VK_EXT_metal_surface, ext_metal_surface),
    (VK_KHR_surface_protected_capabilities, khr_surface_protected_capabilities),
    (VK_EXT_validation_features, ext_validation_features),
    (VK_EXT_headless_surface, ext_headless_surface),
    (VK_EXT_acquire_drm_display, ext_acquire_drm_display),
    (VK_EXT_directfb_surface, ext_directfb_surface),
    (VK_QNX_screen_surface, qnx_screen_surface),
    (VK_GOOGLE_surfaceless_query, google_surfaceless_query)
}

#[cfg(test)]
mod tests {
    use crate::gfx::instance::InstanceExtensions;

    #[test]
    fn default() {
        assert_eq!(
            InstanceExtensions {
                khr_surface: false,
                khr_display: false,
                khr_xlib_surface: false,
                khr_xcb_surface: false,
                khr_wayland_surface: false,
                khr_android_surface: false,
                khr_win32_surface: false,
                ext_debug_report: false,
                ggp_stream_descriptor_surface: false,
                nv_external_memory_capabilities: false,
                khr_get_physical_device_properties2: false,
                ext_validation_flags: false,
                nn_vi_surface: false,
                khr_device_group_creation: false,
                khr_external_memory_capabilities: false,
                khr_external_semaphore_capabilities: false,
                ext_direct_mode_display: false,
                ext_acquire_xlib_display: false,
                ext_display_surface_counter: false,
                ext_swapchain_colorspace: false,
                khr_external_fence_capabilities: false,
                khr_get_surface_capabilities2: false,
                khr_get_display_properties2: false,
                mvk_ios_surface: false,
                mvk_macos_surface: false,
                ext_debug_utils: false,
                fuchsia_imagepipe_surface: false,
                ext_metal_surface: false,
                khr_surface_protected_capabilities: false,
                ext_validation_features: false,
                ext_headless_surface: false,
                ext_acquire_drm_display: false,
                ext_directfb_surface: false,
                qnx_screen_surface: false,
                google_surfaceless_query: false
            },
            Default::default()
        );
    }

    #[test]
    fn none() {
        assert_eq!(InstanceExtensions::default(), InstanceExtensions::none());
    }

    #[test]
    fn is_not_a_subset() {
        let a = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            nv_external_memory_capabilities: true,
            ..Default::default()
        };
        let b = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn is_subset() {
        let a = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            nv_external_memory_capabilities: true,
            ..Default::default()
        };
        let b = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            ..Default::default()
        };
        assert!(b.is_subset(&a));
    }

    #[test]
    fn is_not_a_superset() {
        let a = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            nv_external_memory_capabilities: true,
            ..Default::default()
        };
        let b = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            ..Default::default()
        };
        assert!(!b.is_superset(&a));
    }

    #[test]
    fn is_superset() {
        let a = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            nv_external_memory_capabilities: true,
            ..Default::default()
        };
        let b = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            ..Default::default()
        };
        assert!(a.is_superset(&b));
    }

    #[test]
    fn difference() {
        let a = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            nv_external_memory_capabilities: true,
            ..Default::default()
        };
        let b = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            ..Default::default()
        };
        assert_eq!(
            InstanceExtensions {
                nv_external_memory_capabilities: true,
                ..Default::default()
            },
            a.difference(&b)
        );
    }

    #[test]
    fn intersection() {
        let a = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            nv_external_memory_capabilities: true,
            ..Default::default()
        };
        let b = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            ..Default::default()
        };
        assert_eq!(
            InstanceExtensions {
                khr_xlib_surface: true,
                ext_debug_utils: true,
                ..Default::default()
            },
            a.intersection(&b)
        );
    }

    #[test]
    fn union() {
        let a = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            nv_external_memory_capabilities: true,
            ..Default::default()
        };
        let b = InstanceExtensions {
            khr_xlib_surface: true,
            ext_debug_utils: true,
            google_surfaceless_query: true,
            ..Default::default()
        };
        assert_eq!(
            InstanceExtensions {
                khr_xlib_surface: true,
                ext_debug_utils: true,
                nv_external_memory_capabilities: true,
                google_surfaceless_query: true,
                ..Default::default()
            },
            a.union(&b)
        );
    }
}
