use std::ffi::CStr;
#[cfg(all(debug_assertions, not(test), not(doctest)))]
use std::ffi::CString;
use std::fmt;
use std::fmt::{Debug, Formatter};
use std::sync::Arc;

use crate::gfx::instance::{ApplicationInfo, InstanceExtensions};
use crate::gfx::physical_device::PhysicalDevice;
use crate::gfx::GfxError;

/// The main Vulkan object.
pub struct Instance {
    #[cfg(all(debug_assertions, not(test), not(doctest)))]
    debug_utils_messenger: ash::vk::DebugUtilsMessengerEXT,
    debug_utils: ash::extensions::ext::DebugUtils,
    inner: ash::Instance,
    entry: ash::Entry,
}

impl Instance {
    /// Starts building a new Vulkan `Instance`.
    pub fn builder(application_info: ApplicationInfo) -> InstanceBuilder {
        InstanceBuilder {
            application_info,
            extensions: None,
        }
    }

    #[doc(hidden)]
    pub fn entry(&self) -> &ash::Entry {
        &self.entry
    }

    #[doc(hidden)]
    pub fn inner(&self) -> &ash::Instance {
        &self.inner
    }

    /// Returns the [`InstanceExtensions`] supported by the instance.
    pub fn extensions() -> Result<InstanceExtensions, GfxError> {
        Ok(InstanceExtensions::from(
            unsafe { ash::Entry::load() }?
                .enumerate_instance_extension_properties(None)?
                .iter()
                .map(|property| unsafe { CStr::from_ptr(property.extension_name.as_ptr()) }),
        ))
    }

    /// Returns the [`PhysicalDevice`]s.
    ///
    /// # Errors
    /// This function returns an error if enumerating the physical devices fails.
    pub fn physical_devices(self: &Arc<Self>) -> Result<Vec<Arc<PhysicalDevice>>, GfxError> {
        Ok(unsafe { self.inner.enumerate_physical_devices() }?
            .iter()
            .map(|inner| PhysicalDevice::new(self.clone(), *inner))
            .collect())
    }

    pub(crate) fn debug_utils(&self) -> &ash::extensions::ext::DebugUtils {
        &self.debug_utils
    }
}

impl Drop for Instance {
    fn drop(&mut self) {
        unsafe {
            #[cfg(all(debug_assertions, not(test), not(doctest)))]
            self.debug_utils
                .destroy_debug_utils_messenger(self.debug_utils_messenger, None);
            self.inner.destroy_instance(None);
        }
    }
}

impl Debug for Instance {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("Instance")
            .field("handle", &self.inner.handle())
            .finish()
    }
}

/// Type for building an [`Instance`].
#[derive(Debug)]
pub struct InstanceBuilder {
    application_info: ApplicationInfo,
    extensions: Option<InstanceExtensions>,
}

impl InstanceBuilder {
    /// Sets the [`Extensions`] that will be enabled.
    pub fn extensions(&mut self, extensions: InstanceExtensions) -> &mut Self {
        self.extensions = Some(extensions);
        self
    }

    /// Builds the [`Instance`].
    ///
    /// # Errors
    /// This function returns an error if the instance creation fails.
    pub fn build(&self) -> Result<Arc<Instance>, GfxError> {
        let entry = unsafe { ash::Entry::load() }?;
        let application_info = From::from(&self.application_info);
        let extensions = Vec::from(self.extensions.unwrap_or_default().union(
            &InstanceExtensions {
                ext_debug_utils: true,
                ..Default::default()
            },
        ));
        let extensions_ptr: Vec<_> = extensions
            .iter()
            .map(|extension_name| extension_name.as_ptr())
            .collect();
        #[allow(unused_mut)]
        let mut create_info = ash::vk::InstanceCreateInfo::builder()
            .application_info(&application_info)
            .enabled_extension_names(&extensions_ptr);

        #[cfg(all(debug_assertions, not(test), not(doctest)))]
        let layers = vec![CString::new("VK_LAYER_KHRONOS_validation").unwrap()];
        #[cfg(all(debug_assertions, not(test), not(doctest)))]
        let layers_ptr = layers
            .iter()
            .map(|layer| layer.as_ptr())
            .collect::<Vec<_>>();
        #[cfg(all(debug_assertions, not(test), not(doctest)))]
        let mut debug_callback_create_info = Self::create_debug_messenger_create_info();
        #[cfg(all(debug_assertions, not(test), not(doctest)))]
        {
            create_info = create_info.enabled_layer_names(&layers_ptr);
            create_info = create_info.push_next(&mut debug_callback_create_info);
        }

        let inner = unsafe { entry.create_instance(&create_info, None) }?;

        let debug_utils = ash::extensions::ext::DebugUtils::new(&entry, &inner);
        #[cfg(all(debug_assertions, not(test), not(doctest)))]
        let debug_utils_messenger =
            unsafe { debug_utils.create_debug_utils_messenger(&debug_callback_create_info, None) }?;

        Ok(Arc::new(Instance {
            #[cfg(all(debug_assertions, not(test), not(doctest)))]
            debug_utils_messenger,
            debug_utils,
            inner,
            entry,
        }))
    }

    #[cfg(all(debug_assertions, not(test), not(doctest)))]
    fn create_debug_messenger_create_info() -> ash::vk::DebugUtilsMessengerCreateInfoEXT {
        ash::vk::DebugUtilsMessengerCreateInfoEXT::builder()
            .message_severity(
                ash::vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE
                    | ash::vk::DebugUtilsMessageSeverityFlagsEXT::INFO
                    | ash::vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
                    | ash::vk::DebugUtilsMessageSeverityFlagsEXT::ERROR,
            )
            .message_type(
                ash::vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION
                    | ash::vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
                    | ash::vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE,
            )
            .pfn_user_callback(Some(debug_callback))
            .build()
    }
}

#[cfg(all(debug_assertions, not(test), not(doctest)))]
unsafe extern "system" fn debug_callback(
    message_severity: ash::vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: ash::vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const ash::vk::DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut std::os::raw::c_void,
) -> ash::vk::Bool32 {
    let severity = match message_severity {
        ash::vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE => "verbose",
        ash::vk::DebugUtilsMessageSeverityFlagsEXT::INFO => "info",
        ash::vk::DebugUtilsMessageSeverityFlagsEXT::WARNING => "warning",
        ash::vk::DebugUtilsMessageSeverityFlagsEXT::ERROR => "error",
        _ => unreachable!("unrecognized DebugUtilsMessageSeverityFlagsEXT"),
    };
    let message_type = match message_type {
        ash::vk::DebugUtilsMessageTypeFlagsEXT::GENERAL => "general",
        ash::vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE => "performance",
        ash::vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION => "validation",
        _ => unreachable!("unrecognized DebugUtilsMessageTypeFlagsEXT"),
    };
    let data = *p_callback_data;
    let message = CStr::from_ptr(data.p_message).to_str().unwrap();

    log::debug!("[{}][{}] {:?}", severity, message_type, message);

    ash::vk::FALSE
}

#[doc(hidden)]
impl From<&ApplicationInfo> for ash::vk::ApplicationInfo {
    fn from(application_info: &ApplicationInfo) -> Self {
        let mut builder = ash::vk::ApplicationInfo::builder();
        if let Some(application_name) = &application_info.application_name {
            builder = builder.application_name(application_name);
        }
        if let Some(application_version) = application_info.application_version {
            builder = builder.application_version(application_version.into());
        }
        if let Some(engine_name) = &application_info.engine_name {
            builder = builder.application_name(engine_name);
        }
        if let Some(engine_version) = application_info.engine_version {
            builder = builder.application_version(engine_version.into());
        }
        builder
            .api_version(application_info.api_version.into())
            .build()
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::instance::{ApplicationInfo, Instance, InstanceExtensions};
    use crate::gfx::GfxError;

    #[macro_export]
    macro_rules! instance {
        () => {
            crate::gfx::instance::Instance::builder(std::default::Default::default())
                .build()
                .unwrap()
        };
    }

    #[test]
    fn extensions() {
        let extensions = Instance::extensions().unwrap();
        println!("supported instance extensions: {}", extensions);

        assert!(InstanceExtensions {
            khr_surface: true,
            khr_get_physical_device_properties2: true,
            khr_device_group_creation: true,
            khr_external_memory_capabilities: true,
            khr_external_semaphore_capabilities: true,
            khr_external_fence_capabilities: true,
            ext_debug_utils: true,
            ..Default::default()
        }
        .is_subset(&extensions),);
    }

    #[test]
    fn create_instance_extension_missing() {
        let result = Instance::builder(ApplicationInfo::default())
            .extensions(InstanceExtensions {
                khr_win32_surface: true,
                khr_wayland_surface: true,
                ext_metal_surface: true,
                ..Default::default()
            })
            .build();
        assert!(matches!(result, Err(GfxError::ExtensionNotPresent),));
    }

    #[test]
    fn create_instance() {
        let result = Instance::builder(
            ApplicationInfo::new((1, 3, 0).into())
                .application_name("test")
                .application_version(Default::default())
                .engine_name("irradiance")
                .engine_version(Default::default()),
        )
        .extensions(InstanceExtensions {
            ext_debug_utils: true,
            ..Default::default()
        })
        .build();

        assert!(matches!(result, Ok(_)));
    }

    #[test]
    fn physical_devices() {
        let instance = Instance::builder(Default::default()).build().unwrap();
        let result = instance.physical_devices();

        assert!(matches!(result, Ok(physical_devices) if !physical_devices.is_empty()));
    }
}
