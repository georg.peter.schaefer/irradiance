use std::ffi::CString;

use crate::gfx::version::Version;

/// Information about the application.
#[derive(Debug)]
pub struct ApplicationInfo {
    pub(super) application_name: Option<CString>,
    pub(super) application_version: Option<Version>,
    pub(super) engine_name: Option<CString>,
    pub(super) engine_version: Option<Version>,
    pub(super) api_version: Version,
}

impl ApplicationInfo {
    /// Creates a new `ApplicationInfo` with the desired Vulkan-API-version.
    pub fn new(api_version: Version) -> Self {
        Self {
            application_name: None,
            application_version: None,
            engine_name: None,
            engine_version: None,
            api_version,
        }
    }

    /// Sets the name for the application.
    pub fn application_name(mut self, application_name: &str) -> Self {
        self.application_name = Some(CString::new(application_name).unwrap());
        self
    }

    /// Sets the version for the application.
    pub fn application_version(mut self, application_version: Version) -> Self {
        self.application_version = Some(application_version);
        self
    }

    /// Sets the name for the engine.
    pub fn engine_name(mut self, engine_name: &str) -> Self {
        self.engine_name = Some(CString::new(engine_name).unwrap());
        self
    }

    /// Sets the version for the engine.
    pub fn engine_version(mut self, engine_version: Version) -> Self {
        self.engine_version = Some(engine_version);
        self
    }
}

impl Default for ApplicationInfo {
    fn default() -> Self {
        Self {
            application_name: None,
            application_version: None,
            engine_name: None,
            engine_version: None,
            api_version: (1, 3, 0).into(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::ffi::CString;

    use crate::gfx::instance::application_info::ApplicationInfo;
    use crate::gfx::version::Version;

    #[test]
    fn build_application_info() {
        let application_info = ApplicationInfo::new((1, 3, 0).into())
            .application_name("test application")
            .application_version((0, 1, 0).into())
            .engine_name("irradiance")
            .engine_version((1, 2, 3).into());

        assert_eq!(
            Some(CString::new("test application").unwrap()),
            application_info.application_name
        );
        assert_eq!(Some((0, 1, 0).into()), application_info.application_version);
        assert_eq!(
            Some(CString::new("irradiance").unwrap()),
            application_info.engine_name
        );
        assert_eq!(Some((1, 2, 3).into()), application_info.engine_version);
        assert_eq!(Version::from((1, 3, 0)), application_info.api_version);
    }

    #[test]
    fn default() {
        let application_info = ApplicationInfo::default();

        assert_eq!(None, application_info.application_name);
        assert_eq!(None, application_info.application_version);
        assert_eq!(None, application_info.engine_name);
        assert_eq!(None, application_info.engine_version);
        assert_eq!(Version::from((1, 3, 0)), application_info.api_version);
    }
}
