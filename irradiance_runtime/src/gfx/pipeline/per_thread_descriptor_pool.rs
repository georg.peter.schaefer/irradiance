use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
    thread::{self, ThreadId},
};

use crate::gfx::{device::Device, GfxError};

use super::{DescriptorPool, DescriptorSetLayout};

/// Per thread auto growing descriptor pool.
#[derive(Debug)]
pub struct PerThreadDescriptorPool {
    descriptor_pools: Mutex<HashMap<ThreadId, DescriptorPools>>,
    device: Arc<Device>,
}

impl PerThreadDescriptorPool {
    /// Creates a new per thread auto growing descriptor pool.
    pub fn new(device: Arc<Device>) -> Self {
        Self {
            descriptor_pools: Mutex::new(Default::default()),
            device,
        }
    }

    /// Gets the [`DescriptorPool`] for the current thread.
    pub fn get(
        &self,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<DescriptorPool>, GfxError> {
        let mut descriptor_pools = self.descriptor_pools.lock().unwrap();

        if let Some(descriptor_pools) = descriptor_pools.get_mut(&thread::current().id()) {
            descriptor_pools
        } else {
            descriptor_pools
                .entry(thread::current().id())
                .or_insert_with(|| DescriptorPools::new(self.device.clone()))
        }
        .get(descriptor_set_layout)
    }
}

#[derive(Debug)]
struct DescriptorPools {
    descriptor_pools: Vec<Arc<DescriptorPool>>,
    device: Arc<Device>,
}

impl DescriptorPools {
    fn new(device: Arc<Device>) -> Self {
        Self {
            descriptor_pools: vec![],
            device,
        }
    }

    fn get(
        &mut self,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<DescriptorPool>, GfxError> {
        if let Some(descriptor_pool) = self
            .descriptor_pools
            .iter()
            .find(|descriptor_pool| descriptor_pool.can_allocate(descriptor_set_layout))
        {
            Ok(descriptor_pool.clone())
        } else {
            let mut builder = DescriptorPool::builder(self.device.clone());
            builder.max_sets(100);
            for (descriptor_type, count) in descriptor_set_layout.requirements.iter() {
                builder.pool_size(*descriptor_type, 100 * *count);
            }
            let descriptor_pool = builder.build()?;
            self.descriptor_pools.push(descriptor_pool.clone());
            Ok(descriptor_pool)
        }
    }
}
