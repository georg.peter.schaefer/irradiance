/// Shader stage specialization constants.
#[derive(Clone, Debug, Default)]
pub struct Specialization {
    data: Vec<u8>,
    map_entries: Vec<ash::vk::SpecializationMapEntry>,
}

impl Specialization {
    /// Starts building a new specialization.
    pub fn builder() -> SpecializationBuilder {
        SpecializationBuilder {
            data: vec![],
            map_entries: vec![],
        }
    }

    pub(crate) fn as_ffi(&self) -> ash::vk::SpecializationInfo {
        ash::vk::SpecializationInfo::builder()
            .map_entries(&self.map_entries)
            .data(&self.data)
            .build()
    }
}

/// Type for building a [`Specialization`].
#[derive(Debug)]
pub struct SpecializationBuilder {
    data: Vec<u8>,
    map_entries: Vec<ash::vk::SpecializationMapEntry>,
}

impl SpecializationBuilder {
    /// Adds a specialization constant entry.
    pub fn entry(&mut self, constant_id: u32, offset: u32, size: usize) -> &mut Self {
        self.map_entries.push(
            ash::vk::SpecializationMapEntry::builder()
                .constant_id(constant_id)
                .offset(offset)
                .size(size)
                .build(),
        );
        self
    }

    /// Sets the specialization constants data.
    pub fn data<T: Clone>(&mut self, data: T) -> &mut Self {
        unsafe {
            self.data = Vec::from(std::slice::from_raw_parts(
                (&data as *const T) as *const u8,
                std::mem::size_of::<T>(),
            ));
        }
        self
    }

    /// Builds the [`Specialization`].
    pub fn build(&mut self) -> Specialization {
        Specialization {
            data: self.data.clone(),
            map_entries: self.map_entries.clone(),
        }
    }
}
