/// The rate at which vertex attributes are pulled from buffers.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum VertexInputRate {
    /// Vertex attribute addressing is a function of the vertex index.
    Vertex,
    /// Vertex attribute addressing is a function of the instance index.
    Instance,
}

#[doc(hidden)]
impl From<VertexInputRate> for ash::vk::VertexInputRate {
    fn from(vertex_input_rate: VertexInputRate) -> Self {
        match vertex_input_rate {
            VertexInputRate::Vertex => ash::vk::VertexInputRate::VERTEX,
            VertexInputRate::Instance => ash::vk::VertexInputRate::INSTANCE,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::vertex_input_rate::VertexInputRate;

    #[test]
    fn to_vertex_input_rate() {
        assert_eq!(
            ash::vk::VertexInputRate::VERTEX,
            ash::vk::VertexInputRate::from(VertexInputRate::Vertex)
        );
        assert_eq!(
            ash::vk::VertexInputRate::INSTANCE,
            ash::vk::VertexInputRate::from(VertexInputRate::Instance)
        );
    }
}
