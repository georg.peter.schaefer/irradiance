use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::pipeline::{DescriptorSetLayout, ShaderStages};
use crate::gfx::{GfxError, Handle, ObjectType};

/// Gives access to descriptor sets from a pipeline.
#[derive(Debug)]
pub struct PipelineLayout {
    handle: ash::vk::PipelineLayout,
    device: Arc<Device>,
}

impl PipelineLayout {
    /// Starts building a new pipeline layout.
    pub fn builder(device: Arc<Device>) -> PipelineLayoutBuilder {
        PipelineLayoutBuilder {
            push_constant_ranges: vec![],
            set_layouts: vec![],
            device,
        }
    }
}

impl Drop for PipelineLayout {
    fn drop(&mut self) {
        unsafe {
            self.device
                .inner()
                .destroy_pipeline_layout(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for PipelineLayout {
    type Type = ash::vk::PipelineLayout;

    fn object_type(&self) -> ObjectType {
        ObjectType::PipelineLayout
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for PipelineLayout {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`PipelineLayout`].
#[derive(Debug)]
pub struct PipelineLayoutBuilder {
    push_constant_ranges: Vec<ash::vk::PushConstantRange>,
    set_layouts: Vec<Arc<DescriptorSetLayout>>,
    device: Arc<Device>,
}

impl PipelineLayoutBuilder {
    /// Adds a [`DescriptorSetLayout`].
    pub fn set_layout(&mut self, set_layout: Arc<DescriptorSetLayout>) -> &mut Self {
        self.set_layouts.push(set_layout);
        self
    }

    /// Adds a push constant range.
    pub fn push_constant_range(
        &mut self,
        stages: ShaderStages,
        offset: u32,
        size: u32,
    ) -> &mut Self {
        self.push_constant_ranges.push(
            ash::vk::PushConstantRange::builder()
                .stage_flags(stages.into())
                .offset(offset)
                .size(size)
                .build(),
        );
        self
    }

    /// Builds the [`PipelineLayout`].
    ///
    /// # Errors
    /// This function returns an error if the pipeline layout creation fails.
    pub fn build(&mut self) -> Result<Arc<PipelineLayout>, GfxError> {
        let set_layouts = self
            .set_layouts
            .iter()
            .map(|set_layout| set_layout.handle())
            .collect::<Vec<_>>();
        let create_info = ash::vk::PipelineLayoutCreateInfo::builder()
            .set_layouts(&set_layouts)
            .push_constant_ranges(&self.push_constant_ranges)
            .build();

        let handle = unsafe {
            self.device
                .inner()
                .create_pipeline_layout(&create_info, None)
        }?;

        Ok(Arc::new(PipelineLayout {
            handle,
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::pipeline::{DescriptorSetLayout, DescriptorType, PipelineLayout, ShaderStages};
    use crate::physical_device;

    #[test]
    fn build_pipeline_layout() {
        let physical_device = physical_device!();
        let (device, _) = Device::builder(physical_device.clone(), Default::default())
            .build()
            .unwrap();
        let descriptor_set_layout = DescriptorSetLayout::builder(device.clone())
            .binding(
                0,
                DescriptorType::UniformBuffer,
                1,
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
            )
            .binding(
                1,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
            .unwrap();

        let result = PipelineLayout::builder(device.clone())
            .set_layout(descriptor_set_layout.clone())
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                24,
            )
            .build();

        assert!(result.is_ok());
    }
}
