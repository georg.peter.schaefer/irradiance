/// Supported primitive topologies.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum PrimitiveTopology {
    /// A series of separate point primitives.
    PointList,
    /// A series of separate line primitives.
    LineList,
    /// A series of connected line primitives with consecutive lines sharing a vertex.
    LineStrip,
    /// A series of separate triangle primitives.
    TriangleList,
    /// A series of connected triangle primitives with consecutive triangles sharing an edge.
    TriangleStrip,
    /// A series of connected triangle primitives with all triangles sharing a common vertex.
    TriangleFan,
    /// A series of separate line primitives with adjacency.
    LineListWithAdjacency,
    /// A series of connected line primitives with adjacency, with consecutive primitives sharing
    /// three vertices.
    LineStripWithAdjacency,
    /// A series of separate triangle primitives with adjacency.
    TriangleListWithAdjacency,
    /// Connected triangle primitives with adjacency, with consecutive triangles sharing an edge.
    TriangleStripWithAdjacency,
    /// Separate patch primitives.
    PatchList,
}

#[doc(hidden)]
impl From<PrimitiveTopology> for ash::vk::PrimitiveTopology {
    fn from(primitive_topology: PrimitiveTopology) -> Self {
        match primitive_topology {
            PrimitiveTopology::PointList => ash::vk::PrimitiveTopology::POINT_LIST,
            PrimitiveTopology::LineList => ash::vk::PrimitiveTopology::LINE_LIST,
            PrimitiveTopology::LineStrip => ash::vk::PrimitiveTopology::LINE_STRIP,
            PrimitiveTopology::TriangleList => ash::vk::PrimitiveTopology::TRIANGLE_LIST,
            PrimitiveTopology::TriangleStrip => ash::vk::PrimitiveTopology::TRIANGLE_STRIP,
            PrimitiveTopology::TriangleFan => ash::vk::PrimitiveTopology::TRIANGLE_FAN,
            PrimitiveTopology::LineListWithAdjacency => {
                ash::vk::PrimitiveTopology::LINE_LIST_WITH_ADJACENCY
            }
            PrimitiveTopology::LineStripWithAdjacency => {
                ash::vk::PrimitiveTopology::LINE_STRIP_WITH_ADJACENCY
            }
            PrimitiveTopology::TriangleListWithAdjacency => {
                ash::vk::PrimitiveTopology::TRIANGLE_LIST_WITH_ADJACENCY
            }
            PrimitiveTopology::TriangleStripWithAdjacency => {
                ash::vk::PrimitiveTopology::TRIANGLE_STRIP_WITH_ADJACENCY
            }
            PrimitiveTopology::PatchList => ash::vk::PrimitiveTopology::PATCH_LIST,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::PrimitiveTopology;

    #[test]
    fn to_primitive_topology() {
        assert_eq!(
            ash::vk::PrimitiveTopology::POINT_LIST,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::PointList)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::LINE_LIST,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::LineList)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::LINE_STRIP,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::LineStrip)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::TRIANGLE_LIST,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::TriangleList)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::TRIANGLE_STRIP,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::TriangleStrip)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::TRIANGLE_FAN,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::TriangleFan)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::LINE_LIST_WITH_ADJACENCY,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::LineListWithAdjacency)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::LINE_STRIP_WITH_ADJACENCY,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::LineStripWithAdjacency)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::TRIANGLE_LIST_WITH_ADJACENCY,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::TriangleListWithAdjacency)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::TRIANGLE_STRIP_WITH_ADJACENCY,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::TriangleStripWithAdjacency)
        );
        assert_eq!(
            ash::vk::PrimitiveTopology::PATCH_LIST,
            ash::vk::PrimitiveTopology::from(PrimitiveTopology::PatchList)
        );
    }
}
