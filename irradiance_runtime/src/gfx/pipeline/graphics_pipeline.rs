use std::ffi::CString;
use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::Format;
use crate::gfx::pipeline::{
    ColorBlendAttachmentState, CompareOp, CullMode, DynamicState, FrontFace, PipelineLayout,
    PrimitiveTopology, ShaderModule, ShaderStages, Specialization, VertexInput, Viewport,
};
use crate::gfx::{GfxError, Handle, ObjectType, Rect2D};

/// Graphics pipeline.
#[derive(Debug)]
pub struct GraphicsPipeline {
    handle: ash::vk::Pipeline,
    device: Arc<Device>,
}

impl GraphicsPipeline {
    /// Starts building a new `GraphicsPipeline`.
    pub fn builder(device: Arc<Device>) -> GraphicsPipelineBuilder {
        GraphicsPipelineBuilder {
            stencil_attachment_format: None,
            depth_attachment_format: None,
            color_attachment_formats: vec![],
            layout: None,
            dynamic_states: vec![],
            color_blend_attachment_states: vec![],
            depth_clamp_enabled: false,
            depth_test: None,
            depth_bias: None,
            line_width: 1.0,
            front_face: FrontFace::CounterClockwise,
            cull_mode: CullMode::None,
            rasterizer_discard_enabled: false,
            scissors: vec![],
            viewports: vec![],
            topology: PrimitiveTopology::TriangleList,
            fragment_shader_specialization: None,
            fragment_shader: None,
            vertex_shader_specialization: None,
            vertex_shader: None,
            vertex_input: VertexInput::builder().build(),
            device,
        }
    }
}

impl Drop for GraphicsPipeline {
    fn drop(&mut self) {
        unsafe { self.device.inner().destroy_pipeline(self.handle, None) };
    }
}

#[doc(hidden)]
impl Handle for GraphicsPipeline {
    type Type = ash::vk::Pipeline;

    fn object_type(&self) -> ObjectType {
        ObjectType::Pipeline
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for GraphicsPipeline {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`GraphicsPipeline`].
#[derive(Debug)]
pub struct GraphicsPipelineBuilder {
    stencil_attachment_format: Option<ash::vk::Format>,
    depth_attachment_format: Option<ash::vk::Format>,
    color_attachment_formats: Vec<ash::vk::Format>,
    layout: Option<Arc<PipelineLayout>>,
    dynamic_states: Vec<ash::vk::DynamicState>,
    color_blend_attachment_states: Vec<ash::vk::PipelineColorBlendAttachmentState>,
    depth_test: Option<(bool, CompareOp)>,
    depth_bias: Option<(f32, f32)>,
    line_width: f32,
    front_face: FrontFace,
    cull_mode: CullMode,
    rasterizer_discard_enabled: bool,
    depth_clamp_enabled: bool,
    scissors: Vec<ash::vk::Rect2D>,
    viewports: Vec<ash::vk::Viewport>,
    topology: PrimitiveTopology,
    fragment_shader_specialization: Option<Specialization>,
    fragment_shader: Option<(Arc<ShaderModule>, CString)>,
    vertex_shader_specialization: Option<Specialization>,
    vertex_shader: Option<(Arc<ShaderModule>, CString)>,
    vertex_input: VertexInput,
    device: Arc<Device>,
}

impl GraphicsPipelineBuilder {
    /// Sets the vertex input.
    pub fn vertex_input(&mut self, vertex_input: VertexInput) -> &mut Self {
        self.vertex_input = vertex_input;
        self
    }

    /// Sets the vertex shader.
    pub fn vertex_shader(
        &mut self,
        vertex_shader: Arc<ShaderModule>,
        entry_point: &str,
    ) -> &mut Self {
        self.vertex_shader = Some((
            vertex_shader,
            CString::new(entry_point.to_string()).unwrap(),
        ));
        self
    }

    /// Sets the vertex shader specialization constants.
    pub fn vertex_shader_specialization(
        &mut self,
        vertex_shader_specialization: Specialization,
    ) -> &mut Self {
        self.vertex_shader_specialization = Some(vertex_shader_specialization);
        self
    }

    /// Sets the fragment shader.
    pub fn fragment_shader(
        &mut self,
        fragment_shader: Arc<ShaderModule>,
        entry_point: &str,
    ) -> &mut Self {
        self.fragment_shader = Some((
            fragment_shader,
            CString::new(entry_point.to_string()).unwrap(),
        ));
        self
    }

    /// Sets the fragment shader specialization constants.
    pub fn fragment_shader_specialization(
        &mut self,
        fragment_shader_specialization: Specialization,
    ) -> &mut Self {
        self.fragment_shader_specialization = Some(fragment_shader_specialization);
        self
    }

    /// Sets the primitive topology for the input assembly state.
    pub fn topology(&mut self, topology: PrimitiveTopology) -> &mut Self {
        self.topology = topology;
        self
    }

    /// Adds a viewport.
    pub fn viewport(&mut self, viewport: Viewport) -> &mut Self {
        self.viewports.push(viewport.into());
        self
    }

    /// Adds a scissor region.
    pub fn scissor(&mut self, scissor: Rect2D) -> &mut Self {
        self.scissors.push(scissor.into());
        self
    }

    /// Enables depth clamp.
    pub fn depth_clamp_enabled(&mut self) -> &mut Self {
        self.depth_clamp_enabled = true;
        self
    }

    /// Enables rasterizer discard.
    pub fn rasterizer_discard_enabled(&mut self) -> &mut Self {
        self.rasterizer_discard_enabled = true;
        self
    }

    /// Sets the cull mode for the rasterization state.
    pub fn cull_mode(&mut self, cull_mode: CullMode) -> &mut Self {
        self.cull_mode = cull_mode;
        self
    }

    /// Sets the polygon front-facing orientation for the rasterization state.
    pub fn front_face(&mut self, front_face: FrontFace) -> &mut Self {
        self.front_face = front_face;
        self
    }

    /// Enables depth bias.
    pub fn depth_bias(&mut self, constant_factor: f32, slope_factor: f32) -> &mut Self {
        self.depth_bias = Some((constant_factor, slope_factor));
        self
    }

    /// Sets the line width.
    pub fn line_width(&mut self, line_width: f32) -> &mut Self {
        self.line_width = line_width;
        self
    }

    /// Enables depth testing.
    pub fn depth_test(&mut self, write: bool, compare_op: CompareOp) -> &mut Self {
        self.depth_test = Some((write, compare_op));
        self
    }

    /// Adds a blend state.
    pub fn color_blend_attachment_state(
        &mut self,
        color_blend_attachment_state: ColorBlendAttachmentState,
    ) -> &mut Self {
        self.color_blend_attachment_states
            .push(color_blend_attachment_state.into());
        self
    }

    /// Adds a dynamic state.
    pub fn dynamic_state(&mut self, dynamic_state: DynamicState) -> &mut Self {
        self.dynamic_states.push(dynamic_state.into());
        self
    }

    /// Sets the [`PipelineLayout`].
    pub fn layout(&mut self, layout: Arc<PipelineLayout>) -> &mut Self {
        self.layout = Some(layout);
        self
    }

    /// Defines the format for a color attachment.
    pub fn color_attachment_format(&mut self, color_attachment_format: Format) -> &mut Self {
        self.color_attachment_formats
            .push(color_attachment_format.into());
        self
    }

    /// Defines the format for the depth attachment.
    pub fn depth_attachment_format(&mut self, depth_attachment_format: Format) -> &mut Self {
        self.depth_attachment_format = Some(depth_attachment_format.into());
        self
    }

    /// Defines the format for the stencil attachment.
    pub fn stencil_attachment_format(&mut self, stencil_attachment_format: Format) -> &mut Self {
        self.stencil_attachment_format = Some(stencil_attachment_format.into());
        self
    }

    /// Builds the [`GraphicsPipeline`].
    ///
    /// # Errors
    /// This function returns an error if the graphics pipeline creation fails.
    pub fn build(&mut self) -> Result<Arc<GraphicsPipeline>, GfxError> {
        let specializations = [
            self.vertex_shader_specialization
                .as_ref()
                .unwrap_or(&Default::default())
                .as_ffi(),
            self.fragment_shader_specialization
                .as_ref()
                .unwrap_or(&Default::default())
                .as_ffi(),
        ];
        let stages = self.stages(&specializations);
        let mut rendering = self.rendering();
        let create_info = [ash::vk::GraphicsPipelineCreateInfo::builder()
            .stages(&stages)
            .vertex_input_state(&self.vertex_input_state())
            .input_assembly_state(&self.input_assembly_state())
            .tessellation_state(&self.tesselation_state())
            .viewport_state(&self.viewport_state())
            .rasterization_state(&self.rasterization_state())
            .multisample_state(&self.multisample_state())
            .depth_stencil_state(&self.depth_stencil_state())
            .color_blend_state(&self.color_blend_state())
            .dynamic_state(&self._dynamic_state())
            .layout(self._layout())
            .push_next(&mut rendering)
            .build()];

        let handle = match unsafe {
            self.device.inner().create_graphics_pipelines(
                ash::vk::PipelineCache::null(),
                &create_info,
                None,
            )
        } {
            Ok(handles) => handles[0],
            Err((_, result)) => Err(result)?,
        };

        Ok(Arc::new(GraphicsPipeline {
            handle,
            device: self.device.clone(),
        }))
    }

    fn stages(
        &self,
        specializations: &[ash::vk::SpecializationInfo],
    ) -> Vec<ash::vk::PipelineShaderStageCreateInfo> {
        [
            (
                &self.vertex_shader,
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                &specializations[0],
            ),
            (
                &self.fragment_shader,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
                &specializations[1],
            ),
        ]
        .into_iter()
        .filter(|(shader, _, _)| shader.is_some())
        .map(|(shader, stage, specialization)| (shader.as_ref().unwrap(), stage, specialization))
        .map(|((module, entry_point), stage, specialization)| {
            ash::vk::PipelineShaderStageCreateInfo::builder()
                .stage(stage.into())
                .module(module.handle())
                .name(entry_point)
                .specialization_info(specialization)
                .build()
        })
        .collect()
    }

    fn vertex_input_state(&self) -> ash::vk::PipelineVertexInputStateCreateInfo {
        let vertex_input = &self.vertex_input;
        ash::vk::PipelineVertexInputStateCreateInfo::builder()
            .vertex_binding_descriptions(&vertex_input.vertex_binding_descriptions)
            .vertex_attribute_descriptions(&vertex_input.vertex_attribute_descriptions)
            .build()
    }

    fn input_assembly_state(&self) -> ash::vk::PipelineInputAssemblyStateCreateInfo {
        ash::vk::PipelineInputAssemblyStateCreateInfo::builder()
            .topology(self.topology.into())
            .build()
    }

    fn tesselation_state(&self) -> ash::vk::PipelineTessellationStateCreateInfo {
        Default::default()
    }

    fn viewport_state(&self) -> ash::vk::PipelineViewportStateCreateInfo {
        ash::vk::PipelineViewportStateCreateInfo::builder()
            .viewports(&self.viewports)
            .scissors(&self.scissors)
            .build()
    }

    fn rasterization_state(&self) -> ash::vk::PipelineRasterizationStateCreateInfo {
        ash::vk::PipelineRasterizationStateCreateInfo::builder()
            .depth_clamp_enable(self.depth_clamp_enabled)
            .rasterizer_discard_enable(self.rasterizer_discard_enabled)
            .polygon_mode(ash::vk::PolygonMode::FILL)
            .cull_mode(self.cull_mode.into())
            .front_face(self.front_face.into())
            .depth_bias_enable(self.depth_bias.is_some())
            .depth_bias_constant_factor(self.depth_bias.unwrap_or_default().0)
            .depth_bias_slope_factor(self.depth_bias.unwrap_or_default().1)
            .line_width(self.line_width)
            .build()
    }

    fn multisample_state(&self) -> ash::vk::PipelineMultisampleStateCreateInfo {
        ash::vk::PipelineMultisampleStateCreateInfo::builder()
            .rasterization_samples(ash::vk::SampleCountFlags::TYPE_1)
            .build()
    }

    fn depth_stencil_state(&self) -> ash::vk::PipelineDepthStencilStateCreateInfo {
        ash::vk::PipelineDepthStencilStateCreateInfo::builder()
            .depth_test_enable(self.depth_test.is_some())
            .depth_write_enable(self.depth_test.map(|(write, _)| write).unwrap_or(false))
            .depth_compare_op(
                self.depth_test
                    .map(|(_, compare_op)| compare_op)
                    .unwrap_or(CompareOp::Never)
                    .into(),
            )
            .depth_bounds_test_enable(false)
            .stencil_test_enable(false)
            .front(ash::vk::StencilOpState::default())
            .back(ash::vk::StencilOpState::default())
            .min_depth_bounds(0.0)
            .max_depth_bounds(1.0)
            .build()
    }

    fn color_blend_state(&self) -> ash::vk::PipelineColorBlendStateCreateInfo {
        ash::vk::PipelineColorBlendStateCreateInfo::builder()
            .attachments(&self.color_blend_attachment_states)
            .build()
    }

    fn _dynamic_state(&self) -> ash::vk::PipelineDynamicStateCreateInfo {
        ash::vk::PipelineDynamicStateCreateInfo::builder()
            .dynamic_states(&self.dynamic_states)
            .build()
    }

    fn _layout(&self) -> ash::vk::PipelineLayout {
        self.layout
            .as_ref()
            .map(|layout| layout.handle())
            .unwrap_or(ash::vk::PipelineLayout::null())
    }

    fn rendering(&self) -> ash::vk::PipelineRenderingCreateInfo {
        ash::vk::PipelineRenderingCreateInfo::builder()
            .color_attachment_formats(&self.color_attachment_formats)
            .depth_attachment_format(
                self.depth_attachment_format
                    .unwrap_or(ash::vk::Format::UNDEFINED),
            )
            .stencil_attachment_format(
                self.stencil_attachment_format
                    .unwrap_or(ash::vk::Format::UNDEFINED),
            )
            .build()
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::image::Format;
    use crate::gfx::pipeline::{
        CullMode, FrontFace, GraphicsPipeline, PipelineLayout, PrimitiveTopology, Viewport,
    };
    use crate::gfx::{Extent2D, Rect2D};
    use crate::physical_device;

    mod vertex_shader {
        use crate::shader;

        shader! {
            path: "src/gfx/pipeline/test.hlsl",
            ty: "vertex",
            entry_point: "main"
        }
    }

    mod fragment_shader {
        use crate::shader;

        shader! {
            path: "src/gfx/pipeline/test.hlsl",
            ty: "fragment",
            entry_point: "main"
        }
    }

    #[test]
    fn build_graphics_pipeline() {
        let physical_device = physical_device!();
        let (device, _) = Device::builder(physical_device.clone(), Default::default())
            .build()
            .unwrap();
        let vertex_shader = vertex_shader::Shader::load(device.clone()).unwrap();
        let fragment_shader = fragment_shader::Shader::load(device.clone()).unwrap();
        let layout = PipelineLayout::builder(device.clone()).build().unwrap();

        let result = GraphicsPipeline::builder(device.clone())
            .vertex_shader(vertex_shader.clone(), "main")
            .fragment_shader(fragment_shader.clone(), "main")
            .topology(PrimitiveTopology::PointList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 640.0,
                height: 480.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 640,
                    height: 480,
                },
            })
            .cull_mode(CullMode::Front)
            .front_face(FrontFace::Clockwise)
            .layout(layout.clone())
            .color_attachment_format(Format::B8G8R8A8Srgb)
            .depth_attachment_format(Format::Undefined)
            .stencil_attachment_format(Format::Undefined)
            .build();

        assert!(result.is_ok());
    }
}
