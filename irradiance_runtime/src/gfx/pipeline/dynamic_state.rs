/// Indicate which dynamic state is taken from dynamic state commands.
#[allow(missing_docs)]
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum DynamicState {
    Viewport,
    Scissor,
    LineWidth,
    DepthBias,
    BlendConstants,
    DepthBounds,
    StencilCompareMask,
    StencilWriteMask,
    StencilReference,
}

#[doc(hidden)]
impl From<DynamicState> for ash::vk::DynamicState {
    fn from(dynamic_state: DynamicState) -> Self {
        match dynamic_state {
            DynamicState::Viewport => ash::vk::DynamicState::VIEWPORT,
            DynamicState::Scissor => ash::vk::DynamicState::SCISSOR,
            DynamicState::LineWidth => ash::vk::DynamicState::LINE_WIDTH,
            DynamicState::DepthBias => ash::vk::DynamicState::DEPTH_BIAS,
            DynamicState::BlendConstants => ash::vk::DynamicState::BLEND_CONSTANTS,
            DynamicState::DepthBounds => ash::vk::DynamicState::DEPTH_BOUNDS,
            DynamicState::StencilCompareMask => ash::vk::DynamicState::STENCIL_COMPARE_MASK,
            DynamicState::StencilWriteMask => ash::vk::DynamicState::STENCIL_WRITE_MASK,
            DynamicState::StencilReference => ash::vk::DynamicState::STENCIL_REFERENCE,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::DynamicState;

    #[test]
    fn to_dynamic_state() {
        assert_eq!(
            ash::vk::DynamicState::VIEWPORT,
            ash::vk::DynamicState::from(DynamicState::Viewport)
        );
        assert_eq!(
            ash::vk::DynamicState::SCISSOR,
            ash::vk::DynamicState::from(DynamicState::Scissor)
        );
        assert_eq!(
            ash::vk::DynamicState::LINE_WIDTH,
            ash::vk::DynamicState::from(DynamicState::LineWidth)
        );
        assert_eq!(
            ash::vk::DynamicState::DEPTH_BIAS,
            ash::vk::DynamicState::from(DynamicState::DepthBias)
        );
        assert_eq!(
            ash::vk::DynamicState::BLEND_CONSTANTS,
            ash::vk::DynamicState::from(DynamicState::BlendConstants)
        );
        assert_eq!(
            ash::vk::DynamicState::DEPTH_BOUNDS,
            ash::vk::DynamicState::from(DynamicState::DepthBounds)
        );
        assert_eq!(
            ash::vk::DynamicState::STENCIL_COMPARE_MASK,
            ash::vk::DynamicState::from(DynamicState::StencilCompareMask)
        );
        assert_eq!(
            ash::vk::DynamicState::STENCIL_WRITE_MASK,
            ash::vk::DynamicState::from(DynamicState::StencilWriteMask)
        );
        assert_eq!(
            ash::vk::DynamicState::STENCIL_REFERENCE,
            ash::vk::DynamicState::from(DynamicState::StencilReference)
        );
    }
}
