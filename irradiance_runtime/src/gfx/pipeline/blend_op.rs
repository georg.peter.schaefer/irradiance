/// Framebuffer blending operations.
#[allow(missing_docs)]
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum BlendOp {
    Add,
    Subtract,
    ReverseSubtract,
    Min,
    Max,
}

#[doc(hidden)]
impl From<BlendOp> for ash::vk::BlendOp {
    fn from(blend_op: BlendOp) -> Self {
        match blend_op {
            BlendOp::Add => ash::vk::BlendOp::ADD,
            BlendOp::Subtract => ash::vk::BlendOp::SUBTRACT,
            BlendOp::ReverseSubtract => ash::vk::BlendOp::REVERSE_SUBTRACT,
            BlendOp::Min => ash::vk::BlendOp::MIN,
            BlendOp::Max => ash::vk::BlendOp::MAX,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::BlendOp;

    #[test]
    fn to_blend_op() {
        assert_eq!(ash::vk::BlendOp::ADD, ash::vk::BlendOp::from(BlendOp::Add));
        assert_eq!(
            ash::vk::BlendOp::SUBTRACT,
            ash::vk::BlendOp::from(BlendOp::Subtract)
        );
        assert_eq!(
            ash::vk::BlendOp::REVERSE_SUBTRACT,
            ash::vk::BlendOp::from(BlendOp::ReverseSubtract)
        );
        assert_eq!(ash::vk::BlendOp::MIN, ash::vk::BlendOp::from(BlendOp::Min));
        assert_eq!(ash::vk::BlendOp::MAX, ash::vk::BlendOp::from(BlendOp::Max));
    }
}
