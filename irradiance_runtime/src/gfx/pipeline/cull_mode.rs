/// Triangle culling.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum CullMode {
    /// No triangles are discarded.
    None,
    /// Front-facing triangles are discarded.
    Front,
    /// Back-facing triangles are discarded.
    Back,
    /// All triangles are discarded.
    FrontAndBack,
}

#[doc(hidden)]
impl From<CullMode> for ash::vk::CullModeFlags {
    fn from(cull_mode: CullMode) -> Self {
        match cull_mode {
            CullMode::None => ash::vk::CullModeFlags::NONE,
            CullMode::Front => ash::vk::CullModeFlags::FRONT,
            CullMode::Back => ash::vk::CullModeFlags::BACK,
            CullMode::FrontAndBack => ash::vk::CullModeFlags::FRONT_AND_BACK,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::CullMode;

    #[test]
    fn to_cull_mode() {
        assert_eq!(
            ash::vk::CullModeFlags::NONE,
            ash::vk::CullModeFlags::from(CullMode::None)
        );
        assert_eq!(
            ash::vk::CullModeFlags::FRONT,
            ash::vk::CullModeFlags::from(CullMode::Front)
        );
        assert_eq!(
            ash::vk::CullModeFlags::BACK,
            ash::vk::CullModeFlags::from(CullMode::Back)
        );
        assert_eq!(
            ash::vk::CullModeFlags::FRONT_AND_BACK,
            ash::vk::CullModeFlags::from(CullMode::FrontAndBack)
        );
    }
}
