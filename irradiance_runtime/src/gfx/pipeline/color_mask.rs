use crate::impl_flag_set;

impl_flag_set! {
    ColorMask,
    doc = "Which components are written to the framebuffer.",
    (VK_COLOR_COMPONENT_R_BIT, r),
    (VK_COLOR_COMPONENT_G_BIT, g),
    (VK_COLOR_COMPONENT_B_BIT, b),
    (VK_COLOR_COMPONENT_A_BIT, a)
}

#[doc(hidden)]
impl From<ColorMask> for ash::vk::ColorComponentFlags {
    fn from(color_mask: ColorMask) -> Self {
        let mut flags = ash::vk::ColorComponentFlags::empty();
        if color_mask.r {
            flags |= ash::vk::ColorComponentFlags::R;
        }
        if color_mask.g {
            flags |= ash::vk::ColorComponentFlags::G;
        }
        if color_mask.b {
            flags |= ash::vk::ColorComponentFlags::B;
        }
        if color_mask.a {
            flags |= ash::vk::ColorComponentFlags::A;
        }
        flags
    }
}
