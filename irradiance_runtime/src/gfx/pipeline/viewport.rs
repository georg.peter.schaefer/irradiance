/// A viewport.
#[derive(Copy, Clone, Debug, Default)]
pub struct Viewport {
    /// The x-component of the upper left corner.
    pub x: f32,
    /// The y-component of the upper left corner.
    pub y: f32,
    /// The width.
    pub width: f32,
    /// The height.
    pub height: f32,
    /// Minimum of the depth range.
    pub min_depth: f32,
    /// Maximum of the depth range.
    pub max_depth: f32,
}

#[doc(hidden)]
impl From<Viewport> for ash::vk::Viewport {
    fn from(viewport: Viewport) -> Self {
        ash::vk::Viewport::builder()
            .x(viewport.x)
            .y(viewport.y)
            .width(viewport.width)
            .height(viewport.height)
            .min_depth(viewport.min_depth)
            .max_depth(viewport.max_depth)
            .build()
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::viewport::Viewport;

    #[test]
    fn to_viewport() {
        let result = ash::vk::Viewport::from(Viewport {
            x: 5.0,
            y: 0.0,
            width: 128.0,
            height: 512.0,
            min_depth: -42.0,
            max_depth: 19.0,
        });

        assert_eq!(5.0, result.x);
        assert_eq!(0.0, result.y);
        assert_eq!(128.0, result.width);
        assert_eq!(512.0, result.height);
        assert_eq!(-42.0, result.min_depth);
        assert_eq!(19.0, result.max_depth);
    }
}
