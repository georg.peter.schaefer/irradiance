use std::cell::RefCell;
use std::collections::HashMap;
use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::pipeline::DescriptorType;
use crate::gfx::Handle;
use crate::gfx::{GfxError, ObjectType};

use super::DescriptorSetLayout;

/// A descriptor pool maintains a pool of descriptors, from which descriptor sets are allocated.
#[derive(Debug)]
pub struct DescriptorPool {
    inner: RefCell<Inner>,
    handle: ash::vk::DescriptorPool,
    device: Arc<Device>,
}

impl DescriptorPool {
    /// Starts building a new descriptor pool.
    pub fn builder(device: Arc<Device>) -> DescriptorPoolBuilder {
        DescriptorPoolBuilder {
            free: Default::default(),
            pool_sizes: vec![],
            max_sets: 0,
            device,
        }
    }

    /// Returns if the descriptor pool has enough free descriptors to allocate one descriptor set with `descriptor_set_layout`.
    pub fn can_allocate(&self, descriptor_set_layout: &DescriptorSetLayout) -> bool {
        let inner = self.inner.borrow();
        inner.sets > 0
            && descriptor_set_layout
                .requirements
                .iter()
                .all(|(descriptor_type, count)| {
                    inner
                        .free
                        .get(descriptor_type)
                        .filter(|free| *free >= count)
                        .is_some()
                })
    }

    pub(crate) fn remove_free(&self, descriptor_set_layout: &DescriptorSetLayout) {
        debug_assert!(
            self.can_allocate(descriptor_set_layout),
            "enough free descriptors"
        );
        let mut inner = self.inner.borrow_mut();
        inner.sets -= 1;
        descriptor_set_layout
            .requirements
            .iter()
            .for_each(|(descriptor_type, count)| {
                inner
                    .free
                    .entry(*descriptor_type)
                    .and_modify(|free| *free -= count);
            });
    }

    pub(crate) fn add_free(&self, descriptor_set_layout: &DescriptorSetLayout) {
        let mut inner = self.inner.borrow_mut();
        inner.sets += 1;
        descriptor_set_layout
            .requirements
            .iter()
            .for_each(|(descriptor_type, count)| {
                inner
                    .free
                    .entry(*descriptor_type)
                    .and_modify(|free| *free += count);
            });
    }
}

impl Drop for DescriptorPool {
    fn drop(&mut self) {
        unsafe {
            self.device
                .inner()
                .destroy_descriptor_pool(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for DescriptorPool {
    type Type = ash::vk::DescriptorPool;

    fn object_type(&self) -> ObjectType {
        ObjectType::DescriptorPool
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for DescriptorPool {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

#[derive(Debug)]
struct Inner {
    free: HashMap<DescriptorType, u32>,
    sets: u32,
}

/// Type for building a [`DescriptorPool`].
#[derive(Debug)]
pub struct DescriptorPoolBuilder {
    free: HashMap<DescriptorType, u32>,
    pool_sizes: Vec<ash::vk::DescriptorPoolSize>,
    max_sets: u32,
    device: Arc<Device>,
}

impl DescriptorPoolBuilder {
    /// Sets the maximum number of descriptor sets that can be allocated from the pool.
    pub fn max_sets(&mut self, max_sets: u32) -> &mut Self {
        self.max_sets = max_sets;
        self
    }

    /// Adds an amount of `descriptor_type`s to allocate.
    pub fn pool_size(
        &mut self,
        descriptor_type: DescriptorType,
        descriptor_count: u32,
    ) -> &mut Self {
        *self.free.entry(descriptor_type).or_default() += descriptor_count;
        self.pool_sizes.push(
            ash::vk::DescriptorPoolSize::builder()
                .ty(descriptor_type.into())
                .descriptor_count(descriptor_count)
                .build(),
        );
        self
    }

    /// Builds the [`DescriptorPool`].
    ///
    /// # Errors
    /// This function returns an error if the descriptor pool creation fails.
    pub fn build(&mut self) -> Result<Arc<DescriptorPool>, GfxError> {
        let create_info = ash::vk::DescriptorPoolCreateInfo::builder()
            .flags(ash::vk::DescriptorPoolCreateFlags::FREE_DESCRIPTOR_SET)
            .max_sets(self.max_sets)
            .pool_sizes(&self.pool_sizes)
            .build();

        let handle = unsafe {
            self.device
                .inner()
                .create_descriptor_pool(&create_info, None)
        }?;

        Ok(Arc::new(DescriptorPool {
            inner: RefCell::new(Inner {
                free: self.free.clone(),
                sets: self.max_sets,
            }),
            handle,
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::pipeline::{DescriptorPool, DescriptorType};
    use crate::physical_device;

    #[test]
    fn build_descriptor_pool() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();

        let result = DescriptorPool::builder(device.clone())
            .max_sets(10)
            .pool_size(DescriptorType::UniformBuffer, 5)
            .pool_size(DescriptorType::CombinedImageSampler, 2)
            .build();

        assert!(result.is_ok());
    }
}
