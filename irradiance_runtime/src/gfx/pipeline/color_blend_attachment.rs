use crate::gfx::pipeline::{BlendFactor, BlendOp, ColorMask};

/// Pipeline color blend attachment state.
#[derive(Copy, Clone, Debug)]
pub struct ColorBlendAttachmentState {
    color_write_mask: ColorMask,
    alpha_blend_op: BlendOp,
    dst_alpha_blend_factor: BlendFactor,
    src_alpha_blend_factor: BlendFactor,
    color_blend_op: BlendOp,
    dst_color_blend_factor: BlendFactor,
    src_color_blend_factor: BlendFactor,
    blend_enable: bool,
}

impl ColorBlendAttachmentState {
    /// Starts building a new color blend attachment state.
    pub fn builder() -> ColorBlendAttachmentStateBuilder {
        ColorBlendAttachmentStateBuilder {
            color_write_mask: ColorMask {
                r: true,
                g: true,
                b: true,
                a: true,
            },
            alpha_blend_op: BlendOp::Add,
            dst_alpha_blend_factor: BlendFactor::One,
            src_alpha_blend_factor: BlendFactor::One,
            color_blend_op: BlendOp::Add,
            dst_color_blend_factor: BlendFactor::One,
            src_color_blend_factor: BlendFactor::One,
            blend_enable: false,
        }
    }
}

#[doc(hidden)]
impl From<ColorBlendAttachmentState> for ash::vk::PipelineColorBlendAttachmentState {
    fn from(color_blend_attachment_state: ColorBlendAttachmentState) -> Self {
        ash::vk::PipelineColorBlendAttachmentState::builder()
            .blend_enable(color_blend_attachment_state.blend_enable)
            .src_color_blend_factor(color_blend_attachment_state.src_color_blend_factor.into())
            .dst_color_blend_factor(color_blend_attachment_state.dst_color_blend_factor.into())
            .color_blend_op(color_blend_attachment_state.color_blend_op.into())
            .src_alpha_blend_factor(color_blend_attachment_state.src_alpha_blend_factor.into())
            .dst_alpha_blend_factor(color_blend_attachment_state.dst_alpha_blend_factor.into())
            .alpha_blend_op(color_blend_attachment_state.alpha_blend_op.into())
            .color_write_mask(color_blend_attachment_state.color_write_mask.into())
            .build()
    }
}

/// Type for building a [`ColorBlendAttachmentState`].
#[derive(Debug)]
pub struct ColorBlendAttachmentStateBuilder {
    color_write_mask: ColorMask,
    alpha_blend_op: BlendOp,
    dst_alpha_blend_factor: BlendFactor,
    src_alpha_blend_factor: BlendFactor,
    color_blend_op: BlendOp,
    dst_color_blend_factor: BlendFactor,
    src_color_blend_factor: BlendFactor,
    blend_enable: bool,
}

impl ColorBlendAttachmentStateBuilder {
    /// Enables the blending.
    pub fn enable_blend(&mut self) -> &mut Self {
        self.blend_enable = true;
        self
    }

    /// Sets the source color blend factor.
    pub fn src_color_blend_factor(&mut self, src_color_blend_factor: BlendFactor) -> &mut Self {
        self.src_color_blend_factor = src_color_blend_factor;
        self
    }

    /// Sets the destination color blend factor.
    pub fn dst_color_blend_factor(&mut self, dst_color_blend_factor: BlendFactor) -> &mut Self {
        self.dst_color_blend_factor = dst_color_blend_factor;
        self
    }

    /// Sets the color blend operation.
    pub fn color_blend_op(&mut self, color_blend_op: BlendOp) -> &mut Self {
        self.color_blend_op = color_blend_op;
        self
    }

    /// Sets the source alpha blend factor.
    pub fn src_alpha_blend_factor(&mut self, src_alpha_blend_factor: BlendFactor) -> &mut Self {
        self.src_alpha_blend_factor = src_alpha_blend_factor;
        self
    }

    /// Sets the destination alpha blend factor.
    pub fn dst_alpha_blend_factor(&mut self, dst_alpha_blend_factor: BlendFactor) -> &mut Self {
        self.dst_alpha_blend_factor = dst_alpha_blend_factor;
        self
    }

    /// Sets the alpha blend operation.
    pub fn alpha_blend_op(&mut self, alpha_blend_op: BlendOp) -> &mut Self {
        self.alpha_blend_op = alpha_blend_op;
        self
    }

    /// Sets the color write mask.
    pub fn color_write_mask(&mut self, color_write_mask: ColorMask) -> &mut Self {
        self.color_write_mask = color_write_mask;
        self
    }

    /// Builds the [`ColorBlendAttachmentState`].
    pub fn build(&mut self) -> ColorBlendAttachmentState {
        ColorBlendAttachmentState {
            color_write_mask: self.color_write_mask,
            alpha_blend_op: self.alpha_blend_op,
            dst_alpha_blend_factor: self.dst_alpha_blend_factor,
            src_alpha_blend_factor: self.src_alpha_blend_factor,
            color_blend_op: self.color_blend_op,
            dst_color_blend_factor: self.dst_color_blend_factor,
            src_color_blend_factor: self.src_color_blend_factor,
            blend_enable: self.blend_enable,
        }
    }
}
