use std::cell::RefCell;
use std::sync::Arc;

use crate::gfx::buffer::Buffer;
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{ImageView, Sampler};
use crate::gfx::pipeline::write_descriptor_set::{
    WriteDescriptorSetBuffers, WriteDescriptorSetImages,
};
use crate::gfx::pipeline::{DescriptorPool, DescriptorSetLayout};
use crate::gfx::{GfxError, Handle, ObjectType};

/// Descriptor set.
#[derive(Debug)]
pub struct DescriptorSet {
    _buffers: RefCell<Vec<Arc<Buffer>>>,
    _image_views: RefCell<Vec<Arc<ImageView>>>,
    _samplers: RefCell<Vec<Arc<Sampler>>>,
    handle: ash::vk::DescriptorSet,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    descriptor_pool: Arc<DescriptorPool>,
    device: Arc<Device>,
}

impl DescriptorSet {
    /// Starts building a new descriptor set.
    pub fn builder(
        device: Arc<Device>,
        descriptor_pool: Arc<DescriptorPool>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> DescriptorSetBuilder {
        DescriptorSetBuilder {
            descriptor_set_layout,
            descriptor_pool,
            device,
        }
    }

    /// Writes buffers to the descriptor set.
    pub fn write_buffers(&self, write_descriptor_set_buffers: Vec<WriteDescriptorSetBuffers>) {
        let write_descriptor_sets = write_descriptor_set_buffers
            .iter()
            .map(|write_descriptor_set_buffers| {
                ash::vk::WriteDescriptorSet::builder()
                    .dst_set(self.handle)
                    .dst_binding(write_descriptor_set_buffers.dst_binding)
                    .dst_array_element(write_descriptor_set_buffers.dst_array_element)
                    .descriptor_type(write_descriptor_set_buffers.descriptor_type.into())
                    .buffer_info(&write_descriptor_set_buffers.buffer_infos)
                    .build()
            })
            .collect::<Vec<_>>();

        self._buffers.replace(
            write_descriptor_set_buffers
                .iter()
                .flat_map(|write| write.buffers.clone())
                .collect(),
        );
        unsafe {
            self.device
                .inner()
                .update_descriptor_sets(&write_descriptor_sets, &[]);
        }
    }

    /// Writes images to the descriptor set.
    pub fn write_images(&self, write_descriptor_set_images: Vec<WriteDescriptorSetImages>) {
        let write_descriptor_sets = write_descriptor_set_images
            .iter()
            .map(|write_descriptor_set_images| {
                ash::vk::WriteDescriptorSet::builder()
                    .dst_set(self.handle)
                    .dst_binding(write_descriptor_set_images.dst_binding)
                    .dst_array_element(write_descriptor_set_images.dst_array_element)
                    .descriptor_type(write_descriptor_set_images.descriptor_type.into())
                    .image_info(&write_descriptor_set_images.image_infos)
                    .build()
            })
            .collect::<Vec<_>>();

        self._image_views.replace(
            write_descriptor_set_images
                .iter()
                .flat_map(|write| write.image_views.clone())
                .collect(),
        );
        self._samplers.replace(
            write_descriptor_set_images
                .iter()
                .flat_map(|write| write.samplers.clone())
                .collect(),
        );
        unsafe {
            self.device
                .inner()
                .update_descriptor_sets(&write_descriptor_sets, &[]);
        }
    }
}

impl Drop for DescriptorSet {
    fn drop(&mut self) {
        self.descriptor_pool.add_free(&self.descriptor_set_layout);
        unsafe {
            self.device
                .inner()
                .free_descriptor_sets(self.descriptor_pool.handle(), &[self.handle])
                .unwrap();
        }
    }
}

unsafe impl Send for DescriptorSet {}
unsafe impl Sync for DescriptorSet {}

#[doc(hidden)]
impl Handle for DescriptorSet {
    type Type = ash::vk::DescriptorSet;

    fn object_type(&self) -> ObjectType {
        ObjectType::DescriptorSet
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for DescriptorSet {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`DescriptorSet`].
#[derive(Debug)]
pub struct DescriptorSetBuilder {
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    descriptor_pool: Arc<DescriptorPool>,
    device: Arc<Device>,
}

impl DescriptorSetBuilder {
    /// Builds the [`DescriptorSet`].
    ///
    /// # Errors
    /// This function returns an error if creating the descriptor set fails.
    pub fn build(&mut self) -> Result<Arc<DescriptorSet>, GfxError> {
        let allocate_info = ash::vk::DescriptorSetAllocateInfo::builder()
            .descriptor_pool(self.descriptor_pool.handle())
            .set_layouts(&[self.descriptor_set_layout.handle()])
            .build();

        let handle = unsafe { self.device.inner().allocate_descriptor_sets(&allocate_info) }?[0];
        self.descriptor_pool
            .remove_free(&self.descriptor_set_layout);

        Ok(Arc::new(DescriptorSet {
            _buffers: RefCell::new(vec![]),
            _image_views: RefCell::new(vec![]),
            _samplers: RefCell::new(vec![]),
            handle,
            descriptor_set_layout: self.descriptor_set_layout.clone(),
            descriptor_pool: self.descriptor_pool.clone(),
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::pipeline::{
        DescriptorPool, DescriptorSet, DescriptorSetLayout, DescriptorType, ShaderStages,
    };
    use crate::physical_device;

    #[test]
    fn build_descriptor_set() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let descriptor_pool = DescriptorPool::builder(device.clone())
            .max_sets(1)
            .pool_size(DescriptorType::UniformBuffer, 1)
            .pool_size(DescriptorType::CombinedImageSampler, 1)
            .build()
            .unwrap();
        let descriptor_set_layout = DescriptorSetLayout::builder(device.clone())
            .binding(
                0,
                DescriptorType::UniformBuffer,
                1,
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
            )
            .binding(
                1,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
            .unwrap();

        let result = DescriptorSet::builder(
            device.clone(),
            descriptor_pool.clone(),
            descriptor_set_layout.clone(),
        )
        .build();

        assert!(result.is_ok());
    }
}
