/// Bind point of a pipeline object to a command buffer.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum PipelineBindPoint {
    /// Binding as a compute pipeline.
    Graphics,
    /// Binding as a graphics pipeline.
    Compute,
    /// Binding as a ray tracing pipeline.
    RayTracing,
    /// Binding as a subpass shading pipeline.
    SubpassShading,
}

#[doc(hidden)]
impl From<PipelineBindPoint> for ash::vk::PipelineBindPoint {
    fn from(pipeline_bind_point: PipelineBindPoint) -> Self {
        match pipeline_bind_point {
            PipelineBindPoint::Graphics => ash::vk::PipelineBindPoint::GRAPHICS,
            PipelineBindPoint::Compute => ash::vk::PipelineBindPoint::COMPUTE,
            PipelineBindPoint::RayTracing => ash::vk::PipelineBindPoint::RAY_TRACING_KHR,
            PipelineBindPoint::SubpassShading => ash::vk::PipelineBindPoint::SUBPASS_SHADING_HUAWEI,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::PipelineBindPoint;

    #[test]
    fn to_pipeline_bind_point() {
        assert_eq!(
            ash::vk::PipelineBindPoint::GRAPHICS,
            ash::vk::PipelineBindPoint::from(PipelineBindPoint::Graphics)
        );
        assert_eq!(
            ash::vk::PipelineBindPoint::COMPUTE,
            ash::vk::PipelineBindPoint::from(PipelineBindPoint::Compute)
        );
        assert_eq!(
            ash::vk::PipelineBindPoint::RAY_TRACING_KHR,
            ash::vk::PipelineBindPoint::from(PipelineBindPoint::RayTracing)
        );
        assert_eq!(
            ash::vk::PipelineBindPoint::SUBPASS_SHADING_HUAWEI,
            ash::vk::PipelineBindPoint::from(PipelineBindPoint::SubpassShading)
        );
    }
}
