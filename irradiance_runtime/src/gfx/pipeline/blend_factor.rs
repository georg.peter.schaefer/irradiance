/// Framebuffer blending factors.
#[allow(missing_docs)]
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum BlendFactor {
    Zero,
    One,
    SrcColor,
    OneMinusSrcColor,
    DstColor,
    OneMinusDstColor,
    SrcAlpha,
    OneMinusSrcAlpha,
    DstAlpha,
    OneMinusDstAlpha,
    ConstantColor,
    OneMinusConstantColor,
    ConstantAlpha,
    OneMinusConstantAlpha,
    SrcAlphaSaturate,
    Src1Color,
    OneMinusSrc1Color,
    Src1Alpha,
    OneMinusSrc1Alpha,
}

#[doc(hidden)]
impl From<BlendFactor> for ash::vk::BlendFactor {
    fn from(blend_factor: BlendFactor) -> Self {
        match blend_factor {
            BlendFactor::Zero => ash::vk::BlendFactor::ZERO,
            BlendFactor::One => ash::vk::BlendFactor::ONE,
            BlendFactor::SrcColor => ash::vk::BlendFactor::SRC_COLOR,
            BlendFactor::OneMinusSrcColor => ash::vk::BlendFactor::ONE_MINUS_SRC_COLOR,
            BlendFactor::DstColor => ash::vk::BlendFactor::DST_COLOR,
            BlendFactor::OneMinusDstColor => ash::vk::BlendFactor::ONE_MINUS_DST_COLOR,
            BlendFactor::SrcAlpha => ash::vk::BlendFactor::SRC_ALPHA,
            BlendFactor::OneMinusSrcAlpha => ash::vk::BlendFactor::ONE_MINUS_SRC_ALPHA,
            BlendFactor::DstAlpha => ash::vk::BlendFactor::DST_ALPHA,
            BlendFactor::OneMinusDstAlpha => ash::vk::BlendFactor::ONE_MINUS_DST_ALPHA,
            BlendFactor::ConstantColor => ash::vk::BlendFactor::CONSTANT_COLOR,
            BlendFactor::OneMinusConstantColor => ash::vk::BlendFactor::ONE_MINUS_CONSTANT_COLOR,
            BlendFactor::ConstantAlpha => ash::vk::BlendFactor::CONSTANT_ALPHA,
            BlendFactor::OneMinusConstantAlpha => ash::vk::BlendFactor::ONE_MINUS_CONSTANT_ALPHA,
            BlendFactor::SrcAlphaSaturate => ash::vk::BlendFactor::SRC_ALPHA_SATURATE,
            BlendFactor::Src1Color => ash::vk::BlendFactor::SRC1_COLOR,
            BlendFactor::OneMinusSrc1Color => ash::vk::BlendFactor::ONE_MINUS_SRC1_COLOR,
            BlendFactor::Src1Alpha => ash::vk::BlendFactor::SRC1_ALPHA,
            BlendFactor::OneMinusSrc1Alpha => ash::vk::BlendFactor::ONE_MINUS_SRC1_ALPHA,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::BlendFactor;

    #[test]
    fn to_blend_factor() {
        assert_eq!(
            ash::vk::BlendFactor::ZERO,
            ash::vk::BlendFactor::from(BlendFactor::Zero)
        );
        assert_eq!(
            ash::vk::BlendFactor::ONE,
            ash::vk::BlendFactor::from(BlendFactor::One)
        );
        assert_eq!(
            ash::vk::BlendFactor::SRC_COLOR,
            ash::vk::BlendFactor::from(BlendFactor::SrcColor)
        );
        assert_eq!(
            ash::vk::BlendFactor::ONE_MINUS_SRC_COLOR,
            ash::vk::BlendFactor::from(BlendFactor::OneMinusSrcColor)
        );
        assert_eq!(
            ash::vk::BlendFactor::DST_COLOR,
            ash::vk::BlendFactor::from(BlendFactor::DstColor)
        );
        assert_eq!(
            ash::vk::BlendFactor::ONE_MINUS_DST_COLOR,
            ash::vk::BlendFactor::from(BlendFactor::OneMinusDstColor)
        );
        assert_eq!(
            ash::vk::BlendFactor::SRC_ALPHA,
            ash::vk::BlendFactor::from(BlendFactor::SrcAlpha)
        );
        assert_eq!(
            ash::vk::BlendFactor::ONE_MINUS_SRC_ALPHA,
            ash::vk::BlendFactor::from(BlendFactor::OneMinusSrcAlpha)
        );
        assert_eq!(
            ash::vk::BlendFactor::DST_ALPHA,
            ash::vk::BlendFactor::from(BlendFactor::DstAlpha)
        );
        assert_eq!(
            ash::vk::BlendFactor::ONE_MINUS_DST_ALPHA,
            ash::vk::BlendFactor::from(BlendFactor::OneMinusDstAlpha)
        );
        assert_eq!(
            ash::vk::BlendFactor::CONSTANT_COLOR,
            ash::vk::BlendFactor::from(BlendFactor::ConstantColor)
        );
        assert_eq!(
            ash::vk::BlendFactor::ONE_MINUS_CONSTANT_COLOR,
            ash::vk::BlendFactor::from(BlendFactor::OneMinusConstantColor)
        );
        assert_eq!(
            ash::vk::BlendFactor::CONSTANT_ALPHA,
            ash::vk::BlendFactor::from(BlendFactor::ConstantAlpha)
        );
        assert_eq!(
            ash::vk::BlendFactor::ONE_MINUS_CONSTANT_ALPHA,
            ash::vk::BlendFactor::from(BlendFactor::OneMinusConstantAlpha)
        );
        assert_eq!(
            ash::vk::BlendFactor::SRC_ALPHA_SATURATE,
            ash::vk::BlendFactor::from(BlendFactor::SrcAlphaSaturate)
        );
        assert_eq!(
            ash::vk::BlendFactor::SRC1_COLOR,
            ash::vk::BlendFactor::from(BlendFactor::Src1Color)
        );
        assert_eq!(
            ash::vk::BlendFactor::ONE_MINUS_SRC1_COLOR,
            ash::vk::BlendFactor::from(BlendFactor::OneMinusSrc1Color)
        );
        assert_eq!(
            ash::vk::BlendFactor::SRC1_ALPHA,
            ash::vk::BlendFactor::from(BlendFactor::Src1Alpha)
        );
        assert_eq!(
            ash::vk::BlendFactor::ONE_MINUS_SRC1_ALPHA,
            ash::vk::BlendFactor::from(BlendFactor::OneMinusSrc1Alpha)
        );
    }
}
