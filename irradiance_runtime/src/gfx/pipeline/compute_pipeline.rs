use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::pipeline::{PipelineLayout, ShaderStages};
use crate::gfx::{GfxError, Handle, ObjectType};
use irradiance_runtime::gfx::pipeline::ShaderModule;
use std::ffi::CString;
use std::sync::Arc;

/// Compute pipeline.
#[derive(Debug)]
pub struct ComputePipeline {
    handle: ash::vk::Pipeline,
    device: Arc<Device>,
}

impl ComputePipeline {
    /// Starts building a new [`ComputePipeline`].
    pub fn builder(device: Arc<Device>) -> ComputePipelineBuilder {
        ComputePipelineBuilder {
            layout: None,
            shader: None,
            device,
        }
    }
}

impl Drop for ComputePipeline {
    fn drop(&mut self) {
        unsafe {
            self.device.inner().destroy_pipeline(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for ComputePipeline {
    type Type = ash::vk::Pipeline;

    fn object_type(&self) -> ObjectType {
        ObjectType::Pipeline
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for ComputePipeline {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`ComputePipeline`].
pub struct ComputePipelineBuilder {
    layout: Option<Arc<PipelineLayout>>,
    shader: Option<(Arc<ShaderModule>, CString)>,
    device: Arc<Device>,
}

impl ComputePipelineBuilder {
    /// Sets the compute shader.
    pub fn shader(&mut self, shader: Arc<ShaderModule>, entry_point: &str) -> &mut Self {
        self.shader = Some((
            shader,
            CString::new(entry_point.to_string()).expect("valid CString"),
        ));
        self
    }

    /// Sets the pipeline layout.
    pub fn layout(&mut self, layout: Arc<PipelineLayout>) -> &mut Self {
        self.layout = Some(layout);
        self
    }

    /// Builds the [`ComputePipeline`].
    pub fn build(&mut self) -> Result<Arc<ComputePipeline>, GfxError> {
        let create_info = [ash::vk::ComputePipelineCreateInfo::builder()
            .stage(self.stage())
            .layout(self._layout())
            .build()];

        let handle = match unsafe {
            self.device.inner().create_compute_pipelines(
                ash::vk::PipelineCache::null(),
                &create_info,
                None,
            )
        } {
            Ok(handles) => handles[0],
            Err((_, result)) => Err(result)?,
        };

        Ok(Arc::new(ComputePipeline {
            handle,
            device: self.device.clone(),
        }))
    }

    fn stage(&self) -> ash::vk::PipelineShaderStageCreateInfo {
        let (module, entry_point) = self.shader.as_ref().expect("shader module");
        ash::vk::PipelineShaderStageCreateInfo::builder()
            .stage(
                ShaderStages {
                    compute: true,
                    ..Default::default()
                }
                .into(),
            )
            .module(module.handle())
            .name(entry_point)
            .build()
    }

    fn _layout(&self) -> ash::vk::PipelineLayout {
        self.layout
            .as_ref()
            .map(|layout| layout.handle())
            .unwrap_or(ash::vk::PipelineLayout::null())
    }
}
