use std::collections::HashMap;
use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::pipeline::{DescriptorType, ShaderStages};
use crate::gfx::{GfxError, Handle, ObjectType};

/// A collection of descriptor binding.
#[derive(Debug)]
pub struct DescriptorSetLayout {
    pub(crate) requirements: HashMap<DescriptorType, u32>,
    handle: ash::vk::DescriptorSetLayout,
    device: Arc<Device>,
}

impl DescriptorSetLayout {
    /// Starts building a new descriptor set layout.
    pub fn builder(device: Arc<Device>) -> DescriptorSetLayoutBuilder {
        DescriptorSetLayoutBuilder {
            requirements: Default::default(),
            bindings: vec![],
            device,
        }
    }
}

impl Drop for DescriptorSetLayout {
    fn drop(&mut self) {
        unsafe {
            self.device
                .inner()
                .destroy_descriptor_set_layout(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for DescriptorSetLayout {
    type Type = ash::vk::DescriptorSetLayout;

    fn object_type(&self) -> ObjectType {
        ObjectType::DescriptorSetLayout
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for DescriptorSetLayout {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`DescriptorSetLayout`].
#[derive(Debug)]
pub struct DescriptorSetLayoutBuilder {
    requirements: HashMap<DescriptorType, u32>,
    bindings: Vec<ash::vk::DescriptorSetLayoutBinding>,
    device: Arc<Device>,
}

impl DescriptorSetLayoutBuilder {
    /// Adds a binding to the descriptor set layout.
    pub fn binding(
        &mut self,
        binding: u32,
        descriptor_type: DescriptorType,
        descriptor_count: u32,
        stages: ShaderStages,
    ) -> &mut Self {
        *self.requirements.entry(descriptor_type).or_default() += descriptor_count;
        self.bindings.push(
            ash::vk::DescriptorSetLayoutBinding::builder()
                .binding(binding)
                .descriptor_type(descriptor_type.into())
                .descriptor_count(descriptor_count)
                .stage_flags(stages.into())
                .build(),
        );
        self
    }

    /// Builds the [`DescriptorSetLayout`].
    pub fn build(&mut self) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        let create_info = ash::vk::DescriptorSetLayoutCreateInfo::builder()
            .bindings(&self.bindings)
            .build();

        let handle = unsafe {
            self.device
                .inner()
                .create_descriptor_set_layout(&create_info, None)
        }?;

        Ok(Arc::new(DescriptorSetLayout {
            requirements: self.requirements.clone(),
            handle,
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::pipeline::{DescriptorSetLayout, DescriptorType, ShaderStages};
    use crate::physical_device;

    #[test]
    fn build_descriptor_set_layout() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();

        let result = DescriptorSetLayout::builder(device.clone())
            .binding(
                1,
                DescriptorType::UniformBuffer,
                1,
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
            )
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build();

        assert!(result.is_ok());
    }
}
