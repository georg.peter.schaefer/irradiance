use crate::gfx::buffer::Buffer;
use crate::gfx::image::{ImageLayout, ImageView, Sampler};
use crate::gfx::pipeline::DescriptorType;
use crate::gfx::Handle;
use std::sync::Arc;

/// Descriptor set write buffers operation.
#[derive(Debug)]
pub struct WriteDescriptorSetBuffers {
    pub(crate) buffer_infos: Vec<ash::vk::DescriptorBufferInfo>,
    pub(crate) buffers: Vec<Arc<Buffer>>,
    pub(crate) descriptor_type: DescriptorType,
    pub(crate) dst_array_element: u32,
    pub(crate) dst_binding: u32,
}

impl WriteDescriptorSetBuffers {
    /// Starts building a new descriptor set write buffers operation.
    pub fn builder() -> WriteDescriptorSetBuffersBuilder {
        WriteDescriptorSetBuffersBuilder {
            buffer_infos: vec![],
            buffers: vec![],
            descriptor_type: DescriptorType::Sampler,
            dst_array_element: 0,
            dst_binding: 0,
        }
    }
}

/// Type for building a [`WriteDescriptorSetBuffers`].
#[derive(Debug)]
pub struct WriteDescriptorSetBuffersBuilder {
    buffer_infos: Vec<ash::vk::DescriptorBufferInfo>,
    buffers: Vec<Arc<Buffer>>,
    descriptor_type: DescriptorType,
    dst_array_element: u32,
    dst_binding: u32,
}

impl WriteDescriptorSetBuffersBuilder {
    /// Sets the destination descriptor binding.
    pub fn dst_binding(&mut self, dst_binding: u32) -> &mut Self {
        self.dst_binding = dst_binding;
        self
    }

    /// Sets the starting element in the descriptor array.
    pub fn dst_array_element(&mut self, dst_array_element: u32) -> &mut Self {
        self.dst_array_element = dst_array_element;
        self
    }

    /// Sets the descriptor type.
    pub fn descriptor_type(&mut self, descriptor_type: DescriptorType) -> &mut Self {
        self.descriptor_type = descriptor_type;
        self
    }

    /// Adds a buffer region.
    pub fn buffer(&mut self, buffer: &Arc<Buffer>, offset: u64, range: u64) -> &mut Self {
        self.buffer_infos.push(
            ash::vk::DescriptorBufferInfo::builder()
                .buffer(buffer.handle())
                .offset(offset)
                .range(range)
                .build(),
        );
        self.buffers.push(buffer.clone());
        self
    }

    /// Builds the [`WriteDescriptorSetBuffers`].
    pub fn build(&mut self) -> WriteDescriptorSetBuffers {
        WriteDescriptorSetBuffers {
            buffer_infos: self.buffer_infos.clone(),
            buffers: self.buffers.clone(),
            descriptor_type: self.descriptor_type,
            dst_array_element: self.dst_array_element,
            dst_binding: self.dst_binding,
        }
    }
}

/// Descriptor set write buffers operation.
#[derive(Debug)]
pub struct WriteDescriptorSetImages {
    pub(crate) image_infos: Vec<ash::vk::DescriptorImageInfo>,
    pub(crate) image_views: Vec<Arc<ImageView>>,
    pub(crate) samplers: Vec<Arc<Sampler>>,
    pub(crate) descriptor_type: DescriptorType,
    pub(crate) dst_array_element: u32,
    pub(crate) dst_binding: u32,
}

impl WriteDescriptorSetImages {
    /// Starts building a new descriptor set write images operation.
    pub fn builder() -> WriteDescriptorSetImagesBuilder {
        WriteDescriptorSetImagesBuilder {
            image_infos: vec![],
            image_views: vec![],
            samplers: vec![],
            descriptor_type: DescriptorType::Sampler,
            dst_array_element: 0,
            dst_binding: 0,
        }
    }
}

/// Type for building a [`WriteDescriptorSetImages`].
#[derive(Debug)]
pub struct WriteDescriptorSetImagesBuilder {
    image_infos: Vec<ash::vk::DescriptorImageInfo>,
    image_views: Vec<Arc<ImageView>>,
    samplers: Vec<Arc<Sampler>>,
    descriptor_type: DescriptorType,
    dst_array_element: u32,
    dst_binding: u32,
}

impl WriteDescriptorSetImagesBuilder {
    /// Sets the destination descriptor binding.
    pub fn dst_binding(&mut self, dst_binding: u32) -> &mut Self {
        self.dst_binding = dst_binding;
        self
    }

    /// Sets the starting element in the descriptor array.
    pub fn dst_array_element(&mut self, dst_array_element: u32) -> &mut Self {
        self.dst_array_element = dst_array_element;
        self
    }

    /// Sets the descriptor type.
    pub fn descriptor_type(&mut self, descriptor_type: DescriptorType) -> &mut Self {
        self.descriptor_type = descriptor_type;
        self
    }

    /// Adds an image.
    pub fn image(&mut self, image_view: &Arc<ImageView>, image_layout: ImageLayout) -> &mut Self {
        self.image_infos.push(
            ash::vk::DescriptorImageInfo::builder()
                .image_view(image_view.handle())
                .image_layout(image_layout.into())
                .build(),
        );
        self.image_views.push(image_view.clone());
        self
    }

    /// Adds a sampled image.
    pub fn sampled_image(
        &mut self,
        sampler: &Arc<Sampler>,
        image_view: &Arc<ImageView>,
        image_layout: ImageLayout,
    ) -> &mut Self {
        self.image_infos.push(
            ash::vk::DescriptorImageInfo::builder()
                .sampler(sampler.handle())
                .image_view(image_view.handle())
                .image_layout(image_layout.into())
                .build(),
        );
        self.image_views.push(image_view.clone());
        self.samplers.push(sampler.clone());
        self
    }

    /// Builds the [`WriteDescriptorSetImages`].
    pub fn build(&mut self) -> WriteDescriptorSetImages {
        WriteDescriptorSetImages {
            image_infos: self.image_infos.clone(),
            image_views: self.image_views.clone(),
            samplers: self.samplers.clone(),
            descriptor_type: self.descriptor_type,
            dst_array_element: self.dst_array_element,
            dst_binding: self.dst_binding,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::buffer::{Buffer, BufferUsage};
    use crate::gfx::device::Device;
    use crate::gfx::image::{
        Format, Image, ImageLayout, ImageTiling, ImageType, ImageUsage, ImageView, ImageViewType,
        SampleCount, Sampler,
    };
    use crate::gfx::pipeline::{
        DescriptorType, WriteDescriptorSetBuffers, WriteDescriptorSetImages,
    };
    use crate::gfx::sync::SharingMode;
    use crate::gfx::{Extent3D, Handle};
    use crate::physical_device;

    #[test]
    fn build_write_descriptor_set_buffers() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let buffer = Buffer::with_size(device.clone(), 32)
            .usage(BufferUsage {
                uniform_buffer: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .build()
            .unwrap();

        let write_descriptor_set = WriteDescriptorSetBuffers::builder()
            .dst_binding(0)
            .dst_array_element(0)
            .descriptor_type(DescriptorType::UniformBuffer)
            .buffer(&buffer, 0, 24)
            .build();

        assert_eq!(0, write_descriptor_set.dst_binding);
        assert_eq!(0, write_descriptor_set.dst_array_element);
        assert_eq!(
            DescriptorType::UniformBuffer,
            write_descriptor_set.descriptor_type
        );
        assert_eq!(write_descriptor_set.buffer_infos[0].buffer, buffer.handle());
        assert_eq!(write_descriptor_set.buffer_infos[0].offset, 0);
        assert_eq!(write_descriptor_set.buffer_infos[0].range, 24);
    }

    #[test]
    fn build_write_descriptor_set_images() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let image = Image::builder(device.clone())
            .image_type(ImageType::Type2D)
            .format(Format::R8G8B8A8Srgb)
            .extent(Extent3D {
                width: 1024,
                height: 1024,
                depth: 1,
            })
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCount::Sample1)
            .tiling(ImageTiling::Optimal)
            .usage(ImageUsage {
                sampled: true,
                transfer_dst: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .initial_layout(ImageLayout::Undefined)
            .build()
            .unwrap();
        let image_view = ImageView::builder(device.clone(), image.clone())
            .view_type(ImageViewType::Type2D)
            .build()
            .unwrap();
        let sampler = Sampler::builder(device.clone()).build().unwrap();

        let write_descriptor_set = WriteDescriptorSetImages::builder()
            .dst_binding(0)
            .dst_array_element(0)
            .descriptor_type(DescriptorType::CombinedImageSampler)
            .image(&image_view, ImageLayout::ShaderReadOnlyOptimal)
            .sampled_image(
                &sampler,
                &image_view,
                ImageLayout::DepthStencilReadOnlyOptimal,
            )
            .build();

        assert_eq!(0, write_descriptor_set.dst_binding);
        assert_eq!(0, write_descriptor_set.dst_array_element);
        assert_eq!(
            DescriptorType::CombinedImageSampler,
            write_descriptor_set.descriptor_type
        );
        assert_eq!(
            write_descriptor_set.image_infos[0].image_view,
            image_view.handle()
        );
        assert_eq!(
            write_descriptor_set.image_infos[0].sampler,
            ash::vk::Sampler::null()
        );
        assert_eq!(
            write_descriptor_set.image_infos[0].image_layout,
            ash::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL
        );
        assert_eq!(
            write_descriptor_set.image_infos[1].image_view,
            image_view.handle()
        );
        assert_eq!(
            write_descriptor_set.image_infos[1].sampler,
            sampler.handle()
        );
        assert_eq!(
            write_descriptor_set.image_infos[1].image_layout,
            ash::vk::ImageLayout::DEPTH_STENCIL_READ_ONLY_OPTIMAL
        );
    }
}
