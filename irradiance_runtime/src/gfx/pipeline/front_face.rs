/// Interpret polygon front-facing orientation.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum FrontFace {
    /// A triangle with positive area is considered front-facing.
    CounterClockwise,
    /// A triangle with negative area is considered front-facing.
    Clockwise,
}

#[doc(hidden)]
impl From<FrontFace> for ash::vk::FrontFace {
    fn from(front_face: FrontFace) -> Self {
        match front_face {
            FrontFace::CounterClockwise => ash::vk::FrontFace::COUNTER_CLOCKWISE,
            FrontFace::Clockwise => ash::vk::FrontFace::CLOCKWISE,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::FrontFace;

    #[test]
    fn to_front_face() {
        assert_eq!(
            ash::vk::FrontFace::COUNTER_CLOCKWISE,
            ash::vk::FrontFace::from(FrontFace::CounterClockwise)
        );
        assert_eq!(
            ash::vk::FrontFace::CLOCKWISE,
            ash::vk::FrontFace::from(FrontFace::Clockwise)
        );
    }
}
