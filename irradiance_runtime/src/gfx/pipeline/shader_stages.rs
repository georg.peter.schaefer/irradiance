use crate::impl_flag_set;

impl_flag_set! {
    ShaderStages,
    doc = "Pipeline stage",
    (VK_SHADER_STAGE_VERTEX_BIT, vertex),
    (VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT, tesselation_control),
    (VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT, tesselation_evaluation),
    (VK_SHADER_STAGE_GEOMETRY_BIT, geoemtry),
    (VK_SHADER_STAGE_FRAGMENT_BIT, fragment),
    (VK_SHADER_STAGE_COMPUTE_BIT, compute),
    (VK_SHADER_STAGE_RAYGEN_BIT_KHR, raygen),
    (VK_SHADER_STAGE_ANY_HIT_BIT_KHR, any_hit),
    (VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR, closest_hit),
    (VK_SHADER_STAGE_MISS_BIT_KHR, miss),
    (VK_SHADER_STAGE_INTERSECTION_BIT_KHR, intersection),
    (VK_SHADER_STAGE_CALLABLE_BIT_KHR, callable),
    (VK_SHADER_STAGE_TASK_BIT_NV, task),
    (VK_SHADER_STAGE_MESH_BIT_NV, mesh)
}

impl ShaderStages {
    /// Creates `ShaderStages` with all graphics related stages set.
    pub const fn all_graphics() -> Self {
        Self {
            vertex: true,
            tesselation_control: true,
            tesselation_evaluation: true,
            geoemtry: true,
            fragment: true,
            compute: false,
            raygen: false,
            any_hit: false,
            closest_hit: false,
            miss: false,
            intersection: false,
            callable: false,
            task: false,
            mesh: false,
        }
    }

    /// Creates `ShaderStages` with all stages set.
    pub const fn all() -> Self {
        Self {
            vertex: true,
            tesselation_control: true,
            tesselation_evaluation: true,
            geoemtry: true,
            fragment: true,
            compute: true,
            raygen: true,
            any_hit: true,
            closest_hit: true,
            miss: true,
            intersection: true,
            callable: true,
            task: true,
            mesh: true,
        }
    }
}

#[doc(hidden)]
impl From<ash::vk::ShaderStageFlags> for ShaderStages {
    fn from(shader_stage_flags: ash::vk::ShaderStageFlags) -> Self {
        use ash::vk::ShaderStageFlags;

        Self {
            vertex: shader_stage_flags & ShaderStageFlags::VERTEX == ShaderStageFlags::VERTEX,
            tesselation_control: shader_stage_flags & ShaderStageFlags::TESSELLATION_CONTROL
                == ShaderStageFlags::TESSELLATION_CONTROL,
            tesselation_evaluation: shader_stage_flags & ShaderStageFlags::TESSELLATION_EVALUATION
                == ShaderStageFlags::TESSELLATION_EVALUATION,
            geoemtry: shader_stage_flags & ShaderStageFlags::GEOMETRY == ShaderStageFlags::GEOMETRY,
            fragment: shader_stage_flags & ShaderStageFlags::FRAGMENT == ShaderStageFlags::FRAGMENT,
            compute: shader_stage_flags & ShaderStageFlags::COMPUTE == ShaderStageFlags::COMPUTE,
            raygen: shader_stage_flags & ShaderStageFlags::RAYGEN_KHR
                == ShaderStageFlags::RAYGEN_KHR,
            any_hit: shader_stage_flags & ShaderStageFlags::ANY_HIT_KHR
                == ShaderStageFlags::ANY_HIT_KHR,
            closest_hit: shader_stage_flags & ShaderStageFlags::CLOSEST_HIT_KHR
                == ShaderStageFlags::CLOSEST_HIT_KHR,
            miss: shader_stage_flags & ShaderStageFlags::MISS_KHR == ShaderStageFlags::MISS_KHR,
            intersection: shader_stage_flags & ShaderStageFlags::INTERSECTION_KHR
                == ShaderStageFlags::INTERSECTION_KHR,
            callable: shader_stage_flags & ShaderStageFlags::CALLABLE_KHR
                == ShaderStageFlags::CALLABLE_KHR,
            task: shader_stage_flags & ShaderStageFlags::TASK_NV == ShaderStageFlags::TASK_NV,
            mesh: shader_stage_flags & ShaderStageFlags::MESH_NV == ShaderStageFlags::MESH_NV,
        }
    }
}

#[doc(hidden)]
impl From<ShaderStages> for ash::vk::ShaderStageFlags {
    fn from(shader_stages: ShaderStages) -> Self {
        use ash::vk::ShaderStageFlags;

        let mut shader_stage_flags = ShaderStageFlags::empty();
        if shader_stages.vertex {
            shader_stage_flags |= ShaderStageFlags::VERTEX;
        }
        if shader_stages.tesselation_control {
            shader_stage_flags |= ShaderStageFlags::TESSELLATION_CONTROL;
        }
        if shader_stages.tesselation_evaluation {
            shader_stage_flags |= ShaderStageFlags::TESSELLATION_EVALUATION;
        }
        if shader_stages.geoemtry {
            shader_stage_flags |= ShaderStageFlags::GEOMETRY;
        }
        if shader_stages.fragment {
            shader_stage_flags |= ShaderStageFlags::FRAGMENT;
        }
        if shader_stages.compute {
            shader_stage_flags |= ShaderStageFlags::COMPUTE;
        }
        if shader_stages.raygen {
            shader_stage_flags |= ShaderStageFlags::RAYGEN_KHR;
        }
        if shader_stages.any_hit {
            shader_stage_flags |= ShaderStageFlags::ANY_HIT_KHR;
        }
        if shader_stages.closest_hit {
            shader_stage_flags |= ShaderStageFlags::CLOSEST_HIT_KHR;
        }
        if shader_stages.miss {
            shader_stage_flags |= ShaderStageFlags::MISS_KHR;
        }
        if shader_stages.intersection {
            shader_stage_flags |= ShaderStageFlags::INTERSECTION_KHR;
        }
        if shader_stages.callable {
            shader_stage_flags |= ShaderStageFlags::CALLABLE_KHR;
        }
        if shader_stages.task {
            shader_stage_flags |= ShaderStageFlags::TASK_NV;
        }
        if shader_stages.mesh {
            shader_stage_flags |= ShaderStageFlags::MESH_NV;
        }

        shader_stage_flags
    }
}

#[cfg(test)]
mod tests {
    use ash::vk::ShaderStageFlags;

    use crate::gfx::pipeline::ShaderStages;

    #[test]
    fn default() {
        assert_eq!(
            ShaderStages {
                vertex: false,
                tesselation_control: false,
                tesselation_evaluation: false,
                geoemtry: false,
                fragment: false,
                compute: false,
                raygen: false,
                any_hit: false,
                closest_hit: false,
                miss: false,
                intersection: false,
                callable: false,
                task: false,
                mesh: false
            },
            Default::default()
        );
    }

    #[test]
    fn none() {
        assert_eq!(ShaderStages::default(), ShaderStages::none());
    }

    #[test]
    fn is_not_a_subset() {
        let a = ShaderStages {
            vertex: true,
            compute: true,
            ..Default::default()
        };
        let b = ShaderStages {
            vertex: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn is_subset() {
        let a = ShaderStages {
            vertex: true,
            compute: true,
            ..Default::default()
        };
        let b = ShaderStages {
            vertex: true,
            ..Default::default()
        };
        assert!(b.is_subset(&a));
    }

    #[test]
    fn is_not_a_superset() {
        let a = ShaderStages {
            vertex: true,
            compute: true,
            ..Default::default()
        };
        let b = ShaderStages {
            vertex: true,
            ..Default::default()
        };
        assert!(!b.is_superset(&a));
    }

    #[test]
    fn is_superset() {
        let a = ShaderStages {
            vertex: true,
            compute: true,
            ..Default::default()
        };
        let b = ShaderStages {
            vertex: true,
            ..Default::default()
        };
        assert!(a.is_superset(&b));
    }

    #[test]
    fn difference() {
        let a = ShaderStages {
            vertex: true,
            compute: true,
            ..Default::default()
        };
        let b = ShaderStages {
            vertex: true,
            ..Default::default()
        };
        assert_eq!(
            ShaderStages {
                compute: true,
                ..Default::default()
            },
            a.difference(&b)
        );
    }

    #[test]
    fn intersection() {
        let a = ShaderStages {
            vertex: true,
            compute: true,
            ..Default::default()
        };
        let b = ShaderStages {
            vertex: true,
            ..Default::default()
        };
        assert_eq!(
            ShaderStages {
                vertex: true,
                ..Default::default()
            },
            a.intersection(&b)
        );
    }

    #[test]
    fn union() {
        let a = ShaderStages {
            vertex: true,
            compute: true,
            ..Default::default()
        };
        let b = ShaderStages {
            vertex: true,
            mesh: true,
            ..Default::default()
        };
        assert_eq!(
            ShaderStages {
                vertex: true,
                compute: true,
                mesh: true,
                ..Default::default()
            },
            a.union(&b)
        );
    }

    #[test]
    fn all_graphics() {
        assert_eq!(
            ShaderStages::all_graphics(),
            ShaderStages::from(ShaderStageFlags::ALL_GRAPHICS)
        );
    }

    #[test]
    fn all() {
        assert_eq!(
            ShaderStages::all(),
            ShaderStages::from(ShaderStageFlags::ALL)
        );
    }

    #[test]
    fn from_shader_stage_flags() {
        assert_eq!(
            ShaderStages {
                vertex: true,
                tesselation_evaluation: true,
                fragment: true,
                raygen: true,
                closest_hit: true,
                intersection: true,
                task: true,
                ..Default::default()
            },
            ShaderStages::from(
                ShaderStageFlags::VERTEX
                    | ShaderStageFlags::TESSELLATION_EVALUATION
                    | ShaderStageFlags::FRAGMENT
                    | ShaderStageFlags::RAYGEN_KHR
                    | ShaderStageFlags::CLOSEST_HIT_KHR
                    | ShaderStageFlags::INTERSECTION_KHR
                    | ShaderStageFlags::TASK_NV
            )
        );
        assert_eq!(
            ShaderStages {
                tesselation_control: true,
                geoemtry: true,
                compute: true,
                any_hit: true,
                miss: true,
                callable: true,
                mesh: true,
                ..Default::default()
            },
            ShaderStages::from(
                ShaderStageFlags::TESSELLATION_CONTROL
                    | ShaderStageFlags::GEOMETRY
                    | ShaderStageFlags::COMPUTE
                    | ShaderStageFlags::ANY_HIT_KHR
                    | ShaderStageFlags::MISS_KHR
                    | ShaderStageFlags::CALLABLE_KHR
                    | ShaderStageFlags::MESH_NV
            )
        );
    }

    #[test]
    fn to_shader_stage_flags() {
        assert_eq!(
            ShaderStageFlags::VERTEX
                | ShaderStageFlags::TESSELLATION_EVALUATION
                | ShaderStageFlags::FRAGMENT
                | ShaderStageFlags::RAYGEN_KHR
                | ShaderStageFlags::CLOSEST_HIT_KHR
                | ShaderStageFlags::INTERSECTION_KHR
                | ShaderStageFlags::TASK_NV,
            ShaderStageFlags::from(ShaderStages {
                vertex: true,
                tesselation_evaluation: true,
                fragment: true,
                raygen: true,
                closest_hit: true,
                intersection: true,
                task: true,
                ..Default::default()
            }),
        );
        assert_eq!(
            ShaderStageFlags::TESSELLATION_CONTROL
                | ShaderStageFlags::GEOMETRY
                | ShaderStageFlags::COMPUTE
                | ShaderStageFlags::ANY_HIT_KHR
                | ShaderStageFlags::MISS_KHR
                | ShaderStageFlags::CALLABLE_KHR
                | ShaderStageFlags::MESH_NV,
            ShaderStageFlags::from(ShaderStages {
                tesselation_control: true,
                geoemtry: true,
                compute: true,
                any_hit: true,
                miss: true,
                callable: true,
                mesh: true,
                ..Default::default()
            })
        );
    }
}
