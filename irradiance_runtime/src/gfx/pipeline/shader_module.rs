use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::{GfxError, Handle, ObjectType};

/// Shader module.
#[derive(Debug)]
pub struct ShaderModule {
    handle: ash::vk::ShaderModule,
    device: Arc<Device>,
}

impl ShaderModule {
    /// Starts building a new `ShaderModule`.
    pub fn builder(device: Arc<Device>, code: &[u32]) -> ShaderModuleBuilder {
        ShaderModuleBuilder { code, device }
    }
}

impl Drop for ShaderModule {
    fn drop(&mut self) {
        unsafe { self.device.inner().destroy_shader_module(self.handle, None) };
    }
}

#[doc(hidden)]
impl Handle for ShaderModule {
    type Type = ash::vk::ShaderModule;

    fn object_type(&self) -> ObjectType {
        ObjectType::ShaderModule
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for ShaderModule {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for building a [`ShaderModule`].
#[derive(Debug)]
pub struct ShaderModuleBuilder<'a> {
    code: &'a [u32],
    device: Arc<Device>,
}

impl<'a> ShaderModuleBuilder<'a> {
    /// Builds the shader module.
    ///
    /// # Errors
    /// This functions returns an error if the shader module creation fails.
    pub fn build(&mut self) -> Result<Arc<ShaderModule>, GfxError> {
        let create_info = ash::vk::ShaderModuleCreateInfo::builder()
            .code(self.code)
            .build();
        unsafe {
            Ok(Arc::new(ShaderModule {
                handle: self
                    .device
                    .inner()
                    .create_shader_module(&create_info, None)?,
                device: self.device.clone(),
            }))
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::{physical_device, shader};

    shader! {
        path: "src/gfx/pipeline/test.hlsl",
        ty: "vertex",
        entry_point: "main"
    }

    #[test]
    fn load_shader() {
        let physical_device = physical_device!();
        let (device, _) = Device::builder(physical_device.clone(), Default::default())
            .build()
            .unwrap();
        let result = Shader::load(device.clone());

        assert!(result.is_ok());
    }
}
