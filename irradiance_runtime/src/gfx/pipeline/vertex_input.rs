use crate::gfx::image::Format;
use crate::gfx::pipeline::vertex_input_rate::VertexInputRate;

/// Vertex input description.
#[derive(Clone, Debug)]
pub struct VertexInput {
    pub(crate) vertex_binding_descriptions: Vec<ash::vk::VertexInputBindingDescription>,
    pub(crate) vertex_attribute_descriptions: Vec<ash::vk::VertexInputAttributeDescription>,
}

impl VertexInput {
    /// Starts building an new vertex input.
    pub fn builder() -> VertexInputBuilder {
        VertexInputBuilder {
            vertex_binding_descriptions: vec![],
            vertex_attribute_descriptions: vec![],
        }
    }
}

/// Type for building [`VertexInput`].
#[derive(Debug)]
pub struct VertexInputBuilder {
    vertex_binding_descriptions: Vec<ash::vk::VertexInputBindingDescription>,
    vertex_attribute_descriptions: Vec<ash::vk::VertexInputAttributeDescription>,
}

impl VertexInputBuilder {
    /// Defines a binding.
    pub fn binding(&mut self, binding: u32, stride: u32, input_rate: VertexInputRate) -> &mut Self {
        self.vertex_binding_descriptions.push(
            ash::vk::VertexInputBindingDescription::builder()
                .binding(binding)
                .stride(stride)
                .input_rate(input_rate.into())
                .build(),
        );
        self
    }

    /// Defines a vertex attribute.
    pub fn attribute(
        &mut self,
        location: u32,
        binding: u32,
        format: Format,
        offset: u32,
    ) -> &mut Self {
        self.vertex_attribute_descriptions.push(
            ash::vk::VertexInputAttributeDescription::builder()
                .location(location)
                .binding(binding)
                .format(format.into())
                .offset(offset)
                .build(),
        );
        self
    }

    /// Builds the [`VertexInput`]
    pub fn build(&mut self) -> VertexInput {
        VertexInput {
            vertex_binding_descriptions: self.vertex_binding_descriptions.clone(),
            vertex_attribute_descriptions: self.vertex_attribute_descriptions.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::image::Format;
    use crate::gfx::pipeline::vertex_input::VertexInput;
    use crate::gfx::pipeline::vertex_input_rate::VertexInputRate;

    #[test]
    fn build_vertex_input() {
        let vertex_input = VertexInput::builder()
            .binding(0, 24, VertexInputRate::Vertex)
            .attribute(0, 0, Format::R32G32B32SFloat, 0)
            .build();

        assert_eq!(0, vertex_input.vertex_binding_descriptions[0].binding);
        assert_eq!(24, vertex_input.vertex_binding_descriptions[0].stride);
        assert_eq!(
            ash::vk::VertexInputRate::VERTEX,
            vertex_input.vertex_binding_descriptions[0].input_rate
        );
        assert_eq!(0, vertex_input.vertex_attribute_descriptions[0].location);
        assert_eq!(0, vertex_input.vertex_attribute_descriptions[0].binding);
        assert_eq!(
            ash::vk::Format::R32G32B32_SFLOAT,
            vertex_input.vertex_attribute_descriptions[0].format
        );
        assert_eq!(0, vertex_input.vertex_attribute_descriptions[0].offset);
    }
}
