/// Stencil comparison function.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum CompareOp {
    /// The test evaluates to false.
    Never,
    /// The test evaluates A < B.
    Less,
    /// The test evaluates A = B.
    Equal,
    /// The test evaluates A ≤ B.
    LessOrEqual,
    /// The test evaluates A > B.
    Greater,
    /// The test evaluates A ≠ B.
    NotEqual,
    /// The test evaluates A ≥ B.
    GreaterOrEqual,
    /// The test evaluates to true.
    Always,
}

#[doc(hidden)]
impl From<CompareOp> for ash::vk::CompareOp {
    fn from(compare_op: CompareOp) -> Self {
        match compare_op {
            CompareOp::Never => ash::vk::CompareOp::NEVER,
            CompareOp::Less => ash::vk::CompareOp::LESS,
            CompareOp::Equal => ash::vk::CompareOp::EQUAL,
            CompareOp::LessOrEqual => ash::vk::CompareOp::LESS_OR_EQUAL,
            CompareOp::Greater => ash::vk::CompareOp::GREATER,
            CompareOp::NotEqual => ash::vk::CompareOp::NOT_EQUAL,
            CompareOp::GreaterOrEqual => ash::vk::CompareOp::GREATER_OR_EQUAL,
            CompareOp::Always => ash::vk::CompareOp::ALWAYS,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::CompareOp;

    #[test]
    fn to_compare_op() {
        assert_eq!(
            ash::vk::CompareOp::NEVER,
            ash::vk::CompareOp::from(CompareOp::Never)
        );
        assert_eq!(
            ash::vk::CompareOp::LESS,
            ash::vk::CompareOp::from(CompareOp::Less)
        );
        assert_eq!(
            ash::vk::CompareOp::EQUAL,
            ash::vk::CompareOp::from(CompareOp::Equal)
        );
        assert_eq!(
            ash::vk::CompareOp::LESS_OR_EQUAL,
            ash::vk::CompareOp::from(CompareOp::LessOrEqual)
        );
        assert_eq!(
            ash::vk::CompareOp::GREATER,
            ash::vk::CompareOp::from(CompareOp::Greater)
        );
        assert_eq!(
            ash::vk::CompareOp::NOT_EQUAL,
            ash::vk::CompareOp::from(CompareOp::NotEqual)
        );
        assert_eq!(
            ash::vk::CompareOp::GREATER_OR_EQUAL,
            ash::vk::CompareOp::from(CompareOp::GreaterOrEqual)
        );
        assert_eq!(
            ash::vk::CompareOp::ALWAYS,
            ash::vk::CompareOp::from(CompareOp::Always)
        );
    }
}
