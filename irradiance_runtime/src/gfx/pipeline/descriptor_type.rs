/// Type of a descriptor in a descriptor set.
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum DescriptorType {
    /// Sampler descriptor.
    Sampler,
    /// Combined image sampler descriptor.
    CombinedImageSampler,
    /// Sampled image descriptor.
    SampledImage,
    /// Storage image descriptor.
    StorageImage,
    /// Uniform texel buffer descriptor.
    UniformTexelBuffer,
    /// Storage texel buffer descriptor.
    StorageTexelBuffer,
    /// Uniform buffer descriptor.
    UniformBuffer,
    /// Storage buffer descriptor.
    StorageBuffer,
    /// Dynamic uniform buffer descriptor.
    UniformBufferDynamic,
    /// Dynamic storage buffer descriptor.
    StorageBufferDynamic,
    /// Input attachment descriptor.
    InputAttachment,
    /// Inline uniform block descriptor.
    InlineUniformBlock,
    /// Acceleration structure descriptor.
    AccelerationStructure,
    /// Mutable value descriptor.
    MutableValue,
}

#[doc(hidden)]
impl From<DescriptorType> for ash::vk::DescriptorType {
    fn from(descriptor_type: DescriptorType) -> Self {
        match descriptor_type {
            DescriptorType::Sampler => ash::vk::DescriptorType::SAMPLER,
            DescriptorType::CombinedImageSampler => ash::vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            DescriptorType::SampledImage => ash::vk::DescriptorType::SAMPLED_IMAGE,
            DescriptorType::StorageImage => ash::vk::DescriptorType::STORAGE_IMAGE,
            DescriptorType::UniformTexelBuffer => ash::vk::DescriptorType::UNIFORM_TEXEL_BUFFER,
            DescriptorType::StorageTexelBuffer => ash::vk::DescriptorType::STORAGE_TEXEL_BUFFER,
            DescriptorType::UniformBuffer => ash::vk::DescriptorType::UNIFORM_BUFFER,
            DescriptorType::StorageBuffer => ash::vk::DescriptorType::STORAGE_BUFFER,
            DescriptorType::UniformBufferDynamic => ash::vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
            DescriptorType::StorageBufferDynamic => ash::vk::DescriptorType::STORAGE_BUFFER_DYNAMIC,
            DescriptorType::InputAttachment => ash::vk::DescriptorType::INPUT_ATTACHMENT,
            DescriptorType::InlineUniformBlock => ash::vk::DescriptorType::INLINE_UNIFORM_BLOCK,
            DescriptorType::AccelerationStructure => {
                ash::vk::DescriptorType::ACCELERATION_STRUCTURE_KHR
            }
            DescriptorType::MutableValue => ash::vk::DescriptorType::MUTABLE_VALVE,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::pipeline::DescriptorType;

    #[test]
    fn to_descriptor_type() {
        assert_eq!(
            ash::vk::DescriptorType::SAMPLER,
            ash::vk::DescriptorType::from(DescriptorType::Sampler)
        );
        assert_eq!(
            ash::vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            ash::vk::DescriptorType::from(DescriptorType::CombinedImageSampler)
        );
        assert_eq!(
            ash::vk::DescriptorType::SAMPLED_IMAGE,
            ash::vk::DescriptorType::from(DescriptorType::SampledImage)
        );
        assert_eq!(
            ash::vk::DescriptorType::STORAGE_IMAGE,
            ash::vk::DescriptorType::from(DescriptorType::StorageImage)
        );
        assert_eq!(
            ash::vk::DescriptorType::UNIFORM_TEXEL_BUFFER,
            ash::vk::DescriptorType::from(DescriptorType::UniformTexelBuffer)
        );
        assert_eq!(
            ash::vk::DescriptorType::STORAGE_BUFFER,
            ash::vk::DescriptorType::from(DescriptorType::StorageBuffer)
        );
        assert_eq!(
            ash::vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
            ash::vk::DescriptorType::from(DescriptorType::UniformBufferDynamic)
        );
        assert_eq!(
            ash::vk::DescriptorType::STORAGE_BUFFER_DYNAMIC,
            ash::vk::DescriptorType::from(DescriptorType::StorageBufferDynamic)
        );
        assert_eq!(
            ash::vk::DescriptorType::INPUT_ATTACHMENT,
            ash::vk::DescriptorType::from(DescriptorType::InputAttachment)
        );
        assert_eq!(
            ash::vk::DescriptorType::INLINE_UNIFORM_BLOCK,
            ash::vk::DescriptorType::from(DescriptorType::InlineUniformBlock)
        );
        assert_eq!(
            ash::vk::DescriptorType::ACCELERATION_STRUCTURE_KHR,
            ash::vk::DescriptorType::from(DescriptorType::AccelerationStructure)
        );
        assert_eq!(
            ash::vk::DescriptorType::MUTABLE_VALVE,
            ash::vk::DescriptorType::from(DescriptorType::MutableValue)
        );
    }
}
