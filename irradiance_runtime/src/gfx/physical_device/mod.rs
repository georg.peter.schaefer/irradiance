//! Physical device.
//!
//! # Querying available `PhysicalDevice`s
//! Available [`PhysicalDevice`]s can be queried through an [`instance`](super::instance::Instance).
//!
//! ```
//! use irradiance_runtime::gfx::instance::{Instance, ApplicationInfo, InstanceExtensions};
//!
//! let instance = Instance::new(ApplicationInfo::default())
//!     .extensions(InstanceExtensions {
//!         ext_debug_utils: true,
//!         ..Default::default()
//!     })
//!     .build()
//!     .expect("Failed to create instance");
//!
//! for physical_device in instance.physical_devices().expect("Failed to query physical devices") {
//!     // Select a physical device
//! }
//!
//! ```
//!
//! # Supported Extensions and Features
//! A [`PhysicalDevice`] supports a certain set of [`DeviceExtensions`] and
//! [`PhysicalDeviceFeatures`]. Those can be queried from the [`PhysicalDevice`].
//!
//! ```
//! # use irradiance_runtime::gfx::instance::{Instance, ApplicationInfo, InstanceExtensions};
//! #
//! # let instance = Instance::new(ApplicationInfo::default())
//! #     .extensions(InstanceExtensions {
//! #         ext_debug_utils: true,
//! #         ..Default::default()
//! #     })
//! #     .build()
//! #     .expect("Failed to create instance");
//! # let physical_device = instance.physical_devices()
//! #     .expect("Failed to query physical devices")
//! #     .iter()
//! #     .next()
//! #     .cloned()
//! #     .unwrap();
//! #
//! // Query supported extensions
//! physical_device.extensions().expect("Failed to query extensions");
//!
//! // Query supported features
//! physical_device.features();
//! ```
//!
//! # Querying physical device properties and queue family properties
//! Properties of a [`PhysicalDevice`] can be queried via [`properties`](PhysicalDevice::properties).
//! The properties of the queue families supported by a [`PhysicalDevice`] can be queried using
//! [`queue_family_properties`](PhysicalDevice::queue_family_properties).
//!
//! ```
//! # use irradiance_runtime::gfx::instance::{Instance, ApplicationInfo, InstanceExtensions};
//! use irradiance_runtime::gfx::physical_device::PhysicalDeviceType;
//! #
//! # let instance = Instance::new(ApplicationInfo::default())
//! #     .extensions(InstanceExtensions {
//! #         ext_debug_utils: true,
//! #         ..Default::default()
//! #     })
//! #     .build()
//! #     .expect("Failed to create instance");
//! # let physical_device = instance.physical_devices()
//! #     .expect("Failed to query physical devices")
//! #     .iter()
//! #     .next()
//! #     .cloned()
//! #     .unwrap();
//! #
//! // Querying physical device properties
//! if physical_device.properties().device_type == PhysicalDeviceType::DiscreteGpu {
//!     // Do something if selected physical device is a discrete gpu
//! }
//!
//! // Querying supported queue families properties
//! let queue_family_index = physical_device.queue_family_properties()
//!     .into_iter()
//!     .enumerate()
//!     .filter(|(_, properties)| properties.queue_flags.graphics)
//!     .map(|(family_index, _)| family_index)
//!     .next()
//!     .unwrap();
//! ```

pub use extensions::DeviceExtensions;
pub use features::PhysicalDeviceFeatures;
pub(crate) use features::PhysicalDeviceFeatures2;
pub use physical_device_limits::PhysicalDeviceLimits;
pub use private::PhysicalDevice;
pub use properties::PhysicalDeviceProperties;
pub use properties::PhysicalDeviceType;
pub use sample_counts::SampleCounts;

mod extensions;
mod features;
mod physical_device_limits;
mod private;
mod properties;
mod sample_counts;
