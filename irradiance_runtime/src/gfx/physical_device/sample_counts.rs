use crate::impl_flag_set;
use ash::vk::SampleCountFlags;

impl_flag_set! {
    SampleCounts,
    doc = "Sample counts supported for an image used for storage operations.",
    (VK_SAMPLE_COUNT_1_BIT, sample1),
    (VK_SAMPLE_COUNT_2_BIT, sample2),
    (VK_SAMPLE_COUNT_4_BIT, sample4),
    (VK_SAMPLE_COUNT_8_BIT, sample8),
    (VK_SAMPLE_COUNT_16_BIT, sample16),
    (VK_SAMPLE_COUNT_32_BIT, sample32),
    (VK_SAMPLE_COUNT_64_BIT, sample64)
}

#[doc(hidden)]
impl From<ash::vk::SampleCountFlags> for SampleCounts {
    fn from(sample_counts: SampleCountFlags) -> Self {
        Self {
            sample1: sample_counts.contains(ash::vk::SampleCountFlags::TYPE_1),
            sample2: sample_counts.contains(ash::vk::SampleCountFlags::TYPE_2),
            sample4: sample_counts.contains(ash::vk::SampleCountFlags::TYPE_4),
            sample8: sample_counts.contains(ash::vk::SampleCountFlags::TYPE_8),
            sample16: sample_counts.contains(ash::vk::SampleCountFlags::TYPE_16),
            sample32: sample_counts.contains(ash::vk::SampleCountFlags::TYPE_32),
            sample64: sample_counts.contains(ash::vk::SampleCountFlags::TYPE_64),
        }
    }
}
