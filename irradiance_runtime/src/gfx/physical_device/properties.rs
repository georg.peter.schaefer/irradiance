use std::ffi::CStr;

use crate::gfx::physical_device::PhysicalDeviceLimits;
use ash::vk::PhysicalDeviceProperties2;

use crate::gfx::Version;

/// The properties of a physical device.
#[derive(Clone, Debug)]
pub struct PhysicalDeviceProperties {
    /// The version of Vulkan supported by the device.
    pub api_version: Version,
    /// The vendor-specified version of the driver.
    pub driver_version: Version,
    /// A unique identifier for the vendor of the physical device.
    pub vendor_id: u32,
    /// A unique identifier for the physical device among devices available from the vendor.
    pub device_id: u32,
    /// The type of device.
    pub device_type: PhysicalDeviceType,
    /// The name of the device.
    pub device_name: String,
    /// A universal unique identifier for the device.
    pub pipeline_cache_uuid: [u8; ash::vk::UUID_SIZE],
    /// Device-specific limits of the physical device.
    pub limits: PhysicalDeviceLimits,
}

#[doc(hidden)]
impl From<ash::vk::PhysicalDeviceProperties2> for PhysicalDeviceProperties {
    fn from(properties2: PhysicalDeviceProperties2) -> Self {
        Self {
            api_version: properties2.properties.api_version.into(),
            driver_version: properties2.properties.driver_version.into(),
            vendor_id: properties2.properties.vendor_id,
            device_id: properties2.properties.device_id,
            device_type: properties2.properties.device_type.into(),
            device_name: unsafe { CStr::from_ptr(properties2.properties.device_name.as_ptr()) }
                .to_str()
                .unwrap()
                .to_string(),
            pipeline_cache_uuid: properties2.properties.pipeline_cache_uuid,
            limits: properties2.properties.limits.into(),
        }
    }
}

/// Types of physical devices.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum PhysicalDeviceType {
    /// The device does not match any other available types.
    Other,
    /// The device is typically one embedded in or tightly coupled with the host.
    IntegratedGpu,
    /// The device is typically a separate processor connected to the host via an interlink.
    DiscreteGpu,
    /// The device is typically a virtual node in a virtualization environment.
    VirtualGpu,
    /// The device is typically running on the same processors as the host
    Cpu,
}

#[doc(hidden)]
impl From<ash::vk::PhysicalDeviceType> for PhysicalDeviceType {
    fn from(device_type: ash::vk::PhysicalDeviceType) -> Self {
        match device_type {
            ash::vk::PhysicalDeviceType::OTHER => Self::Other,
            ash::vk::PhysicalDeviceType::INTEGRATED_GPU => Self::IntegratedGpu,
            ash::vk::PhysicalDeviceType::DISCRETE_GPU => Self::DiscreteGpu,
            ash::vk::PhysicalDeviceType::VIRTUAL_GPU => Self::VirtualGpu,
            ash::vk::PhysicalDeviceType::CPU => Self::Cpu,
            _ => unreachable!("Unrecognized 'PhysicalDeviceType'"),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::physical_device::PhysicalDeviceType;

    #[test]
    fn from_physical_type() {
        assert_eq!(
            PhysicalDeviceType::Other,
            PhysicalDeviceType::from(ash::vk::PhysicalDeviceType::OTHER)
        );
        assert_eq!(
            PhysicalDeviceType::IntegratedGpu,
            PhysicalDeviceType::from(ash::vk::PhysicalDeviceType::INTEGRATED_GPU)
        );
        assert_eq!(
            PhysicalDeviceType::DiscreteGpu,
            PhysicalDeviceType::from(ash::vk::PhysicalDeviceType::DISCRETE_GPU)
        );
        assert_eq!(
            PhysicalDeviceType::VirtualGpu,
            PhysicalDeviceType::from(ash::vk::PhysicalDeviceType::VIRTUAL_GPU)
        );
        assert_eq!(
            PhysicalDeviceType::Cpu,
            PhysicalDeviceType::from(ash::vk::PhysicalDeviceType::CPU)
        );
    }
}
