use crate::gfx::physical_device::SampleCounts;

/// Implementation-dependent physical device limits.
#[allow(missing_docs)]
#[derive(Copy, Clone, Debug)]
pub struct PhysicalDeviceLimits {
    pub max_image_dimension1_d: u32,
    pub max_image_dimension2_d: u32,
    pub max_image_dimension3_d: u32,
    pub max_image_dimension_cube: u32,
    pub max_image_array_layers: u32,
    pub max_texel_buffer_elements: u32,
    pub max_uniform_buffer_range: u32,
    pub max_storage_buffer_range: u32,
    pub max_push_constants_size: u32,
    pub max_memory_allocation_count: u32,
    pub max_sampler_allocation_count: u32,
    pub buffer_image_granularity: u64,
    pub sparse_address_space_size: u64,
    pub max_bound_descriptor_sets: u32,
    pub max_per_stage_descriptor_samplers: u32,
    pub max_per_stage_descriptor_uniform_buffers: u32,
    pub max_per_stage_descriptor_storage_buffers: u32,
    pub max_per_stage_descriptor_sampled_images: u32,
    pub max_per_stage_descriptor_storage_images: u32,
    pub max_per_stage_descriptor_input_attachments: u32,
    pub max_per_stage_resources: u32,
    pub max_descriptor_set_samplers: u32,
    pub max_descriptor_set_uniform_buffers: u32,
    pub max_descriptor_set_uniform_buffers_dynamic: u32,
    pub max_descriptor_set_storage_buffers: u32,
    pub max_descriptor_set_storage_buffers_dynamic: u32,
    pub max_descriptor_set_sampled_images: u32,
    pub max_descriptor_set_storage_images: u32,
    pub max_descriptor_set_input_attachments: u32,
    pub max_vertex_input_attributes: u32,
    pub max_vertex_input_bindings: u32,
    pub max_vertex_input_attribute_offset: u32,
    pub max_vertex_input_binding_stride: u32,
    pub max_vertex_output_components: u32,
    pub max_tessellation_generation_level: u32,
    pub max_tessellation_patch_size: u32,
    pub max_tessellation_control_per_vertex_input_components: u32,
    pub max_tessellation_control_per_vertex_output_components: u32,
    pub max_tessellation_control_per_patch_output_components: u32,
    pub max_tessellation_control_total_output_components: u32,
    pub max_tessellation_evaluation_input_components: u32,
    pub max_tessellation_evaluation_output_components: u32,
    pub max_geometry_shader_invocations: u32,
    pub max_geometry_input_components: u32,
    pub max_geometry_output_components: u32,
    pub max_geometry_output_vertices: u32,
    pub max_geometry_total_output_components: u32,
    pub max_fragment_input_components: u32,
    pub max_fragment_output_attachments: u32,
    pub max_fragment_dual_src_attachments: u32,
    pub max_fragment_combined_output_resources: u32,
    pub max_compute_shared_memory_size: u32,
    pub max_compute_work_group_count: [u32; 3],
    pub max_compute_work_group_invocations: u32,
    pub max_compute_work_group_size: [u32; 3],
    pub sub_pixel_precision_bits: u32,
    pub sub_texel_precision_bits: u32,
    pub mipmap_precision_bits: u32,
    pub max_draw_indexed_index_value: u32,
    pub max_draw_indirect_count: u32,
    pub max_sampler_lod_bias: f32,
    pub max_sampler_anisotropy: f32,
    pub max_viewports: u32,
    pub max_viewport_dimensions: [u32; 2],
    pub viewport_bounds_range: [f32; 2],
    pub viewport_sub_pixel_bits: u32,
    pub min_memory_map_alignment: usize,
    pub min_texel_buffer_offset_alignment: u64,
    pub min_uniform_buffer_offset_alignment: u64,
    pub min_storage_buffer_offset_alignment: u64,
    pub min_texel_offset: i32,
    pub max_texel_offset: u32,
    pub min_texel_gather_offset: i32,
    pub max_texel_gather_offset: u32,
    pub min_interpolation_offset: f32,
    pub max_interpolation_offset: f32,
    pub sub_pixel_interpolation_offset_bits: u32,
    pub max_framebuffer_width: u32,
    pub max_framebuffer_height: u32,
    pub max_framebuffer_layers: u32,
    pub framebuffer_color_sample_counts: SampleCounts,
    pub framebuffer_depth_sample_counts: SampleCounts,
    pub framebuffer_stencil_sample_counts: SampleCounts,
    pub framebuffer_no_attachments_sample_counts: SampleCounts,
    pub max_color_attachments: u32,
    pub sampled_image_color_sample_counts: SampleCounts,
    pub sampled_image_integer_sample_counts: SampleCounts,
    pub sampled_image_depth_sample_counts: SampleCounts,
    pub sampled_image_stencil_sample_counts: SampleCounts,
    pub storage_image_sample_counts: SampleCounts,
    pub max_sample_mask_words: u32,
    pub timestamp_compute_and_graphics: bool,
    pub timestamp_period: f32,
    pub max_clip_distances: u32,
    pub max_cull_distances: u32,
    pub max_combined_clip_and_cull_distances: u32,
    pub discrete_queue_priorities: u32,
    pub point_size_range: [f32; 2],
    pub line_width_range: [f32; 2],
    pub point_size_granularity: f32,
    pub line_width_granularity: f32,
    pub strict_lines: bool,
    pub standard_sample_locations: bool,
    pub optimal_buffer_copy_offset_alignment: u64,
    pub optimal_buffer_copy_row_pitch_alignment: u64,
    pub non_coherent_atom_size: u64,
}

impl From<ash::vk::PhysicalDeviceLimits> for PhysicalDeviceLimits {
    fn from(physical_device_limits: ash::vk::PhysicalDeviceLimits) -> Self {
        Self {
            max_image_dimension1_d: physical_device_limits.max_image_dimension1_d,
            max_image_dimension2_d: physical_device_limits.max_image_dimension2_d,
            max_image_dimension3_d: physical_device_limits.max_image_dimension3_d,
            max_image_dimension_cube: physical_device_limits.max_image_dimension_cube,
            max_image_array_layers: physical_device_limits.max_image_array_layers,
            max_texel_buffer_elements: physical_device_limits.max_texel_buffer_elements,
            max_uniform_buffer_range: physical_device_limits.max_uniform_buffer_range,
            max_storage_buffer_range: physical_device_limits.max_storage_buffer_range,
            max_push_constants_size: physical_device_limits.max_push_constants_size,
            max_memory_allocation_count: physical_device_limits.max_memory_allocation_count,
            max_sampler_allocation_count: physical_device_limits.max_sampler_allocation_count,
            buffer_image_granularity: physical_device_limits.buffer_image_granularity,
            sparse_address_space_size: physical_device_limits.sparse_address_space_size,
            max_bound_descriptor_sets: physical_device_limits.max_bound_descriptor_sets,
            max_per_stage_descriptor_samplers: physical_device_limits
                .max_per_stage_descriptor_samplers,
            max_per_stage_descriptor_uniform_buffers: physical_device_limits
                .max_descriptor_set_uniform_buffers,
            max_per_stage_descriptor_storage_buffers: physical_device_limits
                .max_descriptor_set_storage_buffers,
            max_per_stage_descriptor_sampled_images: physical_device_limits
                .max_descriptor_set_sampled_images,
            max_per_stage_descriptor_storage_images: physical_device_limits
                .max_descriptor_set_storage_images,
            max_per_stage_descriptor_input_attachments: physical_device_limits
                .max_descriptor_set_input_attachments,
            max_per_stage_resources: physical_device_limits.max_per_stage_resources,
            max_descriptor_set_samplers: physical_device_limits.max_descriptor_set_samplers,
            max_descriptor_set_uniform_buffers: physical_device_limits
                .max_descriptor_set_uniform_buffers,
            max_descriptor_set_uniform_buffers_dynamic: physical_device_limits
                .max_descriptor_set_uniform_buffers_dynamic,
            max_descriptor_set_storage_buffers: physical_device_limits
                .max_descriptor_set_storage_buffers,
            max_descriptor_set_storage_buffers_dynamic: physical_device_limits
                .max_descriptor_set_storage_buffers_dynamic,
            max_descriptor_set_sampled_images: physical_device_limits
                .max_descriptor_set_sampled_images,
            max_descriptor_set_storage_images: physical_device_limits
                .max_descriptor_set_storage_images,
            max_descriptor_set_input_attachments: physical_device_limits
                .max_descriptor_set_input_attachments,
            max_vertex_input_attributes: physical_device_limits.max_vertex_input_attributes,
            max_vertex_input_bindings: physical_device_limits.max_vertex_input_bindings,
            max_vertex_input_attribute_offset: physical_device_limits
                .max_vertex_input_attribute_offset,
            max_vertex_input_binding_stride: physical_device_limits.max_vertex_input_binding_stride,
            max_vertex_output_components: physical_device_limits.max_vertex_output_components,
            max_tessellation_generation_level: physical_device_limits
                .max_tessellation_generation_level,
            max_tessellation_patch_size: physical_device_limits.max_tessellation_patch_size,
            max_tessellation_control_per_vertex_input_components: physical_device_limits
                .max_tessellation_control_per_vertex_input_components,
            max_tessellation_control_per_vertex_output_components: physical_device_limits
                .max_tessellation_control_per_vertex_output_components,
            max_tessellation_control_per_patch_output_components: physical_device_limits
                .max_tessellation_control_per_patch_output_components,
            max_tessellation_control_total_output_components: physical_device_limits
                .max_tessellation_control_total_output_components,
            max_tessellation_evaluation_input_components: physical_device_limits
                .max_tessellation_evaluation_input_components,
            max_tessellation_evaluation_output_components: physical_device_limits
                .max_tessellation_evaluation_output_components,
            max_geometry_shader_invocations: physical_device_limits.max_geometry_shader_invocations,
            max_geometry_input_components: physical_device_limits.max_geometry_input_components,
            max_geometry_output_components: physical_device_limits.max_geometry_output_components,
            max_geometry_output_vertices: physical_device_limits.max_geometry_output_vertices,
            max_geometry_total_output_components: physical_device_limits
                .max_geometry_total_output_components,
            max_fragment_input_components: physical_device_limits.max_fragment_input_components,
            max_fragment_output_attachments: physical_device_limits.max_fragment_output_attachments,
            max_fragment_dual_src_attachments: physical_device_limits
                .max_fragment_dual_src_attachments,
            max_fragment_combined_output_resources: physical_device_limits
                .max_fragment_combined_output_resources,
            max_compute_shared_memory_size: physical_device_limits.max_compute_shared_memory_size,
            max_compute_work_group_count: physical_device_limits.max_compute_work_group_count,
            max_compute_work_group_invocations: physical_device_limits
                .max_compute_work_group_invocations,
            max_compute_work_group_size: physical_device_limits.max_compute_work_group_size,
            sub_pixel_precision_bits: physical_device_limits.sub_pixel_precision_bits,
            sub_texel_precision_bits: physical_device_limits.sub_texel_precision_bits,
            mipmap_precision_bits: physical_device_limits.mipmap_precision_bits,
            max_draw_indexed_index_value: physical_device_limits.max_draw_indexed_index_value,
            max_draw_indirect_count: physical_device_limits.max_draw_indirect_count,
            max_sampler_lod_bias: physical_device_limits.max_sampler_lod_bias,
            max_sampler_anisotropy: physical_device_limits.max_sampler_anisotropy,
            max_viewports: physical_device_limits.max_viewports,
            max_viewport_dimensions: physical_device_limits.max_viewport_dimensions,
            viewport_bounds_range: physical_device_limits.viewport_bounds_range,
            viewport_sub_pixel_bits: physical_device_limits.viewport_sub_pixel_bits,
            min_memory_map_alignment: physical_device_limits.min_memory_map_alignment,
            min_texel_buffer_offset_alignment: physical_device_limits
                .min_texel_buffer_offset_alignment,
            min_uniform_buffer_offset_alignment: physical_device_limits
                .min_uniform_buffer_offset_alignment,
            min_storage_buffer_offset_alignment: physical_device_limits
                .min_storage_buffer_offset_alignment,
            min_texel_offset: physical_device_limits.min_texel_offset,
            max_texel_offset: physical_device_limits.max_texel_offset,
            min_texel_gather_offset: physical_device_limits.min_texel_gather_offset,
            max_texel_gather_offset: physical_device_limits.max_texel_gather_offset,
            min_interpolation_offset: physical_device_limits.min_interpolation_offset,
            max_interpolation_offset: physical_device_limits.max_interpolation_offset,
            sub_pixel_interpolation_offset_bits: physical_device_limits
                .sub_pixel_interpolation_offset_bits,
            max_framebuffer_width: physical_device_limits.max_framebuffer_width,
            max_framebuffer_height: physical_device_limits.max_framebuffer_height,
            max_framebuffer_layers: physical_device_limits.max_framebuffer_layers,
            framebuffer_color_sample_counts: physical_device_limits
                .framebuffer_color_sample_counts
                .into(),
            framebuffer_depth_sample_counts: physical_device_limits
                .framebuffer_depth_sample_counts
                .into(),
            framebuffer_stencil_sample_counts: physical_device_limits
                .framebuffer_stencil_sample_counts
                .into(),
            framebuffer_no_attachments_sample_counts: physical_device_limits
                .framebuffer_no_attachments_sample_counts
                .into(),
            max_color_attachments: physical_device_limits.max_color_attachments,
            sampled_image_color_sample_counts: physical_device_limits
                .sampled_image_color_sample_counts
                .into(),
            sampled_image_integer_sample_counts: physical_device_limits
                .sampled_image_integer_sample_counts
                .into(),
            sampled_image_depth_sample_counts: physical_device_limits
                .sampled_image_depth_sample_counts
                .into(),
            sampled_image_stencil_sample_counts: physical_device_limits
                .sampled_image_stencil_sample_counts
                .into(),
            storage_image_sample_counts: physical_device_limits.storage_image_sample_counts.into(),
            max_sample_mask_words: physical_device_limits.max_sample_mask_words,
            timestamp_compute_and_graphics: physical_device_limits.timestamp_compute_and_graphics
                == ash::vk::TRUE,
            timestamp_period: physical_device_limits.timestamp_period,
            max_clip_distances: physical_device_limits.max_clip_distances,
            max_cull_distances: physical_device_limits.max_cull_distances,
            max_combined_clip_and_cull_distances: physical_device_limits
                .max_combined_clip_and_cull_distances,
            discrete_queue_priorities: physical_device_limits.discrete_queue_priorities,
            point_size_range: physical_device_limits.point_size_range,
            line_width_range: physical_device_limits.line_width_range,
            point_size_granularity: physical_device_limits.point_size_granularity,
            line_width_granularity: physical_device_limits.line_width_granularity,
            strict_lines: physical_device_limits.strict_lines == ash::vk::TRUE,
            standard_sample_locations: physical_device_limits.standard_sample_locations
                == ash::vk::TRUE,
            optimal_buffer_copy_offset_alignment: physical_device_limits
                .optimal_buffer_copy_offset_alignment,
            optimal_buffer_copy_row_pitch_alignment: physical_device_limits
                .optimal_buffer_copy_row_pitch_alignment,
            non_coherent_atom_size: physical_device_limits.non_coherent_atom_size,
        }
    }
}
