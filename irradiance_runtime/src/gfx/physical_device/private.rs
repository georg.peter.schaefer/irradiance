use std::ffi::CStr;
use std::sync::Arc;

use crate::gfx::instance::Instance;
use crate::gfx::physical_device::features::PhysicalDeviceFeatures2;
use crate::gfx::physical_device::PhysicalDeviceProperties;
use crate::gfx::physical_device::{DeviceExtensions, PhysicalDeviceFeatures};
use crate::gfx::queue::QueueFamilyProperties;
use crate::gfx::wsi::{PresentMode, Surface, SurfaceCapabilities, SurfaceFormat};
use crate::gfx::{GfxError, Handle, ObjectType};

/// A physical device.
#[derive(Debug)]
pub struct PhysicalDevice {
    handle: ash::vk::PhysicalDevice,
    instance: Arc<Instance>,
}

impl PhysicalDevice {
    pub(crate) fn new(instance: Arc<Instance>, inner: ash::vk::PhysicalDevice) -> Arc<Self> {
        Arc::new(Self {
            handle: inner,
            instance,
        })
    }

    /// Returns the [`Instance`].
    pub fn instance(&self) -> &Arc<Instance> {
        &self.instance
    }

    /// Returns the [`DeviceExtensions`] supported by the device.
    ///
    /// # Errors
    /// This functions returns an error if enumerating the device extensions fails.
    pub fn extensions(&self) -> Result<DeviceExtensions, GfxError> {
        Ok(DeviceExtensions::from(
            unsafe {
                self.instance
                    .inner()
                    .enumerate_device_extension_properties(self.handle)
            }?
            .iter()
            .map(|property| unsafe { CStr::from_ptr(property.extension_name.as_ptr()) }),
        ))
    }

    /// Returns the [`PhysicalDeviceProperties`] supported by the device.
    pub fn properties(&self) -> PhysicalDeviceProperties {
        let mut properties = ash::vk::PhysicalDeviceProperties2::default();
        unsafe {
            self.instance
                .inner()
                .get_physical_device_properties2(self.handle, &mut properties)
        };
        PhysicalDeviceProperties::from(properties)
    }

    /// Returns the [`PhysicalDeviceFeatures`] supported by the device.
    pub fn features(&self) -> PhysicalDeviceFeatures {
        let mut features2 = PhysicalDeviceFeatures2::new();
        let mut features = features2.as_ffi();
        unsafe {
            self.instance
                .inner()
                .get_physical_device_features2(self.handle, &mut features)
        };
        features2.features = features.features;
        PhysicalDeviceFeatures::from(features2)
    }

    /// Returns the properties of the queue families supported by the device.
    pub fn queue_family_properties(&self) -> Vec<QueueFamilyProperties> {
        let len = unsafe {
            self.instance
                .inner()
                .get_physical_device_queue_family_properties2_len(self.handle)
        };
        let mut properties = vec![ash::vk::QueueFamilyProperties2::default(); len];
        unsafe {
            self.instance
                .inner()
                .get_physical_device_queue_family_properties2(self.handle, &mut properties)
        };
        properties
            .into_iter()
            .map(QueueFamilyProperties::from)
            .collect()
    }

    /// Returns `true` if the queue family with `queue_family_index` supports presentation to
    /// `surface`.
    ///
    /// # Errors
    /// This function returns an error if querying for surface support fails.
    pub fn is_surface_supported(
        &self,
        queue_family_index: u32,
        surface: &Surface,
    ) -> Result<bool, GfxError> {
        Ok(unsafe {
            surface.loader().get_physical_device_surface_support(
                self.handle,
                queue_family_index,
                surface.handle(),
            )
        }?)
    }

    /// Returns the capabilities for `surface` on the physical device.
    ///
    /// # Errors
    /// This function returns an error if querying for the capabilities fails.  
    pub fn surface_capabilities(&self, surface: &Surface) -> Result<SurfaceCapabilities, GfxError> {
        Ok(unsafe {
            surface
                .loader()
                .get_physical_device_surface_capabilities(self.handle, surface.handle())
        }?
        .into())
    }

    /// Returns the color formats supported by `surface`.
    ///
    /// # Errors
    /// This function returns an error if querying for the color formats fails.
    pub fn surface_formats(&self, surface: &Surface) -> Result<Vec<SurfaceFormat>, GfxError> {
        Ok(unsafe {
            surface
                .loader()
                .get_physical_device_surface_formats(self.handle, surface.handle())
        }?
        .into_iter()
        .map(SurfaceFormat::from)
        .collect())
    }

    /// Returns the color formats supported by `surface`.
    ///
    /// # Errors
    /// This function returns an error if querying for the color formats fails.
    pub fn surface_present_modes(&self, surface: &Surface) -> Result<Vec<PresentMode>, GfxError> {
        Ok(unsafe {
            surface
                .loader()
                .get_physical_device_surface_present_modes(self.handle, surface.handle())
        }?
        .into_iter()
        .map(PresentMode::from)
        .collect())
    }
}

#[doc(hidden)]
impl Handle for PhysicalDevice {
    type Type = ash::vk::PhysicalDevice;

    fn object_type(&self) -> ObjectType {
        ObjectType::PhysicalDevice
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::physical_device::{
        DeviceExtensions, PhysicalDeviceFeatures, PhysicalDeviceType,
    };
    use crate::gfx::queue::QueueFlags;

    #[macro_export]
    macro_rules! physical_device {
        () => {
            crate::instance!()
                .physical_devices()
                .unwrap()
                .first()
                .cloned()
                .unwrap()
        };
    }

    #[test]
    fn extensions() {
        let physical_device = physical_device!();
        let extensions = physical_device.extensions().unwrap();
        println!("supported device extensions: {}", extensions);

        assert!(DeviceExtensions {
            khr_maintenance1: true,
            khr_maintenance2: true,
            khr_maintenance3: true,
            khr_maintenance4: true,
            ..Default::default()
        }
        .is_subset(&extensions),);
    }

    #[test]
    fn properties() {
        let physical_device = physical_device!();
        let properties = physical_device.properties();
        println!("{}", properties.device_name);
        assert!(matches!(
            properties.device_type,
            PhysicalDeviceType::DiscreteGpu | PhysicalDeviceType::Cpu
        ));
    }

    #[test]
    fn features() {
        let physical_device = physical_device!();
        let features = physical_device.features();
        println!("supported device features: {}", features);

        assert!(PhysicalDeviceFeatures {
            robust_buffer_access: true,
            fragment_stores_and_atomics: true,
            dynamic_rendering: true,
            ..Default::default()
        }
        .is_subset(&features));
    }

    #[test]
    fn queue_family_properties() {
        let physical_device = physical_device!();
        let queue_family_properties = physical_device.queue_family_properties();

        assert!(!queue_family_properties.is_empty());
        assert!(QueueFlags {
            graphics: true,
            ..Default::default()
        }
        .is_subset(&queue_family_properties[0].queue_flags))
    }
}
