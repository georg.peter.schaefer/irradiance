use crate::impl_flag_set;

impl_flag_set! {
    PhysicalDeviceFeatures,
    doc = "Physical device features.",
    (robustBufferAccess, robust_buffer_access),
    (fullDrawIndexUint32, full_draw_index_uint32),
    (imageCubeArray, image_cube_array),
    (independentBlend, independent_blend),
    (geometryShader, geometry_shader),
    (tessellationShader, tessellation_shader),
    (sampleRateShading, sample_rate_shading),
    (dualSrcBlend, dual_src_blend),
    (logicOp, logic_op),
    (multiDrawIndirect, multi_draw_indirect),
    (drawIndirectFirstInstance, draw_indirect_first_instance),
    (depthClamp, depth_clamp),
    (depthBiasClamp, depth_bias_clamp),
    (fillModeNonSolid, fill_mode_non_solid),
    (depthBounds, depth_bounds),
    (wideLines, wide_lines),
    (largePoints, large_points),
    (alphaToOne, alpha_to_one),
    (multiViewport, multi_viewport),
    (samplerAnisotropy, sampler_anisotropy),
    (textureCompressionETC2, texture_compression_etc2),
    (textureCompressionASTC_LDR, texture_compression_astc_ldr),
    (textureCompressionBC, texture_compression_bc),
    (occlusionQueryPrecise, occlusion_query_precise),
    (pipelineStatisticsQuery, pipeline_statistics_query),
    (vertexPipelineStoresAndAtomics, vertex_pipeline_stores_and_atomics),
    (fragmentStoresAndAtomics, fragment_stores_and_atomics),
    (shaderTessellationAndGeometryPointSize, shader_tessellation_and_geometry_point_size),
    (shaderImageGatherExtended, shader_image_gather_extended),
    (shaderStorageImageExtendedFormats, shader_storage_image_extended_formats),
    (shaderStorageImageMultisample, shader_storage_image_multisample),
    (shaderStorageImageReadWithoutFormat, shader_storage_image_read_without_format),
    (shaderStorageImageWriteWithoutFormat, shader_storage_image_write_without_format),
    (shaderUniformBufferArrayDynamicIndexing, shader_uniform_buffer_array_dynamic_indexing),
    (shaderSampledImageArrayDynamicIndexing, shader_sampled_image_array_dynamic_indexing),
    (shaderStorageBufferArrayDynamicIndexing, shader_storage_buffer_array_dynamic_indexing),
    (shaderStorageImageArrayDynamicIndexing, shader_storage_image_array_dynamic_indexing),
    (shaderClipDistance, shader_clip_distance),
    (shaderCullDistance, shader_cull_distance),
    (shaderFloat64, shader_float64),
    (shaderInt64, shader_int64),
    (shaderInt16, shader_int16),
    (shaderResourceResidency, shader_resource_residency),
    (shaderResourceMinLod, shader_resource_min_lod),
    (sparseBinding, sparse_binding),
    (sparseResidencyBuffer, sparse_residency_buffer),
    (sparseResidencyImage2D, sparse_residency_image2d),
    (sparseResidencyImage3D, sparse_residency_image3d),
    (sparseResidency2Samples, sparse_residency_2_samples),
    (sparseResidency4Samples, sparse_residency_4_samples),
    (sparseResidency8Samples, sparse_residency_8_samples),
    (sparseResidency16Samples, sparse_residency_16_samples),
    (sparseResidencyAliased, sparse_residency_aliased),
    (variableMultisampleRate, variable_multisample_rate),
    (inheritedQueries, inherited_queries),
    (synchronization2, synchronization2),
    (dynamicRendering, dynamic_rendering)
}

#[doc(hidden)]
impl From<ash::vk::PhysicalDeviceFeatures> for PhysicalDeviceFeatures {
    fn from(features: ash::vk::PhysicalDeviceFeatures) -> Self {
        Self {
            robust_buffer_access: features.robust_buffer_access == ash::vk::TRUE,
            full_draw_index_uint32: features.full_draw_index_uint32 == ash::vk::TRUE,
            image_cube_array: features.image_cube_array == ash::vk::TRUE,
            independent_blend: features.independent_blend == ash::vk::TRUE,
            geometry_shader: features.geometry_shader == ash::vk::TRUE,
            tessellation_shader: features.tessellation_shader == ash::vk::TRUE,
            sample_rate_shading: features.sample_rate_shading == ash::vk::TRUE,
            dual_src_blend: features.dual_src_blend == ash::vk::TRUE,
            logic_op: features.logic_op == ash::vk::TRUE,
            multi_draw_indirect: features.multi_draw_indirect == ash::vk::TRUE,
            draw_indirect_first_instance: features.draw_indirect_first_instance == ash::vk::TRUE,
            depth_clamp: features.depth_clamp == ash::vk::TRUE,
            depth_bias_clamp: features.depth_bias_clamp == ash::vk::TRUE,
            fill_mode_non_solid: features.fill_mode_non_solid == ash::vk::TRUE,
            depth_bounds: features.depth_bounds == ash::vk::TRUE,
            wide_lines: features.wide_lines == ash::vk::TRUE,
            large_points: features.large_points == ash::vk::TRUE,
            alpha_to_one: features.alpha_to_one == ash::vk::TRUE,
            multi_viewport: features.multi_viewport == ash::vk::TRUE,
            sampler_anisotropy: features.sampler_anisotropy == ash::vk::TRUE,
            texture_compression_etc2: features.texture_compression_etc2 == ash::vk::TRUE,
            texture_compression_astc_ldr: features.texture_compression_astc_ldr == ash::vk::TRUE,
            texture_compression_bc: features.texture_compression_bc == ash::vk::TRUE,
            occlusion_query_precise: features.occlusion_query_precise == ash::vk::TRUE,
            pipeline_statistics_query: features.pipeline_statistics_query == ash::vk::TRUE,
            vertex_pipeline_stores_and_atomics: features.vertex_pipeline_stores_and_atomics
                == ash::vk::TRUE,
            fragment_stores_and_atomics: features.fragment_stores_and_atomics == ash::vk::TRUE,
            shader_tessellation_and_geometry_point_size: features
                .shader_tessellation_and_geometry_point_size
                == ash::vk::TRUE,
            shader_image_gather_extended: features.shader_image_gather_extended == ash::vk::TRUE,
            shader_storage_image_extended_formats: features.shader_storage_image_extended_formats
                == ash::vk::TRUE,
            shader_storage_image_multisample: features.shader_storage_image_multisample
                == ash::vk::TRUE,
            shader_storage_image_read_without_format: features
                .shader_storage_image_read_without_format
                == ash::vk::TRUE,
            shader_storage_image_write_without_format: features
                .shader_storage_image_write_without_format
                == ash::vk::TRUE,
            shader_uniform_buffer_array_dynamic_indexing: features
                .shader_uniform_buffer_array_dynamic_indexing
                == ash::vk::TRUE,
            shader_sampled_image_array_dynamic_indexing: features
                .shader_sampled_image_array_dynamic_indexing
                == ash::vk::TRUE,
            shader_storage_buffer_array_dynamic_indexing: features
                .shader_storage_buffer_array_dynamic_indexing
                == ash::vk::TRUE,
            shader_storage_image_array_dynamic_indexing: features
                .shader_storage_image_array_dynamic_indexing
                == ash::vk::TRUE,
            shader_clip_distance: features.shader_clip_distance == ash::vk::TRUE,
            shader_cull_distance: features.shader_cull_distance == ash::vk::TRUE,
            shader_float64: features.shader_float64 == ash::vk::TRUE,
            shader_int64: features.shader_int64 == ash::vk::TRUE,
            shader_int16: features.shader_int16 == ash::vk::TRUE,
            shader_resource_residency: features.shader_resource_residency == ash::vk::TRUE,
            shader_resource_min_lod: features.shader_resource_min_lod == ash::vk::TRUE,
            sparse_binding: features.sparse_binding == ash::vk::TRUE,
            sparse_residency_buffer: features.sparse_residency_buffer == ash::vk::TRUE,
            sparse_residency_image2d: features.sparse_residency_image2_d == ash::vk::TRUE,
            sparse_residency_image3d: features.sparse_residency_image3_d == ash::vk::TRUE,
            sparse_residency_2_samples: features.sparse_residency2_samples == ash::vk::TRUE,
            sparse_residency_4_samples: features.sparse_residency4_samples == ash::vk::TRUE,
            sparse_residency_8_samples: features.sparse_residency8_samples == ash::vk::TRUE,
            sparse_residency_16_samples: features.sparse_residency16_samples == ash::vk::TRUE,
            sparse_residency_aliased: features.sparse_residency_aliased == ash::vk::TRUE,
            variable_multisample_rate: features.variable_multisample_rate == ash::vk::TRUE,
            inherited_queries: features.inherited_queries == ash::vk::TRUE,
            dynamic_rendering: false,
            synchronization2: false,
        }
    }
}

#[doc(hidden)]
impl From<PhysicalDeviceFeatures> for ash::vk::PhysicalDeviceFeatures {
    fn from(features: PhysicalDeviceFeatures) -> Self {
        Self {
            robust_buffer_access: features.robust_buffer_access as ash::vk::Bool32,
            full_draw_index_uint32: features.full_draw_index_uint32 as ash::vk::Bool32,
            image_cube_array: features.image_cube_array as ash::vk::Bool32,
            independent_blend: features.independent_blend as ash::vk::Bool32,
            geometry_shader: features.geometry_shader as ash::vk::Bool32,
            tessellation_shader: features.tessellation_shader as ash::vk::Bool32,
            sample_rate_shading: features.sample_rate_shading as ash::vk::Bool32,
            dual_src_blend: features.dual_src_blend as ash::vk::Bool32,
            logic_op: features.logic_op as ash::vk::Bool32,
            multi_draw_indirect: features.multi_draw_indirect as ash::vk::Bool32,
            draw_indirect_first_instance: features.draw_indirect_first_instance as ash::vk::Bool32,
            depth_clamp: features.depth_clamp as ash::vk::Bool32,
            depth_bias_clamp: features.depth_bias_clamp as ash::vk::Bool32,
            fill_mode_non_solid: features.fill_mode_non_solid as ash::vk::Bool32,
            depth_bounds: features.depth_bounds as ash::vk::Bool32,
            wide_lines: features.wide_lines as ash::vk::Bool32,
            large_points: features.large_points as ash::vk::Bool32,
            alpha_to_one: features.alpha_to_one as ash::vk::Bool32,
            multi_viewport: features.multi_viewport as ash::vk::Bool32,
            sampler_anisotropy: features.sampler_anisotropy as ash::vk::Bool32,
            texture_compression_etc2: features.texture_compression_etc2 as ash::vk::Bool32,
            texture_compression_astc_ldr: features.texture_compression_astc_ldr as ash::vk::Bool32,
            texture_compression_bc: features.texture_compression_bc as ash::vk::Bool32,
            occlusion_query_precise: features.occlusion_query_precise as ash::vk::Bool32,
            pipeline_statistics_query: features.pipeline_statistics_query as ash::vk::Bool32,
            vertex_pipeline_stores_and_atomics: features.vertex_pipeline_stores_and_atomics
                as ash::vk::Bool32,
            fragment_stores_and_atomics: features.fragment_stores_and_atomics as ash::vk::Bool32,
            shader_tessellation_and_geometry_point_size: features
                .shader_tessellation_and_geometry_point_size
                as ash::vk::Bool32,
            shader_image_gather_extended: features.shader_image_gather_extended as ash::vk::Bool32,
            shader_storage_image_extended_formats: features.shader_storage_image_extended_formats
                as ash::vk::Bool32,
            shader_storage_image_multisample: features.shader_storage_image_multisample
                as ash::vk::Bool32,
            shader_storage_image_read_without_format: features
                .shader_storage_image_read_without_format
                as ash::vk::Bool32,
            shader_storage_image_write_without_format: features
                .shader_storage_image_write_without_format
                as ash::vk::Bool32,
            shader_uniform_buffer_array_dynamic_indexing: features
                .shader_uniform_buffer_array_dynamic_indexing
                as ash::vk::Bool32,
            shader_sampled_image_array_dynamic_indexing: features
                .shader_sampled_image_array_dynamic_indexing
                as ash::vk::Bool32,
            shader_storage_buffer_array_dynamic_indexing: features
                .shader_storage_buffer_array_dynamic_indexing
                as ash::vk::Bool32,
            shader_storage_image_array_dynamic_indexing: features
                .shader_storage_image_array_dynamic_indexing
                as ash::vk::Bool32,
            shader_clip_distance: features.shader_clip_distance as ash::vk::Bool32,
            shader_cull_distance: features.shader_cull_distance as ash::vk::Bool32,
            shader_float64: features.shader_float64 as ash::vk::Bool32,
            shader_int64: features.shader_int64 as ash::vk::Bool32,
            shader_int16: features.shader_int16 as ash::vk::Bool32,
            shader_resource_residency: features.shader_resource_residency as ash::vk::Bool32,
            shader_resource_min_lod: features.shader_resource_min_lod as ash::vk::Bool32,
            sparse_binding: features.sparse_binding as ash::vk::Bool32,
            sparse_residency_buffer: features.sparse_residency_buffer as ash::vk::Bool32,
            sparse_residency_image2_d: features.sparse_residency_image2d as ash::vk::Bool32,
            sparse_residency_image3_d: features.sparse_residency_image3d as ash::vk::Bool32,
            sparse_residency2_samples: features.sparse_residency_2_samples as ash::vk::Bool32,
            sparse_residency4_samples: features.sparse_residency_4_samples as ash::vk::Bool32,
            sparse_residency8_samples: features.sparse_residency_8_samples as ash::vk::Bool32,
            sparse_residency16_samples: features.sparse_residency_16_samples as ash::vk::Bool32,
            sparse_residency_aliased: features.sparse_residency_aliased as ash::vk::Bool32,
            variable_multisample_rate: features.variable_multisample_rate as ash::vk::Bool32,
            inherited_queries: features.inherited_queries as ash::vk::Bool32,
        }
    }
}

pub(crate) struct PhysicalDeviceFeatures2 {
    pub dynamic_render: ash::vk::PhysicalDeviceDynamicRenderingFeatures,
    pub synchronization2: ash::vk::PhysicalDeviceSynchronization2Features,
    pub features: ash::vk::PhysicalDeviceFeatures,
}

impl PhysicalDeviceFeatures2 {
    pub(crate) fn new() -> Self {
        Self {
            dynamic_render: Default::default(),
            synchronization2: Default::default(),
            features: Default::default(),
        }
    }

    pub(crate) fn as_ffi(&mut self) -> ash::vk::PhysicalDeviceFeatures2 {
        ash::vk::PhysicalDeviceFeatures2::builder()
            .features(self.features)
            .push_next(&mut self.synchronization2)
            .push_next(&mut self.dynamic_render)
            .build()
    }
}

impl From<PhysicalDeviceFeatures2> for PhysicalDeviceFeatures {
    fn from(features2: PhysicalDeviceFeatures2) -> Self {
        Self {
            dynamic_rendering: features2.dynamic_render.dynamic_rendering == ash::vk::TRUE,
            synchronization2: features2.synchronization2.synchronization2 == ash::vk::TRUE,
            ..features2.features.into()
        }
    }
}

impl From<PhysicalDeviceFeatures> for PhysicalDeviceFeatures2 {
    fn from(features: PhysicalDeviceFeatures) -> Self {
        Self {
            synchronization2: ash::vk::PhysicalDeviceSynchronization2Features::builder()
                .synchronization2(features.synchronization2)
                .build(),
            dynamic_render: ash::vk::PhysicalDeviceDynamicRenderingFeatures::builder()
                .dynamic_rendering(features.dynamic_rendering)
                .build(),
            features: features.into(),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::physical_device::PhysicalDeviceFeatures;

    #[test]
    fn default() {
        assert_eq!(
            PhysicalDeviceFeatures {
                robust_buffer_access: false,
                full_draw_index_uint32: false,
                image_cube_array: false,
                independent_blend: false,
                geometry_shader: false,
                tessellation_shader: false,
                sample_rate_shading: false,
                dual_src_blend: false,
                logic_op: false,
                multi_draw_indirect: false,
                draw_indirect_first_instance: false,
                depth_clamp: false,
                depth_bias_clamp: false,
                fill_mode_non_solid: false,
                depth_bounds: false,
                wide_lines: false,
                large_points: false,
                alpha_to_one: false,
                multi_viewport: false,
                sampler_anisotropy: false,
                texture_compression_etc2: false,
                texture_compression_astc_ldr: false,
                texture_compression_bc: false,
                occlusion_query_precise: false,
                pipeline_statistics_query: false,
                vertex_pipeline_stores_and_atomics: false,
                fragment_stores_and_atomics: false,
                shader_tessellation_and_geometry_point_size: false,
                shader_image_gather_extended: false,
                shader_storage_image_extended_formats: false,
                shader_storage_image_multisample: false,
                shader_storage_image_read_without_format: false,
                shader_storage_image_write_without_format: false,
                shader_uniform_buffer_array_dynamic_indexing: false,
                shader_sampled_image_array_dynamic_indexing: false,
                shader_storage_buffer_array_dynamic_indexing: false,
                shader_storage_image_array_dynamic_indexing: false,
                shader_clip_distance: false,
                shader_cull_distance: false,
                shader_float64: false,
                shader_int64: false,
                shader_int16: false,
                shader_resource_residency: false,
                shader_resource_min_lod: false,
                sparse_binding: false,
                sparse_residency_buffer: false,
                sparse_residency_image2d: false,
                sparse_residency_image3d: false,
                sparse_residency_2_samples: false,
                sparse_residency_4_samples: false,
                sparse_residency_8_samples: false,
                sparse_residency_16_samples: false,
                sparse_residency_aliased: false,
                variable_multisample_rate: false,
                inherited_queries: false,
                dynamic_rendering: false,
                synchronization2: false,
            },
            Default::default()
        );
    }

    #[test]
    fn none() {
        assert_eq!(
            PhysicalDeviceFeatures::default(),
            PhysicalDeviceFeatures::none()
        );
    }

    #[test]
    fn is_not_a_subset() {
        let a = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            depth_clamp: true,
            ..Default::default()
        };
        let b = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            ..Default::default()
        };
        assert!(!a.is_subset(&b));
    }

    #[test]
    fn is_subset() {
        let a = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            depth_clamp: true,
            ..Default::default()
        };
        let b = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            ..Default::default()
        };
        assert!(b.is_subset(&a));
    }

    #[test]
    fn is_not_a_superset() {
        let a = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            depth_clamp: true,
            ..Default::default()
        };
        let b = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            ..Default::default()
        };
        assert!(!b.is_superset(&a));
    }

    #[test]
    fn is_superset() {
        let a = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            depth_clamp: true,
            ..Default::default()
        };
        let b = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            ..Default::default()
        };
        assert!(a.is_superset(&b));
    }

    #[test]
    fn difference() {
        let a = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            depth_clamp: true,
            ..Default::default()
        };
        let b = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            ..Default::default()
        };
        assert_eq!(
            PhysicalDeviceFeatures {
                depth_clamp: true,
                ..Default::default()
            },
            a.difference(&b)
        );
    }

    #[test]
    fn intersection() {
        let a = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            depth_clamp: true,
            ..Default::default()
        };
        let b = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            ..Default::default()
        };
        assert_eq!(
            PhysicalDeviceFeatures {
                sample_rate_shading: true,
                large_points: true,
                ..Default::default()
            },
            a.intersection(&b)
        );
    }

    #[test]
    fn union() {
        let a = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            depth_clamp: true,
            ..Default::default()
        };
        let b = PhysicalDeviceFeatures {
            sample_rate_shading: true,
            large_points: true,
            geometry_shader: true,
            ..Default::default()
        };
        assert_eq!(
            PhysicalDeviceFeatures {
                sample_rate_shading: true,
                large_points: true,
                depth_clamp: true,
                geometry_shader: true,
                ..Default::default()
            },
            a.union(&b)
        );
    }
}
