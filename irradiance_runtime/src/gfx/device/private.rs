use std::fmt::{Debug, Formatter};
use std::sync::Arc;

use crate::gfx::memory::Allocator;
use crate::gfx::physical_device::{
    DeviceExtensions, PhysicalDevice, PhysicalDeviceFeatures, PhysicalDeviceFeatures2,
};
use crate::gfx::queue::Queue;
use crate::gfx::{GfxError, Handle};

/// A logical device.
pub struct Device {
    allocator: Arc<Allocator>,
    physical_device: Arc<PhysicalDevice>,
    inner: InnerDevice,
}

impl Device {
    /// Start building a logical device from a [`PhysicalDevice`].
    ///
    /// `queues` contains a pair of a queue family index and a list of queue priorities for that
    /// family. For each such pair, queues will be created from the specified family and as many as
    /// priorities are given.
    pub fn builder(
        physical_device: Arc<PhysicalDevice>,
        queues: Vec<(u32, Vec<f32>)>,
    ) -> DeviceBuilder {
        DeviceBuilder {
            physical_device,
            queues,
            extensions: None,
            features: None,
        }
    }

    #[doc(hidden)]
    pub fn inner(&self) -> &ash::Device {
        &self.inner.0
    }

    /// Returns the [`PhysicalDevice`] this `Device` has been created from.
    pub fn physical_device(&self) -> &Arc<PhysicalDevice> {
        &self.physical_device
    }

    /// Returns the [`Allocator`].
    pub fn allocator(&self) -> &Arc<Allocator> {
        &self.allocator
    }

    /// Waits for the device to become idle.
    pub fn wait(&self) -> Result<(), GfxError> {
        unsafe { self.inner().device_wait_idle()? }

        Ok(())
    }

    /// Sets a debug utils object name.
    #[allow(unused)]
    pub fn set_debug_utils_object_name(&self, object: &impl Handle, name: impl AsRef<str>) {
        #[cfg(all(debug_assertions, not(test), not(doctest)))]
        {
            let name = name.as_ref();
            let name_ffi = std::ffi::CString::new(name).expect("c string");
            let info = ash::vk::DebugUtilsObjectNameInfoEXT::builder()
                .object_type(object.object_type().into())
                .object_handle(object.as_raw())
                .object_name(&name_ffi)
                .build();
            unsafe {
                self.physical_device
                    .instance()
                    .debug_utils()
                    .debug_utils_set_object_name(self.inner().handle(), &info)
                    .expect("set object name")
            }
        }
    }
}

impl Debug for Device {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Device")
            .field("handle", &self.inner().handle())
            .finish()
    }
}

struct InnerDevice(ash::Device);

impl Drop for InnerDevice {
    fn drop(&mut self) {
        unsafe {
            self.0.destroy_device(None);
        }
    }
}

/// Type for building a [`Device`].
#[derive(Debug)]
pub struct DeviceBuilder {
    features: Option<PhysicalDeviceFeatures>,
    extensions: Option<DeviceExtensions>,
    queues: Vec<(u32, Vec<f32>)>,
    physical_device: Arc<PhysicalDevice>,
}

impl DeviceBuilder {
    /// Sets the [`DeviceExtensions`] that will be enabled.
    pub fn extensions(mut self, extensions: DeviceExtensions) -> Self {
        self.extensions = Some(extensions);
        self
    }

    /// Sets the [`PhysicalDeviceFeatures`] that will be enabled.
    pub fn features(mut self, features: PhysicalDeviceFeatures) -> Self {
        self.features = Some(features);
        self
    }

    /// Builds the [`Device`].
    ///
    /// # Errors
    /// This function returns an error if the instance creation fails.
    pub fn build(self) -> Result<(Arc<Device>, Vec<Arc<Queue>>), GfxError> {
        let queues = Queues(&self.queues);
        let queue_create_infos: Vec<_> = queues.into();
        let extensions: Vec<_> = self
            .extensions
            .unwrap_or_default()
            .union(&DeviceExtensions {
                khr_synchronization2: true,
                khr_dynamic_rendering: true,
                ..Default::default()
            })
            .into();
        let extensions_ptr: Vec<_> = extensions
            .iter()
            .map(|extension_name| extension_name.as_ptr())
            .collect();
        let mut features2 = PhysicalDeviceFeatures2::from(self.features.unwrap_or_default().union(
            &PhysicalDeviceFeatures {
                synchronization2: true,
                dynamic_rendering: true,
                ..Default::default()
            },
        ));
        let mut features = features2.as_ffi();
        let create_info = ash::vk::DeviceCreateInfo::builder()
            .queue_create_infos(&queue_create_infos)
            .enabled_extension_names(&extensions_ptr)
            .push_next(&mut features)
            .build();
        let inner = unsafe {
            self.physical_device.instance().inner().create_device(
                self.physical_device.handle(),
                &create_info,
                None,
            )
        }?;

        let device = Arc::new(Device {
            allocator: Allocator::new(
                self.physical_device.instance().inner().clone(),
                self.physical_device.handle(),
                inner.clone(),
            )?,
            physical_device: self.physical_device.clone(),
            inner: InnerDevice(inner),
        });
        let queues = queues.get(device.clone());

        Ok((device, queues))
    }
}

#[derive(Copy, Clone)]
struct Queues<'a>(&'a Vec<(u32, Vec<f32>)>);

impl<'a> Queues<'a> {
    fn get(&self, device: Arc<Device>) -> Vec<Arc<Queue>> {
        self.0
            .iter()
            .flat_map(|(family_index, priorities)| {
                (0u32..priorities.len() as u32)
                    .into_iter()
                    .map(|queue_index| {
                        Queue::new(
                            device.clone(),
                            unsafe { device.inner().get_device_queue(*family_index, queue_index) },
                            *family_index,
                        )
                    })
                    .collect::<Vec<_>>()
            })
            .collect()
    }
}

impl<'a> From<Queues<'a>> for Vec<ash::vk::DeviceQueueCreateInfo> {
    fn from(queues: Queues) -> Self {
        queues
            .0
            .iter()
            .map(|(family_index, priorities)| {
                ash::vk::DeviceQueueCreateInfo::builder()
                    .queue_family_index(*family_index)
                    .queue_priorities(priorities)
                    .build()
            })
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::device::Device;
    use crate::gfx::physical_device::{DeviceExtensions, PhysicalDeviceFeatures};
    use crate::gfx::GfxError;
    use crate::instance;

    #[test]
    fn build_device_err() {
        let instance = instance!();
        let physical_device = instance
            .physical_devices()
            .unwrap()
            .iter()
            .next()
            .cloned()
            .unwrap();
        let result = Device::builder(physical_device, vec![(0, vec![1.0])])
            .extensions(DeviceExtensions {
                android_external_memory_android_hardware_buffer: true,
                nv_acquire_winrt_display: true,
                amd_buffer_marker: true,
                google_user_type: true,
                ..Default::default()
            })
            .build();

        assert!(result.is_err());
        assert!(matches!(result.err(), Some(GfxError::ExtensionNotPresent)));
    }

    #[test]
    fn build_device() {
        let instance = instance!();
        let physical_device = instance
            .physical_devices()
            .unwrap()
            .iter()
            .next()
            .cloned()
            .unwrap();
        let result = Device::builder(physical_device, vec![(0, vec![1.0])])
            .extensions(DeviceExtensions {
                khr_maintenance1: true,
                ..Default::default()
            })
            .features(PhysicalDeviceFeatures {
                robust_buffer_access: true,
                ..Default::default()
            })
            .build();

        assert!(matches!(result, Ok((_, queues)) if queues.len() == 1))
    }
}
