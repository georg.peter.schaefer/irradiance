//! Logical device.
//!
//! A logical device is a connection to a [physical device](super::physical_device::PhysicalDevice).
//!
//! # Creating a `Device`
//! To create a [`Device`], first you have to initialize the graphics API by creating an
//! [`Instance`](super::instance::Instance) and selecting a suitable
//! [physical device](super::physical_device::PhysicalDevice).
//!
//! When creating a [`Device`] you have to specify how many [queues](super::queue::Queue)s of which
//! queue family you want to create. You can query the properties of available queue families with
//! [queue_family_properties](super::physical_device::PhysicalDevice::queue_family_properties).
//!
//! You can enable [extensions](super::physical_device::DeviceExtensions) and
//! [features](super::physical_device::PhysicalDeviceFeatures) during device creation. These have to
//! be supported by the selected [physical device](super::physical_device::PhysicalDevice).
//!
//! ```
//! # use irradiance_runtime::gfx::instance::{Instance, InstanceExtensions};
//! # use irradiance_runtime::gfx::device::Device;
//! #
//! # let instance = Instance::new(Default::default())
//! #     .extensions(InstanceExtensions {
//! #         ext_debug_utils: true,
//! #         ..Default::default()
//! #     })
//! #     .build()
//! #     .unwrap();
//! # let physical_device = instance.physical_devices()
//! #     .unwrap()
//! #     .iter()
//! #     .next()
//! #     .cloned()
//! #     .unwrap();
//! #
//! let queue_family_index = physical_device.queue_family_properties()
//!     .into_iter()
//!     .enumerate()
//!     .filter(|(_, properties)| properties.queue_flags.graphics)
//!     .map(|(family_index, _)| family_index)
//!     .next()
//!     .unwrap() as _;
//! let (device, queues) = Device::new(
//!     physical_device.clone(),
//!     vec![(queue_family_index, vec![1.0])]
//! )
//! .extensions(physical_device.extensions().unwrap())
//! .features(physical_device.features())
//! .build()
//! .unwrap();
//! ```

pub use owned::DeviceOwned;
pub use private::Device;
pub use private::DeviceBuilder;

mod owned;
mod private;
