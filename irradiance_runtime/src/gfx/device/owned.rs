use std::sync::Arc;

use crate::gfx::device::Device;
use crate::gfx::Handle;

/// A type owned by a [`Device`].
pub trait DeviceOwned: Handle {
    /// Returns the owning [`Device`].
    fn device(&self) -> &Arc<Device>;

    /// Sets a debug utils object name.
    fn set_debug_utils_object_name(&self, name: impl AsRef<str>)
    where
        Self: Sized,
    {
        self.device().set_debug_utils_object_name(self, name);
    }
}
