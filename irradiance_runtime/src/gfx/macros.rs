#[doc(hidden)]
#[macro_export]
macro_rules! impl_flag_set {
    ($set:ident, doc = $documentation:literal, $(($flag:ident, $field:ident)),+) => {
        #[allow(unused)]
        #[derive(Copy, Clone, Eq, PartialEq, Default)]
        #[doc = $documentation]
        pub struct $set {
            $(
                #[allow(missing_docs)]
                pub $field: bool
            ),+
        }

        impl $set {
            /// Returns all fields set to `false`.
            pub const fn none() -> Self {
                Self {
                    $(
                        $field: false
                    ),+
                }
            }

            /// Returns `true` if `self` is a subset of `other`.
            pub fn is_subset(&self, other: &Self) -> bool {
                self.intersection(other).eq(self)
            }

            /// Returns `true` if `self` is a superset of `other`.
            pub fn is_superset(&self, other: &Self) -> bool {
                other.is_subset(self)
            }

            /// Returns the difference between `self` and `other`.
            pub const fn difference(&self, other: &Self) -> Self {
                Self {
                    $($field: self.$field && !other.$field),+
                }
            }

            /// Returns the intersection between `self` and `other`.
            pub const fn intersection(&self, other: &Self) -> Self {
                Self {
                    $($field: self.$field && other.$field),+
                }
            }

            /// Returns the union between `self` and `other`.
            pub const fn union(&self, other: &Self) -> Self {
                Self {
                    $($field: self.$field || other.$field),+
                }
            }
        }

        impl std::fmt::Debug for $set {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                let mut names = Vec::new();
                $(
                    if self.$field {
                        names.push(stringify!($flag));
                    }
                )+
                let names = names.join(", ");
                write!(f, "[{}]", names)
            }
        }

        impl std::fmt::Display for $set {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                std::fmt::Debug::fmt(self, f)
            }
        }
    };
}

#[doc(hidden)]
#[macro_export]
macro_rules! impl_extensions {
    ($extension_type:ident, doc = $documentation:literal, $(($extension:ident, $field:ident)),+) => {
        $crate::impl_flag_set!{$extension_type, doc = $documentation, $(($extension, $field)),+}

        #[doc(hidden)]
        impl From<$extension_type> for Vec<std::ffi::CString> {
            fn from(extensions: $extension_type) -> Self {
                let mut extension_names = Vec::new();
                $(
                    if extensions.$field {
                        extension_names.push(std::ffi::CString::new(stringify!($extension)).unwrap());
                    }
                )+
                extension_names
            }
        }

        #[doc(hidden)]
        impl<'a, I> From<I> for $extension_type
        where
            I: IntoIterator<Item = &'a std::ffi::CStr>,
        {
            fn from(extension_names: I) -> Self {
                let mut extensions = Self::default();
                for extension_name in extension_names {
                    $(
                        if extension_name.to_bytes() == stringify!($extension).as_bytes() {
                            extensions.$field = true;
                        }
                    )+
                }
                extensions
            }
        }
    };
}
