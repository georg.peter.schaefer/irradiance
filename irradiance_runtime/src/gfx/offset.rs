/// A two-dimensional offset
#[derive(Copy, Clone, Eq, PartialEq, Debug, Default)]
pub struct Offset2D {
    /// The x offset.
    pub x: i32,
    /// The y offset.
    pub y: i32,
}

#[doc(hidden)]
impl From<Offset2D> for ash::vk::Offset2D {
    fn from(offset: Offset2D) -> Self {
        ash::vk::Offset2D::builder().x(offset.x).y(offset.y).build()
    }
}

/// A three-dimensional offset
#[derive(Copy, Clone, Eq, PartialEq, Debug, Default)]
pub struct Offset3D {
    /// The x offset.
    pub x: i32,
    /// The y offset.
    pub y: i32,
    /// The z offset.
    pub z: i32,
}

#[doc(hidden)]
impl From<Offset3D> for ash::vk::Offset3D {
    fn from(offset: Offset3D) -> Self {
        ash::vk::Offset3D::builder()
            .x(offset.x)
            .y(offset.y)
            .z(offset.z)
            .build()
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::Offset2D;
    use crate::gfx::Offset3D;

    #[test]
    fn to_offset2d() {
        assert_eq!(
            ash::vk::Offset2D::builder().x(19).y(42).build(),
            ash::vk::Offset2D::from(Offset2D { x: 19, y: 42 })
        )
    }

    #[test]
    fn to_offset3d() {
        assert_eq!(
            ash::vk::Offset3D::builder().x(19).y(42).z(3).build(),
            ash::vk::Offset3D::from(Offset3D { x: 19, y: 42, z: 3 })
        )
    }
}
