use crate::gfx::image::ImageSubresourceLayers;
use crate::gfx::Offset3D;

/// Image blit region.
#[derive(Copy, Clone, Debug)]
pub struct ImageBlit {
    /// The subresource to blit from.
    pub src_subresource: ImageSubresourceLayers,
    /// The bounds of the source region within the source subresource.
    pub src_offsets: [Offset3D; 2],
    /// The subresource to blit to.
    pub dst_subresource: ImageSubresourceLayers,
    /// The bounds of the source region within the destination subresource.
    pub dst_offsets: [Offset3D; 2],
}

impl From<ImageBlit> for ash::vk::ImageBlit {
    fn from(image_blit: ImageBlit) -> Self {
        ash::vk::ImageBlit::builder()
            .src_subresource(image_blit.src_subresource.into())
            .src_offsets([
                image_blit.src_offsets[0].into(),
                image_blit.src_offsets[1].into(),
            ])
            .dst_subresource(image_blit.dst_subresource.into())
            .dst_offsets([
                image_blit.dst_offsets[0].into(),
                image_blit.dst_offsets[1].into(),
            ])
            .build()
    }
}
