use std::sync::{Arc, LockResult, Mutex, MutexGuard};

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::sync::ExternallySynchronized;
use crate::gfx::{GfxError, Handle, ObjectType};

/// Command pools are opaque objects that command buffer memory is allocated from, and which allow
/// the implementation to amortize the cost of resource creation across multiple command buffers.
#[derive(Debug)]
pub struct CommandPool {
    handle: ash::vk::CommandPool,
    mutex: Mutex<()>,
    device: Arc<Device>,
}

impl CommandPool {
    /// Starts building a new command pool.
    pub fn builder(device: Arc<Device>) -> CommandPoolBuilder {
        CommandPoolBuilder {
            queue_family_index: Default::default(),
            device,
        }
    }
}

impl Drop for CommandPool {
    fn drop(&mut self) {
        unsafe {
            self.device.inner().destroy_command_pool(self.handle, None);
        }
    }
}

#[doc(hidden)]
impl Handle for CommandPool {
    type Type = ash::vk::CommandPool;

    fn object_type(&self) -> ObjectType {
        ObjectType::CommandPool
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl ExternallySynchronized for CommandPool {
    fn lock(&self) -> LockResult<MutexGuard<()>> {
        self.mutex.lock()
    }
}

impl DeviceOwned for CommandPool {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for creating a [`CommandPool`].
#[derive(Debug)]
pub struct CommandPoolBuilder {
    queue_family_index: u32,
    device: Arc<Device>,
}

impl CommandPoolBuilder {
    /// Sets the queue family index.
    pub fn queue_family_index(&mut self, queue_family_index: u32) -> &mut Self {
        self.queue_family_index = queue_family_index;
        self
    }

    /// Builds the [`CommandPool`].
    ///
    /// # Errors
    /// This function returns an error if the command pool creation fails.
    pub fn build(&mut self) -> Result<Arc<CommandPool>, GfxError> {
        let create_info = ash::vk::CommandPoolCreateInfo::builder()
            .flags(ash::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .queue_family_index(self.queue_family_index)
            .build();

        let handle = unsafe { self.device.inner().create_command_pool(&create_info, None) }?;

        Ok(Arc::new(CommandPool {
            handle,
            mutex: Mutex::new(()),
            device: self.device.clone(),
        }))
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::command_buffer::CommandPool;
    use crate::gfx::device::Device;
    use crate::physical_device;

    #[test]
    fn build_command_pool() {
        let physical_device = physical_device!();
        let (device, queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let queue = &queues[0];

        let result = CommandPool::builder(device.clone())
            .queue_family_index(queue.family_index())
            .build();

        assert!(result.is_ok());
    }
}
