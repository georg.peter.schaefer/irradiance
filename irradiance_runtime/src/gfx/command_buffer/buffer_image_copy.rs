use crate::gfx::image::ImageSubresourceLayers;
use crate::gfx::{Extent3D, Offset3D};

/// Buffer image copy operation.
#[derive(Copy, Clone, Debug)]
pub struct BufferImageCopy {
    image_extent: Extent3D,
    image_offset: Offset3D,
    image_subresource: ImageSubresourceLayers,
    buffer_image_height: u32,
    buffer_row_length: u32,
    buffer_offset: u64,
}

impl BufferImageCopy {
    /// Starts building a new buffer image copy.
    pub fn builder() -> BufferImageCopyBuilder {
        BufferImageCopyBuilder {
            image_extent: Default::default(),
            image_offset: Default::default(),
            image_subresource: ImageSubresourceLayers::builder().build(),
            buffer_image_height: 0,
            buffer_row_length: 0,
            buffer_offset: 0,
        }
    }
}

#[doc(hidden)]
impl From<BufferImageCopy> for ash::vk::BufferImageCopy {
    fn from(buffer_image_copy: BufferImageCopy) -> Self {
        ash::vk::BufferImageCopy::builder()
            .buffer_offset(buffer_image_copy.buffer_offset)
            .buffer_row_length(buffer_image_copy.buffer_row_length)
            .buffer_image_height(buffer_image_copy.buffer_image_height)
            .image_subresource(buffer_image_copy.image_subresource.into())
            .image_offset(buffer_image_copy.image_offset.into())
            .image_extent(buffer_image_copy.image_extent.into())
            .build()
    }
}

/// Type for building a [`BufferImageCopy`].
#[derive(Copy, Clone, Debug)]
pub struct BufferImageCopyBuilder {
    image_extent: Extent3D,
    image_offset: Offset3D,
    image_subresource: ImageSubresourceLayers,
    buffer_image_height: u32,
    buffer_row_length: u32,
    buffer_offset: u64,
}

impl BufferImageCopyBuilder {
    /// Sets the buffer offset.
    pub fn buffer_offset(&mut self, buffer_offset: u64) -> &mut Self {
        self.buffer_offset = buffer_offset;
        self
    }

    /// Sets the buffer row length.
    pub fn buffer_row_length(&mut self, buffer_row_length: u32) -> &mut Self {
        self.buffer_row_length = buffer_row_length;
        self
    }

    /// Sets the buffer image height.
    pub fn buffer_image_height(&mut self, buffer_image_height: u32) -> &mut Self {
        self.buffer_image_height = buffer_image_height;
        self
    }

    /// Sets the image subresource.
    pub fn image_subresource(&mut self, image_subresource: ImageSubresourceLayers) -> &mut Self {
        self.image_subresource = image_subresource;
        self
    }

    /// Sets the image offset.
    pub fn image_offset(&mut self, image_offset: Offset3D) -> &mut Self {
        self.image_offset = image_offset;
        self
    }

    /// Sets the image extent.
    pub fn image_extent(&mut self, image_extent: Extent3D) -> &mut Self {
        self.image_extent = image_extent;
        self
    }

    /// Builds the [`BufferImageCopy`].
    pub fn build(&mut self) -> BufferImageCopy {
        BufferImageCopy {
            image_extent: self.image_extent,
            image_offset: self.image_offset,
            image_subresource: self.image_subresource,
            buffer_image_height: self.buffer_image_height,
            buffer_row_length: self.buffer_row_length,
            buffer_offset: self.buffer_offset,
        }
    }
}
