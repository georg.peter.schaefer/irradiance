use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::thread::ThreadId;

use crate::gfx::command_buffer::CommandPool;
use crate::gfx::device::Device;
use crate::gfx::GfxError;

/// Per thread command pool.
#[derive(Debug)]
pub struct PerThreadCommandPool {
    command_pools: Mutex<HashMap<ThreadId, Arc<CommandPool>>>,
    queue_family_index: u32,
    device: Arc<Device>,
}

impl PerThreadCommandPool {
    /// Creates a new per thread command pool.
    pub fn new(device: Arc<Device>, queue_family_index: u32) -> Self {
        Self {
            command_pools: Mutex::new(Default::default()),
            queue_family_index,
            device,
        }
    }

    /// Gets the [`CommandPool`] for the current thread.
    pub fn get(&self) -> Result<Arc<CommandPool>, GfxError> {
        let mut command_pools = self.command_pools.lock().unwrap();
        if let Some(command_pool) = command_pools.get(&std::thread::current().id()) {
            Ok(command_pool.clone())
        } else {
            let command_pool = CommandPool::builder(self.device.clone())
                .queue_family_index(self.queue_family_index)
                .build()?;
            command_pools.insert(std::thread::current().id(), command_pool.clone());
            Ok(command_pool)
        }
    }
}
