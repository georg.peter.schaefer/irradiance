/// Type of index buffer indices.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum IndexType {
    /// Indices are 16-bit unsigned integer values.
    UInt16,
    /// Indices are 32-bit unsigned integer values.
    UInt32,
    /// No indices are provided.
    None,
    /// Indices are 8-bit unsigned integer values.
    UInt8,
}

#[doc(hidden)]
impl From<IndexType> for ash::vk::IndexType {
    fn from(index_type: IndexType) -> Self {
        match index_type {
            IndexType::UInt16 => ash::vk::IndexType::UINT16,
            IndexType::UInt32 => ash::vk::IndexType::UINT32,
            IndexType::None => ash::vk::IndexType::NONE_KHR,
            IndexType::UInt8 => ash::vk::IndexType::UINT8_EXT,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::command_buffer::IndexType;

    #[test]
    fn to_index_type() {
        assert_eq!(
            ash::vk::IndexType::UINT16,
            ash::vk::IndexType::from(IndexType::UInt16)
        );
        assert_eq!(
            ash::vk::IndexType::UINT32,
            ash::vk::IndexType::from(IndexType::UInt32)
        );
        assert_eq!(
            ash::vk::IndexType::NONE_KHR,
            ash::vk::IndexType::from(IndexType::None)
        );
        assert_eq!(
            ash::vk::IndexType::UINT8_EXT,
            ash::vk::IndexType::from(IndexType::UInt8)
        );
    }
}
