use std::ffi::CString;
use std::marker::PhantomData;
use std::sync::Arc;

use irradiance_runtime::gfx::command_buffer::BufferImageCopy;
use irradiance_runtime::gfx::pipeline::ComputePipeline;
use irradiance_runtime::gfx::sync::PipelineBarrier;

use crate::gfx::buffer::Buffer;
use crate::gfx::command_buffer::{BufferCopy, CommandPool, ImageBlit, IndexType};
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{Filter, Image, ImageLayout};
use crate::gfx::pipeline::{
    DescriptorSet, GraphicsPipeline, PipelineBindPoint, PipelineLayout, ShaderStages, Viewport,
};
use crate::gfx::render_pass::{Attachment, RenderPass};
use crate::gfx::sync::ExternallySynchronized;
use crate::gfx::{GfxError, Handle, ObjectType, Rect2D};
use crate::math::Vec4;

/// Command buffers are objects used to record commands which can be subsequently submitted to a
/// device queue for execution.
#[derive(Debug)]
pub struct CommandBuffer {
    _commands: Vec<Command>,
    handle: ash::vk::CommandBuffer,
    command_pool: Arc<CommandPool>,
    device: Arc<Device>,
}

impl CommandBuffer {
    /// Starts building a new primary command buffer.
    pub fn primary(device: Arc<Device>, command_pool: Arc<CommandPool>) -> CommandBufferBuilder {
        Self::builder(device, command_pool, ash::vk::CommandBufferLevel::PRIMARY)
    }

    /// Starts building a new secondary command buffer.
    pub fn secondary(device: Arc<Device>, command_pool: Arc<CommandPool>) -> CommandBufferBuilder {
        Self::builder(device, command_pool, ash::vk::CommandBufferLevel::SECONDARY)
    }

    fn builder(
        device: Arc<Device>,
        command_pool: Arc<CommandPool>,
        level: ash::vk::CommandBufferLevel,
    ) -> CommandBufferBuilder {
        CommandBufferBuilder {
            commands: vec![],
            level,
            command_pool,
            device,
            _not_send_sync: Default::default(),
        }
    }
}

impl Drop for CommandBuffer {
    fn drop(&mut self) {
        let _guard = self.command_pool.lock().unwrap();
        unsafe {
            self.device
                .inner()
                .reset_command_buffer(self.handle, Default::default())
                .unwrap();
        }
    }
}

#[doc(hidden)]
impl Handle for CommandBuffer {
    type Type = ash::vk::CommandBuffer;

    fn object_type(&self) -> ObjectType {
        ObjectType::CommandBuffer
    }

    fn handle(&self) -> Self::Type {
        self.handle
    }
}

impl DeviceOwned for CommandBuffer {
    fn device(&self) -> &Arc<Device> {
        &self.device
    }
}

/// Type for recording a [`CommandBuffer`].
#[derive(Debug)]
pub struct CommandBufferBuilder {
    commands: Vec<Command>,
    level: ash::vk::CommandBufferLevel,
    command_pool: Arc<CommandPool>,
    device: Arc<Device>,
    _not_send_sync: PhantomData<*const ()>,
}

impl CommandBufferBuilder {
    /// Begins a [`RenderPass`].
    pub fn begin_rendering(&mut self, render_pass: RenderPass) -> &mut Self {
        self.commands
            .push(Command::BeginRendering(BeginRenderingCommand::new(
                render_pass,
            )));
        self
    }

    /// Copy regions of an image, potentially performing format conversion.
    pub fn blit_image(
        &mut self,
        src_image: Arc<Image>,
        src_image_layout: ImageLayout,
        dst_image: Arc<Image>,
        dst_image_layout: ImageLayout,
        regions: Vec<ImageBlit>,
        filter: Filter,
    ) -> &mut Self {
        self.commands.push(Command::BlitImage(BlitImageCommand::new(
            src_image,
            src_image_layout,
            dst_image,
            dst_image_layout,
            regions,
            filter,
        )));
        self
    }

    /// Binds descriptor sets.
    pub fn bind_descriptor_sets(
        &mut self,
        pipeline_bind_point: PipelineBindPoint,
        layout: Arc<PipelineLayout>,
        first_set: u32,
        descriptor_sets: Vec<Arc<DescriptorSet>>,
    ) -> &mut Self {
        self.commands
            .push(Command::BindDescriptorSets(BindDescriptorSetsCommand::new(
                pipeline_bind_point,
                layout,
                first_set,
                descriptor_sets,
            )));
        self
    }

    /// Binds a [`GraphicsPipeline`].
    pub fn bind_graphics_pipeline(
        &mut self,
        graphics_pipeline: Arc<GraphicsPipeline>,
    ) -> &mut Self {
        self.commands.push(Command::BindGraphicsPipeline(
            BindGraphicsPipelineCommand::new(graphics_pipeline),
        ));
        self
    }

    /// Binds a [`ComputePipeline`].
    pub fn bind_compute_pipeline(&mut self, compute_pipeline: Arc<ComputePipeline>) -> &mut Self {
        self.commands.push(Command::BindComputePipeline(
            BindComputePipelineCommand::new(compute_pipeline),
        ));
        self
    }

    /// Binds a [`Buffer`] as index buffer.
    pub fn bind_index_buffer(
        &mut self,
        index_buffer: Arc<Buffer>,
        offset: u64,
        index_type: IndexType,
    ) -> &mut Self {
        self.commands
            .push(Command::BindIndexBuffer(BindIndexBufferCommand::new(
                index_buffer,
                offset,
                index_type,
            )));
        self
    }

    /// Binds a [`Buffer`] as vertex buffer.
    pub fn bind_vertex_buffer(&mut self, vertex_buffer: Arc<Buffer>) -> &mut Self {
        self.commands
            .push(Command::BindVertexBuffer(BindVertexBufferCommand::new(
                vertex_buffer,
            )));
        self
    }

    /// Copy data between buffer regions.
    pub fn copy_buffer(
        &mut self,
        src_buffer: Arc<Buffer>,
        dst_buffer: Arc<Buffer>,
        regions: Vec<BufferCopy>,
    ) -> &mut Self {
        self.commands
            .push(Command::CopyBuffer(CopyBufferCommand::new(
                src_buffer, dst_buffer, regions,
            )));
        self
    }

    /// Copy data between buffer and image regions.
    pub fn copy_buffer_to_image(
        &mut self,
        src_buffer: Arc<Buffer>,
        dst_image: Arc<Image>,
        dst_image_layout: ImageLayout,
        regions: Vec<BufferImageCopy>,
    ) -> &mut Self {
        self.commands
            .push(Command::CopyBufferToImage(CopyBufferToImageCommand::new(
                src_buffer,
                dst_image,
                dst_image_layout,
                regions,
            )));
        self
    }

    /// Dispatch the currently bound [`ComputePipeline`].
    pub fn dispatch(
        &mut self,
        group_count_x: u32,
        group_count_y: u32,
        group_count_z: u32,
    ) -> &mut Self {
        self.commands.push(Command::Dispatch(DispatchCommand::new(
            group_count_x,
            group_count_y,
            group_count_z,
        )));
        self
    }

    /// Draw primitives.
    pub fn draw(
        &mut self,
        vertex_count: u32,
        instance_count: u32,
        first_vertex: u32,
        first_instance: u32,
    ) -> &mut Self {
        self.commands.push(Command::Draw(DrawCommand::new(
            vertex_count,
            instance_count,
            first_vertex,
            first_instance,
        )));
        self
    }

    /// Draw primitives with indexed vertices.
    pub fn draw_indexed(
        &mut self,
        index_count: u32,
        instance_count: u32,
        first_index: u32,
        vertex_offset: i32,
        first_instance: u32,
    ) -> &mut Self {
        self.commands
            .push(Command::DrawIndexed(DrawIndexedCommand::new(
                index_count,
                instance_count,
                first_index,
                vertex_offset,
                first_instance,
            )));
        self
    }

    /// Ends the current [`RenderPass`].
    pub fn end_rendering(&mut self) -> &mut Self {
        self.commands.push(Command::EndRendering);
        self
    }

    /// Executes secondary command buffers.
    pub fn execute_commands(&mut self, command_buffers: Vec<Arc<CommandBuffer>>) -> &mut Self {
        self.commands
            .push(Command::ExecuteCommands(ExecuteCommandsCommand::new(
                command_buffers,
            )));
        self
    }

    /// Inserts a memory dependency.
    pub fn pipeline_barrier(&mut self, pipeline_barrier: PipelineBarrier) -> &mut Self {
        self.commands
            .push(Command::PipelineBarrier(PipelineBarrierCommand::new(
                pipeline_barrier,
            )));
        self
    }

    /// Pushes constants.
    pub fn push_constants<T>(
        &mut self,
        pipeline_layout: Arc<PipelineLayout>,
        stages: ShaderStages,
        offset: u32,
        values: T,
    ) -> &mut Self {
        unsafe {
            self.commands
                .push(Command::PushConstants(PushConstantsCommand::new(
                    pipeline_layout,
                    stages,
                    offset,
                    Vec::from(std::slice::from_raw_parts(
                        (&values as *const T) as _,
                        std::mem::size_of::<T>(),
                    )),
                )));
        }
        self
    }

    /// Updates scissor rectangles.
    pub fn set_scissor(&mut self, first_scissor: u32, scissors: Vec<Rect2D>) -> &mut Self {
        self.commands
            .push(Command::SetScissor(SetScissorCommand::new(
                first_scissor,
                scissors,
            )));
        self
    }

    /// Updates viewports.
    pub fn set_viewport(&mut self, first_viewport: u32, viewports: Vec<Viewport>) -> &mut Self {
        self.commands
            .push(Command::SetViewport(SetViewportCommand::new(
                first_viewport,
                viewports,
            )));
        self
    }

    /// Begins a debug label.
    pub fn begin_label(&mut self, label: &'static str, color: Vec4) -> &mut Self {
        self.commands
            .push(Command::BeginLabel(BeginLabelCommand::new(label, color)));
        self
    }

    /// Ends a debug label.
    pub fn end_label(&mut self) -> &mut Self {
        self.commands.push(Command::EndLabel);
        self
    }

    /// Builds the [`CommandBuffer`].
    ///
    /// # Errors
    /// This function returns an error if the command buffer creation fails.
    pub fn build(&mut self) -> Result<Arc<CommandBuffer>, GfxError> {
        let command_pool = self.command_pool.clone();
        let _guard = command_pool.lock().unwrap();

        let command_buffer = self.allocate()?;
        self.begin(&command_buffer)?;
        unsafe {
            self.record(&command_buffer);
        }
        self.end(&command_buffer)?;

        Ok(command_buffer)
    }

    fn allocate(&self) -> Result<Arc<CommandBuffer>, GfxError> {
        let allocate_info = ash::vk::CommandBufferAllocateInfo::builder()
            .command_pool(self.command_pool.handle())
            .level(self.level)
            .command_buffer_count(1)
            .build();

        let handle = unsafe { self.device.inner().allocate_command_buffers(&allocate_info) }?[0];

        Ok(Arc::new(CommandBuffer {
            _commands: self.commands.clone(),
            handle,
            command_pool: self.command_pool.clone(),
            device: self.device.clone(),
        }))
    }

    fn begin(&self, command_buffer: &CommandBuffer) -> Result<(), GfxError> {
        let begin_info = ash::vk::CommandBufferBeginInfo::builder()
            .flags(ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT)
            .build();

        unsafe {
            self.device
                .inner()
                .begin_command_buffer(command_buffer.handle(), &begin_info)?;
        }

        Ok(())
    }

    unsafe fn record(&mut self, command_buffer: &CommandBuffer) {
        let debug_utils = self.device.physical_device().instance().debug_utils();
        let device = self.device.inner();
        let command_buffer = command_buffer.handle();
        for command in &self.commands {
            match command {
                Command::BeginRendering(command) => {
                    device.cmd_begin_rendering(command_buffer, &command.rendering_info())
                }
                Command::BlitImage(command) => device.cmd_blit_image(
                    command_buffer,
                    command.src_image.handle(),
                    command.src_image_layout.into(),
                    command.dst_image.handle(),
                    command.dst_image_layout.into(),
                    &command.regions,
                    command.filter.into(),
                ),
                Command::BindComputePipeline(command) => device.cmd_bind_pipeline(
                    command_buffer,
                    ash::vk::PipelineBindPoint::COMPUTE,
                    command.compute_pipeline.handle(),
                ),
                Command::BindGraphicsPipeline(command) => device.cmd_bind_pipeline(
                    command_buffer,
                    ash::vk::PipelineBindPoint::GRAPHICS,
                    command.graphics_pipeline.handle(),
                ),
                Command::BindDescriptorSets(command) => device.cmd_bind_descriptor_sets(
                    command_buffer,
                    command.pipeline_bind_point.into(),
                    command.layout.handle(),
                    command.first_set,
                    &command.descriptor_set_handles,
                    &[],
                ),
                Command::BindIndexBuffer(command) => device.cmd_bind_index_buffer(
                    command_buffer,
                    command.index_buffer.handle(),
                    command.offset,
                    command.index_type.into(),
                ),
                Command::BindVertexBuffer(command) => device.cmd_bind_vertex_buffers(
                    command_buffer,
                    0,
                    &[command.vertex_buffer.handle()],
                    &[0],
                ),
                Command::CopyBuffer(command) => device.cmd_copy_buffer(
                    command_buffer,
                    command.src_buffer.handle(),
                    command.dst_buffer.handle(),
                    &command.regions,
                ),
                Command::CopyBufferToImage(command) => device.cmd_copy_buffer_to_image(
                    command_buffer,
                    command.src_buffer.handle(),
                    command.dst_image.handle(),
                    command.dst_image_layout.into(),
                    &command.regions,
                ),
                Command::Dispatch(command) => device.cmd_dispatch(
                    command_buffer,
                    command.group_count_x,
                    command.group_count_y,
                    command.group_count_z,
                ),
                Command::Draw(command) => device.cmd_draw(
                    command_buffer,
                    command.vertex_count,
                    command.instance_count,
                    command.first_vertex,
                    command.first_instance,
                ),
                Command::DrawIndexed(command) => device.cmd_draw_indexed(
                    command_buffer,
                    command.index_count,
                    command.instance_count,
                    command.first_index,
                    command.vertex_offset,
                    command.first_instance,
                ),
                Command::EndRendering => device.cmd_end_rendering(command_buffer),
                Command::ExecuteCommands(command) => {
                    device.cmd_execute_commands(command_buffer, &command.command_buffers_ash)
                }
                Command::PipelineBarrier(command) => {
                    let synchronization2 = ash::extensions::khr::Synchronization2::new(
                        self.device.physical_device().instance().inner(),
                        self.device.inner(),
                    );
                    let dependency_info = command.dependency_info();
                    synchronization2.cmd_pipeline_barrier2(command_buffer, &dependency_info)
                }
                Command::PushConstants(command) => device.cmd_push_constants(
                    command_buffer,
                    command.layout.handle(),
                    command.stages.into(),
                    command.offset,
                    &command.values,
                ),
                Command::SetScissor(command) => {
                    device.cmd_set_scissor(command_buffer, command.first_scissor, &command.scissors)
                }
                Command::SetViewport(command) => device.cmd_set_viewport(
                    command_buffer,
                    command.first_viewport,
                    &command.viewports,
                ),
                Command::BeginLabel(command) => {
                    debug_utils.cmd_begin_debug_utils_label(command_buffer, &command.label)
                }
                Command::EndLabel => debug_utils.cmd_end_debug_utils_label(command_buffer),
            }
        }
    }

    fn end(&self, command_buffer: &CommandBuffer) -> Result<(), GfxError> {
        unsafe {
            self.device
                .inner()
                .end_command_buffer(command_buffer.handle())?;
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
enum Command {
    BeginRendering(BeginRenderingCommand),
    BlitImage(BlitImageCommand),
    BindComputePipeline(BindComputePipelineCommand),
    BindGraphicsPipeline(BindGraphicsPipelineCommand),
    BindDescriptorSets(BindDescriptorSetsCommand),
    BindIndexBuffer(BindIndexBufferCommand),
    BindVertexBuffer(BindVertexBufferCommand),
    CopyBuffer(CopyBufferCommand),
    CopyBufferToImage(CopyBufferToImageCommand),
    Dispatch(DispatchCommand),
    Draw(DrawCommand),
    DrawIndexed(DrawIndexedCommand),
    EndRendering,
    ExecuteCommands(ExecuteCommandsCommand),
    PipelineBarrier(PipelineBarrierCommand),
    PushConstants(PushConstantsCommand),
    SetScissor(SetScissorCommand),
    SetViewport(SetViewportCommand),
    BeginLabel(BeginLabelCommand),
    EndLabel,
}

#[derive(Clone, Debug)]
struct BeginRenderingCommand {
    stencil_attachment: Option<ash::vk::RenderingAttachmentInfo>,
    depth_attachment: Option<ash::vk::RenderingAttachmentInfo>,
    color_attachments: Vec<ash::vk::RenderingAttachmentInfo>,
    render_pass: RenderPass,
}

impl BeginRenderingCommand {
    fn new(render_pass: RenderPass) -> Self {
        Self {
            stencil_attachment: render_pass
                .stencil_attachment
                .as_ref()
                .map(Attachment::as_ffi),
            depth_attachment: render_pass
                .depth_attachment
                .as_ref()
                .map(Attachment::as_ffi),
            color_attachments: render_pass
                .color_attachments
                .iter()
                .map(Attachment::as_ffi)
                .collect(),
            render_pass,
        }
    }

    fn rendering_info(&self) -> ash::vk::RenderingInfo {
        let mut builder = ash::vk::RenderingInfo::builder()
            .flags(self.render_pass.flags.into())
            .render_area(self.render_pass.render_area.into())
            .layer_count(self.render_pass.layer_count)
            .view_mask(self.render_pass.view_mask)
            .color_attachments(&self.color_attachments);
        if let Some(depth_attachment) = &self.depth_attachment {
            builder = builder.depth_attachment(depth_attachment);
        }
        if let Some(stencil_attachment) = &self.stencil_attachment {
            builder = builder.stencil_attachment(stencil_attachment);
        }
        builder.build()
    }
}

#[derive(Clone, Debug)]
struct BlitImageCommand {
    src_image: Arc<Image>,
    src_image_layout: ImageLayout,
    dst_image: Arc<Image>,
    dst_image_layout: ImageLayout,
    regions: Vec<ash::vk::ImageBlit>,
    filter: Filter,
}

impl BlitImageCommand {
    fn new(
        src_image: Arc<Image>,
        src_image_layout: ImageLayout,
        dst_image: Arc<Image>,
        dst_image_layout: ImageLayout,
        regions: Vec<ImageBlit>,
        filter: Filter,
    ) -> Self {
        Self {
            src_image,
            src_image_layout,
            dst_image,
            dst_image_layout,
            regions: regions.into_iter().map(ash::vk::ImageBlit::from).collect(),
            filter,
        }
    }
}

#[derive(Clone, Debug)]
struct BindDescriptorSetsCommand {
    pipeline_bind_point: PipelineBindPoint,
    layout: Arc<PipelineLayout>,
    first_set: u32,
    descriptor_set_handles: Vec<ash::vk::DescriptorSet>,
    _descriptor_sets: Vec<Arc<DescriptorSet>>,
}

impl BindDescriptorSetsCommand {
    fn new(
        pipeline_bind_point: PipelineBindPoint,
        layout: Arc<PipelineLayout>,
        first_set: u32,
        descriptor_sets: Vec<Arc<DescriptorSet>>,
    ) -> Self {
        Self {
            pipeline_bind_point,
            layout,
            first_set,
            descriptor_set_handles: descriptor_sets
                .iter()
                .map(|descriptor_set| descriptor_set.handle())
                .collect(),
            _descriptor_sets: descriptor_sets,
        }
    }
}

#[derive(Clone, Debug)]
struct BindGraphicsPipelineCommand {
    graphics_pipeline: Arc<GraphicsPipeline>,
}

impl BindGraphicsPipelineCommand {
    fn new(graphics_pipeline: Arc<GraphicsPipeline>) -> Self {
        Self { graphics_pipeline }
    }
}

#[derive(Clone, Debug)]
struct BindComputePipelineCommand {
    compute_pipeline: Arc<ComputePipeline>,
}

impl BindComputePipelineCommand {
    fn new(compute_pipeline: Arc<ComputePipeline>) -> Self {
        Self { compute_pipeline }
    }
}

#[derive(Clone, Debug)]
struct BindIndexBufferCommand {
    index_type: IndexType,
    offset: u64,
    index_buffer: Arc<Buffer>,
}

impl BindIndexBufferCommand {
    fn new(index_buffer: Arc<Buffer>, offset: u64, index_type: IndexType) -> Self {
        Self {
            index_type,
            offset,
            index_buffer,
        }
    }
}

#[derive(Clone, Debug)]
struct BindVertexBufferCommand {
    vertex_buffer: Arc<Buffer>,
}

impl BindVertexBufferCommand {
    fn new(vertex_buffer: Arc<Buffer>) -> Self {
        Self { vertex_buffer }
    }
}

#[derive(Clone, Debug)]
struct CopyBufferCommand {
    src_buffer: Arc<Buffer>,
    dst_buffer: Arc<Buffer>,
    regions: Vec<ash::vk::BufferCopy>,
}

impl CopyBufferCommand {
    fn new(src_buffer: Arc<Buffer>, dst_buffer: Arc<Buffer>, regions: Vec<BufferCopy>) -> Self {
        Self {
            src_buffer,
            dst_buffer,
            regions: regions.into_iter().map(ash::vk::BufferCopy::from).collect(),
        }
    }
}

#[derive(Clone, Debug)]
struct CopyBufferToImageCommand {
    src_buffer: Arc<Buffer>,
    dst_image: Arc<Image>,
    dst_image_layout: ImageLayout,
    regions: Vec<ash::vk::BufferImageCopy>,
}

impl CopyBufferToImageCommand {
    fn new(
        src_buffer: Arc<Buffer>,
        dst_image: Arc<Image>,
        dst_image_layout: ImageLayout,
        regions: Vec<BufferImageCopy>,
    ) -> Self {
        Self {
            src_buffer,
            dst_image,
            dst_image_layout,
            regions: regions
                .into_iter()
                .map(ash::vk::BufferImageCopy::from)
                .collect(),
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct DispatchCommand {
    group_count_x: u32,
    group_count_y: u32,
    group_count_z: u32,
}

impl DispatchCommand {
    fn new(group_count_x: u32, group_count_y: u32, group_count_z: u32) -> Self {
        Self {
            group_count_x,
            group_count_y,
            group_count_z,
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct DrawCommand {
    vertex_count: u32,
    instance_count: u32,
    first_vertex: u32,
    first_instance: u32,
}

impl DrawCommand {
    fn new(vertex_count: u32, instance_count: u32, first_vertex: u32, first_instance: u32) -> Self {
        Self {
            vertex_count,
            instance_count,
            first_vertex,
            first_instance,
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct DrawIndexedCommand {
    index_count: u32,
    instance_count: u32,
    first_index: u32,
    vertex_offset: i32,
    first_instance: u32,
}

impl DrawIndexedCommand {
    fn new(
        index_count: u32,
        instance_count: u32,
        first_index: u32,
        vertex_offset: i32,
        first_instance: u32,
    ) -> Self {
        Self {
            index_count,
            instance_count,
            first_index,
            vertex_offset,
            first_instance,
        }
    }
}

#[derive(Clone, Debug)]
struct ExecuteCommandsCommand {
    command_buffers_ash: Vec<ash::vk::CommandBuffer>,
    _command_buffers: Vec<Arc<CommandBuffer>>,
}

impl ExecuteCommandsCommand {
    fn new(command_buffers: Vec<Arc<CommandBuffer>>) -> Self {
        Self {
            command_buffers_ash: command_buffers
                .iter()
                .map(|command_buffer| command_buffer.handle())
                .collect(),
            _command_buffers: command_buffers,
        }
    }
}

#[derive(Clone, Debug)]
struct PipelineBarrierCommand {
    image_memory_barriers: Vec<ash::vk::ImageMemoryBarrier2>,
    buffer_memory_barriers: Vec<ash::vk::BufferMemoryBarrier2>,
    memory_barriers: Vec<ash::vk::MemoryBarrier2>,
    pipeline_barrier: PipelineBarrier,
}

impl PipelineBarrierCommand {
    fn new(pipeline_barrier: PipelineBarrier) -> Self {
        Self {
            image_memory_barriers: pipeline_barrier
                .image_memory_barriers
                .iter()
                .cloned()
                .map(ash::vk::ImageMemoryBarrier2::from)
                .collect(),
            buffer_memory_barriers: pipeline_barrier
                .buffer_memory_barriers
                .iter()
                .cloned()
                .map(ash::vk::BufferMemoryBarrier2::from)
                .collect(),
            memory_barriers: pipeline_barrier
                .memory_barriers
                .iter()
                .cloned()
                .map(ash::vk::MemoryBarrier2::from)
                .collect(),
            pipeline_barrier,
        }
    }

    fn dependency_info(&self) -> ash::vk::DependencyInfo {
        ash::vk::DependencyInfo::builder()
            .dependency_flags(self.pipeline_barrier.dependency_flags.into())
            .memory_barriers(&self.memory_barriers)
            .buffer_memory_barriers(&self.buffer_memory_barriers)
            .image_memory_barriers(&self.image_memory_barriers)
            .build()
    }
}

#[derive(Clone, Debug)]
struct PushConstantsCommand {
    values: Vec<u8>,
    offset: u32,
    stages: ShaderStages,
    layout: Arc<PipelineLayout>,
}

impl PushConstantsCommand {
    fn new(
        layout: Arc<PipelineLayout>,
        stages: ShaderStages,
        offset: u32,
        values: Vec<u8>,
    ) -> Self {
        Self {
            values,
            offset,
            stages,
            layout,
        }
    }
}

#[derive(Clone, Debug)]
struct SetScissorCommand {
    scissors: Vec<ash::vk::Rect2D>,
    first_scissor: u32,
}

impl SetScissorCommand {
    fn new(first_scissor: u32, scissors: Vec<Rect2D>) -> Self {
        Self {
            scissors: scissors.into_iter().map(ash::vk::Rect2D::from).collect(),
            first_scissor,
        }
    }
}

#[derive(Clone, Debug)]
struct SetViewportCommand {
    viewports: Vec<ash::vk::Viewport>,
    first_viewport: u32,
}

impl SetViewportCommand {
    fn new(first_viewport: u32, viewports: Vec<Viewport>) -> Self {
        Self {
            viewports: viewports.into_iter().map(ash::vk::Viewport::from).collect(),
            first_viewport,
        }
    }
}

#[derive(Clone, Debug)]
struct BeginLabelCommand {
    label: ash::vk::DebugUtilsLabelEXT,
    name: CString,
}

impl BeginLabelCommand {
    fn new(label: &'static str, color: Vec4) -> Self {
        let mut command = Self {
            label: Default::default(),
            name: CString::new(label).expect("c string"),
        };
        command.label = ash::vk::DebugUtilsLabelEXT::builder()
            .label_name(&command.name)
            .color(color.into())
            .build();
        command
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::command_buffer::{CommandBuffer, CommandPool};
    use crate::gfx::device::Device;
    use crate::physical_device;

    #[test]
    fn build_command_buffer() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let command_pool = CommandPool::builder(device.clone())
            .queue_family_index(0)
            .build()
            .unwrap();
        let result = CommandBuffer::primary(device.clone(), command_pool.clone()).build();

        assert!(result.is_ok())
    }
}
