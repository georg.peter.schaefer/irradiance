/// Buffer copy operation.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct BufferCopy {
    src_offset: u64,
    dst_offset: u64,
    size: u64,
}

impl BufferCopy {
    /// Starts building a new copy buffer.
    pub fn builder() -> BufferCopyBuilder {
        BufferCopyBuilder {
            src_offset: 0,
            dst_offset: 0,
            size: 0,
        }
    }
}

/// Type for building [`BufferCopy`].
#[derive(Debug)]
pub struct BufferCopyBuilder {
    src_offset: u64,
    dst_offset: u64,
    size: u64,
}

impl BufferCopyBuilder {
    /// Sets the offset into the source buffer.
    pub fn src_offset(&mut self, src_offset: u64) -> &mut Self {
        self.src_offset = src_offset;
        self
    }

    /// Sets the offset into the destination buffer.
    pub fn dst_offset(&mut self, dst_offset: u64) -> &mut Self {
        self.dst_offset = dst_offset;
        self
    }

    /// Sets the size of the buffer region to copy.
    pub fn size(&mut self, size: u64) -> &mut Self {
        self.size = size;
        self
    }

    /// Builds the [`BufferCopy`].
    pub fn build(&mut self) -> BufferCopy {
        BufferCopy {
            src_offset: self.src_offset,
            dst_offset: self.dst_offset,
            size: self.size,
        }
    }
}

#[doc(hidden)]
impl From<BufferCopy> for ash::vk::BufferCopy {
    fn from(buffer_copy: BufferCopy) -> Self {
        ash::vk::BufferCopy::builder()
            .src_offset(buffer_copy.src_offset)
            .dst_offset(buffer_copy.dst_offset)
            .size(buffer_copy.size)
            .build()
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::command_buffer::BufferCopy;

    #[test]
    fn to_buffer_copy() {
        let buffer_copy = ash::vk::BufferCopy::from(
            BufferCopy::builder()
                .src_offset(64)
                .dst_offset(32)
                .size(24)
                .build(),
        );

        assert_eq!(64, buffer_copy.src_offset);
        assert_eq!(32, buffer_copy.dst_offset);
        assert_eq!(24, buffer_copy.size);
    }
}
