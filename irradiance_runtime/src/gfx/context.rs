use std::cell::UnsafeCell;
use std::sync::Arc;

use itertools::Itertools;

use crate::gfx::command_buffer::{CommandPool, PerThreadCommandPool};
use crate::gfx::device::Device;
use crate::gfx::image::{Format, ImageUsage};
use crate::gfx::instance::{ApplicationInfo, Instance, InstanceExtensions};
use crate::gfx::physical_device::{
    DeviceExtensions, PhysicalDevice, PhysicalDeviceFeatures, PhysicalDeviceType,
};
use crate::gfx::queue::Queue;
use crate::gfx::sync::SharingMode;
use crate::gfx::wsi::{
    ColorSpace, PresentMode, Surface, SurfaceCapabilities, SurfaceFormat, Swapchain,
};
use crate::gfx::{Extent2D, GfxError};
use crate::window::Window;

use super::pipeline::{DescriptorPool, DescriptorSetLayout, PerThreadDescriptorPool};

/// The graphics context.
#[derive(Debug)]
pub struct GraphicsContext {
    swapchain: UnsafeCell<Option<Arc<Swapchain>>>,
    per_thread_descriptor_pool: PerThreadDescriptorPool,
    per_thread_command_pool: PerThreadCommandPool,
    present_queue: Option<Arc<Queue>>,
    graphics_queue: Arc<Queue>,
    device: Arc<Device>,
    surface: Option<Arc<Surface>>,
    instance: Arc<Instance>,
    window: Option<Arc<Window>>,
}

impl GraphicsContext {
    /// Starts building a graphics context.
    pub fn builder() -> GraphicsContextBuilder {
        GraphicsContextBuilder {
            physical_device_features: None,
            device_extensions: None,
            instance_extensions: None,
            window: None,
        }
    }

    /// Starts building a graphics context with a swapchain for presenting images.
    pub fn with_swapchain(window: Arc<Window>) -> GraphicsContextBuilder {
        GraphicsContextBuilder {
            physical_device_features: None,
            device_extensions: None,
            instance_extensions: None,
            window: Some(window),
        }
    }

    /// Returns the [`Instance`].
    pub fn instance(&self) -> &Arc<Instance> {
        &self.instance
    }

    /// Returns the [`Surface`].
    pub fn surface(&self) -> Option<&Arc<Surface>> {
        self.surface.as_ref()
    }

    /// Returns the [`Device`].
    pub fn device(&self) -> &Arc<Device> {
        &self.device
    }

    /// Returns the graphics [`Queue`].
    pub fn graphics_queue(&self) -> &Arc<Queue> {
        &self.graphics_queue
    }

    /// Returns the present [`Queue`].
    pub fn present_queue(&self) -> Option<&Arc<Queue>> {
        self.present_queue.as_ref()
    }

    /// Returns a [`CommandPool`] for the current thread.
    pub fn command_pool(&self) -> Result<Arc<CommandPool>, GfxError> {
        self.per_thread_command_pool.get()
    }

    /// Returns a [`DescriptorPool`] for the current thread with enough free descriptors to allocate a descriptor set wirh [`descriptor_set_layout`].
    pub fn descriptor_pool(
        &self,
        descriptor_set_layout: &DescriptorSetLayout,
    ) -> Result<Arc<DescriptorPool>, GfxError> {
        self.per_thread_descriptor_pool.get(descriptor_set_layout)
    }

    /// Returns the [`Swapchain`].
    pub fn swapchain(&self) -> Option<&Arc<Swapchain>> {
        unsafe { &*self.swapchain.get() }.as_ref()
    }

    #[doc(hidden)]
    pub fn recreate_swapchain(&self) -> Result<(), GfxError> {
        self.device.wait().unwrap();
        let old_swapchain = unsafe { &mut *self.swapchain.get() }.take();

        unsafe { &mut *self.swapchain.get() }.replace(GraphicsContextBuilder::create_swapchain(
            self.window.as_ref().unwrap(),
            self.surface.clone(),
            self.device.clone(),
            old_swapchain,
        )?);

        Ok(())
    }
}

unsafe impl Send for GraphicsContext {}

unsafe impl Sync for GraphicsContext {}

/// Type for building a [`Context`].
#[derive(Debug)]
pub struct GraphicsContextBuilder {
    physical_device_features: Option<PhysicalDeviceFeatures>,
    device_extensions: Option<DeviceExtensions>,
    instance_extensions: Option<InstanceExtensions>,
    window: Option<Arc<Window>>,
}

impl GraphicsContextBuilder {
    /// Sets the required [instance extensions](InstanceExtensions).
    pub fn instance_extensions(&mut self, instance_extensions: InstanceExtensions) -> &mut Self {
        self.instance_extensions = Some(instance_extensions);
        self
    }

    /// Sets the required [device extensions](DeviceExtensions).
    pub fn device_extensions(&mut self, physical_device_extensions: DeviceExtensions) -> &mut Self {
        self.device_extensions = Some(physical_device_extensions);
        self
    }

    /// Sets the required [physical device features](PhysicalDeviceFeatures).
    pub fn physical_device_features(
        &mut self,
        physical_device_features: PhysicalDeviceFeatures,
    ) -> &mut Self {
        self.physical_device_features = Some(physical_device_features);
        self
    }

    /// Builds the [`Device`].
    ///
    /// # Errors
    /// This functions returns an error if the device creation fails.
    pub fn build(&mut self) -> Result<Arc<GraphicsContext>, GfxError> {
        self.check_required_instance_extensions()?;

        let instance = Instance::builder(ApplicationInfo::new((1, 3, 0).into()))
            .extensions(self.instance_extensions.unwrap_or_default())
            .build()?;
        let surface = self.create_surface(instance.clone(), self.window.clone())?;
        let physical_device = self.select_physical_device(instance.clone(), surface.as_deref())?;
        let (device, queues) = self.create_device(physical_device.clone(), surface.as_deref())?;
        let graphics_queue = queues.get(0).cloned().unwrap();
        let present_queue = if surface.is_some() {
            Some(queues.get(1).unwrap_or(&graphics_queue).clone())
        } else {
            None
        };
        let per_thread_command_pool =
            PerThreadCommandPool::new(device.clone(), graphics_queue.family_index());
        let per_thread_descriptor_pool = PerThreadDescriptorPool::new(device.clone());
        let swapchain = UnsafeCell::new(if let Some(surface) = surface.clone() {
            Some(Self::create_swapchain(
                self.window.as_ref().unwrap(),
                Some(surface),
                device.clone(),
                None,
            )?)
        } else {
            None
        });

        log::info!(
            "Selected physical device: {}",
            physical_device.properties().device_name
        );
        log::info!(
            "Vulkan API Version: {}",
            physical_device.properties().api_version
        );

        Ok(Arc::new(GraphicsContext {
            swapchain,
            per_thread_descriptor_pool,
            per_thread_command_pool,
            present_queue,
            graphics_queue,
            device,
            surface,
            instance,
            window: self.window.clone(),
        }))
    }

    fn check_required_instance_extensions(&self) -> Result<(), GfxError> {
        let supported_extensions = Instance::extensions()?;
        match self.instance_extensions {
            Some(instance_extensions) if !instance_extensions.is_subset(&supported_extensions) => {
                Err(GfxError::ExtensionNotPresent)
            }
            _ => Ok(()),
        }
    }

    fn create_surface(
        &self,
        instance: Arc<Instance>,
        window: Option<Arc<Window>>,
    ) -> Result<Option<Arc<Surface>>, GfxError> {
        if let Some(window) = window {
            return Ok(Some(Surface::builder(instance, window).build()?));
        }
        Ok(None)
    }

    fn select_physical_device(
        &self,
        instance: Arc<Instance>,
        surface: Option<&Surface>,
    ) -> Result<Arc<PhysicalDevice>, GfxError> {
        let mut rated_physical_devices = Vec::new();
        for physical_device in instance.physical_devices()? {
            if self.is_suitable(&physical_device, surface)? {
                rated_physical_devices
                    .push((physical_device.clone(), Self::rate_device(&physical_device)));
            }
        }

        rated_physical_devices
            .iter()
            .sorted_by_key(|(_, rating)| *rating)
            .rev()
            .map(|(physical_device, _)| physical_device)
            .next()
            .cloned()
            .ok_or(GfxError::NoSuitablePhysicalDevice)
    }

    fn is_suitable(
        &self,
        physical_device: &PhysicalDevice,
        surface: Option<&Surface>,
    ) -> Result<bool, GfxError> {
        Ok([
            self.has_required_device_extensions(physical_device)?,
            self.has_required_physical_device_features(physical_device),
            QueueFamilyIndices::new(physical_device, surface)?.is_complete(),
            self.has_swapchain_support(physical_device, surface)?,
        ]
        .into_iter()
        .all(std::convert::identity))
    }

    fn has_required_device_extensions(
        &self,
        physical_device: &PhysicalDevice,
    ) -> Result<bool, GfxError> {
        let supported_extensions = physical_device.extensions()?;
        match self.device_extensions {
            Some(device_extensions) if !device_extensions.is_subset(&supported_extensions) => {
                Ok(false)
            }
            _ => Ok(true),
        }
    }

    fn has_required_physical_device_features(&self, physical_device: &PhysicalDevice) -> bool {
        let supported_features = physical_device.features();
        !matches!(self.physical_device_features, Some(device_features) if !device_features.is_subset(&supported_features))
    }

    fn has_swapchain_support(
        &self,
        physical_device: &PhysicalDevice,
        surface: Option<&Surface>,
    ) -> Result<bool, GfxError> {
        if let Some(surface) = surface {
            return Ok(!physical_device.surface_formats(surface)?.is_empty()
                && !physical_device.surface_present_modes(surface)?.is_empty());
        }
        Ok(true)
    }

    const TYPE_OTHER_RATING: usize = 0;
    const TYPE_INTEGRATED_GPU_RATING: usize = 500;
    const TYPE_DISCRETE_GPU_RATING: usize = 1000;
    const TYPE_VIRTUAL_GPU_RATING: usize = 200;
    const TYPE_CPU_RATING: usize = 100;

    fn rate_device(physical_device: &PhysicalDevice) -> usize {
        match physical_device.properties().device_type {
            PhysicalDeviceType::Other => Self::TYPE_OTHER_RATING,
            PhysicalDeviceType::IntegratedGpu => Self::TYPE_INTEGRATED_GPU_RATING,
            PhysicalDeviceType::DiscreteGpu => Self::TYPE_DISCRETE_GPU_RATING,
            PhysicalDeviceType::VirtualGpu => Self::TYPE_VIRTUAL_GPU_RATING,
            PhysicalDeviceType::Cpu => Self::TYPE_CPU_RATING,
        }
    }

    fn create_device(
        &self,
        physical_device: Arc<PhysicalDevice>,
        surface: Option<&Surface>,
    ) -> Result<(Arc<Device>, Vec<Arc<Queue>>), GfxError> {
        let queue_family_indices = QueueFamilyIndices::new(&physical_device, surface)?;
        let mut queues = vec![queue_family_indices.graphics.unwrap()];
        if let Some(present_queue_family_index) = queue_family_indices.present {
            queues.push(present_queue_family_index);
        }
        let queues = queues
            .into_iter()
            .unique()
            .map(|family_index| (family_index, vec![1.0]))
            .collect();
        Device::builder(physical_device, queues)
            .extensions(self.device_extensions.unwrap_or_default())
            .features(self.physical_device_features.unwrap_or_default())
            .build()
    }

    fn create_swapchain(
        window: &Window,
        surface: Option<Arc<Surface>>,
        device: Arc<Device>,
        old_swapchain: Option<Arc<Swapchain>>,
    ) -> Result<Arc<Swapchain>, GfxError> {
        let physical_device = device.physical_device();
        let surface_capabilities = device
            .physical_device()
            .surface_capabilities(surface.as_deref().unwrap())?;
        let min_image_count = Self::calculate_min_image_count(&surface_capabilities);
        let surface_format = Self::select_surface_format(
            &physical_device.surface_formats(surface.as_deref().unwrap())?,
        );
        let queue_family_indices = QueueFamilyIndices::new(physical_device, surface.as_deref())?;
        let queue_family_indices = vec![
            queue_family_indices.graphics.unwrap(),
            queue_family_indices.present.unwrap(),
        ]
        .into_iter()
        .dedup()
        .collect::<Vec<_>>();
        let image_sharing_mode = (queue_family_indices.len() == 2)
            .then(|| SharingMode::Concurrent(queue_family_indices))
            .unwrap_or(SharingMode::Exclusive);
        let image_extent = Self::calculate_image_extent(window, &surface_capabilities);
        let present_mode = Self::select_present_mode(
            &physical_device.surface_present_modes(surface.as_deref().unwrap())?,
        );

        let mut builder = Swapchain::builder(surface.unwrap(), device);
        builder
            .min_image_count(min_image_count)
            .image_format(surface_format.format)
            .image_color_space(surface_format.color_space)
            .image_extent(image_extent)
            .image_usage(ImageUsage {
                color_attachment: true,
                ..Default::default()
            })
            .image_sharing_mode(image_sharing_mode)
            .pre_transform(surface_capabilities.current_transforms)
            .present_mode(present_mode)
            .clipped(true);

        if let Some(old_swapchain) = old_swapchain {
            builder.old_swapchain(old_swapchain);
        }

        builder.build()
    }

    fn calculate_min_image_count(surface_capabilities: &SurfaceCapabilities) -> u32 {
        let mut min_image_count = surface_capabilities.min_image_count + 1;
        if surface_capabilities.max_image_count > 0
            && min_image_count > surface_capabilities.max_image_count
        {
            min_image_count = surface_capabilities.max_image_count;
        }
        min_image_count
    }

    fn select_surface_format(surface_formats: &[SurfaceFormat]) -> SurfaceFormat {
        *surface_formats
            .iter()
            .find(|surface_format| {
                surface_format.format == Format::B8G8R8A8Srgb
                    && surface_format.color_space == ColorSpace::SrgbNonLinear
            })
            .unwrap_or(&surface_formats[0])
    }

    fn calculate_image_extent(
        window: &Window,
        surface_capabilities: &SurfaceCapabilities,
    ) -> Extent2D {
        let window_extent = window.resolution();
        if surface_capabilities.current_extent.width != u32::MAX {
            surface_capabilities.current_extent
        } else {
            Extent2D {
                width: window_extent[0].clamp(
                    surface_capabilities.min_image_extent.width,
                    surface_capabilities.max_image_extent.width,
                ),
                height: window_extent[1].clamp(
                    surface_capabilities.min_image_extent.height,
                    surface_capabilities.max_image_extent.height,
                ),
            }
        }
    }

    fn select_present_mode(present_modes: &[PresentMode]) -> PresentMode {
        *present_modes
            .iter()
            .find(|present_mode| **present_mode == PresentMode::Mailbox)
            .unwrap_or(&present_modes[0])
    }
}

struct QueueFamilyIndices {
    present: Option<u32>,
    graphics: Option<u32>,
    surface_support: bool,
}

impl QueueFamilyIndices {
    fn new(physical_device: &PhysicalDevice, surface: Option<&Surface>) -> Result<Self, GfxError> {
        let mut indices = Self {
            present: None,
            graphics: None,
            surface_support: surface.is_some(),
        };
        for (index, properties) in physical_device.queue_family_properties().iter().enumerate() {
            if properties.queue_flags.graphics {
                indices.graphics = Some(index as _);
            }
            if let Some(surface) = surface {
                if physical_device.is_surface_supported(index as _, surface)? {
                    indices.present = Some(index as _);
                }
            }
            if indices.is_complete() {
                break;
            }
        }
        Ok(indices)
    }

    fn is_complete(&self) -> bool {
        !(self.graphics.is_none() || self.surface_support && self.present.is_none())
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::instance::InstanceExtensions;
    use crate::gfx::physical_device::{DeviceExtensions, PhysicalDeviceFeatures};
    use crate::gfx::GfxError;
    use crate::gfx::GraphicsContext;

    #[test]
    fn create_context_instance_extensions_missing() {
        let result = GraphicsContext::builder()
            .instance_extensions(InstanceExtensions {
                khr_android_surface: true,
                ..Default::default()
            })
            .build();

        assert!(matches!(result, Err(GfxError::ExtensionNotPresent)));
    }

    #[test]
    fn create_context_physical_device_extensions_missing() {
        let result = GraphicsContext::builder()
            .device_extensions(DeviceExtensions {
                google_user_type: true,
                amd_buffer_marker: true,
                nv_acquire_winrt_display: true,
                valve_mutable_descriptor_type: true,
                ..Default::default()
            })
            .build();

        assert!(matches!(result, Err(GfxError::NoSuitablePhysicalDevice)));
    }

    #[test]
    fn create_context_physical_device_features_missing() {
        let result = GraphicsContext::builder()
            .physical_device_features(PhysicalDeviceFeatures {
                robust_buffer_access: true,
                full_draw_index_uint32: true,
                image_cube_array: true,
                independent_blend: true,
                geometry_shader: true,
                tessellation_shader: true,
                sample_rate_shading: true,
                dual_src_blend: true,
                logic_op: true,
                multi_draw_indirect: true,
                draw_indirect_first_instance: true,
                depth_clamp: true,
                depth_bias_clamp: true,
                fill_mode_non_solid: true,
                depth_bounds: true,
                wide_lines: true,
                large_points: true,
                alpha_to_one: true,
                multi_viewport: true,
                sampler_anisotropy: true,
                texture_compression_etc2: true,
                texture_compression_astc_ldr: true,
                texture_compression_bc: true,
                occlusion_query_precise: true,
                pipeline_statistics_query: true,
                vertex_pipeline_stores_and_atomics: true,
                fragment_stores_and_atomics: true,
                shader_tessellation_and_geometry_point_size: true,
                shader_image_gather_extended: true,
                shader_storage_image_extended_formats: true,
                shader_storage_image_multisample: true,
                shader_storage_image_read_without_format: true,
                shader_storage_image_write_without_format: true,
                shader_uniform_buffer_array_dynamic_indexing: true,
                shader_sampled_image_array_dynamic_indexing: true,
                shader_storage_buffer_array_dynamic_indexing: true,
                shader_storage_image_array_dynamic_indexing: true,
                shader_clip_distance: true,
                shader_cull_distance: true,
                shader_float64: true,
                shader_int64: true,
                shader_int16: true,
                shader_resource_residency: true,
                shader_resource_min_lod: true,
                sparse_binding: true,
                sparse_residency_buffer: true,
                sparse_residency_image2d: true,
                sparse_residency_image3d: true,
                sparse_residency_2_samples: true,
                sparse_residency_4_samples: true,
                sparse_residency_8_samples: true,
                sparse_residency_16_samples: true,
                sparse_residency_aliased: true,
                variable_multisample_rate: true,
                inherited_queries: true,
                synchronization2: true,
                dynamic_rendering: true,
            })
            .build();

        assert!(matches!(result, Err(GfxError::NoSuitablePhysicalDevice)));
    }

    #[test]
    fn create_context_no_extensions_no_features() {
        let result = GraphicsContext::builder().build();

        assert!(result.is_ok());
    }

    #[test]
    fn create_context() {
        let result = GraphicsContext::builder()
            .instance_extensions(InstanceExtensions {
                ext_debug_utils: true,
                ..Default::default()
            })
            .device_extensions(DeviceExtensions {
                khr_maintenance1: true,
                khr_maintenance2: true,
                khr_maintenance3: true,
                khr_maintenance4: true,
                ..Default::default()
            })
            .physical_device_features(PhysicalDeviceFeatures {
                robust_buffer_access: true,
                ..Default::default()
            })
            .build();

        assert!(result.is_ok())
    }
}
