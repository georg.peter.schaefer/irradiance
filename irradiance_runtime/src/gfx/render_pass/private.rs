use std::sync::Arc;

use crate::gfx::image::{ImageLayout, ImageView};
use crate::gfx::render_pass::{AttachmentLoadOp, AttachmentStoreOp, ClearValue, RenderingFlags};
use crate::gfx::{Handle, Rect2D};

/// Describes a rendering pass.
#[derive(Clone, Debug)]
pub struct RenderPass {
    pub(crate) stencil_attachment: Option<Attachment>,
    pub(crate) depth_attachment: Option<Attachment>,
    pub(crate) color_attachments: Vec<Attachment>,
    pub(crate) view_mask: u32,
    pub(crate) layer_count: u32,
    pub(crate) render_area: Rect2D,
    pub(crate) flags: RenderingFlags,
}

impl RenderPass {
    /// Starts building a render pass.
    pub fn builder() -> RenderPassBuilder {
        RenderPassBuilder {
            stencil_attachment: None,
            depth_attachment: None,
            color_attachments: vec![],
            view_mask: 0,
            layer_count: 1,
            render_area: Default::default(),
            flags: Default::default(),
        }
    }
}

/// Type for building a [`RenderPass`].
#[derive(Debug)]
pub struct RenderPassBuilder {
    stencil_attachment: Option<Attachment>,
    depth_attachment: Option<Attachment>,
    color_attachments: Vec<Attachment>,
    view_mask: u32,
    layer_count: u32,
    render_area: Rect2D,
    flags: RenderingFlags,
}

impl RenderPassBuilder {
    /// Sets the rendering flags.
    pub fn flags(&mut self, flags: RenderingFlags) -> &mut Self {
        self.flags = flags;
        self
    }

    /// Sets the render area.
    pub fn render_area(&mut self, render_area: Rect2D) -> &mut Self {
        self.render_area = render_area;
        self
    }

    /// Sets the layer count.
    pub fn layer_count(&mut self, layer_count: u32) -> &mut Self {
        self.layer_count = layer_count;
        self.view_mask = 0;
        self
    }

    /// Sets the view mask.
    pub fn view_mask(&mut self, view_mask: u32) -> &mut Self {
        self.view_mask = view_mask;
        self.layer_count = 0;
        self
    }

    /// Adds a color attachment.
    pub fn color_attachment(&mut self, color_attachment: Attachment) -> &mut Self {
        self.color_attachments.push(color_attachment);
        self
    }

    /// Sets the depth attachment.
    pub fn depth_attachment(&mut self, depth_attachment: Attachment) -> &mut Self {
        self.depth_attachment = Some(depth_attachment);
        self
    }

    /// Sets the stencil attachment.
    pub fn stencil_attachment(&mut self, stencil_attachment: Attachment) -> &mut Self {
        self.stencil_attachment = Some(stencil_attachment);
        self
    }

    /// Builds the [`RenderPass`].
    pub fn build(&mut self) -> RenderPass {
        RenderPass {
            stencil_attachment: self.stencil_attachment.clone(),
            depth_attachment: self.depth_attachment.clone(),
            color_attachments: self.color_attachments.clone(),
            view_mask: self.view_mask,
            layer_count: self.layer_count,
            render_area: self.render_area,
            flags: self.flags,
        }
    }
}

/// Describes an attachment in a render pass.
#[derive(Clone, Debug)]
pub struct Attachment {
    clear_value: ClearValue,
    store_op: AttachmentStoreOp,
    load_op: AttachmentLoadOp,
    image_layout: ImageLayout,
    image_view: Arc<ImageView>,
}

impl Attachment {
    /// Starts building an attachment.
    pub fn builder(image_view: Arc<ImageView>) -> AttachmentBuilder {
        AttachmentBuilder {
            clear_value: ClearValue::Float([0.0, 0.0, 0.0, 1.0]),
            store_op: AttachmentStoreOp::DontCare,
            load_op: AttachmentLoadOp::DontCare,
            image_layout: ImageLayout::Undefined,
            image_view,
        }
    }

    pub(crate) fn as_ffi(&self) -> ash::vk::RenderingAttachmentInfo {
        ash::vk::RenderingAttachmentInfo::builder()
            .image_view(self.image_view.handle())
            .image_layout(self.image_layout.into())
            .load_op(self.load_op.into())
            .store_op(self.store_op.into())
            .clear_value(self.clear_value.into())
            .build()
    }
}

/// Type for building an [`Attachment`].
#[derive(Debug)]
pub struct AttachmentBuilder {
    clear_value: ClearValue,
    store_op: AttachmentStoreOp,
    load_op: AttachmentLoadOp,
    image_layout: ImageLayout,
    image_view: Arc<ImageView>,
}

impl AttachmentBuilder {
    /// Sets the image layout.
    pub fn image_layout(&mut self, image_layout: ImageLayout) -> &mut Self {
        self.image_layout = image_layout;
        self
    }

    /// Sets the attachment load operation.
    pub fn load_op(&mut self, load_op: AttachmentLoadOp) -> &mut Self {
        self.load_op = load_op;
        self
    }

    /// Sets the attachment store operation.
    pub fn store_op(&mut self, store_op: AttachmentStoreOp) -> &mut Self {
        self.store_op = store_op;
        self
    }

    /// Sets the clear value.
    pub fn clear_value(&mut self, clear_value: ClearValue) -> &mut Self {
        self.clear_value = clear_value;
        self
    }

    /// Builds the [`Attachment`].
    pub fn build(&mut self) -> Attachment {
        Attachment {
            clear_value: self.clear_value,
            store_op: self.store_op,
            load_op: self.load_op,
            image_layout: self.image_layout,
            image_view: self.image_view.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use irradiance_runtime::gfx::image::ImageView;

    use crate::gfx::device::Device;
    use crate::gfx::image::{
        Format, Image, ImageLayout, ImageTiling, ImageType, ImageUsage, ImageViewType, SampleCount,
    };
    use crate::gfx::render_pass::{
        Attachment, AttachmentLoadOp, AttachmentStoreOp, ClearValue, RenderPass, RenderingFlags,
    };
    use crate::gfx::sync::SharingMode;
    use crate::gfx::{Extent2D, Extent3D, Handle, Rect2D};
    use crate::physical_device;

    #[test]
    fn build_render_pass() {
        let physical_device = physical_device!();
        let (device, _queues) = Device::builder(physical_device.clone(), vec![(0, vec![1.0])])
            .build()
            .unwrap();
        let image = Image::builder(device.clone())
            .image_type(ImageType::Type2D)
            .format(Format::R8G8B8A8Srgb)
            .extent(Extent3D {
                width: 1024,
                height: 1024,
                depth: 1,
            })
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCount::Sample1)
            .tiling(ImageTiling::Optimal)
            .usage(ImageUsage {
                sampled: true,
                transfer_dst: true,
                ..Default::default()
            })
            .sharing_mode(SharingMode::Exclusive)
            .initial_layout(ImageLayout::Undefined)
            .build()
            .unwrap();
        let image_view = ImageView::builder(device.clone(), image.clone())
            .view_type(ImageViewType::Type2D)
            .build()
            .unwrap();

        let render_pass = RenderPass::builder()
            .flags(RenderingFlags {
                contents_secondary_command_buffers: true,
                ..Default::default()
            })
            .render_area(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 800,
                    height: 600,
                },
            })
            .view_mask(42)
            .color_attachment(
                Attachment::builder(image_view.clone())
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Float([0.0, 0.0, 0.0, 1.0]))
                    .build(),
            )
            .depth_attachment(
                Attachment::builder(image_view.clone())
                    .image_layout(ImageLayout::DepthStencilReadOnlyOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::DepthStencil((1.0, 0)))
                    .build(),
            )
            .stencil_attachment(
                Attachment::builder(image_view.clone())
                    .image_layout(ImageLayout::DepthStencilReadOnlyOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::DepthStencil((1.0, 0)))
                    .build(),
            )
            .build();

        assert_eq!(
            RenderingFlags {
                contents_secondary_command_buffers: true,
                ..Default::default()
            },
            render_pass.flags
        );
        assert_eq!(
            Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 800,
                    height: 600
                }
            },
            render_pass.render_area
        );
        assert_eq!(0, render_pass.layer_count);
        assert_eq!(42, render_pass.view_mask);
        assert_eq!(
            image_view.handle(),
            render_pass.color_attachments[0].image_view.handle()
        );
        assert_eq!(
            ImageLayout::ColorAttachmentOptimal,
            render_pass.color_attachments[0].image_layout
        );
        assert_eq!(
            AttachmentLoadOp::Clear,
            render_pass.color_attachments[0].load_op
        );
        assert_eq!(
            AttachmentStoreOp::Store,
            render_pass.color_attachments[0].store_op
        );
        assert_eq!(
            image_view.handle(),
            render_pass
                .depth_attachment
                .as_ref()
                .unwrap()
                .image_view
                .handle()
        );
        assert_eq!(
            ImageLayout::DepthStencilReadOnlyOptimal,
            render_pass.depth_attachment.as_ref().unwrap().image_layout
        );
        assert_eq!(
            AttachmentLoadOp::Clear,
            render_pass.depth_attachment.as_ref().unwrap().load_op
        );
        assert_eq!(
            AttachmentStoreOp::Store,
            render_pass.depth_attachment.as_ref().unwrap().store_op
        );
        assert_eq!(
            image_view.handle(),
            render_pass
                .depth_attachment
                .as_ref()
                .unwrap()
                .image_view
                .handle()
        );
        assert_eq!(
            ImageLayout::DepthStencilReadOnlyOptimal,
            render_pass
                .stencil_attachment
                .as_ref()
                .unwrap()
                .image_layout
        );
        assert_eq!(
            AttachmentLoadOp::Clear,
            render_pass.stencil_attachment.as_ref().unwrap().load_op
        );
        assert_eq!(
            AttachmentStoreOp::Store,
            render_pass.stencil_attachment.as_ref().unwrap().store_op
        );
    }
}
