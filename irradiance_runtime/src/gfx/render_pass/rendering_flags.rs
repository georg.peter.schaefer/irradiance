use crate::impl_flag_set;

impl_flag_set! {
    RenderingFlags,
    doc = "Additional properties of a dynamic render pass instance.",
    (VK_RENDERING_CONTENTS_SECONDARY_COMMAND_BUFFERS_BIT, contents_secondary_command_buffers),
    (VK_RENDERING_SUSPENDING_BIT, suspending),
    (VK_RENDERING_RESUMING_BIT, resuming)
}

#[doc(hidden)]
impl From<RenderingFlags> for ash::vk::RenderingFlags {
    fn from(rendering_flags: RenderingFlags) -> Self {
        let mut flags = ash::vk::RenderingFlags::empty();
        if rendering_flags.contents_secondary_command_buffers {
            flags |= ash::vk::RenderingFlags::CONTENTS_SECONDARY_COMMAND_BUFFERS;
        }
        if rendering_flags.suspending {
            flags |= ash::vk::RenderingFlags::SUSPENDING;
        }
        if rendering_flags.resuming {
            flags |= ash::vk::RenderingFlags::RESUMING;
        }
        flags
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::render_pass::RenderingFlags;

    #[test]
    fn to_rendering_flags() {
        assert_eq!(
            ash::vk::RenderingFlags::CONTENTS_SECONDARY_COMMAND_BUFFERS
                | ash::vk::RenderingFlags::RESUMING,
            ash::vk::RenderingFlags::from(RenderingFlags {
                contents_secondary_command_buffers: true,
                suspending: false,
                resuming: true
            })
        );
        assert_eq!(
            ash::vk::RenderingFlags::SUSPENDING,
            ash::vk::RenderingFlags::from(RenderingFlags {
                contents_secondary_command_buffers: false,
                suspending: true,
                resuming: false
            })
        );
    }
}
