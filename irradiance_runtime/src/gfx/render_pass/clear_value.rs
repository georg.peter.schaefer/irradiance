/// Clear value.
#[allow(missing_docs)]
#[derive(Copy, Clone, PartialEq, Debug)]
pub enum ClearValue {
    Float([f32; 4]),
    Int([i32; 4]),
    Uint([u32; 4]),
    Depth(f32),
    Stencil(u32),
    DepthStencil((f32, u32)),
}

#[doc(hidden)]
impl From<ClearValue> for ash::vk::ClearValue {
    fn from(clear_value: ClearValue) -> Self {
        match clear_value {
            ClearValue::Float(float32) => ash::vk::ClearValue {
                color: ash::vk::ClearColorValue { float32 },
            },
            ClearValue::Int(int32) => ash::vk::ClearValue {
                color: ash::vk::ClearColorValue { int32 },
            },
            ClearValue::Uint(uint32) => ash::vk::ClearValue {
                color: ash::vk::ClearColorValue { uint32 },
            },
            ClearValue::Depth(depth) => ash::vk::ClearValue {
                depth_stencil: ash::vk::ClearDepthStencilValue {
                    depth,
                    ..Default::default()
                },
            },
            ClearValue::Stencil(stencil) => ash::vk::ClearValue {
                depth_stencil: ash::vk::ClearDepthStencilValue {
                    stencil,
                    ..Default::default()
                },
            },
            ClearValue::DepthStencil((depth, stencil)) => ash::vk::ClearValue {
                depth_stencil: ash::vk::ClearDepthStencilValue { depth, stencil },
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::render_pass::ClearValue;

    #[test]
    fn to_clear_value() {
        unsafe {
            assert!(matches!(
                ash::vk::ClearValue::from(ClearValue::Float([0.0, 1.0, 2.0, 3.0])),
                ash::vk::ClearValue {
                    color: ash::vk::ClearColorValue {
                        float32: values
                    }
                }
                if values == [0.0, 1.0, 2.0, 3.0]
            ));
            assert!(matches!(
                ash::vk::ClearValue::from(ClearValue::Int([0, 1, 2, 3])),
                ash::vk::ClearValue {
                    color: ash::vk::ClearColorValue {
                        int32: values
                    }
                }
                if values == [0, 1, 2, 3]
            ));
            assert!(matches!(
                ash::vk::ClearValue::from(ClearValue::Uint([0, 1, 2, 3])),
                ash::vk::ClearValue {
                    color: ash::vk::ClearColorValue {
                        uint32: values
                    }
                }
                if values == [0, 1, 2, 3]
            ));
            assert!(matches!(
                ash::vk::ClearValue::from(ClearValue::Depth(42.0)),
                ash::vk::ClearValue {
                    depth_stencil: ash::vk::ClearDepthStencilValue {
                        depth: value,
                        stencil: _
                    }
                }
                if value == 42.0
            ));
            assert!(matches!(
                ash::vk::ClearValue::from(ClearValue::Stencil(19)),
                ash::vk::ClearValue {
                    depth_stencil: ash::vk::ClearDepthStencilValue {
                        depth: _,
                        stencil: value
                    }
                }
                if value == 19
            ));
            assert!(matches!(
                ash::vk::ClearValue::from(ClearValue::DepthStencil((42.0, 19))),
                ash::vk::ClearValue {
                    depth_stencil: ash::vk::ClearDepthStencilValue {
                        depth,
                        stencil
                    }
                }
                if depth == 42.0 && stencil == stencil
            ));
        }
    }
}
