/// How contents of an attachment are treated at the beginning of a render pass.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum AttachmentLoadOp {
    /// The previous contents of the image within the render area will be preserved.
    Load,
    /// The contents within the render area will be cleared to a uniform value.
    Clear,
    /// The previous contents within the area need not be preserved.
    DontCare,
    /// The previous contents of the image within the render area will be preserved, but the
    /// contents of the attachment will be undefined inside the render pass.
    None,
}

#[doc(hidden)]
impl From<AttachmentLoadOp> for ash::vk::AttachmentLoadOp {
    fn from(attachment_load_op: AttachmentLoadOp) -> Self {
        match attachment_load_op {
            AttachmentLoadOp::Load => ash::vk::AttachmentLoadOp::LOAD,
            AttachmentLoadOp::Clear => ash::vk::AttachmentLoadOp::CLEAR,
            AttachmentLoadOp::DontCare => ash::vk::AttachmentLoadOp::DONT_CARE,
            AttachmentLoadOp::None => ash::vk::AttachmentLoadOp::NONE_EXT,
        }
    }
}

/// How contents of an attachment are treated at the end of a render pass.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum AttachmentStoreOp {
    /// The contents generated during the render pass and within the render area are written to
    /// memory.
    Store,
    /// The contents within the render area are not needed after rendering, and may be discarded.
    DontCare,
    /// The contents within the render area are not accessed by the store operation.
    None,
}

#[doc(hidden)]
impl From<AttachmentStoreOp> for ash::vk::AttachmentStoreOp {
    fn from(attachment_store_op: AttachmentStoreOp) -> Self {
        match attachment_store_op {
            AttachmentStoreOp::Store => ash::vk::AttachmentStoreOp::STORE,
            AttachmentStoreOp::DontCare => ash::vk::AttachmentStoreOp::DONT_CARE,
            AttachmentStoreOp::None => ash::vk::AttachmentStoreOp::NONE,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::render_pass::{AttachmentLoadOp, AttachmentStoreOp};

    #[test]
    fn to_attachment_load_op() {
        assert_eq!(
            ash::vk::AttachmentLoadOp::LOAD,
            ash::vk::AttachmentLoadOp::from(AttachmentLoadOp::Load)
        );
        assert_eq!(
            ash::vk::AttachmentLoadOp::CLEAR,
            ash::vk::AttachmentLoadOp::from(AttachmentLoadOp::Clear)
        );
        assert_eq!(
            ash::vk::AttachmentLoadOp::DONT_CARE,
            ash::vk::AttachmentLoadOp::from(AttachmentLoadOp::DontCare)
        );
        assert_eq!(
            ash::vk::AttachmentLoadOp::NONE_EXT,
            ash::vk::AttachmentLoadOp::from(AttachmentLoadOp::None)
        );
    }

    #[test]
    fn to_attachment_store_op() {
        assert_eq!(
            ash::vk::AttachmentStoreOp::STORE,
            ash::vk::AttachmentStoreOp::from(AttachmentStoreOp::Store)
        );
        assert_eq!(
            ash::vk::AttachmentStoreOp::DONT_CARE,
            ash::vk::AttachmentStoreOp::from(AttachmentStoreOp::DontCare)
        );
        assert_eq!(
            ash::vk::AttachmentStoreOp::NONE,
            ash::vk::AttachmentStoreOp::from(AttachmentStoreOp::None)
        );
    }
}
