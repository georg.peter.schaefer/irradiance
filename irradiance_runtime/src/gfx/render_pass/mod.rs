//! Types for defining render passes.

pub use attachment_op::AttachmentLoadOp;
pub use attachment_op::AttachmentStoreOp;
pub use clear_value::ClearValue;
pub use private::Attachment;
pub use private::AttachmentBuilder;
pub use private::RenderPass;
pub use private::RenderPassBuilder;
pub use rendering_flags::RenderingFlags;

mod attachment_op;
mod clear_value;
mod private;
mod rendering_flags;
