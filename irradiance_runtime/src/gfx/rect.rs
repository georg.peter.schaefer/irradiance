use crate::gfx::{Extent2D, Offset2D};

/// A two-dimensional subregion.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Default)]
pub struct Rect2D {
    /// The rectangle offset
    pub offset: Offset2D,
    /// The rectangle extent.
    pub extent: Extent2D,
}

#[doc(hidden)]
impl From<Rect2D> for ash::vk::Rect2D {
    fn from(rect: Rect2D) -> Self {
        ash::vk::Rect2D::builder()
            .offset(rect.offset.into())
            .extent(rect.extent.into())
            .build()
    }
}

#[cfg(test)]
mod tests {
    use crate::gfx::rect::Rect2D;
    use crate::gfx::{Extent2D, Offset2D};

    #[test]
    fn to_rect2d() {
        assert_eq!(
            ash::vk::Rect2D::builder()
                .offset(ash::vk::Offset2D::builder().x(0).y(-10).build())
                .extent(ash::vk::Extent2D::builder().width(512).height(128).build())
                .build(),
            ash::vk::Rect2D::from(Rect2D {
                offset: Offset2D { x: 0, y: -10 },
                extent: Extent2D {
                    width: 512,
                    height: 128
                }
            })
        )
    }
}
