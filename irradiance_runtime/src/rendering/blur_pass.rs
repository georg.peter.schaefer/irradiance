use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::image::{Format, ImageView};
use crate::gfx::{GfxError, GraphicsContext};
use crate::math::{Vec2, Vec4};
use crate::rendering::SeparableBlurPass;
use std::sync::Arc;

/// Blur pass.
#[derive(Debug)]
pub struct BlurPass {
    separable_blur_pass: SeparableBlurPass,
}

impl BlurPass {
    /// Creates a new blur pass.
    pub fn new(graphics_context: Arc<GraphicsContext>, format: Format) -> Result<Self, GfxError> {
        Ok(Self {
            separable_blur_pass: SeparableBlurPass::new(graphics_context, format)?,
        })
    }

    /// Applies the blur.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        range: u32,
        image: Arc<ImageView>,
        intermediate_image: Arc<ImageView>,
    ) -> Result<(), GfxError> {
        builder.begin_label("Separable Blur Filter", Vec4::new(0.0, 0.5, 0.0, 1.0));
        builder.begin_label("Horizontal Blur Pass", Vec4::new(0.0, 0.1, 0.0, 1.0));
        self.separable_blur_pass.draw(
            builder,
            range,
            Vec2::new(1.0, 0.0),
            image.clone(),
            intermediate_image.clone(),
        )?;
        builder.end_label();
        builder.begin_label("Vertical Blur Pass", Vec4::new(0.0, 0.9, 0.0, 1.0));
        self.separable_blur_pass.draw(
            builder,
            range,
            Vec2::new(0.0, 1.0),
            intermediate_image,
            image,
        )?;
        builder.end_label();
        builder.end_label();
        Ok(())
    }
}
