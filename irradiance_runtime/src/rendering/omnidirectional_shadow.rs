use std::any::Any;
use std::sync::Arc;

use ron::Value;
use serde::ser::SerializeStruct;
use serde::{Serialize, Serializer};

use crate::core::Engine;
use crate::ecs::{Component, ComponentExt};
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{
    BorderColor, Format, Image, ImageAspects, ImageCreateFlags, ImageLayout, ImageSubresourceRange,
    ImageUsage, ImageView, ImageViewType, Sampler, SamplerAddressMode,
};
use crate::gfx::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorType, ShaderStages, WriteDescriptorSetImages,
};
use crate::gfx::{Extent3D, GfxError, GraphicsContext};
use crate::math::Mat4;

/// Omnidirectional shadow component.
#[derive(Clone, Debug)]
pub struct OmnidirectionalShadow {
    depth_buffer: Arc<ImageView>,
    faces: [Arc<ImageView>; 6],
    descriptor_set: Arc<DescriptorSet>,
    sampler: Arc<Sampler>,
    image_view: Arc<ImageView>,
    graphics_context: Arc<GraphicsContext>,
}

impl OmnidirectionalShadow {
    /// Creates a new omnidirectional shadow component.
    pub fn new(graphics_context: &Arc<GraphicsContext>, resolution: u32) -> Result<Self, GfxError> {
        let device = graphics_context.device();
        let image = Image::builder(device.clone())
            .image_create_flags(ImageCreateFlags {
                cube_compatible: true,
                ..Default::default()
            })
            .format(Format::R32SFloat)
            .extent(Extent3D {
                width: resolution,
                height: resolution,
                depth: 1,
            })
            .array_layers(6)
            .usage(ImageUsage {
                color_attachment: true,
                sampled: true,
                ..Default::default()
            })
            .build()?;
        let image_view = ImageView::builder(device.clone(), image.clone())
            .view_type(ImageViewType::Cube)
            .subresource_range(
                ImageSubresourceRange::builder()
                    .aspect_mask(ImageAspects {
                        color: true,
                        ..Default::default()
                    })
                    .layer_count(6)
                    .build(),
            )
            .build()?;
        let sampler = Sampler::builder(device.clone())
            .address_mode_u(SamplerAddressMode::ClampToBorder)
            .address_mode_v(SamplerAddressMode::ClampToBorder)
            .address_mode_w(SamplerAddressMode::ClampToBorder)
            .border_color(BorderColor::FloatOpaqueWhite)
            .build()?;
        let descriptor_set_layout = DescriptorSetLayout::builder(device.clone())
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()?;
        let descriptor_set = DescriptorSet::builder(
            device.clone(),
            graphics_context.descriptor_pool(&descriptor_set_layout)?,
            descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::CombinedImageSampler)
            .sampled_image(&sampler, &image_view, ImageLayout::ShaderReadOnlyOptimal)
            .build()]);
        let faces = [
            Self::create_face(device.clone(), image.clone(), 0)?,
            Self::create_face(device.clone(), image.clone(), 1)?,
            Self::create_face(device.clone(), image.clone(), 2)?,
            Self::create_face(device.clone(), image.clone(), 3)?,
            Self::create_face(device.clone(), image.clone(), 4)?,
            Self::create_face(device.clone(), image.clone(), 5)?,
        ];
        let depth_buffer = ImageView::builder(
            device.clone(),
            Image::builder(device.clone())
                .format(Format::D32SFloat)
                .extent(
                    Extent3D {
                        width: resolution,
                        height: resolution,
                        depth: 1,
                    }
                    .into(),
                )
                .usage(ImageUsage {
                    depth_stencil_attachment: true,
                    ..Default::default()
                })
                .build()?,
        )
        .subresource_range(
            ImageSubresourceRange::builder()
                .aspect_mask(ImageAspects {
                    depth: true,
                    ..Default::default()
                })
                .build(),
        )
        .build()?;

        image.set_debug_utils_object_name("Omnidirectional Shadow Image");
        image_view.set_debug_utils_object_name("Omnidirectional Shadow Image View");
        sampler.set_debug_utils_object_name("Omnidirectional Shadow Sampler");
        descriptor_set_layout
            .set_debug_utils_object_name("Omnidirectional Shadow Descriptor Set Layout");
        descriptor_set.set_debug_utils_object_name("Omnidirectional Shadow Descriptor Set");
        faces.iter().for_each(|face| {
            face.set_debug_utils_object_name("Omnidirectional Shadow Face Image View")
        });
        depth_buffer
            .image()
            .set_debug_utils_object_name("Omnidirectional Shadow Depth Buffer Image");
        depth_buffer.set_debug_utils_object_name("Omnidirectional Shadow Depth Buffer Image View");

        Ok(Self {
            depth_buffer,
            faces,
            descriptor_set,
            sampler,
            image_view,
            graphics_context: graphics_context.clone(),
        })
    }

    fn create_face(
        device: Arc<Device>,
        image: Arc<Image>,
        base_array_layer: u32,
    ) -> Result<Arc<ImageView>, GfxError> {
        ImageView::builder(device, image)
            .view_type(ImageViewType::Type2D)
            .subresource_range(
                ImageSubresourceRange::builder()
                    .aspect_mask(ImageAspects {
                        color: true,
                        ..Default::default()
                    })
                    .base_array_layer(base_array_layer)
                    .build(),
            )
            .build()
    }

    /// Returns the image view.
    pub fn image_view(&self) -> &Arc<ImageView> {
        &self.image_view
    }

    /// Returns the sampler.
    pub fn sampler(&self) -> &Arc<Sampler> {
        &self.sampler
    }

    /// Returns the descriptor set.
    pub fn descriptor_set(&self) -> &Arc<DescriptorSet> {
        &self.descriptor_set
    }

    /// Returns the faces.
    pub fn faces(&self) -> &[Arc<ImageView>] {
        &self.faces
    }

    /// Returns the depth buffer.
    pub fn depth_buffer(&self) -> &Arc<ImageView> {
        &self.depth_buffer
    }

    /// Returns the projection.
    pub fn projection(&self, intensity: f32) -> Mat4 {
        const NEAR: f32 = 0.1;
        const EPSILON: f32 = 0.001;
        let far = intensity.sqrt() / EPSILON.sqrt();
        Mat4::perspective(90f32.to_radians(), 1.0, NEAR, far)
    }
}

impl Serialize for OmnidirectionalShadow {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut serialize_struct = serializer.serialize_struct("OmnidirectionalShadow", 2)?;
        serialize_struct.serialize_field("type_name", "OmnidirectionalShadow")?;
        serialize_struct.serialize_field("resolution", &self.image_view.image().extent().width)?;
        serialize_struct.end()
    }
}

impl Component for OmnidirectionalShadow {
    fn as_any(self: Arc<Self>) -> Arc<dyn Any + Send + Sync + 'static> {
        self
    }

    fn duplicate(&self) -> Arc<dyn Component> {
        Arc::new(
            Self::new(
                &self.graphics_context,
                self.image_view.image().extent().width,
            )
            .expect("omnidirectional shadow"),
        )
    }
}

impl ComponentExt for OmnidirectionalShadow {
    fn type_name() -> &'static str {
        "OmnidirectionalShadow"
    }

    fn from_str(value: Value, engine: &dyn Engine) -> Option<Arc<dyn Component>> {
        #[derive(serde::Deserialize)]
        struct Properties {
            resolution: u32,
        }
        if let Ok(properties) = value.into_rust::<Properties>() {
            if let Ok(omnidirectional_shadow) =
                Self::new(engine.graphics_context(), properties.resolution)
            {
                return Some(Arc::new(omnidirectional_shadow));
            }
        }
        None
    }
}
