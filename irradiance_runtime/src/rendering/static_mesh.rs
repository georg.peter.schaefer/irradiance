use serde::Serialize;

use crate::asset::AssetRef;
use crate::rendering::Mesh;
use crate::Component;

/// Static mesh component
#[derive(Clone, Default, Debug, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct StaticMesh {
    /// Disables shadow casting.
    #[serde(default)]
    pub disable_shadow: bool,
    sub_mesh: Option<usize>,
    mesh: AssetRef<Mesh>,
}

impl StaticMesh {
    /// Creates a new static mesh component.
    pub fn new(mesh: AssetRef<Mesh>) -> Self {
        Self {
            disable_shadow: false,
            sub_mesh: None,
            mesh,
        }
    }

    /// Restricts the `StaticMesh` to use only the given `sub_mesh`.
    pub fn with_sub_mesh(mut self, sub_mesh: Option<usize>) -> Self {
        self.sub_mesh = sub_mesh;
        self
    }

    /// Returns the [`Mesh`]
    pub fn mesh(&self) -> &AssetRef<Mesh> {
        &self.mesh
    }

    /// Sets the [`Mesh`].
    pub fn set_mesh(&mut self, mesh: AssetRef<Mesh>) {
        self.mesh = mesh;
    }

    /// Returns the index of the used sub mesh.
    ///
    /// If `None` is returned, the [`StaticMesh`] uses the entire [`Mesh`].
    pub fn sub_mesh(&self) -> Option<usize> {
        self.sub_mesh
    }
}
