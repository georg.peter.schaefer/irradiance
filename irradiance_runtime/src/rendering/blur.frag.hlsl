[[vk::binding(0, 0)]]
Texture2D buffer_texture;
[[vk::binding(0, 0)]]
SamplerState buffer_sampler;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

struct PushConstants {
    uint range;
    float2 direction;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

float4 main(VSOutput input) : SV_TARGET {
    float2 dimensions;
    buffer_texture.GetDimensions(dimensions.x, dimensions.y);
    float2 texel_size = 1.0 / dimensions;
    uint n = 0;
    int range = int(push_constants.range);
    float4 result = float4(0.0);
    for (int i = -range; i < range; i++) {
        float2 offset = push_constants.direction * float(i) * texel_size;
        result += buffer_texture.Sample(buffer_sampler, input.tex_coord + offset);
        n++;
    }
    return result / float(n);
}
