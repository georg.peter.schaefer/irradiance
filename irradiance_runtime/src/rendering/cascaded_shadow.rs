use std::any::Any;
use std::sync::Arc;

use ron::Value;
use serde::ser::SerializeStruct;
use serde::{Serialize, Serializer};

use crate::core::Engine;
use crate::ecs::{Component, ComponentExt};
use crate::gfx::buffer::{Buffer, BufferUsage};
use crate::gfx::device::DeviceOwned;
use crate::gfx::image::{
    BorderColor, Format, Image, ImageAspects, ImageLayout, ImageSubresourceRange, ImageUsage,
    ImageView, ImageViewType, Sampler, SamplerAddressMode,
};
use crate::gfx::pipeline::{
    DescriptorSet, DescriptorSetLayout, DescriptorType, ShaderStages, WriteDescriptorSetBuffers,
    WriteDescriptorSetImages,
};
use crate::gfx::{Extent3D, GfxError, GraphicsContext};
use crate::math::Mat4;

/// Cascaded shadow component.
#[derive(Clone, Debug)]
pub struct CascadedShadow {
    ssbo: Arc<Buffer>,
    cascades: Vec<Cascade>,
    descriptor_set: Arc<DescriptorSet>,
    sampler: Arc<Sampler>,
    image_view: Arc<ImageView>,
    graphics_context: Arc<GraphicsContext>,
}

impl CascadedShadow {
    /// Creates a new cascaded shadow component.
    pub fn new(
        graphics_context: Arc<GraphicsContext>,
        layer_count: u32,
        resolution: u32,
    ) -> Result<Self, GfxError> {
        let device = graphics_context.device();
        let image = Image::builder(device.clone())
            .format(Format::D32SFloat)
            .extent(Extent3D {
                width: resolution,
                height: resolution,
                depth: 1,
            })
            .array_layers(layer_count)
            .usage(ImageUsage {
                depth_stencil_attachment: true,
                sampled: true,
                ..Default::default()
            })
            .build()?;
        let image_view = ImageView::builder(device.clone(), image.clone())
            .view_type(ImageViewType::Type2DArray)
            .subresource_range(
                ImageSubresourceRange::builder()
                    .aspect_mask(ImageAspects {
                        depth: true,
                        ..Default::default()
                    })
                    .layer_count(layer_count)
                    .build(),
            )
            .build()?;
        let sampler = Sampler::builder(device.clone())
            .address_mode_u(SamplerAddressMode::ClampToBorder)
            .address_mode_v(SamplerAddressMode::ClampToBorder)
            .address_mode_w(SamplerAddressMode::ClampToBorder)
            .border_color(BorderColor::FloatOpaqueWhite)
            .build()?;
        let descriptor_set_layout = DescriptorSetLayout::builder(device.clone())
            .binding(
                0,
                DescriptorType::StorageBuffer,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                1,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()?;
        let descriptor_set = DescriptorSet::builder(
            device.clone(),
            graphics_context.descriptor_pool(&descriptor_set_layout)?,
            descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
            .dst_binding(1)
            .descriptor_type(DescriptorType::CombinedImageSampler)
            .sampled_image(&sampler, &image_view, ImageLayout::ShaderReadOnlyOptimal)
            .build()]);

        image.set_debug_utils_object_name("Cascaded Shadow Image");
        image_view.set_debug_utils_object_name("Cascaded Shadow Image View");
        sampler.set_debug_utils_object_name("Cascaded Shadow Sampler");
        descriptor_set_layout.set_debug_utils_object_name("Cascaded Shadow Descriptor Set Layout");
        descriptor_set.set_debug_utils_object_name("Cascaded Shadow Descriptor Set");

        let mut cascades = Vec::default();
        for layer in 0..layer_count {
            let image_view = ImageView::builder(device.clone(), image.clone())
                .subresource_range(
                    ImageSubresourceRange::builder()
                        .aspect_mask(ImageAspects {
                            depth: true,
                            ..Default::default()
                        })
                        .base_array_layer(layer)
                        .build(),
                )
                .build()?;
            image_view.set_debug_utils_object_name("Cascaded Shadow Cascade Image View");

            cascades.push(Cascade {
                image_view,
                view_projection: Mat4::identity(),
                split_depth: 0.0,
            });
        }

        let ssbo = Buffer::from_data(
            device.clone(),
            cascades
                .iter()
                .map(|cascade| PerCascade {
                    view_projection: cascade.view_projection,
                    split_depth: Mat4::identity() * cascade.split_depth,
                })
                .collect(),
        )
        .usage(BufferUsage {
            storage_buffer: true,
            ..Default::default()
        })
        .build()?;
        descriptor_set.write_buffers(vec![WriteDescriptorSetBuffers::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::StorageBuffer)
            .buffer(&ssbo, 0, ash::vk::WHOLE_SIZE)
            .build()]);
        ssbo.set_debug_utils_object_name("Cascaded Shadow Per Cascade Buffer");

        Ok(Self {
            image_view,
            sampler,
            descriptor_set,
            cascades,
            ssbo,
            graphics_context,
        })
    }

    /// Returns the image view.
    pub fn image_view(&self) -> &Arc<ImageView> {
        &self.image_view
    }

    /// Returns the sampler.
    pub fn sampler(&self) -> &Arc<Sampler> {
        &self.sampler
    }

    /// Returns the descriptor set.
    pub fn descriptor_set(&self) -> &Arc<DescriptorSet> {
        &self.descriptor_set
    }

    /// Returns the cascades.
    pub fn cascades(&self) -> &Vec<Cascade> {
        &self.cascades
    }

    /// Returns a mutable reference to the cascades.
    pub fn cascades_mut(&mut self) -> &mut Vec<Cascade> {
        &mut self.cascades
    }

    /// Returns the shader storage buffer for the cascades.
    pub fn ssbo(&self) -> &Arc<Buffer> {
        &self.ssbo
    }

    /// Updates the shader storage buffer with the current cascades.
    pub fn update_ssbo(&self) {
        self.ssbo.write(
            self.cascades
                .iter()
                .map(|cascade| PerCascade {
                    view_projection: cascade.view_projection,
                    split_depth: Mat4::identity() * cascade.split_depth,
                })
                .collect::<Vec<_>>(),
        );
    }
}

impl Serialize for CascadedShadow {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut serialize_struct = serializer.serialize_struct("CascadedShadow", 3)?;
        serialize_struct.serialize_field("type_name", "CascadedShadow")?;
        serialize_struct.serialize_field("cascades", &(self.cascades.len() as u32))?;
        serialize_struct.serialize_field("resolution", &self.image_view.image().extent().width)?;
        serialize_struct.end()
    }
}

impl Component for CascadedShadow {
    fn as_any(self: Arc<Self>) -> Arc<dyn Any + Send + Sync + 'static> {
        self
    }

    fn duplicate(&self) -> Arc<dyn Component> {
        Arc::new(
            Self::new(
                self.graphics_context.clone(),
                self.image_view().image().array_layers(),
                self.image_view().image().extent().width,
            )
            .expect("cascaded shadow"),
        )
    }
}

impl ComponentExt for CascadedShadow {
    fn type_name() -> &'static str {
        "CascadedShadow"
    }

    fn from_str(value: Value, engine: &dyn Engine) -> Option<Arc<dyn Component>> {
        #[derive(serde::Deserialize)]
        struct Properties {
            resolution: u32,
            cascades: u32,
        }
        if let Ok(properties) = value.into_rust::<Properties>() {
            if let Ok(cascaded_shadow) = Self::new(
                engine.graphics_context().clone(),
                properties.cascades,
                properties.resolution,
            ) {
                return Some(Arc::new(cascaded_shadow));
            }
        }
        None
    }
}

/// Shadow map cascade.
#[derive(Clone, Debug)]
pub struct Cascade {
    image_view: Arc<ImageView>,
    view_projection: Mat4,
    split_depth: f32,
}

impl Cascade {
    /// Returns the image view.
    pub fn image_view(&self) -> &Arc<ImageView> {
        &self.image_view
    }

    /// Returns the split depth.
    pub fn split_depth(&self) -> f32 {
        self.split_depth
    }

    /// Sets the split depth.
    pub fn set_split_depth(&mut self, split_depth: f32) {
        self.split_depth = split_depth;
    }

    /// Gets the view projection matrix.
    pub fn view_projection(&self) -> Mat4 {
        self.view_projection
    }

    /// Sets the view projection matrix.
    pub fn set_view_projection(&mut self, view_projection: Mat4) {
        self.view_projection = view_projection;
    }
}

#[allow(unused)]
#[derive(Copy, Clone, Default)]
#[repr(C)]
struct PerCascade {
    view_projection: Mat4,
    split_depth: Mat4,
}
