//! Static mesh.

use std::collections::HashMap;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::ops::Deref;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::Arc;

use gltf::json;

use crate::asset::{Asset, AssetRef, Assets};
use crate::gfx::buffer::{Buffer, BufferUsage};
use crate::gfx::device::DeviceOwned;
use crate::gfx::sync::{AccessMask, PipelineStages};
use crate::gfx::{GfxError, GraphicsContext};
use crate::math::{Vec2, Vec3, Vec4};
use crate::rendering::{BoundingBox, Material};

/// A static mesh.
#[derive(Clone, Debug)]
pub struct Mesh {
    bounding_box: BoundingBox,
    sub_meshes: Vec<SubMesh>,
    index_buffer: Arc<Buffer>,
    vertex_buffer: Arc<Buffer>,
    indices: Vec<u32>,
    vertices: Vec<Vertex>,
}

impl Mesh {
    /// Returns the vertices.
    pub fn vertices(&self) -> &Vec<Vertex> {
        &self.vertices
    }

    /// Returns the indices.
    pub fn indices(&self) -> &Vec<u32> {
        &self.indices
    }

    /// Returns the vertex buffer.
    pub fn vertex_buffer(&self) -> &Arc<Buffer> {
        &self.vertex_buffer
    }

    /// Returns the index buffer.
    pub fn index_buffer(&self) -> &Arc<Buffer> {
        &self.index_buffer
    }

    /// Returns the sub meshes.
    pub fn sub_meshes(&self) -> &Vec<SubMesh> {
        &self.sub_meshes
    }

    /// Returns a mutable slice to the sub meshes.
    pub fn sub_meshes_mut(&mut self) -> &mut [SubMesh] {
        &mut self.sub_meshes
    }

    /// Returns the bounding box.
    pub fn bounding_box(&self) -> BoundingBox {
        self.bounding_box
    }

    fn create_buffer<T>(
        graphics_context: &GraphicsContext,
        data: Vec<T>,
        usage: BufferUsage,
        dst_stage_mask: PipelineStages,
        dst_access_mask: AccessMask,
    ) -> Result<Arc<Buffer>, MeshCreationError> {
        let size = (std::mem::size_of::<T>() * data.len()) as _;
        let staging_buffer = Buffer::from_data(graphics_context.device().clone(), data)
            .usage(BufferUsage {
                transfer_src: true,
                ..Default::default()
            })
            .build()?;
        let buffer = Buffer::with_size(graphics_context.device().clone(), size)
            .usage(usage.union(&BufferUsage {
                transfer_dst: true,
                ..Default::default()
            }))
            .build()?;
        staging_buffer.set_debug_utils_object_name("Mesh Staging Buffer");
        staging_buffer.copy_to_buffer(
            graphics_context.graphics_queue().clone(),
            graphics_context.command_pool()?,
            buffer.clone(),
            dst_stage_mask,
            dst_access_mask,
        )?;
        Ok(buffer)
    }

    fn create_vertex_buffer(
        graphics_context: &GraphicsContext,
        vertices: Vec<Vertex>,
    ) -> Result<Arc<Buffer>, MeshCreationError> {
        Self::create_buffer(
            graphics_context,
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            PipelineStages {
                vertex_input: true,
                ..Default::default()
            },
            AccessMask {
                vertex_attribute_read: true,
                ..Default::default()
            },
        )
    }

    fn create_index_buffer(
        graphics_context: &GraphicsContext,
        indices: Vec<u32>,
    ) -> Result<Arc<Buffer>, MeshCreationError> {
        Self::create_buffer(
            graphics_context,
            indices,
            BufferUsage {
                index_buffer: true,
                ..Default::default()
            },
            PipelineStages {
                index_input: true,
                ..Default::default()
            },
            AccessMask {
                index_read: true,
                ..Default::default()
            },
        )
    }
}

impl Asset for Mesh {
    type Error = MeshCreationError;

    fn try_from_bytes(
        assets: Arc<Assets>,
        graphics_context: Arc<GraphicsContext>,
        _path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, Self::Error> {
        let (document, buffers, _) = gltf::import_slice(&bytes).unwrap();
        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let mut sub_meshes = Vec::new();
        let mut min = Vec3::new(f32::MAX, f32::MAX, f32::MAX);
        let mut max = Vec3::new(f32::MIN, f32::MIN, f32::MIN);
        for mesh in document.meshes() {
            for primitive in mesh.primitives() {
                let reader = primitive.reader(|buffer| Some(&buffers[buffer.index()].0));
                let vertex_offset = vertices.len() as u32;
                let index_offset = indices.len();

                vertices.extend(
                    itertools::multizip((
                        reader
                            .read_positions()
                            .ok_or(MeshCreationError::MissingPositions)?,
                        reader
                            .read_normals()
                            .ok_or(MeshCreationError::MissingNormals)?,
                        reader
                            .read_tangents()
                            .ok_or(MeshCreationError::MissingTangents)?,
                        reader
                            .read_tex_coords(0)
                            .ok_or(MeshCreationError::MissingTexCoords)?
                            .into_f32(),
                    ))
                    .map(|(position, normal, tangent, tex_coord)| Vertex {
                        position: Vec3::from(position),
                        normal: Vec3::from(normal),
                        tangent: Vec4::from(tangent),
                        tex_coord: Vec2::from(tex_coord),
                    }),
                );

                indices.extend(
                    reader
                        .read_indices()
                        .ok_or(MeshCreationError::MissingIndices)?
                        .into_u32()
                        .map(|index| index + vertex_offset),
                );

                let material = primitive.material();
                let material_name = PathBuf::from_str(
                    material
                        .name()
                        .ok_or(MeshCreationError::MissingMaterialName)?,
                )
                .unwrap();

                min = min.min(&primitive.bounding_box().min.into());
                max = max.max(&primitive.bounding_box().max.into());
                sub_meshes.push(SubMesh {
                    bounding_box: primitive.bounding_box().into(),
                    material: assets
                        .read(graphics_context.clone(), material_name)
                        .unwrap_or_default(),
                    count: indices.len() - index_offset,
                    offset: index_offset,
                });
            }
        }

        let vertex_buffer = Self::create_vertex_buffer(&graphics_context, vertices.clone())?;
        let index_buffer = Self::create_index_buffer(&graphics_context, indices.clone())?;

        vertex_buffer.set_debug_utils_object_name("Mesh Vertex Buffer");
        index_buffer.set_debug_utils_object_name("Mesh Index Buffer");

        Ok(Self {
            bounding_box: BoundingBox::new(min, max),
            sub_meshes,
            index_buffer,
            vertex_buffer,
            indices,
            vertices,
        })
    }

    fn to_bytes(&self) -> Option<Vec<u8>> {
        let vertex_buffer = json::Buffer {
            byte_length: (self.vertices.len() * std::mem::size_of::<Vertex>()) as u32,
            name: None,
            uri: Some(
                "data:application/octet-stream;base64,".to_string()
                    + &base64::encode(Self::slice_as_bytes(&self.vertices)),
            ),
            extensions: None,
            extras: Default::default(),
        };
        let vertex_buffer_view = json::buffer::View {
            buffer: json::Index::new(0),
            byte_length: vertex_buffer.byte_length,
            byte_offset: None,
            byte_stride: Some(std::mem::size_of::<Vertex>() as _),
            name: None,
            target: Some(json::validation::Checked::Valid(
                json::buffer::Target::ArrayBuffer,
            )),
            extensions: None,
            extras: Default::default(),
        };
        let index_buffer = json::Buffer {
            byte_length: (self.indices.len() * std::mem::size_of::<u32>()) as u32,
            name: None,
            uri: Some(
                "data:application/octet-stream;base64,".to_string()
                    + &base64::encode(Self::slice_as_bytes(&self.indices)),
            ),
            extensions: None,
            extras: Default::default(),
        };
        let position = json::Accessor {
            buffer_view: Some(json::Index::new(0)),
            byte_offset: 0,
            count: self.vertices.len() as _,
            component_type: json::validation::Checked::Valid(json::accessor::GenericComponentType(
                json::accessor::ComponentType::F32,
            )),
            extensions: None,
            extras: Default::default(),
            type_: json::validation::Checked::Valid(json::accessor::Type::Vec3),
            min: Some(json::Value::from(self.bounding_box.min().deref())),
            max: Some(json::Value::from(self.bounding_box.max().deref())),
            name: None,
            normalized: false,
            sparse: None,
        };
        let normal = json::Accessor {
            buffer_view: Some(json::Index::new(0)),
            byte_offset: std::mem::size_of::<Vec3>() as _,
            count: self.vertices.len() as _,
            component_type: json::validation::Checked::Valid(json::accessor::GenericComponentType(
                json::accessor::ComponentType::F32,
            )),
            extensions: None,
            extras: Default::default(),
            type_: json::validation::Checked::Valid(json::accessor::Type::Vec3),
            min: None,
            max: None,
            name: None,
            normalized: true,
            sparse: None,
        };
        let tangent = json::Accessor {
            buffer_view: Some(json::Index::new(0)),
            byte_offset: (2 * std::mem::size_of::<Vec3>()) as _,
            count: self.vertices.len() as _,
            component_type: json::validation::Checked::Valid(json::accessor::GenericComponentType(
                json::accessor::ComponentType::F32,
            )),
            extensions: None,
            extras: Default::default(),
            type_: json::validation::Checked::Valid(json::accessor::Type::Vec4),
            min: None,
            max: None,
            name: None,
            normalized: true,
            sparse: None,
        };
        let tex_coord = json::Accessor {
            buffer_view: Some(json::Index::new(0)),
            byte_offset: (2 * std::mem::size_of::<Vec3>() + std::mem::size_of::<Vec4>()) as _,
            count: self.vertices.len() as _,
            component_type: json::validation::Checked::Valid(json::accessor::GenericComponentType(
                json::accessor::ComponentType::F32,
            )),
            extensions: None,
            extras: Default::default(),
            type_: json::validation::Checked::Valid(json::accessor::Type::Vec2),
            min: None,
            max: None,
            name: None,
            normalized: true,
            sparse: None,
        };
        let mut primitives = vec![];
        let mut materials = vec![];
        let mut indices_accessors = vec![];
        let mut indices_views = vec![];
        for sub_mesh in &self.sub_meshes {
            primitives.push(json::mesh::Primitive {
                attributes: {
                    let mut map = HashMap::new();
                    map.insert(
                        json::validation::Checked::Valid(json::mesh::Semantic::Positions),
                        json::Index::new(0),
                    );
                    map.insert(
                        json::validation::Checked::Valid(json::mesh::Semantic::Normals),
                        json::Index::new(1),
                    );
                    map.insert(
                        json::validation::Checked::Valid(json::mesh::Semantic::Tangents),
                        json::Index::new(2),
                    );
                    map.insert(
                        json::validation::Checked::Valid(json::mesh::Semantic::TexCoords(0)),
                        json::Index::new(3),
                    );
                    map
                },
                extensions: None,
                extras: Default::default(),
                indices: Some(json::Index::new((4 + indices_accessors.len()) as u32)),
                material: Some(json::Index::new(materials.len() as u32)),
                mode: Default::default(),
                targets: None,
            });
            materials.push(json::Material {
                alpha_cutoff: None,
                alpha_mode: Default::default(),
                double_sided: false,
                name: Some(
                    sub_mesh
                        .material
                        .path()
                        .map(|path| path.to_str().unwrap().to_string())
                        .unwrap_or_default(),
                ),
                pbr_metallic_roughness: Default::default(),
                normal_texture: None,
                occlusion_texture: None,
                emissive_texture: None,
                emissive_factor: Default::default(),
                extensions: None,
                extras: Default::default(),
            });
            indices_accessors.push(json::Accessor {
                buffer_view: Some(json::Index::new((1 + indices_views.len()) as u32)),
                byte_offset: 0,
                count: sub_mesh.count as u32,
                component_type: json::validation::Checked::Valid(
                    json::accessor::GenericComponentType(json::accessor::ComponentType::U32),
                ),
                extensions: None,
                extras: Default::default(),
                type_: json::validation::Checked::Valid(json::accessor::Type::Scalar),
                min: None,
                max: None,
                name: None,
                normalized: false,
                sparse: None,
            });
            indices_views.push(json::buffer::View {
                buffer: json::Index::new(1),
                byte_length: (sub_mesh.count * std::mem::size_of::<u32>()) as u32,
                byte_offset: Some((sub_mesh.offset * std::mem::size_of::<u32>()) as u32),
                byte_stride: Some(std::mem::size_of::<u32>() as _),
                name: None,
                target: Some(json::validation::Checked::Valid(
                    json::buffer::Target::ArrayBuffer,
                )),
                extensions: None,
                extras: Default::default(),
            });
        }
        let mesh = json::Mesh {
            extensions: None,
            extras: Default::default(),
            name: None,
            primitives,
            weights: None,
        };
        let node = json::Node {
            camera: None,
            children: None,
            extensions: None,
            extras: Default::default(),
            matrix: None,
            mesh: Some(json::Index::new(0)),
            name: None,
            rotation: None,
            scale: None,
            translation: None,
            skin: None,
            weights: None,
        };
        let root = json::Root {
            accessors: vec![
                vec![position, normal, tangent, tex_coord],
                indices_accessors,
            ]
            .into_iter()
            .flatten()
            .collect(),
            animations: vec![],
            asset: Default::default(),
            buffers: vec![vertex_buffer, index_buffer],
            buffer_views: vec![vec![vertex_buffer_view], indices_views]
                .into_iter()
                .flatten()
                .collect(),
            scene: None,
            extensions: None,
            extras: Default::default(),
            extensions_used: vec![],
            extensions_required: vec![],
            cameras: vec![],
            images: vec![],
            materials,
            meshes: vec![mesh],
            nodes: vec![node],
            samplers: vec![],
            scenes: vec![json::Scene {
                extensions: None,
                extras: Default::default(),
                name: None,
                nodes: vec![json::Index::new(0)],
            }],
            skins: vec![],
            textures: vec![],
        };
        json::serialize::to_vec(&root).ok()
    }
}

impl Mesh {
    fn slice_as_bytes<T>(data: &[T]) -> &[u8] {
        unsafe {
            std::slice::from_raw_parts(
                data.as_ptr() as *const u8,
                data.len() * std::mem::size_of::<T>(),
            )
        }
    }
}

/// A sub mesh.
#[derive(Clone, Debug)]
pub struct SubMesh {
    /// Bounding box.
    pub bounding_box: BoundingBox,
    /// Material.
    pub material: AssetRef<Material>,
    /// Index count.
    pub count: usize,
    /// Offset into the index buffer.
    pub offset: usize,
}

impl SubMesh {
    /// Returns the offset into the index buffer.
    pub fn offset(&self) -> usize {
        self.offset
    }

    /// Returns the vertex count.
    pub fn count(&self) -> usize {
        self.count
    }

    /// Returns the material.
    pub fn material(&self) -> &AssetRef<Material> {
        &self.material
    }

    /// Sets the material.
    pub fn set_material(&mut self, material: AssetRef<Material>) {
        self.material = material;
    }

    /// Returns the bounding box.
    pub fn bounding_box(&self) -> BoundingBox {
        self.bounding_box
    }
}

/// Errors which can occur when creating a [`Mesh`].
#[derive(Debug)]
pub enum MeshCreationError {
    /// Missing positions.
    MissingPositions,
    /// Missing normals.
    MissingNormals,
    /// Missing tangents.
    MissingTangents,
    /// Missing texture coords.
    MissingTexCoords,
    /// Missing indices.
    MissingIndices,
    /// Missing material name.
    MissingMaterialName,
    /// A graphics API operation has failed.
    GfxOperationFailed(GfxError),
}

impl Display for MeshCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MeshCreationError::MissingPositions => write!(f, "Missing positions"),
            MeshCreationError::MissingNormals => write!(f, "Missing normals"),
            MeshCreationError::MissingTangents => write!(f, "Missing tangents"),
            MeshCreationError::MissingTexCoords => write!(f, "Missing texture coords"),
            MeshCreationError::MissingIndices => write!(f, "Missing indices"),
            MeshCreationError::MissingMaterialName => write!(f, "Missing material name"),
            MeshCreationError::GfxOperationFailed(_) => {
                write!(f, "A graphics API operation has failed")
            }
        }
    }
}

impl std::error::Error for MeshCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            MeshCreationError::GfxOperationFailed(e) => Some(e),
            _ => None,
        }
    }
}

impl From<GfxError> for MeshCreationError {
    fn from(e: GfxError) -> Self {
        MeshCreationError::GfxOperationFailed(e)
    }
}

/// A static mesh vertex.
#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct Vertex {
    /// The position.
    pub position: Vec3,
    /// The normal.
    pub normal: Vec3,
    /// The tangent.
    pub tangent: Vec4,
    /// The texture coordinates.
    pub tex_coord: Vec2,
}

#[cfg(test)]
mod tests {
    use std::fs::read;
    use std::path::Path;

    use crate::asset::{Asset, Assets, FilesystemSource};
    use crate::gfx::{GfxError, GraphicsContext};
    use crate::rendering::{Mesh, MeshCreationError};

    #[test]
    fn mesh_try_from_asset() {
        let graphics_context = GraphicsContext::builder().build().unwrap();
        let assets = Assets::builder()
            .source(FilesystemSource::new("rsc/test/rendering").unwrap())
            .build();
        let mesh = Mesh::try_from_bytes(
            assets.clone(),
            graphics_context.clone(),
            Path::new("mesh.gltf"),
            read("rsc/test/rendering/mesh.gltf").unwrap(),
        )
        .unwrap();

        let sub_mesh = &mesh.sub_meshes()[0];
        let material = sub_mesh.material.unwrap();

        material.base_color_texture().unwrap();
        material.normal_texture().unwrap();
        material.metallic_roughness_texture().unwrap();

        assert_eq!(0, sub_mesh.offset());
        assert_eq!(36, sub_mesh.count());
        assert_eq!(
            "material.ron",
            sub_mesh.material.path().unwrap().to_str().unwrap()
        );

        graphics_context.device().wait().unwrap();
    }

    #[test]
    fn mesh_creation_error_from() {
        assert!(matches!(
            MeshCreationError::from(GfxError::DeviceLost),
            MeshCreationError::GfxOperationFailed(GfxError::DeviceLost)
        ));
    }
}
