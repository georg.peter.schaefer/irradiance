use ron::error::SpannedError;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::path::Path;
use std::string::FromUtf8Error;
use std::sync::Arc;

use serde::Serialize;

use crate::asset::{Asset, AssetRef, Assets};
use crate::gfx::GraphicsContext;
use crate::math::Vec4;
use crate::rendering::Texture;

/// Pbr material.
#[derive(Clone, Debug, Serialize)]
pub struct Material {
    metallic_roughness_texture: AssetRef<Texture>,
    normal_texture: AssetRef<Texture>,
    base_color_texture: AssetRef<Texture>,
    roughness: f32,
    metallic: f32,
    base_color: Vec4,
}

impl Material {
    /// Starts building a new material.
    pub fn builder() -> MaterialBuilder {
        MaterialBuilder {
            metallic_roughness_texture: Default::default(),
            normal_texture: Default::default(),
            base_color_texture: Default::default(),
            roughness: 1.0,
            metallic: 0.0,
            base_color: Vec4::new(0.8, 0.8, 0.8, 1.0),
        }
    }

    /// Returns the base color.
    pub fn base_color(&self) -> Vec4 {
        self.base_color
    }

    /// Sets the base color.
    pub fn set_base_color(&mut self, base_color: Vec4) {
        self.base_color = base_color;
    }

    /// Returns the metallic value.
    pub fn metallic(&self) -> f32 {
        self.metallic
    }

    /// Sets the metallic value.
    pub fn set_metallic(&mut self, metallic: f32) {
        self.metallic = metallic;
    }

    /// Returns the roughness value.
    pub fn roughness(&self) -> f32 {
        self.roughness
    }

    /// Sets the roughness value.
    pub fn set_roughness(&mut self, roughness: f32) {
        self.roughness = roughness;
    }

    /// Returns the base color texture.
    pub fn base_color_texture(&self) -> &AssetRef<Texture> {
        &self.base_color_texture
    }

    /// Sets the base color texture.
    pub fn set_base_color_texture(&mut self, base_color_texture: AssetRef<Texture>) {
        self.base_color_texture = base_color_texture;
    }

    /// Returns the normal color texture.
    pub fn normal_texture(&self) -> &AssetRef<Texture> {
        &self.normal_texture
    }

    /// Sets the normal texture.
    pub fn set_normal_texture(&mut self, normal_texture: AssetRef<Texture>) {
        self.normal_texture = normal_texture;
    }

    /// Returns the metallic roughness texture.
    pub fn metallic_roughness_texture(&self) -> &AssetRef<Texture> {
        &self.metallic_roughness_texture
    }

    /// Sets the metallic roughness texture.
    pub fn set_metallic_roughness_texture(
        &mut self,
        metallic_roughness_texture: AssetRef<Texture>,
    ) {
        self.metallic_roughness_texture = metallic_roughness_texture;
    }

    /// Determines the index of the pipeline to use for the given material.
    pub fn pipeline_index(&self) -> usize {
        let mut pipeline_index = 0;
        if self.base_color_texture.path().is_some() {
            pipeline_index |= 0b001;
        }
        if self.normal_texture.path().is_some() {
            pipeline_index |= 0b010;
        }
        if self.metallic_roughness_texture.path().is_some() {
            pipeline_index |= 0b100;
        }
        pipeline_index
    }
}

impl Default for Material {
    fn default() -> Self {
        Self {
            metallic_roughness_texture: Default::default(),
            normal_texture: Default::default(),
            base_color_texture: Default::default(),
            roughness: 0.4,
            metallic: 0.0,
            base_color: Vec4::new(0.8, 0.8, 0.8, 1.0),
        }
    }
}

impl Asset for Material {
    type Error = MaterialCreationError;

    fn try_from_bytes(
        assets: Arc<Assets>,
        graphics_context: Arc<GraphicsContext>,
        _path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, Self::Error> {
        let material: imp::Material = ron::from_str(&String::from_utf8(bytes)?)?;
        Ok(Self {
            metallic_roughness_texture: assets
                .read(
                    graphics_context.clone(),
                    material.metallic_roughness_texture,
                )
                .unwrap_or_default(),
            normal_texture: assets
                .read(graphics_context.clone(), material.normal_texture)
                .unwrap_or_default(),
            base_color_texture: assets
                .read(graphics_context, material.base_color_texture)
                .unwrap_or_default(),
            roughness: material.roughness,
            metallic: material.metallic,
            base_color: material.base_color,
        })
    }

    fn to_bytes(&self) -> Option<Vec<u8>> {
        ron::ser::to_string_pretty(self, Default::default())
            .ok()
            .map(|data| data.into_bytes())
    }
}

/// Type for building a material.
pub struct MaterialBuilder {
    metallic_roughness_texture: AssetRef<Texture>,
    normal_texture: AssetRef<Texture>,
    base_color_texture: AssetRef<Texture>,
    roughness: f32,
    metallic: f32,
    base_color: Vec4,
}

impl MaterialBuilder {
    /// Sets the base color.
    pub fn base_color(mut self, base_color: Vec4) -> Self {
        self.base_color = base_color;
        self
    }

    /// Sets the metallic value.
    pub fn metallic(mut self, metallic: f32) -> Self {
        self.metallic = metallic;
        self
    }

    /// Sets the roughness.
    pub fn roughness(mut self, roughness: f32) -> Self {
        self.roughness = roughness;
        self
    }

    /// Sets the base color texture.
    pub fn base_color_texture(mut self, base_color_texture: AssetRef<Texture>) -> Self {
        self.base_color_texture = base_color_texture;
        self
    }

    /// Sets the normal texture.
    pub fn normal_texture(mut self, normal_texture: AssetRef<Texture>) -> Self {
        self.normal_texture = normal_texture;
        self
    }

    /// Sets the metallic roughness texture.
    pub fn metallic_roughness_texture(
        mut self,
        metallic_roughness_texture: AssetRef<Texture>,
    ) -> Self {
        self.metallic_roughness_texture = metallic_roughness_texture;
        self
    }

    /// Builds the material.
    pub fn build(self) -> Material {
        Material {
            metallic_roughness_texture: self.metallic_roughness_texture,
            normal_texture: self.normal_texture,
            base_color_texture: self.base_color_texture,
            roughness: self.roughness,
            metallic: self.metallic,
            base_color: self.base_color,
        }
    }
}

/// Errors which can occur when creating a material.
#[derive(Debug)]
pub enum MaterialCreationError {
    /// Utf8 conversion has failed.
    Utf8ConversionFailed(FromUtf8Error),
    /// Material deserialization has failed.
    DeserializationFailed(SpannedError),
    /// Reading texture asset has failed.
    AssetReadFailed(std::io::Error),
}

impl Display for MaterialCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MaterialCreationError::Utf8ConversionFailed(_) => {
                write!(f, "Utf8 conversion has failed")
            }
            MaterialCreationError::DeserializationFailed(_) => {
                write!(f, "Material deserialization has failed")
            }
            MaterialCreationError::AssetReadFailed(_) => {
                write!(f, "Reading texture asset has failed")
            }
        }
    }
}

impl Error for MaterialCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            MaterialCreationError::Utf8ConversionFailed(e) => Some(e),
            MaterialCreationError::DeserializationFailed(e) => Some(e),
            MaterialCreationError::AssetReadFailed(e) => Some(e),
        }
    }
}

impl From<FromUtf8Error> for MaterialCreationError {
    fn from(e: FromUtf8Error) -> Self {
        MaterialCreationError::Utf8ConversionFailed(e)
    }
}

impl From<SpannedError> for MaterialCreationError {
    fn from(e: SpannedError) -> Self {
        MaterialCreationError::DeserializationFailed(e)
    }
}

impl From<std::io::Error> for MaterialCreationError {
    fn from(e: std::io::Error) -> Self {
        MaterialCreationError::AssetReadFailed(e)
    }
}

mod imp {
    use std::path::PathBuf;

    use serde::Deserialize;

    use crate::math::Vec4;

    #[derive(Deserialize, Debug)]
    pub struct Material {
        #[serde(default)]
        pub metallic_roughness_texture: PathBuf,
        #[serde(default)]
        pub normal_texture: PathBuf,
        #[serde(default)]
        pub base_color_texture: PathBuf,
        pub roughness: f32,
        pub metallic: f32,
        pub base_color: Vec4,
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read;
    use std::io::ErrorKind;
    use std::path::Path;

    use ron::de::{ErrorCode, Position};

    use crate::asset::{Asset, Assets, FilesystemSource};
    use crate::gfx::GraphicsContext;
    use crate::math::Vec4;
    use crate::rendering::{Material, MaterialCreationError};

    #[test]
    fn material_try_from_bytes() {
        let graphics_context = GraphicsContext::builder().build().unwrap();
        let assets = Assets::builder()
            .source(FilesystemSource::new("rsc/test/rendering").unwrap())
            .build();
        let material = Material::try_from_bytes(
            assets.clone(),
            graphics_context.clone(),
            Path::new("material.ron"),
            read("rsc/test/rendering/material.ron").unwrap(),
        )
        .unwrap();

        material.base_color_texture().unwrap();
        material.normal_texture().unwrap();
        material.metallic_roughness_texture().unwrap();

        assert_relative_eq!(Vec4::new(0.5, 0.0, 0.25, 1.0), material.base_color());
        assert_relative_eq!(0.0, material.metallic());
        assert_relative_eq!(0.8, material.roughness());
        assert_eq!(
            "texture.png",
            material
                .base_color_texture()
                .path()
                .unwrap()
                .to_str()
                .unwrap()
        );
        assert_eq!(
            "texture.png",
            material.normal_texture().path().unwrap().to_str().unwrap()
        );
        assert_eq!(
            "texture.png",
            material
                .metallic_roughness_texture()
                .path()
                .unwrap()
                .to_str()
                .unwrap()
        );

        graphics_context.device().wait().unwrap();
    }

    #[test]
    fn material_creation_error_from() {
        let expected = String::from_utf8(vec![0, 159]).unwrap_err();
        assert!(matches!(
            MaterialCreationError::from(expected.clone()),
            MaterialCreationError::Utf8ConversionFailed(actual) if actual == expected
        ));
        assert!(matches!(
            MaterialCreationError::from(ron::Error {
                code: ErrorCode::Eof,
                position: Position { line: 42, col: 19 }
            }),
            MaterialCreationError::DeserializationFailed(ron::Error {
                code: ErrorCode::Eof,
                position: Position { line: 42, col: 19 }
            })
        ));
        assert!(matches!(
            MaterialCreationError::from(std::io::Error::from(ErrorKind::OutOfMemory)),
            MaterialCreationError::AssetReadFailed(actual) if actual.kind() == ErrorKind::OutOfMemory
        ));
    }
}
