use serde::Serialize;

use crate::ecs::Active;
use crate::math::Mat4;
use crate::Component;

/// A camera with perspective projection.
#[derive(Copy, Clone, Debug, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct Camera {
    fov_y: f32,
    near: f32,
    far: f32,
}

impl Camera {
    /// Creates a new camera.
    pub fn new(fov_y: f32, near: f32, far: f32) -> Self {
        Self { fov_y, near, far }
    }

    /// Returns the field of view y.
    pub fn fov_y(&self) -> f32 {
        self.fov_y
    }

    /// Sets the field of view y.
    pub fn set_fov_y(&mut self, fov_y: f32) {
        self.fov_y = fov_y
    }

    /// Returns the near plane.
    pub fn near(&self) -> f32 {
        self.near
    }

    /// Sets the near plane.
    pub fn set_near(&mut self, near: f32) {
        self.near = near;
    }

    /// Returns the far plane.
    pub fn far(&self) -> f32 {
        self.far
    }

    /// Sets the far plane.
    pub fn set_far(&mut self, far: f32) {
        self.far = far;
    }

    /// Returns the projection matrix.
    pub fn projection(&self, aspect_ratio: f32) -> Mat4 {
        Mat4::perspective(self.fov_y, aspect_ratio, self.near, self.far)
    }
}

impl Default for Camera {
    fn default() -> Self {
        Self {
            fov_y: 45f32.to_radians(),
            near: 0.1,
            far: 100.0,
        }
    }
}

impl Active for Camera {}
