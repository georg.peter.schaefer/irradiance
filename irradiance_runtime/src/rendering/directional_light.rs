use serde::Serialize;

use crate::math::Vec3;
use crate::Component;

/// Directional light component.
#[derive(Copy, Clone, Debug, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct DirectionalLight {
    color: Vec3,
    intensity: f32,
}

impl DirectionalLight {
    /// Creates a new directional light component.
    pub fn new(color: Vec3, intensity: f32) -> Self {
        Self { color, intensity }
    }

    /// Returns the color.
    pub fn color(&self) -> Vec3 {
        self.color
    }

    /// Sets the color.
    pub fn set_color(&mut self, color: Vec3) {
        self.color = color;
    }

    /// Returns the intensity.
    pub fn intensity(&self) -> f32 {
        self.intensity
    }

    /// Sets the intensity.
    pub fn set_intensity(&mut self, intensity: f32) {
        self.intensity = intensity;
    }
}

impl Default for DirectionalLight {
    fn default() -> Self {
        Self {
            color: Vec3::new(1.0, 1.0, 1.0),
            intensity: 2.0,
        }
    }
}
