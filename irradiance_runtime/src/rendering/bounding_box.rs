use crate::math::Vec3;

/// Bounding box.
#[derive(Copy, Clone, Debug)]
pub struct BoundingBox {
    min: Vec3,
    max: Vec3,
}

impl BoundingBox {
    /// Creates a new bounding box.
    pub fn new(min: Vec3, max: Vec3) -> Self {
        Self { min, max }
    }

    /// Returns the minimum bounds.
    pub fn min(&self) -> Vec3 {
        self.min
    }

    /// Returns the maximum bounds.
    pub fn max(&self) -> Vec3 {
        self.max
    }
}

impl From<gltf::mesh::BoundingBox> for BoundingBox {
    fn from(bounding_box: gltf::mesh::BoundingBox) -> Self {
        Self {
            min: bounding_box.min.into(),
            max: bounding_box.max.into(),
        }
    }
}
