use std::path::Path;
use std::sync::Arc;

use crate::asset::{Asset, Assets};
use crate::ecs::{Entities, Transform};
use crate::gfx::command_buffer::{CommandBufferBuilder, IndexType};
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{Format, ImageAspects, ImageLayout, ImageSubresourceRange};
use crate::gfx::pipeline::{
    ColorBlendAttachmentState, CompareOp, CullMode, DescriptorSet, DescriptorSetLayout,
    DescriptorType, DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
    PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
    WriteDescriptorSetImages,
};
use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use crate::math::{Mat4, Vec4};
use crate::rendering::mesh::Vertex;
use crate::rendering::pbr::sky_box_pass::shaders::PushConstants;
use crate::rendering::pbr::{GBuffer, LightProbe};
use crate::rendering::{Camera, Mesh};

/// SkyBox pass.
#[derive(Debug)]
pub struct SkyBoxPass {
    cube: Mesh,
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl SkyBoxPass {
    /// Creates a new sky box pass.
    pub fn new(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
    ) -> Result<Self, GfxError> {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(graphics_context.device().clone())?;
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            descriptor_set_layout.clone(),
        )?;
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            pipeline_layout.clone(),
        )?;
        let cube = Mesh::try_from_bytes(
            assets,
            graphics_context.clone(),
            Path::new("__internal__.gltf"),
            include_bytes!("../../../rsc/rendering/pbr/cube.gltf").to_vec(),
        )
        .expect("cube");

        descriptor_set_layout.set_debug_utils_object_name("Sky Box Pass Descriptor Set Layout");
        pipeline_layout.set_debug_utils_object_name("Sky Box Pass Pipeline Layout");
        graphics_pipeline.set_debug_utils_object_name("Sky Box Pass Graphics Pipeline");

        Ok(Self {
            cube,
            graphics_pipeline,
            pipeline_layout,
            descriptor_set_layout,
            graphics_context,
        })
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        g_buffer: &GBuffer,
        light_probe: &LightProbe,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Sky Box Pass", Vec4::new(0.8, 0.5, 0.0, 1.0));

        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.descriptor_set_layout)?,
            self.descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Sky Box Pass Descriptor Set Layout");
        descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::CombinedImageSampler)
            .sampled_image(
                light_probe.sampler(),
                light_probe.environment_map(),
                ImageLayout::ShaderReadOnlyOptimal,
            )
            .build()]);

        builder
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(g_buffer.base_color().image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(g_buffer.normal().image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(g_buffer.depth_buffer().image().clone())
                            .src_stage_mask(PipelineStages {
                                early_fragment_tests: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                depth_stencil_attachment_read: true,
                                depth_stencil_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                early_fragment_tests: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                depth_stencil_attachment_read: true,
                                depth_stencil_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::DepthAttachmentOptimal)
                            .new_layout(ImageLayout::DepthAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        depth: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .bind_graphics_pipeline(self.graphics_pipeline.clone())
            .begin_rendering(self.create_render_pass(g_buffer))
            .set_viewport(
                0,
                vec![Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: g_buffer.extent().width as _,
                    height: g_buffer.extent().height as _,
                    min_depth: 0.0,
                    max_depth: 1.0,
                }],
            )
            .set_scissor(
                0,
                vec![Rect2D {
                    offset: Default::default(),
                    extent: g_buffer.extent(),
                }],
            )
            .bind_descriptor_sets(
                PipelineBindPoint::Graphics,
                self.pipeline_layout.clone(),
                0,
                vec![descriptor_set],
            )
            .bind_vertex_buffer(self.cube.vertex_buffer().clone())
            .bind_index_buffer(self.cube.index_buffer().clone(), 0, IndexType::UInt32);

        if let Some((Some(camera), Some(camera_transform))) = entities
            .active::<Camera>()
            .map(|camera| (camera.get::<Camera>(), camera.get::<Transform>()))
        {
            builder.push_constants(
                self.pipeline_layout.clone(),
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                PushConstants {
                    projection: camera.projection(
                        g_buffer.extent().width as f32 / g_buffer.extent().height as f32,
                    ),
                    view: Mat4::from(*camera_transform).inverse(),
                },
            );

            for sub_mesh in self.cube.sub_meshes() {
                builder.draw_indexed(sub_mesh.count() as _, 1, sub_mesh.offset() as _, 0, 0);
            }
        }

        builder.end_rendering().end_label();

        Ok(())
    }
}

impl SkyBoxPass {
    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .build(),
            )
            .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::LessOrEqual)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R8G8B8A8Srgb)
            .color_attachment_format(Format::R16G16B16A16SFloat)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    fn create_render_pass(&self, g_buffer: &GBuffer) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: g_buffer.extent(),
            })
            .color_attachment(
                Attachment::builder(g_buffer.base_color().clone())
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .color_attachment(
                Attachment::builder(g_buffer.normal().clone())
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .depth_attachment(
                Attachment::builder(g_buffer.depth_buffer().clone())
                    .image_layout(ImageLayout::DepthAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .build()
    }
}

mod shaders {
    use crate::math::Mat4;

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub projection: Mat4,
        pub view: Mat4,
    }
}

mod vertex_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/sky_box.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod fragment_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/sky_box.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
