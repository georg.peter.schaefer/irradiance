// ACES Hill

[[vk::binding(0, 0)]]
Texture2D light_buffer_texture;
[[vk::binding(0, 0)]]
SamplerState light_buffer_sampler;
[[vk::binding(1, 0)]]
Texture2D bloom_buffer_texture;
[[vk::binding(1, 0)]]
SamplerState bloom_buffer_sampler;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

static const float3x3 ACES_INPUT_MAT =
{
    {0.59719, 0.35458, 0.04823},
    {0.07600, 0.90834, 0.01566},
    {0.02840, 0.13383, 0.83777}
};

static const float3x3 ACES_OUTPUT_MAT =
{
    { 1.60475, -0.53108, -0.07367},
    {-0.10208,  1.10813, -0.00605},
    {-0.00327, -0.07276,  1.07602}
};

float3 rrt_and_odt_fit(float3 v)
{
    float3 a = v * (v + 0.0245786f) - 0.000090537f;
    float3 b = v * (0.983729f * v + 0.4329510f) + 0.238081f;
    return a / b;
}

float4 main(VSOutput input) : SV_TARGET {
    float3 color = light_buffer_texture.Sample(light_buffer_sampler, input.tex_coord).rgb;
    float3 bloom = bloom_buffer_texture.Sample(bloom_buffer_sampler, input.tex_coord).rgb;
    color = lerp(color, bloom, 0.01);

    color = color * ACES_INPUT_MAT;
    color = rrt_and_odt_fit(color);
    color = color * ACES_OUTPUT_MAT;
    color = saturate(color);

    return float4(color, 1.0);
}
