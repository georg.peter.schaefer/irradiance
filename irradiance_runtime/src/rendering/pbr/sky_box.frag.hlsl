#include "../tangent_space.hlsl"

[[vk::binding(0, 0)]]
TextureCube sky_box_texture;
[[vk::binding(0, 0)]]
SamplerState sky_box_sampler;

struct VSOutput {
    [[vk::location(0)]] float3 world_position : POSITION0;
};

struct FSOutput {
    float3 base_color : SV_TARGET0;
    float3 normal : SV_TARGET1;
};

FSOutput main(VSOutput input) {
    FSOutput output = (FSOutput)0;

    output.base_color = sky_box_texture.Sample(sky_box_sampler, input.world_position).rgb;
    output.normal = float3(0.0);

    return output;
}
