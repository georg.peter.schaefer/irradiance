use std::sync::Arc;

use crate::ecs::{Entities, Transform};
use irradiance_runtime::gfx::pipeline::DescriptorSetLayout;
use irradiance_runtime::gfx::GraphicsContext;

use crate::gfx::buffer::Buffer;
use crate::gfx::command_buffer::{CommandBufferBuilder, IndexType};
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{Format, ImageAspects, ImageLayout, ImageSubresourceRange, ImageView};
use crate::gfx::pipeline::{
    ColorBlendAttachmentState, CompareOp, CullMode, DescriptorSet, DescriptorType, DynamicState,
    FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout, PrimitiveTopology,
    ShaderStages, VertexInput, VertexInputRate, Viewport, WriteDescriptorSetBuffers,
};
use crate::gfx::render_pass::{
    Attachment, AttachmentLoadOp, AttachmentStoreOp, ClearValue, RenderPass,
};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::{Extent2D, GfxError, Rect2D};
use crate::math::{Mat4, Vec3, Vec4};
use crate::rendering::pbr::omnidirectional_shadow_pass::shaders::PushConstants;
use crate::rendering::{
    mesh, skinned_mesh, AnimatedMesh, OmnidirectionalShadow, PointLight, StaticMesh,
};

/// Omnidirectional shadow pass.
#[derive(Debug)]
pub struct OmnidirectionalShadowPass {
    animated_graphics_pipeline: Arc<GraphicsPipeline>,
    static_graphics_pipeline: Arc<GraphicsPipeline>,
    animated_pipeline_layout: Arc<PipelineLayout>,
    static_pipeline_layout: Arc<PipelineLayout>,
    joint_descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl OmnidirectionalShadowPass {
    /// Creates a new omnidirectional shadow pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let joint_descriptor_set_layout =
            Self::create_joint_descriptor_set_layout(graphics_context.device().clone())?;
        let static_pipeline_layout =
            Self::create_static_pipeline_layout(graphics_context.device().clone())?;
        let animated_pipeline_layout = Self::create_animated_pipeline_layout(
            graphics_context.device().clone(),
            joint_descriptor_set_layout.clone(),
        )?;
        let static_graphics_pipeline = Self::create_static_graphics_pipeline(
            graphics_context.device().clone(),
            static_pipeline_layout.clone(),
        )?;
        let animated_graphics_pipeline = Self::create_animated_graphics_pipeline(
            graphics_context.device().clone(),
            animated_pipeline_layout.clone(),
        )?;

        joint_descriptor_set_layout
            .set_debug_utils_object_name("Omnidirectional Shadow Pass Joint Descriptor Set Layout");
        static_pipeline_layout
            .set_debug_utils_object_name("Omnidirectional Shadow Pass Static Pipeline Layout");
        animated_pipeline_layout
            .set_debug_utils_object_name("Omnidirectional Shadow Pass Animated Pipeline Layout");
        static_graphics_pipeline
            .set_debug_utils_object_name("Omnidirectional Shadow Pass Static Graphics Pipeline");
        animated_graphics_pipeline
            .set_debug_utils_object_name("Omnidirectional Shadow Pass Animated Graphics Pipeline");

        Ok(Self {
            graphics_context,
            joint_descriptor_set_layout,
            static_pipeline_layout,
            animated_pipeline_layout,
            static_graphics_pipeline,
            animated_graphics_pipeline,
        })
    }

    fn create_joint_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::StorageBuffer,
                1,
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_static_pipeline_layout(device: Arc<Device>) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_animated_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_static_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .build(),
            )
            .vertex_shader(static_vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 1024.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 1024,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::LessOrEqual)
            .depth_clamp_enabled()
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R32SFloat)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    fn create_animated_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<skinned_mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .attribute(1, 0, Format::R32G32B32A32SFloat, 48)
                    .attribute(2, 0, Format::R32G32B32A32SFloat, 64)
                    .build(),
            )
            .vertex_shader(
                animated_vertex_shader::Shader::load(device.clone())?,
                "main",
            )
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 1024.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 1024,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::LessOrEqual)
            .depth_clamp_enabled()
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R32SFloat)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Omnidirectional Shadow Pass", Vec4::new(0.7, 0.3, 0.0, 1.0));

        for (_, point_light, omnidirectional_shadow, point_light_transform) in
            entities.components::<(&PointLight, &OmnidirectionalShadow, &Transform)>()
        {
            builder.begin_label("Omnidirectional Shadow", Vec4::new(0.3, 0.5, 0.0, 1.0));

            for (index, face) in omnidirectional_shadow.faces().iter().enumerate() {
                builder.begin_label("Omnidirectional Shadow Face", Vec4::new(0.8, 0.2, 0.0, 1.0));

                builder
                    .pipeline_barrier(
                        PipelineBarrier::builder()
                            .image_memory_barrier(
                                ImageMemoryBarrier::builder(face.image().clone())
                                    .src_stage_mask(PipelineStages {
                                        fragment_shader: true,
                                        ..Default::default()
                                    })
                                    .src_access_mask(AccessMask {
                                        shader_sampled_read: true,
                                        ..Default::default()
                                    })
                                    .dst_stage_mask(PipelineStages {
                                        color_attachment_output: true,
                                        ..Default::default()
                                    })
                                    .dst_access_mask(AccessMask {
                                        color_attachment_write: true,
                                        ..Default::default()
                                    })
                                    .old_layout(ImageLayout::Undefined)
                                    .new_layout(ImageLayout::ColorAttachmentOptimal)
                                    .subresource_range(
                                        ImageSubresourceRange::builder()
                                            .aspect_mask(ImageAspects {
                                                color: true,
                                                ..Default::default()
                                            })
                                            .base_array_layer(index as _)
                                            .build(),
                                    )
                                    .build(),
                            )
                            .image_memory_barrier(
                                ImageMemoryBarrier::builder(
                                    omnidirectional_shadow.depth_buffer().image().clone(),
                                )
                                .src_stage_mask(PipelineStages {
                                    early_fragment_tests: true,
                                    ..Default::default()
                                })
                                .src_access_mask(AccessMask {
                                    depth_stencil_attachment_write: true,
                                    ..Default::default()
                                })
                                .dst_stage_mask(PipelineStages {
                                    early_fragment_tests: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    depth_stencil_attachment_write: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::Undefined)
                                .new_layout(ImageLayout::DepthAttachmentOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            depth: true,
                                            ..Default::default()
                                        })
                                        .build(),
                                )
                                .build(),
                            )
                            .build(),
                    )
                    .bind_graphics_pipeline(self.static_graphics_pipeline.clone())
                    .begin_rendering(self.create_render_pass(
                        face.clone(),
                        omnidirectional_shadow.depth_buffer().clone(),
                    ))
                    .set_viewport(
                        0,
                        vec![Viewport {
                            x: 0.0,
                            y: 0.0,
                            width: omnidirectional_shadow.image_view().image().extent().width as _,
                            height: omnidirectional_shadow.image_view().image().extent().height
                                as _,
                            min_depth: 0.0,
                            max_depth: 1.0,
                        }],
                    )
                    .set_scissor(
                        0,
                        vec![Rect2D {
                            offset: Default::default(),
                            extent: omnidirectional_shadow.image_view().image().extent().into(),
                        }],
                    );

                for (_, static_mesh, transform) in
                    entities.components::<(&StaticMesh, &Transform)>()
                {
                    builder.push_constants(
                        self.static_pipeline_layout.clone(),
                        ShaderStages {
                            vertex: true,
                            ..Default::default()
                        },
                        0,
                        PushConstants {
                            projection: omnidirectional_shadow.projection(point_light.intensity()),
                            view: Self::view(index, point_light_transform.position()),
                            model: Mat4::from(*transform),
                            light_position: point_light_transform.position(),
                        },
                    );

                    if let Some(mesh) = static_mesh.mesh().ok() {
                        builder
                            .bind_vertex_buffer(mesh.vertex_buffer().clone())
                            .bind_index_buffer(mesh.index_buffer().clone(), 0, IndexType::UInt32);

                        for sub_mesh in mesh.sub_meshes() {
                            builder.draw_indexed(
                                sub_mesh.count() as _,
                                1,
                                sub_mesh.offset() as _,
                                0,
                                0,
                            );
                        }
                    }
                }

                builder.bind_graphics_pipeline(self.animated_graphics_pipeline.clone());

                for (_, animated_mesh, transform) in
                    entities.components::<(&AnimatedMesh, &Transform)>()
                {
                    builder.push_constants(
                        self.animated_pipeline_layout.clone(),
                        ShaderStages {
                            vertex: true,
                            ..Default::default()
                        },
                        0,
                        PushConstants {
                            projection: omnidirectional_shadow.projection(point_light.intensity()),
                            view: Self::view(index, point_light_transform.position()),
                            model: Mat4::from(*transform),
                            light_position: point_light_transform.position(),
                        },
                    );

                    if let Some(joint_buffer) = animated_mesh.joint_buffer() {
                        if let Some(mesh) = animated_mesh.mesh().ok() {
                            let joint_descriptor_set =
                                self.create_joint_descriptor_set(joint_buffer)?;
                            builder
                                .bind_vertex_buffer(mesh.vertex_buffer().clone())
                                .bind_index_buffer(
                                    mesh.index_buffer().clone(),
                                    0,
                                    IndexType::UInt32,
                                )
                                .bind_descriptor_sets(
                                    PipelineBindPoint::Graphics,
                                    self.animated_pipeline_layout.clone(),
                                    0,
                                    vec![joint_descriptor_set],
                                );

                            for sub_mesh in mesh.sub_meshes() {
                                builder.draw_indexed(
                                    sub_mesh.count() as _,
                                    1,
                                    sub_mesh.offset() as _,
                                    0,
                                    0,
                                );
                            }
                        }
                    }
                }

                builder.end_rendering().end_label();
            }

            builder.pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(
                            omnidirectional_shadow.image_view().image().clone(),
                        )
                        .src_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .dst_stage_mask(PipelineStages {
                            fragment_shader: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            shader_sampled_read: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::ColorAttachmentOptimal)
                        .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .layer_count(6)
                                .build(),
                        )
                        .build(),
                    )
                    .build(),
            );

            builder.end_label();
        }

        builder.end_label();

        Ok(())
    }

    fn create_render_pass(&self, face: Arc<ImageView>, depth_buffer: Arc<ImageView>) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: face.image().extent().into(),
            })
            .depth_attachment(
                Attachment::builder(depth_buffer)
                    .image_layout(ImageLayout::DepthAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Depth(1.0))
                    .build(),
            )
            .color_attachment(
                Attachment::builder(face)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Float([0.0, 0.0, 0.0, 0.0]))
                    .build(),
            )
            .build()
    }

    fn view(index: usize, position: Vec3) -> Mat4 {
        match index {
            0 => Mat4::look_at(
                position,
                position + Vec3::new(1.0, 0.0, 0.0),
                Vec3::new(0.0, -1.0, 0.0),
            ),
            1 => Mat4::look_at(
                position,
                position + Vec3::new(-1.0, 0.0, 0.0),
                Vec3::new(0.0, -1.0, 0.0),
            ),
            2 => Mat4::look_at(
                position,
                position + Vec3::new(0.0, -1.0, 0.0),
                Vec3::new(0.0, 0.0, -1.0),
            ),
            3 => Mat4::look_at(
                position,
                position + Vec3::new(0.0, 1.0, 0.0),
                Vec3::new(0.0, 0.0, 1.0),
            ),
            4 => Mat4::look_at(
                position,
                position + Vec3::new(0.0, 0.0, 1.0),
                Vec3::new(0.0, -1.0, 0.0),
            ),
            5 => Mat4::look_at(
                position,
                position + Vec3::new(0.0, 0.0, -1.0),
                Vec3::new(0.0, -1.0, 0.0),
            ),
            _ => Default::default(),
        }
    }

    fn create_joint_descriptor_set(
        &self,
        joint_buffer: Arc<Buffer>,
    ) -> Result<Arc<DescriptorSet>, GfxError> {
        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.joint_descriptor_set_layout)?,
            self.joint_descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set
            .set_debug_utils_object_name("Omnidirectional Shadow Pass Animated Descriptor Set");

        descriptor_set.write_buffers(vec![WriteDescriptorSetBuffers::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::StorageBuffer)
            .buffer(&joint_buffer, 0, ash::vk::WHOLE_SIZE)
            .build()]);

        Ok(descriptor_set)
    }
}

mod shaders {
    use crate::math::{Mat4, Vec3};

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub projection: Mat4,
        pub view: Mat4,
        pub model: Mat4,
        pub light_position: Vec3,
    }
}

mod static_vertex_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/static_omnidirectional_shadow.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod animated_vertex_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/animated_omnidirectional_shadow.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod fragment_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/omnidirectional_shadow.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
