use std::sync::Arc;

use crate::gfx::buffer::Buffer;
use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{
    Format, ImageAspects, ImageLayout, ImageSubresourceRange, ImageView, Sampler,
};
use crate::gfx::pipeline::{
    ColorBlendAttachmentState, CullMode, DescriptorSet, DescriptorSetLayout, DescriptorType,
    DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
    PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
    WriteDescriptorSetImages,
};
use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use crate::math::Vec4;
use crate::rendering::fullscreen_pass::{create_fullscreen_vertex_buffer, vertex_shader, Vertex};
use crate::rendering::pbr::BloomBuffer;

/// Final render pass.
#[derive(Debug)]
pub struct FinalPass {
    sampler: Arc<Sampler>,
    fullscreen_vertex_buffer: Arc<Buffer>,
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl FinalPass {
    /// Creates a new directional light pass.
    pub fn new(graphics_context: Arc<GraphicsContext>, format: Format) -> Result<Self, GfxError> {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(graphics_context.device().clone())?;
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            descriptor_set_layout.clone(),
        )?;
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            format,
            pipeline_layout.clone(),
        )?;
        let fullscreen_vertex_buffer = create_fullscreen_vertex_buffer(&graphics_context)?;
        let sampler = Sampler::builder(graphics_context.device().clone()).build()?;

        descriptor_set_layout.set_debug_utils_object_name("Final Pass Descriptor Set Layout");
        pipeline_layout.set_debug_utils_object_name("Final Pass Descriptor Set Layout");
        graphics_pipeline.set_debug_utils_object_name("Final Pass Graphics Pipeline");
        fullscreen_vertex_buffer.set_debug_utils_object_name("Final Pass Fullscreen Vertex Buffer");
        sampler.set_debug_utils_object_name("Final Pass Sampler");

        Ok(Self {
            sampler,
            fullscreen_vertex_buffer,
            graphics_pipeline,
            pipeline_layout,
            descriptor_set_layout,
            graphics_context,
        })
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        light_buffer: Arc<ImageView>,
        bloom_buffer: &BloomBuffer,
        final_image: Arc<ImageView>,
    ) -> Result<(), GfxError> {
        builder.begin_label("Final Pass", Vec4::new(0.33, 0.66, 0.0, 1.0));

        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.descriptor_set_layout)?,
            self.descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Final Pass Descriptor Set");

        descriptor_set.write_images(vec![
            WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &self.sampler,
                    &light_buffer,
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build(),
            WriteDescriptorSetImages::builder()
                .dst_binding(1)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &self.sampler,
                    &bloom_buffer.views[0],
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build(),
        ]);

        builder
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(final_image.image().clone())
                            .src_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .bind_graphics_pipeline(self.graphics_pipeline.clone())
            .begin_rendering(self.create_render_pass(final_image.clone()))
            .set_viewport(
                0,
                vec![Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: final_image.image().extent().width as _,
                    height: final_image.image().extent().height as _,
                    min_depth: 0.0,
                    max_depth: 1.0,
                }],
            )
            .set_scissor(
                0,
                vec![Rect2D {
                    offset: Default::default(),
                    extent: final_image.image().extent().into(),
                }],
            )
            .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
            .bind_descriptor_sets(
                PipelineBindPoint::Graphics,
                self.pipeline_layout.clone(),
                0,
                vec![descriptor_set],
            )
            .draw(6, 1, 0, 0)
            .end_rendering();

        builder.end_label();

        Ok(())
    }
}

impl FinalPass {
    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                1,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .build()
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        format: Format,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32SFloat, 0)
                    .attribute(1, 0, Format::R32G32SFloat, 8)
                    .build(),
            )
            .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 768.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(format)
            .build()
    }

    fn create_render_pass(&self, final_image: Arc<ImageView>) -> RenderPass {
        let image_extent = final_image.image().extent();
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: image_extent.into(),
            })
            .color_attachment(
                Attachment::builder(final_image)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::None)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .build()
    }
}

mod fragment_shader {
    use crate::shader;

    shader! {
        path: "src/rendering/pbr/final.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
