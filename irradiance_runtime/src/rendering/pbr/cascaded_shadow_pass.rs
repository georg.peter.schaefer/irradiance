use std::sync::Arc;

use crate::ecs::{Entities, Facade, Transform};
use crate::gfx::buffer::Buffer;
use crate::gfx::command_buffer::{CommandBufferBuilder, IndexType};
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{Format, ImageAspects, ImageLayout, ImageSubresourceRange, ImageView};
use crate::gfx::pipeline::{
    CompareOp, CullMode, DescriptorSet, DescriptorSetLayout, DescriptorType, DynamicState,
    FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout, PrimitiveTopology,
    ShaderStages, VertexInput, VertexInputRate, Viewport, WriteDescriptorSetBuffers,
};
use crate::gfx::render_pass::{
    Attachment, AttachmentLoadOp, AttachmentStoreOp, ClearValue, RenderPass,
};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use crate::math::{Mat4, Vec3, Vec4};
use crate::rendering::pbr::cascaded_shadow_pass::shaders::PushConstants;
use crate::rendering::{
    mesh, skinned_mesh, AnimatedMesh, Camera, CascadedShadow, DirectionalLight, StaticMesh,
};

/// Cascaded shadow pass.
#[derive(Debug)]
pub struct CascadedShadowPass {
    animated_graphics_pipeline: Arc<GraphicsPipeline>,
    static_graphics_pipeline: Arc<GraphicsPipeline>,
    animated_pipeline_layout: Arc<PipelineLayout>,
    static_pipeline_layout: Arc<PipelineLayout>,
    joint_descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl CascadedShadowPass {
    /// Creates a new cascaded shadow pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let joint_descriptor_set_layout =
            Self::create_joint_descriptor_set_layout(graphics_context.device().clone())?;
        let static_pipeline_layout =
            Self::create_static_pipeline_layout(graphics_context.device().clone())?;
        let animated_pipeline_layout = Self::create_animated_pipeline_layout(
            graphics_context.device().clone(),
            joint_descriptor_set_layout.clone(),
        )?;
        let static_graphics_pipeline = Self::create_static_graphics_pipeline(
            graphics_context.device().clone(),
            static_pipeline_layout.clone(),
        )?;
        let animated_graphics_pipeline = Self::create_animated_graphics_pipeline(
            graphics_context.device().clone(),
            animated_pipeline_layout.clone(),
        )?;

        joint_descriptor_set_layout
            .set_debug_utils_object_name("Cascaded Shadow Pass Joint Descriptor Set Layout");
        static_pipeline_layout
            .set_debug_utils_object_name("Cascaded Shadow Pass Static Pipeline Layout");
        animated_pipeline_layout
            .set_debug_utils_object_name("Cascaded Shadow Pass Animated Pipeline Layout");
        static_graphics_pipeline
            .set_debug_utils_object_name("Cascaded Shadow Pass Static Graphics Pipeline");
        animated_graphics_pipeline
            .set_debug_utils_object_name("Cascaded Shadow Pass Animated Graphics Pipeline");

        Ok(Self {
            graphics_context,
            joint_descriptor_set_layout,
            static_pipeline_layout,
            animated_pipeline_layout,
            static_graphics_pipeline,
            animated_graphics_pipeline,
        })
    }

    fn create_joint_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::StorageBuffer,
                1,
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_static_pipeline_layout(device: Arc<Device>) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_animated_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_static_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .build(),
            )
            .vertex_shader(static_vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 1024.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 1024,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::LessOrEqual)
            .depth_clamp_enabled()
            .depth_bias(10.25, 10.75)
            .layout(pipeline_layout)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    fn create_animated_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<skinned_mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .attribute(1, 0, Format::R32G32B32A32SFloat, 48)
                    .attribute(2, 0, Format::R32G32B32A32SFloat, 64)
                    .build(),
            )
            .vertex_shader(
                animated_vertex_shader::Shader::load(device.clone())?,
                "main",
            )
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 1024.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 1024,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::LessOrEqual)
            .depth_clamp_enabled()
            .depth_bias(10.25, 10.75)
            .layout(pipeline_layout)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        final_image: &ImageView,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Cascaded Shadow Pass", Vec4::new(1.0, 0.2, 0.0, 1.0));

        if let Some(camera) = entities.active::<Camera>() {
            for (directional_light, _, _, _) in
                entities.components::<(&DirectionalLight, &CascadedShadow, &Transform)>()
            {
                self.update_cascades(final_image, &camera, &directional_light);
            }
            unsafe {
                entities.commit();
            }

            for (_, _, cascaded_shadow, _) in
                entities.components::<(&DirectionalLight, &CascadedShadow, &Transform)>()
            {
                builder.begin_label("Cascaded Shadow", Vec4::new(0.5, 0.2, 0.0, 1.0));

                for (base_array_layer, cascade) in cascaded_shadow.cascades().iter().enumerate() {
                    builder.begin_label("Cascade", Vec4::new(0.2, 0.5, 0.0, 1.0));

                    builder
                        .pipeline_barrier(
                            PipelineBarrier::builder()
                                .image_memory_barrier(
                                    ImageMemoryBarrier::builder(
                                        cascade.image_view().image().clone(),
                                    )
                                    .src_stage_mask(PipelineStages {
                                        fragment_shader: true,
                                        ..Default::default()
                                    })
                                    .src_access_mask(AccessMask {
                                        shader_sampled_read: true,
                                        ..Default::default()
                                    })
                                    .dst_stage_mask(PipelineStages {
                                        early_fragment_tests: true,
                                        ..Default::default()
                                    })
                                    .dst_access_mask(AccessMask {
                                        depth_stencil_attachment_write: true,
                                        ..Default::default()
                                    })
                                    .old_layout(ImageLayout::Undefined)
                                    .new_layout(ImageLayout::DepthAttachmentOptimal)
                                    .subresource_range(
                                        ImageSubresourceRange::builder()
                                            .aspect_mask(ImageAspects {
                                                depth: true,
                                                ..Default::default()
                                            })
                                            .base_array_layer(base_array_layer as _)
                                            .build(),
                                    )
                                    .build(),
                                )
                                .build(),
                        )
                        .bind_graphics_pipeline(self.static_graphics_pipeline.clone())
                        .begin_rendering(self.create_render_pass(cascade.image_view().clone()))
                        .set_viewport(
                            0,
                            vec![Viewport {
                                x: 0.0,
                                y: 0.0,
                                width: cascaded_shadow.image_view().image().extent().width as _,
                                height: cascaded_shadow.image_view().image().extent().height as _,
                                min_depth: 0.0,
                                max_depth: 1.0,
                            }],
                        )
                        .set_scissor(
                            0,
                            vec![Rect2D {
                                offset: Default::default(),
                                extent: cascaded_shadow.image_view().image().extent().into(),
                            }],
                        );

                    for (_, static_mesh, transform) in entities
                        .components::<(&StaticMesh, &Transform)>()
                        .filter(|(_, mesh, _)| !mesh.disable_shadow)
                    {
                        builder.push_constants(
                            self.static_pipeline_layout.clone(),
                            ShaderStages {
                                vertex: true,
                                ..Default::default()
                            },
                            0,
                            PushConstants {
                                view_projection: cascade.view_projection(),
                                model: Mat4::from(*transform),
                            },
                        );

                        if let Some(mesh) = static_mesh.mesh().ok() {
                            builder
                                .bind_vertex_buffer(mesh.vertex_buffer().clone())
                                .bind_index_buffer(
                                    mesh.index_buffer().clone(),
                                    0,
                                    IndexType::UInt32,
                                );

                            for sub_mesh in mesh.sub_meshes() {
                                builder.draw_indexed(
                                    sub_mesh.count() as _,
                                    1,
                                    sub_mesh.offset() as _,
                                    0,
                                    0,
                                );
                            }
                        }
                    }

                    builder.bind_graphics_pipeline(self.animated_graphics_pipeline.clone());

                    for (_, animated_mesh, transform) in
                        entities.components::<(&AnimatedMesh, &Transform)>()
                    {
                        if let Some(joint_buffer) = animated_mesh.joint_buffer() {
                            builder.push_constants(
                                self.animated_pipeline_layout.clone(),
                                ShaderStages {
                                    vertex: true,
                                    ..Default::default()
                                },
                                0,
                                PushConstants {
                                    view_projection: cascade.view_projection(),
                                    model: Mat4::from(*transform),
                                },
                            );

                            if let Some(mesh) = animated_mesh.mesh().ok() {
                                let joint_descriptor_set =
                                    self.create_joint_descriptor_set(joint_buffer)?;
                                builder
                                    .bind_vertex_buffer(mesh.vertex_buffer().clone())
                                    .bind_index_buffer(
                                        mesh.index_buffer().clone(),
                                        0,
                                        IndexType::UInt32,
                                    )
                                    .bind_descriptor_sets(
                                        PipelineBindPoint::Graphics,
                                        self.animated_pipeline_layout.clone(),
                                        0,
                                        vec![joint_descriptor_set],
                                    );

                                for sub_mesh in mesh.sub_meshes() {
                                    builder.draw_indexed(
                                        sub_mesh.count() as _,
                                        1,
                                        sub_mesh.offset() as _,
                                        0,
                                        0,
                                    );
                                }
                            }
                        }
                    }

                    builder.end_rendering().end_label();
                }

                builder
                    .pipeline_barrier(
                        PipelineBarrier::builder()
                            .image_memory_barrier(
                                ImageMemoryBarrier::builder(
                                    cascaded_shadow.image_view().image().clone(),
                                )
                                .src_stage_mask(PipelineStages {
                                    early_fragment_tests: true,
                                    ..Default::default()
                                })
                                .src_access_mask(AccessMask {
                                    depth_stencil_attachment_write: true,
                                    ..Default::default()
                                })
                                .dst_stage_mask(PipelineStages {
                                    fragment_shader: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    shader_sampled_read: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::DepthAttachmentOptimal)
                                .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            depth: true,
                                            ..Default::default()
                                        })
                                        .layer_count(cascaded_shadow.cascades().len() as _)
                                        .build(),
                                )
                                .build(),
                            )
                            .build(),
                    )
                    .end_label();
            }
        }

        builder.end_label();

        Ok(())
    }

    fn update_cascades(
        &self,
        final_image: &ImageView,
        camera_entity: &Facade,
        directional_light_entity: &Facade,
    ) {
        let aspect_ration =
            final_image.image().extent().width as f32 / final_image.image().extent().height as f32;
        let camera = camera_entity.get::<Camera>().expect("camera");
        let camera_transform = camera_entity.get::<Transform>().expect("camera transform");
        let mut cascaded_shadow = directional_light_entity
            .get_mut::<CascadedShadow>()
            .expect("Cascaded Shadow Pass");
        let transform = directional_light_entity
            .get::<Transform>()
            .expect("directional light transform");

        let mut cascade_splits = vec![0.0; cascaded_shadow.cascades().len()];

        let near_clip = camera.near();
        let far_clip = camera.far();
        let clip_range = far_clip - near_clip;

        let min_z = near_clip;
        let max_z = near_clip + clip_range;

        let range = max_z - min_z;
        let ratio = max_z / min_z;

        for i in 0..cascaded_shadow.cascades().len() {
            let p = (i + 1) as f32 / cascaded_shadow.cascades().len() as f32;
            let log = min_z * f32::powf(ratio, p);
            let uniform = min_z + range * p;
            let d = 0.95 * (log - uniform) + uniform;
            cascade_splits[i] = (d - near_clip) / clip_range;
        }

        let mut last_split = 0.0;
        for i in 0..cascaded_shadow.cascades().len() {
            let split_dist = cascade_splits[i];

            let mut frustum_corners = [
                Vec3::new(-1.0, 1.0, -1.0),
                Vec3::new(1.0, 1.0, -1.0),
                Vec3::new(1.0, -1.0, -1.0),
                Vec3::new(-1.0, -1.0, -1.0),
                Vec3::new(-1.0, 1.0, 1.0),
                Vec3::new(1.0, 1.0, 1.0),
                Vec3::new(1.0, -1.0, 1.0),
                Vec3::new(-1.0, -1.0, 1.0),
            ];

            let inv_cam = (camera.projection(aspect_ration)
                * Mat4::from(*camera_transform).inverse())
            .inverse();
            for i in 0..8 {
                let inv_corner = inv_cam * Vec4::from(frustum_corners[i]);
                frustum_corners[i] = (inv_corner / inv_corner[3]).into();
            }

            for i in 0..4 {
                let dist = frustum_corners[i + 4] - frustum_corners[i];
                frustum_corners[i + 4] = frustum_corners[i] + (dist * split_dist);
                frustum_corners[i] = frustum_corners[i] + (dist * last_split);
            }

            let mut frustum_center = Vec3::zero();
            for i in 0..8 {
                frustum_center += frustum_corners[i];
            }
            frustum_center /= 8.0;

            let mut radius = 0.0;
            for i in 0..8 {
                let distance = (frustum_corners[i] - frustum_center).length();
                radius = f32::max(radius, distance);
            }
            radius = f32::ceil(radius * 16.0) / 16.0;

            let max_extent = Vec3::new(radius, radius, radius);
            let min_extent = -max_extent;

            let light_dir = Vec3::from(transform.orientation()).normalize();
            let light_view_matrix = Mat4::look_at(
                frustum_center - light_dir * -min_extent[2],
                frustum_center,
                Vec3::new(0.0, 1.0, 0.0),
            );
            let light_ortho_matrix = Mat4::ortho(
                min_extent[0],
                max_extent[0],
                min_extent[1],
                max_extent[1],
                0.0,
                max_extent[2] - min_extent[2],
            );

            cascaded_shadow.cascades_mut()[i]
                .set_split_depth((camera.near() + split_dist * clip_range) * -1.0);
            cascaded_shadow.cascades_mut()[i]
                .set_view_projection(light_ortho_matrix * light_view_matrix);

            last_split = cascade_splits[i];
        }

        cascaded_shadow.update_ssbo();
    }

    fn create_render_pass(&self, cascade: Arc<ImageView>) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: cascade.image().extent().into(),
            })
            .depth_attachment(
                Attachment::builder(cascade)
                    .image_layout(ImageLayout::DepthAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Depth(1.0))
                    .build(),
            )
            .build()
    }

    fn create_joint_descriptor_set(
        &self,
        joint_buffer: Arc<Buffer>,
    ) -> Result<Arc<DescriptorSet>, GfxError> {
        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.joint_descriptor_set_layout)?,
            self.joint_descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Cascaded Shadow Pass Animated Descriptor Set");

        descriptor_set.write_buffers(vec![WriteDescriptorSetBuffers::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::StorageBuffer)
            .buffer(&joint_buffer, 0, ash::vk::WHOLE_SIZE)
            .build()]);

        Ok(descriptor_set)
    }
}

mod shaders {
    use crate::math::Mat4;

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub view_projection: Mat4,
        pub model: Mat4,
    }
}

mod static_vertex_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/static_cascaded_shadow.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod animated_vertex_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/animated_cascaded_shadow.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod fragment_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/cascaded_shadow.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
