use std::path::Path;
use std::sync::Arc;

use crate::asset::{Asset, Assets};
use crate::ecs::{Entities, Transform};
use crate::gfx::buffer::Buffer;
use crate::gfx::command_buffer::{CommandBufferBuilder, IndexType};
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{Format, ImageAspects, ImageLayout, ImageSubresourceRange};
use crate::gfx::pipeline::{
    ColorBlendAttachmentState, CompareOp, CullMode, DescriptorSet, DescriptorSetLayout,
    DescriptorType, DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
    PrimitiveTopology, ShaderStages, Specialization, VertexInput, VertexInputRate, Viewport,
    WriteDescriptorSetBuffers, WriteDescriptorSetImages,
};
use crate::gfx::render_pass::{
    Attachment, AttachmentLoadOp, AttachmentStoreOp, ClearValue, RenderPass,
};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use crate::math::{Mat4, Vec4};
use crate::rendering::pbr::geometry_pass::shaders::{PushConstants, SpecializationConstants};
use crate::rendering::pbr::GBuffer;
use crate::rendering::{mesh, skinned_mesh, AnimatedMesh, Camera, Material, StaticMesh, Texture};

/// Geometry pass.
#[derive(Debug)]
pub struct GeometryPass {
    default_metallic_roughness_texture: Texture,
    default_normal_texture: Texture,
    default_base_color_texture: Texture,
    default_material: Material,
    animated_graphics_pipelines: [Arc<GraphicsPipeline>; 8],
    animated_pipeline_layout: Arc<PipelineLayout>,
    animated_descriptor_set_layout: Arc<DescriptorSetLayout>,
    static_graphics_pipelines: [Arc<GraphicsPipeline>; 8],
    static_pipeline_layout: Arc<PipelineLayout>,
    material_descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl GeometryPass {
    /// Creates a new geometry pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let material_descriptor_set_layout =
            Self::create_material_descriptor_set_layout(graphics_context.device().clone())?;
        let static_pipeline_layout = Self::create_static_pipeline_layout(
            graphics_context.device().clone(),
            material_descriptor_set_layout.clone(),
        )?;
        let static_graphics_pipelines = Self::create_static_graphics_pipelines(
            graphics_context.device().clone(),
            static_pipeline_layout.clone(),
        )?;
        let animated_descriptor_set_layout =
            Self::create_animated_descriptor_set_layout(graphics_context.device().clone())?;
        let animated_pipeline_layout = Self::create_animated_pipeline_layout(
            graphics_context.device().clone(),
            material_descriptor_set_layout.clone(),
            animated_descriptor_set_layout.clone(),
        )?;
        let animated_graphics_pipelines = Self::create_animated_graphics_pipelines(
            graphics_context.device().clone(),
            animated_pipeline_layout.clone(),
        )?;
        let assets = Assets::builder().build();
        let default_base_color_texture = Texture::try_from_bytes(
            assets.clone(),
            graphics_context.clone(),
            Path::new("__internal__.png"),
            default::DEFAULT_BASE_COLOR_IMAGE.to_vec(),
        )
        .unwrap();
        let default_normal_texture = Texture::try_from_bytes(
            assets.clone(),
            graphics_context.clone(),
            Path::new("__internal__.png"),
            default::DEFAULT_NORMAL_IMAGE.to_vec(),
        )
        .unwrap();
        let default_metallic_roughness_texture = Texture::try_from_bytes(
            assets,
            graphics_context.clone(),
            Path::new("__internal__.png"),
            default::DEFAULT_METALLIC_ROUGHNESS_IMAGE.to_vec(),
        )
        .unwrap();

        material_descriptor_set_layout
            .set_debug_utils_object_name("Geometry Pass Static Descriptor Set Layout");
        static_pipeline_layout.set_debug_utils_object_name("Geometry Pass Static Pipeline Layout");
        static_graphics_pipelines
            .iter()
            .for_each(|graphics_pipeline| {
                graphics_pipeline
                    .set_debug_utils_object_name("Geometry Pass Static Graphics Pipeline")
            });

        Ok(Self {
            default_metallic_roughness_texture,
            default_normal_texture,
            default_base_color_texture,
            default_material: Default::default(),
            animated_graphics_pipelines,
            animated_pipeline_layout,
            animated_descriptor_set_layout,
            static_graphics_pipelines,
            static_pipeline_layout,
            material_descriptor_set_layout,
            graphics_context,
        })
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        g_buffer: &GBuffer,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Geometry Pass", Vec4::new(1.0, 0.4, 0.0, 1.0));

        builder
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(g_buffer.position().image().clone())
                            .src_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(g_buffer.base_color().image().clone())
                            .src_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(g_buffer.normal().image().clone())
                            .src_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(g_buffer.metallic_roughness().image().clone())
                            .src_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(g_buffer.depth_buffer().image().clone())
                            .src_stage_mask(PipelineStages {
                                early_fragment_tests: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                depth_stencil_attachment_read: true,
                                depth_stencil_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                early_fragment_tests: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                depth_stencil_attachment_read: true,
                                depth_stencil_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::DepthAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        depth: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .bind_graphics_pipeline(self.static_graphics_pipelines[0].clone())
            .begin_rendering(self.create_render_pass(g_buffer))
            .set_viewport(
                0,
                vec![Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: g_buffer.extent().width as _,
                    height: g_buffer.extent().height as _,
                    min_depth: 0.0,
                    max_depth: 1.0,
                }],
            )
            .set_scissor(
                0,
                vec![Rect2D {
                    offset: Default::default(),
                    extent: g_buffer.extent(),
                }],
            );

        if let Some((Some(camera), Some(camera_transform))) = entities
            .active::<Camera>()
            .map(|camera| (camera.get::<Camera>(), camera.get::<Transform>()))
        {
            for (_, static_mesh, transform) in entities.components::<(&StaticMesh, &Transform)>() {
                if let Some(mesh) = static_mesh.mesh().ok() {
                    builder
                        .bind_vertex_buffer(mesh.vertex_buffer().clone())
                        .bind_index_buffer(mesh.index_buffer().clone(), 0, IndexType::UInt32);

                    let sub_meshes = if let Some(sub_mesh_index) = static_mesh.sub_mesh() {
                        vec![mesh.sub_meshes()[sub_mesh_index].clone()]
                    } else {
                        mesh.sub_meshes().clone()
                    };

                    for sub_mesh in &sub_meshes {
                        let material = sub_mesh.material().ok().unwrap_or(&self.default_material);
                        let descriptor_set = self.create_material_descriptor_set(material)?;

                        builder
                            .bind_graphics_pipeline(
                                self.static_graphics_pipelines[material.pipeline_index()].clone(),
                            )
                            .bind_descriptor_sets(
                                PipelineBindPoint::Graphics,
                                self.static_pipeline_layout.clone(),
                                0,
                                vec![descriptor_set],
                            )
                            .push_constants(
                                self.static_pipeline_layout.clone(),
                                ShaderStages {
                                    vertex: true,
                                    fragment: true,
                                    ..Default::default()
                                },
                                0,
                                PushConstants {
                                    projection: camera.projection(
                                        g_buffer.extent().width as f32
                                            / g_buffer.extent().height as f32,
                                    ),
                                    view: Mat4::from(*camera_transform).inverse(),
                                    model: Mat4::from(*transform),
                                    base_color: material.base_color(),
                                    metallic: material.metallic(),
                                    roughness: material.roughness(),
                                },
                            )
                            .draw_indexed(sub_mesh.count() as _, 1, sub_mesh.offset() as _, 0, 0);
                    }
                }
            }

            for (_, animated_mesh, transform) in
                entities.components::<(&AnimatedMesh, &Transform)>()
            {
                if let Some(mesh) = animated_mesh.mesh().ok() {
                    if let Some(joint_buffer) = animated_mesh.joint_buffer() {
                        let animated_descriptor_set =
                            self.create_animated_descriptor_set(joint_buffer)?;
                        builder
                            .bind_vertex_buffer(mesh.vertex_buffer().clone())
                            .bind_index_buffer(mesh.index_buffer().clone(), 0, IndexType::UInt32)
                            .bind_descriptor_sets(
                                PipelineBindPoint::Graphics,
                                self.animated_pipeline_layout.clone(),
                                1,
                                vec![animated_descriptor_set],
                            );

                        let sub_meshes = if let Some(sub_mesh_index) = animated_mesh.sub_mesh() {
                            vec![mesh.sub_meshes()[sub_mesh_index].clone()]
                        } else {
                            mesh.sub_meshes().clone()
                        };

                        for sub_mesh in &sub_meshes {
                            let material =
                                sub_mesh.material().ok().unwrap_or(&self.default_material);
                            let material_descriptor_set =
                                self.create_material_descriptor_set(material)?;

                            builder
                                .bind_graphics_pipeline(
                                    self.animated_graphics_pipelines[material.pipeline_index()]
                                        .clone(),
                                )
                                .bind_descriptor_sets(
                                    PipelineBindPoint::Graphics,
                                    self.animated_pipeline_layout.clone(),
                                    0,
                                    vec![material_descriptor_set],
                                )
                                .push_constants(
                                    self.animated_pipeline_layout.clone(),
                                    ShaderStages {
                                        vertex: true,
                                        fragment: true,
                                        ..Default::default()
                                    },
                                    0,
                                    PushConstants {
                                        projection: camera.projection(
                                            g_buffer.extent().width as f32
                                                / g_buffer.extent().height as f32,
                                        ),
                                        view: Mat4::from(*camera_transform).inverse(),
                                        model: Mat4::from(*transform),
                                        base_color: material.base_color(),
                                        metallic: material.metallic(),
                                        roughness: material.roughness(),
                                    },
                                )
                                .draw_indexed(
                                    sub_mesh.count() as _,
                                    1,
                                    sub_mesh.offset() as _,
                                    0,
                                    0,
                                );
                        }
                    }
                }
            }
        }

        builder.end_rendering().end_label();

        Ok(())
    }
}

impl GeometryPass {
    fn create_material_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                1,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                2,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_static_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    fragment: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_static_graphics_pipelines(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<[Arc<GraphicsPipeline>; 8], GfxError> {
        Ok([
            Self::create_static_graphics_pipeline(device.clone(), pipeline_layout.clone(), 0)?,
            Self::create_static_graphics_pipeline(device.clone(), pipeline_layout.clone(), 1)?,
            Self::create_static_graphics_pipeline(device.clone(), pipeline_layout.clone(), 2)?,
            Self::create_static_graphics_pipeline(device.clone(), pipeline_layout.clone(), 3)?,
            Self::create_static_graphics_pipeline(device.clone(), pipeline_layout.clone(), 4)?,
            Self::create_static_graphics_pipeline(device.clone(), pipeline_layout.clone(), 5)?,
            Self::create_static_graphics_pipeline(device.clone(), pipeline_layout.clone(), 6)?,
            Self::create_static_graphics_pipeline(device, pipeline_layout, 7)?,
        ])
    }

    fn create_static_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
        pipeline_index: usize,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .attribute(1, 0, Format::R32G32B32SFloat, 12)
                    .attribute(2, 0, Format::R32G32B32A32SFloat, 24)
                    .attribute(3, 0, Format::R32G32SFloat, 40)
                    .build(),
            )
            .vertex_shader(static_vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .fragment_shader_specialization(
                Specialization::builder()
                    .entry(0, 0, 4)
                    .entry(1, 4, 4)
                    .entry(2, 8, 4)
                    .data(SpecializationConstants::from_pipeline_index(pipeline_index))
                    .build(),
            )
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::Less)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R16G16B16A16SFloat)
            .color_attachment_format(Format::R8G8B8A8Srgb)
            .color_attachment_format(Format::R16G16B16A16SFloat)
            .color_attachment_format(Format::R16G16B16A16SFloat)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    fn create_animated_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::StorageBuffer,
                1,
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_animated_pipeline_layout(
        device: Arc<Device>,
        material_descriptor_set_layout: Arc<DescriptorSetLayout>,
        animated_descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(material_descriptor_set_layout)
            .set_layout(animated_descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    fragment: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_animated_graphics_pipelines(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<[Arc<GraphicsPipeline>; 8], GfxError> {
        Ok([
            Self::create_animated_graphics_pipeline(device.clone(), pipeline_layout.clone(), 0)?,
            Self::create_animated_graphics_pipeline(device.clone(), pipeline_layout.clone(), 1)?,
            Self::create_animated_graphics_pipeline(device.clone(), pipeline_layout.clone(), 2)?,
            Self::create_animated_graphics_pipeline(device.clone(), pipeline_layout.clone(), 3)?,
            Self::create_animated_graphics_pipeline(device.clone(), pipeline_layout.clone(), 4)?,
            Self::create_animated_graphics_pipeline(device.clone(), pipeline_layout.clone(), 5)?,
            Self::create_animated_graphics_pipeline(device.clone(), pipeline_layout.clone(), 6)?,
            Self::create_animated_graphics_pipeline(device, pipeline_layout, 7)?,
        ])
    }

    fn create_animated_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
        pipeline_index: usize,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<skinned_mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .attribute(1, 0, Format::R32G32B32SFloat, 12)
                    .attribute(2, 0, Format::R32G32B32A32SFloat, 24)
                    .attribute(3, 0, Format::R32G32SFloat, 40)
                    .attribute(4, 0, Format::R32G32B32A32SFloat, 48)
                    .attribute(5, 0, Format::R32G32B32A32SFloat, 64)
                    .build(),
            )
            .vertex_shader(
                animated_vertex_shader::Shader::load(device.clone())?,
                "main",
            )
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .fragment_shader_specialization(
                Specialization::builder()
                    .entry(0, 0, 4)
                    .entry(1, 4, 4)
                    .entry(2, 8, 4)
                    .data(SpecializationConstants::from_pipeline_index(pipeline_index))
                    .build(),
            )
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::Less)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R16G16B16A16SFloat)
            .color_attachment_format(Format::R8G8B8A8Srgb)
            .color_attachment_format(Format::R16G16B16A16SFloat)
            .color_attachment_format(Format::R16G16B16A16SFloat)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    fn create_render_pass(&self, g_buffer: &GBuffer) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: g_buffer.extent(),
            })
            .color_attachment(
                Attachment::builder(g_buffer.position().clone())
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Float([0.0, 0.0, 0.0, 1.0]))
                    .build(),
            )
            .color_attachment(
                Attachment::builder(g_buffer.base_color().clone())
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Float([0.0, 0.0, 0.0, 1.0]))
                    .build(),
            )
            .color_attachment(
                Attachment::builder(g_buffer.normal().clone())
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Float([0.0, 0.0, 0.0, 1.0]))
                    .build(),
            )
            .color_attachment(
                Attachment::builder(g_buffer.metallic_roughness().clone())
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Float([0.0, 0.0, 0.0, 1.0]))
                    .build(),
            )
            .depth_attachment(
                Attachment::builder(g_buffer.depth_buffer().clone())
                    .image_layout(ImageLayout::DepthAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Depth(1.0))
                    .build(),
            )
            .build()
    }

    fn create_material_descriptor_set(
        &self,
        material: &Material,
    ) -> Result<Arc<DescriptorSet>, GfxError> {
        let base_color = material
            .base_color_texture()
            .ok()
            .unwrap_or(&self.default_base_color_texture);
        let normal = material
            .normal_texture()
            .ok()
            .unwrap_or(&self.default_normal_texture);
        let metallic_roughness = material
            .metallic_roughness_texture()
            .ok()
            .unwrap_or(&self.default_metallic_roughness_texture);

        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.material_descriptor_set_layout)?,
            self.material_descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Geometry Pass Material Descriptor Set");

        descriptor_set.write_images(vec![
            WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    base_color.sampler(),
                    base_color.image_view(),
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build(),
            WriteDescriptorSetImages::builder()
                .dst_binding(1)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    normal.sampler(),
                    normal.image_view(),
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build(),
            WriteDescriptorSetImages::builder()
                .dst_binding(2)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    metallic_roughness.sampler(),
                    metallic_roughness.image_view(),
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build(),
        ]);

        Ok(descriptor_set)
    }

    fn create_animated_descriptor_set(
        &self,
        joint_buffer: Arc<Buffer>,
    ) -> Result<Arc<DescriptorSet>, GfxError> {
        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.animated_descriptor_set_layout)?,
            self.animated_descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Geometry Pass Animated Descriptor Set");

        descriptor_set.write_buffers(vec![WriteDescriptorSetBuffers::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::StorageBuffer)
            .buffer(&joint_buffer, 0, ash::vk::WHOLE_SIZE)
            .build()]);

        Ok(descriptor_set)
    }
}

mod shaders {
    use crate::math::{Mat4, Vec4};

    #[allow(unused)]
    #[derive(Copy, Clone, Debug)]
    #[repr(C)]
    pub struct SpecializationConstants {
        with_base_color_texture: u32,
        with_normal_texture: u32,
        with_metallic_roughness_texture: u32,
    }

    impl SpecializationConstants {
        pub fn from_pipeline_index(index: usize) -> Self {
            Self {
                with_base_color_texture: if index & 0b001 == 0b001 { 1 } else { 0 },
                with_normal_texture: if index & 0b010 == 0b010 { 1 } else { 0 },
                with_metallic_roughness_texture: if index & 0b100 == 0b100 { 1 } else { 0 },
            }
        }
    }

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub projection: Mat4,
        pub view: Mat4,
        pub model: Mat4,
        pub base_color: Vec4,
        pub metallic: f32,
        pub roughness: f32,
    }
}

mod static_vertex_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/static_geometry.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod animated_vertex_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/animated_geometry.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod fragment_shader {
    use irradiance_shaders::shader;

    shader! {
        path: "src/rendering/pbr/geometry.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}

mod default {
    pub const DEFAULT_BASE_COLOR_IMAGE: &[u8] =
        include_bytes!("../../../rsc/rendering/pbr/default.base_color.png");
    pub const DEFAULT_NORMAL_IMAGE: &[u8] =
        include_bytes!("../../../rsc/rendering/pbr/default.normal.png");
    pub const DEFAULT_METALLIC_ROUGHNESS_IMAGE: &[u8] =
        include_bytes!("../../../rsc/rendering/pbr/default.metallic_roughness.png");
}
