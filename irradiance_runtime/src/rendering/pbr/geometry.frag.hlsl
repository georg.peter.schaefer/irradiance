#include "../tangent_space.hlsl"

[[vk::binding(0, 0)]]
Texture2D base_color_texture;
[[vk::binding(0, 0)]]
SamplerState base_color_sampler;
[[vk::binding(1, 0)]]
Texture2D normal_texture;
[[vk::binding(1, 0)]]
SamplerState normal_sampler;
[[vk::binding(2, 0)]]
Texture2D metallic_roughness_texture;
[[vk::binding(2, 0)]]
SamplerState metallic_roughness_sampler;

struct VSOutput {
    [[vk::location(0)]] float3 world_position : POSITION0;
    [[vk::location(1)]] float3 normal : NORMAL0;
    [[vk::location(2)]] float4 tangent : TANGENT0;
    [[vk::location(3)]] float2 tex_coord : TEXCOORD0;
};

struct PushConstants {
    float4x4 projection;
    float4x4 view;
    float4x4 model;
    float4 base_color;
    float metallic;
    float roughness;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

struct FSOutput {
    float3 position : SV_TARGET0;
    float3 base_color : SV_TARGET1;
    float3 normal : SV_TARGET2;
    float3 metallic_roughness : SV_TARGET3;
};

[[vk::constant_id(0)]]
const bool with_base_color_texture = false;
[[vk::constant_id(1)]]
const bool with_normal_texture = false;
[[vk::constant_id(2)]]
const bool with_metallic_roughness_texture = false;

float4 base_color(float2 tex_coord) {
    if (with_base_color_texture) {
        float4 base_color = base_color_texture.Sample(base_color_sampler, tex_coord);
        return float4(pow(base_color.rgb, float3(2.2)), base_color.a);
    }
    return push_constants.base_color;
}

float3 normal(float2 tex_coord, float4 tangent, float3 normal) {
    if (with_normal_texture) {
        float3 N = 2.0 * normal_texture.Sample(normal_sampler, tex_coord).xyz - float3(1.0, 1.0, 1.0);
        return normalize(N * tangent_to_world(tangent, normal));
    }
    return normal;
}

float3 metallic_roughness(float2 tex_coord) {
    if (with_metallic_roughness_texture) {
        return metallic_roughness_texture.Sample(metallic_roughness_sampler, tex_coord).rgb;
    }
    return float3(push_constants.metallic, push_constants.roughness, 0.0);
}

FSOutput main(VSOutput input) {
    FSOutput output = (FSOutput)0;

    output.position = input.world_position;
    output.base_color = base_color(input.tex_coord);
    output.normal = normal(input.tex_coord, input.tangent, input.normal);
    output.metallic_roughness = metallic_roughness(input.tex_coord);

    return output;
}
