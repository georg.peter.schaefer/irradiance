use std::sync::Arc;

use fullscreen_pass::{create_fullscreen_vertex_buffer, vertex_shader};
use irradiance_runtime::gfx::image::ImageCreateFlags;

use crate::ecs::{Entities, Transform};
use crate::gfx::buffer::Buffer;
use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{
    Format, Image, ImageAspects, ImageLayout, ImageSubresourceRange, ImageUsage, ImageView,
    ImageViewType, Sampler,
};
use crate::gfx::pipeline::{
    BlendOp, ColorBlendAttachmentState, CullMode, DescriptorSet, DescriptorSetLayout,
    DescriptorType, DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
    PrimitiveTopology, ShaderStages, Specialization, VertexInput, VertexInputRate, Viewport,
    WriteDescriptorSetImages,
};
use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use crate::math::Vec4;
use crate::rendering::fullscreen_pass::Vertex;
use crate::rendering::pbr::point_light_pass::shaders::PushConstants;
use crate::rendering::pbr::GBuffer;
use crate::rendering::{fullscreen_pass, Camera, OmnidirectionalShadow, PointLight};

/// Point light gathering pass.
#[derive(Debug)]
pub struct PointLightPass {
    sampler: Arc<Sampler>,
    fullscreen_vertex_buffer: Arc<Buffer>,
    graphics_pipelines: [Arc<GraphicsPipeline>; 2],
    pipeline_layout: Arc<PipelineLayout>,
    empty_shadow_descriptor_set: Arc<DescriptorSet>,
    empty_image: Arc<ImageView>,
    descriptor_set_layouts: [Arc<DescriptorSetLayout>; 2],
    graphics_context: Arc<GraphicsContext>,
}

impl PointLightPass {
    /// Creates a new point light gathering pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let descriptor_set_layouts =
            Self::create_descriptor_set_layouts(graphics_context.device().clone())?;
        let empty_image = Self::create_empty_image(graphics_context.device().clone())?;
        let empty_shadow_descriptor_set = Self::create_empty_shadow_descriptor_set(
            &graphics_context,
            descriptor_set_layouts[1].clone(),
            empty_image.clone(),
        )?;
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            &descriptor_set_layouts,
        )?;
        let graphics_pipelines = Self::create_graphics_pipelines(
            graphics_context.device().clone(),
            pipeline_layout.clone(),
        )?;
        let fullscreen_vertex_buffer = create_fullscreen_vertex_buffer(&graphics_context)?;
        let sampler = Sampler::builder(graphics_context.device().clone()).build()?;

        descriptor_set_layouts[0]
            .set_debug_utils_object_name("Point Light Pass No Shadow Descriptor Set Layout");
        descriptor_set_layouts[1]
            .set_debug_utils_object_name("Point Light Pass Descriptor Set Layout");
        empty_image.set_debug_utils_object_name("Point Light Pass No Shadow Image");
        empty_shadow_descriptor_set
            .set_debug_utils_object_name("Point Light Pass No Shadow Descriptor Set");
        pipeline_layout.set_debug_utils_object_name("Point Light Pass Pipeline Layout");
        graphics_pipelines[0]
            .set_debug_utils_object_name("Point Light Pass No Shadow Graphics Pipeline");
        graphics_pipelines[1].set_debug_utils_object_name("Point Light Pass Graphics Pipeline");
        fullscreen_vertex_buffer
            .set_debug_utils_object_name("Point Light Pass Fullscreen Vertex Buffer");
        sampler.set_debug_utils_object_name("Point Light Pass Sampler");

        Ok(Self {
            sampler,
            fullscreen_vertex_buffer,
            graphics_pipelines,
            pipeline_layout,
            empty_shadow_descriptor_set,
            empty_image,
            descriptor_set_layouts,
            graphics_context,
        })
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        g_buffer: &GBuffer,
        light_buffer: Arc<ImageView>,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Point Light Pass", Vec4::new(0.5, 1.0, 0.0, 1.0));

        if let Some(camera) = entities.active::<Camera>() {
            let descriptor_set = DescriptorSet::builder(
                self.graphics_context.device().clone(),
                self.graphics_context
                    .descriptor_pool(&self.descriptor_set_layouts[0])?,
                self.descriptor_set_layouts[0].clone(),
            )
            .build()?;
            descriptor_set.set_debug_utils_object_name("Point Light Pass Descriptor Set");

            descriptor_set.write_images(vec![
                WriteDescriptorSetImages::builder()
                    .dst_binding(0)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        &self.sampler,
                        g_buffer.position(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(1)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        &self.sampler,
                        g_buffer.base_color(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(2)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        &self.sampler,
                        g_buffer.normal(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(3)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        &self.sampler,
                        g_buffer.metallic_roughness(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
            ]);

            builder
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(self.empty_image.image().clone())
                                .dst_stage_mask(PipelineStages {
                                    fragment_shader: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    shader_sampled_read: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::Undefined)
                                .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .layer_count(6)
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                )
                .set_viewport(
                    0,
                    vec![Viewport {
                        x: 0.0,
                        y: 0.0,
                        width: light_buffer.image().extent().width as _,
                        height: light_buffer.image().extent().height as _,
                        min_depth: 0.0,
                        max_depth: 1.0,
                    }],
                )
                .set_scissor(
                    0,
                    vec![Rect2D {
                        offset: Default::default(),
                        extent: light_buffer.image().extent().into(),
                    }],
                )
                .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.pipeline_layout.clone(),
                    0,
                    vec![descriptor_set],
                )
                .bind_graphics_pipeline(self.graphics_pipelines[0].clone())
                .begin_rendering(self.create_render_pass(light_buffer.clone()));

            for (_, point_light, transform) in entities
                .components::<(&PointLight, &Transform)>()
                .filter(|(entity, _, _)| entity.get::<OmnidirectionalShadow>().is_none())
            {
                builder
                    .bind_descriptor_sets(
                        PipelineBindPoint::Graphics,
                        self.pipeline_layout.clone(),
                        1,
                        vec![self.empty_shadow_descriptor_set.clone()],
                    )
                    .push_constants(
                        self.pipeline_layout.clone(),
                        ShaderStages {
                            fragment: true,
                            ..Default::default()
                        },
                        0,
                        PushConstants {
                            direction: transform.position(),
                            _dummy0: 0.0,
                            color: point_light.color(),
                            intensity: point_light.intensity(),
                            eye: camera.get::<Transform>().unwrap().position(),
                        },
                    )
                    .draw(6, 1, 0, 0);
            }

            builder.bind_graphics_pipeline(self.graphics_pipelines[1].clone());

            for (_, point_light, omnidirectional_shadow, transform) in
                entities.components::<(&PointLight, &OmnidirectionalShadow, &Transform)>()
            {
                builder
                    .bind_descriptor_sets(
                        PipelineBindPoint::Graphics,
                        self.pipeline_layout.clone(),
                        1,
                        vec![omnidirectional_shadow.descriptor_set().clone()],
                    )
                    .push_constants(
                        self.pipeline_layout.clone(),
                        ShaderStages {
                            fragment: true,
                            ..Default::default()
                        },
                        0,
                        PushConstants {
                            direction: transform.position(),
                            _dummy0: 0.0,
                            color: point_light.color(),
                            intensity: point_light.intensity(),
                            eye: camera.get::<Transform>().unwrap().position(),
                        },
                    )
                    .draw(6, 1, 0, 0);
            }

            builder.end_rendering();
        }

        builder.end_label();

        Ok(())
    }
}

impl PointLightPass {
    fn create_descriptor_set_layouts(
        device: Arc<Device>,
    ) -> Result<[Arc<DescriptorSetLayout>; 2], GfxError> {
        Ok([
            DescriptorSetLayout::builder(device.clone())
                .binding(
                    0,
                    DescriptorType::CombinedImageSampler,
                    1,
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                )
                .binding(
                    1,
                    DescriptorType::CombinedImageSampler,
                    1,
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                )
                .binding(
                    2,
                    DescriptorType::CombinedImageSampler,
                    1,
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                )
                .binding(
                    3,
                    DescriptorType::CombinedImageSampler,
                    1,
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                )
                .build()?,
            DescriptorSetLayout::builder(device)
                .binding(
                    0,
                    DescriptorType::CombinedImageSampler,
                    1,
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                )
                .build()?,
        ])
    }

    fn create_empty_image(device: Arc<Device>) -> Result<Arc<ImageView>, GfxError> {
        ImageView::builder(
            device.clone(),
            Image::builder(device)
                .image_create_flags(ImageCreateFlags {
                    cube_compatible: true,
                    ..Default::default()
                })
                .format(Format::R8G8B8A8Srgb)
                .extent(
                    Extent2D {
                        width: 8,
                        height: 8,
                    }
                    .into(),
                )
                .array_layers(6)
                .usage(ImageUsage {
                    sampled: true,
                    ..Default::default()
                })
                .build()?,
        )
        .view_type(ImageViewType::Cube)
        .subresource_range(
            ImageSubresourceRange::builder()
                .aspect_mask(ImageAspects {
                    color: true,
                    ..Default::default()
                })
                .layer_count(6)
                .build(),
        )
        .build()
    }

    fn create_empty_shadow_descriptor_set(
        graphics_context: &Arc<GraphicsContext>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
        empty_image: Arc<ImageView>,
    ) -> Result<Arc<DescriptorSet>, GfxError> {
        let descriptor_set = DescriptorSet::builder(
            graphics_context.device().clone(),
            graphics_context.descriptor_pool(&descriptor_set_layout)?,
            descriptor_set_layout,
        )
        .build()?;
        let empty_sampler = Sampler::builder(graphics_context.device().clone()).build()?;
        descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
            .descriptor_type(DescriptorType::CombinedImageSampler)
            .dst_binding(0)
            .sampled_image(
                &empty_sampler,
                &empty_image,
                ImageLayout::ShaderReadOnlyOptimal,
            )
            .build()]);
        Ok(descriptor_set)
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layouts: &[Arc<DescriptorSetLayout>; 2],
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layouts[0].clone())
            .set_layout(descriptor_set_layouts[1].clone())
            .push_constant_range(
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_graphics_pipelines(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<[Arc<GraphicsPipeline>; 2], GfxError> {
        Ok([
            Self::create_graphics_pipeline(device.clone(), pipeline_layout.clone(), 0)?,
            Self::create_graphics_pipeline(device.clone(), pipeline_layout.clone(), 1)?,
        ])
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
        with_shadow: u32,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32SFloat, 0)
                    .attribute(1, 0, Format::R32G32SFloat, 8)
                    .build(),
            )
            .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .fragment_shader_specialization(
                Specialization::builder()
                    .data(with_shadow)
                    .entry(0, 0, std::mem::size_of::<u32>() as _)
                    .build(),
            )
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 768.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .color_blend_attachment_state(
                ColorBlendAttachmentState::builder()
                    .enable_blend()
                    .alpha_blend_op(BlendOp::Add)
                    .build(),
            )
            .layout(pipeline_layout)
            .color_attachment_format(Format::R32G32B32A32SFloat)
            .build()
    }

    fn create_render_pass(&self, light_buffer: Arc<ImageView>) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: light_buffer.image().extent().into(),
            })
            .color_attachment(
                Attachment::builder(light_buffer)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .build()
    }
}

mod shaders {
    use crate::math::Vec3;

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub direction: Vec3,
        pub _dummy0: f32,
        pub color: Vec3,
        pub intensity: f32,
        pub eye: Vec3,
    }
}

mod fragment_shader {
    use crate::shader;

    shader! {
        path: "src/rendering/pbr/point_light.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
