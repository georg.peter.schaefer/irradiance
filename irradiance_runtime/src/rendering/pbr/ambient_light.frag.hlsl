[[vk::binding(0, 0)]]
Texture2D position_texture;
[[vk::binding(0, 0)]]
SamplerState position_sampler;
[[vk::binding(1, 0)]]
Texture2D base_color_texture;
[[vk::binding(1, 0)]]
SamplerState base_color_sampler;
[[vk::binding(2, 0)]]
Texture2D normal_texture;
[[vk::binding(2, 0)]]
SamplerState normal_sampler;
[[vk::binding(3, 0)]]
Texture2D metallic_roughness_texture;
[[vk::binding(3, 0)]]
SamplerState metallic_roughness_sampler;
[[vk::binding(4, 0)]]
TextureCube irradiance_map_texture;
[[vk::binding(4, 0)]]
SamplerState irradiance_map_sampler;
[[vk::binding(5, 0)]]
TextureCube pre_filtered_environment_map_texture;
[[vk::binding(5, 0)]]
SamplerState pre_filtered_environment_map_sampler;
[[vk::binding(6, 0)]]
Texture2D brdf_lut_texture;
[[vk::binding(6, 0)]]
SamplerState brdf_lut_sampler;
[[vk::binding(7, 0)]]
Texture2D ao_texture;
[[vk::binding(7, 0)]]
SamplerState ao_sampler;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

struct PushConstants {
    float3 eye;
    float max_reflection_lod;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

float3 pre_filtered_reflection(float3 R, float roughness) {
    float lod = roughness * push_constants.max_reflection_lod;
    float lodf = floor(lod);
    float lodc = ceil(lod);
    float3 a = pre_filtered_environment_map_texture.SampleLevel(pre_filtered_environment_map_sampler, R, lodf).rgb;
    float3 b = pre_filtered_environment_map_texture.SampleLevel(pre_filtered_environment_map_sampler, R, lodc).rgb;
    return lerp(a, b, lod - lodf);
}

float3 fresnel_schlick_roughness(float cos_theta, float3 F0, float roughness) {
    return F0 + (max(float3(1.0 - roughness), F0) - F0) * pow(saturate(1.0 - cos_theta), 5.0);
}

float4 main(VSOutput input) : SV_TARGET {
    float3 Cdiff = base_color_texture.Sample(base_color_sampler, input.tex_coord).rgb;
    float3 N = normal_texture.Sample(normal_sampler, input.tex_coord).xyz;

    if (length(N) <= 0.01) {
        return float4(Cdiff, 1.0);
    } else {
        N = normalize(N);
        float3 P = position_texture.Sample(position_sampler, input.tex_coord).xyz;
        float3 V = normalize(push_constants.eye - P);
        float3 R = reflect(-V, N);

        float roughness = metallic_roughness_texture.Sample(metallic_roughness_sampler, input.tex_coord).g;
        float metallic = metallic_roughness_texture.Sample(metallic_roughness_sampler, input.tex_coord).r;
        float3 irradiance = irradiance_map_texture.Sample(irradiance_map_sampler, N).rgb;
        float ao = ao_texture.Sample(ao_sampler, input.tex_coord).r;
        float NdotV = max(dot(N, V), 0.0);
        float3 F0 = lerp(float3(0.04), Cdiff, metallic);

        float2 brdf = brdf_lut_texture.Sample(brdf_lut_sampler, float2(NdotV, roughness)).rg;
        float3 reflection = pre_filtered_reflection(R, roughness);

        float3 diffuse = irradiance * Cdiff;

        float3 F = fresnel_schlick_roughness(NdotV, F0, roughness);

        float3 specular = reflection * (F * brdf.x + brdf.y);

        float3 kD = 1.0 - F;
        kD *= 1.0 - metallic;
        return float4((kD * diffuse + specular) * ao * 0.5, 1.0);
    }
}

