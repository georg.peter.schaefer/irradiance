use std::sync::Arc;

use fullscreen_pass::{create_fullscreen_vertex_buffer, vertex_shader};
use irradiance_runtime::gfx::pipeline::ColorBlendAttachmentState;

use crate::gfx::buffer::Buffer;
use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{
    Format, ImageAspects, ImageLayout, ImageSubresourceRange, ImageView, Sampler,
    SamplerAddressMode,
};
use crate::gfx::pipeline::{
    CullMode, DescriptorSet, DescriptorSetLayout, DescriptorType, DynamicState, FrontFace,
    GraphicsPipeline, PipelineBindPoint, PipelineLayout, PrimitiveTopology, ShaderStages,
    VertexInput, VertexInputRate, Viewport, WriteDescriptorSetImages,
};
use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use crate::math::Vec4;
use crate::rendering::fullscreen_pass;
use crate::rendering::fullscreen_pass::Vertex;
use crate::rendering::pbr::BloomBuffer;

/// Downsample pass.
#[derive(Debug)]
pub struct DownsamplePass {
    sampler: Arc<Sampler>,
    fullscreen_vertex_buffer: Arc<Buffer>,
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl DownsamplePass {
    /// Creates a new downsample pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(graphics_context.device().clone())?;
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            descriptor_set_layout.clone(),
        )?;
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            pipeline_layout.clone(),
        )?;
        let fullscreen_vertex_buffer = create_fullscreen_vertex_buffer(&graphics_context)?;
        let sampler = Sampler::builder(graphics_context.device().clone())
            .address_mode_u(SamplerAddressMode::ClampToEdge)
            .address_mode_v(SamplerAddressMode::ClampToEdge)
            .address_mode_w(SamplerAddressMode::ClampToEdge)
            .build()?;

        descriptor_set_layout.set_debug_utils_object_name("Downsample Pass Descriptor Set Layout");
        pipeline_layout.set_debug_utils_object_name("Downsample Pass pipeline Layout");
        graphics_pipeline.set_debug_utils_object_name("Downsample Pass Graphics Pipeline");
        fullscreen_vertex_buffer
            .set_debug_utils_object_name("Downsample Pass Fullscreen Vertex Buffer");
        sampler.set_debug_utils_object_name("Downsample Pass Sampler");

        Ok(Self {
            sampler,
            fullscreen_vertex_buffer,
            graphics_pipeline,
            pipeline_layout,
            descriptor_set_layout,
            graphics_context,
        })
    }

    /// Fill the bloom buffer.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        light_buffer: Arc<ImageView>,
        bloom_buffer: &BloomBuffer,
    ) -> Result<(), GfxError> {
        builder.begin_label("Downsample Pass", Vec4::new(0.2, 0.89, 0.21, 1.0));

        builder
            .bind_graphics_pipeline(self.graphics_pipeline.clone())
            .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(light_buffer.image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            );

        let mut src_image = light_buffer;
        let mut extent = bloom_buffer.image.extent();
        for (mip_level, dst_image) in bloom_buffer.views.iter().cloned().enumerate() {
            let descriptor_set = DescriptorSet::builder(
                self.graphics_context.device().clone(),
                self.graphics_context
                    .descriptor_pool(&self.descriptor_set_layout)?,
                self.descriptor_set_layout.clone(),
            )
            .build()?;
            descriptor_set.set_debug_utils_object_name("Downsample Pass Descriptor Set");
            descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &self.sampler,
                    &src_image,
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build()]);

            builder
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(dst_image.image().clone())
                                .src_stage_mask(PipelineStages {
                                    fragment_shader: true,
                                    ..Default::default()
                                })
                                .src_access_mask(AccessMask {
                                    shader_sampled_read: true,
                                    ..Default::default()
                                })
                                .dst_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::Undefined)
                                .new_layout(ImageLayout::ColorAttachmentOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .base_mip_level(mip_level as _)
                                        .level_count(1)
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                )
                .set_viewport(
                    0,
                    vec![Viewport {
                        x: 0.0,
                        y: 0.0,
                        width: extent.width as _,
                        height: extent.height as _,
                        min_depth: 0.0,
                        max_depth: 1.0,
                    }],
                )
                .set_scissor(
                    0,
                    vec![Rect2D {
                        offset: Default::default(),
                        extent: extent.into(),
                    }],
                )
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.pipeline_layout.clone(),
                    0,
                    vec![descriptor_set],
                )
                .begin_rendering(self.create_render_pass(dst_image.clone(), extent.into()))
                .draw(6, 1, 0, 0)
                .end_rendering()
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(dst_image.image().clone())
                                .src_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .src_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .dst_stage_mask(PipelineStages {
                                    fragment_shader: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    shader_sampled_read: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::ColorAttachmentOptimal)
                                .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .base_mip_level(mip_level as _)
                                        .level_count(1)
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                );

            src_image = dst_image;
            extent.width /= 2;
            extent.height /= 2;
        }

        builder.end_label();

        Ok(())
    }
}

impl DownsamplePass {
    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device.clone())
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .build()
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32SFloat, 0)
                    .attribute(1, 0, Format::R32G32SFloat, 8)
                    .build(),
            )
            .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 768.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R32G32B32A32SFloat)
            .build()
    }

    fn create_render_pass(&self, dst_image: Arc<ImageView>, extent: Extent2D) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent,
            })
            .color_attachment(
                Attachment::builder(dst_image)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::DontCare)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .build()
    }
}

mod fragment_shader {
    use crate::shader;

    shader! {
        path: "src/rendering/pbr/downsample.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
