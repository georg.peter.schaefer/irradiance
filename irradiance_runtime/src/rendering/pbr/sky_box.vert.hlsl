struct VSInput {
    [[vk::location(0)]] float3 position : POSITION0;
};

struct PushConstants {
    float4x4 projection;
    float4x4 view;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

struct VSOutput {
    float4 position : SV_POSITION;
    [[vk::location(0)]] float3 world_position : POSITION0;
};

VSOutput main(VSInput input) {
    VSOutput output = (VSOutput)0;

    float4x4 rotation_view = float4x4(float3x3(push_constants.view));
    output.position =  (float4(input.position, 1.0) * rotation_view * push_constants.projection).xyww;
    output.world_position = input.position;

    return output;
}
