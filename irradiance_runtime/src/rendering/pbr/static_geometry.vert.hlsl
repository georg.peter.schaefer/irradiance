struct VSInput {
    [[vk::location(0)]] float3 position : POSITION0;
    [[vk::location(1)]] float3 normal : NORMAL0;
    [[vk::location(2)]] float4 tangent : TANGENT0;
    [[vk::location(3)]] float2 tex_coord : TEXCOORD0;
};

struct PushConstants {
    float4x4 projection;
    float4x4 view;
    float4x4 model;
    float4 base_color;
    float metallic;
    float roughness;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

struct VSOutput {
    float4 position : SV_POSITION;
    [[vk::location(0)]] float3 world_position : POSITION0;
    [[vk::location(1)]] float3 normal : NORMAL0;
    [[vk::location(2)]] float4 tangent : TANGENT0;
    [[vk::location(3)]] float2 tex_coord : TEXCOORD0;
};

VSOutput main(VSInput input) {
    VSOutput output = (VSOutput)0;

    output.position =  float4(input.position, 1.0) * push_constants.model * push_constants.view * push_constants.projection;
    output.world_position = float3(float4(input.position, 1.0) * push_constants.model);
    output.normal = input.normal * float3x3(push_constants.model);
    output.tangent = float4(input.tangent.xyz * float3x3(push_constants.model), input.tangent.w);
    output.tex_coord = input.tex_coord;

    return output;
}
