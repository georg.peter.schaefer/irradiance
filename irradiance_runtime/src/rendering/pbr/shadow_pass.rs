use std::sync::Arc;

use crate::ecs::Entities;
use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::image::ImageView;
use crate::gfx::{GfxError, GraphicsContext};
use crate::math::Vec4;
use crate::rendering::pbr::{CascadedShadowPass, OmnidirectionalShadowPass};

/// Shadow pass.
#[derive(Debug)]
pub struct ShadowPass {
    omnidirectional_shadow_pass: OmnidirectionalShadowPass,
    cascaded_shadow_pass: CascadedShadowPass,
}

impl ShadowPass {
    /// Creates a new shadow pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        Ok(Self {
            cascaded_shadow_pass: CascadedShadowPass::new(graphics_context.clone())?,
            omnidirectional_shadow_pass: OmnidirectionalShadowPass::new(graphics_context)?,
        })
    }

    /// Draws the shadows.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        final_image: &ImageView,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Shadow Pass", Vec4::new(0.5, 0.2, 0.0, 1.0));

        self.cascaded_shadow_pass
            .draw(builder, final_image, entities)?;
        self.omnidirectional_shadow_pass.draw(builder, entities)?;

        builder.end_label();

        Ok(())
    }
}
