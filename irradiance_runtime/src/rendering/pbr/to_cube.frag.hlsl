[[vk::binding(0, 0)]]
Texture2D input_texture;
[[vk::binding(0, 0)]]
SamplerState input_sampler;

struct VSOutput {
    [[vk::location(0)]] float3 world_position : POSITION0;
};

const float2 inverse_atan = float2(0.1591, 0.3183);

float2 spherical_tex_coord(float3 direction) {
    float2 tex_coord = float2(atan2(direction.z, direction.x), asin(direction.y));
    tex_coord *= inverse_atan;
    tex_coord += 0.5;
    return tex_coord;
}

float4 main(VSOutput input) : SV_TARGET {
    return input_texture.Sample(input_sampler, spherical_tex_coord(normalize(input.world_position)));
}
