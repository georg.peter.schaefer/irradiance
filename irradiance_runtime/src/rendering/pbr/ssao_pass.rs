use crate::ecs::Entities;
use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::image::Format;
use crate::gfx::{GfxError, GraphicsContext};
use crate::math::Vec4;
use crate::rendering::pbr::{GBuffer, SsaoBuffer, SsaoGenerationPass};
use crate::rendering::BlurPass;
use std::sync::Arc;

/// Ssao pass.
#[derive(Debug)]
pub struct SsaoPass {
    blur_pass: BlurPass,
    ssao_generation_pass: SsaoGenerationPass,
}

impl SsaoPass {
    /// Creates a new ssao pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        Ok(Self {
            ssao_generation_pass: SsaoGenerationPass::new(graphics_context.clone())?,
            blur_pass: BlurPass::new(graphics_context, Format::R8UNorm)?,
        })
    }

    /// Generates the ssao buffer.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        entities: &Entities,
        g_buffer: &GBuffer,
        ssao_buffer: &SsaoBuffer,
    ) -> Result<(), GfxError> {
        builder.begin_label("Ssao Pass", Vec4::new(0.2, 0.4, 0.0, 1.0));

        self.ssao_generation_pass
            .draw(builder, entities, g_buffer, ssao_buffer)?;
        self.blur_pass.draw(
            builder,
            2,
            ssao_buffer.get().clone(),
            ssao_buffer.get_blur().clone(),
        )?;

        builder.end_label();

        Ok(())
    }
}
