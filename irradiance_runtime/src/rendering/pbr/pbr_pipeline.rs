use std::sync::Arc;

use crate::asset::Assets;
use crate::rendering::pbr::{
    AmbientLightPass, BloomBuffer, BloomPass, LightProbe, SkyBoxPass, SsaoBuffer, SsaoPass,
};
use crate::{
    ecs::Entities,
    gfx::command_buffer::CommandBufferBuilder,
    gfx::device::DeviceOwned,
    gfx::image::{Format, Image, ImageType, ImageUsage, ImageView, ImageViewType},
    gfx::{Extent2D, GfxError, GraphicsContext},
    math::Vec4,
    rendering::pbr::{FinalPass, GeometryPass, LightPass},
    rendering::pbr::{GBuffer, ShadowPass},
    window::Window,
};

/// Physically based rendering pipeline.
#[derive(Debug)]
pub struct PbrPipeline {
    final_pass: FinalPass,
    bloom_pass: BloomPass,
    ambient_light_pass: AmbientLightPass,
    light_probe: LightProbe,
    ssao_pass: SsaoPass,
    light_pass: LightPass,
    shadow_pass: ShadowPass,
    sky_box_pass: SkyBoxPass,
    geometry_pass: GeometryPass,
}

impl PbrPipeline {
    /// Creates a new physically based rendering pipeline.
    pub fn new(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        format: Format,
    ) -> Result<Arc<PbrPipeline>, GfxError> {
        Ok(Arc::new(Self {
            final_pass: FinalPass::new(graphics_context.clone(), format)?,
            bloom_pass: BloomPass::new(graphics_context.clone())?,
            ambient_light_pass: AmbientLightPass::new(graphics_context.clone())?,
            light_probe: LightProbe::new(graphics_context.clone(), assets.clone(), 512)?,
            ssao_pass: SsaoPass::new(graphics_context.clone())?,
            light_pass: LightPass::new(graphics_context.clone())?,
            shadow_pass: ShadowPass::new(graphics_context.clone())?,
            sky_box_pass: SkyBoxPass::new(graphics_context.clone(), assets)?,
            geometry_pass: GeometryPass::new(graphics_context)?,
        }))
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        g_buffer: &GBuffer,
        light_buffer: Arc<ImageView>,
        ssao_buffer: &SsaoBuffer,
        bloom_buffer: &BloomBuffer,
        final_image: Arc<ImageView>,
        clear_color: Vec4,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Pbr Pipeline", Vec4::new(1.0, 0.5, 0.0, 1.0));
        self.geometry_pass.draw(builder, g_buffer, entities)?;
        self.sky_box_pass
            .draw(builder, g_buffer, &self.light_probe, entities)?;
        self.shadow_pass.draw(builder, &final_image, entities)?;
        self.light_pass.draw(
            builder,
            g_buffer,
            light_buffer.clone(),
            clear_color,
            entities,
        )?;
        self.ssao_pass
            .draw(builder, entities, g_buffer, ssao_buffer)?;
        self.ambient_light_pass.draw(
            builder,
            g_buffer,
            ssao_buffer,
            &self.light_probe,
            light_buffer.clone(),
            entities,
        )?;
        self.bloom_pass
            .draw(builder, light_buffer.clone(), bloom_buffer)?;
        self.final_pass
            .draw(builder, light_buffer, bloom_buffer, final_image)?;
        builder.end_label();

        Ok(())
    }
}

/// Physically based rendering pipeline with buffers.
pub struct BufPbrPipeline {
    pbr_pipeline: Arc<PbrPipeline>,
    clear_color: Vec4,
    bloom_buffer: BloomBuffer,
    ssao_buffer: SsaoBuffer,
    light_buffer: Arc<ImageView>,
    g_buffer: GBuffer,
}

impl BufPbrPipeline {
    /// Creates a new new physically based rendering pipeline with buffers.
    pub fn with_size(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        extent: Extent2D,
    ) -> Arc<Self> {
        let device = graphics_context.device();
        let g_buffer = GBuffer::new(device.clone(), extent).expect("g buffer");
        let light_buffer = ImageView::builder(
            device.clone(),
            Image::builder(device.clone())
                .image_type(ImageType::Type2D)
                .format(Format::R32G32B32A32SFloat)
                .extent(extent.into())
                .usage(ImageUsage {
                    color_attachment: true,
                    sampled: true,
                    ..Default::default()
                })
                .build()
                .expect("light buffer"),
        )
        .view_type(ImageViewType::Type2D)
        .build()
        .expect("light buffer image view");
        let ssao_buffer =
            SsaoBuffer::new(graphics_context.device().clone(), extent).expect("ssao buffer");
        let pbr_pipeline = PbrPipeline::new(
            graphics_context.clone(),
            assets,
            graphics_context.swapchain().unwrap().image_format(),
        )
        .expect("pbr pipeline");

        light_buffer
            .image()
            .set_debug_utils_object_name("Buffered Pbr Pipeline Light Buffer Image");
        light_buffer.set_debug_utils_object_name("Buffered Pbr Pipeline Light Buffer Image View");

        Self::new(
            g_buffer,
            light_buffer,
            ssao_buffer,
            BloomBuffer::new(device.clone(), extent).expect("bloom buffer"),
            Vec4::new(0.0, 0.0, 0.0, 1.0),
            pbr_pipeline,
        )
    }

    /// Creates a new new physically based rendering pipeline with buffers with the resolution and image format of the window.
    pub fn from_window(
        window: &Window,
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
    ) -> Arc<Self> {
        let device = graphics_context.device();
        let extent = Extent2D {
            width: window.resolution()[0],
            height: window.resolution()[1],
        };
        let g_buffer = GBuffer::new(device.clone(), extent).expect("g buffer");
        let light_buffer = ImageView::builder(
            device.clone(),
            Image::builder(device.clone())
                .image_type(ImageType::Type2D)
                .format(Format::R32G32B32A32SFloat)
                .extent(extent.into())
                .usage(ImageUsage {
                    color_attachment: true,
                    sampled: true,
                    ..Default::default()
                })
                .build()
                .expect("light buffer"),
        )
        .view_type(ImageViewType::Type2D)
        .build()
        .expect("light buffer image view");
        let ssao_buffer = SsaoBuffer::new(device.clone(), extent).expect("ssao buffer");
        let pbr_pipeline = PbrPipeline::new(
            graphics_context.clone(),
            assets,
            graphics_context
                .swapchain()
                .expect("surface")
                .image_format(),
        )
        .expect("pbr pipeline");

        light_buffer
            .image()
            .set_debug_utils_object_name("Buffered Pbr Pipeline Light Buffer Image");
        light_buffer.set_debug_utils_object_name("Buffered Pbr Pipeline Light Buffer Image View");

        Self::new(
            g_buffer,
            light_buffer,
            ssao_buffer,
            BloomBuffer::new(device.clone(), extent).expect("bloom buffer"),
            Vec4::new(0.0, 0.0, 0.0, 1.0),
            pbr_pipeline,
        )
    }

    /// Resizes the buffers.
    pub fn resize(self: Arc<Self>, extent: Extent2D) -> Arc<Self> {
        let device = self.light_buffer().device();
        let g_buffer = GBuffer::new(device.clone(), extent).expect("g buffer");
        let light_buffer = ImageView::builder(
            device.clone(),
            Image::builder(device.clone())
                .image_type(ImageType::Type2D)
                .format(Format::R32G32B32A32SFloat)
                .extent(extent.into())
                .usage(ImageUsage {
                    color_attachment: true,
                    sampled: true,
                    ..Default::default()
                })
                .build()
                .expect("light buffer"),
        )
        .view_type(ImageViewType::Type2D)
        .build()
        .expect("light buffer image view");
        let ssao_buffer = SsaoBuffer::new(device.clone(), extent).expect("ssao buffer");
        let pbr_pipeline = self.pbr_pipeline.clone();

        light_buffer
            .image()
            .set_debug_utils_object_name("Buffered Pbr Pipeline Light Buffer Image");
        light_buffer.set_debug_utils_object_name("Buffered Pbr Pipeline Light Buffer Image View");

        Self::new(
            g_buffer,
            light_buffer,
            ssao_buffer,
            BloomBuffer::new(device.clone(), extent).expect("bloom buffer"),
            self.clear_color,
            pbr_pipeline,
        )
    }

    /// Creates a new physically based rendering pipeline with buffers.
    pub fn new(
        g_buffer: GBuffer,
        light_buffer: Arc<ImageView>,
        ssao_buffer: SsaoBuffer,
        bloom_buffer: BloomBuffer,
        clear_color: Vec4,
        pbr_pipeline: Arc<PbrPipeline>,
    ) -> Arc<Self> {
        Arc::new(Self {
            pbr_pipeline,
            clear_color,
            bloom_buffer,
            ssao_buffer,
            light_buffer,
            g_buffer,
        })
    }

    /// Returns the g buffer.
    pub fn g_buffer(&self) -> &GBuffer {
        &self.g_buffer
    }

    /// Returns the light buffer.
    pub fn light_buffer(&self) -> &Arc<ImageView> {
        &self.light_buffer
    }

    /// Returns the pbr pipeline.
    pub fn pbr_pipeline(&self) -> &Arc<PbrPipeline> {
        &self.pbr_pipeline
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        final_image: Arc<ImageView>,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        self.pbr_pipeline.draw(
            builder,
            &self.g_buffer,
            self.light_buffer.clone(),
            &self.ssao_buffer,
            &self.bloom_buffer,
            final_image.clone(),
            self.clear_color,
            entities,
        )
    }
}
