use std::sync::Arc;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{
    Format, Image, ImageAspects, ImageSubresourceRange, ImageType, ImageUsage, ImageView,
    ImageViewType,
};
use crate::gfx::{Extent2D, GfxError};

/// Geometry buffer.
#[derive(Debug)]
pub struct GBuffer {
    depth_buffer: Arc<ImageView>,
    metallic_roughness: Arc<ImageView>,
    normal: Arc<ImageView>,
    base_color: Arc<ImageView>,
    position: Arc<ImageView>,
    extent: Extent2D,
}

impl GBuffer {
    /// Creates a new g buffer.
    pub fn new(device: Arc<Device>, extent: Extent2D) -> Result<GBuffer, GfxError> {
        let g_buffer = Self {
            depth_buffer: Self::create_depth_image_view(
                device.clone(),
                Format::D32SFloat,
                extent,
                ImageUsage {
                    depth_stencil_attachment: true,
                    ..Default::default()
                },
            )?,
            metallic_roughness: Self::create_color_image_view(
                device.clone(),
                Format::R8G8B8A8UNorm,
                extent,
                ImageUsage {
                    color_attachment: true,
                    sampled: true,
                    ..Default::default()
                },
            )?,
            normal: Self::create_color_image_view(
                device.clone(),
                Format::R16G16B16A16SFloat,
                extent,
                ImageUsage {
                    color_attachment: true,
                    sampled: true,
                    ..Default::default()
                },
            )?,
            base_color: Self::create_color_image_view(
                device.clone(),
                Format::R8G8B8A8UNorm,
                extent,
                ImageUsage {
                    color_attachment: true,
                    sampled: true,
                    ..Default::default()
                },
            )?,
            position: Self::create_color_image_view(
                device,
                Format::R16G16B16A16SFloat,
                extent,
                ImageUsage {
                    color_attachment: true,
                    sampled: true,
                    ..Default::default()
                },
            )?,
            extent,
        };

        g_buffer
            .position
            .image()
            .set_debug_utils_object_name("G Buffer Position Image");
        g_buffer
            .position
            .set_debug_utils_object_name("G Buffer Position Image View");
        g_buffer
            .base_color
            .image()
            .set_debug_utils_object_name("G Buffer Base Color Image");
        g_buffer
            .base_color
            .set_debug_utils_object_name("G Buffer Base Color Image View");
        g_buffer
            .normal
            .image()
            .set_debug_utils_object_name("G Buffer Normal Image");
        g_buffer
            .normal
            .set_debug_utils_object_name("G Buffer Normal Image View");
        g_buffer
            .metallic_roughness
            .image()
            .set_debug_utils_object_name("G Buffer Metallic Roughness Image");
        g_buffer
            .metallic_roughness
            .set_debug_utils_object_name("G Buffer Metallic Roughness Image View");
        g_buffer
            .depth_buffer
            .image()
            .set_debug_utils_object_name("G Buffer Depth Buffer Image");
        g_buffer
            .depth_buffer
            .set_debug_utils_object_name("G Buffer Depth Buffer Image View");

        Ok(g_buffer)
    }

    /// Returns the extent.
    pub fn extent(&self) -> Extent2D {
        self.extent
    }

    /// Returns the position image.
    pub fn position(&self) -> &Arc<ImageView> {
        &self.position
    }

    /// Returns the base color image.
    pub fn base_color(&self) -> &Arc<ImageView> {
        &self.base_color
    }

    /// Returns the normal image.
    pub fn normal(&self) -> &Arc<ImageView> {
        &self.normal
    }

    /// Returns the metallic roughness image.
    pub fn metallic_roughness(&self) -> &Arc<ImageView> {
        &self.metallic_roughness
    }

    /// Returns the depth buffer image.
    pub fn depth_buffer(&self) -> &Arc<ImageView> {
        &self.depth_buffer
    }

    fn create_color_image_view(
        device: Arc<Device>,
        format: Format,
        extent: Extent2D,
        usage: ImageUsage,
    ) -> Result<Arc<ImageView>, GfxError> {
        Self::create_image_view(
            device,
            format,
            extent,
            usage,
            ImageSubresourceRange::builder()
                .aspect_mask(ImageAspects {
                    color: true,
                    ..Default::default()
                })
                .build(),
        )
    }

    fn create_depth_image_view(
        device: Arc<Device>,
        format: Format,
        extent: Extent2D,
        usage: ImageUsage,
    ) -> Result<Arc<ImageView>, GfxError> {
        Self::create_image_view(
            device,
            format,
            extent,
            usage,
            ImageSubresourceRange::builder()
                .aspect_mask(ImageAspects {
                    depth: true,
                    ..Default::default()
                })
                .build(),
        )
    }

    fn create_image_view(
        device: Arc<Device>,
        format: Format,
        extent: Extent2D,
        usage: ImageUsage,
        subresource_range: ImageSubresourceRange,
    ) -> Result<Arc<ImageView>, GfxError> {
        let image = Image::builder(device.clone())
            .image_type(ImageType::Type2D)
            .format(format)
            .extent(extent.into())
            .usage(usage)
            .build()?;
        ImageView::builder(device, image)
            .view_type(ImageViewType::Type2D)
            .subresource_range(subresource_range)
            .build()
    }
}
