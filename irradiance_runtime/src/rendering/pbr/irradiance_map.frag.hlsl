[[vk::binding(0, 0)]]
TextureCube environment_map_texture;
[[vk::binding(0, 0)]]
SamplerState environment_map_sampler;

struct VSOutput {
    [[vk::location(0)]] float3 world_position : POSITION0;
};

const float PI = 3.14159265358979323846;

float3 main(VSOutput input) : SV_TARGET {
    float3 normal = normalize(input.world_position);
    float3 up = float3(0.0, 1.0, 0.0);
    float3 right = normalize(cross(up, normal));
    up = normalize(cross(normal, right));

    const float TWO_PI = 2.0 * PI;
    const float HALF_PI = 0.5 * PI;
    const float DELTA_PHI = TWO_PI / 180.0;
    const float DELTA_THETA = HALF_PI / 64.0;

    float3 irradiance = float3(0.0);
    uint samples = 0;
    for (float phi = 0.0; phi < TWO_PI; phi += DELTA_PHI) {
        for (float theta = 0.0; theta < HALF_PI; theta += DELTA_THETA) {
            float3 tangent_sample = float3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
            float3 sample_vector = tangent_sample.x * right + tangent_sample.y * up + tangent_sample.z * normal;
            irradiance += environment_map_texture.Sample(environment_map_sampler, sample_vector).rgb * cos(theta) * sin(theta);
            samples++;
        }
    }
    irradiance = PI * irradiance / float(samples);

    return irradiance;
}
