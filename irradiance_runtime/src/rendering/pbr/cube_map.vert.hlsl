struct VSInput {
    [[vk::location(0)]] float2 position : POSITION0;
};

struct PushConstants {
    float4x4 orientation;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

struct VSOutput {
    float4 position : SV_POSITION;
    [[vk::location(0)]] float3 world_position : POSITION0;
};

VSOutput main(VSInput input) {
    VSOutput output = (VSOutput)0;

    output.world_position = (float4(input.position, -1.0, 1.0) * push_constants.orientation).xyz;
    output.position = float4(input.position, 0.0, 1.0);
    output.position.y *= -1.0;

    return output;
}
