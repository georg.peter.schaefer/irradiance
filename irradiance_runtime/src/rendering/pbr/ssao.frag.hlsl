[[vk::binding(0, 0)]]
Texture2D position_texture;
[[vk::binding(0, 0)]]
SamplerState position_sampler;
[[vk::binding(1, 0)]]
Texture2D normal_texture;
[[vk::binding(1, 0)]]
SamplerState normal_sampler;
[[vk::binding(2, 0)]]
Texture2D noise_texture;
[[vk::binding(2, 0)]]
SamplerState noise_sampler;
[[vk::binding(3, 0)]]
StructuredBuffer<float3> samples;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

struct PushConstants {
    float4x4 projection;
    float4x4 view;
    float radius;
    float bias;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

float main(VSOutput input) : SV_TARGET {
    float2 noise_dimensions;
    noise_texture.GetDimensions(noise_dimensions.x, noise_dimensions.y);
    float2 screen_resolution;
    position_texture.GetDimensions(screen_resolution.x, screen_resolution.y);
    float2 noise_scale = screen_resolution / noise_dimensions;

    float3 P = (float4(position_texture.Sample(position_sampler, input.tex_coord).xyz, 1.0) * push_constants.view).xyz;
    float3 N = (float4(normal_texture.Sample(normal_sampler, input.tex_coord).xyz, 0.0) * push_constants.view).xyz;
    float3 noise_vector = noise_texture.Sample(noise_sampler, input.tex_coord * noise_scale).xyz;

    float3 T = normalize(noise_vector - N * dot(noise_vector, N));
    float3 B = cross(T, N);
    float3x3 TBN = transpose(float3x3(T, B, N));

    uint2 kernel_size;
    samples.GetDimensions(kernel_size.x, kernel_size.y);
    float occlusion = 0.0;
    for (int i = 0; i < kernel_size.x; ++i) {
        float3 sample_position = P + samples[i] * TBN * push_constants.radius;
        float4 offset = float4(sample_position, 1.0) * push_constants.projection;
        offset.xyz /= offset.w;
        offset.xy = offset.xy * 0.5 + 0.5;

        float sample_depth = (float4(position_texture.Sample(position_sampler, offset.xy).xyz, 1.0) * push_constants.view).z;
        float range_check = smoothstep(0.0, 1.0, push_constants.radius / abs(P.z - sample_depth));
        occlusion += (sample_depth >= sample_position.z + push_constants.bias ? 1.0 : 0.0) * range_check;
    }
    occlusion = 1.0 - (occlusion / kernel_size.x);

    return occlusion;
}
