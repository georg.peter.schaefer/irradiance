use crate::ecs::{Entities, Transform};
use rand::distributions::Uniform;
use rand::{thread_rng, Rng};
use std::sync::Arc;

use crate::gfx::buffer::{Buffer, BufferUsage};
use crate::gfx::command_buffer::{BufferImageCopy, CommandBufferBuilder};
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{
    Format, Image, ImageAspects, ImageLayout, ImageSubresourceLayers, ImageSubresourceRange,
    ImageType, ImageUsage, ImageView, Sampler,
};
use crate::gfx::pipeline::{
    ColorBlendAttachmentState, CullMode, DescriptorSet, DescriptorSetLayout, DescriptorType,
    DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
    PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
    WriteDescriptorSetBuffers, WriteDescriptorSetImages,
};
use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::{Extent2D, Extent3D, GfxError, GraphicsContext, Rect2D};
use crate::math::{Mat4, Vec4};
use crate::rendering::fullscreen_pass::{create_fullscreen_vertex_buffer, vertex_shader, Vertex};
use crate::rendering::pbr::ssao_generation_pass::shaders::PushConstants;
use crate::rendering::pbr::{GBuffer, SsaoBuffer};
use crate::rendering::Camera;

/// Screen space ambient occlusion render pass.
#[derive(Debug)]
pub struct SsaoGenerationPass {
    samples: Arc<Buffer>,
    noise: Arc<ImageView>,
    sampler: Arc<Sampler>,
    fullscreen_vertex_buffer: Arc<Buffer>,
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl SsaoGenerationPass {
    /// Creates a new ssao pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(graphics_context.device().clone())?;
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            descriptor_set_layout.clone(),
        )?;
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            pipeline_layout.clone(),
        )?;
        let fullscreen_vertex_buffer = create_fullscreen_vertex_buffer(&graphics_context)?;
        let sampler = Sampler::builder(graphics_context.device().clone()).build()?;
        let noise = Self::create_noise(&graphics_context, 4)?;
        let samples = Self::create_samples(&graphics_context, 64)?;

        descriptor_set_layout
            .set_debug_utils_object_name("Ssao Generation Pass Descriptor Set Layout");
        pipeline_layout.set_debug_utils_object_name("Ssao Generation Pass Pipeline Layout");
        graphics_pipeline.set_debug_utils_object_name("Ssao Generation Pass Graphics Pipeline");
        fullscreen_vertex_buffer
            .set_debug_utils_object_name("Ssao Generation Pass Fullscreen Vertex Buffer");
        samples.set_debug_utils_object_name("Ssao Generation Pass Sampler");
        noise
            .image()
            .set_debug_utils_object_name("Ssao Generation Pass Noise Image");
        noise.set_debug_utils_object_name("Ssao Generation Pass Noise Image View");
        samples.set_debug_utils_object_name("Ssao Generation Pass Samples Buffer");

        Ok(Self {
            samples,
            noise,
            sampler,
            fullscreen_vertex_buffer,
            graphics_pipeline,
            pipeline_layout,
            descriptor_set_layout,
            graphics_context,
        })
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        entities: &Entities,
        g_buffer: &GBuffer,
        ssao_buffer: &SsaoBuffer,
    ) -> Result<(), GfxError> {
        builder.begin_label("Ssao Generation Pass", Vec4::new(0.7, 0.25, 0.0, 1.0));

        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.descriptor_set_layout)?,
            self.descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Ssao Generation Pass Descriptor Set Layout");

        descriptor_set.write_images(vec![
            WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &self.sampler,
                    g_buffer.position(),
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build(),
            WriteDescriptorSetImages::builder()
                .dst_binding(1)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &self.sampler,
                    g_buffer.normal(),
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build(),
            WriteDescriptorSetImages::builder()
                .dst_binding(2)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &self.sampler,
                    &self.noise,
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build(),
        ]);
        descriptor_set.write_buffers(vec![WriteDescriptorSetBuffers::builder()
            .dst_binding(3)
            .descriptor_type(DescriptorType::StorageBuffer)
            .buffer(&self.samples, 0, ash::vk::WHOLE_SIZE)
            .build()]);

        builder.pipeline_barrier(
            PipelineBarrier::builder()
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(ssao_buffer.get().image().clone())
                        .src_stage_mask(PipelineStages {
                            fragment_shader: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask {
                            shader_sampled_read: true,
                            ..Default::default()
                        })
                        .dst_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::Undefined)
                        .new_layout(ImageLayout::ColorAttachmentOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .build(),
        );
        if let Some((Some(camera), Some(camera_transform))) = entities
            .active::<Camera>()
            .map(|camera| (camera.get::<Camera>(), camera.get::<Transform>()))
        {
            builder
                .bind_graphics_pipeline(self.graphics_pipeline.clone())
                .begin_rendering(self.create_render_pass(ssao_buffer.get().clone()))
                .set_viewport(
                    0,
                    vec![Viewport {
                        x: 0.0,
                        y: 0.0,
                        width: ssao_buffer.get().image().extent().width as _,
                        height: ssao_buffer.get().image().extent().height as _,
                        min_depth: 0.0,
                        max_depth: 1.0,
                    }],
                )
                .set_scissor(
                    0,
                    vec![Rect2D {
                        offset: Default::default(),
                        extent: ssao_buffer.get().image().extent().into(),
                    }],
                )
                .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.pipeline_layout.clone(),
                    0,
                    vec![descriptor_set],
                )
                .push_constants(
                    self.pipeline_layout.clone(),
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                    0,
                    PushConstants {
                        projection: camera.projection(
                            g_buffer.extent().width as f32 / g_buffer.extent().height as f32,
                        ),
                        view: Mat4::from(*camera_transform).inverse(),
                        radius: 0.5,
                        bias: 0.08,
                    },
                )
                .draw(6, 1, 0, 0)
                .end_rendering();
        }

        builder.end_label();

        Ok(())
    }
}

impl SsaoGenerationPass {
    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                1,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                2,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                3,
                DescriptorType::StorageBuffer,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32SFloat, 0)
                    .attribute(1, 0, Format::R32G32SFloat, 8)
                    .build(),
            )
            .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 768.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R8UNorm)
            .build()
    }

    fn create_noise(
        graphics_context: &Arc<GraphicsContext>,
        noise_size: u32,
    ) -> Result<Arc<ImageView>, GfxError> {
        let mut rng = thread_rng();
        let side = Uniform::new(0.0, 1.0);
        let noise = (0..noise_size * noise_size)
            .map(|_| {
                Vec4::new(
                    rng.sample(side) * 2.0 - 1.0,
                    rng.sample(side) * 2.0 - 1.0,
                    0.0,
                    0.0,
                )
            })
            .collect();

        let staging_buffer = Buffer::from_data(graphics_context.device().clone(), noise)
            .usage(BufferUsage {
                transfer_src: true,
                ..Default::default()
            })
            .build()?;
        let image = Image::builder(graphics_context.device().clone())
            .image_type(ImageType::Type2D)
            .format(Format::R32G32B32A32SFloat)
            .extent(Extent3D {
                width: noise_size,
                height: noise_size,
                depth: 1,
            })
            .usage(ImageUsage {
                transfer_dst: true,
                sampled: true,
                ..Default::default()
            })
            .build()?;
        staging_buffer.copy_to_image(
            graphics_context.graphics_queue().clone(),
            graphics_context.command_pool()?,
            image.clone(),
            BufferImageCopy::builder()
                .image_subresource(
                    ImageSubresourceLayers::builder()
                        .image_aspects(ImageAspects {
                            color: true,
                            ..Default::default()
                        })
                        .build(),
                )
                .image_extent(image.extent())
                .build(),
        )?;
        ImageView::builder(graphics_context.device().clone(), image).build()
    }

    fn create_samples(
        graphics_context: &Arc<GraphicsContext>,
        kernel_size: u32,
    ) -> Result<Arc<Buffer>, GfxError> {
        let mut rng = thread_rng();
        let distribution = Uniform::new(0.0, 1.0);
        Buffer::from_data(
            graphics_context.device().clone(),
            (0..kernel_size)
                .map(|index| {
                    let sample = Vec4::new(
                        rng.sample(distribution) * 2.0 - 1.0,
                        rng.sample(distribution) * 2.0 - 1.0,
                        rng.sample(distribution),
                        0.0,
                    )
                    .normalize()
                        * rng.sample(distribution);
                    let scale = index as f32 / kernel_size as f32;
                    let scale = Self::lerp(0.1, 1.0, scale * scale);
                    sample * scale
                })
                .collect(),
        )
        .usage(BufferUsage {
            storage_buffer: true,
            ..Default::default()
        })
        .build()
    }

    fn lerp(a: f32, b: f32, f: f32) -> f32 {
        a + f * (b - a)
    }

    fn create_render_pass(&self, ssao_buffer: Arc<ImageView>) -> RenderPass {
        let image_extent = ssao_buffer.image().extent();
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: image_extent.into(),
            })
            .color_attachment(
                Attachment::builder(ssao_buffer)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::None)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .build()
    }
}

mod shaders {
    use crate::math::Mat4;

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub projection: Mat4,
        pub view: Mat4,
        pub radius: f32,
        pub bias: f32,
    }
}

mod fragment_shader {
    use crate::shader;

    shader! {
        path: "src/rendering/pbr/ssao.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
