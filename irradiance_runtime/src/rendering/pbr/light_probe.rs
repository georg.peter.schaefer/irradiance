use std::path::Path;
use std::sync::Arc;

use crate::asset::{Asset, Assets};
use crate::gfx::command_buffer::CommandBuffer;
use crate::gfx::image::{ImageView, Sampler, SamplerAddressMode, SamplerMipMapMode};
use crate::gfx::queue::{CommandBufferSubmit, Submit};
use crate::gfx::{GfxError, GraphicsContext};
use crate::math::Vec4;
use crate::rendering::pbr::light_probe::brdf_lut_pass::BrdfLutPass;
use crate::rendering::pbr::light_probe::irradiance_map_pass::IrradianceMapPass;
use crate::rendering::pbr::light_probe::pre_filtered_environment_map_pass::PreFilteredEnvironmentMapPass;
use crate::rendering::pbr::light_probe::to_cube_map_pass::ToCubeMapPass;
use crate::rendering::Texture;

/// Light probe.
#[derive(Debug)]
pub struct LightProbe {
    mip_map_sampler: Arc<Sampler>,
    sampler: Arc<Sampler>,
    brdf_lut: Arc<ImageView>,
    pre_filtered_environment_map: Arc<ImageView>,
    irradiance_map: Arc<ImageView>,
    environment_map: Arc<ImageView>,
}

impl LightProbe {
    /// Creates a new light probe.
    pub fn new(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        resolution: u32,
    ) -> Result<Self, GfxError> {
        let mut builder = CommandBuffer::primary(
            graphics_context.device().clone(),
            graphics_context.command_pool()?,
        );

        builder.begin_label("Light Probe", Vec4::new(0.0, 0.0, 0.4, 1.0));

        let environment_map = Texture::try_from_bytes(
            assets,
            graphics_context.clone(),
            Path::new("__internal__.exr"),
            include_bytes!("../../../rsc/rendering/pbr/environment_map.exr").to_vec(),
        )
        .expect("environment map");
        let environment_map = ToCubeMapPass::new(graphics_context.clone())?.draw(
            &mut builder,
            environment_map,
            resolution,
        )?;
        let irradiance_map = IrradianceMapPass::new(graphics_context.clone())?.draw(
            &mut builder,
            environment_map.clone(),
            resolution,
        )?;
        let pre_filtered_environment_map = PreFilteredEnvironmentMapPass::new(
            graphics_context.clone(),
        )?
        .draw(&mut builder, environment_map.clone(), 128)?;
        let brdf_lut = BrdfLutPass::new(graphics_context.clone())?.draw(&mut builder, 512)?;

        builder.end_label();

        graphics_context.graphics_queue().submit(
            Submit::builder()
                .command_buffer(CommandBufferSubmit::builder(builder.build()?).build())
                .build(),
            None,
        )?;
        graphics_context.device().wait()?;

        let sampler = Sampler::builder(graphics_context.device().clone())
            .address_mode_u(SamplerAddressMode::ClampToEdge)
            .address_mode_v(SamplerAddressMode::ClampToEdge)
            .address_mode_w(SamplerAddressMode::ClampToEdge)
            .build()?;
        let mip_map_sampler = Sampler::builder(graphics_context.device().clone())
            .mip_map_mode(SamplerMipMapMode::Linear)
            .max_lod(pre_filtered_environment_map.image().mip_levels() as _)
            .build()?;

        Ok(Self {
            mip_map_sampler,
            sampler,
            brdf_lut,
            pre_filtered_environment_map,
            irradiance_map,
            environment_map,
        })
    }

    /// Returns the environment map.
    pub fn environment_map(&self) -> &Arc<ImageView> {
        &self.environment_map
    }

    /// Returns the irradiance map.
    pub fn irradiance_map(&self) -> &Arc<ImageView> {
        &self.irradiance_map
    }

    /// Returns the pre filtered environment map.
    pub fn pre_filtered_environment_map(&self) -> &Arc<ImageView> {
        &self.pre_filtered_environment_map
    }

    /// Returns the brdf lut.
    pub fn brdf_lut(&self) -> &Arc<ImageView> {
        &self.brdf_lut
    }

    /// Returns the sampler.
    pub fn sampler(&self) -> &Arc<Sampler> {
        &self.sampler
    }

    /// Returns the mip map sampler.
    pub fn mip_map_sampler(&self) -> &Arc<Sampler> {
        &self.mip_map_sampler
    }
}

mod to_cube_map_pass {
    use std::sync::Arc;

    use crate::gfx::buffer::Buffer;
    use crate::gfx::device::DeviceOwned;
    use crate::gfx::image::ImageLayout;
    use crate::gfx::pipeline::{
        ColorBlendAttachmentState, CullMode, DescriptorSet, DescriptorSetLayout, DescriptorType,
        DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
        PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
        WriteDescriptorSetImages,
    };
    use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
    use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
    use crate::gfx::Rect2D;
    use crate::math::{Mat4, Vec3};
    use crate::rendering::fullscreen_pass;
    use crate::rendering::fullscreen_pass::Vertex;
    use crate::rendering::pbr::light_probe::to_cube_map_pass::shaders::PushConstants;
    use crate::{
        gfx::command_buffer::CommandBufferBuilder,
        gfx::device::Device,
        gfx::image::{
            Format, Image, ImageAspects, ImageCreateFlags, ImageSubresourceRange, ImageType,
            ImageUsage, ImageView, ImageViewType,
        },
        gfx::{Extent2D, GfxError, GraphicsContext},
        rendering::Texture,
    };

    pub struct ToCubeMapPass {
        fullscreen_vertex_buffer: Arc<Buffer>,
        graphics_pipeline: Arc<GraphicsPipeline>,
        pipeline_layout: Arc<PipelineLayout>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
        graphics_context: Arc<GraphicsContext>,
    }

    impl ToCubeMapPass {
        pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
            let descriptor_set_layout =
                Self::create_descriptor_set_layout(graphics_context.device().clone())?;
            let pipeline_layout = Self::create_pipeline_layout(
                graphics_context.device().clone(),
                descriptor_set_layout.clone(),
            )?;
            let graphics_pipeline = Self::create_graphics_pipeline(
                graphics_context.device().clone(),
                pipeline_layout.clone(),
            )?;
            let fullscreen_vertex_buffer =
                fullscreen_pass::create_fullscreen_vertex_buffer(&graphics_context)?;

            descriptor_set_layout
                .set_debug_utils_object_name("To Cube Map Pass Descriptor Set Layout");
            pipeline_layout.set_debug_utils_object_name("To Cube Map Pass Pipeline Layout");
            graphics_pipeline.set_debug_utils_object_name("To Cube Map Graphics Pipeline");
            fullscreen_vertex_buffer
                .set_debug_utils_object_name("To Cube Map Fullscreen Vertex Buffer");

            Ok(Self {
                fullscreen_vertex_buffer,
                graphics_pipeline,
                pipeline_layout,
                descriptor_set_layout,
                graphics_context,
            })
        }

        fn create_descriptor_set_layout(
            device: Arc<Device>,
        ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
            DescriptorSetLayout::builder(device)
                .binding(
                    0,
                    DescriptorType::CombinedImageSampler,
                    1,
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                )
                .build()
        }

        fn create_pipeline_layout(
            device: Arc<Device>,
            descriptor_set_layout: Arc<DescriptorSetLayout>,
        ) -> Result<Arc<PipelineLayout>, GfxError> {
            PipelineLayout::builder(device)
                .set_layout(descriptor_set_layout)
                .push_constant_range(
                    ShaderStages {
                        vertex: true,
                        ..Default::default()
                    },
                    0,
                    std::mem::size_of::<PushConstants>() as _,
                )
                .build()
        }

        fn create_graphics_pipeline(
            device: Arc<Device>,
            pipeline_layout: Arc<PipelineLayout>,
        ) -> Result<Arc<GraphicsPipeline>, GfxError> {
            GraphicsPipeline::builder(device.clone())
                .vertex_input(
                    VertexInput::builder()
                        .binding(
                            0,
                            std::mem::size_of::<Vertex>() as _,
                            VertexInputRate::Vertex,
                        )
                        .attribute(0, 0, Format::R32G32SFloat, 0)
                        .build(),
                )
                .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
                .fragment_shader(fragment_shader::Shader::load(device)?, "main")
                .topology(PrimitiveTopology::TriangleList)
                .viewport(Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: 1024.0,
                    height: 768.0,
                    min_depth: 0.0,
                    max_depth: 1.0,
                })
                .scissor(Rect2D {
                    offset: Default::default(),
                    extent: Extent2D {
                        width: 1024,
                        height: 768,
                    },
                })
                .dynamic_state(DynamicState::Viewport)
                .dynamic_state(DynamicState::Scissor)
                .cull_mode(CullMode::None)
                .front_face(FrontFace::CounterClockwise)
                .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
                .layout(pipeline_layout)
                .color_attachment_format(Format::R32G32B32A32SFloat)
                .build()
        }

        pub fn draw(
            &self,
            builder: &mut CommandBufferBuilder,
            environment_map: Texture,
            resolution: u32,
        ) -> Result<Arc<ImageView>, GfxError> {
            let cube_map =
                Self::create_cube_map(self.graphics_context.device().clone(), resolution)?;

            let descriptor_set = DescriptorSet::builder(
                self.graphics_context.device().clone(),
                self.graphics_context
                    .descriptor_pool(&self.descriptor_set_layout)?,
                self.descriptor_set_layout.clone(),
            )
            .build()?;

            cube_map
                .image()
                .set_debug_utils_object_name("To Cube Map Pass Cube Map Image");
            cube_map.set_debug_utils_object_name("To Cube Map Pass Cube Map Image View");
            descriptor_set.set_debug_utils_object_name("To Cube Map Pass Descriptor Set");

            descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    environment_map.sampler(),
                    environment_map.image_view(),
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build()]);

            builder
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(cube_map.image().clone())
                                .dst_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::Undefined)
                                .new_layout(ImageLayout::ColorAttachmentOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .layer_count(6)
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                )
                .bind_graphics_pipeline(self.graphics_pipeline.clone());

            for array_layer in 0..cube_map.image().array_layers() {
                let face = ImageView::builder(
                    self.graphics_context.device().clone(),
                    cube_map.image().clone(),
                )
                .subresource_range(
                    ImageSubresourceRange::builder()
                        .aspect_mask(ImageAspects {
                            color: true,
                            ..Default::default()
                        })
                        .base_array_layer(array_layer)
                        .build(),
                )
                .build()?;
                face.set_debug_utils_object_name("To Cube Map Pass Cube Map Face Image View");

                builder
                    .begin_rendering(self.create_render_pass(face.clone()))
                    .set_viewport(
                        0,
                        vec![Viewport {
                            x: 0.0,
                            y: 0.0,
                            width: resolution as _,
                            height: resolution as _,
                            min_depth: 0.0,
                            max_depth: 1.0,
                        }],
                    )
                    .set_scissor(
                        0,
                        vec![Rect2D {
                            offset: Default::default(),
                            extent: Extent2D {
                                width: resolution,
                                height: resolution,
                            },
                        }],
                    )
                    .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
                    .bind_descriptor_sets(
                        PipelineBindPoint::Graphics,
                        self.pipeline_layout.clone(),
                        0,
                        vec![descriptor_set.clone()],
                    )
                    .push_constants(
                        self.pipeline_layout.clone(),
                        ShaderStages {
                            vertex: true,
                            ..Default::default()
                        },
                        0,
                        PushConstants {
                            orientation: Self::view(array_layer),
                        },
                    )
                    .draw(6, 1, 0, 0)
                    .end_rendering();
            }

            builder.pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(cube_map.image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .layer_count(6)
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            );

            Ok(cube_map)
        }

        fn create_cube_map(
            device: Arc<Device>,
            resolution: u32,
        ) -> Result<Arc<ImageView>, GfxError> {
            ImageView::builder(
                device.clone(),
                Image::builder(device)
                    .image_create_flags(ImageCreateFlags {
                        cube_compatible: true,
                        ..Default::default()
                    })
                    .format(Format::R32G32B32A32SFloat)
                    .extent(
                        Extent2D {
                            width: resolution,
                            height: resolution,
                        }
                        .into(),
                    )
                    .image_type(ImageType::Type2D)
                    .array_layers(6)
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()?,
            )
            .view_type(ImageViewType::Cube)
            .subresource_range(
                ImageSubresourceRange::builder()
                    .aspect_mask(ImageAspects {
                        color: true,
                        ..Default::default()
                    })
                    .layer_count(6)
                    .build(),
            )
            .build()
        }

        fn create_render_pass(&self, face: Arc<ImageView>) -> RenderPass {
            let image_extent = face.image().extent();
            RenderPass::builder()
                .render_area(Rect2D {
                    offset: Default::default(),
                    extent: image_extent.into(),
                })
                .color_attachment(
                    Attachment::builder(face)
                        .image_layout(ImageLayout::ColorAttachmentOptimal)
                        .load_op(AttachmentLoadOp::None)
                        .store_op(AttachmentStoreOp::Store)
                        .build(),
                )
                .build()
        }

        fn view(index: u32) -> Mat4 {
            match index {
                0 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(1.0, 0.0, 0.0),
                    Vec3::new(0.0, -1.0, 0.0),
                ),
                1 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(-1.0, 0.0, 0.0),
                    Vec3::new(0.0, -1.0, 0.0),
                ),
                2 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, -1.0, 0.0),
                    Vec3::new(0.0, 0.0, -1.0),
                ),
                3 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, 1.0, 0.0),
                    Vec3::new(0.0, 0.0, 1.0),
                ),
                4 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, 0.0, 1.0),
                    Vec3::new(0.0, -1.0, 0.0),
                ),
                5 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, 0.0, -1.0),
                    Vec3::new(0.0, -1.0, 0.0),
                ),
                _ => Default::default(),
            }
            .inverse()
        }
    }

    mod shaders {
        use crate::math::Mat4;

        #[allow(unused)]
        #[derive(Debug)]
        #[repr(C)]
        pub struct PushConstants {
            pub orientation: Mat4,
        }
    }

    mod vertex_shader {
        use crate::shader;

        shader! {
            path: "src/rendering/pbr/cube_map.vert.hlsl",
            ty: "vertex",
            entry_point: "main"
        }
    }

    mod fragment_shader {
        use crate::shader;

        shader! {
            path: "src/rendering/pbr/to_cube.frag.hlsl",
            ty: "fragment",
            entry_point: "main"
        }
    }
}

mod irradiance_map_pass {
    use std::sync::Arc;

    use crate::gfx::buffer::Buffer;
    use crate::gfx::device::DeviceOwned;
    use crate::gfx::image::{ImageLayout, Sampler};
    use crate::gfx::pipeline::{
        ColorBlendAttachmentState, CullMode, DescriptorSet, DescriptorSetLayout, DescriptorType,
        DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
        PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
        WriteDescriptorSetImages,
    };
    use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
    use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
    use crate::gfx::Rect2D;
    use crate::math::{Mat4, Vec3};
    use crate::rendering::fullscreen_pass;
    use crate::rendering::fullscreen_pass::Vertex;
    use crate::rendering::pbr::light_probe::irradiance_map_pass::shaders::PushConstants;
    use crate::{
        gfx::command_buffer::CommandBufferBuilder,
        gfx::device::Device,
        gfx::image::{
            Format, Image, ImageAspects, ImageCreateFlags, ImageSubresourceRange, ImageType,
            ImageUsage, ImageView, ImageViewType,
        },
        gfx::{Extent2D, GfxError, GraphicsContext},
    };

    pub struct IrradianceMapPass {
        fullscreen_vertex_buffer: Arc<Buffer>,
        graphics_pipeline: Arc<GraphicsPipeline>,
        pipeline_layout: Arc<PipelineLayout>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
        graphics_context: Arc<GraphicsContext>,
    }

    impl IrradianceMapPass {
        pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
            let descriptor_set_layout =
                Self::create_descriptor_set_layout(graphics_context.device().clone())?;
            let pipeline_layout = Self::create_pipeline_layout(
                graphics_context.device().clone(),
                descriptor_set_layout.clone(),
            )?;
            let graphics_pipeline = Self::create_graphics_pipeline(
                graphics_context.device().clone(),
                pipeline_layout.clone(),
            )?;
            let fullscreen_vertex_buffer =
                fullscreen_pass::create_fullscreen_vertex_buffer(&graphics_context)?;

            descriptor_set_layout
                .set_debug_utils_object_name("Irradiance Map Pass Descriptor Set Layout");
            pipeline_layout.set_debug_utils_object_name("Irradiance Map Pass Pipeline Layout");
            graphics_pipeline.set_debug_utils_object_name("Irradiance Map Pass Graphics Pipeline");
            fullscreen_vertex_buffer
                .set_debug_utils_object_name("Irradiance Map Pass Fullscreen Vertex Buffer");

            Ok(Self {
                fullscreen_vertex_buffer,
                graphics_pipeline,
                pipeline_layout,
                descriptor_set_layout,
                graphics_context,
            })
        }

        fn create_descriptor_set_layout(
            device: Arc<Device>,
        ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
            DescriptorSetLayout::builder(device)
                .binding(
                    0,
                    DescriptorType::CombinedImageSampler,
                    1,
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                )
                .build()
        }

        fn create_pipeline_layout(
            device: Arc<Device>,
            descriptor_set_layout: Arc<DescriptorSetLayout>,
        ) -> Result<Arc<PipelineLayout>, GfxError> {
            PipelineLayout::builder(device)
                .set_layout(descriptor_set_layout)
                .push_constant_range(
                    ShaderStages {
                        vertex: true,
                        ..Default::default()
                    },
                    0,
                    std::mem::size_of::<PushConstants>() as _,
                )
                .build()
        }

        fn create_graphics_pipeline(
            device: Arc<Device>,
            pipeline_layout: Arc<PipelineLayout>,
        ) -> Result<Arc<GraphicsPipeline>, GfxError> {
            GraphicsPipeline::builder(device.clone())
                .vertex_input(
                    VertexInput::builder()
                        .binding(
                            0,
                            std::mem::size_of::<Vertex>() as _,
                            VertexInputRate::Vertex,
                        )
                        .attribute(0, 0, Format::R32G32SFloat, 0)
                        .build(),
                )
                .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
                .fragment_shader(fragment_shader::Shader::load(device)?, "main")
                .topology(PrimitiveTopology::TriangleList)
                .viewport(Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: 1024.0,
                    height: 768.0,
                    min_depth: 0.0,
                    max_depth: 1.0,
                })
                .scissor(Rect2D {
                    offset: Default::default(),
                    extent: Extent2D {
                        width: 1024,
                        height: 768,
                    },
                })
                .dynamic_state(DynamicState::Viewport)
                .dynamic_state(DynamicState::Scissor)
                .cull_mode(CullMode::None)
                .front_face(FrontFace::CounterClockwise)
                .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
                .layout(pipeline_layout)
                .color_attachment_format(Format::R32G32B32A32SFloat)
                .build()
        }

        pub fn draw(
            &self,
            builder: &mut CommandBufferBuilder,
            environment_map: Arc<ImageView>,
            resolution: u32,
        ) -> Result<Arc<ImageView>, GfxError> {
            let cube_map =
                Self::create_cube_map(self.graphics_context.device().clone(), resolution)?;
            let sampler = Sampler::builder(self.graphics_context.device().clone()).build()?;
            let descriptor_set = DescriptorSet::builder(
                self.graphics_context.device().clone(),
                self.graphics_context
                    .descriptor_pool(&self.descriptor_set_layout)?,
                self.descriptor_set_layout.clone(),
            )
            .build()?;

            cube_map
                .image()
                .set_debug_utils_object_name("Irradiance Map Pass Cube Map Image");
            cube_map.set_debug_utils_object_name("Irradiance Map Pass Cube Map Image View");
            sampler.set_debug_utils_object_name("Irradiance Map Pass Sampler");
            descriptor_set.set_debug_utils_object_name("Irradiance Map Pass Descriptor Set");

            descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &sampler,
                    &environment_map,
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build()]);

            builder
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(cube_map.image().clone())
                                .dst_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::Undefined)
                                .new_layout(ImageLayout::ColorAttachmentOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .layer_count(6)
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                )
                .bind_graphics_pipeline(self.graphics_pipeline.clone());

            for array_layer in 0..cube_map.image().array_layers() {
                let face = ImageView::builder(
                    self.graphics_context.device().clone(),
                    cube_map.image().clone(),
                )
                .subresource_range(
                    ImageSubresourceRange::builder()
                        .aspect_mask(ImageAspects {
                            color: true,
                            ..Default::default()
                        })
                        .base_array_layer(array_layer)
                        .build(),
                )
                .build()?;
                face.set_debug_utils_object_name("Irradiance Map Pass Cube Map Face Image View");

                builder
                    .begin_rendering(self.create_render_pass(face.clone()))
                    .set_viewport(
                        0,
                        vec![Viewport {
                            x: 0.0,
                            y: 0.0,
                            width: resolution as _,
                            height: resolution as _,
                            min_depth: 0.0,
                            max_depth: 1.0,
                        }],
                    )
                    .set_scissor(
                        0,
                        vec![Rect2D {
                            offset: Default::default(),
                            extent: Extent2D {
                                width: resolution,
                                height: resolution,
                            },
                        }],
                    )
                    .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
                    .bind_descriptor_sets(
                        PipelineBindPoint::Graphics,
                        self.pipeline_layout.clone(),
                        0,
                        vec![descriptor_set.clone()],
                    )
                    .push_constants(
                        self.pipeline_layout.clone(),
                        ShaderStages {
                            vertex: true,
                            ..Default::default()
                        },
                        0,
                        PushConstants {
                            orientation: Self::view(array_layer),
                        },
                    )
                    .draw(6, 1, 0, 0)
                    .end_rendering();
            }

            builder.pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(cube_map.image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .layer_count(6)
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            );

            Ok(cube_map)
        }

        fn create_cube_map(
            device: Arc<Device>,
            resolution: u32,
        ) -> Result<Arc<ImageView>, GfxError> {
            ImageView::builder(
                device.clone(),
                Image::builder(device)
                    .image_create_flags(ImageCreateFlags {
                        cube_compatible: true,
                        ..Default::default()
                    })
                    .format(Format::R32G32B32A32SFloat)
                    .extent(
                        Extent2D {
                            width: resolution,
                            height: resolution,
                        }
                        .into(),
                    )
                    .image_type(ImageType::Type2D)
                    .array_layers(6)
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()?,
            )
            .view_type(ImageViewType::Cube)
            .subresource_range(
                ImageSubresourceRange::builder()
                    .aspect_mask(ImageAspects {
                        color: true,
                        ..Default::default()
                    })
                    .layer_count(6)
                    .build(),
            )
            .build()
        }

        fn create_render_pass(&self, face: Arc<ImageView>) -> RenderPass {
            let image_extent = face.image().extent();
            RenderPass::builder()
                .render_area(Rect2D {
                    offset: Default::default(),
                    extent: image_extent.into(),
                })
                .color_attachment(
                    Attachment::builder(face)
                        .image_layout(ImageLayout::ColorAttachmentOptimal)
                        .load_op(AttachmentLoadOp::None)
                        .store_op(AttachmentStoreOp::Store)
                        .build(),
                )
                .build()
        }

        fn view(index: u32) -> Mat4 {
            match index {
                0 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(-1.0, 0.0, 0.0),
                    Vec3::new(0.0, 1.0, 0.0),
                ),
                1 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(1.0, 0.0, 0.0),
                    Vec3::new(0.0, 1.0, 0.0),
                ),
                2 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, 1.0, 0.0),
                    Vec3::new(0.0, 0.0, -1.0),
                ),
                3 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, -1.0, 0.0),
                    Vec3::new(0.0, 0.0, 1.0),
                ),
                4 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, 0.0, 1.0),
                    Vec3::new(0.0, 1.0, 0.0),
                ),
                5 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, 0.0, -1.0),
                    Vec3::new(0.0, 1.0, 0.0),
                ),
                _ => Default::default(),
            }
            .inverse()
        }
    }

    mod shaders {
        use crate::math::Mat4;

        #[allow(unused)]
        #[derive(Debug)]
        #[repr(C)]
        pub struct PushConstants {
            pub orientation: Mat4,
        }
    }

    mod vertex_shader {
        use crate::shader;

        shader! {
            path: "src/rendering/pbr/cube_map.vert.hlsl",
            ty: "vertex",
            entry_point: "main"
        }
    }

    mod fragment_shader {
        use crate::shader;

        shader! {
            path: "src/rendering/pbr/irradiance_map.frag.hlsl",
            ty: "fragment",
            entry_point: "main"
        }
    }
}

mod pre_filtered_environment_map_pass {
    use std::sync::Arc;

    use crate::gfx::buffer::Buffer;
    use crate::gfx::device::DeviceOwned;
    use crate::gfx::image::{ImageLayout, Sampler};
    use crate::gfx::pipeline::{
        ColorBlendAttachmentState, CullMode, DescriptorSet, DescriptorSetLayout, DescriptorType,
        DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
        PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
        WriteDescriptorSetImages,
    };
    use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
    use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
    use crate::gfx::Rect2D;
    use crate::math::{Mat4, Vec3};
    use crate::rendering::fullscreen_pass;
    use crate::rendering::fullscreen_pass::Vertex;
    use crate::rendering::pbr::light_probe::pre_filtered_environment_map_pass::shaders::PushConstants;
    use crate::{
        gfx::command_buffer::CommandBufferBuilder,
        gfx::device::Device,
        gfx::image::{
            Format, Image, ImageAspects, ImageCreateFlags, ImageSubresourceRange, ImageType,
            ImageUsage, ImageView, ImageViewType,
        },
        gfx::{Extent2D, GfxError, GraphicsContext},
    };

    pub struct PreFilteredEnvironmentMapPass {
        fullscreen_vertex_buffer: Arc<Buffer>,
        graphics_pipeline: Arc<GraphicsPipeline>,
        pipeline_layout: Arc<PipelineLayout>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
        graphics_context: Arc<GraphicsContext>,
    }

    impl PreFilteredEnvironmentMapPass {
        pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
            let descriptor_set_layout =
                Self::create_descriptor_set_layout(graphics_context.device().clone())?;
            let pipeline_layout = Self::create_pipeline_layout(
                graphics_context.device().clone(),
                descriptor_set_layout.clone(),
            )?;
            let graphics_pipeline = Self::create_graphics_pipeline(
                graphics_context.device().clone(),
                pipeline_layout.clone(),
            )?;
            let fullscreen_vertex_buffer =
                fullscreen_pass::create_fullscreen_vertex_buffer(&graphics_context)?;

            descriptor_set_layout.set_debug_utils_object_name(
                "Pre Filtered Environment Map Pass Descriptor Set Layout",
            );
            pipeline_layout
                .set_debug_utils_object_name("Pre Filtered Environment Map Pass Pipeline Layout");
            graphics_pipeline
                .set_debug_utils_object_name("Pre Filtered Environment Map Pass Graphics Pipeline");
            fullscreen_vertex_buffer.set_debug_utils_object_name(
                "Pre Filtered Environment Map Pass Full Screen Vertex Buffer",
            );

            Ok(Self {
                fullscreen_vertex_buffer,
                graphics_pipeline,
                pipeline_layout,
                descriptor_set_layout,
                graphics_context,
            })
        }

        fn create_descriptor_set_layout(
            device: Arc<Device>,
        ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
            DescriptorSetLayout::builder(device)
                .binding(
                    0,
                    DescriptorType::CombinedImageSampler,
                    1,
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                )
                .build()
        }

        fn create_pipeline_layout(
            device: Arc<Device>,
            descriptor_set_layout: Arc<DescriptorSetLayout>,
        ) -> Result<Arc<PipelineLayout>, GfxError> {
            PipelineLayout::builder(device)
                .set_layout(descriptor_set_layout)
                .push_constant_range(
                    ShaderStages {
                        vertex: true,
                        fragment: true,
                        ..Default::default()
                    },
                    0,
                    std::mem::size_of::<PushConstants>() as _,
                )
                .build()
        }

        fn create_graphics_pipeline(
            device: Arc<Device>,
            pipeline_layout: Arc<PipelineLayout>,
        ) -> Result<Arc<GraphicsPipeline>, GfxError> {
            GraphicsPipeline::builder(device.clone())
                .vertex_input(
                    VertexInput::builder()
                        .binding(
                            0,
                            std::mem::size_of::<Vertex>() as _,
                            VertexInputRate::Vertex,
                        )
                        .attribute(0, 0, Format::R32G32SFloat, 0)
                        .build(),
                )
                .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
                .fragment_shader(fragment_shader::Shader::load(device)?, "main")
                .topology(PrimitiveTopology::TriangleList)
                .viewport(Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: 1024.0,
                    height: 768.0,
                    min_depth: 0.0,
                    max_depth: 1.0,
                })
                .scissor(Rect2D {
                    offset: Default::default(),
                    extent: Extent2D {
                        width: 1024,
                        height: 768,
                    },
                })
                .dynamic_state(DynamicState::Viewport)
                .dynamic_state(DynamicState::Scissor)
                .cull_mode(CullMode::None)
                .front_face(FrontFace::CounterClockwise)
                .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
                .layout(pipeline_layout)
                .color_attachment_format(Format::R32G32B32A32SFloat)
                .build()
        }

        pub fn draw(
            &self,
            builder: &mut CommandBufferBuilder,
            environment_map: Arc<ImageView>,
            resolution: u32,
        ) -> Result<Arc<ImageView>, GfxError> {
            let cube_map =
                Self::create_cube_map(self.graphics_context.device().clone(), resolution)?;
            let sampler = Sampler::builder(self.graphics_context.device().clone()).build()?;
            let descriptor_set = DescriptorSet::builder(
                self.graphics_context.device().clone(),
                self.graphics_context
                    .descriptor_pool(&self.descriptor_set_layout)?,
                self.descriptor_set_layout.clone(),
            )
            .build()?;

            cube_map
                .image()
                .set_debug_utils_object_name("Pre Filtered Environment Map Pass Cube Map Image");
            cube_map.set_debug_utils_object_name(
                "Pre Filtered Environment Map Pass Cube Map Image View",
            );
            sampler.set_debug_utils_object_name("Pre Filtered Environment Map Pass Sampler");
            descriptor_set
                .set_debug_utils_object_name("Pre Filtered Environment Map Pass Descriptor Set");

            descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &sampler,
                    &environment_map,
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build()]);

            builder
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(cube_map.image().clone())
                                .dst_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::Undefined)
                                .new_layout(ImageLayout::ColorAttachmentOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .layer_count(6)
                                        .level_count(cube_map.image().mip_levels())
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                )
                .bind_graphics_pipeline(self.graphics_pipeline.clone());

            for array_layer in 0..cube_map.image().array_layers() {
                for mip_level in 0..cube_map.image().mip_levels() {
                    let roughness = mip_level as f32 / (cube_map.image().mip_levels() - 1) as f32;
                    let width = (resolution as f32 * 0.5f32.powf(mip_level as _)) as u32;
                    let face = ImageView::builder(
                        self.graphics_context.device().clone(),
                        cube_map.image().clone(),
                    )
                    .subresource_range(
                        ImageSubresourceRange::builder()
                            .aspect_mask(ImageAspects {
                                color: true,
                                ..Default::default()
                            })
                            .base_array_layer(array_layer)
                            .base_mip_level(mip_level)
                            .build(),
                    )
                    .build()?;
                    face.set_debug_utils_object_name(
                        "Pre Filtered Environment Map Pass Cube Map Face Image View",
                    );

                    builder
                        .begin_rendering(self.create_render_pass(face.clone()))
                        .set_viewport(
                            0,
                            vec![Viewport {
                                x: 0.0,
                                y: 0.0,
                                width: width as _,
                                height: width as _,
                                min_depth: 0.0,
                                max_depth: 1.0,
                            }],
                        )
                        .set_scissor(
                            0,
                            vec![Rect2D {
                                offset: Default::default(),
                                extent: Extent2D {
                                    width,
                                    height: width,
                                },
                            }],
                        )
                        .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
                        .bind_descriptor_sets(
                            PipelineBindPoint::Graphics,
                            self.pipeline_layout.clone(),
                            0,
                            vec![descriptor_set.clone()],
                        )
                        .push_constants(
                            self.pipeline_layout.clone(),
                            ShaderStages {
                                vertex: true,
                                fragment: true,
                                ..Default::default()
                            },
                            0,
                            PushConstants {
                                orientation: Self::view(array_layer),
                                roughness,
                            },
                        )
                        .draw(6, 1, 0, 0)
                        .end_rendering();
                }
            }
            builder.pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(cube_map.image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .layer_count(6)
                                    .level_count(cube_map.image().mip_levels())
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            );

            Ok(cube_map)
        }

        fn create_cube_map(
            device: Arc<Device>,
            resolution: u32,
        ) -> Result<Arc<ImageView>, GfxError> {
            let mip_levels = f32::log2(resolution as _).floor() as u32 + 1;
            ImageView::builder(
                device.clone(),
                Image::builder(device)
                    .image_create_flags(ImageCreateFlags {
                        cube_compatible: true,
                        ..Default::default()
                    })
                    .format(Format::R32G32B32A32SFloat)
                    .extent(
                        Extent2D {
                            width: resolution,
                            height: resolution,
                        }
                        .into(),
                    )
                    .image_type(ImageType::Type2D)
                    .array_layers(6)
                    .mip_levels(mip_levels)
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()?,
            )
            .view_type(ImageViewType::Cube)
            .subresource_range(
                ImageSubresourceRange::builder()
                    .aspect_mask(ImageAspects {
                        color: true,
                        ..Default::default()
                    })
                    .layer_count(6)
                    .level_count(mip_levels)
                    .build(),
            )
            .build()
        }

        fn create_render_pass(&self, face: Arc<ImageView>) -> RenderPass {
            let image_extent = face.image().extent();
            RenderPass::builder()
                .render_area(Rect2D {
                    offset: Default::default(),
                    extent: image_extent.into(),
                })
                .color_attachment(
                    Attachment::builder(face)
                        .image_layout(ImageLayout::ColorAttachmentOptimal)
                        .load_op(AttachmentLoadOp::None)
                        .store_op(AttachmentStoreOp::Store)
                        .build(),
                )
                .build()
        }

        fn view(index: u32) -> Mat4 {
            match index {
                0 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(-1.0, 0.0, 0.0),
                    Vec3::new(0.0, 1.0, 0.0),
                ),
                1 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(1.0, 0.0, 0.0),
                    Vec3::new(0.0, 1.0, 0.0),
                ),
                2 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, 1.0, 0.0),
                    Vec3::new(0.0, 0.0, -1.0),
                ),
                3 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, -1.0, 0.0),
                    Vec3::new(0.0, 0.0, 1.0),
                ),
                4 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, 0.0, 1.0),
                    Vec3::new(0.0, 1.0, 0.0),
                ),
                5 => Mat4::look_at(
                    Vec3::zero(),
                    Vec3::new(0.0, 0.0, -1.0),
                    Vec3::new(0.0, 1.0, 0.0),
                ),
                _ => Default::default(),
            }
            .inverse()
        }
    }

    mod shaders {
        use crate::math::Mat4;

        #[allow(unused)]
        #[derive(Debug)]
        #[repr(C)]
        pub struct PushConstants {
            pub orientation: Mat4,
            pub roughness: f32,
        }
    }

    mod vertex_shader {
        use crate::shader;

        shader! {
            path: "src/rendering/pbr/cube_map.vert.hlsl",
            ty: "vertex",
            entry_point: "main"
        }
    }

    mod fragment_shader {
        use crate::shader;

        shader! {
            path: "src/rendering/pbr/pre_filtered_environment_map.frag.hlsl",
            ty: "fragment",
            entry_point: "main"
        }
    }
}

mod brdf_lut_pass {
    use std::sync::Arc;

    use crate::gfx::buffer::Buffer;
    use crate::gfx::command_buffer::CommandBufferBuilder;
    use crate::gfx::device::{Device, DeviceOwned};
    use crate::gfx::image::{
        Format, Image, ImageAspects, ImageLayout, ImageSubresourceRange, ImageUsage, ImageView,
    };
    use crate::gfx::pipeline::{
        ColorBlendAttachmentState, CullMode, DynamicState, FrontFace, GraphicsPipeline,
        PipelineLayout, PrimitiveTopology, VertexInput, VertexInputRate, Viewport,
    };
    use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
    use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
    use crate::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
    use crate::rendering::fullscreen_pass::{
        create_fullscreen_vertex_buffer, vertex_shader, Vertex,
    };

    /// Final render pass.
    #[derive(Debug)]
    pub struct BrdfLutPass {
        fullscreen_vertex_buffer: Arc<Buffer>,
        graphics_pipeline: Arc<GraphicsPipeline>,
        _pipeline_layout: Arc<PipelineLayout>,
        graphics_context: Arc<GraphicsContext>,
    }

    impl BrdfLutPass {
        /// Creates a new directional light pass.
        pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
            let pipeline_layout = Self::create_pipeline_layout(graphics_context.device().clone())?;
            let graphics_pipeline = Self::create_graphics_pipeline(
                graphics_context.device().clone(),
                pipeline_layout.clone(),
            )?;
            let fullscreen_vertex_buffer = create_fullscreen_vertex_buffer(&graphics_context)?;

            pipeline_layout.set_debug_utils_object_name("Brdf Lut Pass Pipeline Layout");
            graphics_pipeline.set_debug_utils_object_name("Brdf Lut Pass Graphics Pipeline");
            fullscreen_vertex_buffer
                .set_debug_utils_object_name("Brdf Lut Pass Fullscreen Vertex Buffer");

            Ok(Self {
                fullscreen_vertex_buffer,
                graphics_pipeline,
                _pipeline_layout: pipeline_layout,
                graphics_context,
            })
        }

        /// Draws the entities.
        pub fn draw(
            &self,
            builder: &mut CommandBufferBuilder,
            resolution: u32,
        ) -> Result<Arc<ImageView>, GfxError> {
            let lut = Self::create_lut(self.graphics_context.device().clone(), resolution)?;
            lut.image()
                .set_debug_utils_object_name("Brdf Lut Pass Image");
            lut.set_debug_utils_object_name("Brdf Lut Pass Image View");

            builder
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(lut.image().clone())
                                .dst_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::Undefined)
                                .new_layout(ImageLayout::ColorAttachmentOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                )
                .bind_graphics_pipeline(self.graphics_pipeline.clone())
                .begin_rendering(self.create_render_pass(lut.clone()))
                .set_viewport(
                    0,
                    vec![Viewport {
                        x: 0.0,
                        y: 0.0,
                        width: resolution as _,
                        height: resolution as _,
                        min_depth: 0.0,
                        max_depth: 1.0,
                    }],
                )
                .set_scissor(
                    0,
                    vec![Rect2D {
                        offset: Default::default(),
                        extent: Extent2D {
                            width: resolution,
                            height: resolution,
                        },
                    }],
                )
                .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
                .draw(6, 1, 0, 0)
                .end_rendering()
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(lut.image().clone())
                                .src_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .src_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .dst_stage_mask(PipelineStages {
                                    fragment_shader: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    shader_sampled_read: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::ColorAttachmentOptimal)
                                .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                );

            Ok(lut)
        }
    }

    impl BrdfLutPass {
        fn create_pipeline_layout(device: Arc<Device>) -> Result<Arc<PipelineLayout>, GfxError> {
            PipelineLayout::builder(device).build()
        }

        fn create_graphics_pipeline(
            device: Arc<Device>,
            pipeline_layout: Arc<PipelineLayout>,
        ) -> Result<Arc<GraphicsPipeline>, GfxError> {
            GraphicsPipeline::builder(device.clone())
                .vertex_input(
                    VertexInput::builder()
                        .binding(
                            0,
                            std::mem::size_of::<Vertex>() as _,
                            VertexInputRate::Vertex,
                        )
                        .attribute(0, 0, Format::R32G32SFloat, 0)
                        .attribute(1, 0, Format::R32G32SFloat, 8)
                        .build(),
                )
                .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
                .fragment_shader(fragment_shader::Shader::load(device)?, "main")
                .topology(PrimitiveTopology::TriangleList)
                .viewport(Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: 1024.0,
                    height: 768.0,
                    min_depth: 0.0,
                    max_depth: 1.0,
                })
                .scissor(Rect2D {
                    offset: Default::default(),
                    extent: Extent2D {
                        width: 1024,
                        height: 768,
                    },
                })
                .dynamic_state(DynamicState::Viewport)
                .dynamic_state(DynamicState::Scissor)
                .cull_mode(CullMode::Back)
                .front_face(FrontFace::CounterClockwise)
                .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
                .layout(pipeline_layout)
                .color_attachment_format(Format::R32G32B32A32SFloat)
                .build()
        }

        fn create_lut(device: Arc<Device>, resolution: u32) -> Result<Arc<ImageView>, GfxError> {
            ImageView::builder(
                device.clone(),
                Image::builder(device)
                    .format(Format::R32G32B32A32SFloat)
                    .extent(
                        Extent2D {
                            width: resolution,
                            height: resolution,
                        }
                        .into(),
                    )
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()?,
            )
            .subresource_range(
                ImageSubresourceRange::builder()
                    .aspect_mask(ImageAspects {
                        color: true,
                        ..Default::default()
                    })
                    .build(),
            )
            .build()
        }

        fn create_render_pass(&self, lut: Arc<ImageView>) -> RenderPass {
            let image_extent = lut.image().extent();
            RenderPass::builder()
                .render_area(Rect2D {
                    offset: Default::default(),
                    extent: image_extent.into(),
                })
                .color_attachment(
                    Attachment::builder(lut)
                        .image_layout(ImageLayout::ColorAttachmentOptimal)
                        .load_op(AttachmentLoadOp::None)
                        .store_op(AttachmentStoreOp::Store)
                        .build(),
                )
                .build()
        }
    }

    mod fragment_shader {
        use crate::shader;

        shader! {
            path: "src/rendering/pbr/brdf_lut.frag.hlsl",
            ty: "fragment",
            entry_point: "main"
        }
    }
}
