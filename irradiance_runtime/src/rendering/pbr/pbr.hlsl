const float PI = 3.14159265358979323846;

float trowbridge_reitz_ggx_distribution(float3 N, float3 H, float roughness) {
    float alpha = roughness * roughness;
    float alpha2 = alpha * alpha;
    float NdotH = saturate(dot(N, H));
    float NdotH2 = NdotH * NdotH;

    float numerator = alpha2;
    float denominator = NdotH2 * (alpha2 - 1.0) + 1.0;
    denominator = PI * denominator * denominator;

    return numerator / denominator;
}

float geometry_schlick(float cos_theta, float roughness) {
    float r = roughness + 1.0;
    float k = (r * r) / 8.0;

    float numerator = cos_theta;
    float denominator = cos_theta * (1.0 - k) + k;

    return numerator / denominator;
}

float geometry_smith(float3 N, float3 V, float3 L, float roughness) {
    float NdotL = saturate(dot(N, L));
    float NdotV = saturate(dot(N, V));

    return geometry_schlick(NdotL, roughness) * geometry_schlick(NdotV, roughness);
}

float3 fresnel_schlick(float3 V, float3 H, float3 F0) {
    float VdotH = saturate(dot(V, H));
    float exponent = (-5.55473 * VdotH - 6.98316) * VdotH;

    return F0 + (1.0 - F0) * pow(2, exponent);
}

float3 pbr_brdf(float3 V, float3 L, float3 radiance, float3 Cdiff, float3 N, float roughness, float metallic) {
    float3 H = normalize(L + V);

    float NdotL = saturate(dot(N, L));
    float NdotV = saturate(dot(N, V));

    float3 fD = Cdiff / PI;

    float D = trowbridge_reitz_ggx_distribution(N, H, roughness);

    float3 F0 = lerp(float3(0.04), Cdiff, metallic);
    float3 F = fresnel_schlick(V, H, F0);

    float G = geometry_smith(N, V, L, roughness);

    float3 fS = (D * F * G) / max((4.0 * NdotL * NdotV), 0.001);

    float3 kD = (1.0 - F) * (1.0 - metallic);

    return (kD * fD + fS) * radiance * NdotL;
}
