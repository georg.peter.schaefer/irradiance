use std::sync::Arc;

use irradiance_runtime::gfx::GraphicsContext;

use crate::ecs::Entities;
use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::image::{ImageAspects, ImageLayout, ImageSubresourceRange, ImageView};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::GfxError;
use crate::math::Vec4;
use crate::rendering::pbr::{DirectionalLightPass, GBuffer, PointLightPass};

/// Light gathering pass.
#[derive(Debug)]
pub struct LightPass {
    directional_light_pass: DirectionalLightPass,
    point_light_pass: PointLightPass,
}

impl LightPass {
    /// Creates a new light gathering pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        Ok(Self {
            directional_light_pass: DirectionalLightPass::new(graphics_context.clone())?,
            point_light_pass: PointLightPass::new(graphics_context)?,
        })
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        g_buffer: &GBuffer,
        light_buffer: Arc<ImageView>,
        clear_color: Vec4,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Light Pass", Vec4::new(0.3, 0.7, 0.0, 1.0));

        builder.pipeline_barrier(
            PipelineBarrier::builder()
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(g_buffer.position().image().clone())
                        .src_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .dst_stage_mask(PipelineStages {
                            fragment_shader: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            shader_sampled_read: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::ColorAttachmentOptimal)
                        .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(g_buffer.base_color().image().clone())
                        .src_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .dst_stage_mask(PipelineStages {
                            fragment_shader: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            shader_sampled_read: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::ColorAttachmentOptimal)
                        .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(g_buffer.normal().image().clone())
                        .src_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .dst_stage_mask(PipelineStages {
                            fragment_shader: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            shader_sampled_read: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::ColorAttachmentOptimal)
                        .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(g_buffer.metallic_roughness().image().clone())
                        .src_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .dst_stage_mask(PipelineStages {
                            fragment_shader: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            shader_sampled_read: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::ColorAttachmentOptimal)
                        .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(light_buffer.image().clone())
                        .src_stage_mask(PipelineStages {
                            fragment_shader: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask {
                            shader_sampled_read: true,
                            ..Default::default()
                        })
                        .dst_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::Undefined)
                        .new_layout(ImageLayout::ColorAttachmentOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .build(),
        );

        self.directional_light_pass.draw(
            builder,
            g_buffer,
            light_buffer.clone(),
            clear_color,
            entities,
        )?;

        builder.pipeline_barrier(
            PipelineBarrier::builder()
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(light_buffer.image().clone())
                        .src_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .dst_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::ColorAttachmentOptimal)
                        .new_layout(ImageLayout::ColorAttachmentOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .build(),
        );

        self.point_light_pass
            .draw(builder, g_buffer, light_buffer.clone(), entities)?;

        builder.end_label();

        Ok(())
    }
}
