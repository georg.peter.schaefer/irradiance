use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{Format, Image, ImageUsage, ImageView};
use crate::gfx::{Extent2D, GfxError};
use std::sync::Arc;

/// Ssao buffer.
pub struct SsaoBuffer {
    blur_buffer: Arc<ImageView>,
    buffer: Arc<ImageView>,
}

impl SsaoBuffer {
    /// Creates a new ssao buffer.
    pub fn new(device: Arc<Device>, extent: Extent2D) -> Result<Self, GfxError> {
        let extent = Extent2D {
            width: extent.width / 2,
            height: extent.height / 2,
        };
        let ssao_buffer = Self {
            buffer: ImageView::builder(
                device.clone(),
                Image::builder(device.clone())
                    .format(Format::R8UNorm)
                    .extent(extent.into())
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()?,
            )
            .build()?,
            blur_buffer: ImageView::builder(
                device.clone(),
                Image::builder(device)
                    .format(Format::R8UNorm)
                    .extent(extent.into())
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()?,
            )
            .build()?,
        };

        ssao_buffer
            .buffer
            .image()
            .set_debug_utils_object_name("Ssao Buffer Image");
        ssao_buffer
            .buffer
            .set_debug_utils_object_name("Ssao Buffer Image View");
        ssao_buffer
            .blur_buffer
            .image()
            .set_debug_utils_object_name("Ssao Blur Buffer Image");
        ssao_buffer
            .blur_buffer
            .set_debug_utils_object_name("Ssao Blur Buffer Image View");

        Ok(ssao_buffer)
    }

    /// Returns the ssao buffer.
    pub fn get(&self) -> &Arc<ImageView> {
        &self.buffer
    }

    /// Returns the blur buffer.
    pub fn get_blur(&self) -> &Arc<ImageView> {
        &self.blur_buffer
    }
}
