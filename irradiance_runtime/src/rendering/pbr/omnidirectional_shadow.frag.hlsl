struct VSOutput {
    float4 position : SV_POSITION;
    [[vk::location(0)]] float3 world_position : POSITION0;
    [[vk::location(1)]] float3 light_position : POSITION1;
};

float main(VSOutput input) : SV_TARGET {
    return length(input.world_position - input.light_position);
}
