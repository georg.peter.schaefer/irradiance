struct VSInput {
    [[vk::location(0)]] float3 position : POSITION0;
    [[vk::location(1)]] float4 joint : TEXCOORD0;
    [[vk::location(2)]] float4 weight : TEXCOORD1;
};

[[vk::binding(0, 0)]]
StructuredBuffer<float4x4> joints;

struct PushConstants {
    float4x4 projection;
    float4x4 view;
    float4x4 model;
    float3 light_position;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

struct VSOutput {
    float4 position : SV_POSITION;
    [[vk::location(0)]] float3 world_position : POSITION0;
    [[vk::location(1)]] float3 light_position : POSITION1;
};

VSOutput main(VSInput input) {
    VSOutput output = (VSOutput)0;

    float4x4 skin = input.weight.x * joints[int(input.joint.x)]
        + input.weight.y * joints[int(input.joint.y)]
        + input.weight.z * joints[int(input.joint.z)]
        + input.weight.w * joints[int(input.joint.w)];
    float4x4 skinned_model = skin * push_constants.model;

    output.position = float4(input.position, 1.0) * skinned_model * push_constants.view * push_constants.projection;
    output.world_position = float3(float4(input.position, 1.0) * skinned_model);
    output.light_position = push_constants.light_position;

    return output;
}
