use std::sync::Arc;

use fullscreen_pass::{create_fullscreen_vertex_buffer, vertex_shader};

use crate::ecs::{Entities, Transform};
use crate::gfx::buffer::Buffer;
use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{
    Format, ImageAspects, ImageLayout, ImageSubresourceRange, ImageView, Sampler,
};
use crate::gfx::pipeline::{
    BlendOp, ColorBlendAttachmentState, CullMode, DescriptorSet, DescriptorSetLayout,
    DescriptorType, DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
    PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
    WriteDescriptorSetImages,
};
use crate::gfx::render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass};
use crate::gfx::sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages};
use crate::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use crate::math::Vec4;
use crate::rendering::fullscreen_pass::Vertex;
use crate::rendering::pbr::ambient_light_pass::shaders::PushConstants;
use crate::rendering::pbr::{GBuffer, LightProbe, SsaoBuffer};
use crate::rendering::{fullscreen_pass, Camera};

/// Directional light gathering pass.
#[derive(Debug)]
pub struct AmbientLightPass {
    sampler: Arc<Sampler>,
    fullscreen_vertex_buffer: Arc<Buffer>,
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl AmbientLightPass {
    /// Creates a new directional light gathering pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(graphics_context.device().clone())?;
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            descriptor_set_layout.clone(),
        )?;
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            pipeline_layout.clone(),
        )?;
        let fullscreen_vertex_buffer = create_fullscreen_vertex_buffer(&graphics_context)?;
        let sampler = Sampler::builder(graphics_context.device().clone()).build()?;

        descriptor_set_layout
            .set_debug_utils_object_name("Ambient Light Pass Descriptor Set Layout");
        pipeline_layout.set_debug_utils_object_name("Ambient Light Pass pipeline Layout");
        graphics_pipeline.set_debug_utils_object_name("Ambient Light Pass Graphics Pipeline");
        fullscreen_vertex_buffer
            .set_debug_utils_object_name("Ambient Light Pass Fullscreen Vertex Buffer");
        sampler.set_debug_utils_object_name("Ambient Light Pass Sampler");

        Ok(Self {
            sampler,
            fullscreen_vertex_buffer,
            graphics_pipeline,
            pipeline_layout,
            descriptor_set_layout,
            graphics_context,
        })
    }

    /// Draws the entities.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        g_buffer: &GBuffer,
        ssao_buffer: &SsaoBuffer,
        light_probe: &LightProbe,
        light_buffer: Arc<ImageView>,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Ambient Light Pass", Vec4::new(0.4, 0.2, 0.0, 1.0));

        if let Some(camera) = entities.active::<Camera>() {
            let descriptor_set = DescriptorSet::builder(
                self.graphics_context.device().clone(),
                self.graphics_context
                    .descriptor_pool(&self.descriptor_set_layout)?,
                self.descriptor_set_layout.clone(),
            )
            .build()?;
            descriptor_set.set_debug_utils_object_name("Ambient Light Pass Descriptor Set");

            descriptor_set.write_images(vec![
                WriteDescriptorSetImages::builder()
                    .dst_binding(0)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        &self.sampler,
                        g_buffer.position(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(1)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        &self.sampler,
                        g_buffer.base_color(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(2)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        &self.sampler,
                        g_buffer.normal(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(3)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        &self.sampler,
                        g_buffer.metallic_roughness(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(4)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        light_probe.sampler(),
                        light_probe.irradiance_map(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(5)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        light_probe.mip_map_sampler(),
                        light_probe.pre_filtered_environment_map(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(6)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        light_probe.sampler(),
                        light_probe.brdf_lut(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
                WriteDescriptorSetImages::builder()
                    .dst_binding(7)
                    .descriptor_type(DescriptorType::CombinedImageSampler)
                    .sampled_image(
                        &self.sampler,
                        ssao_buffer.get(),
                        ImageLayout::ShaderReadOnlyOptimal,
                    )
                    .build(),
            ]);

            builder
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(ssao_buffer.get().image().clone())
                                .src_stage_mask(PipelineStages {
                                    color_attachment_output: true,
                                    ..Default::default()
                                })
                                .src_access_mask(AccessMask {
                                    color_attachment_write: true,
                                    ..Default::default()
                                })
                                .dst_stage_mask(PipelineStages {
                                    fragment_shader: true,
                                    ..Default::default()
                                })
                                .dst_access_mask(AccessMask {
                                    shader_sampled_read: true,
                                    ..Default::default()
                                })
                                .old_layout(ImageLayout::ColorAttachmentOptimal)
                                .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                )
                .set_viewport(
                    0,
                    vec![Viewport {
                        x: 0.0,
                        y: 0.0,
                        width: light_buffer.image().extent().width as _,
                        height: light_buffer.image().extent().height as _,
                        min_depth: 0.0,
                        max_depth: 1.0,
                    }],
                )
                .set_scissor(
                    0,
                    vec![Rect2D {
                        offset: Default::default(),
                        extent: light_buffer.image().extent().into(),
                    }],
                )
                .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.pipeline_layout.clone(),
                    0,
                    vec![descriptor_set],
                )
                .bind_graphics_pipeline(self.graphics_pipeline.clone())
                .begin_rendering(self.create_render_pass(light_buffer.clone()))
                .push_constants(
                    self.pipeline_layout.clone(),
                    ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                    0,
                    PushConstants {
                        eye: camera.get::<Transform>().unwrap().position(),
                        max_reflection_lod: light_probe
                            .pre_filtered_environment_map()
                            .image()
                            .mip_levels() as _,
                    },
                )
                .draw(6, 1, 0, 0)
                .end_rendering();
        }

        builder.end_label();

        Ok(())
    }
}

impl AmbientLightPass {
    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device.clone())
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                1,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                2,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                3,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                4,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                5,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                6,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .binding(
                7,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32SFloat, 0)
                    .attribute(1, 0, Format::R32G32SFloat, 8)
                    .build(),
            )
            .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 768.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .color_blend_attachment_state(
                ColorBlendAttachmentState::builder()
                    .enable_blend()
                    .alpha_blend_op(BlendOp::Add)
                    .build(),
            )
            .layout(pipeline_layout)
            .color_attachment_format(Format::R32G32B32A32SFloat)
            .build()
    }

    fn create_render_pass(&self, light_buffer: Arc<ImageView>) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: light_buffer.image().extent().into(),
            })
            .color_attachment(
                Attachment::builder(light_buffer)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .build()
    }
}

mod shaders {
    use crate::math::Vec3;

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub eye: Vec3,
        pub max_reflection_lod: f32,
    }
}

mod fragment_shader {
    use crate::shader;

    shader! {
        path: "src/rendering/pbr/ambient_light.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
