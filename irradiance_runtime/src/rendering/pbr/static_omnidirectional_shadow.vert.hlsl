struct VSInput {
    [[vk::location(0)]] float3 position : POSITION0;
};

struct PushConstants {
    float4x4 projection;
    float4x4 view;
    float4x4 model;
    float3 light_position;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

struct VSOutput {
    float4 position : SV_POSITION;
    [[vk::location(0)]] float3 world_position : POSITION0;
    [[vk::location(1)]] float3 light_position : POSITION1;
};

static const float4x4 flip = float4x4(
    -1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0
);

VSOutput main(VSInput input) {
    VSOutput output = (VSOutput)0;

    output.position = float4(input.position, 1.0) * push_constants.model * push_constants.view * push_constants.projection;
    output.world_position = float3(float4(input.position, 1.0) * push_constants.model);
    output.light_position = push_constants.light_position;

    return output;
}
