#include "pbr.hlsl"
#include "shadow.hlsl"

[[vk::binding(0, 0)]]
Texture2D position_texture;
[[vk::binding(0, 0)]]
SamplerState position_sampler;
[[vk::binding(1, 0)]]
Texture2D base_color_texture;
[[vk::binding(1, 0)]]
SamplerState base_color_sampler;
[[vk::binding(2, 0)]]
Texture2D normal_texture;
[[vk::binding(2, 0)]]
SamplerState normal_sampler;
[[vk::binding(3, 0)]]
Texture2D metallic_roughness_texture;
[[vk::binding(3, 0)]]
SamplerState metallic_roughness_sampler;

[[vk::binding(0, 1)]]
StructuredBuffer<Cascade> cascades;
[[vk::binding(1, 1)]]
SamplerState shadow_map_sampler;
[[vk::binding(1, 1)]]
Texture2DArray shadow_map_texture;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

struct PushConstants {
    float4x4 view;
    float3 direction;
    float3 color;
    float intensity;
    float3 eye;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

[[vk::constant_id(0)]]
const bool with_shadow = false;

float4 main(VSOutput input) : SV_TARGET {
    float3 Cdiff = base_color_texture.Sample(base_color_sampler, input.tex_coord).rgb;
    float3 N = normal_texture.Sample(normal_sampler, input.tex_coord).xyz;
    float emission = metallic_roughness_texture.Sample(metallic_roughness_sampler, input.tex_coord).b;

    Cdiff = lerp(Cdiff, Cdiff * 100.0, emission);

    if (length(N) <= 0.01) {
        return float4(Cdiff, 1.0);
    } else {
        N = normalize(N);
        float3 P = position_texture.Sample(position_sampler, input.tex_coord).xyz;
        float3 L = normalize(-push_constants.direction);
        float3 V = normalize(push_constants.eye - P);
        float roughness = metallic_roughness_texture.Sample(metallic_roughness_sampler, input.tex_coord).g;
        float metallic = metallic_roughness_texture.Sample(metallic_roughness_sampler, input.tex_coord).r;
        float3 radiance = push_constants.color * push_constants.intensity;

        float3 Lo = pbr_brdf(V, L, radiance, Cdiff, N, roughness, metallic);

        if (with_shadow) {
            Lo *= sample_shadow(cascades, shadow_map_sampler, shadow_map_texture, push_constants.view, P);
        }

        return float4(lerp(Lo, Cdiff, emission), 1.0);
    }
}
