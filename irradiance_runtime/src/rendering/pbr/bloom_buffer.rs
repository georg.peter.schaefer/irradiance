use std::sync::Arc;

use irradiance_runtime::gfx::image::ImageView;

use crate::gfx::device::{Device, DeviceOwned};
use crate::gfx::image::{
    Format, Image, ImageAspects, ImageSubresourceRange, ImageType, ImageUsage, ImageViewType,
};
use crate::gfx::Extent2D;
use crate::gfx::GfxError;

/// Bloom buffer.
#[derive(Debug)]
pub struct BloomBuffer {
    /// Image views for the mip levels.
    pub views: Vec<Arc<ImageView>>,
    /// Bloom image.
    pub image: Arc<Image>,
}

impl BloomBuffer {
    /// Creates a new bloom buffer.
    pub fn new(device: Arc<Device>, mut extent: Extent2D) -> Result<Self, GfxError> {
        extent.width /= 2;
        extent.height /= 2;
        let mip_levels = (extent.width.min(extent.height) as f32).log2().floor() as u32 + 1;
        let mip_levels = mip_levels.min(5);
        let image = Image::builder(device.clone())
            .image_type(ImageType::Type2D)
            .format(Format::R32G32B32A32SFloat)
            .extent(extent.into())
            .mip_levels(mip_levels)
            .usage(ImageUsage {
                color_attachment: true,
                sampled: true,
                ..Default::default()
            })
            .build()?;
        let mut views = vec![];
        for level in 0..mip_levels {
            let view = ImageView::builder(device.clone(), image.clone())
                .view_type(ImageViewType::Type2D)
                .subresource_range(
                    ImageSubresourceRange::builder()
                        .base_mip_level(level)
                        .level_count(1)
                        .aspect_mask(ImageAspects {
                            color: true,
                            ..Default::default()
                        })
                        .build(),
                )
                .build()?;
            view.set_debug_utils_object_name("Bloom Buffer Image View");
            views.push(view);
        }

        image.set_debug_utils_object_name("Bloom Buffer Image");

        Ok(Self { views, image })
    }
}
