use crate::gfx::command_buffer::CommandBufferBuilder;
use crate::gfx::image::ImageView;
use crate::gfx::{GfxError, GraphicsContext};
use crate::rendering::pbr::{BloomBuffer, DownsamplePass, UpsamplePass};
use std::sync::Arc;

/// Bloom pass.
#[derive(Debug)]
pub struct BloomPass {
    downsample_pass: DownsamplePass,
    upsample_pass: UpsamplePass,
}

impl BloomPass {
    /// Creates a new bloom pass.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        Ok(Self {
            downsample_pass: DownsamplePass::new(graphics_context.clone())?,
            upsample_pass: UpsamplePass::new(graphics_context)?,
        })
    }

    /// Fills the bloom buffer.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        light_buffer: Arc<ImageView>,
        bloom_buffer: &BloomBuffer,
    ) -> Result<(), GfxError> {
        self.downsample_pass
            .draw(builder, light_buffer, bloom_buffer)?;
        self.upsample_pass.draw(builder, bloom_buffer)
    }
}
