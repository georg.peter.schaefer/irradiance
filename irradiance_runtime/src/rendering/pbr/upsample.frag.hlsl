[[vk::binding(0, 0)]]
Texture2D src_texture;
[[vk::binding(0, 0)]]
SamplerState src_sampler;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

float4 main(VSOutput input) : SV_TARGET {
    float2 offset = float2(0.005);

    float4 a = src_texture.Sample(src_sampler, input.tex_coord + float2(-offset.x, offset.y));
    float4 b = src_texture.Sample(src_sampler, input.tex_coord + float2(0.0, offset.y));
    float4 c = src_texture.Sample(src_sampler, input.tex_coord + float2(offset.x, offset.y));

    float4 d = src_texture.Sample(src_sampler, input.tex_coord + float2(-offset.x, 0.0));
    float4 e = src_texture.Sample(src_sampler, input.tex_coord + float2(0.0, 0.0));
    float4 f = src_texture.Sample(src_sampler, input.tex_coord + float2(offset.x, 0.0));

    float4 g = src_texture.Sample(src_sampler, input.tex_coord + float2(-offset.x, -offset.y));
    float4 h = src_texture.Sample(src_sampler, input.tex_coord + float2(0.0, -offset.y));
    float4 i = src_texture.Sample(src_sampler, input.tex_coord + float2(offset.y, -offset.y));

    float4 upsample = e * 4.0;
    upsample += (b + d + f + h) * 2.0;
    upsample += (a + c + g + i);
    upsample /= 16.0;
    return upsample;
}

