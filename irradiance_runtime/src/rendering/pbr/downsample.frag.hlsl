[[vk::binding(0, 0)]]
Texture2D src_texture;
[[vk::binding(0, 0)]]
SamplerState src_sampler;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

float4 main(VSOutput input) : SV_TARGET {
    float2 resolution;
    src_texture.GetDimensions(resolution.x, resolution.y);
    float2 texel_size = 1.0 / resolution;

    float4 a = src_texture.Sample(src_sampler, input.tex_coord + float2(-2.0, 2.0) * texel_size);
    float4 b = src_texture.Sample(src_sampler, input.tex_coord + float2(0.0, 2.0) * texel_size);
    float4 c = src_texture.Sample(src_sampler, input.tex_coord + float2(2.0, 2.0) * texel_size);

    float4 d = src_texture.Sample(src_sampler, input.tex_coord + float2(-2.0, 0.0) * texel_size);
    float4 e = src_texture.Sample(src_sampler, input.tex_coord + float2(0.0, 0.0) * texel_size);
    float4 f = src_texture.Sample(src_sampler, input.tex_coord + float2(2.0, 0.0) * texel_size);

    float4 g = src_texture.Sample(src_sampler, input.tex_coord + float2(-2.0, -2.0) * texel_size);
    float4 h = src_texture.Sample(src_sampler, input.tex_coord + float2(0.0, -2.0) * texel_size);
    float4 i = src_texture.Sample(src_sampler, input.tex_coord + float2(2.0, -2.0) * texel_size);

    float4 j = src_texture.Sample(src_sampler, input.tex_coord + float2(-1.0, 1.0) * texel_size);
    float4 k = src_texture.Sample(src_sampler, input.tex_coord + float2(1.0, 1.0) * texel_size);
    float4 l = src_texture.Sample(src_sampler, input.tex_coord + float2(-1.0, -1.0) * texel_size);
    float4 m = src_texture.Sample(src_sampler, input.tex_coord + float2(1.0, -1.0) * texel_size);

    float4 downsample = e * 0.125;
    downsample += (a + c + g + i) * 0.03125;
    downsample += (b + d + f + h) * 0.0625;
    downsample += (j + k + l + m) * 0.125;
    return max(downsample, 0.0001);
}

