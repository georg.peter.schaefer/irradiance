struct Cascade {
    float4x4 view_projection;
    float4x4 split_depth;
};

static const float4x4 bias_matrix = float4x4(
    0.5, 0.0, 0.0, 0.5,
    0.0, 0.5, 0.0, 0.5,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0
);

uint get_cascade_index(StructuredBuffer<Cascade> cascades, float4x4 view, float3 P) {
    uint num_cascades = 0;
    uint stride = 0;
    cascades.GetDimensions(num_cascades, stride);
    uint cascade_index = 0;
    for (uint i = 0; i < num_cascades - 1; ++i) {
        if ((float4(P, 1.0) * view).z < cascades[i].split_depth[0][0]) {
            cascade_index = i + 1;
        }
    }
    return cascade_index;
}

float sample_projected(SamplerState shadow_map_sampler, Texture2DArray shadow_map_texture, float4 shadow_coord, float2 offset, uint cascade_index) {
    float shadow = 1.0;

    if (shadow_coord.z > -1.0 && shadow_coord.z < 1.0) {
        float dist = shadow_map_texture.Sample(shadow_map_sampler, float3(shadow_coord.xy + offset, cascade_index)).r;
        if (shadow_coord.w > 0 && dist < shadow_coord.z) {
            shadow = 0.0;
        }
    }
    return shadow;
}

float filter_pcf(SamplerState shadow_map_sampler, Texture2DArray shadow_map_texture, float4 shadow_coord, uint cascade_index) {
    int3 dimensions;
    shadow_map_texture.GetDimensions(dimensions.x, dimensions.y, dimensions.z);
    float scale = 0.75;
    float dx = scale * 1.0 / float(dimensions.x);
    float dy = scale * 1.0 / float(dimensions.y);

    float shadow_factor = 0.0;
    int count = 0;
    int range = 1;

    for (int x = -range; x <= range; x++) {
        for (int y = -range; y <= range; y++) {
            shadow_factor += sample_projected(shadow_map_sampler, shadow_map_texture, shadow_coord, float2(dx * x, dy * y), cascade_index);
            count++;
        }
    }
    return shadow_factor / count;
}

float sample_shadow(StructuredBuffer<Cascade> cascades, SamplerState shadow_map_sampler, Texture2DArray shadow_map_texture, float4x4 view, float3 P) {
    uint cascade_index = get_cascade_index(cascades, view, P);
    float4 shadow_coord = float4(P, 1.0) * cascades[cascade_index].view_projection * bias_matrix;
    return filter_pcf(shadow_map_sampler, shadow_map_texture, shadow_coord / shadow_coord.w, cascade_index);
}

float sample_cube(SamplerState shadow_map_sampler, TextureCube shadow_map_texture, float3 shadow_coord, float3 offset) {
    float sampled_distance = shadow_map_texture.Sample(shadow_map_sampler, normalize(shadow_coord + offset)).r;
    float distance = length(shadow_coord);
    return (distance <= sampled_distance + 0.15) ? 1.0 : 0.0;
}

float filter_pcf(SamplerState shadow_map_sampler, TextureCube shadow_map_texture, float3 shadow_coord) {
    int2 dimensions;
    shadow_map_texture.GetDimensions(dimensions.x, dimensions.y);
    float scale = 0.75;
    float d = scale * 1.0 / float(dimensions.x);

    float shadow_factor = 0.0;
    int count = 0;
    int range = 1;

    for (int x = -range; x <= range; x++) {
        for (int y = -range; y <= range; y++) {
            for (int z = -range; z <= range; z++) {
                shadow_factor += sample_cube(shadow_map_sampler, shadow_map_texture, shadow_coord, float3(d * x, d * y, d * z));
                count++;
            }
        }
    }
    return shadow_factor / count;
}

float sample_shadow(SamplerState shadow_map_sampler, TextureCube shadow_map_texture, float3 light_position, float3 P) {
    float3 shadow_coord = P - light_position;
    shadow_coord.y *= -1.0;
    return filter_pcf(shadow_map_sampler, shadow_map_texture, shadow_coord);
}
