[[vk::binding(0, 0)]]
TextureCube environment_map_texture;
[[vk::binding(0, 0)]]
SamplerState environment_map_sampler;

struct VSOutput {
    [[vk::location(0)]] float3 world_position : POSITION0;
};

const float PI = 3.14159265358979323846;
const uint SAMPLE_COUNT = 1024;

float radical_inverse_vdc(uint bits) {
    bits = (bits << 16u) | (bits >> 16u);
    bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
    bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
    bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
    bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
    return float(bits) * 2.3283064365386963e-10;
}

float2 hammersley(uint i, uint N) {
    return float2(float(i) / float(N), radical_inverse_vdc(i));
}

float3 importance_sample_ggx(float2 Xi, float3 N, float roughness) {
    float a = roughness * roughness;

    float phi = 2.0 * PI * Xi.x;
    float cos_theta = sqrt((1.0 - Xi.y) / (1.0 + (a * a - 1.0) * Xi.y));
    float sin_theta = sqrt(1.0 - cos_theta * cos_theta);

    float3 H = float3(cos(phi) * sin_theta, sin(phi) * sin_theta, cos_theta);

    float3 up = abs(N.z) < 0.999 ? float3(0.0, 0.0, 1.0) : float3(1.0, 0.0, 0.0);
    float3 tangent = normalize(cross(up, N));
    float3 bitangent = cross(N, tangent);

    float3 sample_vector = tangent * H.x + bitangent * H.y + N * H.z;
    return normalize(sample_vector);
}

struct PushConstants {
    float4x4 orientation;
    float roughness;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

float3 main(VSOutput input) : SV_TARGET {
    float3 N = normalize(input.world_position);
    float3 V = N;

    float total_weight = 0.0;
    float3 pre_filtered_color = float3(0.0);
    for (uint i = 0; i < SAMPLE_COUNT; ++i) {
        float2 Xi = hammersley(i, SAMPLE_COUNT);
        float3 H = importance_sample_ggx(Xi, N, push_constants.roughness);
        float3 L = normalize(2.0 * dot(V, H) * H - V);

        float NdotL = max(dot(N, L), 0.0);
        if (NdotL > 0.0) {
            pre_filtered_color += environment_map_texture.Sample(environment_map_sampler, L).rgb * NdotL;
            total_weight += NdotL;
        }
    }

    return pre_filtered_color / total_weight;;
}
