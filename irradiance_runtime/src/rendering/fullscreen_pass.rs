//! Vertex shader and buffer utility for a full screen pass.

use std::sync::Arc;

use irradiance_runtime::gfx::GfxError;

use crate::gfx::buffer::{Buffer, BufferUsage};
use crate::gfx::device::DeviceOwned;
use crate::gfx::sync::{AccessMask, PipelineStages};
use crate::gfx::GraphicsContext;
use crate::math::Vec2;

/// Creates a full screen vertex buffer.
pub fn create_fullscreen_vertex_buffer(
    graphics_context: &GraphicsContext,
) -> Result<Arc<Buffer>, GfxError> {
    let staging_buffer = Buffer::from_data(
        graphics_context.device().clone(),
        vec![
            Vertex {
                position: Vec2::new(-1.0, -1.0),
                tex_coord: Vec2::new(0.0, 0.0),
            },
            Vertex {
                position: Vec2::new(-1.0, 1.0),
                tex_coord: Vec2::new(0.0, 1.0),
            },
            Vertex {
                position: Vec2::new(1.0, 1.0),
                tex_coord: Vec2::new(1.0, 1.0),
            },
            Vertex {
                position: Vec2::new(-1.0, -1.0),
                tex_coord: Vec2::new(0.0, 0.0),
            },
            Vertex {
                position: Vec2::new(1.0, 1.0),
                tex_coord: Vec2::new(1.0, 1.0),
            },
            Vertex {
                position: Vec2::new(1.0, -1.0),
                tex_coord: Vec2::new(1.0, 0.0),
            },
        ],
    )
    .usage(BufferUsage {
        transfer_src: true,
        ..Default::default()
    })
    .build()?;
    staging_buffer.set_debug_utils_object_name("Fullscreen Vertex Staging Buffer");
    let buffer = Buffer::with_size(graphics_context.device().clone(), staging_buffer.size())
        .usage(BufferUsage {
            transfer_dst: true,
            vertex_buffer: true,
            ..Default::default()
        })
        .build()?;
    staging_buffer.copy_to_buffer(
        graphics_context.graphics_queue().clone(),
        graphics_context.command_pool()?,
        buffer.clone(),
        PipelineStages {
            vertex_input: true,
            ..Default::default()
        },
        AccessMask {
            vertex_attribute_read: true,
            ..Default::default()
        },
    )?;
    Ok(buffer)
}

/// Fullscreen vertex.
#[allow(unused)]
#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct Vertex {
    /// Position attribute.
    pub position: Vec2,
    /// Texture coordinate attribute.
    pub tex_coord: Vec2,
}

#[allow(missing_docs)]
pub mod vertex_shader {
    use crate::shader;

    shader! {
        path: "src/rendering/fullscreen.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}
