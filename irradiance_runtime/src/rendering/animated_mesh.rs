use std::sync::Arc;

use serde::Serialize;

use crate::animation::AnimationGraph;
use crate::asset::AssetRef;
use crate::gfx::buffer::Buffer;
use crate::math::Vec3;
use crate::rendering::SkinnedMesh;
use crate::Component;

/// Animated mesh component
#[derive(Clone, Default, Debug, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct AnimatedMesh {
    /// Last root bone translation.
    #[serde(skip_serializing, skip_deserializing)]
    pub last_root_translation: Option<Vec3>,
    /// Root bone translation.
    #[serde(skip_serializing, skip_deserializing)]
    pub root_translation: Option<Vec3>,
    /// Active animation graph.
    #[serde(skip_serializing, skip_deserializing)]
    pub animation_graph: Option<AnimationGraph>,
    #[serde(skip_serializing, skip_deserializing)]
    joint_buffer: Option<Arc<Buffer>>,
    sub_mesh: Option<usize>,
    mesh: AssetRef<SkinnedMesh>,
}

impl AnimatedMesh {
    /// Creates a new animated mesh component.
    pub fn new(mesh: AssetRef<SkinnedMesh>) -> Self {
        Self {
            last_root_translation: None,
            root_translation: None,
            animation_graph: None,
            joint_buffer: None,
            sub_mesh: None,
            mesh,
        }
    }

    /// Restricts the `AnimatedMesh` to use only the given `sub_mesh`.
    pub fn with_sub_mesh(mut self, sub_mesh: Option<usize>) -> Self {
        self.sub_mesh = sub_mesh;
        self
    }

    /// Returns the [`SkinnedMesh`]
    pub fn mesh(&self) -> &AssetRef<SkinnedMesh> {
        &self.mesh
    }

    /// Sets the [`SkinnedMesh`].
    pub fn set_mesh(&mut self, mesh: AssetRef<SkinnedMesh>) {
        self.mesh = mesh;
    }

    /// Returns the index of the used sub mesh.
    ///
    /// If `None` is returned, the [`AnimatedMesh`] uses the entire [`SkinnedMesh`].
    pub fn sub_mesh(&self) -> Option<usize> {
        self.sub_mesh
    }

    /// Returns the joint buffer.
    pub fn joint_buffer(&self) -> Option<Arc<Buffer>> {
        self.joint_buffer.clone()
    }

    /// Sets the joint buffer.
    pub fn set_joint_buffer(&mut self, joint_buffer: Arc<Buffer>) {
        self.joint_buffer = Some(joint_buffer);
    }

    /// Returns if the current animation has finished playing.
    pub fn is_finished(&self) -> bool {
        self.animation_graph
            .as_ref()
            .map(|animation_graph| animation_graph.finished)
            .unwrap_or_default()
    }

    /// Returns the current root offset.
    pub fn root_offset(&self) -> Vec3 {
        if let Some((root_translation, last_root_translation)) =
            self.root_translation.zip(self.last_root_translation)
        {
            root_translation - last_root_translation
        } else {
            Vec3::zero()
        }
    }
}
