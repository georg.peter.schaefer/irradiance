float3x3 tangent_to_world(float4 tangent, float3 normal) {
    float3 bitangent = cross(normal, tangent.xyz) * tangent.w;
    return float3x3(
        tangent.x, bitangent.x, normal.x,
        tangent.y, bitangent.y, normal.y,
        tangent.z, bitangent.z, normal.z
    );
}