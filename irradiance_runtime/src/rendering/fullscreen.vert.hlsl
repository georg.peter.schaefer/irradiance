struct VSInput {
    [[vk::location(0)]] float2 position : POSITION0;
    [[vk::location(1)]] float2 tex_coord : TEXCOORD0;
};

struct VSOutput {
    float4 position : SV_POSITION;
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

VSOutput main(VSInput input) {
    VSOutput output = (VSOutput)0;

    output.position = float4(input.position, 0.0, 1.0);
    output.tex_coord = input.tex_coord;

    return output;
}