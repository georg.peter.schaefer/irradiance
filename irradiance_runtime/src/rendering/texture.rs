use std::error::Error;
use std::fmt::{Display, Formatter};
use std::path::Path;
use std::sync::Arc;

use image::{DynamicImage, GenericImageView, ImageError, ImageFormat};

use crate::asset::{Asset, Assets};
use crate::gfx::buffer::{Buffer, BufferUsage};
use crate::gfx::command_buffer::{BufferImageCopy, CommandBuffer, ImageBlit};
use crate::gfx::device::DeviceOwned;
use crate::gfx::image::{
    Filter, Format, Image, ImageAspects, ImageLayout, ImageSubresourceLayers,
    ImageSubresourceRange, ImageType, ImageUsage, ImageView, ImageViewType, Sampler,
    SamplerMipMapMode,
};
use crate::gfx::queue::{CommandBufferSubmit, Submit};
use crate::gfx::sync::{ImageMemoryBarrier, PipelineBarrier};
use crate::gfx::{Extent3D, GfxError, GraphicsContext, Offset3D};

/// A sampled image.
#[derive(Debug)]
pub struct Texture {
    sampler: Arc<Sampler>,
    image_view: Arc<ImageView>,
    image: Arc<Image>,
}

impl Texture {
    /// Returns the image.
    pub fn image(&self) -> &Arc<Image> {
        &self.image
    }

    /// Returns the image view.
    pub fn image_view(&self) -> &Arc<ImageView> {
        &self.image_view
    }

    /// Returns the sampler.
    pub fn sampler(&self) -> &Arc<Sampler> {
        &self.sampler
    }

    fn create_image(
        graphics_context: &GraphicsContext,
        dynamic_image: &DynamicImage,
    ) -> Result<Arc<Image>, TextureCreationError> {
        let staging_buffer = Buffer::from_data(
            graphics_context.device().clone(),
            dynamic_image.to_rgba8().into_raw(),
        )
        .usage(BufferUsage {
            transfer_src: true,
            ..Default::default()
        })
        .build()?;
        staging_buffer.set_debug_utils_object_name("Texture Staging Buffer");
        let mip_levels = f32::log2(
            dynamic_image
                .dimensions()
                .0
                .min(dynamic_image.dimensions().1) as _,
        )
        .floor() as u32
            + 1;
        let image = Image::builder(graphics_context.device().clone())
            .image_type(ImageType::Type2D)
            .format(Format::R8G8B8A8UNorm)
            .extent(Extent3D {
                width: dynamic_image.dimensions().0,
                height: dynamic_image.dimensions().1,
                depth: 1,
            })
            .usage(ImageUsage {
                transfer_src: true,
                transfer_dst: true,
                sampled: true,
                ..Default::default()
            })
            .mip_levels(mip_levels)
            .build()?;
        staging_buffer.copy_to_image(
            graphics_context.graphics_queue().clone(),
            graphics_context.command_pool()?,
            image.clone(),
            BufferImageCopy::builder()
                .image_subresource(
                    ImageSubresourceLayers::builder()
                        .image_aspects(ImageAspects {
                            color: true,
                            ..Default::default()
                        })
                        .build(),
                )
                .image_extent(image.extent())
                .build(),
        )?;

        let mut builder = CommandBuffer::primary(
            graphics_context.device().clone(),
            graphics_context.command_pool()?,
        );
        builder.pipeline_barrier(
            PipelineBarrier::builder()
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(image.clone())
                        .old_layout(ImageLayout::ShaderReadOnlyOptimal)
                        .new_layout(ImageLayout::TransferDstOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .base_mip_level(0)
                                .level_count(1)
                                .build(),
                        )
                        .build(),
                )
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(image.clone())
                        .old_layout(ImageLayout::Undefined)
                        .new_layout(ImageLayout::TransferDstOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .base_mip_level(1)
                                .level_count(mip_levels - 1)
                                .build(),
                        )
                        .build(),
                )
                .build(),
        );

        let mut src_offset = Offset3D {
            x: image.extent().width as _,
            y: image.extent().height as _,
            z: 1,
        };
        for src_mip_level in 0..mip_levels - 1 {
            let dst_mip_level = src_mip_level + 1;
            let dst_offset = Offset3D {
                x: if src_offset.x > 1 {
                    src_offset.x / 2
                } else {
                    1
                },
                y: if src_offset.y > 1 {
                    src_offset.y / 2
                } else {
                    1
                },
                z: 1,
            };

            builder
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(image.clone())
                                .old_layout(ImageLayout::TransferDstOptimal)
                                .new_layout(ImageLayout::TransferSrcOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .base_mip_level(src_mip_level)
                                        .level_count(1)
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                )
                .blit_image(
                    image.clone(),
                    ImageLayout::TransferSrcOptimal,
                    image.clone(),
                    ImageLayout::TransferDstOptimal,
                    vec![ImageBlit {
                        src_subresource: ImageSubresourceLayers::builder()
                            .image_aspects(ImageAspects {
                                color: true,
                                ..Default::default()
                            })
                            .mip_level(src_mip_level)
                            .build(),
                        src_offsets: [Offset3D::default(), src_offset],
                        dst_subresource: ImageSubresourceLayers::builder()
                            .image_aspects(ImageAspects {
                                color: true,
                                ..Default::default()
                            })
                            .mip_level(dst_mip_level)
                            .build(),
                        dst_offsets: [Offset3D::default(), dst_offset],
                    }],
                    Filter::Linear,
                )
                .pipeline_barrier(
                    PipelineBarrier::builder()
                        .image_memory_barrier(
                            ImageMemoryBarrier::builder(image.clone())
                                .old_layout(ImageLayout::TransferSrcOptimal)
                                .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                                .subresource_range(
                                    ImageSubresourceRange::builder()
                                        .aspect_mask(ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        })
                                        .base_mip_level(src_mip_level)
                                        .level_count(1)
                                        .build(),
                                )
                                .build(),
                        )
                        .build(),
                );

            src_offset = dst_offset;
        }

        let command_buffer = builder
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(image.clone())
                            .old_layout(ImageLayout::TransferDstOptimal)
                            .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .base_mip_level(mip_levels - 1)
                                    .level_count(1)
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .build()?;
        graphics_context.graphics_queue().submit(
            Submit::builder()
                .command_buffer(CommandBufferSubmit::builder(command_buffer).build())
                .build(),
            None,
        )?;

        Ok(image)
    }
}

impl Asset for Texture {
    type Error = TextureCreationError;

    fn try_from_bytes(
        _assets: Arc<Assets>,
        graphics_context: Arc<GraphicsContext>,
        path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, Self::Error> {
        let dynamic_image =
            image::load_from_memory_with_format(&bytes, ImageFormat::from_path(path)?)?;
        let image = Self::create_image(&graphics_context, &dynamic_image)?;
        let image_view = ImageView::builder(graphics_context.device().clone(), image.clone())
            .view_type(ImageViewType::Type2D)
            .build()?;
        let sampler = Sampler::builder(graphics_context.device().clone())
            .mip_map_mode(SamplerMipMapMode::Linear)
            .min_lod(0.0)
            .max_lod(image.mip_levels() as _)
            .max_anisotropy(
                graphics_context
                    .device()
                    .physical_device()
                    .properties()
                    .limits
                    .max_sampler_anisotropy,
            )
            .build()?;

        image.set_debug_utils_object_name("Texture Image");
        image_view.set_debug_utils_object_name("Texture Image View");
        sampler.set_debug_utils_object_name("Texture Sampler");

        Ok(Self {
            sampler,
            image_view,
            image,
        })
    }
}

/// Errors which can occur when creating a [`Texture`].
#[derive(Debug)]
pub enum TextureCreationError {
    /// An image loading operation has failed.
    ImageLoadingFailed(ImageError),
    /// A graphics API operation has failed.
    GfxOperationFailed(GfxError),
}

impl Display for TextureCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            TextureCreationError::ImageLoadingFailed(_) => {
                write!(f, "An image loading operation has failed")
            }
            TextureCreationError::GfxOperationFailed(_) => {
                write!(f, "A graphics API operation has failed")
            }
        }
    }
}

impl std::error::Error for TextureCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            TextureCreationError::ImageLoadingFailed(e) => Some(e),
            TextureCreationError::GfxOperationFailed(e) => Some(e),
        }
    }
}

impl From<ImageError> for TextureCreationError {
    fn from(e: ImageError) -> Self {
        Self::ImageLoadingFailed(e)
    }
}

impl From<GfxError> for TextureCreationError {
    fn from(e: GfxError) -> Self {
        Self::GfxOperationFailed(e)
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read;
    use std::path::Path;

    use image::error::{LimitError, LimitErrorKind};
    use image::ImageError;

    use crate::asset::{Asset, Assets, FilesystemSource};
    use crate::gfx::image::{Format, ImageTiling, ImageType, ImageUsage, SampleCount};
    use crate::gfx::sync::SharingMode;
    use crate::gfx::{Extent3D, GfxError, GraphicsContext};
    use crate::rendering::{Texture, TextureCreationError};

    #[test]
    fn texture_try_from_bytes_with() {
        let graphics_context = GraphicsContext::builder().build().unwrap();
        let assets = Assets::builder()
            .source(FilesystemSource::new("rsc/test/rendering").unwrap())
            .build();
        let texture = Texture::try_from_bytes(
            assets.clone(),
            graphics_context.clone(),
            Path::new("texture.png"),
            read("rsc/test/rendering/texture.png").unwrap(),
        )
        .unwrap();

        assert_eq!(ImageType::Type2D, texture.image.image_type());
        assert_eq!(Format::R8G8B8A8Srgb, texture.image.format());
        assert_eq!(
            Extent3D {
                width: 4,
                height: 4,
                depth: 1
            },
            texture.image.extent()
        );
        assert_eq!(1, texture.image.mip_levels());
        assert_eq!(1, texture.image.array_layers());
        assert_eq!(SampleCount::Sample1, texture.image.samples());
        assert_eq!(ImageTiling::Optimal, texture.image.tiling());
        assert_eq!(
            ImageUsage {
                transfer_dst: true,
                sampled: true,
                ..Default::default()
            },
            texture.image.usage()
        );
        assert_eq!(SharingMode::Exclusive, texture.image.sharing_mode());

        graphics_context.device().wait().unwrap();
    }

    #[test]
    fn texture_creation_error_from() {
        assert!(
            matches!(TextureCreationError::from(ImageError::Limits(LimitError::from_kind(
            LimitErrorKind::DimensionError,
        ))), TextureCreationError::ImageLoadingFailed(ImageError::Limits(limits_error)) if limits_error.kind() == LimitErrorKind::DimensionError)
        );
        assert!(matches!(
            TextureCreationError::from(GfxError::OutOfDeviceMemory),
            TextureCreationError::GfxOperationFailed(GfxError::OutOfDeviceMemory)
        ));
    }
}
