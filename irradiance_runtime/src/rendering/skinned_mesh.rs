//! Skinned mesh.

use std::error::Error;
use std::fmt::{Display, Formatter};
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::Arc;

use gltf::buffer::Data;
use gltf::{json, Document};

use crate::asset::{Asset, Assets};
use crate::ecs::Transform;
use crate::gfx::buffer::{Buffer, BufferUsage};
use crate::gfx::device::DeviceOwned;
use crate::gfx::sync::{AccessMask, PipelineStages};
use crate::gfx::{GfxError, GraphicsContext};
use crate::math::{Mat4, Quat, Vec2, Vec3, Vec4};
use crate::rendering::{BoundingBox, SubMesh};

/// A skinned mesh.
#[derive(Clone, Debug)]
pub struct SkinnedMesh {
    joints: Vec<Joint>,
    bounding_box: BoundingBox,
    sub_meshes: Vec<SubMesh>,
    index_buffer: Arc<Buffer>,
    vertex_buffer: Arc<Buffer>,
    _buffers: Vec<Data>,
    document: Document,
}

impl SkinnedMesh {
    /// Returns the vertex buffer.
    pub fn vertex_buffer(&self) -> &Arc<Buffer> {
        &self.vertex_buffer
    }

    /// Returns the index buffer.
    pub fn index_buffer(&self) -> &Arc<Buffer> {
        &self.index_buffer
    }

    /// Returns the sub meshes.
    pub fn sub_meshes(&self) -> &Vec<SubMesh> {
        &self.sub_meshes
    }

    /// Returns a mutable slice to the sub meshes.
    pub fn sub_meshes_mut(&mut self) -> &mut [SubMesh] {
        &mut self.sub_meshes
    }

    /// Returns the bounding box.
    pub fn bounding_box(&self) -> BoundingBox {
        self.bounding_box
    }

    /// Returns the joints of the skeleton.
    pub fn joints(&self) -> &Vec<Joint> {
        &self.joints
    }

    fn create_buffer<T>(
        graphics_context: &GraphicsContext,
        data: Vec<T>,
        usage: BufferUsage,
        dst_stage_mask: PipelineStages,
        dst_access_mask: AccessMask,
    ) -> Result<Arc<Buffer>, SkinnedMeshCreationError> {
        let size = (std::mem::size_of::<T>() * data.len()) as _;
        let staging_buffer = Buffer::from_data(graphics_context.device().clone(), data)
            .usage(BufferUsage {
                transfer_src: true,
                ..Default::default()
            })
            .build()?;
        let buffer = Buffer::with_size(graphics_context.device().clone(), size)
            .usage(usage.union(&BufferUsage {
                transfer_dst: true,
                ..Default::default()
            }))
            .build()?;
        staging_buffer.set_debug_utils_object_name("Mesh Staging Buffer");
        staging_buffer.copy_to_buffer(
            graphics_context.graphics_queue().clone(),
            graphics_context.command_pool()?,
            buffer.clone(),
            dst_stage_mask,
            dst_access_mask,
        )?;
        Ok(buffer)
    }

    fn create_vertex_buffer(
        graphics_context: &GraphicsContext,
        vertices: Vec<Vertex>,
    ) -> Result<Arc<Buffer>, SkinnedMeshCreationError> {
        Self::create_buffer(
            graphics_context,
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            PipelineStages {
                vertex_input: true,
                ..Default::default()
            },
            AccessMask {
                vertex_attribute_read: true,
                ..Default::default()
            },
        )
    }

    fn create_index_buffer(
        graphics_context: &GraphicsContext,
        indices: Vec<u32>,
    ) -> Result<Arc<Buffer>, SkinnedMeshCreationError> {
        Self::create_buffer(
            graphics_context,
            indices,
            BufferUsage {
                index_buffer: true,
                ..Default::default()
            },
            PipelineStages {
                index_input: true,
                ..Default::default()
            },
            AccessMask {
                index_read: true,
                ..Default::default()
            },
        )
    }
}

impl Asset for SkinnedMesh {
    type Error = SkinnedMeshCreationError;

    fn try_from_bytes(
        assets: Arc<Assets>,
        graphics_context: Arc<GraphicsContext>,
        _path: &Path,
        bytes: Vec<u8>,
    ) -> Result<Self, Self::Error> {
        let (document, buffers, _) = gltf::import_slice(&bytes).unwrap();

        let mut joints = Vec::new();
        match document.skins().next() {
            Some(skin) => {
                let reader = skin.reader(|buffer| Some(&buffers[buffer.index()].0));
                let inverse_bind_matrices = reader
                    .read_inverse_bind_matrices()
                    .ok_or(SkinnedMeshCreationError::MissingInverseBindMatrices)?
                    .map(|m| {
                        Mat4::from([
                            Vec4::from(m[0]),
                            Vec4::from(m[1]),
                            Vec4::from(m[2]),
                            Vec4::from(m[3]),
                        ])
                    })
                    .collect::<Vec<_>>();

                for joint in skin.joints() {
                    let (position, orientation, scale) = joint.transform().decomposed();
                    joints.push(Joint {
                        inverse_bind_matrix: inverse_bind_matrices[joints.len()],
                        transform: Transform::new(
                            Vec3::from(position),
                            Quat::from(orientation),
                            Vec3::from(scale),
                        ),
                        parent: skin.joints().position(|parent| {
                            parent
                                .children()
                                .find(|child| child.index() == joint.index())
                                .is_some()
                        }),
                    })
                }
            }
            None => return Err(SkinnedMeshCreationError::MissingSkin),
        }

        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let mut sub_meshes = Vec::new();
        let mut min = Vec3::new(f32::MAX, f32::MAX, f32::MAX);
        let mut max = Vec3::new(f32::MIN, f32::MIN, f32::MIN);
        match document.meshes().next() {
            Some(mesh) => {
                for primitive in mesh.primitives() {
                    let reader = primitive.reader(|buffer| Some(&buffers[buffer.index()].0));
                    let vertex_offset = vertices.len() as u32;
                    let index_offset = indices.len();

                    vertices.extend(
                        itertools::multizip((
                            reader
                                .read_positions()
                                .ok_or(SkinnedMeshCreationError::MissingPositions)?,
                            reader
                                .read_normals()
                                .ok_or(SkinnedMeshCreationError::MissingNormals)?,
                            reader
                                .read_tangents()
                                .ok_or(SkinnedMeshCreationError::MissingTangents)?,
                            reader
                                .read_tex_coords(0)
                                .ok_or(SkinnedMeshCreationError::MissingTexCoords)?
                                .into_f32(),
                            reader
                                .read_joints(0)
                                .ok_or(SkinnedMeshCreationError::MissingJoints)?
                                .into_u16(),
                            reader
                                .read_weights(0)
                                .ok_or(SkinnedMeshCreationError::MissingWeights)?
                                .into_f32(),
                        ))
                        .map(
                            |(position, normal, tangent, tex_coord, joint, weight)| Vertex {
                                position: Vec3::from(position),
                                normal: Vec3::from(normal),
                                tangent: Vec4::from(tangent),
                                tex_coord: Vec2::from(tex_coord),
                                joints: Vec4::new(
                                    joint[0] as _,
                                    joint[1] as _,
                                    joint[2] as _,
                                    joint[3] as _,
                                ),
                                weight: Vec4::from(weight),
                            },
                        ),
                    );

                    indices.extend(
                        reader
                            .read_indices()
                            .ok_or(SkinnedMeshCreationError::MissingIndices)?
                            .into_u32()
                            .map(|index| index + vertex_offset),
                    );

                    let material = primitive.material();
                    let material_name = PathBuf::from_str(
                        material
                            .name()
                            .ok_or(SkinnedMeshCreationError::MissingMaterialName)?,
                    )
                    .unwrap();

                    min = min.min(&primitive.bounding_box().min.into());
                    max = max.max(&primitive.bounding_box().max.into());
                    sub_meshes.push(SubMesh {
                        bounding_box: primitive.bounding_box().into(),
                        material: assets
                            .read(graphics_context.clone(), material_name)
                            .unwrap_or_default(),
                        count: indices.len() - index_offset,
                        offset: index_offset,
                    });
                }
            }
            None => return Err(SkinnedMeshCreationError::MissingMesh),
        }

        let vertex_buffer = Self::create_vertex_buffer(&graphics_context, vertices.clone())?;
        let index_buffer = Self::create_index_buffer(&graphics_context, indices.clone())?;

        vertex_buffer.set_debug_utils_object_name("Mesh Vertex Buffer");
        index_buffer.set_debug_utils_object_name("Mesh Index Buffer");

        Ok(Self {
            joints,
            bounding_box: BoundingBox::new(min, max),
            sub_meshes,
            index_buffer,
            vertex_buffer,
            _buffers: buffers,
            document,
        })
    }

    fn to_bytes(&self) -> Option<Vec<u8>> {
        let mut root = self.document.clone().into_json();

        if let Some(mesh) = root.meshes.first() {
            for (path, material_index) in
                mesh.primitives
                    .iter()
                    .enumerate()
                    .map(|(sub_mesh_index, primitive)| {
                        (
                            self.sub_meshes[sub_mesh_index]
                                .material
                                .path()
                                .unwrap_or_default(),
                            primitive.material.unwrap(),
                        )
                    })
            {
                root.materials[material_index.value()].name =
                    Some(path.to_str().unwrap().to_string());
            }
        }

        json::serialize::to_vec(&root).ok()
    }
}

/// Errors which can occur when creating a [`SkinnedMesh`].
#[derive(Debug)]
pub enum SkinnedMeshCreationError {
    /// Missing mesh.
    MissingMesh,
    /// Missing positions.
    MissingPositions,
    /// Missing normals.
    MissingNormals,
    /// Missing tangents.
    MissingTangents,
    /// Missing texture coords.
    MissingTexCoords,
    /// Missing joints
    MissingJoints,
    /// Missing weights.
    MissingWeights,
    /// Missing indices.
    MissingIndices,
    /// Missing material name.
    MissingMaterialName,
    /// A graphics API operation has failed.
    GfxOperationFailed(GfxError),
    /// Missing skin.
    MissingSkin,
    /// Missing inverse bind matrices.
    MissingInverseBindMatrices,
}

impl Display for SkinnedMeshCreationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            SkinnedMeshCreationError::MissingMesh => write!(f, "Missing mesh"),
            SkinnedMeshCreationError::MissingPositions => write!(f, "Missing positions"),
            SkinnedMeshCreationError::MissingNormals => write!(f, "Missing normals"),
            SkinnedMeshCreationError::MissingTangents => write!(f, "Missing tangents"),
            SkinnedMeshCreationError::MissingTexCoords => write!(f, "Missing texture coords"),
            SkinnedMeshCreationError::MissingJoints => write!(f, "Missing joints"),
            SkinnedMeshCreationError::MissingWeights => write!(f, "Missing weights"),
            SkinnedMeshCreationError::MissingIndices => write!(f, "Missing indices"),
            SkinnedMeshCreationError::MissingMaterialName => write!(f, "Missing material name"),
            SkinnedMeshCreationError::GfxOperationFailed(_) => {
                write!(f, "A graphics API operation has failed")
            }
            SkinnedMeshCreationError::MissingSkin => write!(f, "Missing skin"),
            SkinnedMeshCreationError::MissingInverseBindMatrices => {
                write!(f, "Missing inverse bind matrices")
            }
        }
    }
}

impl Error for SkinnedMeshCreationError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            SkinnedMeshCreationError::GfxOperationFailed(e) => Some(e),
            _ => None,
        }
    }
}

impl From<GfxError> for SkinnedMeshCreationError {
    fn from(e: GfxError) -> Self {
        SkinnedMeshCreationError::GfxOperationFailed(e)
    }
}

/// A skinned mesh vertex.
#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct Vertex {
    /// The position.
    pub position: Vec3,
    /// The normal.
    pub normal: Vec3,
    /// The tangent.
    pub tangent: Vec4,
    /// The texture coordinates.
    pub tex_coord: Vec2,
    /// The joints.
    pub joints: Vec4,
    /// The vertex weight.
    pub weight: Vec4,
}

/// A skeleton joint.
#[derive(Copy, Clone, Debug)]
pub struct Joint {
    /// Inverse bind matrix.
    pub inverse_bind_matrix: Mat4,
    /// Transform of the joint.
    pub transform: Transform,
    /// Parent joint index.
    pub parent: Option<usize>,
}
