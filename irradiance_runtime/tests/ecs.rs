use irradiance_runtime::ecs::Entities;
use irradiance_runtime::Component;

#[derive(Clone, Component)]
struct ComponentA {
    value: f32,
}

#[derive(Clone, Component)]
struct ComponentB {
    value: String,
}

#[test]
fn get_none() {
    let entities = Entities::new();
    let entity = entities.get("test");

    assert!(entity.is_none());
}

#[test]
fn get() {
    let entities = Entities::new();
    let entity = entities.create("test").unwrap();

    entity.insert(ComponentA { value: 42.0 });
    unsafe {
        entities.commit();
    }

    assert_eq!("test", entities.get("test").unwrap().id())
}

#[test]
fn create_error() {
    let entities = Entities::new();
    let entity = entities.create("test").unwrap();

    entity.insert(ComponentA { value: 42.0 });
    unsafe {
        entities.commit();
    }

    let result = entities.create("test").err();

    assert!(result.is_some())
}

#[test]
fn create() {
    let entities = Entities::new();
    let entity = entities.create("test").unwrap();

    assert_eq!("test", entity.id());
}

#[test]
fn remove() {
    let entities = Entities::new();
    let entity = entities.create("test").unwrap();

    entity.insert(ComponentA { value: 42.0 });
    entity.insert(ComponentB {
        value: "test".to_string(),
    });
    unsafe {
        entities.commit();
    }

    entities.remove(&entity);

    assert!(entity.get::<ComponentA>().is_some());
    assert!(entity.get::<ComponentB>().is_some());

    unsafe {
        entities.commit();
    }

    assert!(entity.get::<ComponentA>().is_none());
    assert!(entity.get::<ComponentB>().is_none());
}

#[test]
fn components() {
    let entities = Entities::new();
    let test = entities.create("test").unwrap();
    test.insert(ComponentA { value: 42.0 });
    test.insert(ComponentB {
        value: "test".to_string(),
    });
    let other = entities.create("other").unwrap();
    other.insert(ComponentA { value: 19.0 });
    unsafe {
        entities.commit();
    }

    assert_eq!(2, entities.components::<(&ComponentA,)>().count());
    assert_eq!(1, entities.components::<(&ComponentB,)>().count());

    for (_, _a, mut b) in entities.components::<(&ComponentA, &mut ComponentB)>() {
        b.value = "modified".to_string();
    }
    unsafe {
        entities.commit();
    }

    assert_eq!(42.0, test.get::<ComponentA>().unwrap().value);
    assert_eq!("modified", test.get::<ComponentB>().unwrap().value);
    assert_eq!(19.0, other.get::<ComponentA>().unwrap().value);
}

#[test]
fn insert_components() {
    let entities = Entities::new();
    let entity = entities.create("test").unwrap();

    entity.insert(ComponentA { value: 42.0 });
    entity.insert(ComponentB {
        value: "test".to_string(),
    });

    assert!(entity.get::<ComponentA>().is_none());
    assert!(entity.get::<ComponentB>().is_none());

    unsafe {
        entities.commit();
    }

    assert_eq!(42.0, entity.get::<ComponentA>().unwrap().value);
    assert_eq!("test", entity.get::<ComponentB>().unwrap().value);
}

#[test]
fn remove_components() {
    let entities = Entities::new();
    let entity = entities.create("test").unwrap();

    entity.insert(ComponentA { value: 42.0 });
    entity.insert(ComponentB {
        value: "test".to_string(),
    });
    unsafe {
        entities.commit();
    }

    entity.remove::<ComponentB>();
    assert!(entity.get::<ComponentB>().is_some());
    unsafe {
        entities.commit();
    }

    assert!(entity.get::<ComponentB>().is_none());
}

#[test]
fn mutable_reference() {
    let entities = Entities::new();
    let entity = entities.create("test").unwrap();

    entity.insert(ComponentA { value: 42.0 });
    entity.insert(ComponentB {
        value: "test".to_string(),
    });
    unsafe {
        entities.commit();
    }

    entity.get_mut::<ComponentB>().unwrap().value = "modified".to_string();
    unsafe {
        entities.commit();
    }

    assert_eq!("modified", entity.get::<ComponentB>().unwrap().value)
}
