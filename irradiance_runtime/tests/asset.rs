// use irradiance_runtime::asset::{Asset, Assets, FilesystemSource};
// use irradiance_runtime::gfx::GraphicsContext;
// use std::path::Path;
// use std::sync::Arc;
// use std::time::Duration;

// #[derive(Debug)]
// struct TestAsset {
//     pub value: String,
// }

// impl Asset for TestAsset {
//     type Error = std::io::Error;

//     fn try_from_bytes(
//         _assets: Arc<Assets>,
//         _graphics_context: Arc<GraphicsContext>,
//         _path: &Path,
//         bytes: Vec<u8>,
//     ) -> Result<Self, Self::Error> {
//         std::thread::sleep(Duration::from_secs(2));
//         Ok(Self {
//             value: String::from_utf8(bytes)
//                 .map_err(|e| Self::Error::new(std::io::ErrorKind::InvalidData, e))?,
//         })
//     }
// }

// #[derive(Debug)]
// struct ErrorAsset;

// impl Asset for ErrorAsset {
//     type Error = std::io::Error;

//     fn try_from_bytes(
//         _assets: Arc<Assets>,
//         _graphics_context: Arc<GraphicsContext>,
//         _path: &Path,
//         _bytes: Vec<u8>,
//     ) -> Result<Self, Self::Error> {
//         std::thread::sleep(Duration::from_secs(2));
//         Err(std::io::Error::from(std::io::ErrorKind::InvalidData))
//     }
// }

// fn setup() -> Arc<Assets> {
//     Assets::new()
//         .source(FilesystemSource::new("rsc/test/asset").unwrap())
//         .build()
// }

// #[test]
// fn access_asset_when_ready() {
//     let assets = setup();
//     let asset = assets.read::<TestAsset, _>("test").unwrap();
//     let mut ready = false;

//     while !ready {
//         asset.ok().map(|_| ready = true);
//     }

//     assert!(ready);
// }

// #[test]
// fn access_asset_blocking() {
//     let assets = setup();
//     let asset = assets.read::<TestAsset, _>("test").unwrap();

//     assert_eq!("test", asset.unwrap().value);
// }

// #[test]
// fn access_error_when_ready() {
//     let assets = setup();
//     let asset = assets.read::<ErrorAsset, _>("test").unwrap();
//     let mut ready = false;

//     while !ready {
//         asset.err().map(|_| ready = true);
//     }

//     assert!(ready);
// }

// #[test]
// fn access_error_blocking() {
//     let assets = setup();
//     let asset = assets.read::<ErrorAsset, _>("test").unwrap();

//     assert_eq!(std::io::ErrorKind::InvalidData, asset.unwrap_err().kind());
// }
