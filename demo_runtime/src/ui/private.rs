use std::sync::Arc;

use crate::ui::private::shaders::PushConstants;
use crate::ui::widget::Widget;
use irradiance_runtime::gfx::buffer::{Buffer, BufferUsage};
use irradiance_runtime::gfx::command_buffer::CommandBufferBuilder;
use irradiance_runtime::gfx::device::{Device, DeviceOwned};
use irradiance_runtime::gfx::image::{
    Format, ImageAspects, ImageLayout, ImageSubresourceRange, ImageView, Sampler,
    SamplerAddressMode,
};
use irradiance_runtime::gfx::pipeline::{
    BlendFactor, BlendOp, ColorBlendAttachmentState, CullMode, DescriptorSet, DescriptorSetLayout,
    DescriptorType, DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint, PipelineLayout,
    PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
    WriteDescriptorSetImages,
};
use irradiance_runtime::gfx::render_pass::{
    Attachment, AttachmentLoadOp, AttachmentStoreOp, ClearValue, RenderPass,
};
use irradiance_runtime::gfx::sync::{
    AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages,
};
use irradiance_runtime::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use irradiance_runtime::math::{Vec2, Vec4};
use irradiance_runtime::rendering::fullscreen_pass::Vertex;

/// Ui renderer.
pub struct Ui {
    sampler: Arc<Sampler>,
    vertex_buffer: Arc<Buffer>,
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl Ui {
    /// Creates a new ui renderer.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(graphics_context.device().clone())?;
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            descriptor_set_layout.clone(),
        )?;
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            graphics_context.swapchain().unwrap().image_format(),
            pipeline_layout.clone(),
        )?;
        let vertex_buffer = Self::create_vertex_buffer(&graphics_context)?;
        let sampler = Sampler::builder(graphics_context.device().clone())
            .address_mode_u(SamplerAddressMode::ClampToEdge)
            .address_mode_v(SamplerAddressMode::ClampToEdge)
            .address_mode_w(SamplerAddressMode::ClampToEdge)
            .build()?;

        descriptor_set_layout.set_debug_utils_object_name("Ui Descriptor Set Layout");
        pipeline_layout.set_debug_utils_object_name("Ui Descriptor Set Layout");
        graphics_pipeline.set_debug_utils_object_name("Ui Graphics Pipeline");
        vertex_buffer.set_debug_utils_object_name("Ui Fullscreen Vertex Buffer");

        Ok(Self {
            sampler,
            vertex_buffer,
            graphics_pipeline,
            pipeline_layout,
            descriptor_set_layout,
            graphics_context,
        })
    }

    /// Draws the ui.
    pub fn draw(
        &self,
        overlay: bool,
        builder: &mut CommandBufferBuilder,
        final_image: Arc<ImageView>,
        widgets: &[&dyn Widget],
    ) -> Result<(), GfxError> {
        builder
            .begin_label("Ui Pass", Vec4::new(0.66, 0.33, 0.22, 1.0))
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(final_image.image().clone())
                            .src_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .bind_graphics_pipeline(self.graphics_pipeline.clone())
            .begin_rendering(self.create_render_pass(final_image.clone(), overlay))
            .set_viewport(
                0,
                vec![Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: final_image.image().extent().width as _,
                    height: final_image.image().extent().height as _,
                    min_depth: 0.0,
                    max_depth: 1.0,
                }],
            )
            .set_scissor(
                0,
                vec![Rect2D {
                    offset: Default::default(),
                    extent: final_image.image().extent().into(),
                }],
            )
            .bind_vertex_buffer(self.vertex_buffer.clone());

        for widget in widgets {
            let descriptor_set = DescriptorSet::builder(
                self.graphics_context.device().clone(),
                self.graphics_context
                    .descriptor_pool(&self.descriptor_set_layout)?,
                self.descriptor_set_layout.clone(),
            )
            .build()?;
            descriptor_set.set_debug_utils_object_name("Ui Descriptor Set");

            descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &self.sampler,
                    &widget.texture().image_view(),
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build()]);

            builder
                .push_constants(
                    self.pipeline_layout.clone(),
                    ShaderStages {
                        vertex: true,
                        ..Default::default()
                    },
                    0,
                    PushConstants {
                        resolution: Vec2::new(
                            final_image.image().extent().width as _,
                            final_image.image().extent().height as _,
                        ),
                        size: widget.size(),
                        offset: widget.aligned_offset(&final_image),
                    },
                )
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    self.pipeline_layout.clone(),
                    0,
                    vec![descriptor_set],
                )
                .draw(6, 1, 0, 0);
        }

        builder.end_rendering().end_label();

        Ok(())
    }
}

impl Ui {
    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        format: Format,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32SFloat, 0)
                    .attribute(1, 0, Format::R32G32SFloat, 8)
                    .build(),
            )
            .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 768.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .color_blend_attachment_state(
                ColorBlendAttachmentState::builder()
                    .enable_blend()
                    .src_color_blend_factor(BlendFactor::SrcAlpha)
                    .dst_color_blend_factor(BlendFactor::OneMinusSrcAlpha)
                    .color_blend_op(BlendOp::Add)
                    .src_alpha_blend_factor(BlendFactor::SrcAlpha)
                    .dst_alpha_blend_factor(BlendFactor::OneMinusSrcAlpha)
                    .alpha_blend_op(BlendOp::Add)
                    .build(),
            )
            .layout(pipeline_layout)
            .color_attachment_format(format)
            .build()
    }

    fn create_vertex_buffer(graphics_context: &GraphicsContext) -> Result<Arc<Buffer>, GfxError> {
        let staging_buffer = Buffer::from_data(
            graphics_context.device().clone(),
            vec![
                Vertex {
                    position: Vec2::new(0.0, 0.0),
                    tex_coord: Vec2::new(0.0, 0.0),
                },
                Vertex {
                    position: Vec2::new(0.0, 1.0),
                    tex_coord: Vec2::new(0.0, 1.0),
                },
                Vertex {
                    position: Vec2::new(1.0, 1.0),
                    tex_coord: Vec2::new(1.0, 1.0),
                },
                Vertex {
                    position: Vec2::new(0.0, 0.0),
                    tex_coord: Vec2::new(0.0, 0.0),
                },
                Vertex {
                    position: Vec2::new(1.0, 1.0),
                    tex_coord: Vec2::new(1.0, 1.0),
                },
                Vertex {
                    position: Vec2::new(1.0, 0.0),
                    tex_coord: Vec2::new(1.0, 0.0),
                },
            ],
        )
        .usage(BufferUsage {
            transfer_src: true,
            ..Default::default()
        })
        .build()?;
        staging_buffer.set_debug_utils_object_name("Vertex Staging Buffer");
        let buffer = Buffer::with_size(graphics_context.device().clone(), staging_buffer.size())
            .usage(BufferUsage {
                transfer_dst: true,
                vertex_buffer: true,
                ..Default::default()
            })
            .build()?;
        staging_buffer.copy_to_buffer(
            graphics_context.graphics_queue().clone(),
            graphics_context.command_pool()?,
            buffer.clone(),
            PipelineStages {
                vertex_input: true,
                ..Default::default()
            },
            AccessMask {
                vertex_attribute_read: true,
                ..Default::default()
            },
        )?;
        Ok(buffer)
    }

    fn create_render_pass(&self, final_image: Arc<ImageView>, overlay: bool) -> RenderPass {
        let image_extent = final_image.image().extent();
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: image_extent.into(),
            })
            .color_attachment(if overlay {
                Attachment::builder(final_image)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build()
            } else {
                Attachment::builder(final_image)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Float([0.0, 0.0, 0.0, 1.0]))
                    .build()
            })
            .build()
    }
}

mod shaders {
    use irradiance_runtime::math::Vec2;

    #[allow(unused)]
    #[derive(Debug)]
    pub struct PushConstants {
        pub resolution: Vec2,
        pub size: Vec2,
        pub offset: Vec2,
    }
}

mod vertex_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/ui/ui.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod fragment_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/ui/ui.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
