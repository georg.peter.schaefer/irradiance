use irradiance_runtime::asset::AssetRef;
use irradiance_runtime::core::Engine;
use irradiance_runtime::gfx::image::ImageView;
use irradiance_runtime::math::Vec2;
use irradiance_runtime::rendering::Texture;

use crate::ui::{Align, Widget};

/// Button widget.
pub struct Button {
    clicked: bool,
    hovered: bool,
    highlight_texture: AssetRef<Texture>,
    texture: AssetRef<Texture>,
    offset: Vec2,
    alignment: Align,
}

impl Button {
    /// Creates a new image widget.
    pub fn new(
        texture: AssetRef<Texture>,
        highlight_texture: AssetRef<Texture>,
        offset: Vec2,
        alignment: Align,
    ) -> Self {
        Self {
            clicked: false,
            hovered: false,
            highlight_texture,
            texture,
            offset,
            alignment,
        }
    }

    /// Returns if the button has been clicked.
    pub fn clicked(&self) -> bool {
        self.clicked
    }
}

impl Widget for Button {
    fn texture(&self) -> &Texture {
        if self.hovered {
            self.highlight_texture.unwrap()
        } else {
            self.texture.unwrap()
        }
    }

    fn size(&self) -> Vec2 {
        Vec2::new(
            self.texture().image().extent().width as _,
            self.texture().image().extent().height as _,
        )
    }

    fn offset(&self) -> Vec2 {
        self.offset
    }

    fn alignment(&self) -> Align {
        self.alignment
    }

    fn update(&mut self, engine: &mut dyn Engine, final_image: &ImageView) {
        self.hovered = false;
        self.clicked = false;

        let cursor = Vec2::new(
            engine.input().axis("Mouse/Horizontal").get(),
            engine.input().axis("Mouse/Vertical").get(),
        );
        let position = self.aligned_offset(final_image);
        let size = self.size();

        if cursor[0] > position[0]
            && cursor[0] < (position + size)[0]
            && cursor[1] > position[1]
            && cursor[1] < (position + size)[1]
        {
            self.hovered = true;
        }

        if self.hovered && engine.input().button("Mouse/Left").pressed() {
            self.clicked = true;
        }
    }
}
