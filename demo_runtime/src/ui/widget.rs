use irradiance_runtime::core::Engine;
use irradiance_runtime::gfx::image::ImageView;
use irradiance_runtime::math::Vec2;
use irradiance_runtime::rendering::Texture;

/// Widget trait.
pub trait Widget {
    /// Returns the texture for the widget.
    fn texture(&self) -> &Texture;
    /// Returns the size of the widget.
    fn size(&self) -> Vec2;
    /// Returns the offset of the widget.
    fn offset(&self) -> Vec2;
    /// Returns the alignment.
    fn alignment(&self) -> Align {
        Align::None
    }
    /// Calculates the aligned offset.
    fn aligned_offset(&self, final_image: &ImageView) -> Vec2 {
        let extent = final_image.image().extent();
        match self.alignment() {
            Align::None => self.offset(),
            Align::CenterX => {
                Vec2::new(extent.width as f32 * 0.5 - self.size()[0] * 0.5, 0.0) + self.offset()
            }
            Align::CenterY => {
                Vec2::new(0.0, extent.height as f32 * 0.5 - self.size()[1] * 0.5) + self.offset()
            }
            Align::Center => {
                Vec2::new(
                    extent.width as f32 * 0.5 - self.size()[0] * 0.5,
                    extent.height as f32 * 0.5 - self.size()[1] * 0.5,
                ) + self.offset()
            }
        }
    }
    /// Updates the widget state.
    fn update(&mut self, engine: &mut dyn Engine, final_image: &ImageView);
}

/// Alignment.
#[derive(Copy, Clone)]
pub enum Align {
    /// No alignment.
    None,
    /// Align center along the x axis.
    CenterX,
    /// Align center on the y axis.
    CenterY,
    /// Align center.
    Center,
}
