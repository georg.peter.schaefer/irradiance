use irradiance_runtime::asset::AssetRef;
use irradiance_runtime::core::Engine;
use irradiance_runtime::gfx::image::ImageView;
use irradiance_runtime::math::Vec2;
use irradiance_runtime::rendering::Texture;

use crate::ui::widget::Align;
use crate::ui::Widget;

/// Image widget.
pub struct Image {
    texture: AssetRef<Texture>,
    offset: Vec2,
    alignment: Align,
}

impl Image {
    /// Creates a new image widget.
    pub fn new(texture: AssetRef<Texture>, offset: Vec2, alignment: Align) -> Self {
        Self {
            texture,
            offset,
            alignment,
        }
    }
}

impl Widget for Image {
    fn texture(&self) -> &Texture {
        self.texture.unwrap()
    }

    fn size(&self) -> Vec2 {
        Vec2::new(
            self.texture().image().extent().width as _,
            self.texture().image().extent().height as _,
        )
    }

    fn offset(&self) -> Vec2 {
        self.offset
    }

    fn alignment(&self) -> Align {
        self.alignment
    }

    fn update(&mut self, _engine: &mut dyn Engine, _final_image: &ImageView) {}
}
