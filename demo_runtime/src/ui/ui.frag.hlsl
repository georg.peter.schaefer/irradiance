[[vk::binding(0, 0)]]
Texture2D widget_texture;
[[vk::binding(0, 0)]]
SamplerState widget_sampler;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

float4 main(VSOutput input) : SV_TARGET {
    return widget_texture.Sample(widget_sampler, input.tex_coord);
}