use irradiance_runtime::animation::{AnimationGraph, BlendSpace};
use irradiance_runtime::approx::{relative_eq, relative_ne};
use irradiance_runtime::audio::AudioSource;
use irradiance_runtime::core::{Engine, Time};
use irradiance_runtime::ecs::{Entities, Facade, Transform};
use irradiance_runtime::math::{Quat, Vec2, Vec3};
use irradiance_runtime::physics::{Physics, RigidBody};
use irradiance_runtime::rendering::AnimatedMesh;

use crate::character::controller::State;
use crate::character::CharacterController;
use crate::objects::Button;

#[derive(Default)]
pub struct CharacterControllers;

impl CharacterControllers {
    const RELATIVE_OFFSET: f32 = 0.01;
    const RELATIVE_SNAP_DISTANCE: f32 = 0.2;

    pub fn update(&self, engine: &mut dyn Engine, time: Time, entities: &Entities) {
        for (entity, mut transform, mut animated_mesh, rigid_body, mut character_controller) in
            entities.components::<(
                &mut Transform,
                &mut AnimatedMesh,
                &RigidBody,
                &mut CharacterController,
            )>()
        {
            if animated_mesh.animation_graph.is_none() {
                animated_mesh.animation_graph =
                    Some(Self::create_animation_graph(&character_controller));
            }

            character_controller.state = match character_controller.state {
                State::Locomotion => self.locomotion(
                    engine,
                    time,
                    &entity,
                    &mut transform,
                    &rigid_body,
                    &mut character_controller,
                ),
                State::Hang => self.hanging(&character_controller),
                State::Climb => self.climbing(
                    engine,
                    &entity,
                    &mut transform,
                    &animated_mesh,
                    &mut character_controller,
                ),
                State::Interact => self.interact(
                    engine.physics_mut(),
                    entities,
                    &entity,
                    &transform,
                    &rigid_body,
                ),
            };
            character_controller.jumping = false;
            character_controller.hold = false;
        }
        unsafe {
            entities.commit();
        }
    }

    fn interact(
        &self,
        physics: &mut Physics,
        entities: &Entities,
        entity: &Facade,
        transform: &Transform,
        rigid_body: &RigidBody,
    ) -> State {
        let position = transform.position() + rigid_body.offset;
        let direction = -transform.orientation().rotate(Vec3::forward());
        if let Some(mut button) = physics
            .cast_ray(position, direction, 1.0, Some(entity))
            .and_then(|(id, _)| entities.get(id))
            .and_then(|entity| entity.get_mut::<Button>())
        {
            button.activated = true;
        }
        State::Locomotion
    }

    fn climbing(
        &self,
        engine: &mut dyn Engine,
        entity: &Facade,
        transform: &mut Transform,
        animated_mesh: &AnimatedMesh,
        character_controller: &mut CharacterController,
    ) -> State {
        if animated_mesh.is_finished() {
            let position = transform.position() + Vec3::down() * 0.1;
            transform.set_position(position);
            character_controller.grounded = true;
            State::Locomotion
        } else {
            let position = transform.position()
                + transform
                    .orientation()
                    .rotate(animated_mesh.root_offset() * transform.scale());
            transform.set_position(position);

            if animated_mesh.animation_graph.as_ref().unwrap().current != "climb" {
                self.play_one_of(
                    engine,
                    entity,
                    &[
                        "character/jump_01.wav",
                        "character/jump_02.wav",
                        "character/jump_03.wav",
                        "character/jump_04.wav",
                    ],
                    false,
                );
            }

            State::Climb
        }
    }

    fn hanging(&self, character_controller: &CharacterController) -> State {
        if character_controller.hold {
            State::Hang
        } else {
            State::Locomotion
        }
    }

    fn locomotion(
        &self,
        engine: &mut dyn Engine,
        time: Time,
        entity: &Facade,
        transform: &mut Transform,
        rigid_body: &RigidBody,
        character_controller: &mut CharacterController,
    ) -> State {
        let mut state = State::Locomotion;
        let mut position = transform.position();
        let mut orientation = transform.orientation();

        if let Some(snap_to_edge) = self.grab_edge(
            engine.physics(),
            &entity,
            &transform,
            &rigid_body,
            &character_controller,
        ) {
            position += snap_to_edge;
            character_controller.velocity = Vec3::zero();
            state = State::Hang;
        } else {
            let result = self.move_character(
                engine,
                time,
                &entity,
                &transform,
                &rigid_body,
                &character_controller,
            );
            position += result.translation + result.penetration_offset;
            orientation = result.orientation;
            character_controller.velocity = result.velocity;
            character_controller.grounded = result.grounded;
        }

        transform.set_position(position);
        transform.set_orientation(orientation);

        state
    }

    fn grab_edge(
        &self,
        physics: &Physics,
        entity: &Facade,
        transform: &Transform,
        rigid_body: &RigidBody,
        character_controller: &CharacterController,
    ) -> Option<Vec3> {
        if character_controller.hold {
            let position = transform.position() + rigid_body.offset;
            let orientation = transform.orientation();
            let forward = -Vec3::from(orientation);
            let dimensions = physics.dimensions(entity).unwrap_or_default();
            let offset = dimensions * Self::RELATIVE_OFFSET;
            if let Some(_) = physics.cast_shape(
                position,
                orientation,
                rigid_body.shape.clone(),
                forward,
                offset[1] * 5.0,
                Some(entity),
            ) {
                if let Some(_) = physics.cast_shape(
                    position + Vec3::up() * (dimensions[1] + offset[1] * 5.0),
                    orientation,
                    rigid_body.shape.clone(),
                    forward,
                    dimensions[0],
                    Some(entity),
                ) {
                    return None;
                }

                if let Some(hit) = physics.cast_shape(
                    position + Vec3::up() * dimensions[1] + forward * dimensions[0],
                    orientation,
                    rigid_body.shape.clone(),
                    Vec3::down(),
                    offset[1] * 1.5,
                    Some(entity),
                ) {
                    return Some(Vec3::down() * (hit.toi + 0.1));
                }
            }
        }
        return None;
    }

    fn move_character(
        &self,
        engine: &mut dyn Engine,
        time: Time,
        entity: &Facade,
        transform: &Transform,
        rigid_body: &RigidBody,
        character_controller: &CharacterController,
    ) -> MovementResult {
        let mut result = MovementResult::default();

        result.penetration_offset = engine.physics().penetration_offset(entity);

        let dimensions = engine.physics().dimensions(entity).unwrap_or_default();
        let position = transform.position() + rigid_body.offset + result.penetration_offset;
        let orientation = (transform.orientation()
            + self.rotation(transform.orientation(), character_controller))
        .normalize();

        result.orientation = orientation;
        result.velocity = self.desired_velocity(time, orientation, character_controller);

        let mut translation_remaining = time.delta() * result.velocity;

        let offset = dimensions * Self::RELATIVE_OFFSET;

        let grounded_at_starting_position =
            engine
                .physics()
                .is_grounded(entity, position, orientation, offset);

        if grounded_at_starting_position {
            self.snap_to_ground(
                engine.physics(),
                entity,
                position + result.translation,
                &rigid_body,
                dimensions,
                &mut result,
            );
        }

        let mut max_iters = 20;
        while translation_remaining.length() >= 1.0e-5 {
            if max_iters == 0 {
                break;
            } else {
                max_iters -= 1;
            }

            let translation_direction = translation_remaining.normalize();
            let translation_distance = translation_remaining.length();
            if let Some(hit) = engine.physics().cast_shape(
                position + result.translation,
                orientation,
                rigid_body.shape.clone(),
                translation_direction,
                translation_distance + offset[1],
                Some(entity),
            ) {
                let allowed_distance =
                    (hit.toi - (-hit.normal1.dot(translation_direction)) * offset[1]).max(0.0);
                let allowed_translation = translation_direction * allowed_distance;
                result.translation += allowed_translation;
                translation_remaining -= allowed_translation;
                translation_remaining -= hit.normal1.dot(translation_remaining) * hit.normal1;
            } else {
                result.translation += translation_remaining;
                translation_remaining = Vec3::zero();
            }
        }

        result.grounded = engine.physics().is_grounded(
            entity,
            position + result.translation,
            orientation,
            offset,
        );

        if grounded_at_starting_position {
            self.snap_to_ground(
                engine.physics(),
                entity,
                position + result.translation,
                &rigid_body,
                dimensions,
                &mut result,
            );
        }

        if character_controller.jumping {
            self.play_one_of(
                engine,
                entity,
                &[
                    "character/jump_01.wav",
                    "character/jump_02.wav",
                    "character/jump_03.wav",
                    "character/jump_04.wav",
                ],
                true,
            );
        }

        if !character_controller.grounded && result.grounded {
            self.play_one_of(
                engine,
                entity,
                &[
                    "character/run_step_01.wav",
                    "character/run_step_02.wav",
                    "character/run_step_03.wav",
                    "character/run_step_04.wav",
                ],
                false,
            );
        }

        if result.grounded
            && Vec2::new(result.velocity[0], result.velocity[2]).length()
                > character_controller.walk_coefficient * character_controller.run_velocity * 1.1
        {
            self.play_one_of(
                engine,
                entity,
                &[
                    "character/run_step_01.wav",
                    "character/run_step_02.wav",
                    "character/run_step_03.wav",
                    "character/run_step_04.wav",
                ],
                false,
            );
        } else if result.grounded
            && Vec2::new(result.velocity[0], result.velocity[2]).length() > 0.0
        {
            self.play_one_of(
                engine,
                entity,
                &[
                    "character/walk_step_01.wav",
                    "character/walk_step_02.wav",
                    "character/walk_step_03.wav",
                    "character/walk_step_04.wav",
                ],
                false,
            );
        }

        result
    }

    fn desired_velocity(
        &self,
        time: Time,
        orientation: Quat,
        character_controller: &CharacterController,
    ) -> Vec3 {
        let mut velocity = character_controller.velocity;
        if !character_controller.grounded {
            velocity[1] += time.delta() * -9.81;
        } else if character_controller.jumping {
            velocity[1] = character_controller.jump_velocity;
        } else {
            velocity = orientation.rotate(
                Vec3::new(
                    character_controller.direction[0],
                    0.0,
                    character_controller.direction[1],
                ) * character_controller.run_velocity,
            );
        }
        velocity
    }

    fn rotation(&self, orientation: Quat, character_controller: &CharacterController) -> Quat {
        0.5 * orientation
            * Quat::new(
                0.0,
                0.0,
                character_controller.turn_angle * character_controller.turn_velocity,
                0.0,
            )
    }

    fn snap_to_ground(
        &self,
        physics: &Physics,
        entity: &Facade,
        position: Vec3,
        rigid_body: &RigidBody,
        dimensions: Vec2,
        result: &mut MovementResult,
    ) {
        if result.translation.dot(Vec3::up()) < 1.0e-5 {
            let offset = dimensions * Self::RELATIVE_OFFSET;
            let snap_distance = dimensions[1] * Self::RELATIVE_SNAP_DISTANCE;
            if let Some(hit) = physics.cast_shape(
                position,
                result.orientation,
                rigid_body.shape.clone(),
                Vec3::down(),
                snap_distance + offset[1],
                Some(entity),
            ) {
                result.translation -= Vec3::up() * (hit.toi - offset[1]).max(0.0);
                result.grounded = true;
            }
        }
    }
}

#[derive(Default)]
struct MovementResult {
    grounded: bool,
    velocity: Vec3,
    penetration_offset: Vec3,
    orientation: Quat,
    translation: Vec3,
}

impl CharacterControllers {
    fn create_animation_graph(character_controller: &CharacterController) -> AnimationGraph {
        AnimationGraph::builder()
            .node("idle", false, character_controller.idle.clone())
            .node(
                "run",
                false,
                BlendSpace::builder()
                    .range(
                        Vec2::new(
                            -std::f32::consts::PI,
                            character_controller.run_velocity
                                * character_controller.walk_coefficient,
                        ),
                        Vec2::new(std::f32::consts::PI, character_controller.run_velocity),
                    )
                    .sample(
                        Vec2::new(-std::f32::consts::PI, character_controller.run_velocity),
                        character_controller.run_backward.clone(),
                    )
                    .sample(
                        Vec2::new(
                            -std::f32::consts::FRAC_PI_2,
                            character_controller.run_velocity,
                        ),
                        character_controller.run_strafe_right.clone(),
                    )
                    .sample(
                        Vec2::new(
                            -std::f32::consts::FRAC_PI_2 - 0.01,
                            character_controller.run_velocity,
                        ),
                        character_controller.run_strafe_right_backward.clone(),
                    )
                    .sample(
                        Vec2::new(0.0, character_controller.run_velocity),
                        character_controller.run_forward.clone(),
                    )
                    .sample(
                        Vec2::new(
                            std::f32::consts::FRAC_PI_2,
                            character_controller.run_velocity,
                        ),
                        character_controller.run_strafe_left.clone(),
                    )
                    .sample(
                        Vec2::new(
                            std::f32::consts::FRAC_PI_2 + 0.01,
                            character_controller.run_velocity,
                        ),
                        character_controller.run_strafe_left_backward.clone(),
                    )
                    .sample(
                        Vec2::new(std::f32::consts::PI, character_controller.run_velocity),
                        character_controller.run_backward.clone(),
                    )
                    .sample(
                        Vec2::new(
                            -std::f32::consts::PI,
                            character_controller.run_velocity
                                * character_controller.walk_coefficient,
                        ),
                        character_controller.walk_backward.clone(),
                    )
                    .sample(
                        Vec2::new(
                            -std::f32::consts::FRAC_PI_2,
                            character_controller.run_velocity
                                * character_controller.walk_coefficient,
                        ),
                        character_controller.walk_strafe_right.clone(),
                    )
                    .sample(
                        Vec2::new(
                            -std::f32::consts::FRAC_PI_2 - 0.01,
                            character_controller.run_velocity
                                * character_controller.walk_coefficient,
                        ),
                        character_controller.walk_strafe_right_backward.clone(),
                    )
                    .sample(
                        Vec2::new(
                            0.0,
                            character_controller.run_velocity
                                * character_controller.walk_coefficient,
                        ),
                        character_controller.walk_forward.clone(),
                    )
                    .sample(
                        Vec2::new(
                            std::f32::consts::FRAC_PI_2,
                            character_controller.run_velocity
                                * character_controller.walk_coefficient,
                        ),
                        character_controller.walk_strafe_left.clone(),
                    )
                    .sample(
                        Vec2::new(
                            std::f32::consts::FRAC_PI_2 + 0.01,
                            character_controller.run_velocity
                                * character_controller.walk_coefficient,
                        ),
                        character_controller.walk_strafe_left_backward.clone(),
                    )
                    .sample(
                        Vec2::new(
                            std::f32::consts::PI,
                            character_controller.run_velocity
                                * character_controller.walk_coefficient,
                        ),
                        character_controller.walk_backward.clone(),
                    )
                    .value_provider(|_, entity| {
                        let character_controller = entity.get::<CharacterController>().unwrap();
                        let velocity = Vec3::new(
                            character_controller.direction[0],
                            0.0,
                            character_controller.direction[1],
                        ) * character_controller.run_velocity;
                        let direction = velocity.normalize();
                        let forward = Vec3::new(0.0, 0.0, 1.0);
                        let up = Vec3::new(0.0, 1.0, 0.0);
                        let angle =
                            f32::atan2(forward.cross(direction).dot(up), forward.dot(direction));

                        Vec2::new(angle, velocity.length())
                    })
                    .build(),
            )
            .node("jump", false, character_controller.jump.clone())
            .node(
                "jump_forward",
                false,
                BlendSpace::builder()
                    .range(
                        Vec2::new(-std::f32::consts::PI, 0.0),
                        Vec2::new(std::f32::consts::PI, 0.0),
                    )
                    .sample(
                        Vec2::new(-std::f32::consts::PI, 0.0),
                        character_controller.jump_backward.clone(),
                    )
                    .sample(
                        Vec2::new(-std::f32::consts::FRAC_PI_2, 0.0),
                        character_controller.jump_strafe_right.clone(),
                    )
                    .sample(
                        Vec2::new(-std::f32::consts::FRAC_PI_2 - 0.01, 0.0),
                        character_controller.jump_strafe_right_backward.clone(),
                    )
                    .sample(
                        Vec2::new(0.0, 0.0),
                        character_controller.jump_forward.clone(),
                    )
                    .sample(
                        Vec2::new(std::f32::consts::FRAC_PI_2, 0.0),
                        character_controller.jump_strafe_left.clone(),
                    )
                    .sample(
                        Vec2::new(std::f32::consts::FRAC_PI_2 + 0.01, 0.0),
                        character_controller.jump_strafe_left_backward.clone(),
                    )
                    .sample(
                        Vec2::new(std::f32::consts::PI, 0.0),
                        character_controller.jump_backward.clone(),
                    )
                    .value_provider(|_, entity| {
                        let character_controller = entity.get::<CharacterController>().unwrap();
                        let velocity = Vec3::new(
                            character_controller.direction[0],
                            0.0,
                            character_controller.direction[1],
                        ) * character_controller.run_velocity;
                        let direction = velocity.normalize();
                        let forward = Vec3::new(0.0, 0.0, 1.0);
                        let up = Vec3::new(0.0, 1.0, 0.0);
                        let angle =
                            f32::atan2(forward.cross(direction).dot(up), forward.dot(direction));

                        Vec2::new(angle, 0.0)
                    })
                    .build(),
            )
            .node("fall", false, character_controller.fall.clone())
            .node("hang", false, character_controller.hang.clone())
            .node("climb", true, character_controller.climb.clone())
            .transition("idle", "run", |_, entity| {
                let velocity = entity.get::<CharacterController>().unwrap().velocity;
                relative_ne!(Vec2::new(velocity[0], velocity[2]), Vec2::zero())
            })
            .transition("idle", "jump", |_, entity| {
                entity.get::<CharacterController>().unwrap().velocity[1] > 0.0
            })
            .transition("idle", "fall", |_, entity| {
                let character_controller = entity.get::<CharacterController>().unwrap();
                !character_controller.grounded && character_controller.velocity[1] < 0.0
            })
            .transition("run", "idle", |_, entity| {
                let velocity = entity.get::<CharacterController>().unwrap().velocity;
                relative_eq!(Vec2::new(velocity[0], velocity[2]), Vec2::zero())
            })
            .transition("run", "jump_forward", |_, entity| {
                entity.get::<CharacterController>().unwrap().velocity[1] > 0.0
            })
            .transition("run", "fall", |_, entity| {
                let character_controller = entity.get::<CharacterController>().unwrap();
                !character_controller.grounded && character_controller.velocity[1] < 0.0
            })
            .transition("jump", "idle", |_, entity| {
                entity.get::<CharacterController>().unwrap().grounded
            })
            .transition("jump", "hang", |_, entity| {
                entity.get::<CharacterController>().unwrap().state == State::Hang
            })
            .transition("jump_forward", "run", |_, entity| {
                let character_controller = entity.get::<CharacterController>().unwrap();
                relative_ne!(character_controller.direction, Vec2::zero())
                    && character_controller.grounded
            })
            .transition("jump_forward", "fall", |_, entity| {
                entity.get::<AnimatedMesh>().unwrap().is_finished()
            })
            .transition("jump_forward", "idle", |_, entity| {
                entity.get::<CharacterController>().unwrap().grounded
            })
            .transition("jump_forward", "hang", |_, entity| {
                entity.get::<CharacterController>().unwrap().state == State::Hang
            })
            .transition("fall", "run", |_, entity| {
                let character_controller = entity.get::<CharacterController>().unwrap();
                relative_ne!(character_controller.direction, Vec2::zero())
                    && character_controller.grounded
            })
            .transition("fall", "idle", |_, entity| {
                entity.get::<CharacterController>().unwrap().grounded
            })
            .transition("fall", "hang", |_, entity| {
                entity.get::<CharacterController>().unwrap().state == State::Hang
            })
            .transition("fall", "climb", |_, entity| {
                entity.get::<CharacterController>().unwrap().state == State::Climb
            })
            .transition("hang", "climb", |_, entity| {
                entity.get::<CharacterController>().unwrap().state == State::Climb
            })
            .transition("hang", "fall", |_, entity| {
                entity.get::<CharacterController>().unwrap().state == State::Locomotion
            })
            .transition("climb", "idle", |_, entity| {
                entity.get::<CharacterController>().unwrap().state == State::Locomotion
            })
            .build()
    }

    fn play_one_of(&self, engine: &mut dyn Engine, entity: &Facade, sounds: &[&str], queue: bool) {
        unsafe {
            static mut INDEX: usize = 0;

            let graphics_context = engine.graphics_context().clone();
            let assets = engine.assets().clone();
            let audio = engine.audio_mut();

            if queue || !audio.is_playing(entity) {
                let sound = assets
                    .read::<AudioSource, _>(graphics_context, sounds[INDEX])
                    .unwrap();
                audio.play_at(entity, sound.unwrap().clone());
                INDEX = (INDEX + 1) % sounds.len();
            }
        }
    }
}
