use std::sync::Arc;

use serde::Serialize;

use irradiance_editor::core::EditorEngine;
use irradiance_editor::ecs::EditorComponentExt;
use irradiance_editor::egui::Ui;
use irradiance_editor::gui::widgets::{AssetSlot, DragValue, Widget};
use irradiance_editor::gui::{Action, EditorContext, Icon, Icons};
use irradiance_runtime::animation::Animation;
use irradiance_runtime::approx::relative_eq;
use irradiance_runtime::asset::AssetRef;
use irradiance_runtime::core::Engine;
use irradiance_runtime::ecs::Entities;
use irradiance_runtime::math::{Vec2, Vec3};
use irradiance_runtime::Component;

/// Character controller.
#[derive(Clone, Default, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct CharacterController {
    /// The current character state.
    #[serde(skip_serializing, skip_deserializing)]
    pub state: State,
    /// Indicates if the character should hold an edge.
    #[serde(skip_serializing, skip_deserializing)]
    pub hold: bool,
    /// Indicates if the character should jump.
    #[serde(skip_serializing, skip_deserializing)]
    pub jumping: bool,
    #[serde(skip_serializing, skip_deserializing)]
    /// Indicates if the character is on the ground.
    pub grounded: bool,
    /// Current turn angle.
    #[serde(skip_serializing, skip_deserializing)]
    pub turn_angle: f32,
    /// Current walking direction.
    #[serde(skip_serializing, skip_deserializing)]
    pub direction: Vec2,
    /// Current angular velocity.
    #[serde(skip_serializing, skip_deserializing)]
    pub angular_velocity: f32,
    /// Current velocity.
    #[serde(skip_serializing, skip_deserializing)]
    pub velocity: Vec3,
    /// Climb animation.
    pub climb: AssetRef<Animation>,
    /// Hang animation.
    pub hang: AssetRef<Animation>,
    /// Fall animation.
    pub fall: AssetRef<Animation>,
    /// Jump strafe left backward animation.
    pub jump_strafe_right_backward: AssetRef<Animation>,
    /// Jump strafe left animation.
    pub jump_strafe_right: AssetRef<Animation>,
    /// Jump strafe left backward animation.
    pub jump_strafe_left_backward: AssetRef<Animation>,
    /// Jump strafe left animation.
    pub jump_strafe_left: AssetRef<Animation>,
    /// Jump backward animation.
    pub jump_backward: AssetRef<Animation>,
    /// Jump forward animation.
    pub jump_forward: AssetRef<Animation>,
    /// Jump animation.
    pub jump: AssetRef<Animation>,
    /// Walk strafe right backward animation.
    pub walk_strafe_right_backward: AssetRef<Animation>,
    /// Walk strafe right animation.
    pub walk_strafe_right: AssetRef<Animation>,
    /// Walk strafe left backward animation.
    pub walk_strafe_left_backward: AssetRef<Animation>,
    /// Walk strafe left animation.
    pub walk_strafe_left: AssetRef<Animation>,
    /// Walk backward animation.
    pub walk_backward: AssetRef<Animation>,
    /// Walk forward animation.
    pub walk_forward: AssetRef<Animation>,
    /// Run strafe right backward animation.
    pub run_strafe_right_backward: AssetRef<Animation>,
    /// Run strafe right animation.
    pub run_strafe_right: AssetRef<Animation>,
    /// Run strafe left backward animation.
    pub run_strafe_left_backward: AssetRef<Animation>,
    /// Run strafe left animation.
    pub run_strafe_left: AssetRef<Animation>,
    /// Run backward animation.
    pub run_backward: AssetRef<Animation>,
    /// Run forward animation.
    pub run_forward: AssetRef<Animation>,
    /// Idle animation.
    pub idle: AssetRef<Animation>,
    /// Jump velocity.
    pub jump_velocity: f32,
    /// Turn velocity.
    pub turn_velocity: f32,
    /// Walk coefficient.
    pub walk_coefficient: f32,
    /// Run velocity.
    pub run_velocity: f32,
}

impl CharacterController {
    /// Lets the character walk into the given direction.
    pub fn walk(&mut self, direction: Vec2, scale: f32) {
        self.direction = direction * scale;
    }

    /// Turns the character around the given delta angle.
    pub fn turn(&mut self, delta_angle: f32) {
        self.turn_angle = delta_angle;
    }

    /// Lets the character jump.
    pub fn jump(&mut self) {
        if self.grounded {
            self.jumping = true;
        }
    }

    /// Lets the character hold an edge.
    pub fn hold(&mut self) {
        self.hold = true;
    }

    /// Lets the character climb if hanging on an edge.
    pub fn climb(&mut self) {
        if self.state == State::Hang {
            self.state = State::Climb;
        }
    }

    /// Lets the character interact.
    pub fn interact(&mut self) {
        if relative_eq!(self.direction, Vec2::zero()) && self.grounded {
            self.state = State::Interact;
        }
    }
}

/// Character states.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Default)]
pub enum State {
    /// Locomotion state.
    #[default]
    Locomotion,
    /// Hanging state.
    Hang,
    /// Climbing state.
    Climb,
    /// Interaction state.
    Interact,
}

struct CharacterControllerInspector {
    climb: AssetSlot<Animation>,
    hang: AssetSlot<Animation>,
    fall: AssetSlot<Animation>,
    jump_strafe_right_backward: AssetSlot<Animation>,
    jump_strafe_right: AssetSlot<Animation>,
    jump_strafe_left_backward: AssetSlot<Animation>,
    jump_strafe_left: AssetSlot<Animation>,
    jump_backward: AssetSlot<Animation>,
    jump_forward: AssetSlot<Animation>,
    jump: AssetSlot<Animation>,
    walk_strafe_right_backward: AssetSlot<Animation>,
    walk_strafe_right: AssetSlot<Animation>,
    walk_strafe_left_backward: AssetSlot<Animation>,
    walk_strafe_left: AssetSlot<Animation>,
    walk_backward: AssetSlot<Animation>,
    walk_forward: AssetSlot<Animation>,
    run_strafe_right_backward: AssetSlot<Animation>,
    run_strafe_right: AssetSlot<Animation>,
    run_strafe_left_backward: AssetSlot<Animation>,
    run_strafe_left: AssetSlot<Animation>,
    run_backward: AssetSlot<Animation>,
    run_forward: AssetSlot<Animation>,
    idle: AssetSlot<Animation>,
    jump_velocity: DragValue<f32>,
    turn_velocity: DragValue<f32>,
    walk_coefficient: DragValue<f32>,
    run_velocity: DragValue<f32>,
    entities: Arc<Entities>,
    entity: String,
}

impl CharacterControllerInspector {
    fn new(entity: String, entities: Arc<Entities>, engine: &EditorEngine) -> Self {
        let mut run_velocity = DragValue::default();
        let mut walk_coefficient = DragValue::default();
        let mut turn_velocity = DragValue::default();
        let mut jump_velocity = DragValue::default();
        let mut idle = AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut run_forward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut run_backward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut run_strafe_left =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut run_strafe_left_backward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut run_strafe_right =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut run_strafe_right_backward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut walk_forward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut walk_backward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut walk_strafe_left =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut walk_strafe_left_backward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut walk_strafe_right =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut walk_strafe_right_backward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut jump = AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut jump_forward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut jump_backward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut jump_strafe_left =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut jump_strafe_left_backward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut jump_strafe_right =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut jump_strafe_right_backward =
            AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut fall = AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut hang = AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut climb = AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());

        let character_controller = entities
            .get(&entity)
            .expect("entity")
            .get::<CharacterController>()
            .expect("character controller component");
        run_velocity.set(character_controller.run_velocity);
        walk_coefficient.set(character_controller.walk_coefficient);
        turn_velocity.set(character_controller.turn_velocity);
        jump_velocity.set(character_controller.jump_velocity);
        idle.set(character_controller.idle.clone());
        run_forward.set(character_controller.run_forward.clone());
        run_backward.set(character_controller.run_backward.clone());
        run_strafe_left.set(character_controller.run_strafe_left.clone());
        run_strafe_left_backward.set(character_controller.run_strafe_left_backward.clone());
        run_strafe_right.set(character_controller.run_strafe_right.clone());
        run_strafe_right_backward.set(character_controller.run_strafe_right_backward.clone());
        walk_forward.set(character_controller.walk_forward.clone());
        walk_backward.set(character_controller.walk_backward.clone());
        walk_strafe_left.set(character_controller.walk_strafe_left.clone());
        walk_strafe_left_backward.set(character_controller.walk_strafe_left_backward.clone());
        walk_strafe_right.set(character_controller.walk_strafe_right.clone());
        walk_strafe_right_backward.set(character_controller.walk_strafe_right_backward.clone());
        jump.set(character_controller.jump.clone());
        jump_forward.set(character_controller.jump_forward.clone());
        jump_backward.set(character_controller.jump_backward.clone());
        jump_strafe_left.set(character_controller.jump_strafe_left.clone());
        jump_strafe_left_backward.set(character_controller.jump_strafe_left_backward.clone());
        jump_strafe_right.set(character_controller.jump_strafe_right.clone());
        jump_strafe_right_backward.set(character_controller.jump_strafe_right_backward.clone());
        fall.set(character_controller.fall.clone());
        hang.set(character_controller.hang.clone());
        climb.set(character_controller.climb.clone());

        Self {
            climb,
            hang,
            fall,
            jump_strafe_right_backward,
            jump_strafe_right,
            jump_strafe_left_backward,
            jump_strafe_left,
            jump_backward,
            jump_forward,
            jump,
            walk_strafe_right_backward,
            walk_strafe_right,
            walk_strafe_left_backward,
            walk_strafe_left,
            walk_backward,
            walk_forward,
            run_strafe_right_backward,
            run_strafe_right,
            run_strafe_left_backward,
            run_strafe_left,
            run_backward,
            run_forward,
            idle,
            jump_velocity,
            turn_velocity,
            walk_coefficient,
            run_velocity,
            entities,
            entity,
        }
    }
}

impl Widget for CharacterControllerInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.idle.handle_messages(context, engine);
        self.run_forward.handle_messages(context, engine);
        self.run_backward.handle_messages(context, engine);
        self.run_strafe_left.handle_messages(context, engine);
        self.run_strafe_left_backward
            .handle_messages(context, engine);
        self.run_strafe_right.handle_messages(context, engine);
        self.run_strafe_right_backward
            .handle_messages(context, engine);
        self.walk_forward.handle_messages(context, engine);
        self.walk_backward.handle_messages(context, engine);
        self.walk_strafe_left.handle_messages(context, engine);
        self.walk_strafe_left_backward
            .handle_messages(context, engine);
        self.walk_strafe_right.handle_messages(context, engine);
        self.walk_strafe_right_backward
            .handle_messages(context, engine);
        self.jump.handle_messages(context, engine);
        self.jump_forward.handle_messages(context, engine);
        self.jump_backward.handle_messages(context, engine);
        self.jump_strafe_left_backward
            .handle_messages(context, engine);
        self.jump_strafe_right.handle_messages(context, engine);
        self.jump_strafe_right_backward
            .handle_messages(context, engine);
        self.fall.handle_messages(context, engine);
        self.hang.handle_messages(context, engine);
        self.climb.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Velocity");
        ui.separator();
        ui.end_row();

        ui.label("    Run");
        self.run_velocity.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Walk Coefficient");
        self.walk_coefficient.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Turn");
        self.turn_velocity.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Jump");
        self.jump_velocity.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Animation");
        ui.separator();
        ui.end_row();

        ui.label("    Idle");
        self.idle.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Run Forward");
        self.run_forward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Run Backward");
        self.run_backward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Run Strafe Left");
        self.run_strafe_left.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Run Strafe Left Backward");
        self.run_strafe_left_backward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Run Strafe Right");
        self.run_strafe_right.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Run Strafe Right Backward");
        self.run_strafe_right_backward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Walk Forward");
        self.walk_forward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Walk Backward");
        self.walk_backward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Walk Strafe Left");
        self.walk_strafe_left.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Walk Strafe Left Backward");
        self.walk_strafe_left_backward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Walk Strafe Right");
        self.walk_strafe_right.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Walk Strafe Right Backward");
        self.walk_strafe_right_backward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Jump");
        self.jump.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Jump Forward");
        self.jump_forward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Jump Backward");
        self.jump_backward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Jump Strafe Left");
        self.jump_strafe_left.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Jump Strafe Left Backward");
        self.jump_strafe_left_backward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Jump Strafe Right");
        self.jump_strafe_right.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Jump Strafe Right Backward");
        self.jump_strafe_right_backward.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Fall");
        self.fall.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Hang");
        self.hang.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Climb");
        self.climb.ui(ui, context, engine);
        ui.end_row();

        if self.run_velocity.changed() {
            context.actions_mut().push(
                engine,
                ChangeRunVelocity::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.run_velocity.get(),
                ),
            );
        }
        if self.walk_coefficient.changed() {
            context.actions_mut().push(
                engine,
                ChangeWalkCoefficient::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.walk_coefficient.get(),
                ),
            );
        }
        if self.turn_velocity.changed() {
            context.actions_mut().push(
                engine,
                ChangeTurnVelocity::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.turn_velocity.get(),
                ),
            );
        }
        if self.jump_velocity.changed() {
            context.actions_mut().push(
                engine,
                ChangeJumpVelocity::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.jump_velocity.get(),
                ),
            );
        }

        if self.idle.changed() {
            context.actions_mut().push(
                engine,
                ChangeIdleAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.idle.get(),
                ),
            );
        }
        if self.run_forward.changed() {
            context.actions_mut().push(
                engine,
                ChangeRunForwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.run_forward.get(),
                ),
            );
        }
        if self.run_backward.changed() {
            context.actions_mut().push(
                engine,
                ChangeRunBackwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.run_backward.get(),
                ),
            );
        }
        if self.run_strafe_left.changed() {
            context.actions_mut().push(
                engine,
                ChangeRunStrafeLeftAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.run_strafe_left.get(),
                ),
            );
        }
        if self.run_strafe_left_backward.changed() {
            context.actions_mut().push(
                engine,
                ChangeRunStrafeLeftBackwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.run_strafe_left_backward.get(),
                ),
            );
        }
        if self.run_strafe_right.changed() {
            context.actions_mut().push(
                engine,
                ChangeRunStrafeRightAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.run_strafe_right.get(),
                ),
            );
        }
        if self.run_strafe_right_backward.changed() {
            context.actions_mut().push(
                engine,
                ChangeRunStrafeRightBackwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.run_strafe_right_backward.get(),
                ),
            );
        }
        if self.walk_forward.changed() {
            context.actions_mut().push(
                engine,
                ChangeWalkForwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.walk_forward.get(),
                ),
            );
        }
        if self.walk_backward.changed() {
            context.actions_mut().push(
                engine,
                ChangeWalkBackwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.walk_backward.get(),
                ),
            );
        }
        if self.walk_strafe_left.changed() {
            context.actions_mut().push(
                engine,
                ChangeWalkStrafeLeftAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.walk_strafe_left.get(),
                ),
            );
        }
        if self.walk_strafe_left_backward.changed() {
            context.actions_mut().push(
                engine,
                ChangeWalkStrafeLeftBackwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.walk_strafe_left_backward.get(),
                ),
            );
        }
        if self.walk_strafe_right.changed() {
            context.actions_mut().push(
                engine,
                ChangeWalkStrafeRightAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.walk_strafe_right.get(),
                ),
            );
        }
        if self.walk_strafe_right_backward.changed() {
            context.actions_mut().push(
                engine,
                ChangeWalkStrafeRightBackwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.walk_strafe_right_backward.get(),
                ),
            );
        }
        if self.jump.changed() {
            context.actions_mut().push(
                engine,
                ChangeJumpAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.jump.get(),
                ),
            );
        }
        if self.jump_forward.changed() {
            context.actions_mut().push(
                engine,
                ChangeJumpForwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.jump_forward.get(),
                ),
            );
        }
        if self.jump_backward.changed() {
            context.actions_mut().push(
                engine,
                ChangeJumpBackwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.jump_backward.get(),
                ),
            );
        }
        if self.jump_strafe_left.changed() {
            context.actions_mut().push(
                engine,
                ChangeJumpStrafeLeftAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.jump_strafe_left.get(),
                ),
            );
        }
        if self.jump_strafe_left_backward.changed() {
            context.actions_mut().push(
                engine,
                ChangeJumpStrafeLeftBackwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.jump_strafe_left_backward.get(),
                ),
            );
        }
        if self.jump_strafe_right.changed() {
            context.actions_mut().push(
                engine,
                ChangeJumpStrafeRightAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.jump_strafe_right.get(),
                ),
            );
        }
        if self.jump_strafe_right_backward.changed() {
            context.actions_mut().push(
                engine,
                ChangeJumpStrafeRightBackwardAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.jump_strafe_right_backward.get(),
                ),
            );
        }
        if self.fall.changed() {
            context.actions_mut().push(
                engine,
                ChangeFallAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.fall.get(),
                ),
            );
        }
        if self.hang.changed() {
            context.actions_mut().push(
                engine,
                ChangeHangAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.hang.get(),
                ),
            );
        }
        if self.climb.changed() {
            context.actions_mut().push(
                engine,
                ChangeClimbAnimation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.climb.get(),
                ),
            );
        }
    }
}

impl EditorComponentExt for CharacterController {
    fn display_name() -> &'static str {
        "Character Controller"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::person())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(CharacterControllerInspector::new(
            entity, entities, engine,
        )))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }
}

struct ChangeRunVelocity {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeRunVelocity {
    fn new(entity: String, entities: Arc<Entities>, new: f32) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .run_velocity,
            entities,
            entity,
        }
    }

    fn replace(&self, forward_velocity: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .run_velocity = forward_velocity;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeRunVelocity {
    fn description(&self) -> String {
        "Change Run Velocity".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeWalkCoefficient {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeWalkCoefficient {
    fn new(entity: String, entities: Arc<Entities>, new: f32) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .walk_coefficient,
            entities,
            entity,
        }
    }

    fn replace(&self, backward_velocity: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .walk_coefficient = backward_velocity;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeWalkCoefficient {
    fn description(&self) -> String {
        "Change Walk Coefficient".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeTurnVelocity {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeTurnVelocity {
    fn new(entity: String, entities: Arc<Entities>, new: f32) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .turn_velocity,
            entities,
            entity,
        }
    }

    fn replace(&self, value: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .turn_velocity = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeTurnVelocity {
    fn description(&self) -> String {
        "Change Turn Velocity".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeJumpVelocity {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeJumpVelocity {
    fn new(entity: String, entities: Arc<Entities>, new: f32) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .jump_velocity,
            entities,
            entity,
        }
    }

    fn replace(&self, value: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .jump_velocity = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeJumpVelocity {
    fn description(&self) -> String {
        "Change Jump Velocity".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeIdleAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeIdleAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .idle
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, idle: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .idle = idle;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeIdleAnimation {
    fn description(&self) -> String {
        "Change Idle Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeRunForwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeRunForwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .run_forward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, run_forward: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .run_forward = run_forward;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeRunForwardAnimation {
    fn description(&self) -> String {
        "Change Run Forward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeRunBackwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeRunBackwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .run_backward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .run_backward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeRunBackwardAnimation {
    fn description(&self) -> String {
        "Change Run Backward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeRunStrafeLeftAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeRunStrafeLeftAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .run_strafe_left
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .run_strafe_left = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeRunStrafeLeftAnimation {
    fn description(&self) -> String {
        "Change Run Strafe Left Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeRunStrafeLeftBackwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeRunStrafeLeftBackwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .run_strafe_left_backward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .run_strafe_left_backward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeRunStrafeLeftBackwardAnimation {
    fn description(&self) -> String {
        "Change Run Strafe Left Backward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeRunStrafeRightAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeRunStrafeRightAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .run_strafe_right
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .run_strafe_right = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeRunStrafeRightAnimation {
    fn description(&self) -> String {
        "Change Run Strafe Right Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeRunStrafeRightBackwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeRunStrafeRightBackwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .run_strafe_right_backward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .run_strafe_right_backward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeRunStrafeRightBackwardAnimation {
    fn description(&self) -> String {
        "Change Run Strafe Right Backward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeWalkForwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeWalkForwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .walk_forward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .walk_forward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeWalkForwardAnimation {
    fn description(&self) -> String {
        "Change Walk Forward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeWalkBackwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeWalkBackwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .walk_backward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .walk_backward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeWalkBackwardAnimation {
    fn description(&self) -> String {
        "Change Walk Backward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeWalkStrafeLeftAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeWalkStrafeLeftAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .walk_strafe_left
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .walk_strafe_left = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeWalkStrafeLeftAnimation {
    fn description(&self) -> String {
        "Change Walk Strafe Left Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeWalkStrafeLeftBackwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeWalkStrafeLeftBackwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .walk_strafe_left_backward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .walk_strafe_left_backward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeWalkStrafeLeftBackwardAnimation {
    fn description(&self) -> String {
        "Change Walk Strafe Left Backward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeWalkStrafeRightAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeWalkStrafeRightAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .walk_strafe_right
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .walk_strafe_right = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeWalkStrafeRightAnimation {
    fn description(&self) -> String {
        "Change Walk Strafe Right Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeWalkStrafeRightBackwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeWalkStrafeRightBackwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .walk_strafe_right_backward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .walk_strafe_right_backward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeWalkStrafeRightBackwardAnimation {
    fn description(&self) -> String {
        "Change Walk Strafe Right Backward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeJumpAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeJumpAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .jump
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .jump = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeJumpAnimation {
    fn description(&self) -> String {
        "Change Jump Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeJumpForwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeJumpForwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .jump_forward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .jump_forward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeJumpForwardAnimation {
    fn description(&self) -> String {
        "Change Jump Forward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeJumpBackwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeJumpBackwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .jump_backward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .jump_backward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeJumpBackwardAnimation {
    fn description(&self) -> String {
        "Change Jump Backward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeJumpStrafeLeftAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeJumpStrafeLeftAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .jump_strafe_left
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .jump_strafe_left = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeJumpStrafeLeftAnimation {
    fn description(&self) -> String {
        "Change Jump Strafe Left Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeJumpStrafeLeftBackwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeJumpStrafeLeftBackwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .jump_strafe_left_backward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .jump_strafe_left_backward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeJumpStrafeLeftBackwardAnimation {
    fn description(&self) -> String {
        "Change Jump Strafe Left Backward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeJumpStrafeRightAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeJumpStrafeRightAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .jump_strafe_right
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .jump_strafe_right = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeJumpStrafeRightAnimation {
    fn description(&self) -> String {
        "Change Jump Strafe Right Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeJumpStrafeRightBackwardAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeJumpStrafeRightBackwardAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .jump_strafe_right_backward
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .jump_strafe_right_backward = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeJumpStrafeRightBackwardAnimation {
    fn description(&self) -> String {
        "Change Jump Strafe Right Backward Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeFallAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeFallAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .fall
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .fall = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeFallAnimation {
    fn description(&self) -> String {
        "Change Fall Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeHangAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeHangAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .hang
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .hang = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeHangAnimation {
    fn description(&self) -> String {
        "Change Hang Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeClimbAnimation {
    new: AssetRef<Animation>,
    old: AssetRef<Animation>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeClimbAnimation {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Animation>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<CharacterController>()
                .expect("character controller component")
                .climb
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, value: AssetRef<Animation>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<CharacterController>()
            .expect("character controller component")
            .climb = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeClimbAnimation {
    fn description(&self) -> String {
        "Change Climb Animation".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}
