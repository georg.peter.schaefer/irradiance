//! All game screens.
pub use menu::Menu;
pub use pause::Pause;
pub use play::Play;
pub use win::Win;

mod menu;
mod pause;
mod play;
mod win;
