use std::any::Any;

use irradiance_runtime::core::{Engine, Frame, Mode, Screen, ScreenExt};
use irradiance_runtime::input::Mouse;
use irradiance_runtime::math::Vec2;

use crate::ui::{Align, Button, Image, Ui, Widget};

/// Pause screen.
pub struct Pause {
    exit: Button,
    play: Button,
    title: Image,
    ui: Ui,
}

impl Screen for Pause {
    fn name(&self) -> &'static str {
        "pause"
    }

    fn mode(&self) -> Mode {
        Mode::Modal
    }

    fn on_enter(&mut self, engine: &mut dyn Engine) {
        engine
            .input_mut()
            .get_mut::<Mouse>("Mouse")
            .set_continuous(false);
    }

    fn update(&mut self, engine: &mut dyn Engine, frame: &mut Frame) {
        self.play.update(engine, &frame.swapchain_image());
        self.exit.update(engine, &frame.swapchain_image());

        if self.play.clicked() || engine.input().button("Keyboard/Escape").pressed() {
            engine.screens().pop();
        }
        if self.exit.clicked() {
            engine.screens().pop();
            engine.screens().pop();
        }
    }

    fn draw(&mut self, _engine: &mut dyn Engine, frame: &mut Frame) {
        let swapchain_image = frame.swapchain_image().clone();
        self.ui
            .draw(
                true,
                frame.builder_mut(),
                swapchain_image,
                &[&self.title, &self.play, &self.exit],
            )
            .expect("valid frame");
    }

    fn on_leave(&mut self, engine: &mut dyn Engine) {
        engine
            .input_mut()
            .get_mut::<Mouse>("Mouse")
            .set_continuous(true);
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

impl ScreenExt for Pause {
    fn new(engine: &dyn Engine) -> Self {
        Self {
            ui: Ui::new(engine.graphics_context().clone()).expect("ui"),
            title: Image::new(
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/pause.png")
                    .unwrap(),
                Vec2::new(0.0, 50.0),
                Align::CenterX,
            ),
            play: Button::new(
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/play.png")
                    .unwrap(),
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/play_highlight.png")
                    .unwrap(),
                Vec2::zero(),
                Align::Center,
            ),
            exit: Button::new(
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/exit.png")
                    .unwrap(),
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/exit_highlight.png")
                    .unwrap(),
                Vec2::new(0.0, 200.0),
                Align::Center,
            ),
        }
    }
}
