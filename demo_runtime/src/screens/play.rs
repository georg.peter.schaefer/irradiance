use irradiance_runtime::audio::AudioSource;
use std::any::Any;
use std::sync::Arc;

use irradiance_runtime::core::{Engine, Frame, Mode, Screen, ScreenExt};
use irradiance_runtime::ecs::{Entities, Springs, Transform};
use irradiance_runtime::input::Mouse;
use irradiance_runtime::physics::Physics;
use irradiance_runtime::rendering::Camera;
use irradiance_runtime::rodio::{Sink, Source};

use crate::character::CharacterControllers;
use crate::objects::{Buttons, Orbs, Risers};
use crate::player::{Player, Players};

/// Demo play screen.
pub struct Play {
    music: Option<Sink>,
    orbs: Orbs,
    buttons: Buttons,
    risers: Risers,
    springs: Springs,
    character_controllers: CharacterControllers,
    players: Players,
    entities: Arc<Entities>,
}

impl Play {
    fn load_scene(&mut self, engine: &dyn Engine) {
        if let Some(scene) = engine.scenes().get("play") {
            self.entities = Entities::from_scene(scene);
        }
        if let Some(camera) = self.entities.get("camera") {
            camera.activate::<Camera>();
        }
    }
}

impl Screen for Play {
    fn name(&self) -> &'static str {
        "play"
    }

    fn mode(&self) -> Mode {
        Mode::Opaque
    }

    fn entities(&self) -> Option<Arc<Entities>> {
        Some(self.entities.clone())
    }

    fn on_enter(&mut self, engine: &mut dyn Engine) {
        *engine.physics_mut() =
            Physics::new(engine.graphics_context().clone(), engine.assets().clone());
        engine
            .input_mut()
            .get_mut::<Mouse>("Mouse")
            .set_continuous(true);
        self.load_scene(engine);

        let music = engine
            .assets()
            .read::<AudioSource, _>(engine.graphics_context().clone(), "temple/music.wav")
            .unwrap();
        self.music = Some(
            engine
                .audio_mut()
                .play(music.unwrap().clone().repeat_infinite()),
        );
    }

    fn update(&mut self, engine: &mut dyn Engine, frame: &mut Frame) {
        if engine.input().button("Keyboard/Escape").pressed() {
            engine.screens().push("pause");
        }

        self.players.update(engine, &self.entities);
        self.character_controllers
            .update(engine, frame.time(), &self.entities);
        self.springs.update(engine, frame.time(), &self.entities);
        self.risers.update(engine, frame.time(), &self.entities);
        self.buttons.update(frame.time(), &self.entities);
        self.orbs.update(engine, frame.time(), &self.entities);
        engine.physics_mut().update(&self.entities);
        engine
            .animations()
            .update(engine, frame.time(), &self.entities);
        if let Some((entity, transform, _)) =
            self.entities.components::<(&Transform, &Player)>().next()
        {
            engine.audio_mut().update(
                transform.position(),
                transform.orientation(),
                &self.entities,
            );

            if let Some(win_trigger) = self.entities.get("win_trigger") {
                if engine.physics().intersecting(&win_trigger, &entity) {
                    engine.screens().push("win");
                }
            }
        }
    }

    fn draw(&mut self, engine: &mut dyn Engine, frame: &mut Frame) {
        let swapchain_image = frame.swapchain_image();
        let builder = frame.builder_mut();

        engine
            .buf_pbr_pipeline()
            .draw(builder, swapchain_image.clone(), &self.entities)
            .expect("valid frame");
        let config = engine.config().clone();
        let buf_pbr_pipeline = engine.buf_pbr_pipeline().clone();
        engine
            .physics_mut()
            .draw(
                config,
                buf_pbr_pipeline,
                builder,
                swapchain_image,
                &self.entities,
            )
            .expect("valid frame");
    }

    fn on_leave(&mut self, engine: &mut dyn Engine) {
        self.music = None;
        engine.audio_mut().stop_all();
        engine
            .input_mut()
            .get_mut::<Mouse>("Mouse")
            .set_continuous(false);
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

impl ScreenExt for Play {
    fn new(_engine: &dyn Engine) -> Self {
        Self {
            music: None,
            orbs: Orbs::default(),
            buttons: Buttons::default(),
            risers: Risers::default(),
            springs: Default::default(),
            character_controllers: Default::default(),
            players: Default::default(),
            entities: Entities::new(),
        }
    }
}
