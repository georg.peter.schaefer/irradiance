use std::any::Any;

use irradiance_runtime::core::{Engine, Frame, Mode, Screen, ScreenExt};
use irradiance_runtime::input::Mouse;
use irradiance_runtime::math::Vec2;

use crate::ui::{Align, Button, Image, Ui, Widget};

/// Win screen.
pub struct Win {
    exit: Button,
    title: Image,
    ui: Ui,
}

impl Screen for Win {
    fn name(&self) -> &'static str {
        "win"
    }

    fn mode(&self) -> Mode {
        Mode::Modal
    }

    fn on_enter(&mut self, engine: &mut dyn Engine) {
        engine
            .input_mut()
            .get_mut::<Mouse>("Mouse")
            .set_continuous(false);
    }

    fn update(&mut self, engine: &mut dyn Engine, frame: &mut Frame) {
        self.exit.update(engine, &frame.swapchain_image());

        if self.exit.clicked() {
            engine.screens().pop();
            engine.screens().pop();
        }
    }

    fn draw(&mut self, _engine: &mut dyn Engine, frame: &mut Frame) {
        let swapchain_image = frame.swapchain_image().clone();
        self.ui
            .draw(
                true,
                frame.builder_mut(),
                swapchain_image,
                &[&self.title, &self.exit],
            )
            .expect("valid frame");
    }

    fn on_leave(&mut self, engine: &mut dyn Engine) {
        engine
            .input_mut()
            .get_mut::<Mouse>("Mouse")
            .set_continuous(true);
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

impl ScreenExt for Win {
    fn new(engine: &dyn Engine) -> Self {
        Self {
            ui: Ui::new(engine.graphics_context().clone()).expect("ui"),
            title: Image::new(
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/win.png")
                    .unwrap(),
                Vec2::new(0.0, 50.0),
                Align::CenterX,
            ),
            exit: Button::new(
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/exit.png")
                    .unwrap(),
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/exit_highlight.png")
                    .unwrap(),
                Vec2::zero(),
                Align::Center,
            ),
        }
    }
}
