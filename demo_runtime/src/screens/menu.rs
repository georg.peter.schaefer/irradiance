use crate::ui::{Align, Button, Image, Ui, Widget};
use irradiance_runtime::core::{Engine, Frame, Mode, Screen, ScreenExt};
use irradiance_runtime::math::Vec2;
use std::any::Any;

/// Menu screen.
pub struct Menu {
    exit: Button,
    play: Button,
    title: Image,
    ui: Ui,
}

impl Screen for Menu {
    fn name(&self) -> &'static str {
        "menu"
    }

    fn mode(&self) -> Mode {
        Mode::Opaque
    }

    fn on_enter(&mut self, _engine: &mut dyn Engine) {}

    fn update(&mut self, engine: &mut dyn Engine, frame: &mut Frame) {
        self.play.update(engine, &frame.swapchain_image());
        self.exit.update(engine, &frame.swapchain_image());

        if self.play.clicked() {
            engine.screens().push("play");
        }
        if self.exit.clicked() {
            engine.close();
        }
    }

    fn draw(&mut self, _engine: &mut dyn Engine, frame: &mut Frame) {
        let swapchain_image = frame.swapchain_image().clone();
        self.ui
            .draw(
                false,
                frame.builder_mut(),
                swapchain_image,
                &[&self.title, &self.play, &self.exit],
            )
            .expect("valid frame");
    }

    fn on_leave(&mut self, _engine: &mut dyn Engine) {}

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

impl ScreenExt for Menu {
    fn new(engine: &dyn Engine) -> Self {
        Self {
            ui: Ui::new(engine.graphics_context().clone()).expect("ui"),
            title: Image::new(
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/title.png")
                    .unwrap(),
                Vec2::new(0.0, 50.0),
                Align::CenterX,
            ),
            play: Button::new(
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/play.png")
                    .unwrap(),
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/play_highlight.png")
                    .unwrap(),
                Vec2::zero(),
                Align::Center,
            ),
            exit: Button::new(
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/exit.png")
                    .unwrap(),
                engine
                    .assets()
                    .read(engine.graphics_context().clone(), "menu/exit_highlight.png")
                    .unwrap(),
                Vec2::new(0.0, 200.0),
                Align::Center,
            ),
        }
    }
}
