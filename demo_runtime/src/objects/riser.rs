use std::sync::Arc;

use serde::Serialize;

use irradiance_editor::core::EditorEngine;
use irradiance_editor::ecs::EditorComponentExt;
use irradiance_editor::egui::Ui;
use irradiance_editor::gui::widgets::{DragValue, VecValue, Widget};
use irradiance_editor::gui::{Action, EditorContext, Icon, Icons};
use irradiance_runtime::ecs::Entities;
use irradiance_runtime::math::Vec3;
use irradiance_runtime::Component;

/// A riser.
#[derive(Clone, Default, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct Riser {
    /// Button state.
    #[serde(skip_serializing, skip_deserializing)]
    pub distance: f32,
    /// Maximum distance the riser can rise.
    pub max_distance: f32,
    /// Velocity.
    pub velocity: Vec3,
}

struct RiserInspector {
    max_distance: DragValue<f32>,
    velocity: VecValue<3>,
    entities: Arc<Entities>,
    entity: String,
}

impl RiserInspector {
    fn new(entity: String, entities: Arc<Entities>) -> Self {
        let mut velocity = VecValue::<3>::default();
        let mut max_distance = DragValue::<f32>::default();
        let riser = entities
            .get(&entity)
            .expect("entity")
            .get::<Riser>()
            .expect("riser");

        velocity.set(riser.velocity);
        max_distance.set(riser.max_distance);

        Self {
            max_distance,
            velocity,
            entities,
            entity,
        }
    }
}

impl Widget for RiserInspector {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Velocity");
        self.velocity.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Max Distance");
        self.max_distance.ui(ui, context, engine);
        ui.end_row();

        if self.velocity.changed() {
            context.actions_mut().push(
                engine,
                ChangeVelocity::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.velocity.get(),
                ),
            );
        }
        if self.max_distance.changed() {
            context.actions_mut().push(
                engine,
                ChangeMaxDistance::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.max_distance.get(),
                ),
            );
        }
    }
}

impl EditorComponentExt for Riser {
    fn display_name() -> &'static str {
        "Riser"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::arrow_up_from_ground_water())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(RiserInspector::new(entity, entities)))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }
}

struct ChangeVelocity {
    new: Vec3,
    old: Vec3,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeVelocity {
    fn new(entity: String, entities: Arc<Entities>, new: Vec3) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<Riser>()
                .expect("riser component")
                .velocity,
            entities,
            entity,
        }
    }

    fn replace(&self, value: Vec3) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Riser>()
            .expect("riser component")
            .velocity = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeVelocity {
    fn description(&self) -> String {
        "Change Velocity".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeMaxDistance {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeMaxDistance {
    fn new(entity: String, entities: Arc<Entities>, new: f32) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<Riser>()
                .expect("riser component")
                .max_distance,
            entities,
            entity,
        }
    }

    fn replace(&self, value: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Riser>()
            .expect("riser component")
            .max_distance = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeMaxDistance {
    fn description(&self) -> String {
        "Change Max Distance".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}
