use irradiance_runtime::audio::AudioSource;
use irradiance_runtime::core::{Engine, Time};
use irradiance_runtime::ecs::{Entities, Transform};

use crate::objects::{Button, Riser};

#[derive(Default)]
pub struct Risers;

impl Risers {
    pub fn update(&self, engine: &mut dyn Engine, time: Time, entities: &Entities) {
        for (entity, mut transform, mut riser) in entities
            .components::<(&mut Transform, &mut Riser)>()
            .filter(|(entity, _, _)| entity.get::<Button>().is_none())
        {
            if let Some(button) = entities
                .components::<(&Button,)>()
                .map(|(_, button)| button)
                .filter(|button| button.activates == entity.id())
                .next()
            {
                if button.activated {
                    if riser.distance < riser.max_distance && !engine.audio().is_playing(&entity) {
                        let sound = engine
                            .assets()
                            .read::<AudioSource, _>(
                                engine.graphics_context().clone(),
                                "temple/tomb_door.mp3",
                            )
                            .unwrap();
                        engine.audio_mut().play_at(&entity, sound.unwrap().clone());
                    }

                    self.update_position(time, &mut transform, &mut riser);
                }
            }
        }
        unsafe {
            entities.commit();
        }
    }

    fn update_position(&self, time: Time, transform: &mut Transform, riser: &mut Riser) {
        if riser.distance < riser.max_distance {
            let desired_translation = time.delta() * riser.velocity;
            let allowed_distance = desired_translation
                .length()
                .min(riser.max_distance - riser.distance);
            let allowed_translation = desired_translation.normalize() * allowed_distance;
            let position = transform.position() + allowed_translation;
            transform.set_position(position);
            riser.distance += allowed_distance;
        }
    }
}
