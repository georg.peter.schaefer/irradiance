use irradiance_runtime::core::Time;
use irradiance_runtime::ecs::{Entities, Transform};

use crate::objects::{Button, Riser};

#[derive(Default)]
pub struct Buttons;

impl Buttons {
    pub fn update(&self, time: Time, entities: &Entities) {
        for (_, mut transform, button, mut riser) in
            entities.components::<(&mut Transform, &Button, &mut Riser)>()
        {
            if button.activated {
                self.update_position(time, &mut transform, &mut riser);
            }
        }
        unsafe {
            entities.commit();
        }
    }

    fn update_position(&self, time: Time, transform: &mut Transform, riser: &mut Riser) {
        if riser.distance < riser.max_distance {
            let desired_translation = time.delta() * riser.velocity;
            let allowed_distance = desired_translation
                .length()
                .min(riser.max_distance - riser.distance);
            let allowed_translation = desired_translation.normalize() * allowed_distance;
            let position = transform.position() + allowed_translation;
            transform.set_position(position);
            riser.distance += allowed_distance;
        }
    }
}
