use crate::objects::Orb;
use irradiance_runtime::audio::AudioSource;
use irradiance_runtime::core::{Engine, Time};
use irradiance_runtime::ecs::{Entities, Transform};
use irradiance_runtime::math::Vec3;
use irradiance_runtime::rendering::PointLight;
use irradiance_runtime::rodio::Source;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::time::Duration;

#[derive(Default)]
pub struct Orbs;

impl Orbs {
    pub fn update(&self, engine: &mut dyn Engine, time: Time, entities: &Entities) {
        for (entity, mut transform, mut point_light, _) in
            entities.components::<(&mut Transform, &mut PointLight, &Orb)>()
        {
            let mut hasher = DefaultHasher::default();
            entity.id().hash(&mut hasher);
            let hash = hasher.finish();

            let velocity = self.velocity(time, hash);
            let position = transform.position() + time.delta() * velocity;
            transform.set_position(position);

            let intensity =
                point_light.intensity() + time.delta() * self.intensity(time, hash) * 20.0;
            point_light.set_intensity(intensity);

            if !engine.audio().is_playing(&entity) {
                let sound = engine
                    .assets()
                    .read::<AudioSource, _>(engine.graphics_context().clone(), "temple/pulse.wav")
                    .unwrap();
                engine.audio_mut().play_at(
                    &entity,
                    sound
                        .unwrap()
                        .clone()
                        .repeat_infinite()
                        .skip_duration(Duration::from_millis(hash % 1024)),
                );
            }
        }

        unsafe {
            entities.commit();
        }
    }

    fn velocity(&self, time: Time, hash: u64) -> Vec3 {
        let offset = 1.0 / hash as f32;
        match hash % 3 {
            0 => {
                Vec3::new(
                    -(time.elapsed() + 0.1).sin() + offset,
                    time.elapsed().cos() - offset,
                    (time.elapsed() + 0.5).sin() - offset,
                ) * 0.15
            }
            1 => {
                Vec3::new(
                    (time.elapsed() + 0.1).cos() + offset,
                    time.elapsed().cos() - offset,
                    -(time.elapsed() + 0.5).sin() + offset,
                ) * 0.175
            }
            2 => {
                Vec3::new(
                    -(time.elapsed() + 0.2).cos() - offset,
                    time.elapsed().sin() - offset,
                    (time.elapsed() + 0.05).sin() + offset,
                ) * 0.2
            }
            _ => unreachable!(),
        }
    }

    fn intensity(&self, time: Time, hash: u64) -> f32 {
        let offset = 1.0 / hash as f32;
        match hash % 4 {
            0 => time.elapsed().cos() + offset,
            1 => time.elapsed().sin() - offset,
            2 => -time.elapsed().cos() - offset,
            3 => -time.elapsed().sin() + offset,
            _ => unreachable!(),
        }
    }
}
