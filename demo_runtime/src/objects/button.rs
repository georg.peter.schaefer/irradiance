use std::sync::Arc;

use serde::Serialize;

use irradiance_editor::core::EditorEngine;
use irradiance_editor::ecs::EditorComponentExt;
use irradiance_editor::egui::Ui;
use irradiance_editor::gui::widgets::{TextEdit, Widget};
use irradiance_editor::gui::{Action, EditorContext, Icon, Icons};
use irradiance_runtime::ecs::Entities;
use irradiance_runtime::Component;

/// A button.
#[derive(Clone, Default, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct Button {
    /// Button state.
    #[serde(skip_serializing, skip_deserializing)]
    pub activated: bool,
    /// Entity that is activated by this button.
    pub activates: String,
}

struct ButtonInspector {
    activates: TextEdit,
    entities: Arc<Entities>,
    entity: String,
}

impl ButtonInspector {
    fn new(entity: String, entities: Arc<Entities>) -> Self {
        let mut activates = TextEdit::default();

        activates.set(
            entities
                .get(&entity)
                .expect("entity")
                .get::<Button>()
                .expect("button")
                .activates
                .clone(),
        );

        Self {
            activates,
            entities,
            entity,
        }
    }
}

impl Widget for ButtonInspector {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Activates");
        self.activates.ui(ui, context, engine);
        ui.end_row();

        if self.activates.changing() {
            self.entities
                .get(&self.entity)
                .expect("entity")
                .get_mut::<Button>()
                .expect("button")
                .activates = self.activates.get().clone();
            unsafe {
                self.entities.commit();
            }
        }
        if self.activates.changed() {
            context.actions_mut().push(
                engine,
                ChangeActivates::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.activates.get_old().clone(),
                    self.activates.get().clone(),
                ),
            );
        }
    }
}

impl EditorComponentExt for Button {
    fn display_name() -> &'static str {
        "Button"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::pushed())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(ButtonInspector::new(entity, entities)))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }
}

struct ChangeActivates {
    new: String,
    old: String,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeActivates {
    fn new(entity: String, entities: Arc<Entities>, old: String, new: String) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: String) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Button>()
            .expect("button")
            .activates = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeActivates {
    fn description(&self) -> String {
        "Change Activates".to_owned()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone())
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone())
    }
}
