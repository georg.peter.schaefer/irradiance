use std::marker::PhantomData;
use std::sync::Arc;

use serde::Serialize;

use irradiance_editor::core::EditorEngine;
use irradiance_editor::ecs::EditorComponentExt;
use irradiance_editor::egui::Ui;
use irradiance_editor::gui::widgets::Widget;
use irradiance_editor::gui::{EditorContext, Icon, Icons};
use irradiance_runtime::ecs::Entities;
use irradiance_runtime::Component;

/// Orb component.
#[derive(Default, Copy, Clone, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct Orb {
    _phantom_data: PhantomData<()>,
}

#[derive(Default)]
struct OrbInspector;

impl Widget for OrbInspector {
    fn ui(&mut self, _ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {}
}

impl EditorComponentExt for Orb {
    fn display_name() -> &'static str {
        "Orb"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::wand_magic_sparkles())
    }

    fn inspect(
        _entity: String,
        _entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(OrbInspector::default()))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }
}
