use irradiance_runtime::core::Engine;
use irradiance_runtime::ecs::{Entities, SphericalCoordinate, Spring};
use irradiance_runtime::math::Vec2;

use crate::character::{CharacterController, State};
use crate::player::Player;

#[derive(Default)]
pub struct Players;

impl Players {
    pub fn update(&self, engine: &dyn Engine, entities: &Entities) {
        for (entity, mut character_controller, _) in
            entities.components::<(&mut CharacterController, &Player)>()
        {
            let walk_coefficient = engine
                .input()
                .action::<bool>("character/walk")
                .then_some(character_controller.walk_coefficient)
                .unwrap_or(1.0);
            character_controller.walk(
                engine.input().action::<Vec2>("character/move") * Vec2::new(-1.0, 1.0),
                walk_coefficient,
            );
            character_controller.turn(-engine.input().action::<f32>("character/turn"));

            for (_, _, mut spherical_coordinate) in entities
                .components::<(&Spring, &mut SphericalCoordinate)>()
                .filter(|(_, spring, _)| spring.target == entity.id())
                .filter(|_| character_controller.state == State::Locomotion)
            {
                let look = engine.input().action::<Vec2>("character/look")
                    * Vec2::new(1.0, 0.125)
                    * character_controller.turn_velocity;
                let inclination = (spherical_coordinate.inclination() - look[1])
                    .clamp(0.01, std::f32::consts::PI - 0.01);
                let azimuth = spherical_coordinate.azimuth() - look[0];
                spherical_coordinate.set_inclination(inclination);
                spherical_coordinate.set_azimuth(azimuth);
            }

            if engine.input().action::<bool>("character/jump") {
                character_controller.jump();
            }
            if engine.input().action::<bool>("character/hold") {
                character_controller.hold();
            }
            if engine.input().action::<bool>("character/climb") {
                character_controller.climb();
            }
            if engine.input().action::<bool>("character/interact") {
                character_controller.interact();
            }
        }
        unsafe {
            entities.commit();
        }
    }
}
