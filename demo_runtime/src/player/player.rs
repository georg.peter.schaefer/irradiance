use irradiance_editor::core::EditorEngine;
use irradiance_editor::ecs::EditorComponentExt;
use irradiance_editor::egui::Ui;
use irradiance_editor::gui::widgets::Widget;
use irradiance_editor::gui::{EditorContext, Icon, Icons};
use irradiance_runtime::ecs::Entities;
use irradiance_runtime::Component;
use serde::Serialize;
use std::marker::PhantomData;
use std::sync::Arc;

/// Player component.
#[derive(Default, Copy, Clone, Serialize, Component)]
#[serde(tag = "type_name")]
pub struct Player {
    _phantom_data: PhantomData<()>,
}

#[derive(Default)]
struct PlayerInspector;

impl Widget for PlayerInspector {
    fn ui(&mut self, _ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {}
}

impl EditorComponentExt for Player {
    fn display_name() -> &'static str {
        "Player"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::user())
    }

    fn inspect(
        _entity: String,
        _entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(PlayerInspector::default()))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }
}
