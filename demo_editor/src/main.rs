use demo_runtime::character::CharacterController;
use demo_runtime::objects::{Button, Orb, Riser};
use demo_runtime::player::Player;
use demo_runtime::screens::{Menu, Pause, Play, Win};
use irradiance_editor::core::Editor;
use irradiance_runtime::core::App;

fn main() {
    App::run(
        Editor::builder()
            .title("irradiance - Demo")
            .assets("assets")
            .screen::<Menu>()
            .screen::<Play>()
            .screen::<Pause>()
            .screen::<Win>()
            .component::<Player>()
            .component::<CharacterController>()
            .component::<Button>()
            .component::<Riser>()
            .component::<Orb>()
            .build(),
    )
}
