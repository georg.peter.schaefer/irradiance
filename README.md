# irradiance engine
## Build
Before you can build the engine and demo project make sure that the following components are installed von your system:
- git-lfs (see https://git-lfs.com/)
- Vulkan SDK (see https://www.lunarg.com/vulkan-sdk/)
- Rust (see https://www.rust-lang.org/tools/install)

Make sure to install git-lfs before cloning the repository. If you install git-lfs afterwards, change into the directory of the repository and run ```git lfs pull```.
To run the demo application and the demo editor execute ```cargo run --bin demo --release``` or ```cargo run --bin demo_editor --release```. The first build can take some time, as shaderc may be build from source.

## Controls
The character in the demo can be controlled with WASD-keys and the camera can be moved with the mouse. The character can jump by pressing the ```alt```-key and can grab an edge by holding the ```shift```-key when jumping. The demo contains two buttons that can pressed by positioning the character in front of the button and pressing the left mouse button. The ```escape```-key opens the pause menu.

The camera inside the scene panel of the editor can be moved by holding the middle mouse button and using the WASD-keys to navigate. Objects in the scene can be selected by clicking on them.