use std::sync::Arc;

use egui::Ui;

use irradiance_runtime::core::Engine;
use irradiance_runtime::ecs::{Entities, Ref};
use irradiance_runtime::gfx::GraphicsContext;
use irradiance_runtime::rendering::CascadedShadow;

use crate::core::EditorEngine;
use crate::ecs::EditorComponentExt;
use crate::gui::widgets::{ComboBox, Widget};
use crate::gui::{Action, EditorContext, Icon, Icons};

struct CascadedShadowInspector {
    resolution: ComboBox<u32>,
    cascades: ComboBox<u32>,
    entities: Arc<Entities>,
    entity: String,
}

impl CascadedShadowInspector {
    fn new(entity: String, entities: Arc<Entities>) -> Self {
        let cascaded_shadow = entities
            .get(&entity)
            .expect("entity")
            .get::<CascadedShadow>()
            .expect("cascaded shadow component");
        let mut cascades = ComboBox::builder()
            .value(1)
            .value(2)
            .value(3)
            .value(4)
            .value(5)
            .value(6)
            .build();
        cascades.set(cascaded_shadow.cascades().len() as _);
        let mut resolution = ComboBox::builder()
            .value(256)
            .value(512)
            .value(1024)
            .value(2048)
            .value(4096)
            .value(8192)
            .build();
        resolution.set(cascaded_shadow.image_view().image().extent().width);

        Self {
            entity,
            entities,
            cascades,
            resolution,
        }
    }
}

impl Widget for CascadedShadowInspector {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Cascades");
        self.cascades.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Resolution");
        self.resolution.ui(ui, context, engine);
        ui.end_row();

        if self.cascades.changed() {
            context.actions_mut().push(
                engine,
                ChangeCascades::new(
                    engine.graphics_context().clone(),
                    self.entity.clone(),
                    self.entities.clone(),
                    self.component().cascades().len() as _,
                    self.cascades.get(),
                ),
            );
        }
        if self.resolution.changed() {
            context.actions_mut().push(
                engine,
                ChangeResolution::new(
                    engine.graphics_context().clone(),
                    self.entity.clone(),
                    self.entities.clone(),
                    self.component().image_view().image().extent().width,
                    self.resolution.get(),
                ),
            );
        }
    }
}

impl CascadedShadowInspector {
    fn component(&self) -> Ref<'_, CascadedShadow> {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get::<CascadedShadow>()
            .expect("cascaded shadow component")
    }
}

impl EditorComponentExt for CascadedShadow {
    fn display_name() -> &'static str {
        "Cascaded Shadow"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::cloud_sun())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(CascadedShadowInspector::new(entity, entities)))
    }

    fn create(engine: &EditorEngine) -> Self {
        Self::new(engine.graphics_context().clone(), 4, 4096).expect("cascaded shadow")
    }
}

struct ChangeCascades {
    new: u32,
    old: u32,
    entities: Arc<Entities>,
    entity: String,
    graphics_context: Arc<GraphicsContext>,
}

impl ChangeCascades {
    fn new(
        graphics_context: Arc<GraphicsContext>,
        entity: String,
        entities: Arc<Entities>,
        old: u32,
        new: u32,
    ) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
            graphics_context,
        }
    }

    fn replace(&self, cascades: u32) {
        let entity = self.entities.get(&self.entity).expect("entity");
        let resolution = entity
            .get::<CascadedShadow>()
            .expect("cascaded shadow component")
            .image_view()
            .image()
            .extent()
            .width;
        entity.insert(
            CascadedShadow::new(self.graphics_context.clone(), cascades, resolution)
                .expect("cascaded shadow"),
        );
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeCascades {
    fn description(&self) -> String {
        "Change Cascades of Cascaded Shadow Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeResolution {
    new: u32,
    old: u32,
    entities: Arc<Entities>,
    entity: String,
    graphics_context: Arc<GraphicsContext>,
}

impl ChangeResolution {
    fn new(
        graphics_context: Arc<GraphicsContext>,
        entity: String,
        entities: Arc<Entities>,
        old: u32,
        new: u32,
    ) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
            graphics_context,
        }
    }

    fn replace(&self, resolution: u32) {
        let entity = self.entities.get(&self.entity).expect("entity");
        let cascades = entity
            .get::<CascadedShadow>()
            .expect("cascaded shadow component")
            .cascades()
            .len() as u32;
        entity.insert(
            CascadedShadow::new(self.graphics_context.clone(), cascades, resolution)
                .expect("cascaded shadow"),
        );
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeResolution {
    fn description(&self) -> String {
        "Change Resolution of Cascaded Shadow Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}
