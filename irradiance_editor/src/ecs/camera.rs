use std::path::Path;
use std::sync::Arc;

use egui::Ui;
use irradiance_runtime::asset::{Asset, AssetRef};
use irradiance_runtime::core::Engine;
use irradiance_runtime::math::Vec4;
use irradiance_runtime::rendering::{Material, Mesh};
use irradiance_runtime::{ecs::Entities, rendering::Camera};

use crate::{
    core::EditorEngine,
    ecs::EditorComponentExt,
    gui::{
        widgets::{DragValue, Widget},
        Action, EditorContext, Icon, Icons,
    },
};

pub struct CameraInspector {
    far: DragValue<f32>,
    near: DragValue<f32>,
    fov_y: DragValue<f32>,
    entities: Arc<Entities>,
    entity: String,
}

impl CameraInspector {
    fn new(entity: String, entities: Arc<Entities>) -> Self {
        let camera = entities
            .get(&entity)
            .expect("entity")
            .get::<Camera>()
            .expect("camera component");

        let mut fov_y = DragValue::builder().suffix("°").build();
        fov_y.set(camera.fov_y().to_degrees());
        let mut near = DragValue::default();
        near.set(camera.near());
        let mut far = DragValue::default();
        far.set(camera.far());

        Self {
            entity,
            entities,
            fov_y,
            near,
            far,
        }
    }
}

impl Widget for CameraInspector {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Fovy");
        self.fov_y.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Near Plane");
        self.near.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Far Plane");
        self.far.ui(ui, context, engine);
        ui.end_row();

        if self.fov_y.changing() {
            self.entities
                .get(self.entity.clone())
                .expect("entity")
                .get_mut::<Camera>()
                .expect("camera component")
                .set_fov_y(self.fov_y.get().to_radians());
        }
        if self.fov_y.changed() {
            context.actions_mut().push(
                engine,
                ChangeFovY::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.fov_y.get_old().to_radians(),
                    self.fov_y.get().to_radians(),
                ),
            );
        }

        if self.near.changing() {
            self.entities
                .get(self.entity.clone())
                .expect("entity")
                .get_mut::<Camera>()
                .expect("camera component")
                .set_near(self.near.get());
        }
        if self.near.changed() {
            context.actions_mut().push(
                engine,
                ChangeNearPlane::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.near.get_old(),
                    self.near.get(),
                ),
            );
        }

        if self.far.changing() {
            self.entities
                .get(self.entity.clone())
                .expect("entity")
                .get_mut::<Camera>()
                .expect("transform component")
                .set_far(self.far.get());
        }
        if self.far.changed() {
            context.actions_mut().push(
                engine,
                ChangeFarPlane::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.far.get_old(),
                    self.far.get(),
                ),
            );
        }
    }
}

impl EditorComponentExt for Camera {
    fn display_name() -> &'static str {
        "Camera"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::camera())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(CameraInspector::new(entity, entities)))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }

    fn display(engine: &EditorEngine) -> Option<Mesh> {
        let mut display = Mesh::try_from_bytes(
            engine.assets().clone(),
            engine.graphics_context().clone(),
            Path::new("__internal__"),
            include_bytes!("camera.gltf").to_vec(),
        )
        .expect("mesh");
        let blue = Material::builder()
            .base_color(Vec4::new(
                0.19596512615680695,
                0.11650196462869644,
                1.0,
                1.0,
            ))
            .build();
        let black = Material::builder()
            .base_color(Vec4::new(
                0.0290991198271513,
                0.0290991198271513,
                0.0290991198271513,
                1.0,
            ))
            .build();
        display.sub_meshes_mut()[0].set_material(AssetRef::new_pre_read(Ok(blue)));
        display.sub_meshes_mut()[1].set_material(AssetRef::new_pre_read(Ok(black)));
        Some(display)
    }
}

struct ChangeFovY {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeFovY {
    fn new(entity: String, entities: Arc<Entities>, old: f32, new: f32) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, fov_y: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Camera>()
            .expect("camera component")
            .set_fov_y(fov_y);
    }
}

impl Action for ChangeFovY {
    fn description(&self) -> String {
        "Change Field of View Y of Camera Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeNearPlane {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeNearPlane {
    fn new(entity: String, entities: Arc<Entities>, old: f32, new: f32) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, near: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Camera>()
            .expect("camera component")
            .set_near(near);
    }
}

impl Action for ChangeNearPlane {
    fn description(&self) -> String {
        "Change Near Plane of Camera Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeFarPlane {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeFarPlane {
    fn new(entity: String, entities: Arc<Entities>, old: f32, new: f32) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, far: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Camera>()
            .expect("camera component")
            .set_far(far);
    }
}

impl Action for ChangeFarPlane {
    fn description(&self) -> String {
        "Change Far Plane of Camera Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}
