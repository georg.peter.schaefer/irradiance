use crate::core::EditorEngine;
use crate::ecs::component_display_pipeline::shaders::PushConstants;
use crate::ecs::EditorComponentTypes;
use irradiance_runtime::ecs::{Entities, Transform};
use irradiance_runtime::gfx::command_buffer::{CommandBufferBuilder, IndexType};
use irradiance_runtime::gfx::device::Device;
use irradiance_runtime::gfx::image::{
    Format, ImageAspects, ImageLayout, ImageSubresourceRange, ImageView,
};
use irradiance_runtime::gfx::pipeline::{
    ColorBlendAttachmentState, CompareOp, CullMode, DynamicState, FrontFace, GraphicsPipeline,
    PipelineLayout, PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
};
use irradiance_runtime::gfx::render_pass::{
    Attachment, AttachmentLoadOp, AttachmentStoreOp, RenderPass,
};
use irradiance_runtime::gfx::sync::{
    AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages,
};
use irradiance_runtime::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use irradiance_runtime::math::{Mat4, Vec3, Vec4};
use irradiance_runtime::rendering::pbr::GBuffer;
use irradiance_runtime::rendering::{mesh, Camera, Mesh};
use std::any::TypeId;
use std::collections::HashMap;
use std::sync::Arc;

/// Pipeline for displaying components.
pub struct ComponentDisplayPipeline {
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    displays: HashMap<TypeId, Mesh>,
}

impl ComponentDisplayPipeline {
    /// Creates a new component display pipeline.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let pipeline_layout = Self::create_pipeline_layout(graphics_context.device().clone())?;
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            pipeline_layout.clone(),
        )?;

        Ok(Self {
            graphics_pipeline,
            pipeline_layout,
            displays: Default::default(),
        })
    }

    fn create_pipeline_layout(device: Arc<Device>) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    fragment: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .attribute(1, 0, Format::R32G32B32SFloat, 12)
                    .build(),
            )
            .vertex_shader(vertex_shader::Shader::load(device.clone())?, "main")
            .fragment_shader(fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::Less)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::B8G8R8A8Srgb)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    /// Sets the component displays.
    pub fn set_displays(&mut self, component_types: &EditorComponentTypes, engine: &EditorEngine) {
        self.displays = component_types
            .types()
            .flat_map(|component_type| {
                component_types
                    .display(component_type.clone(), engine)
                    .map(|display| (component_type, display))
            })
            .collect();
    }

    /// Draws the components.
    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        g_buffer: &GBuffer,
        final_image: Arc<ImageView>,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Component Display Pass", Vec4::new(0.5, 0.2, 0.9, 1.0));

        builder
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(g_buffer.depth_buffer().image().clone())
                            .src_stage_mask(PipelineStages {
                                early_fragment_tests: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                depth_stencil_attachment_read: true,
                                depth_stencil_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                early_fragment_tests: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                depth_stencil_attachment_read: true,
                                depth_stencil_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::DepthAttachmentOptimal)
                            .new_layout(ImageLayout::DepthAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        depth: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(final_image.image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .bind_graphics_pipeline(self.graphics_pipeline.clone())
            .begin_rendering(self.create_render_pass(g_buffer, final_image))
            .set_viewport(
                0,
                vec![Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: g_buffer.extent().width as _,
                    height: g_buffer.extent().height as _,
                    min_depth: 0.0,
                    max_depth: 1.0,
                }],
            )
            .set_scissor(
                0,
                vec![Rect2D {
                    offset: Default::default(),
                    extent: g_buffer.extent(),
                }],
            );

        if let Some((Some(camera), Some(camera_transform))) = entities
            .active::<Camera>()
            .map(|camera| (camera.get::<Camera>(), camera.get::<Transform>()))
        {
            for (entity, transform) in
                entities
                    .components::<(&Transform,)>()
                    .filter(|(entity, _)| {
                        Some(entity.id().to_string())
                            != entities
                                .active::<Camera>()
                                .map(|entity| entity.id().to_string())
                    })
            {
                for component_type in entity.types() {
                    if let Some(display) = self.displays.get(&component_type) {
                        builder
                            .bind_vertex_buffer(display.vertex_buffer().clone())
                            .bind_index_buffer(
                                display.index_buffer().clone(),
                                0,
                                IndexType::UInt32,
                            );

                        for sub_mesh in display.sub_meshes() {
                            let material = sub_mesh.material.unwrap();

                            builder
                                .push_constants(
                                    self.pipeline_layout.clone(),
                                    ShaderStages {
                                        vertex: true,
                                        fragment: true,
                                        ..Default::default()
                                    },
                                    0,
                                    PushConstants {
                                        projection: camera.projection(
                                            g_buffer.extent().width as f32
                                                / g_buffer.extent().height as f32,
                                        ),
                                        view: Mat4::from(*camera_transform).inverse(),
                                        model: Mat4::from(*transform),
                                        eye: camera_transform.position(),
                                        _padding0: 0.0,
                                        base_color: Vec3::new(
                                            material.base_color()[0],
                                            material.base_color()[1],
                                            material.base_color()[2],
                                        ),
                                        _padding1: 0.0,
                                    },
                                )
                                .draw_indexed(
                                    sub_mesh.count() as _,
                                    1,
                                    sub_mesh.offset() as _,
                                    0,
                                    0,
                                );
                        }
                    }
                }
            }
        }

        builder.end_rendering().end_label();

        Ok(())
    }

    fn create_render_pass(&self, g_buffer: &GBuffer, final_image: Arc<ImageView>) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: final_image.image().extent().into(),
            })
            .color_attachment(
                Attachment::builder(final_image)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .depth_attachment(
                Attachment::builder(g_buffer.depth_buffer().clone())
                    .image_layout(ImageLayout::DepthAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .build()
    }
}

mod shaders {
    use irradiance_runtime::math::{Mat4, Vec3};

    #[allow(unused)]
    #[repr(C)]
    pub struct PushConstants {
        pub projection: Mat4,
        pub view: Mat4,
        pub model: Mat4,
        pub eye: Vec3,
        pub _padding0: f32,
        pub base_color: Vec3,
        pub _padding1: f32,
    }
}

mod vertex_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/ecs/component_display.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod fragment_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/ecs/component_display.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
