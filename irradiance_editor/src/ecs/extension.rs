use std::vec::IntoIter;
use std::{any::TypeId, collections::HashMap, sync::Arc};

use irradiance_runtime::ecs::{Component, Entities, Spring, Transform};
use irradiance_runtime::physics::RigidBody;
use irradiance_runtime::rendering::{
    AnimatedMesh, Camera, CascadedShadow, DirectionalLight, Mesh, OmnidirectionalShadow,
    PointLight, StaticMesh,
};

use crate::{
    core::EditorEngine,
    gui::{widgets::Widget, Icon},
};

/// Component extension trait for the editor.
pub trait EditorComponentExt: Component {
    /// Returns the display name of the component type.
    fn display_name() -> &'static str;
    /// Returns an icon for the component type.
    fn icon() -> Option<Icon> {
        None
    }
    /// Returns a widget for inspecting the component.
    fn inspect(
        _entity: String,
        _entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        None
    }
    /// Creates a component.
    fn create(_engine: &EditorEngine) -> Self;
    /// Returns a [`Mesh`] for displaying the component in the scene editor.
    fn display(_engine: &EditorEngine) -> Option<Mesh> {
        None
    }
}

type CreateFactory = dyn Fn(&EditorEngine) -> Arc<dyn Component>;
type InspectorFactory = dyn Fn(String, Arc<Entities>, &EditorEngine) -> Option<Box<dyn Widget>>;
type DisplayFactory = dyn Fn(&EditorEngine) -> Option<Mesh>;

/// Component type registration.
pub struct EditorComponentTypes {
    displays: Vec<Box<DisplayFactory>>,
    creates: Vec<Box<CreateFactory>>,
    inspects: Vec<Box<InspectorFactory>>,
    icons: Vec<Option<Icon>>,
    display_names: Vec<&'static str>,
    types: HashMap<TypeId, usize>,
}

impl EditorComponentTypes {
    pub(crate) fn builder() -> EditorComponentTypesBuilder {
        EditorComponentTypesBuilder {
            types: Default::default(),
            display_names: vec![],
            icons: vec![],
            inspects: vec![],
            creates: vec![],
            displays: vec![],
        }
        .component::<Transform>()
        .component::<StaticMesh>()
        .component::<AnimatedMesh>()
        .component::<PointLight>()
        .component::<Camera>()
        .component::<DirectionalLight>()
        .component::<CascadedShadow>()
        .component::<OmnidirectionalShadow>()
        .component::<RigidBody>()
        .component::<Spring>()
    }

    /// Returns the types.
    pub fn types(&self) -> IntoIter<TypeId> {
        let mut types = self.types.clone().into_keys().collect::<Vec<_>>();
        types.sort_by_key(|component_type| self.types[component_type]);
        types.into_iter()
    }

    /// Returns the display name.
    pub fn display_name(&self, component_type: TypeId) -> Option<&'static str> {
        self.types
            .get(&component_type)
            .map(|index| self.display_names[*index])
    }

    /// Returns the icon.
    pub fn icon(&self, component_type: TypeId) -> Option<Icon> {
        self.types
            .get(&component_type)
            .and_then(|index| self.icons[*index])
    }

    /// Returns a widget for inspecting a component.
    pub fn inspect(
        &self,
        component_type: TypeId,
        entity: String,
        entities: Arc<Entities>,
        engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        entities
            .get(entity.clone())
            .and_then(|entity| entity.get_dyn(component_type))
            .and_then(|_| {
                self.types
                    .get(&component_type)
                    .and_then(|index| self.inspects[*index](entity, entities, engine))
            })
    }

    /// Creates a component and inserts it into an entity.
    pub fn create(
        &self,
        component_type: TypeId,
        engine: &EditorEngine,
    ) -> Option<Arc<dyn Component>> {
        self.types
            .get(&component_type)
            .map(|index| self.creates[*index](engine))
    }

    /// Returns the add sub menu entries for all registered component types.
    pub fn create_entries(&self) -> Vec<(Option<Icon>, &'static str, TypeId)> {
        let mut component_types = self.types.iter().collect::<Vec<_>>();
        component_types.sort_by_key(|(_, index)| *index);
        component_types
            .into_iter()
            .map(|(component_type, index)| {
                (
                    self.icons[*index],
                    self.display_names[*index],
                    *component_type,
                )
            })
            .collect()
    }

    /// Returns a [`Mesh`] for displaying the component in the scene editor.
    pub fn display(&self, component_type: TypeId, engine: &EditorEngine) -> Option<Mesh> {
        self.types
            .get(&component_type)
            .and_then(|index| self.displays[*index](engine))
    }
}

/// Type for building component types.
pub struct EditorComponentTypesBuilder {
    displays: Vec<Box<DisplayFactory>>,
    creates: Vec<Box<CreateFactory>>,
    inspects: Vec<Box<InspectorFactory>>,
    icons: Vec<Option<Icon>>,
    display_names: Vec<&'static str>,
    types: HashMap<TypeId, usize>,
}

impl EditorComponentTypesBuilder {
    /// Registers a component type.
    pub fn component<C: EditorComponentExt>(mut self) -> Self {
        let index = self.types.len();
        self.types.insert(TypeId::of::<C>(), index);
        self.display_names.push(C::display_name());
        self.icons.push(C::icon());
        self.inspects.push(Box::new(C::inspect));
        self.creates
            .push(Box::new(|engine| Arc::new(C::create(engine))));
        self.displays.push(Box::new(|engine| C::display(engine)));
        self
    }

    /// Builds the component types.
    pub fn build(self) -> EditorComponentTypes {
        EditorComponentTypes {
            types: self.types,
            display_names: self.display_names,
            icons: self.icons,
            inspects: self.inspects,
            creates: self.creates,
            displays: self.displays,
        }
    }
}
