struct VSOutput {
    [[vk::location(0)]] float3 normal : NORMAL0;
    [[vk::location(1)]] float3 light_dir: NORMAL1;
};

struct PushConstants {
    float4x4 projection;
    float4x4 view;
    float4x4 model;
    float3 eye;
    float3 base_color;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

float4 main(VSOutput input) : SV_TARGET {
    float3 N = normalize(input.normal);
    float3 L = normalize(input.light_dir);

    float NdotL = saturate(dot(N, L));

    return float4(push_constants.base_color * NdotL, 1.0);
}
