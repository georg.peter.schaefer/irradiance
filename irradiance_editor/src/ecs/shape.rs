use std::path::PathBuf;
use std::sync::Arc;

use egui::Ui;

use irradiance_runtime::asset::Assets;
use irradiance_runtime::core::Engine;
use irradiance_runtime::gfx::GraphicsContext;
use irradiance_runtime::physics::shape::{Ball, Capsule, Cuboid, Cylinder};
use irradiance_runtime::physics::Shape;
use irradiance_runtime::rendering::Mesh;

use crate::core::EditorEngine;
use crate::gui::widgets::{AssetSlot, ComboBox, DragValue, VecValue, Widget};
use crate::gui::EditorContext;

pub struct ShapeInspector {
    old_value: Shape,
    value: Shape,
    shape_widget: Box<dyn ShapeWidget>,
    shape_type: ComboBox<Shape>,
    assets: Arc<Assets>,
    graphics_context: Arc<GraphicsContext>,
}

impl ShapeInspector {
    pub fn new(engine: &EditorEngine, shape: Shape) -> Self {
        let mut shape_type = ComboBox::builder()
            .value(Shape::Ball(Default::default()))
            .value(Shape::Cuboid(Default::default()))
            .value(Shape::Cylinder(Default::default()))
            .value(Shape::Capsule(Default::default()))
            .value(Shape::Mesh(Default::default()))
            .build();
        shape_type.set(shape.clone());

        Self {
            old_value: shape.clone(),
            value: shape.clone(),
            shape_widget: Self::shape_widget(
                engine.graphics_context().clone(),
                engine.assets().clone(),
                shape,
            ),
            shape_type,
            assets: engine.assets().clone(),
            graphics_context: engine.graphics_context().clone(),
        }
    }

    pub fn changed(&self) -> bool {
        self.shape_type.changed() || self.shape_widget.changed()
    }

    pub fn changing(&self) -> bool {
        self.shape_type.changed() || self.shape_widget.changing()
    }

    pub fn get(&self) -> Shape {
        self.value.clone()
    }

    pub fn get_old(&self) -> Shape {
        self.old_value.clone()
    }
}

impl Widget for ShapeInspector {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        if self.changed() {
            self.old_value = self.value.clone();
        }

        ui.label("    Shape");
        self.shape_type.ui(ui, context, engine);
        ui.end_row();

        if self.shape_type.changed() {
            self.shape_widget = Self::shape_widget(
                self.graphics_context.clone(),
                self.assets.clone(),
                self.shape_type.get(),
            );
        }

        self.shape_widget.ui(ui, context, engine);

        self.value = self.shape_widget.get();
    }
}

impl ShapeInspector {
    fn shape_widget(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        shape: Shape,
    ) -> Box<dyn ShapeWidget> {
        match shape {
            Shape::Ball(ball) => Box::new(BallWidget::new(ball)),
            Shape::Cuboid(cuboid) => Box::new(CuboidWidget::new(cuboid)),
            Shape::Cylinder(cylinder) => Box::new(CylinderWidget::new(cylinder)),
            Shape::Capsule(capsule) => Box::new(CapsuleWidget::new(capsule)),
            Shape::Mesh(mesh) => {
                Box::new(MeshWidget::new(graphics_context, assets, mesh.path.clone()))
            }
        }
    }
}

trait ShapeWidget: Widget {
    fn get(&self) -> Shape;
    fn changed(&self) -> bool;
    fn changing(&self) -> bool;
}

struct BallWidget {
    radius: DragValue<f32>,
    ball: Ball,
}

impl BallWidget {
    fn new(ball: Ball) -> Self {
        let mut radius = DragValue::default();
        radius.set(ball.radius);

        Self { ball, radius }
    }
}

impl Widget for BallWidget {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Radius");
        self.radius.ui(ui, context, engine);
        ui.end_row();

        self.ball.radius = self.radius.get();
    }
}

impl ShapeWidget for BallWidget {
    fn get(&self) -> Shape {
        Shape::Ball(self.ball)
    }

    fn changed(&self) -> bool {
        self.radius.changed()
    }

    fn changing(&self) -> bool {
        self.radius.changing()
    }
}

struct CuboidWidget {
    half_extent: VecValue<3>,
    cuboid: Cuboid,
}

impl CuboidWidget {
    fn new(cuboid: Cuboid) -> Self {
        let mut half_extent = VecValue::default();
        half_extent.set(cuboid.half_extent);

        Self {
            cuboid,
            half_extent,
        }
    }
}

impl Widget for CuboidWidget {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Half Extent");
        self.half_extent.ui(ui, context, engine);
        ui.end_row();

        self.cuboid.half_extent = self.half_extent.get();
    }
}

impl ShapeWidget for CuboidWidget {
    fn get(&self) -> Shape {
        Shape::Cuboid(self.cuboid)
    }

    fn changed(&self) -> bool {
        self.half_extent.changed()
    }

    fn changing(&self) -> bool {
        self.half_extent.changing()
    }
}

struct CylinderWidget {
    radius: DragValue<f32>,
    half_height: DragValue<f32>,
    cylinder: Cylinder,
}

impl CylinderWidget {
    fn new(cylinder: Cylinder) -> Self {
        let mut half_height = DragValue::default();
        half_height.set(cylinder.half_height);
        let mut radius = DragValue::default();
        radius.set(cylinder.radius);

        Self {
            cylinder,
            half_height,
            radius,
        }
    }
}

impl Widget for CylinderWidget {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Half Height");
        self.half_height.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Radius");
        self.radius.ui(ui, context, engine);
        ui.end_row();

        self.cylinder.half_height = self.half_height.get();
        self.cylinder.radius = self.radius.get();
    }
}

impl ShapeWidget for CylinderWidget {
    fn get(&self) -> Shape {
        Shape::Cylinder(self.cylinder)
    }

    fn changed(&self) -> bool {
        self.half_height.changed() || self.radius.changed()
    }

    fn changing(&self) -> bool {
        self.half_height.changing() || self.radius.changing()
    }
}

struct CapsuleWidget {
    radius: DragValue<f32>,
    half_height: DragValue<f32>,
    cylinder: Capsule,
}

impl CapsuleWidget {
    fn new(cylinder: Capsule) -> Self {
        let mut half_height = DragValue::default();
        half_height.set(cylinder.half_height);
        let mut radius = DragValue::default();
        radius.set(cylinder.radius);

        Self {
            cylinder,
            half_height,
            radius,
        }
    }
}

impl Widget for CapsuleWidget {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Half Height");
        self.half_height.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Radius");
        self.radius.ui(ui, context, engine);
        ui.end_row();

        self.cylinder.half_height = self.half_height.get();
        self.cylinder.radius = self.radius.get();
    }
}

impl ShapeWidget for CapsuleWidget {
    fn get(&self) -> Shape {
        Shape::Capsule(self.cylinder)
    }

    fn changed(&self) -> bool {
        self.half_height.changed() || self.radius.changed()
    }

    fn changing(&self) -> bool {
        self.half_height.changing() || self.radius.changing()
    }
}

struct MeshWidget {
    mesh: AssetSlot<Mesh>,
}

impl MeshWidget {
    fn new(graphics_context: Arc<GraphicsContext>, assets: Arc<Assets>, path: PathBuf) -> Self {
        let mut mesh = AssetSlot::new(graphics_context.clone(), assets.clone());
        mesh.set(assets.read(graphics_context, path).unwrap_or_default());

        Self { mesh }
    }
}

impl Widget for MeshWidget {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Mesh");
        self.mesh.ui(ui, context, engine);
        ui.end_row();
    }
}

impl ShapeWidget for MeshWidget {
    fn get(&self) -> Shape {
        Shape::Mesh(irradiance_runtime::physics::shape::Mesh::new(
            self.mesh.get().path().unwrap_or_default(),
        ))
    }

    fn changed(&self) -> bool {
        self.mesh.changed()
    }

    fn changing(&self) -> bool {
        self.mesh.changed()
    }
}
