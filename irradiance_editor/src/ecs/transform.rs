use std::sync::Arc;

use egui::Ui;
use irradiance_runtime::{
    ecs::{Entities, Transform},
    math::{Quat, Vec3},
};

use crate::gui::message::{GizmoChange, Recv};
use crate::{
    core::EditorEngine,
    ecs::EditorComponentExt,
    gui::{
        widgets::{QuatValue, VecValue, Widget},
        Action, EditorContext, Icon, Icons,
    },
};

struct TransformInspector {
    scale: VecValue<3>,
    orientation: QuatValue,
    position: VecValue<3>,
    entities: Arc<Entities>,
    entity: String,
}

impl TransformInspector {
    fn new(entity: String, entities: Arc<Entities>) -> Self {
        let transform = entities
            .get(&entity)
            .expect("entity")
            .get::<Transform>()
            .expect("transform component");

        let mut position = VecValue::default();
        position.set(transform.position());
        let mut orientation = QuatValue::default();
        orientation.set(transform.orientation());
        let mut scale = VecValue::default();
        scale.set(transform.scale());

        Self {
            entity,
            entities,
            position,
            orientation,
            scale,
        }
    }
}

impl Widget for TransformInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, _engine: &mut EditorEngine) {
        if let Some(GizmoChange::Position(position)) = context.message_bus_mut().recv().cloned() {
            self.position.set_changing(position);
        }
        if let Some(GizmoChange::Orientation(orientation)) =
            context.message_bus_mut().recv().cloned()
        {
            self.orientation.set_changing(orientation);
        }
        if let Some(GizmoChange::Scale(scale)) = context.message_bus_mut().recv().cloned() {
            self.scale.set_changing(scale);
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Position");
        self.position.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Orientation");
        self.orientation.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Scale");
        self.scale.ui(ui, context, engine);
        ui.end_row();

        if self.position.changing() {
            self.entities
                .get(self.entity.clone())
                .expect("entity")
                .get_mut::<Transform>()
                .expect("transform component")
                .set_position(self.position.get());
            unsafe {
                self.entities.commit();
            }
        }
        if self.position.changed() {
            context.actions_mut().push(
                engine,
                ChangePosition::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.position.get_old(),
                    self.position.get(),
                ),
            );
        }

        if self.orientation.changing() {
            self.entities
                .get(self.entity.clone())
                .expect("entity")
                .get_mut::<Transform>()
                .expect("transform component")
                .set_orientation(self.orientation.get());
            unsafe {
                self.entities.commit();
            }
        }
        if self.orientation.changed() {
            context.actions_mut().push(
                engine,
                ChangeOrientation::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.orientation.get_old(),
                    self.orientation.get(),
                ),
            );
        }

        if self.scale.changing() {
            self.entities
                .get(self.entity.clone())
                .expect("entity")
                .get_mut::<Transform>()
                .expect("transform component")
                .set_scale(self.scale.get());
            unsafe {
                self.entities.commit();
            }
        }
        if self.scale.changed() {
            context.actions_mut().push(
                engine,
                ChangeScale::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.scale.get_old(),
                    self.scale.get(),
                ),
            );
        }
    }
}

impl EditorComponentExt for Transform {
    fn display_name() -> &'static str {
        "Transform"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::arrows_up_down_left_right())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(TransformInspector::new(entity, entities)))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }
}

struct ChangePosition {
    new: Vec3,
    old: Vec3,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangePosition {
    fn new(entity: String, entities: Arc<Entities>, old: Vec3, new: Vec3) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, position: Vec3) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Transform>()
            .expect("transform component")
            .set_position(position);
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangePosition {
    fn description(&self) -> String {
        "Change Position of Transform Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeOrientation {
    new: Quat,
    old: Quat,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeOrientation {
    fn new(entity: String, entities: Arc<Entities>, old: Quat, new: Quat) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, orientation: Quat) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Transform>()
            .expect("transform component")
            .set_orientation(orientation);
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeOrientation {
    fn description(&self) -> String {
        "Change Orientation of Transform Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeScale {
    new: Vec3,
    old: Vec3,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeScale {
    fn new(entity: String, entities: Arc<Entities>, old: Vec3, new: Vec3) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, scale: Vec3) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Transform>()
            .expect("transform component")
            .set_scale(scale);
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeScale {
    fn description(&self) -> String {
        "Change Scale of Transform Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}
