use std::path::Path;
use std::sync::Arc;

use irradiance_runtime::{ecs::Entities, math::Vec3, rendering::PointLight};
use irradiance_runtime::asset::{Asset, AssetRef};
use irradiance_runtime::core::Engine;
use irradiance_runtime::math::Vec4;
use irradiance_runtime::rendering::{Material, Mesh};

use crate::{
    core::EditorEngine,
    ecs::EditorComponentExt,
    gui::{
        Action,
        EditorContext, Icon, Icons, widgets::{ColorPicker, Widget},
    },
};
use crate::gui::widgets::DragValue;

struct PointLightInspector {
    intensity: DragValue<f32>,
    color: ColorPicker,
    entities: Arc<Entities>,
    entity: String,
}

impl PointLightInspector {
    fn new(entity: String, entities: Arc<Entities>) -> Self {
        let directional_light = entities
            .get(&entity)
            .expect("entity")
            .get::<PointLight>()
            .expect("point light component");
        let mut color = ColorPicker::default();
        color.set(directional_light.color().into());
        let mut intensity = DragValue::default();
        intensity.set(directional_light.intensity());

        Self {
            entity,
            entities,
            color,
            intensity,
        }
    }
}

impl Widget for PointLightInspector {
    fn ui(&mut self, ui: &mut egui::Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Color");
        self.color.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Intensity");
        self.intensity.ui(ui, context, engine);
        ui.end_row();

        if self.color.changing() {
            self.entities
                .get(self.entity.clone())
                .expect("entity")
                .get_mut::<PointLight>()
                .expect("point light")
                .set_color(self.color.get().into());
        }
        if self.color.changed() {
            context.actions_mut().push(
                engine,
                ChangeColor::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.color.get_old().into(),
                    self.color.get().into(),
                ),
            );
        }

        if self.intensity.changing() {
            self.entities
                .get(self.entity.clone())
                .expect("entity")
                .get_mut::<PointLight>()
                .expect("point light")
                .set_intensity(self.intensity.get());
        }
        if self.intensity.changed() {
            context.actions_mut().push(
                engine,
                ChangeIntensity::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.intensity.get_old(),
                    self.intensity.get(),
                ),
            );
        }
    }
}

impl EditorComponentExt for PointLight {
    fn display_name() -> &'static str {
        "Point Light"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::lightbulb())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(PointLightInspector::new(entity, entities)))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }

    fn display(engine: &EditorEngine) -> Option<Mesh> {
        let mut display = Mesh::try_from_bytes(
            engine.assets().clone(),
            engine.graphics_context().clone(),
            Path::new("__internal__"),
            include_bytes!("light_bulb.gltf").to_vec(),
        )
            .expect("mesh");
        let blue = Material::builder()
            .base_color(Vec4::new(0.7259109616279602, 0.8000000715255737, 0.0, 1.0))
            .build();
        let black = Material::builder()
            .base_color(Vec4::new(
                0.26676008105278015,
                0.26676008105278015,
                0.26676008105278015,
                1.0,
            ))
            .build();
        display.sub_meshes_mut()[0].set_material(AssetRef::new_pre_read(Ok(blue)));
        display.sub_meshes_mut()[1].set_material(AssetRef::new_pre_read(Ok(black)));
        Some(display)
    }
}

struct ChangeColor {
    new: Vec3,
    old: Vec3,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeColor {
    fn new(entity: String, entities: Arc<Entities>, old: Vec3, new: Vec3) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, color: Vec3) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<PointLight>()
            .expect("point light component")
            .set_color(color);
    }
}

impl Action for ChangeColor {
    fn description(&self) -> String {
        "Change Color of Point Light Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeIntensity {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeIntensity {
    fn new(entity: String, entities: Arc<Entities>, old: f32, new: f32) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, intensity: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<PointLight>()
            .expect("point light component")
            .set_intensity(intensity);
    }
}

impl Action for ChangeIntensity {
    fn description(&self) -> String {
        "Change Intensity of Point Light Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}
