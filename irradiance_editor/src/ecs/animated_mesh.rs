use std::sync::Arc;

use irradiance_runtime::rendering::{AnimatedMesh, SkinnedMesh};
use irradiance_runtime::{asset::AssetRef, core::Engine, ecs::Entities};

use crate::{
    core::EditorEngine,
    ecs::EditorComponentExt,
    gui::{
        widgets::{AssetSlot, Widget},
        Action, EditorContext, Icon, Icons,
    },
};

struct AnimatedMeshInspector {
    mesh: AssetSlot<SkinnedMesh>,
    entities: Arc<Entities>,
    entity: String,
}

impl AnimatedMeshInspector {
    fn new(entity: String, entities: Arc<Entities>, engine: &EditorEngine) -> Self {
        let mut mesh = AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());

        mesh.set(
            entities
                .get(&entity)
                .expect("entity")
                .get::<AnimatedMesh>()
                .expect("animated mesh component")
                .mesh()
                .clone(),
        );

        Self {
            mesh,
            entities,
            entity,
        }
    }
}

impl Widget for AnimatedMeshInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.mesh.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut egui::Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Mesh");
        self.mesh.ui(ui, context, engine);
        ui.end_row();

        if self.mesh.changed() {
            context.actions_mut().push(
                engine,
                ChangeMesh::new(self.entity.clone(), self.entities.clone(), self.mesh.get()),
            );
        }
    }
}

impl EditorComponentExt for AnimatedMesh {
    fn display_name() -> &'static str {
        "Animated Mesh"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::cube())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(AnimatedMeshInspector::new(
            entity, entities, engine,
        )))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }
}

struct ChangeMesh {
    new: AssetRef<SkinnedMesh>,
    old: AssetRef<SkinnedMesh>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeMesh {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<SkinnedMesh>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<AnimatedMesh>()
                .expect("animated mesh component")
                .mesh()
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, mesh: AssetRef<SkinnedMesh>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<AnimatedMesh>()
            .expect("animated mesh component")
            .set_mesh(mesh);
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeMesh {
    fn description(&self) -> String {
        "Change Mesh of Animated Mesh Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}
