use std::sync::Arc;

use egui::Ui;

use irradiance_runtime::core::Engine;
use irradiance_runtime::ecs::{Entities, Ref};
use irradiance_runtime::gfx::GraphicsContext;
use irradiance_runtime::rendering::OmnidirectionalShadow;

use crate::core::EditorEngine;
use crate::ecs::EditorComponentExt;
use crate::gui::widgets::{ComboBox, Widget};
use crate::gui::{Action, EditorContext, Icon, Icons};

struct OmnidirectionalShadowInspector {
    resolution: ComboBox<u32>,
    entities: Arc<Entities>,
    entity: String,
}

impl OmnidirectionalShadowInspector {
    fn new(entity: String, entities: Arc<Entities>) -> Self {
        let omnidirectional_shadow = entities
            .get(&entity)
            .expect("entity")
            .get::<OmnidirectionalShadow>()
            .expect("omnidirectional shadow component");
        let mut resolution = ComboBox::builder()
            .value(256)
            .value(512)
            .value(1024)
            .value(2048)
            .value(4096)
            .value(8192)
            .build();
        resolution.set(omnidirectional_shadow.image_view().image().extent().width);

        Self {
            entity,
            entities,
            resolution,
        }
    }
}

impl Widget for OmnidirectionalShadowInspector {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Resolution");
        self.resolution.ui(ui, context, engine);
        ui.end_row();

        if self.resolution.changed() {
            context.actions_mut().push(
                engine,
                ChangeResolution::new(
                    engine.graphics_context().clone(),
                    self.entity.clone(),
                    self.entities.clone(),
                    self.component().image_view().image().extent().width,
                    self.resolution.get(),
                ),
            );
        }
    }
}

impl OmnidirectionalShadowInspector {
    fn component(&self) -> Ref<'_, OmnidirectionalShadow> {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get::<OmnidirectionalShadow>()
            .expect("omnidirectional shadow component")
    }
}

impl EditorComponentExt for OmnidirectionalShadow {
    fn display_name() -> &'static str {
        "Omnidirectional Shadow"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::cloud_sun())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(OmnidirectionalShadowInspector::new(
            entity, entities,
        )))
    }

    fn create(engine: &EditorEngine) -> Self {
        Self::new(engine.graphics_context(), 1024).expect("omnidirectional shadow")
    }
}

struct ChangeResolution {
    new: u32,
    old: u32,
    entities: Arc<Entities>,
    entity: String,
    graphics_context: Arc<GraphicsContext>,
}

impl ChangeResolution {
    fn new(
        graphics_context: Arc<GraphicsContext>,
        entity: String,
        entities: Arc<Entities>,
        old: u32,
        new: u32,
    ) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
            graphics_context,
        }
    }

    fn replace(&self, resolution: u32) {
        let entity = self.entities.get(&self.entity).expect("entity");
        entity.insert(
            OmnidirectionalShadow::new(&self.graphics_context, resolution)
                .expect("omnidirectional shadow"),
        );
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeResolution {
    fn description(&self) -> String {
        "Change Resolution of Omnidirectional Shadow Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}
