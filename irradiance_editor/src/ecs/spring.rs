use std::sync::Arc;

use egui::Ui;

use irradiance_runtime::ecs::{Entities, RefMut, Spring};
use irradiance_runtime::math::Vec3;

use crate::core::EditorEngine;
use crate::ecs::EditorComponentExt;
use crate::gui::widgets::{DragValue, TextEdit, VecValue, Widget};
use crate::gui::{Action, EditorContext, Icon};

struct SpringInspector {
    dampening: DragValue<f32>,
    distance: DragValue<f32>,
    stiffness: DragValue<f32>,
    mass: DragValue<f32>,
    offset: VecValue<3>,
    target: TextEdit,
    entities: Arc<Entities>,
    entity: String,
}

impl SpringInspector {
    fn new(entity: String, entities: Arc<Entities>) -> Self {
        let spring = entities
            .get(&entity)
            .expect("entity")
            .get::<Spring>()
            .expect("spring component");

        let mut target = TextEdit::default();
        target.set(spring.target.clone());
        let mut offset = VecValue::default();
        offset.set(spring.offset);
        let mut mass = DragValue::default();
        mass.set(spring.mass);
        let mut stiffness = DragValue::default();
        stiffness.set(spring.stiffness);
        let mut distance = DragValue::default();
        distance.set(spring.distance);
        let mut dampening = DragValue::default();
        dampening.set(spring.dampening);

        Self {
            dampening,
            distance,
            stiffness,
            mass,
            offset,
            target,
            entities,
            entity,
        }
    }

    fn get_mut(&self) -> RefMut<'_, Spring> {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut()
            .expect("spring component")
    }
}

impl Widget for SpringInspector {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Target");
        self.target.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Offset");
        self.offset.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Mass");
        self.mass.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Stiffness");
        self.stiffness.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Distance");
        self.distance.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Dampening");
        self.dampening.ui(ui, context, engine);
        ui.end_row();

        if self.target.changing() {
            self.get_mut().target = self.target.get().clone();
            unsafe {
                self.entities.commit();
            }
        }
        if self.target.changed() {
            context.actions_mut().push(
                engine,
                ChangeTarget::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.target.get_old().clone(),
                    self.target.get().clone(),
                ),
            );
        }

        if self.offset.changing() {
            self.get_mut().offset = self.offset.get();
            unsafe {
                self.entities.commit();
            }
        }
        if self.offset.changed() {
            context.actions_mut().push(
                engine,
                ChangeOffset::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.offset.get_old(),
                    self.offset.get(),
                ),
            );
        }

        if self.mass.changing() {
            self.get_mut().mass = self.mass.get();
            unsafe {
                self.entities.commit();
            }
        }
        if self.mass.changed() {
            context.actions_mut().push(
                engine,
                ChangeMass::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.mass.get_old(),
                    self.mass.get(),
                ),
            );
        }

        if self.stiffness.changing() {
            self.get_mut().stiffness = self.stiffness.get();
            unsafe {
                self.entities.commit();
            }
        }
        if self.stiffness.changed() {
            context.actions_mut().push(
                engine,
                ChangeStiffness::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.stiffness.get_old(),
                    self.stiffness.get(),
                ),
            );
        }

        if self.distance.changing() {
            self.get_mut().distance = self.distance.get();
            unsafe {
                self.entities.commit();
            }
        }
        if self.distance.changed() {
            context.actions_mut().push(
                engine,
                ChangeDistance::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.distance.get_old(),
                    self.distance.get(),
                ),
            );
        }

        if self.dampening.changing() {
            self.get_mut().dampening = self.dampening.get();
            unsafe {
                self.entities.commit();
            }
        }
        if self.dampening.changed() {
            context.actions_mut().push(
                engine,
                ChangeDampening::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.dampening.get_old(),
                    self.dampening.get(),
                ),
            );
        }
    }
}

impl EditorComponentExt for Spring {
    fn display_name() -> &'static str {
        "Spring"
    }

    fn icon() -> Option<Icon> {
        None
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        _engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(SpringInspector::new(entity, entities)))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }
}

struct ChangeTarget {
    new: String,
    old: String,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeTarget {
    fn new(entity: String, entities: Arc<Entities>, old: String, new: String) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: String) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Spring>()
            .expect("spring component")
            .target = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeTarget {
    fn description(&self) -> String {
        "Change Target".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeOffset {
    new: Vec3,
    old: Vec3,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeOffset {
    fn new(entity: String, entities: Arc<Entities>, old: Vec3, new: Vec3) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: Vec3) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Spring>()
            .expect("spring component")
            .offset = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeOffset {
    fn description(&self) -> String {
        "Change Offset".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeMass {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeMass {
    fn new(entity: String, entities: Arc<Entities>, old: f32, new: f32) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Spring>()
            .expect("spring component")
            .mass = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeMass {
    fn description(&self) -> String {
        "Change Mass".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeStiffness {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeStiffness {
    fn new(entity: String, entities: Arc<Entities>, old: f32, new: f32) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Spring>()
            .expect("spring component")
            .stiffness = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeStiffness {
    fn description(&self) -> String {
        "Change Stiffness".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeDistance {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeDistance {
    fn new(entity: String, entities: Arc<Entities>, old: f32, new: f32) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Spring>()
            .expect("spring component")
            .distance = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeDistance {
    fn description(&self) -> String {
        "Change Distance".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeDampening {
    new: f32,
    old: f32,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeDampening {
    fn new(entity: String, entities: Arc<Entities>, old: f32, new: f32) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: f32) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<Spring>()
            .expect("spring component")
            .dampening = value;
        unsafe {
            self.entities.commit();
        }
    }
}

impl Action for ChangeDampening {
    fn description(&self) -> String {
        "Change Dampening".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}
