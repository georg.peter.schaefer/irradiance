use std::sync::Arc;

use egui::Ui;

use irradiance_runtime::ecs::Entities;
use irradiance_runtime::math::Vec3;
use irradiance_runtime::physics::shape::Ball;
use irradiance_runtime::physics::{RigidBody, RigidBodyType, Shape};

use crate::core::EditorEngine;
use crate::ecs::shape::ShapeInspector;
use crate::ecs::EditorComponentExt;
use crate::gui::widgets::{Checkbox, ComboBox, VecValue, Widget};
use crate::gui::{Action, EditorContext, Icon, Icons};

struct RigidBodyInspector {
    offset: VecValue<3>,
    sensor: Checkbox,
    shape_inspector: ShapeInspector,
    body_type: ComboBox<RigidBodyType>,
    entities: Arc<Entities>,
    entity: String,
}

impl RigidBodyInspector {
    fn new(engine: &EditorEngine, entity: String, entities: Arc<Entities>) -> Self {
        let rigid_body = entities
            .get(&entity)
            .and_then(|entity| entity.get::<RigidBody>())
            .expect("rigid body");
        let mut body_type = ComboBox::builder()
            .value(RigidBodyType::Dynamic)
            .value(RigidBodyType::Fixed)
            .value(RigidBodyType::Kinematic)
            .build();
        body_type.set(rigid_body.body_type);
        let mut sensor = Checkbox::default();
        sensor.set(rigid_body.sensor);
        let mut offset = VecValue::default();
        offset.set(rigid_body.offset);

        Self {
            offset,
            sensor,
            shape_inspector: ShapeInspector::new(engine, rigid_body.shape.clone()),
            body_type,
            entity,
            entities,
        }
    }
}

impl Widget for RigidBodyInspector {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Type");
        self.body_type.ui(ui, context, engine);
        ui.end_row();

        self.shape_inspector.ui(ui, context, engine);

        ui.label("    Sensor");
        self.sensor.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Offset");
        self.offset.ui(ui, context, engine);
        ui.end_row();

        if self.body_type.changed() {
            context.actions_mut().push(
                engine,
                ChangeBodyType::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.entities
                        .get(&self.entity)
                        .and_then(|entity| entity.get::<RigidBody>())
                        .expect("rigid body")
                        .body_type,
                    self.body_type.get(),
                ),
            );
        }

        if self.shape_inspector.changing() {
            self.entities
                .get(&self.entity)
                .and_then(|entity| entity.get_mut::<RigidBody>())
                .expect("rigid body")
                .shape = self.shape_inspector.get().clone();
        }
        if self.shape_inspector.changed() {
            context.actions_mut().push(
                engine,
                ChangeShape::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.shape_inspector.get_old(),
                    self.shape_inspector.get(),
                ),
            );
        }

        if self.sensor.changed() {
            context.actions_mut().push(
                engine,
                ChangeSensor::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    !self.sensor.get(),
                    self.sensor.get(),
                ),
            );
        }

        if self.offset.changing() {
            self.entities
                .get(&self.entity)
                .and_then(|entity| entity.get_mut::<RigidBody>())
                .expect("rigid body")
                .offset = self.offset.get();
        }
        if self.offset.changed() {
            context.actions_mut().push(
                engine,
                ChangeOffset::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.offset.get_old(),
                    self.offset.get(),
                ),
            );
        }
    }
}

impl EditorComponentExt for RigidBody {
    fn display_name() -> &'static str {
        "Rigid Body"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::cubes_stacked())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(RigidBodyInspector::new(engine, entity, entities)))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::new(Shape::Ball(Ball::default()))
    }
}

struct ChangeShape {
    new: Shape,
    old: Shape,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeShape {
    fn new(entity: String, entities: Arc<Entities>, old: Shape, new: Shape) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, shape: Shape) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<RigidBody>()
            .expect("rigid body component")
            .shape = shape;
    }
}

impl Action for ChangeShape {
    fn description(&self) -> String {
        "Change Shape of Rigid Body Component".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeOffset {
    new: Vec3,
    old: Vec3,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeOffset {
    fn new(entity: String, entities: Arc<Entities>, old: Vec3, new: Vec3) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: Vec3) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<RigidBody>()
            .expect("rigid body component")
            .offset = value;
    }
}

impl Action for ChangeOffset {
    fn description(&self) -> String {
        "Change Offset of Rigid Body Component".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeBodyType {
    new: RigidBodyType,
    old: RigidBodyType,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeBodyType {
    fn new(
        entity: String,
        entities: Arc<Entities>,
        old: RigidBodyType,
        new: RigidBodyType,
    ) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: RigidBodyType) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<RigidBody>()
            .expect("rigid body component")
            .body_type = value;
    }
}

impl Action for ChangeBodyType {
    fn description(&self) -> String {
        "Change Type of Rigid Body Component".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeSensor {
    new: bool,
    old: bool,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeSensor {
    fn new(entity: String, entities: Arc<Entities>, old: bool, new: bool) -> Self {
        Self {
            new,
            old,
            entities,
            entity,
        }
    }

    fn replace(&self, value: bool) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<RigidBody>()
            .expect("rigid body component")
            .sensor = value;
    }
}

impl Action for ChangeSensor {
    fn description(&self) -> String {
        "Change Sensor of Rigid Body Component".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}
