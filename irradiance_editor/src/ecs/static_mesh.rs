use std::sync::Arc;

use irradiance_runtime::{
    asset::AssetRef,
    core::Engine,
    ecs::Entities,
    rendering::{Mesh, StaticMesh},
};

use crate::gui::widgets::Checkbox;
use crate::{
    core::EditorEngine,
    ecs::EditorComponentExt,
    gui::{
        widgets::{AssetSlot, Widget},
        Action, EditorContext, Icon, Icons,
    },
};

struct StaticMeshInspector {
    disable_shadow: Checkbox,
    mesh: AssetSlot<Mesh>,
    entities: Arc<Entities>,
    entity: String,
}

impl StaticMeshInspector {
    fn new(entity: String, entities: Arc<Entities>, engine: &EditorEngine) -> Self {
        let static_mesh = entities
            .get(&entity)
            .expect("entity")
            .get::<StaticMesh>()
            .expect("static mesh component");
        let mut mesh = AssetSlot::new(engine.graphics_context().clone(), engine.assets().clone());
        let mut disable_shadow = Checkbox::default();

        mesh.set(static_mesh.mesh().clone());
        disable_shadow.set(static_mesh.disable_shadow);

        Self {
            disable_shadow,
            mesh,
            entities,
            entity,
        }
    }
}

impl Widget for StaticMeshInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.mesh.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut egui::Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("    Mesh");
        self.mesh.ui(ui, context, engine);
        ui.end_row();

        ui.label("    Disable Shadow");
        self.disable_shadow.ui(ui, context, engine);
        ui.end_row();

        if self.mesh.changed() {
            context.actions_mut().push(
                engine,
                ChangeMesh::new(self.entity.clone(), self.entities.clone(), self.mesh.get()),
            );
        }

        if self.disable_shadow.changed() {
            context.actions_mut().push(
                engine,
                ChangeDisableShadow::new(
                    self.entity.clone(),
                    self.entities.clone(),
                    self.disable_shadow.get(),
                ),
            );
        }
    }
}

impl EditorComponentExt for StaticMesh {
    fn display_name() -> &'static str {
        "Static Mesh"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::cube())
    }

    fn inspect(
        entity: String,
        entities: Arc<Entities>,
        engine: &EditorEngine,
    ) -> Option<Box<dyn Widget>> {
        Some(Box::new(StaticMeshInspector::new(entity, entities, engine)))
    }

    fn create(_engine: &EditorEngine) -> Self {
        Self::default()
    }
}

struct ChangeMesh {
    new: AssetRef<Mesh>,
    old: AssetRef<Mesh>,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeMesh {
    fn new(entity: String, entities: Arc<Entities>, new: AssetRef<Mesh>) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<StaticMesh>()
                .expect("static mesh component")
                .mesh()
                .clone(),
            entities,
            entity,
        }
    }

    fn replace(&self, mesh: AssetRef<Mesh>) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<StaticMesh>()
            .expect("static mesh component")
            .set_mesh(mesh);
    }
}

impl Action for ChangeMesh {
    fn description(&self) -> String {
        "Change Mesh of Static Mesh Component".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeDisableShadow {
    new: bool,
    old: bool,
    entities: Arc<Entities>,
    entity: String,
}

impl ChangeDisableShadow {
    fn new(entity: String, entities: Arc<Entities>, new: bool) -> Self {
        Self {
            new,
            old: entities
                .get(&entity)
                .expect("entity")
                .get::<StaticMesh>()
                .expect("static mesh component")
                .disable_shadow,
            entities,
            entity,
        }
    }

    fn replace(&self, value: bool) {
        self.entities
            .get(&self.entity)
            .expect("entity")
            .get_mut::<StaticMesh>()
            .expect("static mesh component")
            .disable_shadow = value;
    }
}

impl Action for ChangeDisableShadow {
    fn description(&self) -> String {
        "Change Disable Shadow".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}
