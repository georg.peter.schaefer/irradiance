use std::path::PathBuf;

use egui::{Context, Key, Modifiers, Pos2};
use irradiance_runtime::{
    core::{Engine, Frame},
    winit::event::Event,
    winit::event::WindowEvent,
};

use crate::gui::widgets::dialog::DuplicateDialog;
use crate::{asset::EditorAssetTypesBuilder, core::EditorEngine, ecs::EditorComponentTypesBuilder};

use super::{
    message::{Close, CloseDialog, Delete, Message, OpenDeleteDialog, OpenDialog, Recv, Repaint},
    widgets::{
        dialog::{ConfirmDialog, CreateDialog, Dialog, RenameDialog},
        BottomPanel, CentralPanel, LeftPanel, MenuBar, RightPanel, Widget,
    },
    EditorContext, GuiContext,
};

/// The editor gui.
pub struct Gui {
    dialog: Option<Box<dyn Dialog>>,
    bottom_panel: BottomPanel,
    right_panel: RightPanel,
    central_panel: CentralPanel,
    left_panel: LeftPanel,
    menu_bar: MenuBar,
    pub(crate) editor_context: EditorContext,
    gui_context: GuiContext,
}

impl Gui {
    pub fn new(
        engine: &EditorEngine,
        asset_path: PathBuf,
        asset_types: EditorAssetTypesBuilder,
        component_types: EditorComponentTypesBuilder,
    ) -> Self {
        let mut editor_context =
            EditorContext::new(engine, asset_path.clone(), asset_types, component_types);
        let menu_bar = MenuBar::new(&editor_context);
        let left_panel = LeftPanel::new(&mut editor_context, engine);
        let central_panel = CentralPanel::new(&mut editor_context, engine);
        let bottom_panel = BottomPanel::new(&mut editor_context, asset_path);

        Self {
            gui_context: GuiContext::new(
                engine.window().clone(),
                engine.graphics_context().clone(),
            ),
            editor_context,
            menu_bar,
            left_panel,
            central_panel,
            right_panel: RightPanel::default(),
            bottom_panel,
            dialog: None,
        }
    }

    pub fn on_event(&mut self, event: &Event<()>) {
        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                if self.editor_context.actions().has_pending_changes() {
                    self.editor_context
                        .message_bus_mut()
                        .post(Message::OpenDialog(OpenDialog::Close))
                } else {
                    self.editor_context
                        .message_bus_mut()
                        .post(Message::Close(Close))
                }
            }
            Event::WindowEvent { event, .. } => {
                self.gui_context.on_event(event);
            }
            _ => {}
        }
    }

    pub fn begin_screen_frame(&mut self, frame: &mut Frame) {
        self.central_panel.begin_screen_frame(frame);
    }

    pub fn end_screen_frame(&mut self, frame: &mut Frame) {
        self.central_panel.end_screen_frame(frame);
    }

    pub fn update(&mut self, engine: &mut EditorEngine, frame: &mut Frame) {
        if self.gui_context.ctx.input().key_pressed(Key::End) {
            self.gui_context
                .ctx
                .set_debug_on_hover(!self.gui_context.ctx.debug_on_hover());
        }

        self.editor_context.message_bus_mut().swap();
        engine
            .window()
            .set_unsaved(self.editor_context.actions().has_pending_changes());
        self.handle_editor_shortcuts(engine);
        self.handle_messages(engine);
        self.menu_bar
            .handle_messages(&mut self.editor_context, engine);
        self.left_panel
            .handle_messages(&mut self.editor_context, engine);
        self.central_panel
            .handle_messages(&mut self.editor_context, engine);
        self.right_panel
            .handle_messages(&mut self.editor_context, engine);
        self.bottom_panel
            .handle_messages(&mut self.editor_context, engine);

        self.gui_context
            .update(&mut self.editor_context, frame, |ctx, editor_context| {
                egui::CentralPanel::default()
                    .frame(egui::Frame::default())
                    .show(ctx, |ui| {
                        ui.set_enabled(self.dialog.is_none());

                        self.bottom_panel.ui(ui, editor_context, engine);
                        self.menu_bar.ui(ui, editor_context, engine);
                        self.left_panel.ui(ui, editor_context, engine);
                        self.right_panel.ui(ui, editor_context, engine);
                        self.central_panel.ui(ui, editor_context, engine);
                    });

                Self::handle_dialog(ctx, editor_context, engine, &mut self.dialog);
            });
    }

    fn handle_editor_shortcuts(&mut self, engine: &mut EditorEngine) {
        if self.editor_context.is_playing() {
            return;
        }

        let mut input = self.gui_context.ctx.input_mut();

        if input.consume_key(Modifiers::CTRL, Key::S)
            && self.editor_context.actions().has_pending_changes()
        {
            engine.assets().write_all().expect("write unsaved changes");
            engine.scenes().save();
            engine.input().save();
            self.editor_context.actions_mut().clear_pending_changes();
        }
        if input.consume_key(Modifiers::CTRL, Key::Z) {
            self.editor_context.actions_mut().undo(engine);
            self.editor_context
                .message_bus_mut()
                .post(Message::Repaint(Repaint::All));
        }
        if input.consume_key(Modifiers::CTRL | Modifiers::SHIFT, Key::Z) {
            self.editor_context.actions_mut().redo(engine);
            self.editor_context
                .message_bus_mut()
                .post(Message::Repaint(Repaint::All));
        }
    }

    fn handle_messages(&mut self, engine: &mut EditorEngine) {
        match self.editor_context.message_bus().recv() {
            Some(OpenDialog::Create(create)) => {
                self.dialog = Some(CreateDialog::new(&self.editor_context, create.clone()))
            }
            Some(OpenDialog::Delete(OpenDeleteDialog::Asset(path))) => {
                self.dialog = Some(ConfirmDialog::new(
                    "Delete Asset",
                    "Are you sure you want to delete this asset?",
                    Message::Delete(Delete::Asset(path.clone())),
                ));
            }
            Some(OpenDialog::Delete(OpenDeleteDialog::Directory(path))) => {
                self.dialog = Some(ConfirmDialog::new(
                    "Delete Directory",
                    "Are you sure you want to delete this directory?",
                    Message::Delete(Delete::Directory(path.clone())),
                ));
            }
            Some(OpenDialog::Delete(OpenDeleteDialog::Component(
                component_type,
                entity,
                entities,
            ))) => {
                self.dialog = Some(ConfirmDialog::new(
                    "Delete Component",
                    "Are you sure you want to delete this component?",
                    Message::Delete(Delete::Component(
                        *component_type,
                        entity.clone(),
                        entities.clone(),
                    )),
                ));
            }
            Some(OpenDialog::Delete(OpenDeleteDialog::Entity(entity))) => {
                self.dialog = Some(ConfirmDialog::new(
                    "Delete Entity",
                    "Are you sure you want to delete this entity?",
                    Message::Delete(Delete::Entity(entity.clone())),
                ));
            }
            Some(OpenDialog::Delete(OpenDeleteDialog::ActionMap(name))) => {
                self.dialog = Some(ConfirmDialog::new(
                    "Delete Action Map",
                    "Are you sure you want to delete this action map?",
                    Message::Delete(Delete::ActionMap(name.clone())),
                ));
            }
            Some(OpenDialog::Delete(OpenDeleteDialog::Action(action_map, name))) => {
                self.dialog = Some(ConfirmDialog::new(
                    "Delete Action",
                    "Are you sure you want to delete this action?",
                    Message::Delete(Delete::Action(action_map.clone(), name.clone())),
                ));
            }
            Some(OpenDialog::Delete(OpenDeleteDialog::Binding(action_map, action, binding))) => {
                self.dialog = Some(ConfirmDialog::new(
                    "Delete Binding",
                    "Are you sure you want to delete this binding?",
                    Message::Delete(Delete::Binding(
                        action_map.clone(),
                        action.clone(),
                        binding.clone(),
                    )),
                ));
            }
            Some(OpenDialog::Rename(rename)) => {
                self.dialog = Some(RenameDialog::new(rename.clone()));
            }
            Some(OpenDialog::Duplicate(duplicate)) => {
                self.dialog = Some(DuplicateDialog::new(duplicate.clone()));
            }
            Some(OpenDialog::Close) => {
                self.dialog = Some(ConfirmDialog::new(
                    "Unsaved Changes",
                    "Are you sure you want to discard your changes?",
                    Message::Close(Close),
                ))
            }
            _ => {}
        }
        if let Some(CloseDialog) = self.editor_context.message_bus().recv() {
            self.dialog = None;
        }
        if let Some(Close) = self.editor_context.message_bus().recv() {
            engine.close();
        }
    }

    fn handle_dialog(
        ctx: &Context,
        editor_context: &mut EditorContext,
        engine: &mut EditorEngine,
        dialog: &mut Option<Box<dyn Dialog>>,
    ) {
        if let Some(dialog) = dialog {
            let screen_rect = ctx.input().screen_rect;
            let size = dialog.size();
            let response = egui::Window::new(dialog.title())
                .collapsible(false)
                .auto_sized()
                .fixed_pos(Pos2::new(
                    (screen_rect.width() - size.x) * 0.5,
                    (screen_rect.height() - size.y) * 0.5,
                ))
                .show(ctx, |ui| {
                    dialog.ui(ui, editor_context, engine);
                })
                .expect("response")
                .response;
            dialog.set_size(response.rect.size());
        }
    }
}
