//! Message bus.

use std::{any::TypeId, path::PathBuf, sync::Arc};

use irradiance_runtime::ecs::Entities;
use irradiance_runtime::math::{Quat, Vec3};

/// Message bus.
#[derive(Default)]
pub struct MessageBus {
    source: Vec<Message>,
    sink: Vec<Message>,
}

impl MessageBus {
    /// Posts a message.
    pub fn post(&mut self, message: Message) {
        self.sink.push(message);
    }

    pub(crate) fn swap(&mut self) {
        self.source = self.sink.drain(0..).collect();
    }
}

/// Receive trait.
pub trait Recv<T> {
    /// Receives a message.
    fn recv(&self) -> Option<&T>;
}

macro_rules! impl_recv {
    ($message:tt) => {
        impl Recv<$message> for MessageBus {
            fn recv(&self) -> Option<&$message> {
                self.source
                    .iter()
                    .rfind(|message| matches!(message, Message::$message(_)))
                    .map(|message| {
                        if let Message::$message(payload) = message {
                            Some(payload)
                        } else {
                            None
                        }
                    })
                    .flatten()
            }
        }
    };
}

impl_recv!(Highlight);
impl_recv!(Inspect);
impl_recv!(GizmoChange);
impl_recv!(Repaint);
impl_recv!(OpenDialog);
impl_recv!(CloseDialog);
impl_recv!(Create);
impl_recv!(Delete);
impl_recv!(Rename);
impl_recv!(Duplicate);
impl_recv!(Move);
impl_recv!(Close);
impl_recv!(OpenTab);

/// Messages.
#[derive(Clone)]
pub enum Message {
    /// Highlight an entity.
    Highlight(Highlight),
    /// Inspect something.
    Inspect(Inspect),
    /// Gizmo change.
    GizmoChange(GizmoChange),
    /// Repaint something.
    Repaint(Repaint),
    /// Opens a dialog.
    OpenDialog(OpenDialog),
    /// Closes the current dialog.
    CloseDialog(CloseDialog),
    /// Creates something.
    Create(Create),
    /// Deletes something.
    Delete(Delete),
    /// Renames something.
    Rename(Rename),
    /// Duplicate something.
    Duplicate(Duplicate),
    /// Moves something.
    Move(Move),
    /// Close the editor.
    Close(Close),
    /// Opens a tab.
    OpenTab(OpenTab),
}

/// Highlight message payload.
#[derive(Clone)]
pub enum Highlight {
    /// Highlights an entity.
    Add(String),
    /// Removes any highlight.
    Remove,
}

/// Inspect message payload.
#[derive(Clone)]
pub enum Inspect {
    /// Inspects an entity.
    Entity(String, Arc<Entities>),
    /// Inspect an asset.
    Asset(PathBuf),
    /// Inspect a component.
    Component(&'static str, String, TypeId),
    /// Inspect a screen.
    Screen(&'static str),
}

/// Gizmo change message payload.
#[derive(Clone)]
pub enum GizmoChange {
    /// Position change.
    Position(Vec3),
    /// Orientation change.
    Orientation(Quat),
    /// Scale change.
    Scale(Vec3),
}

/// Repaint message payload.
#[derive(Clone)]
pub enum Repaint {
    /// Repaint an asset.
    Asset(PathBuf),
    /// Repaint all.
    All,
}

/// Open dialog message payload.
#[derive(Clone)]
pub enum OpenDialog {
    /// Open a create dialog.
    Create(OpenCreateDialog),
    /// Open a delete dialog.
    Delete(OpenDeleteDialog),
    /// Open a rename dialog.
    Rename(OpenRenameDialog),
    /// Open a duplicate dialog.
    Duplicate(OpenDuplicateDialog),
    /// Open the close dialog.
    Close,
}

/// Create dialog message payload.
#[derive(Clone)]
pub enum OpenCreateDialog {
    /// Open a create directory dialog.
    Directory,
    /// Open a create asset dialog.
    Asset(&'static str),
    /// Open a create entity dialog.
    Entity(Arc<Entities>),
    /// Open a create action map dialog.
    ActionMap,
    /// Opens a create action dialog.
    Action(String),
    /// Opens a create binding dialog.
    Binding(String, String),
}

/// Delete dialog message payload.
#[derive(Clone)]
pub enum OpenDeleteDialog {
    /// Opens a delete asset dialog.
    Asset(PathBuf),
    /// Opens a delete directory dialog.
    Directory(PathBuf),
    /// Opens a delete entity dialog.
    Entity(String),
    /// Opens a delete component dialog.
    Component(TypeId, String, Arc<Entities>),
    /// Opens a delete action map dialog.
    ActionMap(String),
    /// Opens a delete action dialog.
    Action(String, String),
    /// Opens a delete binding dialog.
    Binding(String, String, String),
}

/// Rename dialog message payload.
#[derive(Clone)]
pub enum OpenRenameDialog {
    /// Opens a rename asset dialog.
    Asset(PathBuf),
    /// OPens a rename directory dialog.
    Directory(PathBuf),
    /// Opens a rename entity dialog.
    Entity(String, Arc<Entities>),
    /// Opens a rename action map dialog.
    ActionMap(String),
    /// Opens a rename action dialog.
    Action(String, String),
    /// Opens a rename binding dialog.
    Binding(String, String, String),
}

/// Duplicate dialog message payload.
#[derive(Clone)]
pub enum OpenDuplicateDialog {
    /// Opens a duplicate entity dialog.
    Entity(String, Arc<Entities>),
}

/// Close dialog message payload.
#[derive(Copy, Clone)]
pub struct CloseDialog;

/// Create message payload.
#[derive(Clone)]
pub enum Create {
    /// Creates a directory.
    Directory(String),
    /// Creates an asset.
    Asset(String, &'static str),
    /// Creates an entity.
    Entity(String),
    /// Creates a component for an entity.
    Component(TypeId, String, Arc<Entities>),
    /// Creates an action map.
    ActionMap(String),
    /// Creates an action.
    Action(String, String),
    /// Creates a binding.
    Binding(String, String, String),
}

/// Delete message payload.
#[derive(Clone)]
pub enum Delete {
    /// Deletes an asset.
    Asset(PathBuf),
    /// Deletes a directory.
    Directory(PathBuf),
    /// Deletes an entity.
    Entity(String),
    /// Deletes a component.
    Component(TypeId, String, Arc<Entities>),
    /// Deletes an action map.
    ActionMap(String),
    /// Deletes an action.
    Action(String, String),
    /// Deletes a binding.
    Binding(String, String, String),
}

/// Rename message payload.
#[derive(Clone)]
pub enum Rename {
    /// Renames an asset.
    Asset(PathBuf, PathBuf),
    /// Renames a directory.
    Directory(PathBuf, PathBuf),
    /// Renames an entity.
    Entity(String, String),
    /// Renames an action map.
    ActionMap(String, String),
    /// Renames an action.
    Action(String, String, String),
    /// Renames a binding.
    Binding(String, String, String, String),
}

/// Duplicate message payload.
#[derive(Clone)]
pub enum Duplicate {
    /// Duplicate an entity.
    Entity(String, String),
}

/// Move message payload.
#[derive(Clone)]
pub enum Move {
    /// Moves an asset.
    Asset(PathBuf, PathBuf),
    /// Moves a directory.
    Directory(PathBuf, PathBuf),
}

/// Close message payload.
#[derive(Copy, Clone)]
pub struct Close;

/// Open tab message payload.
#[derive(Clone)]
pub enum OpenTab {
    /// Opens the input tab.
    Input,
}
