use egui::Ui;

use crate::core::EditorEngine;
use crate::gui::widgets::Widget;
use crate::gui::EditorContext;

/// Checkbox.
#[derive(Default)]
pub struct Checkbox {
    changed: bool,
    value: bool,
}

impl Checkbox {
    /// Returns the value.
    pub fn get(&self) -> bool {
        self.value
    }

    /// Sets the value.
    pub fn set(&mut self, value: bool) {
        self.value = value;
    }

    /// Returns if the value has changed.
    pub fn changed(&self) -> bool {
        self.changed
    }
}

impl Widget for Checkbox {
    fn ui(&mut self, ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
        self.changed = false;

        if ui.checkbox(&mut self.value, "").changed() {
            self.changed = true;
        }
    }
}
