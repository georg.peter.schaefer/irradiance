use std::{
    cell::RefCell,
    marker::{Send, Sync},
    rc::Rc,
};

use egui::{Color32, Id, LayerId, Order, Pos2, Response, Rounding, Sense, Ui};

/// Drag source.
pub struct DragSource<V> {
    start: RefCell<Pos2>,
    drag_value: V,
    id: Id,
}

impl<V: Clone + Send + Sync + 'static> DragSource<V> {
    /// Creates a new drag source.
    pub fn new(id: Id, drag_value: V) -> Rc<Self> {
        Rc::new(Self {
            id,
            drag_value,
            start: Default::default(),
        })
    }

    /// Enables dragging.
    pub fn drag_value(
        &self,
        ui: &mut Ui,
        mut add_content: impl FnMut(&mut Ui) -> Response,
    ) -> Response {
        let is_being_dragged = ui.memory().is_being_dragged(self.id);
        let drag = is_being_dragged && self.is_over_threshold(ui);
        if !drag {
            let response = add_content(ui);
            let response = ui.interact(response.rect, self.id, Sense::click_and_drag());
            if response.drag_started() {
                ui.data()
                    .insert_temp(Id::new("__drag__"), self.drag_value.clone());
                if let Some(pointer) = ui.ctx().pointer_interact_pos() {
                    *self.start.borrow_mut() = pointer;
                }
            }
            response
        } else {
            let layer_id = LayerId::new(Order::Tooltip, self.id);
            let response = ui.with_layer_id(layer_id, add_content).response;
            if let Some(pointer) = ui.ctx().pointer_interact_pos() {
                let delta = pointer - response.rect.center();
                ui.ctx().translate_layer(layer_id, delta);
            }
            response
        }
    }

    fn is_over_threshold(&self, ui: &Ui) -> bool {
        (ui.ctx().pointer_interact_pos().unwrap_or_default() - *self.start.borrow()).length_sq()
            >= 20.0
    }
}

/// Drop sink.
pub struct DropSink<U, V> {
    map: Box<dyn Fn(U) -> Option<V>>,
}

impl<U: Clone + Send + Sync + 'static, V> DropSink<U, V> {
    /// Creates a new drop sink.
    pub fn new(map: impl Fn(U) -> Option<V> + 'static) -> Rc<Self> {
        Rc::new(Self { map: Box::new(map) })
    }

    /// Enables dropping.
    pub fn drop_value(
        &self,
        ui: &mut Ui,
        mut add_content: impl FnMut(&mut Ui) -> Response,
    ) -> (Response, Option<V>) {
        let is_being_dragged = ui.memory().is_anything_being_dragged();
        let drag_value = ui
            .data()
            .get_temp::<U>(Id::new("__drag__"))
            .and_then(&self.map);
        let response = add_content(ui);
        if is_being_dragged && response.hovered() {
            if drag_value.is_some() {
                ui.painter().rect_filled(
                    response.rect,
                    Rounding::default(),
                    Color32::from_white_alpha(16),
                );

                if ui.input().pointer.any_released() {
                    ui.data().remove::<U>(Id::new("__drag__"));
                    return (response, drag_value);
                }
            } else {
                ui.painter().rect_filled(
                    response.rect,
                    Rounding::default(),
                    Color32::from_rgba_unmultiplied(128, 0, 0, 16),
                );
            }
        }
        (response, None)
    }
}
