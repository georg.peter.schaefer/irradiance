use approx::relative_ne;
use egui::{
    color::Hsva,
    color_picker::{self, Alpha},
    epaint::Shadow,
    style::Margin,
    Area, Frame, Rgba, Sense, Vec2,
};
use irradiance_runtime::math::Vec4;

use crate::{core::EditorEngine, gui::EditorContext};

use super::Widget;

/// A color picker.
pub struct ColorPicker {
    old_color: Vec4,
    color: Vec4,
    changing: bool,
    changed: bool,
    show: bool,
}

impl ColorPicker {
    /// Returns if the value has changed.
    pub fn changed(&self) -> bool {
        self.changed
    }

    /// Returns if the value is changing.
    pub fn changing(&self) -> bool {
        self.changing
    }

    /// Returns the color.
    pub fn get(&self) -> Vec4 {
        self.color
    }

    /// Returns the old color.
    pub fn get_old(&self) -> Vec4 {
        self.old_color
    }

    /// Sets the color.
    pub fn set(&mut self, color: Vec4) {
        if relative_ne!(self.color, color) {
            self.color = color;
            self.old_color = color;
            self.changed = true;
        }
    }
}

impl Default for ColorPicker {
    fn default() -> Self {
        Self {
            old_color: Vec4::new(0.0, 0.0, 0.0, 1.0),
            color: Vec4::new(0.0, 0.0, 0.0, 1.0),
            changing: Default::default(),
            changed: Default::default(),
            show: Default::default(),
        }
    }
}

impl Widget for ColorPicker {
    fn ui(&mut self, ui: &mut egui::Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
        if self.changed {
            self.old_color = self.color;
        }
        self.changed = false;

        let show_color_response = color_picker::show_color(
            ui,
            Rgba::from_rgba_unmultiplied(
                self.color[0],
                self.color[1],
                self.color[2],
                self.color[3],
            ),
            Vec2::new(30.0, 15.0),
        );
        let mut color_response = show_color_response.interact(Sense::click());
        if color_response.clicked() {
            self.show = true;
        }
        if self.show {
            let response = Area::new(color_response.id.with("color_picker"))
                .fixed_pos(color_response.rect.left_bottom())
                .show(ui.ctx(), |ui| {
                    Frame::default()
                        .inner_margin(Margin::same(5.0))
                        .fill(ui.style().visuals.window_fill())
                        .stroke(ui.style().visuals.window_stroke())
                        .shadow(Shadow::small_light())
                        .show(ui, |ui| {
                            ui.spacing_mut().slider_width = 128.0;

                            let mut hsva = Hsva::from_rgba_unmultiplied(
                                self.color[0],
                                self.color[1],
                                self.color[2],
                                self.color[3],
                            );
                            if color_picker::color_picker_hsva_2d(ui, &mut hsva, Alpha::Opaque) {
                                color_response.mark_changed();
                                self.color = hsva.to_rgba_unmultiplied().into();
                            }
                        });
                })
                .response;
            if response.clicked_elsewhere() && !color_response.clicked() {
                self.show = false;
            }
        }

        let changing = color_response.changed() || (self.changing && ui.input().pointer.any_down());
        if self.changing && !changing {
            self.changed = true;
        }
        self.changing = changing;
    }
}
