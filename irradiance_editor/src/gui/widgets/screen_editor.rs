use std::sync::Arc;

use egui::epaint::Shadow;
use egui::style::Margin;
use egui::{Area, Button, Id, TextureId, Ui, Vec2};
use egui_gizmo::{
    GizmoMode, GizmoOrientation, GizmoVisuals, DEFAULT_SNAP_ANGLE, DEFAULT_SNAP_DISTANCE,
    DEFAULT_SNAP_SCALE,
};

use irradiance_runtime::ecs::{Entities, Facade, Transform};
use irradiance_runtime::gfx::device::DeviceOwned;
use irradiance_runtime::input::Mouse;
use irradiance_runtime::rendering::Camera;
use irradiance_runtime::{
    core::{Engine, Frame},
    gfx::{
        image::{
            Image, ImageAspects, ImageLayout, ImageSubresourceRange, ImageType, ImageUsage,
            ImageView, ImageViewType::Type2D,
        },
        sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages},
        Extent2D, Extent3D, GraphicsContext,
    },
    math,
};

use crate::core::Selected;
use crate::gui::message::{GizmoChange, Message};
use crate::{
    core::{EditorEngine, EditorScreen},
    gui::{
        message::{Inspect, Recv},
        EditorContext, Icons,
    },
};

use super::{Tab, Widget};

pub struct ScreenEditor {
    gizmo_orientation: GizmoOrientation,
    gizmo_mode: GizmoMode,
    texture_id: TextureId,
    final_image: Arc<ImageView>,
    screen: &'static str,
    title: String,
}

impl ScreenEditor {
    pub fn new(context: &mut EditorContext, engine: &EditorEngine) -> Self {
        let final_image = create_final_image(
            engine.graphics_context().clone(),
            Extent2D {
                width: 100,
                height: 100,
            },
        );

        final_image
            .image()
            .set_debug_utils_object_name("Screen Editor Final Image");
        final_image.set_debug_utils_object_name("Screen Editor Final Image View");

        Self {
            title: "Screen".to_owned(),
            screen: "",
            final_image: final_image.clone(),
            texture_id: context.register_texture(true, final_image),
            gizmo_mode: GizmoMode::Translate,
            gizmo_orientation: GizmoOrientation::Global,
        }
    }

    pub fn begin_screen_frame(&mut self, frame: &mut Frame) {
        frame.builder_mut().pipeline_barrier(
            PipelineBarrier::builder()
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(self.final_image.image().clone())
                        .src_stage_mask(PipelineStages {
                            top_of_pipe: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask::none())
                        .dst_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::Undefined)
                        .new_layout(ImageLayout::ColorAttachmentOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .build(),
        );
        self.swap_frame_image(frame);
    }

    pub fn end_screen_frame(&mut self, frame: &mut Frame) {
        self.swap_frame_image(frame);
        frame.builder_mut().pipeline_barrier(
            PipelineBarrier::builder()
                .image_memory_barrier(
                    ImageMemoryBarrier::builder(self.final_image.image().clone())
                        .src_stage_mask(PipelineStages {
                            color_attachment_output: true,
                            ..Default::default()
                        })
                        .src_access_mask(AccessMask {
                            color_attachment_write: true,
                            ..Default::default()
                        })
                        .dst_stage_mask(PipelineStages {
                            fragment_shader: true,
                            ..Default::default()
                        })
                        .dst_access_mask(AccessMask {
                            shader_sampled_read: true,
                            ..Default::default()
                        })
                        .old_layout(ImageLayout::ColorAttachmentOptimal)
                        .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                        .subresource_range(
                            ImageSubresourceRange::builder()
                                .aspect_mask(ImageAspects {
                                    color: true,
                                    ..Default::default()
                                })
                                .build(),
                        )
                        .build(),
                )
                .build(),
        );
    }

    fn swap_frame_image(&mut self, frame: &mut Frame) {
        let swapchain_image = frame.swapchain_image();
        frame.set_swapchain_image(self.final_image.clone());
        self.final_image = swapchain_image;
    }
}

impl Widget for ScreenEditor {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Inspect::Screen(screen)) = context.message_bus().recv() {
            self.title = format!("{screen} - Screen");
            self.screen = screen;
            let entities = engine.scenes().get(self.screen).unwrap().entities().clone();
            engine
                .screens()
                .get_mut::<EditorScreen>("__internal_editor_screen__")
                .expect("editor screen")
                .set_entities(entities);
            engine.screens().pop_all();
            engine.screens().push("__internal_editor_screen__");
        }
        if let Some(Inspect::Entity(id, entities)) = context.message_bus().recv() {
            if let Some(selected) = entities.active::<Selected>() {
                selected.remove::<Selected>();
                unsafe {
                    entities.commit();
                }
            }
            if let Some(selected) = entities.get(id) {
                selected.insert(Selected::default());
                selected.activate::<Selected>();
                unsafe {
                    entities.commit();
                }
            }
        }
        if let Some(Inspect::Asset(_)) = context.message_bus().recv() {
            let entities = engine.scenes().get(self.screen).expect("scene").entities();
            if let Some(selected) = entities.active::<Selected>() {
                selected.remove::<Selected>();
                unsafe {
                    entities.commit();
                }
            }
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.set_clip_rect(ui.max_rect());
        engine
            .input_mut()
            .get_mut::<Mouse>("Mouse")
            .set_offset(math::Vec2::new(ui.max_rect().min.x, ui.max_rect().min.y));
        ui.image(self.texture_id, ui.max_rect().size());

        if !context.is_playing() {
            self.gizmo(ui, context, engine);
            self.gizmo_modes(ui);
            self.visualization(ui, engine);
        }

        self.handle_resize(ui, context, engine);
        let hovered = ui.rect_contains_pointer(
            ui.max_rect()
                .shrink(ui.style().interaction.resize_grab_radius_side),
        );
        context.set_screen_editor_hovered(hovered);
        engine
            .screens()
            .get_mut::<EditorScreen>("__internal_editor_screen__")
            .expect("editor screen")
            .set_hovered(hovered);
        engine
            .screens()
            .get_mut::<EditorScreen>("__internal_editor_screen__")
            .expect("editor screen")
            .set_displays(context, engine);
    }
}

impl ScreenEditor {
    fn gizmo(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &EditorEngine) {
        if let Some(selected) = self
            .entities(engine)
            .and_then(|entities| entities.active::<Selected>())
            .filter(|entity| entity.get::<Transform>().is_some())
        {
            let camera = self
                .entities(engine)
                .expect("entities")
                .get("__internal_editor_camera__")
                .expect("editor camera");
            let projection = Self::projection_matrix(ui, &camera);
            let view = Self::view_matrix(&camera);
            let transform = selected.get::<Transform>().expect("transform component");
            let model = Self::model_matrix(&transform);
            let snapping = ui.input().modifiers.command;
            let snap_angle = if ui.input().modifiers.shift {
                DEFAULT_SNAP_ANGLE / 2.0
            } else {
                DEFAULT_SNAP_ANGLE
            };
            let snap_distance = if ui.input().modifiers.shift {
                DEFAULT_SNAP_DISTANCE / 2.0
            } else {
                DEFAULT_SNAP_DISTANCE
            };
            let snap_scale = if ui.input().modifiers.shift {
                DEFAULT_SNAP_SCALE / 2.0
            } else {
                DEFAULT_SNAP_SCALE
            };

            if let Some(result) = egui_gizmo::Gizmo::new(Id::new(selected.id()).with("_gizmo__"))
                .projection_matrix(projection.to_cols_array_2d())
                .view_matrix(view.to_cols_array_2d())
                .model_matrix(model.to_cols_array_2d())
                .viewport(ui.max_rect())
                .mode(self.gizmo_mode)
                .orientation(self.gizmo_orientation)
                .visuals(GizmoVisuals {
                    inactive_alpha: 0.8,
                    highlight_alpha: 1.0,
                    ..Default::default()
                })
                .snapping(snapping)
                .snap_angle(snap_angle)
                .snap_distance(snap_distance)
                .snap_scale(snap_scale)
                .interact(ui)
            {
                let (scale, orientation, position) =
                    glam::Mat4::from_cols_array_2d(&result.transform)
                        .to_scale_rotation_translation();
                match result.mode {
                    GizmoMode::Translate => {
                        context
                            .message_bus_mut()
                            .post(Message::GizmoChange(GizmoChange::Position(
                                position.to_array().into(),
                            )))
                    }
                    GizmoMode::Rotate => context.message_bus_mut().post(Message::GizmoChange(
                        GizmoChange::Orientation(orientation.to_array().into()),
                    )),
                    GizmoMode::Scale => {
                        context
                            .message_bus_mut()
                            .post(Message::GizmoChange(GizmoChange::Scale(
                                scale.to_array().into(),
                            )))
                    }
                }
            }
        }
    }

    fn projection_matrix(ui: &Ui, camera: &Facade) -> glam::Mat4 {
        let camera = camera.get::<Camera>().expect("camera component");
        glam::Mat4::perspective_rh_gl(
            camera.fov_y(),
            ui.max_rect().width() / ui.max_rect().height(),
            camera.near(),
            camera.far(),
        )
    }

    fn view_matrix(camera: &Facade) -> glam::Mat4 {
        let transform = camera.get::<Transform>().expect("transform component");
        glam::Mat4::from_rotation_translation(
            glam::Quat::from_xyzw(
                transform.orientation()[0],
                transform.orientation()[1],
                transform.orientation()[2],
                transform.orientation()[3],
            ),
            glam::Vec3::new(
                transform.position()[0],
                transform.position()[1],
                transform.position()[2],
            ),
        )
        .inverse()
    }

    fn model_matrix(transform: &Transform) -> glam::Mat4 {
        glam::Mat4::from_scale_rotation_translation(
            glam::Vec3::new(
                transform.scale()[0],
                transform.scale()[1],
                transform.scale()[2],
            ),
            glam::Quat::from_xyzw(
                transform.orientation()[0],
                transform.orientation()[1],
                transform.orientation()[2],
                transform.orientation()[3],
            ),
            glam::Vec3::new(
                transform.position()[0],
                transform.position()[1],
                transform.position()[2],
            ),
        )
    }

    fn entities<'a>(&self, engine: &'a EditorEngine) -> Option<&'a Entities> {
        engine
            .scenes()
            .get(self.screen)
            .map(|scene| scene.entities().as_ref())
    }

    fn gizmo_modes(&mut self, ui: &mut Ui) {
        Area::new("editor_gizmo_modes")
            .fixed_pos(ui.max_rect().left_top() + Vec2::splat(10.0))
            .movable(false)
            .show(ui.ctx(), |ui| {
                ui.vertical(|ui| {
                    egui::Frame::default()
                        .inner_margin(Margin::same(2.0))
                        .fill(ui.visuals().faint_bg_color)
                        .shadow(Shadow::small_light())
                        .show(ui, |ui| {
                            ui.selectable_value(
                                &mut self.gizmo_mode,
                                GizmoMode::Translate,
                                Icons::up_down_left_right().size(20.0),
                            );
                            ui.selectable_value(
                                &mut self.gizmo_mode,
                                GizmoMode::Rotate,
                                Icons::arrows_spin().size(20.0),
                            );
                            ui.selectable_value(
                                &mut self.gizmo_mode,
                                GizmoMode::Scale,
                                Icons::expand().size(20.0),
                            );
                            ui.add_space(5.0);
                            if ui
                                .selectable_label(
                                    self.gizmo_orientation == GizmoOrientation::Global,
                                    Icons::globe().size(20.0),
                                )
                                .clicked()
                            {
                                if self.gizmo_orientation == GizmoOrientation::Global {
                                    self.gizmo_orientation = GizmoOrientation::Local;
                                } else {
                                    self.gizmo_orientation = GizmoOrientation::Global;
                                }
                            }
                        });
                });
            });
    }

    fn visualization(&mut self, ui: &mut Ui, engine: &mut EditorEngine) {
        Area::new("editor_visualization")
            .fixed_pos(ui.max_rect().right_top() + Vec2::new(-45.0, 10.0))
            .movable(false)
            .show(ui.ctx(), |ui| {
                ui.vertical(|ui| {
                    egui::Frame::default()
                        .inner_margin(Margin::same(2.0))
                        .fill(ui.visuals().faint_bg_color)
                        .shadow(Shadow::small_light())
                        .show(ui, |ui| {
                            ui.visuals_mut().button_frame = false;

                            ui.menu_button(Icons::eye().size(20.0), |ui| {
                                egui::Frame::default()
                                    .inner_margin(Margin::symmetric(10.0, 5.0))
                                    .show(ui, |ui| {
                                        ui.checkbox(
                                            &mut engine.config_mut().physics_debug,
                                            "Physics Debug",
                                        );
                                    });
                            });
                        });
                });
            });
    }

    fn handle_resize(
        &mut self,
        ui: &mut Ui,
        context: &mut EditorContext,
        engine: &mut EditorEngine,
    ) {
        let extent = Extent3D {
            width: ui.max_rect().width() as _,
            height: ui.max_rect().height() as _,
            depth: 1,
        };
        if self.final_image.image().extent() != extent {
            engine.recreate_buf_pbr_pipeline(extent.into());
            self.final_image = create_final_image(engine.graphics_context().clone(), extent.into());
            engine
                .screens()
                .get_mut::<EditorScreen>("__internal_editor_screen__")
                .expect("editor screen")
                .resize(extent.into());
            self.texture_id = context.register_texture(true, self.final_image.clone());
        }
    }
}

fn create_final_image(graphics_context: Arc<GraphicsContext>, extent: Extent2D) -> Arc<ImageView> {
    ImageView::builder(
        graphics_context.device().clone(),
        Image::builder(graphics_context.device().clone())
            .image_type(ImageType::Type2D)
            .format(graphics_context.swapchain().unwrap().image_format())
            .extent(extent.into())
            .usage(ImageUsage {
                color_attachment: true,
                sampled: true,
                ..Default::default()
            })
            .build()
            .expect("final image"),
    )
    .view_type(Type2D)
    .build()
    .expect("final image")
}

impl Tab for ScreenEditor {
    fn title(&self) -> &str {
        &self.title
    }

    fn tool_bar(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        let response = ui.add_enabled(
            context.is_playing(),
            Button::new(Icons::stop()).frame(false),
        );
        if response.clicked() {
            context.set_playing(false);
            engine.screens().pop_all();
            engine.screens().push("__internal_editor_screen__");
        }

        let response = ui.add_enabled(
            !context.is_playing(),
            Button::new(Icons::play()).frame(false),
        );
        if response.clicked() {
            context.set_playing(true);
            engine.screens().pop_all();
            engine.screens().push(self.screen);
        }
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn std::any::Any {
        self
    }
}
