use crate::gui::Icons;
use crate::{
    core::EditorEngine,
    gui::{
        message::{Message, OpenCreateDialog, OpenDialog},
        EditorContext, Icon,
    },
};

use super::{MenuEntry, Widget};

pub struct NewSubMenu {
    new_asset_entries: Vec<(Option<Icon>, &'static str, &'static str)>,
}

impl NewSubMenu {
    pub fn new(context: &EditorContext) -> Self {
        Self {
            new_asset_entries: context.asset_types().create_entries(),
        }
    }
}

impl Widget for NewSubMenu {
    fn ui(&mut self, ui: &mut egui::Ui, context: &mut EditorContext, _engine: &mut EditorEngine) {
        if ui
            .add(MenuEntry::new("Directory").icon(Icons::folder()))
            .clicked()
        {
            context
                .message_bus_mut()
                .post(Message::OpenDialog(OpenDialog::Create(
                    OpenCreateDialog::Directory,
                )));
            ui.close_menu();
        }

        ui.separator();

        for (icon, display_name, extension) in &self.new_asset_entries {
            let sub_menu_entry = if let Some(icon) = icon {
                MenuEntry::new(*display_name).icon(*icon)
            } else {
                MenuEntry::new(*display_name)
            };

            if ui.add(sub_menu_entry).clicked() {
                context
                    .message_bus_mut()
                    .post(Message::OpenDialog(OpenDialog::Create(
                        OpenCreateDialog::Asset(extension),
                    )));
                ui.close_menu();
            }
        }
    }
}
