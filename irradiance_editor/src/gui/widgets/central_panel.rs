use egui::Ui;
use irradiance_runtime::core::Frame;

use crate::gui::message::{OpenTab, Recv};
use crate::gui::widgets::InputInspector;
use crate::{core::EditorEngine, gui::EditorContext};

use super::{ScreenEditor, Tabs, Widget};

pub struct CentralPanel {
    tabs: Tabs,
}

impl CentralPanel {
    pub fn new(context: &mut EditorContext, engine: &EditorEngine) -> Self {
        Self {
            tabs: Tabs::builder()
                .tab(ScreenEditor::new(context, engine))
                .build(),
        }
    }

    pub fn begin_screen_frame(&mut self, frame: &mut Frame) {
        self.tabs
            .get_mut::<ScreenEditor>()
            .unwrap()
            .begin_screen_frame(frame);
    }

    pub fn end_screen_frame(&mut self, frame: &mut Frame) {
        self.tabs
            .get_mut::<ScreenEditor>()
            .unwrap()
            .end_screen_frame(frame);
    }
}

impl Widget for CentralPanel {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(OpenTab::Input) = context.message_bus().recv() {
            if self.tabs.get::<InputInspector>().is_none() {
                self.tabs.push(InputInspector::default());
            }
        }

        self.tabs.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.style_mut().spacing.item_spacing = egui::Vec2::ZERO;
        egui::CentralPanel::default()
            .frame(egui::Frame::default().fill(ui.style().visuals.window_fill()))
            .show_inside(ui, |ui| {
                ui.reset_style();

                self.tabs.ui(ui, context, engine);
            });
    }
}
