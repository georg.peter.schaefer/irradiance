use std::any::{Any, TypeId};

use egui::{
    style::Margin, Align, Button, Color32, Frame, Label, Layout, RichText, Sense, Stroke, Ui,
};

use crate::gui::Icons;
use crate::{core::EditorEngine, gui::EditorContext};

use super::Widget;

/// Selectable tabs.
pub struct Tabs {
    tabs: Vec<(TypeId, Box<dyn Tab>)>,
    selected: Option<TypeId>,
}

impl Tabs {
    /// Starts building new tabs.
    pub fn builder() -> TabsBuilder {
        TabsBuilder {
            tabs: vec![],
            selcted: None,
        }
    }

    /// Returns a reference to a tab.
    pub fn get<T: Tab + 'static>(&self) -> Option<&T> {
        self.tabs
            .iter()
            .find_map(|(_, tab)| tab.as_any().downcast_ref())
    }

    /// Returns a mutable reference to a tab.
    pub fn get_mut<T: Tab + 'static>(&mut self) -> Option<&mut T> {
        self.tabs
            .iter_mut()
            .find_map(|(_, tab)| tab.as_any_mut().downcast_mut())
    }

    /// Pushes a new tab.
    pub fn push<T: Tab + 'static>(&mut self, tab: T) {
        self.tabs.push((TypeId::of::<T>(), Box::new(tab)));
        self.selected = Some(TypeId::of::<T>());
    }
}

impl Widget for Tabs {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        for (_, tab) in &mut self.tabs {
            tab.handle_messages(context, engine);
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        let item_spacing = ui.spacing_mut().item_spacing;
        ui.spacing_mut().item_spacing.y = 0.0;

        Frame::default()
            .inner_margin(Margin::symmetric(10.0, 5.0))
            .stroke(Stroke::new(0.1, Color32::GRAY))
            .show(ui, |ui| {
                ui.horizontal(|ui| {
                    ui.set_width(ui.available_width());

                    ui.scope(|ui| {
                        let mut close = None;
                        for (type_id, tab) in &self.tabs {
                            ui.spacing_mut().item_spacing.x += 5.0;

                            if self.selected == Some(*type_id) {
                                ui.label(RichText::new(tab.title()).strong());
                            } else if ui
                                .add(Label::new(tab.title()).sense(Sense::click()))
                                .clicked()
                            {
                                self.selected = Some(*type_id);
                            }

                            ui.spacing_mut().item_spacing.x -= 15.0;

                            if tab.is_closeable() {
                                if ui
                                    .add(Button::new(Icons::circle_xmark()).frame(false))
                                    .clicked()
                                {
                                    close = Some(type_id.clone());
                                }
                            }
                        }

                        if let Some(close) = close {
                            self.tabs.retain(|(type_id, _)| *type_id != close);
                            self.selected =
                                self.tabs.iter().map(|(type_id, _)| type_id).last().cloned();
                        }
                    });

                    ui.with_layout(Layout::right_to_left(Align::Center), |ui| {
                        if let Some(tab) = self.selected() {
                            tab.tool_bar(ui, context, engine);
                        }
                    })
                });
            });

        ui.spacing_mut().item_spacing = item_spacing;

        Frame::default()
            .stroke(Stroke::new(0.1, Color32::GRAY))
            .show(ui, |ui| {
                if let Some(tab) = self.selected() {
                    tab.ui(ui, context, engine);
                }
            });
    }
}

impl Tabs {
    fn selected(&mut self) -> Option<&mut (dyn Tab + 'static)> {
        self.tabs
            .iter_mut()
            .find(|(type_id, _)| self.selected.as_ref() == Some(type_id))
            .map(|(_, tab)| tab.as_mut())
    }
}

/// Trait for a tab.
pub trait Tab: Widget {
    /// Returns the title of the tab.
    fn title(&self) -> &str;
    /// Adds a tool bar to the far right side of the tab bar.
    fn tool_bar(&mut self, _ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
    }
    /// Returns if the tab is closeable.
    fn is_closeable(&self) -> bool {
        false
    }
    /// Returns a reference as any.
    fn as_any(&self) -> &dyn Any;
    /// Returns a mutable reference as any.
    fn as_any_mut(&mut self) -> &mut dyn Any;
}

/// Type for building tabs.
pub struct TabsBuilder {
    tabs: Vec<(TypeId, Box<dyn Tab>)>,
    selcted: Option<TypeId>,
}

impl TabsBuilder {
    /// Adds a tab with `label` and ``content.
    pub fn tab<T: Tab + 'static>(mut self, tab: T) -> Self {
        if self.tabs.is_empty() {
            self.selcted = Some(TypeId::of::<T>());
        }
        self.tabs.push((TypeId::of::<T>(), Box::new(tab)));
        self
    }

    /// Builds the tabs.
    pub fn build(self) -> Tabs {
        Tabs {
            tabs: self.tabs,
            selected: self.selcted,
        }
    }
}
