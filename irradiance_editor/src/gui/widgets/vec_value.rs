use egui::{Color32, Ui};
use irradiance_runtime::math::vec::{Column, TVec};

use crate::{core::EditorEngine, gui::EditorContext};

use super::{DragValue, Widget};

/// Widget for editing a vector.
pub struct VecValue<const N: usize> {
    components: [DragValue<f32>; N],
}

impl<const N: usize> VecValue<N> {
    /// Returns if the value has changed.
    pub fn changed(&self) -> bool {
        self.components.iter().any(|component| component.changed())
    }

    /// Returns if the value is changing.
    pub fn changing(&self) -> bool {
        self.components.iter().any(|component| component.changing())
    }

    /// Returns the value.
    pub fn get(&self) -> TVec<Column, f32, N> {
        let mut value = TVec::<Column, f32, N>::default();
        for (index, component) in self.components.iter().enumerate() {
            value[index] = component.get();
        }
        value
    }

    /// Returns the old value.
    pub fn get_old(&self) -> TVec<Column, f32, N> {
        let mut old_value = TVec::<Column, f32, N>::default();
        for (index, component) in self.components.iter().enumerate() {
            old_value[index] = component.get_old();
        }
        old_value
    }

    /// Sets the value.
    pub fn set(&mut self, value: TVec<Column, f32, N>) {
        for (index, component) in self.components.iter_mut().enumerate() {
            component.set(value[index]);
        }
    }

    /// Sets the value in changing mode.
    pub fn set_changing(&mut self, value: TVec<Column, f32, N>) {
        for (index, component) in self.components.iter_mut().enumerate() {
            component.set_changing(value[index]);
        }
    }
}

impl Default for VecValue<3> {
    fn default() -> Self {
        Self {
            components: [
                DragValue::builder().color(Color32::RED).build(),
                DragValue::builder().color(Color32::GREEN).build(),
                DragValue::builder().color(Color32::BLUE).build(),
            ],
        }
    }
}

impl<const N: usize> Widget for VecValue<N> {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.horizontal(|ui| {
            ui.set_width(ui.available_width());
            ui.spacing_mut().interact_size.x =
                (ui.available_width() / 3.0) - ui.spacing().item_spacing.x;

            for component in &mut self.components {
                component.ui(ui, context, engine);
            }
        });
    }
}
