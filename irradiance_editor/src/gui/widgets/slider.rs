use std::ops::RangeInclusive;

use approx::{relative_ne, RelativeEq};
use egui::emath::Numeric;

use crate::{core::EditorEngine, gui::EditorContext};

use super::Widget;

/// A slider.
pub struct Slider<Number> {
    old_value: Number,
    value: Number,
    range: RangeInclusive<Number>,
    changing: bool,
    changed: bool,
}

impl<Number: Numeric + Default + RelativeEq> Slider<Number> {
    /// Creates a new slider.
    pub fn new(range: RangeInclusive<Number>) -> Self {
        Self {
            changed: false,
            changing: false,
            range,
            value: Default::default(),
            old_value: Default::default(),
        }
    }

    /// Returns if the value has changed.
    pub fn changed(&self) -> bool {
        self.changed
    }

    /// Returns if the value is changing.
    pub fn changing(&self) -> bool {
        self.changing
    }

    /// Returns the value.
    pub fn get(&self) -> Number {
        self.value
    }

    /// Returns the old value.
    pub fn get_old(&self) -> Number {
        self.old_value
    }

    /// Sets the value.
    pub fn set(&mut self, value: Number) {
        if relative_ne!(self.value, value) {
            self.value = value;
            self.old_value = value;
            self.changed = true;
        }
    }
}

impl<Number: Numeric + RelativeEq> Widget for Slider<Number> {
    fn ui(&mut self, ui: &mut egui::Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
        if self.changed {
            self.old_value = self.value;
        }
        self.changed = false;

        let response = ui.add(egui::Slider::new(&mut self.value, self.range.clone()));

        let changing = response.changed() || (self.changing && ui.input().pointer.any_down());
        if self.changing && !changing {
            self.changed = true;
        }
        self.changing = changing;
    }
}
