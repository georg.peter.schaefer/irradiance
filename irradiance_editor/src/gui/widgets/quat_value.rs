use egui::{Color32, Ui};

use irradiance_runtime::math::Quat;

use crate::{core::EditorEngine, gui::EditorContext};

use super::{DragValue, Widget};

/// Widget for editing a vector.
pub struct QuatValue {
    components: [DragValue<f32>; 3],
}

impl QuatValue {
    /// Returns if the value has changed.
    pub fn changed(&self) -> bool {
        self.components.iter().any(|component| component.changed())
    }

    /// Returns if the value is changing.
    pub fn changing(&self) -> bool {
        self.components.iter().any(|component| component.changing())
    }

    /// Returns the value.
    pub fn get(&self) -> Quat {
        Quat::from(
            glam::Quat::from_euler(
                glam::EulerRot::YXZ,
                self.components[1].get().to_radians(),
                self.components[0].get().to_radians(),
                self.components[2].get().to_radians(),
            )
            .to_array(),
        )
    }

    /// Returns the old value.
    pub fn get_old(&self) -> Quat {
        Quat::from(
            glam::Quat::from_euler(
                glam::EulerRot::YXZ,
                self.components[1].get_old().to_radians(),
                self.components[0].get_old().to_radians(),
                self.components[2].get_old().to_radians(),
            )
            .to_array(),
        )
    }

    /// Sets the value.
    pub fn set(&mut self, value: Quat) {
        let (y, x, z) = glam::Quat::from_xyzw(value[0], value[1], value[2], value[3])
            .to_euler(glam::EulerRot::YXZ);
        self.components[0].set(x.to_degrees());
        self.components[1].set(y.to_degrees());
        self.components[2].set(z.to_degrees());
    }

    /// Sets the value in changing mode.
    pub fn set_changing(&mut self, value: Quat) {
        let (y, x, z) = glam::Quat::from_xyzw(value[0], value[1], value[2], value[3])
            .to_euler(glam::EulerRot::YXZ);
        self.components[0].set_changing(x.to_degrees());
        self.components[1].set_changing(y.to_degrees());
        self.components[2].set_changing(z.to_degrees());
    }
}

impl Default for QuatValue {
    fn default() -> Self {
        Self {
            components: [
                DragValue::builder().suffix("°").color(Color32::RED).build(),
                DragValue::builder()
                    .suffix("°")
                    .color(Color32::GREEN)
                    .build(),
                DragValue::builder()
                    .suffix("°")
                    .color(Color32::BLUE)
                    .build(),
            ],
        }
    }
}

impl Widget for QuatValue {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.horizontal(|ui| {
            ui.set_width(ui.available_width());
            ui.spacing_mut().interact_size.x =
                (ui.available_width() / 3.0) - ui.spacing().item_spacing.x;

            for component in &mut self.components {
                component.ui(ui, context, engine);
            }
        });
    }
}
