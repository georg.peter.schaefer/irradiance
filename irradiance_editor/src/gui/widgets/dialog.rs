//! Dialogs.

use std::path::Path;

use egui::{Button, Key, Ui, Vec2};
use irradiance_runtime::core::Engine;

use crate::gui::message::{Duplicate, OpenDuplicateDialog};
use crate::{
    core::EditorEngine,
    gui::{
        message::{CloseDialog, Create, Message, OpenCreateDialog, OpenRenameDialog, Rename},
        EditorContext,
    },
};

use super::Widget;

/// Dialog trait.
pub trait Dialog: Widget {
    /// Returns the dialog title.
    fn title(&self) -> &str;
    /// Returns the dialog size.
    fn size(&self) -> Vec2;
    /// Sets the dialog size.
    fn set_size(&mut self, size: Vec2);
}

/// Widget for creating something new.
pub struct CreateDialog {
    name: String,
    create: OpenCreateDialog,
    title: &'static str,
    size: Vec2,
}

impl CreateDialog {
    /// Creates a new dialog.
    pub fn new(context: &EditorContext, create: OpenCreateDialog) -> Box<Self> {
        Box::new(Self {
            size: Vec2::default(),
            title: Self::display_name(context, &create),
            create,
            name: Default::default(),
        })
    }

    fn display_name(context: &EditorContext, create: &OpenCreateDialog) -> &'static str {
        match create {
            OpenCreateDialog::Directory => "Directory",
            OpenCreateDialog::Asset(extension) => context
                .asset_types()
                .display_name(Path::new("default").with_extension(extension))
                .unwrap_or_default(),
            OpenCreateDialog::Entity(_) => "Entity",
            OpenCreateDialog::ActionMap => "Action Map",
            OpenCreateDialog::Action(_) => "Action",
            OpenCreateDialog::Binding(_, _) => "Binding",
        }
    }
}

impl Widget for CreateDialog {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label(self.title);
        ui.text_edit_singleline(&mut self.name).request_focus();
        ui.horizontal(|ui| {
            if ui.button("Cancel").clicked() || ui.input().key_pressed(Key::Escape) {
                context
                    .message_bus_mut()
                    .post(Message::CloseDialog(CloseDialog));
            }
            let valid = self.is_valid(context, engine);
            if ui.add_enabled(valid, Button::new("Create")).clicked()
                || (valid && ui.input().key_pressed(Key::Enter))
            {
                self.create(context);
                context
                    .message_bus_mut()
                    .post(Message::CloseDialog(CloseDialog));
            }
        });
    }
}

impl CreateDialog {
    fn is_valid(&self, context: &EditorContext, engine: &EditorEngine) -> bool {
        if self.name.is_empty() {
            return false;
        }

        match &self.create {
            OpenCreateDialog::Directory => !context.current_asset_path().join(&self.name).exists(),
            OpenCreateDialog::Asset(extensions) => !engine.assets().contains(
                context
                    .current_asset_path()
                    .join(&self.name)
                    .with_extension(extensions),
            ),
            OpenCreateDialog::Entity(entities) => entities.get(&self.name).is_none(),
            OpenCreateDialog::ActionMap => engine.input().action_map(&self.name).is_none(),
            OpenCreateDialog::Action(action_map) => !engine
                .input()
                .action_map(action_map)
                .expect("action map")
                .0
                .contains_key(&self.name),
            OpenCreateDialog::Binding(action_map, action) => !engine
                .input()
                .action_map(action_map)
                .expect("action map")
                .0
                .get(action)
                .expect("action")
                .contains_binding(&self.name),
        }
    }

    fn create(&self, context: &mut EditorContext) {
        match &self.create {
            OpenCreateDialog::Directory => {
                context
                    .message_bus_mut()
                    .post(Message::Create(Create::Directory(self.name.clone())));
            }
            OpenCreateDialog::Asset(extension) => {
                context
                    .message_bus_mut()
                    .post(Message::Create(Create::Asset(
                        self.name.clone(),
                        *extension,
                    )))
            }
            OpenCreateDialog::Entity(_) => context
                .message_bus_mut()
                .post(Message::Create(Create::Entity(self.name.clone()))),
            OpenCreateDialog::ActionMap => context
                .message_bus_mut()
                .post(Message::Create(Create::ActionMap(self.name.clone()))),
            OpenCreateDialog::Action(action_map) => {
                context
                    .message_bus_mut()
                    .post(Message::Create(Create::Action(
                        action_map.clone(),
                        self.name.clone(),
                    )))
            }
            OpenCreateDialog::Binding(action_map, action) => {
                context
                    .message_bus_mut()
                    .post(Message::Create(Create::Binding(
                        action_map.clone(),
                        action.clone(),
                        self.name.clone(),
                    )))
            }
        }
    }
}

impl Dialog for CreateDialog {
    fn title(&self) -> &str {
        self.title
    }

    fn size(&self) -> Vec2 {
        self.size
    }

    fn set_size(&mut self, size: Vec2) {
        self.size = size;
    }
}

/// Widget for renaming something.
pub struct RenameDialog {
    new_name: String,
    old_name: String,
    rename: OpenRenameDialog,
    title: &'static str,
    size: Vec2,
}

impl RenameDialog {
    /// Creates a new dialog.
    pub fn new(rename: OpenRenameDialog) -> Box<Self> {
        let old_name = Self::old_name(&rename);

        Box::new(Self {
            size: Vec2::default(),
            title: Self::display_name(&rename),
            rename,
            old_name: old_name.clone(),
            new_name: old_name,
        })
    }

    fn display_name(rename: &OpenRenameDialog) -> &'static str {
        match rename {
            OpenRenameDialog::Asset(_) => "Asset",
            OpenRenameDialog::Directory(_) => "Directory",
            OpenRenameDialog::Entity(_, _) => "Entity",
            OpenRenameDialog::ActionMap(_) => "Action Map",
            OpenRenameDialog::Action(_, _) => "Action",
            OpenRenameDialog::Binding(_, _, _) => "Binding",
        }
    }

    fn old_name(rename: &OpenRenameDialog) -> String {
        match rename {
            OpenRenameDialog::Asset(path) => {
                path.file_stem().unwrap().to_str().unwrap().to_string()
            }
            OpenRenameDialog::Directory(path) => {
                path.file_name().unwrap().to_str().unwrap().to_string()
            }
            OpenRenameDialog::Entity(old_name, _) => old_name.clone(),
            OpenRenameDialog::ActionMap(old_name) => old_name.clone(),
            OpenRenameDialog::Action(_, old_name) => old_name.clone(),
            OpenRenameDialog::Binding(_, _, old_name) => old_name.clone(),
        }
    }
}

impl Widget for RenameDialog {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label(self.title);
        ui.text_edit_singleline(&mut self.new_name).request_focus();
        ui.horizontal(|ui| {
            if ui.button("Cancel").clicked() || ui.input().key_pressed(Key::Escape) {
                context
                    .message_bus_mut()
                    .post(Message::CloseDialog(CloseDialog));
            }
            let valid = self.is_valid(context, engine);
            if ui.add_enabled(valid, Button::new("Rename")).clicked()
                || (valid && ui.input().key_pressed(Key::Enter))
            {
                self.rename(context);
                context
                    .message_bus_mut()
                    .post(Message::CloseDialog(CloseDialog));
            }
        });
    }
}

impl RenameDialog {
    fn is_valid(&self, context: &EditorContext, engine: &EditorEngine) -> bool {
        if self.new_name.is_empty() || self.new_name == self.old_name {
            return false;
        }

        match &self.rename {
            OpenRenameDialog::Asset(path) => !engine.assets().contains(
                path.parent()
                    .unwrap()
                    .join(&self.new_name)
                    .with_extension(path.extension().unwrap()),
            ),
            OpenRenameDialog::Directory(path) => !context
                .asset_path()
                .join(path.parent().unwrap().join(&self.new_name))
                .exists(),
            OpenRenameDialog::Entity(_, entities) => entities.get(&self.new_name).is_none(),
            OpenRenameDialog::ActionMap(_) => engine.input().action_map(&self.new_name).is_none(),
            OpenRenameDialog::Action(action_map, _) => !engine
                .input()
                .action_map(action_map)
                .expect("action map")
                .0
                .contains_key(&self.new_name),
            OpenRenameDialog::Binding(action_map, action, _) => !engine
                .input()
                .action_map(action_map)
                .expect("action map")
                .0
                .get(action)
                .expect("action")
                .contains_binding(&self.new_name),
        }
    }

    fn rename(&self, context: &mut EditorContext) {
        match &self.rename {
            OpenRenameDialog::Asset(old_path) => {
                let new_path = old_path
                    .parent()
                    .unwrap()
                    .join(&self.new_name)
                    .with_extension(old_path.extension().unwrap());
                context
                    .message_bus_mut()
                    .post(Message::Rename(Rename::Asset(old_path.clone(), new_path)));
            }
            OpenRenameDialog::Directory(old_path) => {
                let new_path = old_path.parent().unwrap().join(&self.new_name);
                context
                    .message_bus_mut()
                    .post(Message::Rename(Rename::Directory(
                        old_path.clone(),
                        new_path,
                    )));
            }
            OpenRenameDialog::Entity(_, _) => {
                context
                    .message_bus_mut()
                    .post(Message::Rename(Rename::Entity(
                        self.old_name.clone(),
                        self.new_name.clone(),
                    )))
            }
            OpenRenameDialog::ActionMap(_) => {
                context
                    .message_bus_mut()
                    .post(Message::Rename(Rename::ActionMap(
                        self.old_name.clone(),
                        self.new_name.clone(),
                    )));
            }
            OpenRenameDialog::Action(action_map, _) => {
                context
                    .message_bus_mut()
                    .post(Message::Rename(Rename::Action(
                        action_map.clone(),
                        self.old_name.clone(),
                        self.new_name.clone(),
                    )));
            }
            OpenRenameDialog::Binding(action_map, action, _) => {
                context
                    .message_bus_mut()
                    .post(Message::Rename(Rename::Binding(
                        action_map.clone(),
                        action.clone(),
                        self.old_name.clone(),
                        self.new_name.clone(),
                    )));
            }
        }
    }
}

impl Dialog for RenameDialog {
    fn title(&self) -> &str {
        self.title
    }

    fn size(&self) -> Vec2 {
        self.size
    }

    fn set_size(&mut self, size: Vec2) {
        self.size = size;
    }
}

/// Widget for duplicating something.
pub struct DuplicateDialog {
    new_name: String,
    old_name: String,
    duplicate: OpenDuplicateDialog,
    title: &'static str,
    size: Vec2,
}

impl DuplicateDialog {
    /// Creates a new dialog.
    pub fn new(duplicate: OpenDuplicateDialog) -> Box<Self> {
        let old_name = Self::old_name(&duplicate);

        Box::new(Self {
            size: Vec2::default(),
            title: Self::display_name(&duplicate),
            duplicate,
            old_name: old_name.clone(),
            new_name: old_name,
        })
    }

    fn display_name(duplicate: &OpenDuplicateDialog) -> &'static str {
        match duplicate {
            OpenDuplicateDialog::Entity(_, _) => "Entity",
        }
    }

    fn old_name(duplicate: &OpenDuplicateDialog) -> String {
        match duplicate {
            OpenDuplicateDialog::Entity(old_name, _) => old_name.clone(),
        }
    }
}

impl Widget for DuplicateDialog {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, _engine: &mut EditorEngine) {
        ui.label(self.title);
        ui.text_edit_singleline(&mut self.new_name).request_focus();
        ui.horizontal(|ui| {
            if ui.button("Cancel").clicked() || ui.input().key_pressed(Key::Escape) {
                context
                    .message_bus_mut()
                    .post(Message::CloseDialog(CloseDialog));
            }
            let valid = self.is_valid();
            if ui.add_enabled(valid, Button::new("Duplicate")).clicked()
                || (valid && ui.input().key_pressed(Key::Enter))
            {
                self.duplicate(context);
                context
                    .message_bus_mut()
                    .post(Message::CloseDialog(CloseDialog));
            }
        });
    }
}

impl DuplicateDialog {
    fn is_valid(&self) -> bool {
        if self.new_name.is_empty() || self.new_name == self.old_name {
            return false;
        }

        match &self.duplicate {
            OpenDuplicateDialog::Entity(_, entities) => entities.get(&self.new_name).is_none(),
        }
    }

    fn duplicate(&self, context: &mut EditorContext) {
        match &self.duplicate {
            OpenDuplicateDialog::Entity(_, _) => {
                context
                    .message_bus_mut()
                    .post(Message::Duplicate(Duplicate::Entity(
                        self.old_name.clone(),
                        self.new_name.clone(),
                    )))
            }
        }
    }
}

impl Dialog for DuplicateDialog {
    fn title(&self) -> &str {
        self.title
    }

    fn size(&self) -> Vec2 {
        self.size
    }

    fn set_size(&mut self, size: Vec2) {
        self.size = size;
    }
}

/// Confirm dialog.
pub struct ConfirmDialog {
    confirm: Message,
    description: &'static str,
    title: &'static str,
    size: Vec2,
}

impl ConfirmDialog {
    /// Creates a new confirm dialog.
    pub fn new(title: &'static str, description: &'static str, confirm: Message) -> Box<Self> {
        Box::new(Self {
            confirm,
            description,
            title,
            size: Vec2::default(),
        })
    }
}

impl Widget for ConfirmDialog {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, _engine: &mut EditorEngine) {
        ui.label(self.description);
        ui.horizontal(|ui| {
            if ui.button("Cancel").clicked() || ui.input().key_pressed(Key::Escape) {
                context
                    .message_bus_mut()
                    .post(Message::CloseDialog(CloseDialog));
            }
            if ui.button("Confirm").clicked() || ui.input().key_pressed(Key::Enter) {
                context.message_bus_mut().post(self.confirm.clone());
                context
                    .message_bus_mut()
                    .post(Message::CloseDialog(CloseDialog));
            }
        });
    }
}

impl Dialog for ConfirmDialog {
    fn title(&self) -> &str {
        self.title
    }

    fn size(&self) -> Vec2 {
        self.size
    }

    fn set_size(&mut self, size: Vec2) {
        self.size = size;
    }
}
