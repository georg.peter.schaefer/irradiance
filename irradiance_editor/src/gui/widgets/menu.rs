use egui::{text::LayoutJob, Button, Color32, Response, TextFormat, Ui, Widget};

use crate::gui::Icon;

/// A sub menu entry.
pub struct MenuEntry {
    text: String,
    icon: Option<Icon>,
}

impl MenuEntry {
    /// Creates a new sub menu entry.
    pub fn new(text: impl Into<String>) -> Self {
        Self {
            text: text.into(),
            icon: None,
        }
    }

    /// Sets an icon.
    pub fn icon(mut self, icon: Icon) -> Self {
        self.icon = Some(icon);
        self
    }

    /// Adds a sub menu.
    pub fn sub_menu(self, ui: &mut Ui, add_contents: impl FnOnce(&mut Ui)) {
        ui.menu_button(self.layout_job(), add_contents);
    }

    fn layout_job(&self) -> LayoutJob {
        let mut layout_job = LayoutJob::default();
        let mut leading_space = 19.0;
        if let Some(icon) = self.icon {
            leading_space = 5.0;
            layout_job.append(
                icon.inner,
                0.0,
                TextFormat::simple(icon.font_id(), icon.color.unwrap_or(Color32::GRAY)),
            );
        }
        layout_job.append(&self.text, leading_space, TextFormat::default());
        layout_job
    }
}

impl Widget for MenuEntry {
    fn ui(self, ui: &mut Ui) -> Response {
        ui.add(Button::new(self.layout_job()).wrap(false))
    }
}
