use std::path::Path;
use std::{any, ffi::OsStr, fs, path::PathBuf, rc::Rc, sync::Arc};

use egui::{
    style::Margin, Align, Button, Color32, FontSelection, Frame, Id, Key, Layout, Rect, Response,
    Rounding, ScrollArea, Sense, Ui, Vec2, WidgetText,
};
use irradiance_runtime::{asset::Assets, core::Engine, math};
use rand::distributions::{Alphanumeric, DistString};

use crate::gui::message::{Delete, Move, OpenDeleteDialog, OpenDialog, OpenRenameDialog, Rename};
use crate::gui::widgets::DropSink;
use crate::{
    asset::EditorAssetTypes,
    core::EditorEngine,
    gui::{
        message::{Create, Inspect, Message, Recv, Repaint},
        Action, EditorContext, Icon, Icons,
    },
};

use super::{DragSource, MenuEntry, NewSubMenu, Tab, Widget};

pub struct AssetBrowser {
    new_sub_menu: NewSubMenu,
    files: Vec<Entry>,
    dirs: Vec<Entry>,
    current: PathBuf,
    path: PathBuf,
    delete_dir: PathBuf,
}

impl AssetBrowser {
    pub fn new(context: &mut EditorContext, path: PathBuf) -> Self {
        let delete_dir = std::env::temp_dir().join(format!(
            "{}-{}",
            std::env::current_exe()
                .unwrap()
                .file_name()
                .unwrap()
                .to_str()
                .unwrap(),
            std::process::id()
        ));
        if !delete_dir.exists() {
            fs::create_dir(&delete_dir).expect("create delete directory");
        }

        let mut asset_browser = Self {
            delete_dir,
            path: path.clone(),
            current: path,
            dirs: vec![],
            files: vec![],
            new_sub_menu: NewSubMenu::new(context),
        };
        asset_browser.refresh(context);
        asset_browser
    }

    fn refresh(&mut self, context: &mut EditorContext) {
        if !self.current.exists() {
            self.current = self.path.clone();
        }
        context.set_current_asset_path(self.current.clone());
        self.dirs = self.entries(
            |_| Preview::Icon(Icons::folder_open()),
            |entry| entry.is_dir(),
        );
        self.files = self.entries(
            |path| {
                context
                    .asset_types()
                    .display(path, math::Vec2::new(50.0, 50.0))
                    .map(|display| Preview::Widget(display))
                    .unwrap_or(Preview::Icon(Icons::file()))
            },
            |entry| entry.is_file(),
        );
    }

    fn entries(
        &self,
        preview: impl Fn(PathBuf) -> Preview,
        filter: impl Fn(&PathBuf) -> bool,
    ) -> Vec<Entry> {
        let mut entries = fs::read_dir(&self.current)
            .expect("read directory")
            .map(|result| result.expect("directory entry"))
            .map(|entry| entry.path())
            .filter(filter)
            .map(|entry| {
                entry
                    .strip_prefix(&self.path)
                    .expect("relative asset path")
                    .to_owned()
            })
            .map(|path| Entry::new(preview(path.clone()), self.path.join(&path), path))
            .collect::<Vec<_>>();
        entries.sort_by_key(|entry| entry.file_stem.clone());
        entries
    }
}

impl Drop for AssetBrowser {
    fn drop(&mut self) {
        if self.delete_dir.exists() {
            fs::remove_dir_all(&self.delete_dir).expect("remove delete directory");
        }
    }
}

impl Widget for AssetBrowser {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Create::Directory(name)) = context.message_bus().recv() {
            let create_directory = CreateDirectory::new(context, name);
            context.actions_mut().push(engine, create_directory);
            self.refresh(context);
        }
        if let Some(Create::Asset(name, extension)) = context.message_bus().recv() {
            let create_asset = CreateAsset::new(name, extension, context, engine);
            context.actions_mut().push(engine, create_asset);
            self.refresh(context);
        }
        if let Some(Rename::Asset(old_path, new_path)) = context.message_bus().recv().cloned() {
            context.actions_mut().push(
                engine,
                RenameAsset::new(self.path.clone(), old_path, new_path),
            );
            self.refresh(context);
        }
        if let Some(Rename::Directory(old_path, new_path)) = context.message_bus().recv().cloned() {
            context.actions_mut().push(
                engine,
                RenameDirectory::new(self.path.clone(), old_path, new_path),
            );
            self.refresh(context);
        }
        if let Some(Move::Asset(from, to)) = context.message_bus().recv().cloned() {
            context
                .actions_mut()
                .push(engine, MoveAsset::new(self.path.clone(), from, to));
            self.refresh(context);
        }
        if let Some(Move::Directory(from, to)) = context.message_bus().recv().cloned() {
            context
                .actions_mut()
                .push(engine, MoveDirectory::new(self.path.clone(), from, to));
            self.refresh(context);
        }
        if let Some(Delete::Asset(path)) = context.message_bus().recv().cloned() {
            context.actions_mut().push(
                engine,
                DeleteAsset::new(self.delete_dir.clone(), self.path.clone(), path),
            );
            self.refresh(context);
        }
        if let Some(Delete::Directory(path)) = context.message_bus().recv().cloned() {
            context.actions_mut().push(
                engine,
                DeleteDirectory::new(self.delete_dir.clone(), self.path.clone(), path),
            );
            self.refresh(context);
        }
        if let Some(Repaint::All) = context.message_bus().recv() {
            self.refresh(context);
        }

        for file in &mut self.files {
            file.handle_messages(context, engine);
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.set_clip_rect(ui.max_rect());
        ScrollArea::both()
            .auto_shrink([false, false])
            .show(ui, |ui| {
                let mut refresh = false;

                Frame::default()
                    .inner_margin(Margin::same(10.0))
                    .show(ui, |ui| {
                        ui.with_layout(
                            Layout::left_to_right(Align::Center)
                                .with_cross_align(Align::Min)
                                .with_main_wrap(true),
                            |ui| {
                                for dir in &mut self.dirs {
                                    let response = dir.ui(ui, context, engine);

                                    if dir.selected && ui.input().key_pressed(Key::Delete) {
                                        context.message_bus_mut().post(Message::OpenDialog(
                                            OpenDialog::Delete(OpenDeleteDialog::Directory(
                                                dir.path.clone(),
                                            )),
                                        ));
                                    }
                                    if dir.double_clicked {
                                        self.current = self.path.join(&dir.path);
                                        refresh = true;
                                    }

                                    response.context_menu(|ui| {
                                        MenuEntry::new("New").sub_menu(ui, |ui| {
                                            self.new_sub_menu.ui(ui, context, engine);
                                        });

                                        if self.path != self.current {
                                            ui.separator();

                                            if ui.add(MenuEntry::new("Move Up")).clicked() {
                                                context.message_bus_mut().post(Message::Move(
                                                    Move::Directory(
                                                        dir.path.clone(),
                                                        dir.path
                                                            .parent()
                                                            .unwrap()
                                                            .parent()
                                                            .unwrap()
                                                            .to_path_buf(),
                                                    ),
                                                ));
                                                ui.close_menu();
                                            }
                                        }

                                        ui.separator();

                                        if ui.add(MenuEntry::new("Rename")).clicked() {
                                            context.message_bus_mut().post(Message::OpenDialog(
                                                OpenDialog::Rename(OpenRenameDialog::Directory(
                                                    dir.path.clone(),
                                                )),
                                            ));
                                            ui.close_menu();
                                        }
                                        if ui.add(MenuEntry::new("Delete")).clicked() {
                                            context.message_bus_mut().post(Message::OpenDialog(
                                                OpenDialog::Delete(OpenDeleteDialog::Directory(
                                                    dir.path.clone(),
                                                )),
                                            ));
                                            ui.close_menu();
                                        }
                                    });
                                }
                                for file in &mut self.files {
                                    let response = file.ui(ui, context, engine);

                                    if file.selected && ui.input().key_pressed(Key::Delete) {
                                        context.message_bus_mut().post(Message::OpenDialog(
                                            OpenDialog::Delete(OpenDeleteDialog::Asset(
                                                file.path.clone(),
                                            )),
                                        ));
                                    }
                                    if file.double_clicked
                                        && context.asset_types().is_registered(&file.path)
                                    {
                                        context.message_bus_mut().post(Message::Inspect(
                                            Inspect::Asset(file.path.clone()),
                                        ))
                                    }

                                    response.context_menu(|ui| {
                                        MenuEntry::new("New").sub_menu(ui, |ui| {
                                            self.new_sub_menu.ui(ui, context, engine);
                                        });

                                        if self.path != self.current {
                                            ui.separator();

                                            if ui.add(MenuEntry::new("Move Up")).clicked() {
                                                context.message_bus_mut().post(Message::Move(
                                                    Move::Asset(
                                                        file.path.clone(),
                                                        file.path
                                                            .parent()
                                                            .unwrap()
                                                            .parent()
                                                            .unwrap()
                                                            .to_path_buf(),
                                                    ),
                                                ));
                                                ui.close_menu();
                                            }
                                        }

                                        ui.separator();

                                        if ui.add(MenuEntry::new("Rename")).clicked() {
                                            context.message_bus_mut().post(Message::OpenDialog(
                                                OpenDialog::Rename(OpenRenameDialog::Asset(
                                                    file.path.clone(),
                                                )),
                                            ));
                                            ui.close_menu();
                                        }
                                        if ui.add(MenuEntry::new("Delete")).clicked() {
                                            context.message_bus_mut().post(Message::OpenDialog(
                                                OpenDialog::Delete(OpenDeleteDialog::Asset(
                                                    file.path.clone(),
                                                )),
                                            ));
                                            ui.close_menu();
                                        }
                                    });
                                }
                            },
                        );
                    });

                ui.interact(
                    ui.max_rect(),
                    Id::new("asset browser"),
                    Sense::click_and_drag(),
                )
                .context_menu(|ui| {
                    MenuEntry::new("New").sub_menu(ui, |ui| {
                        self.new_sub_menu.ui(ui, context, engine);
                    });
                });

                if refresh {
                    self.refresh(context);
                }
            });
    }
}

impl Tab for AssetBrowser {
    fn title(&self) -> &str {
        "Assets"
    }

    fn tool_bar(&mut self, ui: &mut Ui, context: &mut EditorContext, _engine: &mut EditorEngine) {
        let response = ui.add(Button::new(Icons::arrows_rotate()).frame(false));
        if response.clicked() {
            self.refresh(context);
        }

        let response = ui.add_enabled(
            self.current != self.path,
            Button::new(Icons::arrow_up()).frame(false),
        );
        if response.clicked() {
            if let Some(parent) = self.current.parent() {
                self.current = parent.to_owned();
                self.refresh(context);
            }
        }
    }

    fn as_any(&self) -> &dyn any::Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn any::Any {
        self
    }
}

struct Entry {
    double_clicked: bool,
    selected: bool,
    file_stem: String,
    path: PathBuf,
    preview: Preview,
    drag_source: Rc<DragSource<PathBuf>>,
    drop_sink: Option<Rc<DropSink<PathBuf, PathBuf>>>,
}

impl Entry {
    fn new(preview: Preview, full_path: PathBuf, path: PathBuf) -> Self {
        let file_stem = path
            .file_stem()
            .and_then(OsStr::to_str)
            .map(ToOwned::to_owned)
            .expect("file stem");

        Self {
            drop_sink: full_path.is_dir().then(|| DropSink::new(|path| Some(path))),
            drag_source: DragSource::new(Id::new(&path), path.clone()),
            preview,
            path,
            file_stem,
            selected: false,
            double_clicked: false,
        }
    }

    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Preview::Widget(widget) = &mut self.preview {
            widget.handle_messages(context, engine);
        }
    }

    fn ui(
        &mut self,
        ui: &mut Ui,
        context: &mut EditorContext,
        engine: &mut EditorEngine,
    ) -> Response {
        if let Some(drop_sink) = self.drop_sink.clone() {
            let (response, drop_value) =
                drop_sink.drop_value(ui, |ui| self.drag(ui, context, engine));
            if let Some(drop_value) = drop_value.filter(|drop_value| *drop_value != self.path) {
                let full_path = context.asset_path().join(&drop_value);
                if full_path.is_dir() {
                    context
                        .message_bus_mut()
                        .post(Message::Move(Move::Directory(
                            drop_value,
                            self.path.clone(),
                        )));
                } else if full_path.is_file() {
                    context
                        .message_bus_mut()
                        .post(Message::Move(Move::Asset(drop_value, self.path.clone())));
                }
            }
            response
        } else {
            self.drag(ui, context, engine)
        }
    }

    fn drag(
        &mut self,
        ui: &mut Ui,
        context: &mut EditorContext,
        engine: &mut EditorEngine,
    ) -> Response {
        self.drag_source.clone().drag_value(ui, |ui| {
            let (rect, response) =
                ui.allocate_exact_size(self.size(ui, &self.file_stem), Sense::click());

            if response.clicked() {
                self.selected = true;
            } else if response.clicked_elsewhere() {
                self.selected = false;
            }
            self.double_clicked = response.double_clicked();

            let mut ui = ui.child_ui(rect, Layout::top_down(Align::Center));

            self.preview.ui(&mut ui, context, engine, self.selected);
            self.text(&mut ui, &self.file_stem);

            response
        })
    }

    fn text(&self, ui: &mut Ui, text: &str) {
        let galley =
            WidgetText::from(text).into_galley(ui, Some(true), 80.0, FontSelection::default());
        let (rect, _) = ui.allocate_exact_size(
            Vec2::new(80.0, galley.size().y),
            Sense::focusable_noninteractive(),
        );
        let pos = Layout::top_down(Align::Center)
            .align_size_within_rect(galley.size(), rect)
            .min;

        if self.selected {
            let padding = (rect.width() - galley.size().x) * 0.5 - 2.0;
            let mut rect = rect;
            rect.min.x += padding;
            rect.max.x -= padding;
            ui.painter()
                .rect_filled(rect, Rounding::default(), ui.visuals().selection.bg_fill);
        }

        ui.painter()
            .galley_with_color(pos, galley.galley().clone(), Color32::GRAY);
    }

    fn size(&self, ui: &Ui, text: &str) -> Vec2 {
        let galley =
            WidgetText::from(text).into_galley(ui, Some(true), 80.0, FontSelection::default());
        Vec2::new(80.0, 60.0 + ui.spacing().item_spacing.y + galley.size().y)
    }
}

enum Preview {
    Icon(Icon),
    Widget(Box<dyn Widget>),
}

impl Preview {
    fn ui(
        &mut self,
        ui: &mut Ui,
        context: &mut EditorContext,
        engine: &mut EditorEngine,
        selected: bool,
    ) {
        let (rect, _) =
            ui.allocate_exact_size(Vec2::new(80.0, 60.0), Sense::focusable_noninteractive());

        match self {
            Preview::Icon(icon) => {
                let text = WidgetText::from(icon.size(40.0));
                let galley = text.into_galley(ui, None, 100.0, FontSelection::default());
                let pos = Layout::top_down(Align::Center)
                    .align_size_within_rect(galley.size(), rect)
                    .min;
                ui.painter().galley_with_color(
                    pos,
                    galley.galley().clone(),
                    Self::icon_color(ui, selected),
                );
            }
            Preview::Widget(widget) => {
                let pos = Layout::top_down(Align::Center)
                    .align_size_within_rect(Vec2::splat(50.0), rect)
                    .min;
                let mut ui = ui.child_ui(
                    Rect::from_min_size(pos, Vec2::splat(50.0)),
                    Layout::default(),
                );
                widget.ui(&mut ui, context, engine);

                if selected {
                    let color = ui.visuals().selection.bg_fill.to_srgba_unmultiplied();
                    let color = Color32::from_rgba_unmultiplied(color[0], color[1], color[2], 160);

                    ui.painter()
                        .rect_filled(ui.max_rect(), Rounding::default(), color)
                }
            }
        }
    }

    fn icon_color(ui: &Ui, selected: bool) -> Color32 {
        if selected {
            ui.visuals().selection.bg_fill
        } else {
            Color32::GRAY
        }
    }
}

struct CreateDirectory {
    path: PathBuf,
}

impl CreateDirectory {
    fn new(context: &EditorContext, name: &str) -> Self {
        Self {
            path: context.current_asset_path().join(name),
        }
    }
}

impl Action for CreateDirectory {
    fn description(&self) -> String {
        "Create Directory".to_string()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        fs::create_dir(&self.path).expect("create directory");
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        fs::remove_dir_all(&self.path).expect("remove directory");
    }
}

struct CreateAsset {
    assets: Arc<Assets>,
    asset_types: Rc<EditorAssetTypes>,
    absolute_path: PathBuf,
    path: PathBuf,
    display_name: &'static str,
}

impl CreateAsset {
    fn new(name: &str, extension: &str, context: &EditorContext, engine: &EditorEngine) -> Self {
        let absolute_path = context
            .current_asset_path()
            .join(name)
            .with_extension(extension);
        let path = absolute_path
            .strip_prefix(context.asset_path())
            .expect("relative asset path")
            .to_owned();

        Self {
            display_name: context
                .asset_types()
                .display_name(path.clone())
                .expect("display name"),
            path,
            absolute_path,
            asset_types: context.asset_types().clone(),
            assets: engine.assets().clone(),
        }
    }
}

impl Action for CreateAsset {
    fn description(&self) -> String {
        format!("Create {}", self.display_name)
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.asset_types
            .create(self.absolute_path.clone())
            .expect("create asset");
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.assets.remove(&self.path);
        fs::remove_file(&self.absolute_path).expect("remove asset");
    }
}

struct RenameAsset {
    new_path: PathBuf,
    old_path: PathBuf,
    assets_path: PathBuf,
}

impl RenameAsset {
    fn new(assets_path: PathBuf, old_path: PathBuf, new_path: PathBuf) -> Self {
        Self {
            new_path,
            old_path,
            assets_path,
        }
    }
}

impl Action for RenameAsset {
    fn description(&self) -> String {
        "Rename Asset".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        fs::rename(
            self.assets_path.join(&self.old_path),
            self.assets_path.join(&self.new_path),
        )
        .expect("rename asset");
        engine.assets().rename(&self.old_path, &self.new_path);
    }

    fn undo(&self, engine: &mut EditorEngine) {
        fs::rename(
            self.assets_path.join(&self.new_path),
            self.assets_path.join(&self.old_path),
        )
        .expect("rename asset");
        engine.assets().rename(&self.new_path, &self.old_path);
    }
}

struct RenameDirectory {
    new_path: PathBuf,
    old_path: PathBuf,
    assets_path: PathBuf,
}

impl RenameDirectory {
    fn new(assets_path: PathBuf, old_path: PathBuf, new_path: PathBuf) -> Self {
        Self {
            new_path,
            old_path,
            assets_path,
        }
    }
}

impl Action for RenameDirectory {
    fn description(&self) -> String {
        "Rename Directory".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        recursive_dir_iter(
            self.assets_path.join(&self.old_path),
            |path| {
                let old_path = path.strip_prefix(&self.assets_path).unwrap();
                let relative_path = path
                    .strip_prefix(self.assets_path.join(&self.old_path))
                    .unwrap();
                let new_path = self.new_path.join(relative_path);
                engine.assets().rename(old_path, new_path);
            },
            false,
        );
        fs::rename(
            self.assets_path.join(&self.old_path),
            self.assets_path.join(&self.new_path),
        )
        .expect("rename directory");
    }

    fn undo(&self, engine: &mut EditorEngine) {
        recursive_dir_iter(
            self.assets_path.join(&self.new_path),
            |path| {
                let new_path = path.strip_prefix(&self.assets_path).unwrap();
                let relative_path = path
                    .strip_prefix(self.assets_path.join(&self.new_path))
                    .unwrap();
                let old_path = self.old_path.join(relative_path);
                engine.assets().rename(new_path, old_path);
            },
            false,
        );
        fs::rename(
            self.assets_path.join(&self.new_path),
            self.assets_path.join(&self.old_path),
        )
        .expect("rename directory");
    }
}

struct MoveAsset {
    new_path: PathBuf,
    old_path: PathBuf,
    assets_path: PathBuf,
}

impl MoveAsset {
    fn new(assets_path: PathBuf, from: PathBuf, to: PathBuf) -> Self {
        Self {
            new_path: to.join(from.file_name().unwrap()),
            old_path: from,
            assets_path,
        }
    }
}

impl Action for MoveAsset {
    fn description(&self) -> String {
        "Move Asset".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        fs::rename(
            self.assets_path.join(&self.old_path),
            self.assets_path.join(&self.new_path),
        )
        .expect("move asset");
        engine.assets().rename(&self.old_path, &self.new_path);
    }

    fn undo(&self, engine: &mut EditorEngine) {
        fs::rename(
            self.assets_path.join(&self.new_path),
            self.assets_path.join(&self.old_path),
        )
        .expect("move asset");
        engine.assets().rename(&self.new_path, &self.old_path);
    }
}

struct MoveDirectory {
    new_path: PathBuf,
    old_path: PathBuf,
    assets_path: PathBuf,
}

impl MoveDirectory {
    fn new(assets_path: PathBuf, from: PathBuf, to: PathBuf) -> Self {
        Self {
            new_path: to.join(from.file_name().unwrap()),
            old_path: from,
            assets_path,
        }
    }
}

impl Action for MoveDirectory {
    fn description(&self) -> String {
        "Move Directory".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        recursive_dir_iter(
            self.assets_path.join(&self.old_path),
            |path| {
                let old_path = path.strip_prefix(&self.assets_path).unwrap();
                let relative_path = path
                    .strip_prefix(self.assets_path.join(&self.old_path))
                    .unwrap();
                let new_path = self.new_path.join(relative_path);
                engine.assets().rename(old_path, new_path);
            },
            false,
        );
        fs::rename(
            self.assets_path.join(&self.old_path),
            self.assets_path.join(&self.new_path),
        )
        .expect("move directory");
    }

    fn undo(&self, engine: &mut EditorEngine) {
        recursive_dir_iter(
            self.assets_path.join(&self.new_path),
            |path| {
                let new_path = path.strip_prefix(&self.assets_path).unwrap();
                let relative_path = path
                    .strip_prefix(self.assets_path.join(&self.new_path))
                    .unwrap();
                let old_path = self.old_path.join(relative_path);
                engine.assets().rename(new_path, old_path);
            },
            false,
        );
        fs::rename(
            self.assets_path.join(&self.new_path),
            self.assets_path.join(&self.old_path),
        )
        .expect("move asset");
    }
}

struct DeleteAsset {
    new_path: PathBuf,
    old_path: PathBuf,
    path: PathBuf,
}

impl DeleteAsset {
    fn new(delete_dir: PathBuf, assets_path: PathBuf, path: PathBuf) -> Self {
        let tmp_file_name = Alphanumeric.sample_string(&mut rand::thread_rng(), 10);

        Self {
            new_path: delete_dir.join(tmp_file_name),
            old_path: assets_path.join(&path),
            path,
        }
    }
}

impl Action for DeleteAsset {
    fn description(&self) -> String {
        "Delete Asset".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        engine.assets().remove(&self.path);
        fs::copy(&self.old_path, &self.new_path).expect("copy asset");
        fs::remove_file(&self.old_path).expect("remove asset");
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        fs::copy(&self.new_path, &self.old_path).expect("copy tmp");
        fs::remove_file(&self.new_path).expect("remove tmp")
    }
}

struct DeleteDirectory {
    new_path: PathBuf,
    old_path: PathBuf,
    assets_path: PathBuf,
}

impl DeleteDirectory {
    fn new(delete_dir: PathBuf, assets_path: PathBuf, path: PathBuf) -> Self {
        let tmp_file_name = Alphanumeric.sample_string(&mut rand::thread_rng(), 10);

        Self {
            new_path: delete_dir.join(tmp_file_name),
            old_path: assets_path.join(&path),
            assets_path,
        }
    }
}

impl Action for DeleteDirectory {
    fn description(&self) -> String {
        "Delete Asset".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        recursive_dir_iter(
            &self.old_path,
            |path| {
                engine
                    .assets()
                    .remove(path.strip_prefix(&self.assets_path).unwrap());
            },
            false,
        );
        recursive_dir_iter(
            &self.old_path,
            |path| {
                let new_path = self
                    .new_path
                    .join(path.strip_prefix(&self.old_path).unwrap());
                if path.is_dir() {
                    fs::create_dir_all(new_path).expect("create dir");
                } else {
                    fs::copy(path, new_path).expect("copy asset");
                }
            },
            true,
        );
        fs::remove_dir_all(&self.old_path).expect("remove directory");
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        recursive_dir_iter(
            &self.new_path,
            |path| {
                let old_path = self
                    .old_path
                    .join(path.strip_prefix(&self.new_path).unwrap());
                if path.is_dir() {
                    fs::create_dir_all(old_path).expect("create dir");
                } else {
                    fs::copy(path, old_path).expect("copy asset");
                }
            },
            true,
        );
        fs::remove_dir_all(&self.new_path).expect("remove tmp")
    }
}

fn recursive_dir_iter(path: impl AsRef<Path>, consumer: impl Fn(&Path), consume_dirs: bool) {
    let mut stack = vec![path.as_ref().to_path_buf()];
    while let Some(path) = stack.pop() {
        for path in fs::read_dir(path)
            .unwrap()
            .map(|read_dir| read_dir.unwrap().path())
        {
            if path.is_file() {
                consumer(&path);
            } else {
                if consume_dirs {
                    consumer(&path);
                }
                stack.push(path);
            }
        }
    }
}
