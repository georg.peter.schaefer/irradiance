use std::fmt::Display;

use egui::{Id, Ui};

use crate::core::EditorEngine;
use crate::gui::widgets::Widget;
use crate::gui::EditorContext;

/// Combo box.
pub struct ComboBox<T> {
    values: Vec<T>,
    selected: usize,
    changed: bool,
    id: Id,
}

impl<T: Clone + PartialEq> ComboBox<T> {
    /// Starts building a new combo box.
    pub fn builder() -> ComboBoxBuilder<T> {
        ComboBoxBuilder {
            values: Default::default(),
        }
    }

    /// Returns if the selected value has changed.
    pub fn changed(&self) -> bool {
        self.changed
    }

    /// Returns the selected value.
    pub fn get(&self) -> T {
        self.values[self.selected].clone()
    }

    /// Sets the selected value.
    pub fn set(&mut self, new_value: T) {
        if let Some(position) = self.values.iter().position(|value| *value == new_value) {
            self.selected = position;
            self.changed = true;
        }
    }
}

impl<T: Display> Widget for ComboBox<T> {
    fn ui(&mut self, ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
        self.changed = egui::ComboBox::from_id_source(self.id.clone())
            .show_index(ui, &mut self.selected, self.values.len(), |index| {
                format!("{}", &self.values[index])
            })
            .changed();
    }
}

/// Type for building a combo box.
pub struct ComboBoxBuilder<T> {
    values: Vec<T>,
}

impl<T: Display> ComboBoxBuilder<T> {
    /// Adds a value.
    pub fn value(mut self, value: T) -> Self {
        self.values.push(value);
        self
    }

    /// Builds the combo box.
    pub fn build(self) -> ComboBox<T> {
        let mut id = Id::new("combo_box");
        for value in &self.values {
            id = id.with(format!("_{value}"));
        }

        ComboBox {
            values: self.values,
            selected: 0,
            changed: false,
            id,
        }
    }
}
