use std::sync::Arc;

use egui::style::Margin;
use egui::{Color32, Frame, Key, Sense, Ui};
use irradiance_runtime::ecs::Component;
use irradiance_runtime::{core::Engine, ecs::Entities};

use super::{Tab, Widget};
use crate::gui::message::{
    Create, Delete, Duplicate, Inspect, Message, OpenCreateDialog, OpenDeleteDialog, OpenDialog,
    OpenDuplicateDialog, OpenRenameDialog, Recv, Rename, Repaint,
};
use crate::gui::widgets::MenuEntry;
use crate::gui::{Action, Icons};
use crate::{core::EditorEngine, gui::EditorContext};

/// List of entities with components.
#[derive(Default)]
pub struct EntityList {
    state: Option<State>,
}

impl Widget for EntityList {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Inspect::Screen(screen)) = context.message_bus().recv() {
            self.state = engine
                .scenes()
                .get(screen)
                .map(|scene| scene.entities().clone())
                .map(|entities| State::new(entities));
        }
        if let Some(state) = &mut self.state {
            if let Some(Create::Entity(id)) = context.message_bus().recv().cloned() {
                context.actions_mut().push(
                    engine,
                    CreateEntity::new(id.clone(), state.entities.clone()),
                );
                state.refresh();
                context
                    .message_bus_mut()
                    .post(Message::Inspect(Inspect::Entity(
                        id,
                        state.entities.clone(),
                    )));
            }
            if let Some(Rename::Entity(old, new)) = context.message_bus().recv().cloned() {
                context
                    .actions_mut()
                    .push(engine, RenameEntity::new(old, new, state.entities.clone()));
                state.refresh();
            }
            if let Some(Delete::Entity(id)) = context.message_bus().recv().cloned() {
                context
                    .actions_mut()
                    .push(engine, DeleteEntity::new(id, state.entities.clone()));
                state.refresh();
            }
            if let Some(Duplicate::Entity(old, new)) = context.message_bus().recv().cloned() {
                context.actions_mut().push(
                    engine,
                    DuplicateEntity::new(old, new.clone(), state.entities.clone()),
                );
                context
                    .message_bus_mut()
                    .post(Message::Inspect(Inspect::Entity(
                        new,
                        state.entities.clone(),
                    )));
                state.refresh();
            }
            if let Some(Repaint::All) = context.message_bus().recv() {
                state.refresh();
            }

            for entry in &mut state.entries {
                entry.handle_messages(context, engine);
            }
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.set_clip_rect(ui.max_rect());
        egui::ScrollArea::both()
            .auto_shrink([false, false])
            .show(ui, |ui| {
                let response = Frame::default()
                    .show(ui, |ui| {
                        ui.set_width(ui.available_width());
                        ui.set_height(ui.available_height());

                        if let Some(state) = &mut self.state {
                            for entry in &mut state.entries {
                                entry.ui(ui, context, engine)
                            }
                        }
                    })
                    .response
                    .interact(Sense::click());

                response.context_menu(|ui| {
                    new_entry(
                        ui,
                        context,
                        self.state.as_ref().expect("state").entities.clone(),
                    );
                });
            });
    }
}

impl Tab for EntityList {
    fn title(&self) -> &str {
        "Entities"
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn std::any::Any {
        self
    }
}

struct State {
    entries: Vec<EntityEntry>,
    entities: Arc<Entities>,
}

impl State {
    fn new(entities: Arc<Entities>) -> Self {
        let mut state = Self {
            entities,
            entries: Default::default(),
        };
        state.refresh();
        state
    }

    fn refresh(&mut self) {
        let mut entities = self.entities.all();
        entities.sort_by_key(|entity| entity.id().to_string());
        self.entries = entities
            .iter()
            .map(|entity| EntityEntry::new(self.entities.clone(), entity.id().to_string()))
            .collect();
    }
}

struct EntityEntry {
    selected: bool,
    id: String,
    entities: Arc<Entities>,
}

impl EntityEntry {
    fn new(entities: Arc<Entities>, id: String) -> Self {
        Self {
            selected: false,
            id,
            entities,
        }
    }
}

impl Widget for EntityEntry {
    fn handle_messages(&mut self, context: &mut EditorContext, _engine: &mut EditorEngine) {
        if let Some(Inspect::Entity(id, _)) = context.message_bus().recv() {
            self.selected = self.id == *id;
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, _engine: &mut EditorEngine) {
        let response = Frame::default()
            .inner_margin(Margin::symmetric(10.0, 5.0))
            .fill(fill(ui, self.selected))
            .show(ui, |ui| {
                ui.set_width(ui.available_width());

                ui.label(&self.id);
            })
            .response
            .interact(Sense::click());

        if response.clicked() {
            self.selected = true;
            context
                .message_bus_mut()
                .post(Message::Inspect(Inspect::Entity(
                    self.id.clone(),
                    self.entities.clone(),
                )));
        } else if response.clicked_elsewhere() {
            self.selected = false;
        }

        if self.selected && ui.input().key_pressed(Key::Delete) {
            self.delete(context);
        }

        response.context_menu(|ui| {
            self.selected = true;

            new_entry(ui, context, self.entities.clone());

            ui.separator();

            if ui.add(MenuEntry::new("Duplicate")).clicked() {
                context
                    .message_bus_mut()
                    .post(Message::OpenDialog(OpenDialog::Duplicate(
                        OpenDuplicateDialog::Entity(self.id.clone(), self.entities.clone()),
                    )));
                ui.close_menu();
            }
            if ui.add(MenuEntry::new("Rename")).clicked() {
                context
                    .message_bus_mut()
                    .post(Message::OpenDialog(OpenDialog::Rename(
                        OpenRenameDialog::Entity(self.id.clone(), self.entities.clone()),
                    )));
                ui.close_menu();
            }
            if ui.add(MenuEntry::new("Delete")).clicked() {
                self.delete(context);
                ui.close_menu();
            }
        });
    }
}

impl EntityEntry {
    fn delete(&self, context: &mut EditorContext) {
        context
            .message_bus_mut()
            .post(Message::OpenDialog(OpenDialog::Delete(
                OpenDeleteDialog::Entity(self.id.clone()),
            )));
    }
}

fn fill(ui: &Ui, selected: bool) -> Color32 {
    if selected {
        ui.visuals().selection.bg_fill
    } else {
        Default::default()
    }
}

fn new_entry(ui: &mut Ui, context: &mut EditorContext, entities: Arc<Entities>) {
    MenuEntry::new("New").sub_menu(ui, |ui| {
        if ui
            .add(MenuEntry::new("  Entity").icon(Icons::chess_pawn()))
            .clicked()
        {
            context
                .message_bus_mut()
                .post(Message::OpenDialog(OpenDialog::Create(
                    OpenCreateDialog::Entity(entities),
                )));
            ui.close_menu();
        }
    });
}

struct CreateEntity {
    entities: Arc<Entities>,
    id: String,
}

impl CreateEntity {
    fn new(id: String, entities: Arc<Entities>) -> Self {
        Self { id, entities }
    }
}

impl Action for CreateEntity {
    fn description(&self) -> String {
        "Create Entity".to_owned()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        if self.entities.create(self.id.clone()).is_ok() {
            unsafe {
                self.entities.commit();
            }
        }
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        if let Some(entity) = self.entities.get(self.id.clone()) {
            self.entities.remove(&entity);
            unsafe {
                self.entities.commit();
            }
        }
    }
}

struct DeleteEntity {
    components: Vec<Arc<dyn Component>>,
    entities: Arc<Entities>,
    entity: String,
}

impl DeleteEntity {
    fn new(entity: String, entities: Arc<Entities>) -> Self {
        let facade = entities.get(&entity).expect("entity");

        Self {
            components: facade
                .types()
                .into_iter()
                .flat_map(|component_type| facade.get_dyn(component_type))
                .collect(),
            entities,
            entity,
        }
    }
}

impl Action for DeleteEntity {
    fn description(&self) -> String {
        "Delete Entity".to_owned()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        if let Some(entity) = self.entities.get(&self.entity) {
            self.entities.remove(&entity);
            unsafe {
                self.entities.commit();
            }
        }
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        if let Ok(entity) = self.entities.create(self.entity.clone()) {
            for component in &self.components {
                entity.insert_dyn(component.clone());
            }
            unsafe {
                self.entities.commit();
            }
        }
    }
}

struct RenameEntity {
    components: Vec<Arc<dyn Component>>,
    entities: Arc<Entities>,
    new_name: String,
    old_name: String,
}

impl RenameEntity {
    fn new(old_name: String, new_name: String, entities: Arc<Entities>) -> Self {
        let facade = entities.get(&old_name).expect("entity");

        Self {
            components: facade
                .types()
                .into_iter()
                .flat_map(|component_type| facade.get_dyn(component_type))
                .collect(),
            entities,
            new_name,
            old_name,
        }
    }
}

impl Action for RenameEntity {
    fn description(&self) -> String {
        "Rename Entity".to_owned()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        if let Some(entity) = self.entities.get(&self.old_name) {
            self.entities.remove(&entity);

            if let Ok(entity) = self.entities.create(self.new_name.clone()) {
                for component in &self.components {
                    entity.insert_dyn(component.clone());
                }
            }

            unsafe {
                self.entities.commit();
            }
        }
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        if let Some(entity) = self.entities.get(&self.new_name) {
            self.entities.remove(&entity);

            if let Ok(entity) = self.entities.create(self.old_name.clone()) {
                for component in &self.components {
                    entity.insert_dyn(component.clone());
                }
            }

            unsafe {
                self.entities.commit();
            }
        }
    }
}

struct DuplicateEntity {
    components: Vec<Arc<dyn Component>>,
    entities: Arc<Entities>,
    new_name: String,
}

impl DuplicateEntity {
    fn new(old_name: String, new_name: String, entities: Arc<Entities>) -> Self {
        let facade = entities.get(&old_name).expect("entity");

        Self {
            components: facade
                .types()
                .into_iter()
                .flat_map(|component_type| facade.get_dyn(component_type))
                .collect(),
            entities,
            new_name,
        }
    }
}

impl Action for DuplicateEntity {
    fn description(&self) -> String {
        "Duplicate Entity".to_owned()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        if let Ok(entity) = self.entities.create(self.new_name.clone()) {
            for component in &self.components {
                entity.insert_dyn(component.duplicate());
            }
        }

        unsafe {
            self.entities.commit();
        }
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        if let Some(entity) = self.entities.get(&self.new_name) {
            self.entities.remove(&entity);

            unsafe {
                self.entities.commit();
            }
        }
    }
}
