use crate::core::EditorEngine;
use crate::gui::widgets::Widget;
use crate::gui::EditorContext;
use egui::Ui;

/// A text edit.
#[derive(Default)]
pub struct TextEdit {
    old_value: String,
    value: String,
    changing: bool,
    changed: bool,
}

impl TextEdit {
    /// Returns if the value has changed.
    pub fn changed(&self) -> bool {
        self.changed
    }

    /// Returns if the value is changing.
    pub fn changing(&self) -> bool {
        self.changing
    }

    /// Returns the value.
    pub fn get(&self) -> &String {
        &self.value
    }

    /// Returns the old value.
    pub fn get_old(&self) -> &String {
        &self.old_value
    }

    /// Sets the value.
    pub fn set(&mut self, value: impl Into<String>) {
        self.value = value.into();
        self.old_value = self.value.clone();
    }
}

impl Widget for TextEdit {
    fn ui(&mut self, ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
        if self.changed {
            self.old_value = self.value.clone();
        }
        self.changed = false;

        let response = ui.add(egui::TextEdit::singleline(&mut self.value));

        if self.changing && !response.changed() {
            self.changed = true;
        }
        self.changing = response.changed();
    }
}
