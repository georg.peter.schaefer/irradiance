use egui::{SidePanel, Ui};

use crate::{core::EditorEngine, gui::EditorContext};

use super::{Inspector, Tabs, Widget};

pub struct RightPanel {
    tabs: Tabs,
}

impl Default for RightPanel {
    fn default() -> Self {
        Self {
            tabs: Tabs::builder().tab(Inspector::default()).build(),
        }
    }
}

impl Widget for RightPanel {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.tabs.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.style_mut().spacing.item_spacing = egui::Vec2::ZERO;
        SidePanel::right("right_panel")
            .frame(egui::Frame::default().fill(ui.style().visuals.window_fill()))
            .default_width(405.0)
            .show_inside(ui, |ui| {
                ui.reset_style();
                ui.set_enabled(!context.is_playing());

                self.tabs.ui(ui, context, engine);
            });
    }
}
