use egui::{menu, TopBottomPanel, Ui};
use irradiance_runtime::core::{Engine, Screen};

use crate::core::EditorScreen;
use crate::gui::message::{OpenCreateDialog, OpenTab};
use crate::{
    core::EditorEngine,
    gui::{
        message::{Close, Message, OpenDialog, Repaint},
        EditorContext, Icons,
    },
};

use super::{MenuEntry, NewSubMenu, Widget};

pub struct MenuBar {
    view: View,
    edit: Edit,
    file: File,
}

impl MenuBar {
    pub fn new(context: &EditorContext) -> Self {
        Self {
            file: File::new(context),
            edit: Default::default(),
            view: Default::default(),
        }
    }
}

impl Widget for MenuBar {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.style_mut().spacing.item_spacing = egui::Vec2::ZERO;
        TopBottomPanel::top("top_panel").show_inside(ui, |ui| {
            ui.reset_style();
            ui.set_enabled(!context.is_playing());

            menu::bar(ui, |ui| {
                self.file.ui(ui, context, engine);
                self.edit.ui(ui, context, engine);
                self.view.ui(ui, context, engine);
            });
        });
    }
}

struct File {
    new_sub_menu: NewSubMenu,
}

impl File {
    fn new(context: &EditorContext) -> Self {
        Self {
            new_sub_menu: NewSubMenu::new(context),
        }
    }
}

impl Widget for File {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.menu_button("File", |ui| {
            MenuEntry::new("New").sub_menu(ui, |ui| {
                if ui
                    .add(MenuEntry::new("  Entity").icon(Icons::chess_pawn()))
                    .clicked()
                {
                    context
                        .message_bus_mut()
                        .post(Message::OpenDialog(OpenDialog::Create(
                            OpenCreateDialog::Entity(
                                engine
                                    .screens()
                                    .get::<EditorScreen>("__internal_editor_screen__")
                                    .expect("editor screen")
                                    .entities()
                                    .expect("entities")
                                    .clone(),
                            ),
                        )));
                    ui.close_menu();
                }

                ui.separator();

                self.new_sub_menu.ui(ui, context, engine);
            });

            ui.separator();

            if ui
                .add_enabled(
                    context.actions().has_pending_changes(),
                    MenuEntry::new("Save").icon(Icons::floppy_disk()),
                )
                .clicked()
            {
                engine.assets().write_all().expect("wirte unsaved changes");
                context.actions_mut().clear_pending_changes();
                ui.close_menu();
            }

            ui.separator();
            if ui.add(MenuEntry::new("Exit")).clicked() {
                if context.actions().has_pending_changes() {
                    context
                        .message_bus_mut()
                        .post(Message::OpenDialog(OpenDialog::Close))
                } else {
                    context.message_bus_mut().post(Message::Close(Close))
                }
                ui.close_menu();
            }
        });
    }
}

#[derive(Default)]
struct Edit;

impl Widget for Edit {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.menu_button("Edit", |ui| {
            if ui
                .add_enabled(
                    context.actions_mut().has_undos(),
                    MenuEntry::new(format!(
                        "Undo {}",
                        context
                            .actions_mut()
                            .next_undo_description()
                            .unwrap_or_default(),
                    ))
                    .icon(Icons::reply()),
                )
                .clicked()
            {
                context.actions_mut().undo(engine);
                context
                    .message_bus_mut()
                    .post(Message::Repaint(Repaint::All));
                ui.close_menu();
            }
            if ui
                .add_enabled(
                    context.actions_mut().has_redos(),
                    MenuEntry::new(format!(
                        "Redo {}",
                        context
                            .actions_mut()
                            .next_redo_description()
                            .unwrap_or_default()
                    ))
                    .icon(Icons::share()),
                )
                .clicked()
            {
                context.actions_mut().redo(engine);
                context
                    .message_bus_mut()
                    .post(Message::Repaint(Repaint::All));
                ui.close_menu();
            }
        });
    }
}

#[derive(Default)]
struct View;

impl Widget for View {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, _engine: &mut EditorEngine) {
        ui.menu_button("View", |ui| {
            if ui
                .add(MenuEntry::new("Input").icon(Icons::keyboard()))
                .clicked()
            {
                context
                    .message_bus_mut()
                    .post(Message::OpenTab(OpenTab::Input));
                ui.close_menu();
            }
        });
    }
}
