use egui::{SidePanel, Ui};

use crate::{core::EditorEngine, gui::EditorContext};

use super::{EntityList, ScreenList, Tabs, Widget};

pub struct LeftPanel {
    tabs: Tabs,
}

impl LeftPanel {
    pub fn new(context: &mut EditorContext, engine: &EditorEngine) -> Self {
        Self {
            tabs: Tabs::builder()
                .tab(ScreenList::new(context, engine))
                .tab(EntityList::default())
                .build(),
        }
    }
}

impl Widget for LeftPanel {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.tabs.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.style_mut().spacing.item_spacing = egui::Vec2::ZERO;
        SidePanel::left("left_panel")
            .frame(egui::Frame::default().fill(ui.style().visuals.window_fill()))
            .default_width(405.0)
            .show_inside(ui, |ui| {
                ui.reset_style();
                ui.set_enabled(!context.is_playing());

                self.tabs.ui(ui, context, engine);
            });
    }
}
