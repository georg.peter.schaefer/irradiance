use std::any::Any;
use std::path::PathBuf;

use egui::style::Margin;
use egui::{
    Align, Button, CentralPanel, Color32, Frame, Grid, Key, Layout, RichText, ScrollArea, Sense,
    SidePanel, Stroke, Ui,
};

use irradiance_runtime::core::Engine;
use irradiance_runtime::input::{
    AxisBinding, Binding, ButtonAction, ButtonBinding, InputAction, InputActionMap, ScalarAction,
    ScalarBinding, ScalarCompoundBinding, ValueAction, Vector2DAction, Vector2DAxisBinding,
    Vector2DBinding, Vector2DCompoundBinding,
};

use crate::core::EditorEngine;
use crate::gui::message::{
    Create, Delete, Message, OpenCreateDialog, OpenDeleteDialog, OpenDialog, OpenRenameDialog,
    Recv, Rename, Repaint,
};
use crate::gui::widgets::{Checkbox, ComboBox, MenuEntry, Tab, Widget};
use crate::gui::{Action, EditorContext, Icons};

#[derive(Default)]
pub struct InputInspector {
    properties_inspector: PropertiesInspector,
    action_map_inspector: ActionMapInspector,
    action_maps_inspector: ActionMapsInspector,
}

impl Widget for InputInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.action_maps_inspector.handle_messages(context, engine);
        self.action_map_inspector.handle_messages(context, engine);
        self.properties_inspector.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.action_maps_inspector.ui(ui, context, engine);
        self.properties_inspector.ui(ui, context, engine);
        self.action_map_inspector.ui(ui, context, engine);

        if self.action_maps_inspector.selection_changed {
            if let Some(action_map) = &self.action_maps_inspector.selected {
                self.action_map_inspector = ActionMapInspector::new(action_map.clone());
            }
        }

        if self.action_map_inspector.selection_changed
            || self.action_maps_inspector.selection_changed
        {
            if let Some(entry) = &self.action_map_inspector.selected {
                self.properties_inspector = PropertiesInspector::new(
                    engine,
                    self.action_map_inspector.action_map.clone(),
                    entry,
                );
            } else {
                self.properties_inspector = PropertiesInspector::default();
            }
        }

        self.action_maps_inspector.selection_changed = false;
        self.action_map_inspector.selection_changed = false;
    }
}

impl Tab for InputInspector {
    fn title(&self) -> &str {
        "Input"
    }

    fn is_closeable(&self) -> bool {
        true
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

#[derive(Default)]
struct ActionMapsInspector {
    selection_changed: bool,
    selected: Option<String>,
}

impl Widget for ActionMapsInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Create::ActionMap(name)) = context.message_bus().recv().cloned() {
            context
                .actions_mut()
                .push(engine, CreateActionMap::new(name.clone()));
            self.selected = Some(name);
            self.selection_changed = true;
        }
        if let Some(Rename::ActionMap(old, new)) = context.message_bus().recv().cloned() {
            context
                .actions_mut()
                .push(engine, RenameActionMap::new(old, new.clone()));
            self.selected = Some(new);
            self.selection_changed = true;
        }
        if let Some(Delete::ActionMap(name)) = context.message_bus().recv().cloned() {
            context
                .actions_mut()
                .push(engine, DeleteActionMap::new(engine, name));
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.spacing_mut().item_spacing.x = 0.0;

        let width = ui.available_width() / 3.0;
        SidePanel::left("input_left")
            .frame(Frame::default().fill(ui.style().visuals.window_fill()))
            .default_width(width)
            .min_width(width)
            .max_width(width)
            .resizable(false)
            .show_inside(ui, |ui| {
                let item_spacing = ui.spacing_mut().item_spacing;
                ui.spacing_mut().item_spacing.y = 0.0;

                Frame::default()
                    .inner_margin(Margin::symmetric(10.0, 5.0))
                    .stroke(Stroke::new(0.1, Color32::GRAY))
                    .show(ui, |ui| {
                        ui.horizontal(|ui| {
                            ui.set_width(ui.available_width());

                            ui.label(RichText::new("Action Maps").strong());
                            ui.with_layout(Layout::right_to_left(Align::Center), |ui| {
                                if ui.add(Button::new(Icons::plus()).frame(false)).clicked() {
                                    context.message_bus_mut().post(Message::OpenDialog(
                                        OpenDialog::Create(OpenCreateDialog::ActionMap),
                                    ));
                                }
                            });
                        });
                    });

                ui.spacing_mut().item_spacing = item_spacing;

                Frame::default()
                    .stroke(Stroke::new(0.1, Color32::GRAY))
                    .show(ui, |ui| {
                        ScrollArea::both()
                            .auto_shrink([false, false])
                            .show(ui, |ui| {
                                for action_map in engine.input().action_maps().0.keys() {
                                    let response = Frame::default()
                                        .inner_margin(Margin::symmetric(10.0, 5.0))
                                        .fill(fill(ui, self.is_selected(action_map)))
                                        .show(ui, |ui| {
                                            ui.horizontal(|ui| {
                                                ui.set_width(ui.available_width());

                                                ui.label(action_map);
                                            });
                                        })
                                        .response
                                        .interact(Sense::click());

                                    if response.clicked() {
                                        self.selected = Some(action_map.clone());
                                        self.selection_changed = true;
                                    } else if response.clicked_elsewhere() {
                                        if self.is_selected(action_map) {
                                            self.selected = None;
                                        }
                                    }

                                    if self.is_selected(action_map)
                                        && ui.input().key_pressed(Key::Delete)
                                    {
                                        self.delete(context, action_map.clone());
                                    }

                                    response.context_menu(|ui| {
                                        self.selected = Some(action_map.clone());
                                        self.selection_changed = true;

                                        if ui.add(MenuEntry::new("Rename")).clicked() {
                                            context.message_bus_mut().post(Message::OpenDialog(
                                                OpenDialog::Rename(OpenRenameDialog::ActionMap(
                                                    action_map.clone(),
                                                )),
                                            ));
                                            ui.close_menu();
                                        }
                                        if ui.add(MenuEntry::new("Delete")).clicked() {
                                            self.delete(context, action_map.clone());
                                            ui.close_menu();
                                        }
                                    });
                                }
                            });
                    });
            });
    }
}

impl ActionMapsInspector {
    fn is_selected(&self, action_map: &String) -> bool {
        self.selected.as_ref() == Some(action_map)
    }

    fn delete(&self, context: &mut EditorContext, action_map: String) {
        context
            .message_bus_mut()
            .post(Message::OpenDialog(OpenDialog::Delete(
                OpenDeleteDialog::ActionMap(action_map),
            )));
    }
}

#[derive(Default)]
struct ActionMapInspector {
    selection_changed: bool,
    selected: Option<Entry>,
    action_map: String,
}

impl ActionMapInspector {
    fn new(action_map: String) -> Self {
        Self {
            selection_changed: false,
            selected: None,
            action_map,
        }
    }
}

impl Widget for ActionMapInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Create::Action(action_map, name)) = context.message_bus().recv().cloned() {
            context
                .actions_mut()
                .push(engine, CreateAction::new(action_map, name.clone()));
            self.selected = Some(Entry::Action(name));
            self.selection_changed = true;
        }
        if let Some(Create::Binding(action_map, action, binding)) =
            context.message_bus().recv().cloned()
        {
            context.actions_mut().push(
                engine,
                CreateBinding::new(action_map, action.clone(), binding.clone()),
            );
        }
        if let Some(Rename::Action(action_map, old, new)) = context.message_bus().recv().cloned() {
            context
                .actions_mut()
                .push(engine, RenameAction::new(action_map, old, new.clone()));
            self.selected = Some(Entry::Action(new));
            self.selection_changed = true;
        }
        if let Some(Rename::Binding(action_map, action, old, new)) =
            context.message_bus().recv().cloned()
        {
            context.actions_mut().push(
                engine,
                RenameBinding::new(action_map, action.clone(), old, new.clone()),
            );
        }
        if let Some(Delete::Action(action_map, name)) = context.message_bus().recv().cloned() {
            context
                .actions_mut()
                .push(engine, DeleteAction::new(engine, action_map, name));
        }
        if let Some(Delete::Binding(action_map, action, binding)) =
            context.message_bus().recv().cloned()
        {
            context.actions_mut().push(
                engine,
                DeleteBinding::new(engine, action_map, action, binding),
            );
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        CentralPanel::default()
            .frame(Frame::default().fill(ui.style().visuals.window_fill()))
            .show_inside(ui, |ui| {
                let item_spacing = ui.spacing_mut().item_spacing;
                ui.spacing_mut().item_spacing.y = 0.0;

                Frame::default()
                    .inner_margin(Margin::symmetric(10.0, 5.0))
                    .stroke(Stroke::new(0.1, Color32::GRAY))
                    .show(ui, |ui| {
                        ui.horizontal(|ui| {
                            ui.set_width(ui.available_width());

                            ui.label(RichText::new("Actions").strong());
                            ui.with_layout(Layout::right_to_left(Align::Center), |ui| {
                                if ui
                                    .add_enabled(
                                        engine.input().action_map(&self.action_map).is_some(),
                                        Button::new(Icons::plus()).frame(false),
                                    )
                                    .clicked()
                                {
                                    context.message_bus_mut().post(Message::OpenDialog(
                                        OpenDialog::Create(OpenCreateDialog::Action(
                                            self.action_map.clone(),
                                        )),
                                    ));
                                }
                            });
                        });
                    });

                ui.spacing_mut().item_spacing = item_spacing;

                Frame::default()
                    .stroke(Stroke::new(0.1, Color32::GRAY))
                    .show(ui, |ui| {
                        ScrollArea::both()
                            .auto_shrink([false, false])
                            .show(ui, |ui| {
                                if let Some(action_map) =
                                    engine.input().action_map(&self.action_map)
                                {
                                    let entries = Entry::from_action_map(action_map);
                                    for entry in entries {
                                        let response = Frame::default()
                                            .inner_margin(Margin::symmetric(10.0, 5.0))
                                            .fill(fill(ui, self.is_selected(&entry)))
                                            .show(ui, |ui| {
                                                ui.horizontal(|ui| {
                                                    ui.set_width(ui.available_width());

                                                    match &entry {
                                                        Entry::Action(action) => {
                                                            ui.label(entry.label());

                                                            ui.with_layout(Layout::right_to_left(Align::Center), |ui| {
                                                                if ui
                                                                    .add(
                                                                        Button::new(Icons::plus())
                                                                            .frame(false)
                                                                            .small(),
                                                                    )
                                                                    .clicked()
                                                                {
                                                                    context.message_bus_mut().post(
                                                                        Message::OpenDialog(
                                                                            OpenDialog::Create(
                                                                                OpenCreateDialog::Binding(
                                                                                    self.action_map.clone(),
                                                                                    action.clone(),
                                                                                ),
                                                                            ),
                                                                        ),
                                                                    );
                                                                }
                                                            });
                                                        }
                                                        Entry::Binding(_, _, _) => {
                                                            ui.add_space(4.0);
                                                            ui.separator();
                                                            ui.add_space(4.0);
                                                            ui.label(entry.label());
                                                        }
                                                        Entry::MultiBinding(_, _, _, _) => {
                                                            ui.add_space(18.0);
                                                            ui.separator();
                                                            ui.add_space(4.0);
                                                            ui.label(entry.label());
                                                        }
                                                    }
                                                });
                                            })
                                            .response
                                            .interact(Sense::click());

                                        if response.clicked() {
                                            self.selected = Some(entry.clone());
                                            self.selection_changed = true;
                                        } else if response.clicked_elsewhere() {
                                            if self.is_selected(&entry) {
                                                self.selected = None;
                                            }
                                        }

                                        if entry.has_context_menu()
                                            && self.is_selected(&entry)
                                            && ui.input().key_pressed(Key::Delete)
                                        {
                                            self.delete(context, &entry);
                                        }

                                        if entry.has_context_menu() {
                                            response.context_menu(|ui| {
                                                self.selected = Some(entry.clone());
                                                self.selection_changed = true;
                                                self.context_menu(ui, context, &entry);
                                            });
                                        }
                                    }
                                }
                            });
                    });
            });
    }
}

impl ActionMapInspector {
    fn is_selected(&self, entry: &Entry) -> bool {
        self.selected.as_ref() == Some(entry)
    }

    fn context_menu(&mut self, ui: &mut Ui, context: &mut EditorContext, entry: &Entry) {
        if ui.add(MenuEntry::new("Rename")).clicked() {
            self.rename(context, entry);
            ui.close_menu();
        }
        if ui.add(MenuEntry::new("Delete")).clicked() {
            self.delete(context, entry);
            ui.close_menu();
        }
    }

    fn rename(&mut self, context: &mut EditorContext, entry: &Entry) {
        match entry {
            Entry::Action(action) => {
                context
                    .message_bus_mut()
                    .post(Message::OpenDialog(OpenDialog::Rename(
                        OpenRenameDialog::Action(self.action_map.clone(), action.clone()),
                    )))
            }
            Entry::Binding(action, binding, _) => {
                context
                    .message_bus_mut()
                    .post(Message::OpenDialog(OpenDialog::Rename(
                        OpenRenameDialog::Binding(
                            self.action_map.clone(),
                            action.clone(),
                            binding.clone(),
                        ),
                    )))
            }
            Entry::MultiBinding(_, _, _, _) => {}
        }
    }

    fn delete(&mut self, context: &mut EditorContext, entry: &Entry) {
        match entry {
            Entry::Action(action) => {
                context
                    .message_bus_mut()
                    .post(Message::OpenDialog(OpenDialog::Delete(
                        OpenDeleteDialog::Action(self.action_map.clone(), action.clone()),
                    )))
            }
            Entry::Binding(action, binding, _) => {
                context
                    .message_bus_mut()
                    .post(Message::OpenDialog(OpenDialog::Delete(
                        OpenDeleteDialog::Binding(
                            self.action_map.clone(),
                            action.clone(),
                            binding.clone(),
                        ),
                    )))
            }
            Entry::MultiBinding(_, _, _, _) => {}
        }
        self.selected = None;
        self.selection_changed = true;
    }
}

#[derive(Clone, Eq, PartialEq)]
enum Entry {
    Action(String),
    Binding(String, String, Option<String>),
    MultiBinding(String, String, String, Option<String>),
}

impl Entry {
    fn from_action_map(action_map: &InputActionMap) -> Vec<Entry> {
        let mut entries = vec![];
        for (action_name, action) in &action_map.0 {
            entries.push(Entry::Action(action_name.clone()));

            match action {
                InputAction::Button(button_action) => {
                    for (binding_name, binding) in &button_action.bindings {
                        entries.push(Entry::Binding(
                            action_name.clone(),
                            binding_name.clone(),
                            binding
                                .button
                                .as_ref()
                                .map(|button| button.to_str().unwrap().to_string()),
                        ));
                    }
                }
                InputAction::Value(value_action) => match value_action {
                    ValueAction::Scalar(scalar_action) => {
                        for (binding_name, binding) in &scalar_action.bindings {
                            match binding {
                                ScalarBinding::Axis(scalar_axis_binding) => {
                                    entries.push(Entry::Binding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        scalar_axis_binding
                                            .axis
                                            .as_ref()
                                            .map(|axis| axis.to_str().unwrap().to_string()),
                                    ));
                                }
                                ScalarBinding::Compound(scalar_compound_binding) => {
                                    entries.push(Entry::Binding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        Some(Default::default()),
                                    ));
                                    entries.push(Entry::MultiBinding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        "Min".to_string(),
                                        scalar_compound_binding
                                            .min
                                            .button
                                            .as_ref()
                                            .map(|button| button.to_str().unwrap().to_string()),
                                    ));
                                    entries.push(Entry::MultiBinding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        "Max".to_string(),
                                        scalar_compound_binding
                                            .max
                                            .button
                                            .as_ref()
                                            .map(|button| button.to_str().unwrap().to_string()),
                                    ));
                                }
                            }
                        }
                    }
                    ValueAction::Vector2D(vector2d_action) => {
                        for (binding_name, binding) in &vector2d_action.bindings {
                            entries.push(Entry::Binding(
                                action_name.clone(),
                                binding_name.clone(),
                                Some(String::default()),
                            ));
                            match binding {
                                Vector2DBinding::Axis(vector2_axis_binding) => {
                                    entries.push(Entry::MultiBinding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        "Horizontal".to_string(),
                                        vector2_axis_binding
                                            .horizontal
                                            .axis
                                            .as_ref()
                                            .map(|axis| axis.to_str().unwrap().to_string()),
                                    ));
                                    entries.push(Entry::MultiBinding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        "Vertical".to_string(),
                                        vector2_axis_binding
                                            .vertical
                                            .axis
                                            .as_ref()
                                            .map(|axis| axis.to_str().unwrap().to_string()),
                                    ));
                                }
                                Vector2DBinding::Compound(vector2d_compound_binding) => {
                                    entries.push(Entry::MultiBinding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        "Up".to_string(),
                                        vector2d_compound_binding
                                            .up
                                            .button
                                            .as_ref()
                                            .map(|button| button.to_str().unwrap().to_string()),
                                    ));
                                    entries.push(Entry::MultiBinding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        "Down".to_string(),
                                        vector2d_compound_binding
                                            .down
                                            .button
                                            .as_ref()
                                            .map(|button| button.to_str().unwrap().to_string()),
                                    ));
                                    entries.push(Entry::MultiBinding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        "Left".to_string(),
                                        vector2d_compound_binding
                                            .left
                                            .button
                                            .as_ref()
                                            .map(|button| button.to_str().unwrap().to_string()),
                                    ));
                                    entries.push(Entry::MultiBinding(
                                        action_name.clone(),
                                        binding_name.clone(),
                                        "Right".to_string(),
                                        vector2d_compound_binding
                                            .right
                                            .button
                                            .as_ref()
                                            .map(|button| button.to_str().unwrap().to_string()),
                                    ));
                                }
                            }
                        }
                    }
                },
            }
        }
        entries
    }

    fn label(&self) -> String {
        match self {
            Entry::Action(name) => name.clone(),
            Entry::Binding(_, name, binding) => {
                if let Some(binding) = binding {
                    if binding.is_empty() {
                        name.clone()
                    } else {
                        format!("{name}: {binding}")
                    }
                } else {
                    format!("{name}: <No Binding>")
                }
            }
            Entry::MultiBinding(_, _, name, binding) => {
                if let Some(binding) = binding {
                    format!("{name}: {binding}")
                } else {
                    format!("{name}: <No Binding>")
                }
            }
        }
    }

    fn has_context_menu(&self) -> bool {
        match self {
            Entry::Action(_) => true,
            Entry::Binding(_, _, _) => true,
            Entry::MultiBinding(_, _, _, _) => false,
        }
    }
}

fn fill(ui: &Ui, selected: bool) -> Color32 {
    if selected {
        ui.visuals().selection.bg_fill
    } else {
        Default::default()
    }
}

#[derive(Default)]
struct PropertiesInspector {
    properties: Properties,
}

impl PropertiesInspector {
    fn new(engine: &mut EditorEngine, action_map: String, entry: &Entry) -> Self {
        Self {
            properties: Properties::new(engine, action_map, entry),
        }
    }
}

impl Widget for PropertiesInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.properties.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        SidePanel::right("input_right")
            .frame(Frame::default().fill(ui.style().visuals.window_fill()))
            .default_width(ui.available_width() * 0.5)
            .min_width(ui.available_width() * 0.5)
            .max_width(ui.available_width() * 0.5)
            .resizable(false)
            .show_inside(ui, |ui| {
                let item_spacing = ui.spacing_mut().item_spacing;
                ui.spacing_mut().item_spacing.y = 0.0;

                Frame::default()
                    .inner_margin(Margin::symmetric(10.0, 5.0))
                    .stroke(Stroke::new(0.1, Color32::GRAY))
                    .show(ui, |ui| {
                        ui.horizontal(|ui| {
                            ui.set_width(ui.available_width());

                            ui.label(RichText::new("Properties").strong());
                        });
                    });

                ui.spacing_mut().item_spacing = item_spacing;

                Frame::default()
                    .stroke(Stroke::new(0.1, Color32::GRAY))
                    .show(ui, |ui| {
                        ScrollArea::both()
                            .auto_shrink([false, false])
                            .show(ui, |ui| {
                                Frame::default()
                                    .inner_margin(Margin::same(15.0))
                                    .show(ui, |ui| {
                                        Grid::new("input_properties")
                                            .num_columns(2)
                                            .spacing([40.0, 4.0])
                                            .show(ui, |ui| {
                                                ui.set_width(ui.available_width());

                                                self.properties.ui(ui, context, engine);
                                            });
                                    });
                            });
                    });
            });
    }
}

#[derive(Default)]
enum Properties {
    #[default]
    None,
    Action(ActionProperties),
    ValueBinding(ValueBindingProperties),
    Binding(BindingProperties),
}

impl Properties {
    fn new(engine: &mut EditorEngine, action_map: String, entry: &Entry) -> Self {
        match entry {
            Entry::Action(action) => {
                Self::Action(ActionProperties::new(engine, action_map, action.clone()))
            }
            Entry::Binding(action, name, _) => {
                match engine
                    .input()
                    .action_map(&action_map)
                    .expect("action map")
                    .0
                    .get(action)
                {
                    Some(InputAction::Button(_)) => Self::Binding(BindingProperties::new(
                        action_map.clone(),
                        action.clone(),
                        name.clone(),
                        None,
                    )),
                    Some(InputAction::Value(_)) => Self::ValueBinding(ValueBindingProperties::new(
                        engine,
                        action_map.clone(),
                        action.clone(),
                        name.clone(),
                    )),
                    _ => Self::None,
                }
            }
            Entry::MultiBinding(action, binding, sub_binding, _) => {
                Self::Binding(BindingProperties::new(
                    action_map.clone(),
                    action.clone(),
                    binding.clone(),
                    Some(sub_binding.clone()),
                ))
            }
        }
    }
}

impl Widget for Properties {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Repaint::All) = context.message_bus().recv() {
            match self {
                Properties::None => {}
                Properties::Action(action_properties) => {
                    if !engine
                        .input()
                        .action_map(&action_properties.action_map)
                        .expect("action map")
                        .0
                        .contains_key(&action_properties.action)
                    {
                        *self = Self::None;
                    }
                }
                Properties::ValueBinding(multi_binding_properties) => {
                    if !engine
                        .input()
                        .action_map(&multi_binding_properties.action_map)
                        .expect("action map")
                        .0
                        .get(&multi_binding_properties.action)
                        .expect("action")
                        .contains_binding(&multi_binding_properties.binding)
                    {
                        *self = Self::None;
                    }
                }
                Properties::Binding(binding_properties) => {
                    if !engine
                        .input()
                        .action_map(&binding_properties.action_map)
                        .expect("action map")
                        .0
                        .get(&binding_properties.action)
                        .expect("action")
                        .contains_binding(&binding_properties.binding)
                    {
                        *self = Self::None;
                    }
                }
            }
        }

        match self {
            Properties::None => {}
            Properties::Action(properties) => properties.handle_messages(context, engine),
            Properties::ValueBinding(properties) => properties.handle_messages(context, engine),
            Properties::Binding(properties) => properties.handle_messages(context, engine),
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        match self {
            Properties::None => {}
            Properties::Action(properties) => properties.ui(ui, context, engine),
            Properties::ValueBinding(properties) => properties.ui(ui, context, engine),
            Properties::Binding(properties) => properties.ui(ui, context, engine),
        }
    }
}

struct ActionProperties {
    additional_action_properties: AdditionalActionProperties,
    action_type: ComboBox<InputAction>,
    action: String,
    action_map: String,
}

impl ActionProperties {
    fn new(engine: &EditorEngine, action_map: String, action: String) -> Self {
        let mut action_type = ComboBox::builder()
            .value(InputAction::Button(Default::default()))
            .value(InputAction::Value(ValueAction::default()))
            .build();
        action_type.set(
            engine
                .input()
                .action_map(&action_map)
                .expect("action map")
                .0
                .get(&action)
                .expect("action")
                .clone(),
        );
        let additional_action_properties = match engine
            .input()
            .action_map(&action_map)
            .expect("action map")
            .0
            .get(&action)
            .expect("action")
            .clone()
        {
            InputAction::Button(button) => AdditionalActionProperties::Button(
                ButtonActionProperties::new(action_map.clone(), action.clone(), &button),
            ),
            InputAction::Value(value) => AdditionalActionProperties::Value(
                ValueActionProperties::new(action_map.clone(), action.clone(), &value),
            ),
        };

        Self {
            additional_action_properties,
            action_type,
            action_map,
            action,
        }
    }

    fn action(&self, engine: &EditorEngine) -> InputAction {
        engine
            .input()
            .action_map(&self.action_map)
            .expect("action map")
            .0
            .get(&self.action)
            .expect("action")
            .clone()
    }
}

impl Widget for ActionProperties {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Repaint::All) = context.message_bus().recv() {
            if let Some(action) = engine
                .input()
                .action_map(&self.action_map)
                .expect("action map")
                .0
                .get(&self.action)
                .cloned()
            {
                self.action_type.set(action);
                self.additional_action_properties = match self.action_type.get() {
                    InputAction::Button(button) => {
                        AdditionalActionProperties::Button(ButtonActionProperties::new(
                            self.action_map.clone(),
                            self.action.clone(),
                            &button,
                        ))
                    }
                    InputAction::Value(value) => {
                        AdditionalActionProperties::Value(ValueActionProperties::new(
                            self.action_map.clone(),
                            self.action.clone(),
                            &value,
                        ))
                    }
                };
            }
        }

        self.additional_action_properties
            .handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("Action Type");
        self.action_type.ui(ui, context, engine);
        ui.end_row();

        self.additional_action_properties.ui(ui, context, engine);

        if self.action_type.changed() {
            context.actions_mut().push(
                engine,
                ChangeActionType::new(
                    self.action_map.clone(),
                    self.action.clone(),
                    self.action(engine),
                    self.action_type.get(),
                ),
            );

            self.additional_action_properties = match self.action_type.get() {
                InputAction::Button(button) => {
                    AdditionalActionProperties::Button(ButtonActionProperties::new(
                        self.action_map.clone(),
                        self.action.clone(),
                        &button,
                    ))
                }
                InputAction::Value(value) => {
                    AdditionalActionProperties::Value(ValueActionProperties::new(
                        self.action_map.clone(),
                        self.action.clone(),
                        &value,
                    ))
                }
            };
        }
    }
}

enum AdditionalActionProperties {
    Button(ButtonActionProperties),
    Value(ValueActionProperties),
}

impl Widget for AdditionalActionProperties {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        match self {
            AdditionalActionProperties::Button(button) => button.handle_messages(context, engine),
            AdditionalActionProperties::Value(value) => value.handle_messages(context, engine),
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        match self {
            AdditionalActionProperties::Button(widget) => widget.ui(ui, context, engine),
            AdditionalActionProperties::Value(widget) => widget.ui(ui, context, engine),
        }
    }
}

struct ButtonActionProperties {
    down: Checkbox,
    action: String,
    action_map: String,
}

impl ButtonActionProperties {
    fn new(action_map: String, action: String, button_action: &ButtonAction) -> Self {
        let mut down = Checkbox::default();
        down.set(button_action.down);

        Self {
            down,
            action,
            action_map,
        }
    }
}

impl Widget for ButtonActionProperties {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Repaint::All) = context.message_bus().recv() {
            if let Some(InputAction::Button(action)) = engine
                .input()
                .action_map(&self.action_map)
                .expect("action map")
                .0
                .get(&self.action)
            {
                self.down.set(action.down);
            }
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("Down");
        self.down.ui(ui, context, engine);
        ui.end_row();

        if self.down.changed() {
            context.actions_mut().push(
                engine,
                ChangeDown::new(
                    self.action_map.clone(),
                    self.action.clone(),
                    !self.down.get(),
                    self.down.get(),
                ),
            );
        }
    }
}

struct ValueActionProperties {
    normalize: Checkbox,
    value_type: ComboBox<ValueAction>,
    action: String,
    action_map: String,
}

impl ValueActionProperties {
    fn new(action_map: String, action: String, value_action: &ValueAction) -> Self {
        let mut value_type = ComboBox::builder()
            .value(ValueAction::Scalar(ScalarAction::default()))
            .value(ValueAction::Vector2D(Vector2DAction::default()))
            .build();
        value_type.set(value_action.clone());
        let mut normalize = Checkbox::default();
        normalize.set(match value_action {
            ValueAction::Scalar(scalar) => scalar.normalize,
            ValueAction::Vector2D(vector2d) => vector2d.normalize,
        });

        Self {
            action_map,
            action,
            value_type,
            normalize,
        }
    }
}

impl Widget for ValueActionProperties {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Repaint::All) = context.message_bus().recv() {
            if let Some(InputAction::Value(action)) = engine
                .input()
                .action_map(&self.action_map)
                .expect("action map")
                .0
                .get(&self.action)
            {
                self.value_type.set(action.clone());
                self.normalize.set(match action {
                    ValueAction::Scalar(scalar) => scalar.normalize,
                    ValueAction::Vector2D(vector2d) => vector2d.normalize,
                });
            }
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.label("Value Type");
        self.value_type.ui(ui, context, engine);
        ui.end_row();

        ui.label("Normalize");
        self.normalize.ui(ui, context, engine);
        ui.end_row();

        if self.value_type.changed() {
            if let Some(InputAction::Value(action)) = engine
                .input_mut()
                .action_map_mut(&self.action_map)
                .expect("action map")
                .0
                .get_mut(&self.action)
                .cloned()
            {
                context.actions_mut().push(
                    engine,
                    ChangeValueType::new(
                        self.action_map.clone(),
                        self.action.clone(),
                        action,
                        self.value_type.get(),
                    ),
                );
                self.normalize.set(false);
            }
        }
        if self.normalize.changed() {
            context.actions_mut().push(
                engine,
                ChangeNormalize::new(
                    self.action_map.clone(),
                    self.action.clone(),
                    !self.normalize.get(),
                    self.normalize.get(),
                ),
            );
        }
    }
}

struct BindingProperties {
    sub_binding: Option<String>,
    binding: String,
    action: String,
    action_map: String,
}

impl BindingProperties {
    fn new(
        action_map: String,
        action: String,
        binding: String,
        sub_binding: Option<String>,
    ) -> Self {
        Self {
            sub_binding,
            binding,
            action,
            action_map,
        }
    }
}

impl Widget for BindingProperties {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        let action = engine
            .input()
            .action_map(&self.action_map)
            .expect("action map")
            .0
            .get(&self.action)
            .expect("action");
        if let Some(binding) = if let Some(sub_binding) = &self.sub_binding {
            action.sub_binding(&self.binding, sub_binding)
        } else {
            action.binding(&self.binding)
        } {
            let mut selected_device = binding.path().map(|path| {
                path.components()
                    .next()
                    .unwrap()
                    .as_os_str()
                    .to_str()
                    .unwrap()
                    .to_string()
            });
            let mut selected = binding.path().map(|path| {
                path.components()
                    .last()
                    .unwrap()
                    .as_os_str()
                    .to_str()
                    .unwrap()
                    .to_string()
            });

            let mut device_changed = false;
            let mut input_changed = false;

            ui.label("Binding");
            ui.horizontal(|ui| {
                egui::ComboBox::from_id_source("device_selection")
                    .selected_text(match &selected_device {
                        None => "",
                        Some(device) => device,
                    })
                    .show_ui(ui, |ui| {
                        if ui
                            .selectable_value(&mut selected_device, None, "")
                            .clicked()
                        {
                            device_changed = true;
                        }
                        for device_name in engine.input().device_names() {
                            if ui
                                .selectable_value(
                                    &mut selected_device,
                                    Some(device_name.clone()),
                                    device_name,
                                )
                                .clicked()
                            {
                                device_changed = true;
                            }
                        }
                    });

                ui.add_space(5.0);

                if let Some(device) = &selected_device {
                    match binding {
                        Binding::Button(_) => {
                            if let Some(mut selected_input) = selected.or_else(|| {
                                engine
                                    .input()
                                    .device_button_names(device)
                                    .next()
                                    .map(|button| button.to_string())
                            }) {
                                egui::ComboBox::from_id_source("input_selection")
                                    .selected_text(&selected_input)
                                    .width(100.0)
                                    .show_ui(ui, |ui| {
                                        for button_name in
                                            engine.input().device_button_names(device)
                                        {
                                            if ui
                                                .selectable_value(
                                                    &mut selected_input,
                                                    button_name.to_string(),
                                                    button_name,
                                                )
                                                .clicked()
                                            {
                                                input_changed = true;
                                            }
                                        }
                                    });
                                selected = Some(selected_input);
                            } else {
                                ui.add_enabled_ui(false, |ui| {
                                    egui::ComboBox::from_id_source("input_selection")
                                        .show_ui(ui, |_| {})
                                })
                                .inner;
                                selected = None;
                            }
                        }
                        Binding::Axis(_) => {
                            if let Some(mut selected_input) = selected.or_else(|| {
                                engine
                                    .input()
                                    .device_axis_names(device)
                                    .next()
                                    .map(|axis| axis.to_string())
                            }) {
                                egui::ComboBox::from_id_source("input_selection")
                                    .selected_text(&selected_input)
                                    .show_ui(ui, |ui| {
                                        for axis_name in engine.input().device_axis_names(device) {
                                            if ui
                                                .selectable_value(
                                                    &mut selected_input,
                                                    axis_name.to_string(),
                                                    axis_name,
                                                )
                                                .clicked()
                                            {
                                                input_changed = true;
                                            }
                                        }
                                    });
                                selected = Some(selected_input);
                            } else {
                                ui.add_enabled_ui(false, |ui| {
                                    egui::ComboBox::from_id_source("input_selection")
                                        .show_ui(ui, |_| {})
                                })
                                .inner;
                                selected = None;
                            }
                        }
                    }
                } else {
                    ui.add_enabled_ui(false, |ui| {
                        egui::ComboBox::from_id_source("input_selection").show_ui(ui, |_| {})
                    })
                    .inner;
                    selected = None;
                };

                if device_changed || input_changed {
                    let path = selected_device
                        .zip(selected)
                        .map(|(device, input)| PathBuf::from(device).join(input));
                    let mut new = binding.clone();
                    new.set_path(path);
                    context.actions_mut().push(
                        engine,
                        ChangeBinding::new(
                            self.action_map.clone(),
                            self.action.clone(),
                            self.binding.clone(),
                            self.sub_binding.clone(),
                            binding,
                            new,
                        ),
                    )
                }
            });
            ui.end_row();
        }
    }
}

struct ValueBindingProperties {
    binding_properties: Option<BindingProperties>,
    binding: String,
    action: String,
    action_map: String,
}

impl ValueBindingProperties {
    fn new(
        engine: &EditorEngine,
        action_map: String,
        action_name: String,
        binding: String,
    ) -> Self {
        let action = engine
            .input()
            .action_map(&action_map)
            .expect("action map")
            .0
            .get(&action_name)
            .expect("action");
        let binding_properties = if let InputAction::Value(ValueAction::Scalar(action)) = action {
            if let Some(ScalarBinding::Axis(_)) = action.bindings.get(&binding) {
                Some(BindingProperties::new(
                    action_map.clone(),
                    action_name.clone(),
                    binding.clone(),
                    None,
                ))
            } else {
                None
            }
        } else {
            None
        };

        Self {
            binding_properties,
            binding,
            action: action_name,
            action_map,
        }
    }
}

impl Widget for ValueBindingProperties {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(binding_properties) = &mut self.binding_properties {
            binding_properties.handle_messages(context, engine);
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        let action = engine
            .input()
            .action_map(&self.action_map)
            .expect("action map")
            .0
            .get(&self.action)
            .expect("action");
        let old = match action {
            InputAction::Value(ValueAction::Scalar(action)) => {
                match action.bindings.get(&self.binding) {
                    Some(ScalarBinding::Axis(_)) => "Axis",
                    Some(ScalarBinding::Compound(_)) => "Compound",
                    _ => "",
                }
            }
            InputAction::Value(ValueAction::Vector2D(action)) => {
                match action.bindings.get(&self.binding) {
                    Some(Vector2DBinding::Axis(_)) => "Axis",
                    Some(Vector2DBinding::Compound(_)) => "Compound",
                    _ => "",
                }
            }
            _ => unreachable!("not a value action"),
        };
        let mut selected = old;
        let mut changed = false;

        ui.label("Binding Type");
        egui::ComboBox::from_id_source("binding_type_selection")
            .selected_text(selected)
            .show_ui(ui, |ui| {
                if ui.selectable_value(&mut selected, "Axis", "Axis").clicked() {
                    changed = true;
                }
                if ui
                    .selectable_value(&mut selected, "Compound", "Compound")
                    .clicked()
                {
                    changed = true;
                }
            });
        ui.end_row();

        if let Some(binding_properties) = &mut self.binding_properties {
            binding_properties.ui(ui, context, engine);
        }

        if changed {
            context.actions_mut().push(
                engine,
                ChangeBindingType::new(
                    self.action_map.clone(),
                    self.action.clone(),
                    self.binding.clone(),
                    old.to_string(),
                    selected.to_string(),
                ),
            );
        }
    }
}

struct CreateActionMap {
    name: String,
}

impl CreateActionMap {
    fn new(name: String) -> Self {
        Self { name }
    }
}

impl Action for CreateActionMap {
    fn description(&self) -> String {
        "Create Action Map".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        engine
            .input_mut()
            .action_maps_mut()
            .0
            .insert(self.name.clone(), Default::default());
    }

    fn undo(&self, engine: &mut EditorEngine) {
        engine.input_mut().action_maps_mut().0.remove(&self.name);
    }
}

struct RenameActionMap {
    old: String,
    new: String,
}

impl RenameActionMap {
    fn new(old: String, new: String) -> Self {
        Self { old, new }
    }
}

impl Action for RenameActionMap {
    fn description(&self) -> String {
        "Rename Action Map".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        if let Some(action_map) = engine.input_mut().action_maps_mut().0.remove(&self.old) {
            engine
                .input_mut()
                .action_maps_mut()
                .0
                .insert(self.new.clone(), action_map);
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        if let Some(action_map) = engine.input_mut().action_maps_mut().0.remove(&self.new) {
            engine
                .input_mut()
                .action_maps_mut()
                .0
                .insert(self.old.clone(), action_map);
        }
    }
}

struct DeleteActionMap {
    action_map: InputActionMap,
    name: String,
}

impl DeleteActionMap {
    fn new(engine: &EditorEngine, name: String) -> Self {
        Self {
            action_map: engine
                .input()
                .action_map(&name)
                .expect("action map")
                .clone(),
            name,
        }
    }
}

impl Action for DeleteActionMap {
    fn description(&self) -> String {
        "Delete Action Map".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        engine.input_mut().action_maps_mut().0.remove(&self.name);
    }

    fn undo(&self, engine: &mut EditorEngine) {
        engine
            .input_mut()
            .action_maps_mut()
            .0
            .insert(self.name.clone(), self.action_map.clone());
    }
}

struct CreateAction {
    action_map: String,
    name: String,
}

impl CreateAction {
    fn new(action_map: String, name: String) -> Self {
        Self { action_map, name }
    }
}

impl Action for CreateAction {
    fn description(&self) -> String {
        "Create Action".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        if let Some(action_map) = engine.input_mut().action_map_mut(&self.action_map) {
            action_map.0.insert(self.name.clone(), Default::default());
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        if let Some(action_map) = engine.input_mut().action_map_mut(&self.action_map) {
            action_map.0.remove(&self.name);
        }
    }
}

struct RenameAction {
    action_map: String,
    old: String,
    new: String,
}

impl RenameAction {
    fn new(action_map: String, old: String, new: String) -> Self {
        Self {
            action_map,
            old,
            new,
        }
    }
}

impl Action for RenameAction {
    fn description(&self) -> String {
        "Rename Action".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        if let Some(action_map) = engine.input_mut().action_map_mut(&self.action_map) {
            if let Some(action) = action_map.0.remove(&self.old) {
                action_map.0.insert(self.new.clone(), action);
            }
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        if let Some(action_map) = engine.input_mut().action_map_mut(&self.action_map) {
            if let Some(action) = action_map.0.remove(&self.new) {
                action_map.0.insert(self.old.clone(), action);
            }
        }
    }
}

struct DeleteAction {
    action: InputAction,
    name: String,
    action_map: String,
}

impl DeleteAction {
    fn new(engine: &EditorEngine, action_map: String, name: String) -> Self {
        Self {
            action: engine
                .input()
                .action_map(&action_map)
                .expect("action map")
                .0
                .get(&name)
                .expect("action")
                .clone(),
            name,
            action_map,
        }
    }
}

impl Action for DeleteAction {
    fn description(&self) -> String {
        "Delete Action".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        engine
            .input_mut()
            .action_map_mut(&self.action_map)
            .expect("action map")
            .0
            .remove(&self.name);
    }

    fn undo(&self, engine: &mut EditorEngine) {
        engine
            .input_mut()
            .action_map_mut(&self.action_map)
            .expect("action map")
            .0
            .insert(self.name.clone(), self.action.clone());
    }
}

struct CreateBinding {
    action_map: String,
    action: String,
    name: String,
}

impl CreateBinding {
    fn new(action_map: String, action: String, name: String) -> Self {
        Self {
            action_map,
            action,
            name,
        }
    }
}

impl Action for CreateBinding {
    fn description(&self) -> String {
        "Create Binding".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        match action {
            InputAction::Button(button_action) => {
                button_action
                    .bindings
                    .insert(self.name.clone(), ButtonBinding::default());
            }
            InputAction::Value(ValueAction::Scalar(scalar_action)) => {
                scalar_action
                    .bindings
                    .insert(self.name.clone(), ScalarBinding::default());
            }
            InputAction::Value(ValueAction::Vector2D(vector2d_action)) => {
                vector2d_action
                    .bindings
                    .insert(self.name.clone(), Vector2DBinding::default());
            }
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        match action {
            InputAction::Button(button_action) => {
                button_action.bindings.remove(&self.name);
            }
            InputAction::Value(ValueAction::Scalar(scalar_action)) => {
                scalar_action.bindings.remove(&self.name);
            }
            InputAction::Value(ValueAction::Vector2D(vector2d_action)) => {
                vector2d_action.bindings.remove(&self.name);
            }
        }
    }
}

struct RenameBinding {
    new: String,
    old: String,
    action: String,
    action_map: String,
}

impl RenameBinding {
    fn new(action_map: String, action: String, old: String, new: String) -> Self {
        Self {
            new,
            old,
            action,
            action_map,
        }
    }
}

impl Action for RenameBinding {
    fn description(&self) -> String {
        "Rename Binding".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        match action {
            InputAction::Button(button_action) => {
                if let Some(binding) = button_action.bindings.remove(&self.old) {
                    button_action.bindings.insert(self.new.clone(), binding);
                }
            }
            InputAction::Value(ValueAction::Scalar(scalar_action)) => {
                if let Some(binding) = scalar_action.bindings.remove(&self.old) {
                    scalar_action.bindings.insert(self.new.clone(), binding);
                }
            }
            InputAction::Value(ValueAction::Vector2D(vector2d_action)) => {
                if let Some(binding) = vector2d_action.bindings.remove(&self.old) {
                    vector2d_action.bindings.insert(self.new.clone(), binding);
                }
            }
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        match action {
            InputAction::Button(button_action) => {
                if let Some(binding) = button_action.bindings.remove(&self.new) {
                    button_action.bindings.insert(self.old.clone(), binding);
                }
            }
            InputAction::Value(ValueAction::Scalar(scalar_action)) => {
                if let Some(binding) = scalar_action.bindings.remove(&self.new) {
                    scalar_action.bindings.insert(self.old.clone(), binding);
                }
            }
            InputAction::Value(ValueAction::Vector2D(vector2d_action)) => {
                if let Some(binding) = vector2d_action.bindings.remove(&self.new) {
                    vector2d_action.bindings.insert(self.old.clone(), binding);
                }
            }
        }
    }
}

struct DeleteBinding {
    input_action: InputAction,
    binding: String,
    action: String,
    action_map: String,
}

impl DeleteBinding {
    fn new(engine: &EditorEngine, action_map: String, action: String, binding: String) -> Self {
        Self {
            input_action: engine
                .input()
                .action_map(&action_map)
                .expect("action map")
                .0
                .get(&action)
                .expect("action")
                .clone(),
            binding,
            action,
            action_map,
        }
    }
}

impl Action for DeleteBinding {
    fn description(&self) -> String {
        "Delete Binding".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        match action {
            InputAction::Button(button_action) => {
                button_action.bindings.remove(&self.binding);
            }
            InputAction::Value(ValueAction::Scalar(scalar_action)) => {
                scalar_action.bindings.remove(&self.binding);
            }
            InputAction::Value(ValueAction::Vector2D(vector2d_action)) => {
                vector2d_action.bindings.remove(&self.binding);
            }
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        *engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action") = self.input_action.clone();
    }
}

struct ChangeActionType {
    new: InputAction,
    old: InputAction,
    action: String,
    action_map: String,
}

impl ChangeActionType {
    fn new(action_map: String, action: String, old: InputAction, new: InputAction) -> Self {
        Self {
            action_map,
            action,
            old,
            new,
        }
    }
}

impl Action for ChangeActionType {
    fn description(&self) -> String {
        "Change Action Type".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        *action = self.new.clone();
    }

    fn undo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        *action = self.old.clone();
    }
}

struct ChangeDown {
    new: bool,
    old: bool,
    action: String,
    action_map: String,
}

impl ChangeDown {
    fn new(action_map: String, action: String, old: bool, new: bool) -> Self {
        Self {
            new,
            old,
            action,
            action_map,
        }
    }
}

impl Action for ChangeDown {
    fn description(&self) -> String {
        "Change Down".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        if let Some(InputAction::Button(button_action)) = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
        {
            button_action.down = self.new;
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        if let Some(InputAction::Button(button_action)) = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
        {
            button_action.down = self.old;
        }
    }
}

struct ChangeValueType {
    new: ValueAction,
    old: ValueAction,
    action: String,
    action_map: String,
}

impl ChangeValueType {
    fn new(action_map: String, action: String, old: ValueAction, new: ValueAction) -> Self {
        Self {
            new,
            old,
            action,
            action_map,
        }
    }
}

impl Action for ChangeValueType {
    fn description(&self) -> String {
        "Change Value Type".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        if let Some(InputAction::Value(value_action)) = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
        {
            *value_action = self.new.clone();
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        if let Some(InputAction::Value(value_action)) = engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
        {
            *value_action = self.old.clone();
        }
    }
}

struct ChangeNormalize {
    new: bool,
    old: bool,
    action: String,
    action_map: String,
}

impl ChangeNormalize {
    fn new(action_map: String, action: String, old: bool, new: bool) -> Self {
        Self {
            new,
            old,
            action,
            action_map,
        }
    }
}

impl Action for ChangeNormalize {
    fn description(&self) -> String {
        "Change Normalize".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        match engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
        {
            Some(InputAction::Value(ValueAction::Scalar(scalar_action))) => {
                scalar_action.normalize = self.new
            }
            Some(InputAction::Value(ValueAction::Vector2D(vector2d_action))) => {
                vector2d_action.normalize = self.new
            }
            _ => {}
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        match engine
            .input_mut()
            .action_maps_mut()
            .0
            .get_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
        {
            Some(InputAction::Value(ValueAction::Scalar(scalar_action))) => {
                scalar_action.normalize = self.old
            }
            Some(InputAction::Value(ValueAction::Vector2D(vector2d_action))) => {
                vector2d_action.normalize = self.old
            }
            _ => {}
        }
    }
}

struct ChangeBindingType {
    new: String,
    old: String,
    binding: String,
    action: String,
    action_map: String,
}

impl ChangeBindingType {
    fn new(action_map: String, action: String, binding: String, old: String, new: String) -> Self {
        Self {
            new,
            old,
            binding,
            action,
            action_map,
        }
    }
}

impl Action for ChangeBindingType {
    fn description(&self) -> String {
        "Change Binding Type".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_map_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        match action {
            InputAction::Value(ValueAction::Scalar(action)) => {
                if self.new == "Axis" {
                    action.bindings.insert(
                        self.binding.clone(),
                        ScalarBinding::Axis(AxisBinding::default()),
                    );
                } else if self.new == "Compound" {
                    action.bindings.insert(
                        self.binding.clone(),
                        ScalarBinding::Compound(ScalarCompoundBinding::default()),
                    );
                }
            }
            InputAction::Value(ValueAction::Vector2D(action)) => {
                if self.new == "Axis" {
                    action.bindings.insert(
                        self.binding.clone(),
                        Vector2DBinding::Axis(Vector2DAxisBinding::default()),
                    );
                } else if self.new == "Compound" {
                    action.bindings.insert(
                        self.binding.clone(),
                        Vector2DBinding::Compound(Vector2DCompoundBinding::default()),
                    );
                }
            }
            _ => {}
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_map_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        match action {
            InputAction::Value(ValueAction::Scalar(action)) => {
                if self.old == "Axis" {
                    action.bindings.insert(
                        self.binding.clone(),
                        ScalarBinding::Axis(AxisBinding::default()),
                    );
                } else if self.new == "Compound" {
                    action.bindings.insert(
                        self.binding.clone(),
                        ScalarBinding::Compound(ScalarCompoundBinding::default()),
                    );
                }
            }
            InputAction::Value(ValueAction::Vector2D(action)) => {
                if self.old == "Axis" {
                    action.bindings.insert(
                        self.binding.clone(),
                        Vector2DBinding::Axis(Vector2DAxisBinding::default()),
                    );
                } else if self.new == "Compound" {
                    action.bindings.insert(
                        self.binding.clone(),
                        Vector2DBinding::Compound(Vector2DCompoundBinding::default()),
                    );
                }
            }
            _ => {}
        }
    }
}

struct ChangeBinding {
    new: Binding,
    old: Binding,
    sub_binding: Option<String>,
    binding_name: String,
    action: String,
    action_map: String,
}

impl ChangeBinding {
    fn new(
        action_map: String,
        action: String,
        binding_name: String,
        sub_binding: Option<String>,
        old: Binding,
        new: Binding,
    ) -> Self {
        Self {
            new,
            old,
            sub_binding,
            binding_name,
            action,
            action_map,
        }
    }
}

impl Action for ChangeBinding {
    fn description(&self) -> String {
        "Change Binding".to_string()
    }

    fn redo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_map_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        if let Some(sub_binding) = &self.sub_binding {
            action.set_sub_binding(&self.binding_name, &sub_binding, self.new.clone());
        } else {
            action.set_binding(&self.binding_name, self.new.clone());
        }
    }

    fn undo(&self, engine: &mut EditorEngine) {
        let action = engine
            .input_mut()
            .action_map_mut(&self.action_map)
            .expect("action map")
            .0
            .get_mut(&self.action)
            .expect("action");
        if let Some(sub_binding) = &self.sub_binding {
            action.set_sub_binding(&self.binding_name, &sub_binding, self.old.clone());
        } else {
            action.set_binding(&self.binding_name, self.old.clone());
        }
    }
}
