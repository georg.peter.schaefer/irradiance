use std::path::PathBuf;

use egui::{TopBottomPanel, Ui};

use crate::{core::EditorEngine, gui::EditorContext};

use super::{AssetBrowser, Log, Tabs, Widget};

pub struct BottomPanel {
    tabs: Tabs,
}

impl BottomPanel {
    pub fn new(context: &mut EditorContext, asset_path: PathBuf) -> Self {
        Self {
            tabs: Tabs::builder()
                .tab(AssetBrowser::new(context, asset_path))
                .tab(Log::default())
                .build(),
        }
    }
}

impl Widget for BottomPanel {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.tabs.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.style_mut().spacing.item_spacing = egui::Vec2::ZERO;
        TopBottomPanel::bottom("bottom_panel")
            .resizable(true)
            .frame(egui::Frame::default().fill(ui.style().visuals.window_fill()))
            .default_height(350.0)
            .show_inside(ui, |ui| {
                ui.reset_style();
                ui.set_enabled(!context.is_playing());

                self.tabs.ui(ui, context, engine);
            });
    }
}
