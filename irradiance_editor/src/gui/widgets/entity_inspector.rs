use crate::core::EditorEngine;
use crate::gui::message::{Create, Delete, Message, Recv};
use crate::gui::widgets::{MenuEntry, Widget};
use crate::gui::{Action, EditorContext, Icon, Icons};
use egui::style::Margin;
use egui::{Align, Button, Frame, Grid, Layout, Ui};
use irradiance_runtime::ecs::{Component, Entities};
use std::any::TypeId;
use std::sync::Arc;

pub struct EntityInspector {
    components: Vec<ComponentEntry>,
    id: String,
    entities: Arc<Entities>,
}

impl EntityInspector {
    pub fn new(
        entities: Arc<Entities>,
        id: String,
        context: &EditorContext,
        engine: &EditorEngine,
    ) -> Option<Box<Self>> {
        entities.get(&id).map(|_| {
            let mut entity_inspector = Box::new(Self {
                entities: entities.clone(),
                id: id.clone(),
                components: Default::default(),
            });
            entity_inspector.refresh(context, engine);
            entity_inspector
        })
    }
}

impl EntityInspector {
    fn refresh(&mut self, context: &EditorContext, engine: &EditorEngine) {
        self.components = self
            .entities
            .get(&self.id)
            .map(|entity| {
                context
                    .component_types()
                    .types()
                    .filter(|component_type| entity.get_dyn(*component_type).is_some())
                    .flat_map(|component_type| {
                        ComponentEntry::new(
                            self.entities.clone(),
                            self.id.clone(),
                            component_type,
                            context,
                            engine,
                        )
                    })
                    .collect()
            })
            .unwrap_or_default();
    }
}

impl Widget for EntityInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(Create::Component(component_type, id, entities)) =
            context.message_bus().recv().cloned()
        {
            let add_component = AddComponent::new(component_type, id, entities, context, engine);
            context.actions_mut().push(engine, add_component);
            self.refresh(context, engine);
        }
        if let Some(Delete::Component(component_type, id, entities)) =
            context.message_bus().recv().cloned()
        {
            let remove_component = RemoveComponent::new(component_type, id, entities, context);
            context.actions_mut().push(engine, remove_component);
            if let Some(index) = self
                .components
                .iter()
                .position(|component| component.component_type == component_type)
            {
                self.components.remove(index);
            }
        }

        for component in &mut self.components {
            component.handle_messages(context, engine);
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.spacing_mut().item_spacing.y = 0.0;

        Frame::default()
            .inner_margin(Margin::same(2.0))
            .show(ui, |ui| {
                ui.horizontal(|ui| {
                    ui.visuals_mut().button_frame = false;

                    ui.menu_button(Icons::plus(), |ui| {
                        for (icon, display_name, component_type) in
                            context.component_types().create_entries()
                        {
                            let sub_menu_entry = if let Some(icon) = icon {
                                MenuEntry::new(display_name).icon(icon)
                            } else {
                                MenuEntry::new(display_name)
                            };

                            if ui.add(sub_menu_entry).clicked() {
                                context
                                    .message_bus_mut()
                                    .post(Message::Create(Create::Component(
                                        component_type,
                                        self.id.clone(),
                                        self.entities.clone(),
                                    )));
                                ui.close_menu();
                            }
                        }
                    });
                });
            });

        Grid::new("components")
            .num_columns(2)
            .spacing([0.0, 5.0])
            .show(ui, |ui| {
                ui.set_width(ui.available_width());

                for component in &mut self.components {
                    component.ui(ui, context, engine);
                }
            });
    }
}

struct ComponentEntry {
    widget: Box<dyn Widget>,
    display_name: &'static str,
    icon: Option<Icon>,
    component_type: TypeId,
    id: String,
    entities: Arc<Entities>,
}

impl ComponentEntry {
    fn new(
        entities: Arc<Entities>,
        id: String,
        component_type: TypeId,
        context: &EditorContext,
        engine: &EditorEngine,
    ) -> Option<Self> {
        context
            .component_types()
            .inspect(component_type, id.clone(), entities.clone(), engine)
            .map(|widget| Self {
                entities,
                id,
                component_type,
                icon: context.component_types().icon(component_type),
                display_name: context
                    .component_types()
                    .display_name(component_type)
                    .expect("display name"),
                widget,
            })
    }
}

impl Widget for ComponentEntry {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.widget.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        Frame::default()
            .inner_margin(Margin::same(5.0))
            .fill(ui.visuals().extreme_bg_color)
            .show(ui, |ui| {
                ui.set_width(ui.available_width());
                if let Some(icon) = &self.icon {
                    ui.label(*icon);
                }
                ui.label(self.display_name);
            });
        Frame::default()
            .inner_margin(Margin::same(5.0))
            .fill(ui.visuals().extreme_bg_color)
            .show(ui, |ui| {
                ui.with_layout(Layout::right_to_left(Align::Center), |ui| {
                    if ui.add(Button::new(Icons::xmark()).frame(false)).clicked() {
                        context
                            .message_bus_mut()
                            .post(Message::Delete(Delete::Component(
                                self.component_type,
                                self.id.clone(),
                                self.entities.clone(),
                            )));
                    }
                })
            });
        ui.end_row();
        self.widget.ui(ui, context, engine);
    }
}

struct AddComponent {
    entities: Arc<Entities>,
    entity: String,
    component: Arc<dyn Component>,
    component_type: TypeId,
    display_name: &'static str,
}

impl AddComponent {
    fn new(
        component_type: TypeId,
        entity: String,
        entities: Arc<Entities>,
        context: &EditorContext,
        engine: &EditorEngine,
    ) -> Self {
        Self {
            display_name: context
                .component_types()
                .display_name(component_type)
                .expect("display name"),
            component_type,
            component: context
                .component_types()
                .create(component_type, engine)
                .expect("component"),
            entity,
            entities,
        }
    }
}

impl Action for AddComponent {
    fn description(&self) -> String {
        format!("Add {} Component", self.display_name)
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        if let Some(entity) = self.entities.get(&self.entity) {
            entity.insert_dyn(self.component.clone());
            unsafe {
                self.entities.commit();
            }
        }
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        if let Some(entity) = self.entities.get(&self.entity) {
            entity.remove_by_type(self.component_type);
            unsafe {
                self.entities.commit();
            }
        }
    }
}

struct RemoveComponent {
    entities: Arc<Entities>,
    entity: String,
    component: Arc<dyn Component>,
    component_type: TypeId,
    display_name: &'static str,
}

impl RemoveComponent {
    fn new(
        component_type: TypeId,
        entity: String,
        entities: Arc<Entities>,
        context: &EditorContext,
    ) -> Self {
        Self {
            display_name: context
                .component_types()
                .display_name(component_type)
                .expect("display name"),
            component_type,
            component: entities
                .get(&entity)
                .expect("entity")
                .get_dyn(component_type)
                .expect("component"),
            entity,
            entities,
        }
    }
}

impl Action for RemoveComponent {
    fn description(&self) -> String {
        format!("Remove {} Component", self.display_name)
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        if let Some(entity) = self.entities.get(&self.entity) {
            entity.remove_by_type(self.component_type);
            unsafe {
                self.entities.commit();
            }
        }
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        if let Some(entity) = self.entities.get(&self.entity) {
            entity.insert_dyn(self.component.clone());
            unsafe {
                self.entities.commit();
            }
        }
    }
}
