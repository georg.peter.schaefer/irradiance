use egui::Ui;

use crate::{core::EditorEngine, gui::EditorContext};

/// Trait for widgets.
pub trait Widget {
    /// Handles messages.
    fn handle_messages(&mut self, _context: &mut EditorContext, _engine: &mut EditorEngine) {}
    /// Displays the widget in the ui.
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine);
}
