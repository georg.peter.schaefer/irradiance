use std::any;

use egui::{ScrollArea, Ui};

use crate::gui::message::Delete;
use crate::gui::widgets::entity_inspector::EntityInspector;
use crate::{
    core::EditorEngine,
    gui::{
        message::{Inspect, Recv, Repaint},
        EditorContext,
    },
};

use super::{Tab, Widget};

pub struct Inspector {
    open: Option<Open>,
    title: String,
}

impl Default for Inspector {
    fn default() -> Self {
        Self {
            title: "Inspector".to_owned(),
            open: None,
        }
    }
}

impl Widget for Inspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        match context.message_bus().recv().cloned() {
            Some(Inspect::Asset(path)) => {
                self.open = context
                    .asset_types()
                    .inspect(path.clone())
                    .map(|widget| Open {
                        inspect: Inspect::Asset(path.clone()),
                        widget,
                    });
                self.title = format!(
                    "{} - Inspector",
                    context
                        .asset_types()
                        .display_name(path)
                        .expect("display name")
                );
            }
            Some(Inspect::Entity(id, entities)) => {
                if let Some(inspect) =
                    EntityInspector::new(entities.clone(), id.clone(), context, engine).map(
                        |widget| Open {
                            inspect: Inspect::Entity(id.clone(), entities),
                            widget,
                        },
                    )
                {
                    self.open = Some(inspect);
                    self.title = format!("Entity - Inspector - {id}");
                }
            }
            _ => {}
        }

        if let Some(Repaint::All) = context.message_bus().recv() {
            self.open = match &self.open {
                Some(Open {
                    inspect: Inspect::Asset(path),
                    ..
                }) => context
                    .asset_types()
                    .inspect(path.clone())
                    .map(|widget| Open {
                        widget,
                        inspect: Inspect::Asset(path.clone()),
                    }),
                Some(Open {
                    inspect: Inspect::Entity(id, entities),
                    ..
                }) => EntityInspector::new(entities.clone(), id.clone(), context, engine).map(
                    |widget| Open {
                        inspect: Inspect::Entity(id.clone(), entities.clone()),
                        widget,
                    },
                ),
                _ => None,
            };
            if self.open.is_none() {
                self.title = "Inspector".to_owned();
            }
        }

        if let Some(Open { widget, .. }) = &mut self.open {
            widget.handle_messages(context, engine);
        }

        if let Some(Delete::Entity(id)) = context.message_bus_mut().recv() {
            if matches!(
                &self.open,
                Some(Open {
                    inspect: Inspect::Entity(entity, ..),
                    ..
                }) if id == entity
            ) {
                self.open = None;
                self.title = "Inspector".to_owned();
            }
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.set_clip_rect(ui.max_rect());
        ScrollArea::both()
            .auto_shrink([false, false])
            .show(ui, |ui| {
                if let Some(open) = &mut self.open {
                    open.widget.ui(ui, context, engine);
                }
            });
    }
}

impl Tab for Inspector {
    fn title(&self) -> &str {
        &self.title
    }

    fn as_any(&self) -> &dyn any::Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn any::Any {
        self
    }
}

struct Open {
    widget: Box<dyn Widget>,
    inspect: Inspect,
}
