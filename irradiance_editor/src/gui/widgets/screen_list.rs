use std::any::Any;

use egui::{style::Margin, Color32, Frame, ScrollArea, Sense, Ui};
use irradiance_runtime::core::Engine;

use crate::{
    core::EditorEngine,
    gui::{
        message::{Inspect, Message},
        EditorContext,
    },
};

use super::{Tab, Widget};

/// A widget that lists all available screens.
pub struct ScreenList {
    selected: Option<&'static str>,
    screens: Vec<&'static str>,
}

impl ScreenList {
    /// Creates a new screen list.
    pub fn new(context: &mut EditorContext, engine: &EditorEngine) -> Self {
        let mut screens = engine.screens().names();
        screens.sort();
        let selected = screens.first().cloned();
        if let Some(screen) = &selected {
            context
                .message_bus_mut()
                .post(Message::Inspect(Inspect::Screen(*screen)));
        }

        Self { screens, selected }
    }
}

impl Widget for ScreenList {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, _engine: &mut EditorEngine) {
        ui.set_clip_rect(ui.max_rect());
        ScrollArea::both()
            .auto_shrink([false, false])
            .show(ui, |ui| {
                for screen in &self.screens {
                    let response = Frame::default()
                        .inner_margin(Margin::symmetric(10.0, 5.0))
                        .fill(self.fill(ui, screen))
                        .show(ui, |ui| {
                            ui.set_width(ui.available_width());

                            ui.colored_label(
                                ui.visuals().widgets.inactive.fg_stroke.color,
                                *screen,
                            );
                        })
                        .response;
                    let response = response.interact(Sense::click());

                    if response.clicked() {
                        self.selected = Some(screen);
                    }
                    if response.double_clicked() {
                        context
                            .message_bus_mut()
                            .post(Message::Inspect(Inspect::Screen(*screen)));
                    }
                }
            });
    }
}

impl ScreenList {
    fn fill(&self, ui: &Ui, screen: &'static str) -> Color32 {
        if self.selected == Some(screen) {
            ui.visuals().selection.bg_fill
        } else {
            Default::default()
        }
    }
}

impl Tab for ScreenList {
    fn title(&self) -> &str {
        "Screens"
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}
