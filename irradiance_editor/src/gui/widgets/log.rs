use std::any;

use egui::{style::Margin, Frame, Label, RichText, ScrollArea, TextStyle, Ui};

use crate::{core::EditorEngine, gui::EditorContext};

use super::{Tab, Widget};

#[derive(Default)]
pub struct Log;

impl Widget for Log {
    fn ui(&mut self, ui: &mut Ui, _context: &mut EditorContext, engine: &mut EditorEngine) {
        ui.set_clip_rect(ui.max_rect());

        let row_height = ui.text_style_height(&TextStyle::Monospace);
        let buffer = engine.buf_log().buffer();

        ScrollArea::both()
            .auto_shrink([false, false])
            .stick_to_bottom(true)
            .show_rows(ui, row_height, buffer.len(), |ui, rows| {
                Frame::default()
                    .inner_margin(Margin::symmetric(10.0, 5.0))
                    .show(ui, |ui| {
                        for row in &buffer[rows] {
                            ui.add(Label::new(RichText::new(row).monospace()).wrap(false));
                        }
                    });
            });
    }
}

impl Tab for Log {
    fn title(&self) -> &str {
        "Output"
    }

    fn as_any(&self) -> &dyn any::Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn any::Any {
        self
    }
}
