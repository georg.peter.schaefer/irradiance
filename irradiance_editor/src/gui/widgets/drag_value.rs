use std::marker::PhantomData;

use approx::{relative_ne, RelativeEq};
use egui::{emath::Numeric, Color32, Ui};

use crate::{core::EditorEngine, gui::EditorContext};

use super::Widget;

/// A dragable value.
#[derive(Default)]
pub struct DragValue<Number> {
    color: Option<Color32>,
    suffix: Option<String>,
    prefix: Option<String>,
    old_value: Number,
    value: Number,
    changing: bool,
    changed: bool,
}

impl<Number> DragValue<Number> {
    /// Starts building a new drag value.
    pub fn builder() -> DragValueBuilder<Number> {
        DragValueBuilder {
            color: None,
            suffix: None,
            prefix: None,
            _phantom: Default::default(),
        }
    }
}

impl<Number: Numeric + Default + RelativeEq> DragValue<Number> {
    /// Returns if the value has changed.
    pub fn changed(&self) -> bool {
        self.changed
    }

    /// Returns if the value is changing.
    pub fn changing(&self) -> bool {
        self.changing
    }

    /// Returns the value.
    pub fn get(&self) -> Number {
        self.value
    }

    /// Returns the old value.
    pub fn get_old(&self) -> Number {
        self.old_value
    }

    /// Sets the value.
    pub fn set(&mut self, value: Number) {
        if relative_ne!(self.value, value) {
            self.value = value;
            self.old_value = value;
            self.changed = true;
        }
    }

    /// Sets the value in changing mode.
    pub fn set_changing(&mut self, value: Number) {
        self.value = value;
        self.changing = true;
    }
}

impl<Number: Numeric + RelativeEq> Widget for DragValue<Number> {
    fn ui(&mut self, ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
        if self.changed {
            self.old_value = self.value;
        }
        self.changed = false;

        self.style(ui);

        let mut drag_value = egui::DragValue::new(&mut self.value).speed(0.1);
        if let Some(prefix) = &self.prefix {
            drag_value = drag_value.prefix(prefix);
        }
        if let Some(suffix) = &self.suffix {
            drag_value = drag_value.suffix(suffix);
        }

        let response = ui.add(drag_value);

        let changing = response.changed() || (self.changing && ui.input().pointer.any_down());
        if self.changing && !changing {
            self.changed = true;
        }
        self.changing = changing;
    }
}

impl<Number> DragValue<Number> {
    fn style(&self, ui: &mut Ui) {
        if let Some(color) = &self.color {
            ui.visuals_mut().widgets.inactive.bg_stroke.color = *color;
            ui.visuals_mut().widgets.inactive.bg_stroke.width = 1.0;
            ui.visuals_mut().widgets.active.bg_stroke.color = *color;
            ui.visuals_mut().widgets.hovered.bg_stroke.color = *color;
            ui.visuals_mut().widgets.open.bg_stroke.color = *color;
        }
    }
}

/// Type for building a drag value.
pub struct DragValueBuilder<Number> {
    color: Option<Color32>,
    suffix: Option<String>,
    prefix: Option<String>,
    _phantom: PhantomData<Number>,
}

impl<Number: Default> DragValueBuilder<Number> {
    /// Sets the prefix.
    pub fn prefix(mut self, prefix: impl Into<String>) -> Self {
        self.prefix = Some(prefix.into());
        self
    }

    /// Sets the suffix.
    pub fn suffix(mut self, suffix: impl Into<String>) -> Self {
        self.suffix = Some(suffix.into());
        self
    }

    /// Sets the color.
    pub fn color(mut self, color: Color32) -> Self {
        self.color = Some(color);
        self
    }

    /// Builds the drag value.
    pub fn build(self) -> DragValue<Number> {
        DragValue {
            color: self.color,
            prefix: self.prefix,
            suffix: self.suffix,
            old_value: Number::default(),
            value: Number::default(),
            changing: false,
            changed: false,
        }
    }
}
