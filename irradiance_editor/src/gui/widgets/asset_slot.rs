use std::{ffi::OsStr, path::PathBuf, rc::Rc, sync::Arc};

use egui::{Label, Ui};
use irradiance_runtime::{
    asset::{AssetRef, Assets},
    gfx::GraphicsContext,
    math::Vec2,
};

use crate::{asset::EditorAssetExt, core::EditorEngine, gui::EditorContext};

use super::{DropSink, Widget};

/// A slot for an asset.
pub struct AssetSlot<A: EditorAssetExt> {
    display: Box<dyn Widget>,
    drop_sink: Rc<DropSink<PathBuf, AssetRef<A>>>,
    changed: bool,
    asset: AssetRef<A>,
    assets: Arc<Assets>,
    graphics_context: Arc<GraphicsContext>,
}

impl<A: EditorAssetExt> AssetSlot<A> {
    /// Creates a new asset slot.
    pub fn new(graphics_context: Arc<GraphicsContext>, assets: Arc<Assets>) -> Self {
        let graphics_context2 = graphics_context.clone();
        let assets2 = assets.clone();

        Self {
            graphics_context: graphics_context.clone(),
            assets: assets.clone(),
            asset: Default::default(),
            changed: false,
            drop_sink: DropSink::new(move |path| {
                assets2.clone().read(graphics_context2.clone(), path).ok()
            }),
            display: A::display(
                graphics_context,
                assets,
                Default::default(),
                Vec2::new(50.0, 50.0),
            ),
        }
    }

    /// Returns if the asset has changed.
    pub fn changed(&self) -> bool {
        self.changed
    }

    /// Returns the asset.
    pub fn get(&self) -> AssetRef<A> {
        self.asset.clone()
    }

    /// Sets the asset.
    pub fn set(&mut self, asset: AssetRef<A>) {
        if self.asset != asset {
            self.asset = asset;
            self.changed = true;
            self.display = A::display(
                self.graphics_context.clone(),
                self.assets.clone(),
                self.asset.clone(),
                Vec2::new(50.0, 50.0),
            );
        }
    }
}

impl<A: EditorAssetExt> Widget for AssetSlot<A> {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.display.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.changed = false;

        let (_, drop_value) = self.drop_sink.clone().drop_value(ui, |ui| {
            let response = ui
                .scope(|ui| {
                    ui.set_width(ui.available_width());

                    self.display.ui(ui, context, engine);
                    ui.add(Label::new(self.file_stem()).wrap(true));
                })
                .response;

            response.context_menu(|ui| {
                if ui.button("Empty").clicked() {
                    self.set(AssetRef::default());
                    ui.close_menu();
                }
            })
        });
        if let Some(drop_value) = drop_value {
            self.set(drop_value);
        }
    }
}

impl<A: EditorAssetExt> AssetSlot<A> {
    fn file_stem(&self) -> String {
        self.asset
            .path()
            .and_then(|path| {
                path.file_stem()
                    .and_then(OsStr::to_str)
                    .map(ToOwned::to_owned)
            })
            .unwrap_or_default()
    }
}
