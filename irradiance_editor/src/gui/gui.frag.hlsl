[[vk::binding(0, 0)]]
Texture2D color_texture;
[[vk::binding(0, 0)]]
SamplerState color_sampler;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
    [[vk::location(1)]] float4 color : COLOR0;
};

float4 main(VSOutput input) : SV_TARGET {
    return input.color * color_texture.Sample(color_sampler, input.tex_coord);
}