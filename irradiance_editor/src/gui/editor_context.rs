use std::{collections::HashMap, path::PathBuf, rc::Rc, sync::Arc};

use egui::TextureId;
use irradiance_runtime::{
    core::Engine,
    gfx::{
        device::Device,
        image::{Filter, ImageLayout, ImageView, Sampler, SamplerAddressMode},
        pipeline::{
            DescriptorSet, DescriptorSetLayout, DescriptorType, ShaderStages,
            WriteDescriptorSetImages,
        },
        GraphicsContext, Handle,
    },
};

use crate::{
    asset::{EditorAssetTypes, EditorAssetTypesBuilder},
    core::EditorEngine,
    ecs::{EditorComponentTypes, EditorComponentTypesBuilder},
};

use super::{message::MessageBus, Actions};

/// Editor context.
pub struct EditorContext {
    textures: HashMap<TextureId, (bool, Arc<ImageView>, Arc<DescriptorSet>)>,
    sampler: Arc<Sampler>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    grab_input: bool,
    screen_editor_hovered: bool,
    playing: bool,
    current_asset_path: PathBuf,
    asset_path: PathBuf,
    message_bus: MessageBus,
    actions: Actions,
    component_types: EditorComponentTypes,
    asset_types: Rc<EditorAssetTypes>,
    graphics_context: Arc<GraphicsContext>,
}

impl EditorContext {
    pub(crate) fn new(
        engine: &EditorEngine,
        asset_path: PathBuf,
        asset_types: EditorAssetTypesBuilder,
        component_types: EditorComponentTypesBuilder,
    ) -> Self {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(engine.graphics_context().device().clone());
        let sampler = Self::create_sampler(engine.graphics_context().device().clone());

        Self {
            graphics_context: engine.graphics_context().clone(),
            asset_types: Rc::new(asset_types.build(engine)),
            component_types: component_types.build(),
            actions: Default::default(),
            message_bus: MessageBus::default(),
            asset_path: asset_path.clone(),
            current_asset_path: asset_path,
            playing: false,
            screen_editor_hovered: false,
            grab_input: false,
            descriptor_set_layout,
            sampler,
            textures: Default::default(),
        }
    }

    fn create_descriptor_set_layout(device: Arc<Device>) -> Arc<DescriptorSetLayout> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
            .expect("Failed to create 'DescriptorSetLayout'")
    }

    fn create_sampler(device: Arc<Device>) -> Arc<Sampler> {
        Sampler::builder(device)
            .mag_filter(Filter::Linear)
            .address_mode_u(SamplerAddressMode::ClampToEdge)
            .address_mode_v(SamplerAddressMode::ClampToEdge)
            .address_mode_w(SamplerAddressMode::ClampToEdge)
            .build()
            .unwrap()
    }

    /// Returns the asset type registration.
    pub fn asset_types(&self) -> &Rc<EditorAssetTypes> {
        &self.asset_types
    }

    /// Returns the component type registration.
    pub fn component_types(&self) -> &EditorComponentTypes {
        &self.component_types
    }

    /// Returns the actions.
    pub fn actions(&self) -> &Actions {
        &self.actions
    }

    /// Returns a mutable reference to the actions.
    pub fn actions_mut(&mut self) -> &mut Actions {
        &mut self.actions
    }

    /// Returns the message bus.
    pub fn message_bus(&self) -> &MessageBus {
        &self.message_bus
    }

    /// Returns a mutable reference to the message bus.
    pub fn message_bus_mut(&mut self) -> &mut MessageBus {
        &mut self.message_bus
    }

    /// Returns the asset path.
    pub fn asset_path(&self) -> &PathBuf {
        &self.asset_path
    }

    /// Returns the current asset path.
    pub fn current_asset_path(&self) -> &PathBuf {
        &self.current_asset_path
    }

    /// Returns if the current screen is playing.
    pub fn is_playing(&self) -> bool {
        self.playing
    }

    /// Sets if the current screen is playing.
    pub fn set_playing(&mut self, playing: bool) {
        self.playing = playing;
    }

    pub(crate) fn is_screen_editor_hovered(&self) -> bool {
        self.screen_editor_hovered
    }

    pub(crate) fn set_screen_editor_hovered(&mut self, screen_editor_hovered: bool) {
        self.screen_editor_hovered = screen_editor_hovered;
    }

    /// Returns if the input is grabbed.
    pub fn is_grabbing_input(&self) -> bool {
        self.grab_input
    }

    /// Sets input grabbing.
    pub fn set_grab_input(&mut self, grab_input: bool) {
        self.grab_input = grab_input;
    }

    pub(crate) fn set_current_asset_path(&mut self, current_asset_path: PathBuf) {
        self.current_asset_path = current_asset_path;
    }

    pub(crate) fn textures(
        &mut self,
    ) -> &mut HashMap<TextureId, (bool, Arc<ImageView>, Arc<DescriptorSet>)> {
        &mut self.textures
    }

    /// Registers a native texture.
    pub fn register_texture(
        &mut self,
        is_render_buffer: bool,
        image_view: Arc<ImageView>,
    ) -> TextureId {
        let id = TextureId::User(image_view.image().as_raw());

        self.textures.entry(id).or_insert_with(|| {
            let descriptor_set = DescriptorSet::builder(
                self.graphics_context.device().clone(),
                self.graphics_context
                    .descriptor_pool(&self.descriptor_set_layout)
                    .expect("descriptor pool"),
                self.descriptor_set_layout.clone(),
            )
            .build()
            .unwrap();

            descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &self.sampler,
                    &image_view,
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build()]);

            (is_render_buffer, image_view, descriptor_set)
        });

        id
    }

    pub(crate) fn evict_textures(&mut self) {
        self.textures
            .retain(|_, (is_render_buffer, image_view, _)| {
                !(*is_render_buffer && Arc::strong_count(image_view) == 2)
            });
    }
}
