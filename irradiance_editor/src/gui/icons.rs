use egui::{Color32, FontFamily, FontId, RichText, WidgetText};

/// Icons.
pub struct Icons;

#[allow(missing_docs)]
impl Icons {
    pub const fn circle_right() -> Icon {
        Icon::new("")
    }

    pub const fn circle_nodes() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_sampling() -> Icon {
        Icon::new("")
    }

    pub const fn file_zipper() -> Icon {
        Icon::new("")
    }

    pub const fn dhl() -> Icon {
        Icon::new("")
    }

    pub const fn retweet() -> Icon {
        Icon::new("")
    }

    pub const fn viadeo() -> Icon {
        Icon::new("")
    }

    pub const fn wallet() -> Icon {
        Icon::new("")
    }

    pub const fn neos() -> Icon {
        Icon::new("")
    }

    pub const fn diagram_successor() -> Icon {
        Icon::new("")
    }

    pub const fn synagogue() -> Icon {
        Icon::new("")
    }

    pub const fn cross() -> Icon {
        Icon::new("")
    }

    pub const fn helmet_safety() -> Icon {
        Icon::new("")
    }

    pub const fn linkedin() -> Icon {
        Icon::new("")
    }

    pub const fn square_tumblr() -> Icon {
        Icon::new("")
    }

    pub const fn calendar_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn caret_left() -> Icon {
        Icon::new("")
    }

    pub const fn heading() -> Icon {
        Icon::new("")
    }

    pub const fn share_from_square() -> Icon {
        Icon::new("")
    }

    pub const fn droplet() -> Icon {
        Icon::new("")
    }

    pub const fn xmark() -> Icon {
        Icon::new("")
    }

    pub const fn scroll() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_tongue_wink() -> Icon {
        Icon::new("")
    }

    pub const fn cuttlefish() -> Icon {
        Icon::new("")
    }

    pub const fn crop_simple() -> Icon {
        Icon::new("")
    }

    pub const fn contao() -> Icon {
        Icon::new("")
    }

    pub const fn d_and_d_beyond() -> Icon {
        Icon::new("")
    }

    pub const fn pen_nib() -> Icon {
        Icon::new("")
    }

    pub const fn q() -> Icon {
        Icon::new("Q")
    }

    pub const fn sort_up() -> Icon {
        Icon::new("")
    }

    pub const fn crutch() -> Icon {
        Icon::new("")
    }

    pub const fn bell_slash() -> Icon {
        Icon::new("")
    }

    pub const fn dungeon() -> Icon {
        Icon::new("")
    }

    pub const fn _7() -> Icon {
        Icon::new("7")
    }

    pub const fn guitar() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_squint_tears() -> Icon {
        Icon::new("")
    }

    pub const fn notes_medical() -> Icon {
        Icon::new("")
    }

    pub const fn house_flood_water() -> Icon {
        Icon::new("")
    }

    pub const fn skull_crossbones() -> Icon {
        Icon::new("")
    }

    pub const fn school_flag() -> Icon {
        Icon::new("")
    }

    pub const fn virus_covid() -> Icon {
        Icon::new("")
    }

    pub const fn memory() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down_a_z() -> Icon {
        Icon::new("")
    }

    pub const fn bars_progress() -> Icon {
        Icon::new("")
    }

    pub const fn hill_rockslide() -> Icon {
        Icon::new("")
    }

    pub const fn volume_off() -> Icon {
        Icon::new("")
    }

    pub const fn staylinked() -> Icon {
        Icon::new("")
    }

    pub const fn discourse() -> Icon {
        Icon::new("")
    }

    pub const fn link_slash() -> Icon {
        Icon::new("")
    }

    pub const fn square_facebook() -> Icon {
        Icon::new("")
    }

    pub const fn lock_open() -> Icon {
        Icon::new("")
    }

    pub const fn text_height() -> Icon {
        Icon::new("")
    }

    pub const fn graduation_cap() -> Icon {
        Icon::new("")
    }

    pub const fn square_poll_vertical() -> Icon {
        Icon::new("")
    }

    pub const fn heart_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn hive() -> Icon {
        Icon::new("")
    }

    pub const fn universal_access() -> Icon {
        Icon::new("")
    }

    pub const fn square_whatsapp() -> Icon {
        Icon::new("")
    }

    pub const fn vr_cardboard() -> Icon {
        Icon::new("")
    }

    pub const fn rocket() -> Icon {
        Icon::new("")
    }

    pub const fn microchip() -> Icon {
        Icon::new("")
    }

    pub const fn sith() -> Icon {
        Icon::new("")
    }

    pub const fn padlet() -> Icon {
        Icon::new("")
    }

    pub const fn angle_down() -> Icon {
        Icon::new("")
    }

    pub const fn face_kiss_wink_heart() -> Icon {
        Icon::new("")
    }

    pub const fn heart_crack() -> Icon {
        Icon::new("")
    }

    pub const fn house() -> Icon {
        Icon::new("")
    }

    pub const fn check_double() -> Icon {
        Icon::new("")
    }

    pub const fn _8() -> Icon {
        Icon::new("8")
    }

    pub const fn temperature_arrow_down() -> Icon {
        Icon::new("")
    }

    pub const fn file_invoice() -> Icon {
        Icon::new("")
    }

    pub const fn dove() -> Icon {
        Icon::new("")
    }

    pub const fn mobile() -> Icon {
        Icon::new("")
    }

    pub const fn pepper_hot() -> Icon {
        Icon::new("")
    }

    pub const fn square_font_awesome_stroke() -> Icon {
        Icon::new("")
    }

    pub const fn mandalorian() -> Icon {
        Icon::new("")
    }

    pub const fn taxi() -> Icon {
        Icon::new("")
    }

    pub const fn fedex() -> Icon {
        Icon::new("")
    }

    pub const fn angles_left() -> Icon {
        Icon::new("")
    }

    pub const fn square_github() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_tears() -> Icon {
        Icon::new("")
    }

    pub const fn snowman() -> Icon {
        Icon::new("")
    }

    pub const fn audible() -> Icon {
        Icon::new("")
    }

    pub const fn linode() -> Icon {
        Icon::new("")
    }

    pub const fn square_poll_horizontal() -> Icon {
        Icon::new("")
    }

    pub const fn circle_pause() -> Icon {
        Icon::new("")
    }

    pub const fn orcid() -> Icon {
        Icon::new("")
    }

    pub const fn gitter() -> Icon {
        Icon::new("")
    }

    pub const fn up_down_left_right() -> Icon {
        Icon::new("")
    }

    pub const fn goodreads_g() -> Icon {
        Icon::new("")
    }

    pub const fn list_ul() -> Icon {
        Icon::new("")
    }

    pub const fn file_word() -> Icon {
        Icon::new("")
    }

    pub const fn peseta_sign() -> Icon {
        Icon::new("")
    }

    pub const fn hat_cowboy_side() -> Icon {
        Icon::new("")
    }

    pub const fn fort_awesome() -> Icon {
        Icon::new("")
    }

    pub const fn oil_can() -> Icon {
        Icon::new("")
    }

    pub const fn flag_usa() -> Icon {
        Icon::new("")
    }

    pub const fn user_plus() -> Icon {
        Icon::new("")
    }

    pub const fn child() -> Icon {
        Icon::new("")
    }

    pub const fn _6() -> Icon {
        Icon::new("6")
    }

    pub const fn prescription_bottle() -> Icon {
        Icon::new("")
    }

    pub const fn buy_n_large() -> Icon {
        Icon::new("")
    }

    pub const fn wave_square() -> Icon {
        Icon::new("")
    }

    pub const fn user_astronaut() -> Icon {
        Icon::new("")
    }

    pub const fn galactic_republic() -> Icon {
        Icon::new("")
    }

    pub const fn book_skull() -> Icon {
        Icon::new("")
    }

    pub const fn mars_stroke_up() -> Icon {
        Icon::new("")
    }

    pub const fn box_open() -> Icon {
        Icon::new("")
    }

    pub const fn bus_simple() -> Icon {
        Icon::new("")
    }

    pub const fn medrt() -> Icon {
        Icon::new("")
    }

    pub const fn square_phone_flip() -> Icon {
        Icon::new("")
    }

    pub const fn mastodon() -> Icon {
        Icon::new("")
    }

    pub const fn bus() -> Icon {
        Icon::new("")
    }

    pub const fn pencil() -> Icon {
        Icon::new("")
    }

    pub const fn hand_back_fist() -> Icon {
        Icon::new("")
    }

    pub const fn rotate_left() -> Icon {
        Icon::new("")
    }

    pub const fn hackerrank() -> Icon {
        Icon::new("")
    }

    pub const fn cannabis() -> Icon {
        Icon::new("")
    }

    pub const fn tent_arrow_left_right() -> Icon {
        Icon::new("")
    }

    pub const fn square_dribbble() -> Icon {
        Icon::new("")
    }

    pub const fn up_right_from_square() -> Icon {
        Icon::new("")
    }

    pub const fn egg() -> Icon {
        Icon::new("")
    }

    pub const fn shopify() -> Icon {
        Icon::new("")
    }

    pub const fn adn() -> Icon {
        Icon::new("")
    }

    pub const fn phoenix_framework() -> Icon {
        Icon::new("")
    }

    pub const fn user_tag() -> Icon {
        Icon::new("")
    }

    pub const fn fire_burner() -> Icon {
        Icon::new("")
    }

    pub const fn land_mine_on() -> Icon {
        Icon::new("")
    }

    pub const fn wirsindhandwerk() -> Icon {
        Icon::new("")
    }

    pub const fn chart_column() -> Icon {
        Icon::new("")
    }

    pub const fn keybase() -> Icon {
        Icon::new("")
    }

    pub const fn car() -> Icon {
        Icon::new("")
    }

    pub const fn bag_shopping() -> Icon {
        Icon::new("")
    }

    pub const fn hands_holding_circle() -> Icon {
        Icon::new("")
    }

    pub const fn galactic_senate() -> Icon {
        Icon::new("")
    }

    pub const fn bitcoin_sign() -> Icon {
        Icon::new("")
    }

    pub const fn bitcoin() -> Icon {
        Icon::new("")
    }

    pub const fn bore_hole() -> Icon {
        Icon::new("")
    }

    pub const fn link() -> Icon {
        Icon::new("")
    }

    pub const fn circle_chevron_right() -> Icon {
        Icon::new("")
    }

    pub const fn circle_left() -> Icon {
        Icon::new("")
    }

    pub const fn drupal() -> Icon {
        Icon::new("")
    }

    pub const fn bars() -> Icon {
        Icon::new("")
    }

    pub const fn usps() -> Icon {
        Icon::new("")
    }

    pub const fn user_ninja() -> Icon {
        Icon::new("")
    }

    pub const fn wine_bottle() -> Icon {
        Icon::new("")
    }

    pub const fn circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn wpexplorer() -> Icon {
        Icon::new("")
    }

    pub const fn face_smile_wink() -> Icon {
        Icon::new("")
    }

    pub const fn mask_face() -> Icon {
        Icon::new("")
    }

    pub const fn nutritionix() -> Icon {
        Icon::new("")
    }

    pub const fn poo() -> Icon {
        Icon::new("")
    }

    pub const fn safari() -> Icon {
        Icon::new("")
    }

    pub const fn servicestack() -> Icon {
        Icon::new("")
    }

    pub const fn table_list() -> Icon {
        Icon::new("")
    }

    pub const fn bars_staggered() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_beam_sweat() -> Icon {
        Icon::new("")
    }

    pub const fn person_half_dress() -> Icon {
        Icon::new("")
    }

    pub const fn heart_circle_plus() -> Icon {
        Icon::new("")
    }

    pub const fn itunes() -> Icon {
        Icon::new("")
    }

    pub const fn person_booth() -> Icon {
        Icon::new("")
    }

    pub const fn object_ungroup() -> Icon {
        Icon::new("")
    }

    pub const fn heart_pulse() -> Icon {
        Icon::new("")
    }

    pub const fn square_full() -> Icon {
        Icon::new("")
    }

    pub const fn hand_holding_droplet() -> Icon {
        Icon::new("")
    }

    pub const fn square_plus() -> Icon {
        Icon::new("")
    }

    pub const fn warehouse() -> Icon {
        Icon::new("")
    }

    pub const fn wine_glass_empty() -> Icon {
        Icon::new("")
    }

    pub const fn hashtag() -> Icon {
        Icon::new("#")
    }

    pub const fn tag() -> Icon {
        Icon::new("")
    }

    pub const fn person_running() -> Icon {
        Icon::new("")
    }

    pub const fn schlix() -> Icon {
        Icon::new("")
    }

    pub const fn plug_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn itch_io() -> Icon {
        Icon::new("")
    }

    pub const fn person_military_to_person() -> Icon {
        Icon::new("")
    }

    pub const fn hand_point_right() -> Icon {
        Icon::new("")
    }

    pub const fn ticket() -> Icon {
        Icon::new("")
    }

    pub const fn ebay() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_from_water_pump() -> Icon {
        Icon::new("")
    }

    pub const fn car_burst() -> Icon {
        Icon::new("")
    }

    pub const fn inbox() -> Icon {
        Icon::new("")
    }

    pub const fn rectangle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn square_pied_piper() -> Icon {
        Icon::new("")
    }

    pub const fn unity() -> Icon {
        Icon::new("")
    }

    pub const fn barcode() -> Icon {
        Icon::new("")
    }

    pub const fn key() -> Icon {
        Icon::new("")
    }

    pub const fn pizza_slice() -> Icon {
        Icon::new("")
    }

    pub const fn chrome() -> Icon {
        Icon::new("")
    }

    pub const fn person_military_pointing() -> Icon {
        Icon::new("")
    }

    pub const fn cc_amex() -> Icon {
        Icon::new("")
    }

    pub const fn handshake() -> Icon {
        Icon::new("")
    }

    pub const fn broom_ball() -> Icon {
        Icon::new("")
    }

    pub const fn money_bill_transfer() -> Icon {
        Icon::new("")
    }

    pub const fn boxes_stacked() -> Icon {
        Icon::new("")
    }

    pub const fn bucket() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_left() -> Icon {
        Icon::new("")
    }

    pub const fn stack_overflow() -> Icon {
        Icon::new("")
    }

    pub const fn meetup() -> Icon {
        Icon::new("")
    }

    pub const fn house_lock() -> Icon {
        Icon::new("")
    }

    pub const fn user_minus() -> Icon {
        Icon::new("")
    }

    pub const fn avianex() -> Icon {
        Icon::new("")
    }

    pub const fn paperclip() -> Icon {
        Icon::new("")
    }

    pub const fn square_odnoklassniki() -> Icon {
        Icon::new("")
    }

    pub const fn filter_circle_dollar() -> Icon {
        Icon::new("")
    }

    pub const fn glide() -> Icon {
        Icon::new("")
    }

    pub const fn language() -> Icon {
        Icon::new("")
    }

    pub const fn y() -> Icon {
        Icon::new("Y")
    }

    pub const fn compress() -> Icon {
        Icon::new("")
    }

    pub const fn table() -> Icon {
        Icon::new("")
    }

    pub const fn themeisle() -> Icon {
        Icon::new("")
    }

    pub const fn pied_piper_hat() -> Icon {
        Icon::new("")
    }

    pub const fn flask() -> Icon {
        Icon::new("")
    }

    pub const fn hourglass_start() -> Icon {
        Icon::new("")
    }

    pub const fn forward() -> Icon {
        Icon::new("")
    }

    pub const fn spider() -> Icon {
        Icon::new("")
    }

    pub const fn cloudsmith() -> Icon {
        Icon::new("")
    }

    pub const fn user_injured() -> Icon {
        Icon::new("")
    }

    pub const fn won_sign() -> Icon {
        Icon::new("")
    }

    pub const fn circle_play() -> Icon {
        Icon::new("")
    }

    pub const fn stamp() -> Icon {
        Icon::new("")
    }

    pub const fn user_secret() -> Icon {
        Icon::new("")
    }

    pub const fn align_right() -> Icon {
        Icon::new("")
    }

    pub const fn person_walking_dashed_line_arrow_right() -> Icon {
        Icon::new("")
    }

    pub const fn git_alt() -> Icon {
        Icon::new("")
    }

    pub const fn hacker_news() -> Icon {
        Icon::new("")
    }

    pub const fn guarani_sign() -> Icon {
        Icon::new("")
    }

    pub const fn film() -> Icon {
        Icon::new("")
    }

    pub const fn _5() -> Icon {
        Icon::new("5")
    }

    pub const fn hands() -> Icon {
        Icon::new("")
    }

    pub const fn traffic_light() -> Icon {
        Icon::new("")
    }

    pub const fn person_chalkboard() -> Icon {
        Icon::new("")
    }

    pub const fn desktop() -> Icon {
        Icon::new("")
    }

    pub const fn g() -> Icon {
        Icon::new("G")
    }

    pub const fn road_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn mendeley() -> Icon {
        Icon::new("")
    }

    pub const fn npm() -> Icon {
        Icon::new("")
    }

    pub const fn square_twitter() -> Icon {
        Icon::new("")
    }

    pub const fn opera() -> Icon {
        Icon::new("")
    }

    pub const fn trophy() -> Icon {
        Icon::new("")
    }

    pub const fn circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn square_envelope() -> Icon {
        Icon::new("")
    }

    pub const fn fill_drip() -> Icon {
        Icon::new("")
    }

    pub const fn greater_than_equal() -> Icon {
        Icon::new("")
    }

    pub const fn fonticons() -> Icon {
        Icon::new("")
    }

    pub const fn napster() -> Icon {
        Icon::new("")
    }

    pub const fn envelope_open_text() -> Icon {
        Icon::new("")
    }

    pub const fn hockey_puck() -> Icon {
        Icon::new("")
    }

    pub const fn heart_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn house_user() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up() -> Icon {
        Icon::new("")
    }

    pub const fn gratipay() -> Icon {
        Icon::new("")
    }

    pub const fn shirtsinbulk() -> Icon {
        Icon::new("")
    }

    pub const fn untappd() -> Icon {
        Icon::new("")
    }

    pub const fn scroll_torah() -> Icon {
        Icon::new("")
    }

    pub const fn scale_unbalanced() -> Icon {
        Icon::new("")
    }

    pub const fn file_lines() -> Icon {
        Icon::new("")
    }

    pub const fn sort() -> Icon {
        Icon::new("")
    }

    pub const fn users_between_lines() -> Icon {
        Icon::new("")
    }

    pub const fn face_laugh() -> Icon {
        Icon::new("")
    }

    pub const fn studiovinari() -> Icon {
        Icon::new("")
    }

    pub const fn percent() -> Icon {
        Icon::new("%")
    }

    pub const fn door_closed() -> Icon {
        Icon::new("")
    }

    pub const fn cubes() -> Icon {
        Icon::new("")
    }

    pub const fn display() -> Icon {
        Icon::new("")
    }

    pub const fn hand_pointer() -> Icon {
        Icon::new("")
    }

    pub const fn book_open() -> Icon {
        Icon::new("")
    }

    pub const fn bread_slice() -> Icon {
        Icon::new("")
    }

    pub const fn headset() -> Icon {
        Icon::new("")
    }

    pub const fn person_skiing_nordic() -> Icon {
        Icon::new("")
    }

    pub const fn file_pdf() -> Icon {
        Icon::new("")
    }

    pub const fn shop_lock() -> Icon {
        Icon::new("")
    }

    pub const fn cmplid() -> Icon {
        Icon::new("")
    }

    pub const fn closed_captioning() -> Icon {
        Icon::new("")
    }

    pub const fn ruble_sign() -> Icon {
        Icon::new("")
    }

    pub const fn s() -> Icon {
        Icon::new("S")
    }

    pub const fn store_slash() -> Icon {
        Icon::new("")
    }

    pub const fn reacteurope() -> Icon {
        Icon::new("")
    }

    pub const fn square_behance() -> Icon {
        Icon::new("")
    }

    pub const fn wikipedia_w() -> Icon {
        Icon::new("")
    }

    pub const fn neuter() -> Icon {
        Icon::new("")
    }

    pub const fn up_right_and_down_left_from_center() -> Icon {
        Icon::new("")
    }

    pub const fn chart_line() -> Icon {
        Icon::new("")
    }

    pub const fn stop() -> Icon {
        Icon::new("")
    }

    pub const fn house_flag() -> Icon {
        Icon::new("")
    }

    pub const fn landmark() -> Icon {
        Icon::new("")
    }

    pub const fn circle_dollar_to_slot() -> Icon {
        Icon::new("")
    }

    pub const fn heart_circle_bolt() -> Icon {
        Icon::new("")
    }

    pub const fn photo_film() -> Icon {
        Icon::new("")
    }

    pub const fn accessible_icon() -> Icon {
        Icon::new("")
    }

    pub const fn house_fire() -> Icon {
        Icon::new("")
    }

    pub const fn bolt_lightning() -> Icon {
        Icon::new("")
    }

    pub const fn pen() -> Icon {
        Icon::new("")
    }

    pub const fn football() -> Icon {
        Icon::new("")
    }

    pub const fn people_line() -> Icon {
        Icon::new("")
    }

    pub const fn square_rss() -> Icon {
        Icon::new("")
    }

    pub const fn seedling() -> Icon {
        Icon::new("")
    }

    pub const fn stripe() -> Icon {
        Icon::new("")
    }

    pub const fn bullseye() -> Icon {
        Icon::new("")
    }

    pub const fn snapchat() -> Icon {
        Icon::new("")
    }

    pub const fn dollar_sign() -> Icon {
        Icon::new("$")
    }

    pub const fn mars_stroke_right() -> Icon {
        Icon::new("")
    }

    pub const fn person_arrow_up_from_line() -> Icon {
        Icon::new("")
    }

    pub const fn turn_down() -> Icon {
        Icon::new("")
    }

    pub const fn frog() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_z_a() -> Icon {
        Icon::new("")
    }

    pub const fn heart_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_meatball() -> Icon {
        Icon::new("")
    }

    pub const fn rupee_sign() -> Icon {
        Icon::new("")
    }

    pub const fn chart_bar() -> Icon {
        Icon::new("")
    }

    pub const fn user_clock() -> Icon {
        Icon::new("")
    }

    pub const fn nimblr() -> Icon {
        Icon::new("")
    }

    pub const fn martini_glass() -> Icon {
        Icon::new("")
    }

    pub const fn carrot() -> Icon {
        Icon::new("")
    }

    pub const fn envelope_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn erlang() -> Icon {
        Icon::new("")
    }

    pub const fn hand_holding_medical() -> Icon {
        Icon::new("")
    }

    pub const fn cc_jcb() -> Icon {
        Icon::new("")
    }

    pub const fn cube() -> Icon {
        Icon::new("")
    }

    pub const fn person_shelter() -> Icon {
        Icon::new("")
    }

    pub const fn anchor() -> Icon {
        Icon::new("")
    }

    pub const fn dice_one() -> Icon {
        Icon::new("")
    }

    pub const fn crow() -> Icon {
        Icon::new("")
    }

    pub const fn reply() -> Icon {
        Icon::new("")
    }

    pub const fn handshake_simple_slash() -> Icon {
        Icon::new("")
    }

    pub const fn sitrox() -> Icon {
        Icon::new("")
    }

    pub const fn children() -> Icon {
        Icon::new("")
    }

    pub const fn floppy_disk() -> Icon {
        Icon::new("")
    }

    pub const fn fire() -> Icon {
        Icon::new("")
    }

    pub const fn temperature_arrow_up() -> Icon {
        Icon::new("")
    }

    pub const fn binoculars() -> Icon {
        Icon::new("")
    }

    pub const fn tent() -> Icon {
        Icon::new("")
    }

    pub const fn wix() -> Icon {
        Icon::new("")
    }

    pub const fn bilibili() -> Icon {
        Icon::new("")
    }

    pub const fn black_tie() -> Icon {
        Icon::new("")
    }

    pub const fn house_medical_flag() -> Icon {
        Icon::new("")
    }

    pub const fn shuffle() -> Icon {
        Icon::new("")
    }

    pub const fn phone_slash() -> Icon {
        Icon::new("")
    }

    pub const fn square_pinterest() -> Icon {
        Icon::new("")
    }

    pub const fn infinity() -> Icon {
        Icon::new("")
    }

    pub const fn bluetooth() -> Icon {
        Icon::new("")
    }

    pub const fn restroom() -> Icon {
        Icon::new("")
    }

    pub const fn candy_cane() -> Icon {
        Icon::new("")
    }

    pub const fn feather() -> Icon {
        Icon::new("")
    }

    pub const fn gauge() -> Icon {
        Icon::new("")
    }

    pub const fn mars_and_venus_burst() -> Icon {
        Icon::new("")
    }

    pub const fn unlock() -> Icon {
        Icon::new("")
    }

    pub const fn zhihu() -> Icon {
        Icon::new("")
    }

    pub const fn gavel() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin() -> Icon {
        Icon::new("")
    }

    pub const fn plate_wheat() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_share() -> Icon {
        Icon::new("")
    }

    pub const fn child_dress() -> Icon {
        Icon::new("")
    }

    pub const fn person_skiing() -> Icon {
        Icon::new("")
    }

    pub const fn copyright() -> Icon {
        Icon::new("")
    }

    pub const fn tablet_screen_button() -> Icon {
        Icon::new("")
    }

    pub const fn circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn windows() -> Icon {
        Icon::new("")
    }

    pub const fn shield_heart() -> Icon {
        Icon::new("")
    }

    pub const fn book_open_reader() -> Icon {
        Icon::new("")
    }

    pub const fn angle_right() -> Icon {
        Icon::new("")
    }

    pub const fn chart_area() -> Icon {
        Icon::new("")
    }

    pub const fn cart_shopping() -> Icon {
        Icon::new("")
    }

    pub const fn person_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn code_merge() -> Icon {
        Icon::new("")
    }

    pub const fn magnifying_glass_dollar() -> Icon {
        Icon::new("")
    }

    pub const fn gas_pump() -> Icon {
        Icon::new("")
    }

    pub const fn rectangle_list() -> Icon {
        Icon::new("")
    }

    pub const fn _0() -> Icon {
        Icon::new("0")
    }

    pub const fn fly() -> Icon {
        Icon::new("")
    }

    pub const fn brain() -> Icon {
        Icon::new("")
    }

    pub const fn cc_visa() -> Icon {
        Icon::new("")
    }

    pub const fn ship() -> Icon {
        Icon::new("")
    }

    pub const fn tty() -> Icon {
        Icon::new("")
    }

    pub const fn archway() -> Icon {
        Icon::new("")
    }

    pub const fn angular() -> Icon {
        Icon::new("")
    }

    pub const fn road_bridge() -> Icon {
        Icon::new("")
    }

    pub const fn waze() -> Icon {
        Icon::new("")
    }

    pub const fn square_pen() -> Icon {
        Icon::new("")
    }

    pub const fn swatchbook() -> Icon {
        Icon::new("")
    }

    pub const fn _500_px() -> Icon {
        Icon::new("")
    }

    pub const fn gofore() -> Icon {
        Icon::new("")
    }

    pub const fn file_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn fulcrum() -> Icon {
        Icon::new("")
    }

    pub const fn sign_hanging() -> Icon {
        Icon::new("")
    }

    pub const fn h() -> Icon {
        Icon::new("H")
    }

    pub const fn basket_shopping() -> Icon {
        Icon::new("")
    }

    pub const fn yandex() -> Icon {
        Icon::new("")
    }

    pub const fn magnifying_glass_chart() -> Icon {
        Icon::new("")
    }

    pub const fn broom() -> Icon {
        Icon::new("")
    }

    pub const fn user_large() -> Icon {
        Icon::new("")
    }

    pub const fn ethereum() -> Icon {
        Icon::new("")
    }

    pub const fn square_youtube() -> Icon {
        Icon::new("")
    }

    pub const fn fire_flame_simple() -> Icon {
        Icon::new("")
    }

    pub const fn artstation() -> Icon {
        Icon::new("")
    }

    pub const fn earth_europe() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_showers_water() -> Icon {
        Icon::new("")
    }

    pub const fn ns_8() -> Icon {
        Icon::new("")
    }

    pub const fn golang() -> Icon {
        Icon::new("")
    }

    pub const fn school() -> Icon {
        Icon::new("")
    }

    pub const fn money_bill() -> Icon {
        Icon::new("")
    }

    pub const fn wand_sparkles() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_arrow_down() -> Icon {
        Icon::new("")
    }

    pub const fn network_wired() -> Icon {
        Icon::new("")
    }

    pub const fn xbox() -> Icon {
        Icon::new("")
    }

    pub const fn folder() -> Icon {
        Icon::new("")
    }

    pub const fn magnet() -> Icon {
        Icon::new("")
    }

    pub const fn cash_register() -> Icon {
        Icon::new("")
    }

    pub const fn lastfm() -> Icon {
        Icon::new("")
    }

    pub const fn hurricane() -> Icon {
        Icon::new("")
    }

    pub const fn building_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn nfc_symbol() -> Icon {
        Icon::new("")
    }

    pub const fn paper_plane() -> Icon {
        Icon::new("")
    }

    pub const fn square_font_awesome() -> Icon {
        Icon::new("")
    }

    pub const fn microphone_lines() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_right_dots() -> Icon {
        Icon::new("")
    }

    pub const fn file_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn mosque() -> Icon {
        Icon::new("")
    }

    pub const fn keycdn() -> Icon {
        Icon::new("")
    }

    pub const fn delicious() -> Icon {
        Icon::new("")
    }

    pub const fn wpbeginner() -> Icon {
        Icon::new("")
    }

    pub const fn gift() -> Icon {
        Icon::new("")
    }

    pub const fn house_flood_water_circle_arrow_right() -> Icon {
        Icon::new("")
    }

    pub const fn hands_bound() -> Icon {
        Icon::new("")
    }

    pub const fn amazon() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down() -> Icon {
        Icon::new("")
    }

    pub const fn mountain_sun() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_1_9() -> Icon {
        Icon::new("")
    }

    pub const fn tiktok() -> Icon {
        Icon::new("")
    }

    pub const fn landmark_dome() -> Icon {
        Icon::new("")
    }

    pub const fn draw_polygon() -> Icon {
        Icon::new("")
    }

    pub const fn wpforms() -> Icon {
        Icon::new("")
    }

    pub const fn hubspot() -> Icon {
        Icon::new("")
    }

    pub const fn ribbon() -> Icon {
        Icon::new("")
    }

    pub const fn sellsy() -> Icon {
        Icon::new("")
    }

    pub const fn square_caret_right() -> Icon {
        Icon::new("")
    }

    pub const fn flag_checkered() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_sun_rain() -> Icon {
        Icon::new("")
    }

    pub const fn jar() -> Icon {
        Icon::new("")
    }

    pub const fn clipboard() -> Icon {
        Icon::new("")
    }

    pub const fn hand_point_up() -> Icon {
        Icon::new("")
    }

    pub const fn head_side_virus() -> Icon {
        Icon::new("")
    }

    pub const fn house_signal() -> Icon {
        Icon::new("")
    }

    pub const fn linux() -> Icon {
        Icon::new("")
    }

    pub const fn x_ray() -> Icon {
        Icon::new("")
    }

    pub const fn spoon() -> Icon {
        Icon::new("")
    }

    pub const fn square_check() -> Icon {
        Icon::new("")
    }

    pub const fn table_columns() -> Icon {
        Icon::new("")
    }

    pub const fn circle_stop() -> Icon {
        Icon::new("")
    }

    pub const fn file_medical() -> Icon {
        Icon::new("")
    }

    pub const fn grip() -> Icon {
        Icon::new("")
    }

    pub const fn fan() -> Icon {
        Icon::new("")
    }

    pub const fn quote_right() -> Icon {
        Icon::new("")
    }

    pub const fn shopware() -> Icon {
        Icon::new("")
    }

    pub const fn face_grimace() -> Icon {
        Icon::new("")
    }

    pub const fn left_right() -> Icon {
        Icon::new("")
    }

    pub const fn signal() -> Icon {
        Icon::new("")
    }

    pub const fn affiliatetheme() -> Icon {
        Icon::new("")
    }

    pub const fn border_top_left() -> Icon {
        Icon::new("")
    }

    pub const fn grip_lines() -> Icon {
        Icon::new("")
    }

    pub const fn comments_dollar() -> Icon {
        Icon::new("")
    }

    pub const fn tents() -> Icon {
        Icon::new("")
    }

    pub const fn maximize() -> Icon {
        Icon::new("")
    }

    pub const fn js() -> Icon {
        Icon::new("")
    }

    pub const fn equals() -> Icon {
        Icon::new("=")
    }

    pub const fn jedi_order() -> Icon {
        Icon::new("")
    }

    pub const fn hat_wizard() -> Icon {
        Icon::new("")
    }

    pub const fn tablet_button() -> Icon {
        Icon::new("")
    }

    pub const fn ideal() -> Icon {
        Icon::new("")
    }

    pub const fn wheelchair_move() -> Icon {
        Icon::new("")
    }

    pub const fn rug() -> Icon {
        Icon::new("")
    }

    pub const fn baby_carriage() -> Icon {
        Icon::new("")
    }

    pub const fn compass_drafting() -> Icon {
        Icon::new("")
    }

    pub const fn uniregistry() -> Icon {
        Icon::new("")
    }

    pub const fn hippo() -> Icon {
        Icon::new("")
    }

    pub const fn temperature_half() -> Icon {
        Icon::new("")
    }

    pub const fn person_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn bolt() -> Icon {
        Icon::new("")
    }

    pub const fn align_left() -> Icon {
        Icon::new("")
    }

    pub const fn bandcamp() -> Icon {
        Icon::new("")
    }

    pub const fn circle_half_stroke() -> Icon {
        Icon::new("")
    }

    pub const fn bugs() -> Icon {
        Icon::new("")
    }

    pub const fn magnifying_glass_plus() -> Icon {
        Icon::new("")
    }

    pub const fn palette() -> Icon {
        Icon::new("")
    }

    pub const fn square() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_moon_rain() -> Icon {
        Icon::new("")
    }

    pub const fn hands_bubbles() -> Icon {
        Icon::new("")
    }

    pub const fn cake_candles() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_pd_alt() -> Icon {
        Icon::new("")
    }

    pub const fn up_down() -> Icon {
        Icon::new("")
    }

    pub const fn etsy() -> Icon {
        Icon::new("")
    }

    pub const fn download() -> Icon {
        Icon::new("")
    }

    pub const fn markdown() -> Icon {
        Icon::new("")
    }

    pub const fn hand() -> Icon {
        Icon::new("")
    }

    pub const fn palfed() -> Icon {
        Icon::new("")
    }

    pub const fn user_large_slash() -> Icon {
        Icon::new("")
    }

    pub const fn mug_saucer() -> Icon {
        Icon::new("")
    }

    pub const fn flipboard() -> Icon {
        Icon::new("")
    }

    pub const fn line() -> Icon {
        Icon::new("")
    }

    pub const fn plug_circle_plus() -> Icon {
        Icon::new("")
    }

    pub const fn flag() -> Icon {
        Icon::new("")
    }

    pub const fn empire() -> Icon {
        Icon::new("")
    }

    pub const fn terminal() -> Icon {
        Icon::new("")
    }

    pub const fn timeline() -> Icon {
        Icon::new("")
    }

    pub const fn chevron_right() -> Icon {
        Icon::new("")
    }

    pub const fn trash_can_arrow_up() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_sun() -> Icon {
        Icon::new("")
    }

    pub const fn anchor_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn person_circle_minus() -> Icon {
        Icon::new("")
    }

    pub const fn face_sad_tear() -> Icon {
        Icon::new("")
    }

    pub const fn skyatlas() -> Icon {
        Icon::new("")
    }

    pub const fn less_than() -> Icon {
        Icon::new("<")
    }

    pub const fn handshake_angle() -> Icon {
        Icon::new("")
    }

    pub const fn stroopwafel() -> Icon {
        Icon::new("")
    }

    pub const fn euro_sign() -> Icon {
        Icon::new("")
    }

    pub const fn smog() -> Icon {
        Icon::new("")
    }

    pub const fn check_to_slot() -> Icon {
        Icon::new("")
    }

    pub const fn rectangle_ad() -> Icon {
        Icon::new("")
    }

    pub const fn dochub() -> Icon {
        Icon::new("")
    }

    pub const fn critical_role() -> Icon {
        Icon::new("")
    }

    pub const fn address_book() -> Icon {
        Icon::new("")
    }

    pub const fn battery_quarter() -> Icon {
        Icon::new("")
    }

    pub const fn litecoin_sign() -> Icon {
        Icon::new("")
    }

    pub const fn naira_sign() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_to_dot() -> Icon {
        Icon::new("")
    }

    pub const fn glass_water() -> Icon {
        Icon::new("")
    }

    pub const fn xing() -> Icon {
        Icon::new("")
    }

    pub const fn n() -> Icon {
        Icon::new("N")
    }

    pub const fn sack_dollar() -> Icon {
        Icon::new("")
    }

    pub const fn money_bill_1_wave() -> Icon {
        Icon::new("")
    }

    pub const fn plane_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn head_side_mask() -> Icon {
        Icon::new("")
    }

    pub const fn splotch() -> Icon {
        Icon::new("")
    }

    pub const fn chair() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_nd() -> Icon {
        Icon::new("")
    }

    pub const fn people_roof() -> Icon {
        Icon::new("")
    }

    pub const fn forumbee() -> Icon {
        Icon::new("")
    }

    pub const fn weight_hanging() -> Icon {
        Icon::new("")
    }

    pub const fn microphone_lines_slash() -> Icon {
        Icon::new("")
    }

    pub const fn microscope() -> Icon {
        Icon::new("")
    }

    pub const fn satellite() -> Icon {
        Icon::new("")
    }

    pub const fn biohazard() -> Icon {
        Icon::new("")
    }

    pub const fn hands_holding() -> Icon {
        Icon::new("")
    }

    pub const fn video() -> Icon {
        Icon::new("")
    }

    pub const fn elevator() -> Icon {
        Icon::new("")
    }

    pub const fn laptop_medical() -> Icon {
        Icon::new("")
    }

    pub const fn phone() -> Icon {
        Icon::new("")
    }

    pub const fn camera() -> Icon {
        Icon::new("")
    }

    pub const fn square_parking() -> Icon {
        Icon::new("")
    }

    pub const fn people_group() -> Icon {
        Icon::new("")
    }

    pub const fn square_minus() -> Icon {
        Icon::new("")
    }

    pub const fn google_play() -> Icon {
        Icon::new("")
    }

    pub const fn record_vinyl() -> Icon {
        Icon::new("")
    }

    pub const fn plant_wilt() -> Icon {
        Icon::new("")
    }

    pub const fn eject() -> Icon {
        Icon::new("")
    }

    pub const fn behance() -> Icon {
        Icon::new("")
    }

    pub const fn route() -> Icon {
        Icon::new("")
    }

    pub const fn phabricator() -> Icon {
        Icon::new("")
    }

    pub const fn vector_square() -> Icon {
        Icon::new("")
    }

    pub const fn ferry() -> Icon {
        Icon::new("")
    }

    pub const fn medium() -> Icon {
        Icon::new("")
    }

    pub const fn ruler_vertical() -> Icon {
        Icon::new("")
    }

    pub const fn camera_retro() -> Icon {
        Icon::new("")
    }

    pub const fn drum() -> Icon {
        Icon::new("")
    }

    pub const fn sun() -> Icon {
        Icon::new("")
    }

    pub const fn sticker_mule() -> Icon {
        Icon::new("")
    }

    pub const fn file_prescription() -> Icon {
        Icon::new("")
    }

    pub const fn person_arrow_down_to_line() -> Icon {
        Icon::new("")
    }

    pub const fn wheat_awn_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn outdent() -> Icon {
        Icon::new("")
    }

    pub const fn scale_unbalanced_flip() -> Icon {
        Icon::new("")
    }

    pub const fn marker() -> Icon {
        Icon::new("")
    }

    pub const fn buysellads() -> Icon {
        Icon::new("")
    }

    pub const fn wordpress() -> Icon {
        Icon::new("")
    }

    pub const fn coins() -> Icon {
        Icon::new("")
    }

    pub const fn uber() -> Icon {
        Icon::new("")
    }

    pub const fn users_rays() -> Icon {
        Icon::new("")
    }

    pub const fn person_dress_burst() -> Icon {
        Icon::new("")
    }

    pub const fn bahai() -> Icon {
        Icon::new("")
    }

    pub const fn person_walking() -> Icon {
        Icon::new("")
    }

    pub const fn crosshairs() -> Icon {
        Icon::new("")
    }

    pub const fn face_surprise() -> Icon {
        Icon::new("")
    }

    pub const fn democrat() -> Icon {
        Icon::new("")
    }

    pub const fn battle_net() -> Icon {
        Icon::new("")
    }

    pub const fn envira() -> Icon {
        Icon::new("")
    }

    pub const fn plane() -> Icon {
        Icon::new("")
    }

    pub const fn c() -> Icon {
        Icon::new("C")
    }

    pub const fn diamond_turn_right() -> Icon {
        Icon::new("")
    }

    pub const fn r_project() -> Icon {
        Icon::new("")
    }

    pub const fn bong() -> Icon {
        Icon::new("")
    }

    pub const fn chess_rook() -> Icon {
        Icon::new("")
    }

    pub const fn screwdriver_wrench() -> Icon {
        Icon::new("")
    }

    pub const fn leanpub() -> Icon {
        Icon::new("")
    }

    pub const fn map() -> Icon {
        Icon::new("")
    }

    pub const fn shapes() -> Icon {
        Icon::new("")
    }

    pub const fn building_columns() -> Icon {
        Icon::new("")
    }

    pub const fn person_dots_from_line() -> Icon {
        Icon::new("")
    }

    pub const fn a() -> Icon {
        Icon::new("A")
    }

    pub const fn star_half_stroke() -> Icon {
        Icon::new("")
    }

    pub const fn users_gear() -> Icon {
        Icon::new("")
    }

    pub const fn meta() -> Icon {
        Icon::new("")
    }

    pub const fn symfony() -> Icon {
        Icon::new("")
    }

    pub const fn cc_amazon_pay() -> Icon {
        Icon::new("")
    }

    pub const fn d() -> Icon {
        Icon::new("D")
    }

    pub const fn shop_slash() -> Icon {
        Icon::new("")
    }

    pub const fn rupiah_sign() -> Icon {
        Icon::new("")
    }

    pub const fn person_circle_question() -> Icon {
        Icon::new("")
    }

    pub const fn circle_arrow_down() -> Icon {
        Icon::new("")
    }

    pub const fn patreon() -> Icon {
        Icon::new("")
    }

    pub const fn person_circle_plus() -> Icon {
        Icon::new("")
    }

    pub const fn money_check() -> Icon {
        Icon::new("")
    }

    pub const fn syringe() -> Icon {
        Icon::new("")
    }

    pub const fn crown() -> Icon {
        Icon::new("")
    }

    pub const fn venus() -> Icon {
        Icon::new("")
    }

    pub const fn shield_halved() -> Icon {
        Icon::new("")
    }

    pub const fn dice_d_20() -> Icon {
        Icon::new("")
    }

    pub const fn star() -> Icon {
        Icon::new("")
    }

    pub const fn grip_lines_vertical() -> Icon {
        Icon::new("")
    }

    pub const fn podcast() -> Icon {
        Icon::new("")
    }

    pub const fn icicles() -> Icon {
        Icon::new("")
    }

    pub const fn dharmachakra() -> Icon {
        Icon::new("")
    }

    pub const fn researchgate() -> Icon {
        Icon::new("")
    }

    pub const fn subscript() -> Icon {
        Icon::new("")
    }

    pub const fn kickstarter_k() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_split_up_and_left() -> Icon {
        Icon::new("")
    }

    pub const fn raspberry_pi() -> Icon {
        Icon::new("")
    }

    pub const fn yarn() -> Icon {
        Icon::new("")
    }

    pub const fn magnifying_glass_arrow_right() -> Icon {
        Icon::new("")
    }

    pub const fn share() -> Icon {
        Icon::new("")
    }

    pub const fn file_circle_question() -> Icon {
        Icon::new("")
    }

    pub const fn swift() -> Icon {
        Icon::new("")
    }

    pub const fn screenpal() -> Icon {
        Icon::new("")
    }

    pub const fn dice() -> Icon {
        Icon::new("")
    }

    pub const fn lira_sign() -> Icon {
        Icon::new("")
    }

    pub const fn list_ol() -> Icon {
        Icon::new("")
    }

    pub const fn laptop() -> Icon {
        Icon::new("")
    }

    pub const fn user_shield() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_pointer() -> Icon {
        Icon::new("")
    }

    pub const fn shuttle_space() -> Icon {
        Icon::new("")
    }

    pub const fn backward_fast() -> Icon {
        Icon::new("")
    }

    pub const fn hotel() -> Icon {
        Icon::new("")
    }

    pub const fn reply_all() -> Icon {
        Icon::new("")
    }

    pub const fn adversal() -> Icon {
        Icon::new("")
    }

    pub const fn person_praying() -> Icon {
        Icon::new("")
    }

    pub const fn van_shuttle() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_arrow_up() -> Icon {
        Icon::new("")
    }

    pub const fn peso_sign() -> Icon {
        Icon::new("")
    }

    pub const fn certificate() -> Icon {
        Icon::new("")
    }

    pub const fn crop() -> Icon {
        Icon::new("")
    }

    pub const fn tent_arrows_down() -> Icon {
        Icon::new("")
    }

    pub const fn hand_fist() -> Icon {
        Icon::new("")
    }

    pub const fn moon() -> Icon {
        Icon::new("")
    }

    pub const fn speaker_deck() -> Icon {
        Icon::new("")
    }

    pub const fn resolving() -> Icon {
        Icon::new("")
    }

    pub const fn chevron_up() -> Icon {
        Icon::new("")
    }

    pub const fn right_from_bracket() -> Icon {
        Icon::new("")
    }

    pub const fn location_crosshairs() -> Icon {
        Icon::new("")
    }

    pub const fn vial_virus() -> Icon {
        Icon::new("")
    }

    pub const fn octopus_deploy() -> Icon {
        Icon::new("")
    }

    pub const fn truck_field_un() -> Icon {
        Icon::new("")
    }

    pub const fn file_excel() -> Icon {
        Icon::new("")
    }

    pub const fn jira() -> Icon {
        Icon::new("")
    }

    pub const fn mixcloud() -> Icon {
        Icon::new("")
    }

    pub const fn truck_arrow_right() -> Icon {
        Icon::new("")
    }

    pub const fn bacterium() -> Icon {
        Icon::new("")
    }

    pub const fn angellist() -> Icon {
        Icon::new("")
    }

    pub const fn play() -> Icon {
        Icon::new("")
    }

    pub const fn print() -> Icon {
        Icon::new("")
    }

    pub const fn cotton_bureau() -> Icon {
        Icon::new("")
    }

    pub const fn css_3_alt() -> Icon {
        Icon::new("")
    }

    pub const fn robot() -> Icon {
        Icon::new("")
    }

    pub const fn plane_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_showers_heavy() -> Icon {
        Icon::new("")
    }

    pub const fn w() -> Icon {
        Icon::new("W")
    }

    pub const fn google_wallet() -> Icon {
        Icon::new("")
    }

    pub const fn turn_up() -> Icon {
        Icon::new("")
    }

    pub const fn sort_down() -> Icon {
        Icon::new("")
    }

    pub const fn square_steam() -> Icon {
        Icon::new("")
    }

    pub const fn diagram_next() -> Icon {
        Icon::new("")
    }

    pub const fn not_equal() -> Icon {
        Icon::new("")
    }

    pub const fn elementor() -> Icon {
        Icon::new("")
    }

    pub const fn circle_arrow_right() -> Icon {
        Icon::new("")
    }

    pub const fn supple() -> Icon {
        Icon::new("")
    }

    pub const fn aviato() -> Icon {
        Icon::new("")
    }

    pub const fn handshake_simple() -> Icon {
        Icon::new("")
    }

    pub const fn pen_fancy() -> Icon {
        Icon::new("")
    }

    pub const fn user_tie() -> Icon {
        Icon::new("")
    }

    pub const fn bell() -> Icon {
        Icon::new("")
    }

    pub const fn hospital() -> Icon {
        Icon::new("")
    }

    pub const fn angles_right() -> Icon {
        Icon::new("")
    }

    pub const fn star_of_david() -> Icon {
        Icon::new("")
    }

    pub const fn clock_rotate_left() -> Icon {
        Icon::new("")
    }

    pub const fn deviantart() -> Icon {
        Icon::new("")
    }

    pub const fn scale_balanced() -> Icon {
        Icon::new("")
    }

    pub const fn wodu() -> Icon {
        Icon::new("")
    }

    pub const fn earlybirds() -> Icon {
        Icon::new("")
    }

    pub const fn bacon() -> Icon {
        Icon::new("")
    }

    pub const fn ranking_star() -> Icon {
        Icon::new("")
    }

    pub const fn google_pay() -> Icon {
        Icon::new("")
    }

    pub const fn suitcase() -> Icon {
        Icon::new("")
    }

    pub const fn heart_circle_minus() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_squint() -> Icon {
        Icon::new("")
    }

    pub const fn deskpro() -> Icon {
        Icon::new("")
    }

    pub const fn mizuni() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down_wide_short() -> Icon {
        Icon::new("")
    }

    pub const fn calendar_check() -> Icon {
        Icon::new("")
    }

    pub const fn wheat_awn() -> Icon {
        Icon::new("")
    }

    pub const fn dice_five() -> Icon {
        Icon::new("")
    }

    pub const fn hand_middle_finger() -> Icon {
        Icon::new("")
    }

    pub const fn file_invoice_dollar() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_wink() -> Icon {
        Icon::new("")
    }

    pub const fn share_nodes() -> Icon {
        Icon::new("")
    }

    pub const fn vial_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn pills() -> Icon {
        Icon::new("")
    }

    pub const fn map_pin() -> Icon {
        Icon::new("")
    }

    pub const fn chess_pawn() -> Icon {
        Icon::new("")
    }

    pub const fn road_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn mortar_pestle() -> Icon {
        Icon::new("")
    }

    pub const fn umbrella_beach() -> Icon {
        Icon::new("")
    }

    pub const fn newspaper() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_hearts() -> Icon {
        Icon::new("")
    }

    pub const fn head_side_cough_slash() -> Icon {
        Icon::new("")
    }

    pub const fn bimobject() -> Icon {
        Icon::new("")
    }

    pub const fn dolly() -> Icon {
        Icon::new("")
    }

    pub const fn space_awesome() -> Icon {
        Icon::new("")
    }

    pub const fn minimize() -> Icon {
        Icon::new("")
    }

    pub const fn jsfiddle() -> Icon {
        Icon::new("")
    }

    pub const fn wifi() -> Icon {
        Icon::new("")
    }

    pub const fn om() -> Icon {
        Icon::new("")
    }

    pub const fn flask_vial() -> Icon {
        Icon::new("")
    }

    pub const fn person_breastfeeding() -> Icon {
        Icon::new("")
    }

    pub const fn clipboard_user() -> Icon {
        Icon::new("")
    }

    pub const fn power_off() -> Icon {
        Icon::new("")
    }

    pub const fn kit_medical() -> Icon {
        Icon::new("")
    }

    pub const fn wrench() -> Icon {
        Icon::new("")
    }

    pub const fn lungs() -> Icon {
        Icon::new("")
    }

    pub const fn phoenix_squadron() -> Icon {
        Icon::new("")
    }

    pub const fn clapperboard() -> Icon {
        Icon::new("")
    }

    pub const fn truck_fast() -> Icon {
        Icon::new("")
    }

    pub const fn user_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn angles_up() -> Icon {
        Icon::new("")
    }

    pub const fn anchor_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn brush() -> Icon {
        Icon::new("")
    }

    pub const fn mill_sign() -> Icon {
        Icon::new("")
    }

    pub const fn road_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn cart_arrow_down() -> Icon {
        Icon::new("")
    }

    pub const fn ankh() -> Icon {
        Icon::new("")
    }

    pub const fn book_journal_whills() -> Icon {
        Icon::new("")
    }

    pub const fn internet_explorer() -> Icon {
        Icon::new("")
    }

    pub const fn cc_paypal() -> Icon {
        Icon::new("")
    }

    pub const fn shirt() -> Icon {
        Icon::new("")
    }

    pub const fn squarespace() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down_z_a() -> Icon {
        Icon::new("")
    }

    pub const fn money_check_dollar() -> Icon {
        Icon::new("")
    }

    pub const fn radio() -> Icon {
        Icon::new("")
    }

    pub const fn water_ladder() -> Icon {
        Icon::new("")
    }

    pub const fn austral_sign() -> Icon {
        Icon::new("")
    }

    pub const fn soundcloud() -> Icon {
        Icon::new("")
    }

    pub const fn disease() -> Icon {
        Icon::new("")
    }

    pub const fn yoast() -> Icon {
        Icon::new("")
    }

    pub const fn openid() -> Icon {
        Icon::new("")
    }

    pub const fn hand_point_left() -> Icon {
        Icon::new("")
    }

    pub const fn golf_ball_tee() -> Icon {
        Icon::new("")
    }

    pub const fn chess_queen() -> Icon {
        Icon::new("")
    }

    pub const fn draft_2_digital() -> Icon {
        Icon::new("")
    }

    pub const fn thumbs_up() -> Icon {
        Icon::new("")
    }

    pub const fn eye_dropper() -> Icon {
        Icon::new("")
    }

    pub const fn mitten() -> Icon {
        Icon::new("")
    }

    pub const fn _42_group() -> Icon {
        Icon::new("")
    }

    pub const fn clipboard_question() -> Icon {
        Icon::new("")
    }

    pub const fn magnifying_glass_location() -> Icon {
        Icon::new("")
    }

    pub const fn reddit_alien() -> Icon {
        Icon::new("")
    }

    pub const fn xmarks_lines() -> Icon {
        Icon::new("")
    }

    pub const fn paint_roller() -> Icon {
        Icon::new("")
    }

    pub const fn modx() -> Icon {
        Icon::new("")
    }

    pub const fn money_bill_wave() -> Icon {
        Icon::new("")
    }

    pub const fn bomb() -> Icon {
        Icon::new("")
    }

    pub const fn m() -> Icon {
        Icon::new("M")
    }

    pub const fn earth_asia() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_beam() -> Icon {
        Icon::new("")
    }

    pub const fn peace() -> Icon {
        Icon::new("")
    }

    pub const fn align_center() -> Icon {
        Icon::new("")
    }

    pub const fn martini_glass_citrus() -> Icon {
        Icon::new("")
    }

    pub const fn plug() -> Icon {
        Icon::new("")
    }

    pub const fn border_none() -> Icon {
        Icon::new("")
    }

    pub const fn book_bible() -> Icon {
        Icon::new("")
    }

    pub const fn ring() -> Icon {
        Icon::new("")
    }

    pub const fn underline() -> Icon {
        Icon::new("")
    }

    pub const fn trade_federation() -> Icon {
        Icon::new("")
    }

    pub const fn free_code_camp() -> Icon {
        Icon::new("")
    }

    pub const fn file_arrow_down() -> Icon {
        Icon::new("")
    }

    pub const fn wizards_of_the_coast() -> Icon {
        Icon::new("")
    }

    pub const fn dragon() -> Icon {
        Icon::new("")
    }

    pub const fn school_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn sd_card() -> Icon {
        Icon::new("")
    }

    pub const fn firefox_browser() -> Icon {
        Icon::new("")
    }

    pub const fn chalkboard_user() -> Icon {
        Icon::new("")
    }

    pub const fn delete_left() -> Icon {
        Icon::new("")
    }

    pub const fn hard_drive() -> Icon {
        Icon::new("")
    }

    pub const fn house_chimney_crack() -> Icon {
        Icon::new("")
    }

    pub const fn medapps() -> Icon {
        Icon::new("")
    }

    pub const fn signs_post() -> Icon {
        Icon::new("")
    }

    pub const fn redhat() -> Icon {
        Icon::new("")
    }

    pub const fn menorah() -> Icon {
        Icon::new("")
    }

    pub const fn building() -> Icon {
        Icon::new("")
    }

    pub const fn pinterest_p() -> Icon {
        Icon::new("")
    }

    pub const fn qrcode() -> Icon {
        Icon::new("")
    }

    pub const fn square_google_plus() -> Icon {
        Icon::new("")
    }

    pub const fn square_xing() -> Icon {
        Icon::new("")
    }

    pub const fn circle_user() -> Icon {
        Icon::new("")
    }

    pub const fn pump_soap() -> Icon {
        Icon::new("")
    }

    pub const fn node() -> Icon {
        Icon::new("")
    }

    pub const fn ethernet() -> Icon {
        Icon::new("")
    }

    pub const fn google_plus_g() -> Icon {
        Icon::new("")
    }

    pub const fn train() -> Icon {
        Icon::new("")
    }

    pub const fn fish() -> Icon {
        Icon::new("")
    }

    pub const fn java() -> Icon {
        Icon::new("")
    }

    pub const fn sink() -> Icon {
        Icon::new("")
    }

    pub const fn book() -> Icon {
        Icon::new("")
    }

    pub const fn tape() -> Icon {
        Icon::new("")
    }

    pub const fn droplet_slash() -> Icon {
        Icon::new("")
    }

    pub const fn road_spikes() -> Icon {
        Icon::new("")
    }

    pub const fn ussunnah() -> Icon {
        Icon::new("")
    }

    pub const fn up_long() -> Icon {
        Icon::new("")
    }

    pub const fn whiskey_glass() -> Icon {
        Icon::new("")
    }

    pub const fn pinterest() -> Icon {
        Icon::new("")
    }

    pub const fn square_caret_left() -> Icon {
        Icon::new("")
    }

    pub const fn l() -> Icon {
        Icon::new("L")
    }

    pub const fn fax() -> Icon {
        Icon::new("")
    }

    pub const fn hire_a_helper() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_nc() -> Icon {
        Icon::new("")
    }

    pub const fn cpanel() -> Icon {
        Icon::new("")
    }

    pub const fn person_walking_with_cane() -> Icon {
        Icon::new("")
    }

    pub const fn spotify() -> Icon {
        Icon::new("")
    }

    pub const fn vest() -> Icon {
        Icon::new("")
    }

    pub const fn baseball_bat_ball() -> Icon {
        Icon::new("")
    }

    pub const fn house_medical() -> Icon {
        Icon::new("")
    }

    pub const fn pix() -> Icon {
        Icon::new("")
    }

    pub const fn instagram() -> Icon {
        Icon::new("")
    }

    pub const fn replyd() -> Icon {
        Icon::new("")
    }

    pub const fn tumblr() -> Icon {
        Icon::new("")
    }

    pub const fn bottle_water() -> Icon {
        Icon::new("")
    }

    pub const fn school_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn calendar_week() -> Icon {
        Icon::new("")
    }

    pub const fn sim_card() -> Icon {
        Icon::new("")
    }

    pub const fn vest_patches() -> Icon {
        Icon::new("")
    }

    pub const fn wordpress_simple() -> Icon {
        Icon::new("")
    }

    pub const fn user_pen() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_left_right_to_line() -> Icon {
        Icon::new("")
    }

    pub const fn playstation() -> Icon {
        Icon::new("")
    }

    pub const fn lightbulb() -> Icon {
        Icon::new("")
    }

    pub const fn car_tunnel() -> Icon {
        Icon::new("")
    }

    pub const fn less_than_equal() -> Icon {
        Icon::new("")
    }

    pub const fn pied_piper_alt() -> Icon {
        Icon::new("")
    }

    pub const fn car_side() -> Icon {
        Icon::new("")
    }

    pub const fn yelp() -> Icon {
        Icon::new("")
    }

    pub const fn ups() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_tongue_squint() -> Icon {
        Icon::new("")
    }

    pub const fn faucet_drip() -> Icon {
        Icon::new("")
    }

    pub const fn square_lastfm() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down_up_lock() -> Icon {
        Icon::new("")
    }

    pub const fn microphone_slash() -> Icon {
        Icon::new("")
    }

    pub const fn receipt() -> Icon {
        Icon::new("")
    }

    pub const fn circle_question() -> Icon {
        Icon::new("")
    }

    pub const fn r() -> Icon {
        Icon::new("R")
    }

    pub const fn mixer() -> Icon {
        Icon::new("")
    }

    pub const fn globe() -> Icon {
        Icon::new("")
    }

    pub const fn deezer() -> Icon {
        Icon::new("")
    }

    pub const fn tenge_sign() -> Icon {
        Icon::new("")
    }

    pub const fn twitter() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_to_eye() -> Icon {
        Icon::new("")
    }

    pub const fn list() -> Icon {
        Icon::new("")
    }

    pub const fn book_atlas() -> Icon {
        Icon::new("")
    }

    pub const fn bridge_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn scribd() -> Icon {
        Icon::new("")
    }

    pub const fn hands_clapping() -> Icon {
        Icon::new("")
    }

    pub const fn briefcase() -> Icon {
        Icon::new("")
    }

    pub const fn accusoft() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_down_to_people() -> Icon {
        Icon::new("")
    }

    pub const fn baht_sign() -> Icon {
        Icon::new("")
    }

    pub const fn calendar_days() -> Icon {
        Icon::new("")
    }

    pub const fn lungs_virus() -> Icon {
        Icon::new("")
    }

    pub const fn uncharted() -> Icon {
        Icon::new("")
    }

    pub const fn virus_slash() -> Icon {
        Icon::new("")
    }

    pub const fn cart_flatbed_suitcase() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_9_1() -> Icon {
        Icon::new("")
    }

    pub const fn code_pull_request() -> Icon {
        Icon::new("")
    }

    pub const fn vaadin() -> Icon {
        Icon::new("")
    }

    pub const fn calculator() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_long() -> Icon {
        Icon::new("")
    }

    pub const fn viber() -> Icon {
        Icon::new("")
    }

    pub const fn dog() -> Icon {
        Icon::new("")
    }

    pub const fn circle_chevron_down() -> Icon {
        Icon::new("")
    }

    pub const fn connectdevelop() -> Icon {
        Icon::new("")
    }

    pub const fn imdb() -> Icon {
        Icon::new("")
    }

    pub const fn code() -> Icon {
        Icon::new("")
    }

    pub const fn champagne_glasses() -> Icon {
        Icon::new("")
    }

    pub const fn instalod() -> Icon {
        Icon::new("")
    }

    pub const fn cc_stripe() -> Icon {
        Icon::new("")
    }

    pub const fn teeth_open() -> Icon {
        Icon::new("")
    }

    pub const fn joget() -> Icon {
        Icon::new("")
    }

    pub const fn node_js() -> Icon {
        Icon::new("")
    }

    pub const fn dribbble() -> Icon {
        Icon::new("")
    }

    pub const fn layer_group() -> Icon {
        Icon::new("")
    }

    pub const fn briefcase_medical() -> Icon {
        Icon::new("")
    }

    pub const fn signature() -> Icon {
        Icon::new("")
    }

    pub const fn id_card_clip() -> Icon {
        Icon::new("")
    }

    pub const fn face_frown() -> Icon {
        Icon::new("")
    }

    pub const fn mug_hot() -> Icon {
        Icon::new("")
    }

    pub const fn trowel() -> Icon {
        Icon::new("")
    }

    pub const fn note_sticky() -> Icon {
        Icon::new("")
    }

    pub const fn old_republic() -> Icon {
        Icon::new("")
    }

    pub const fn invision() -> Icon {
        Icon::new("")
    }

    pub const fn truck_plane() -> Icon {
        Icon::new("")
    }

    pub const fn temperature_full() -> Icon {
        Icon::new("")
    }

    pub const fn code_compare() -> Icon {
        Icon::new("")
    }

    pub const fn teeth() -> Icon {
        Icon::new("")
    }

    pub const fn magnifying_glass_minus() -> Icon {
        Icon::new("")
    }

    pub const fn temperature_quarter() -> Icon {
        Icon::new("")
    }

    pub const fn anchor_lock() -> Icon {
        Icon::new("")
    }

    pub const fn backward_step() -> Icon {
        Icon::new("")
    }

    pub const fn audio_description() -> Icon {
        Icon::new("")
    }

    pub const fn right_left() -> Icon {
        Icon::new("")
    }

    pub const fn slash() -> Icon {
        Icon::new("")
    }

    pub const fn image_portrait() -> Icon {
        Icon::new("")
    }

    pub const fn pallet() -> Icon {
        Icon::new("")
    }

    pub const fn angles_down() -> Icon {
        Icon::new("")
    }

    pub const fn amilia() -> Icon {
        Icon::new("")
    }

    pub const fn sass() -> Icon {
        Icon::new("")
    }

    pub const fn face_dizzy() -> Icon {
        Icon::new("")
    }

    pub const fn spray_can() -> Icon {
        Icon::new("")
    }

    pub const fn v() -> Icon {
        Icon::new("V")
    }

    pub const fn hand_dots() -> Icon {
        Icon::new("")
    }

    pub const fn spray_can_sparkles() -> Icon {
        Icon::new("")
    }

    pub const fn laptop_code() -> Icon {
        Icon::new("")
    }

    pub const fn location_arrow() -> Icon {
        Icon::new("")
    }

    pub const fn ban() -> Icon {
        Icon::new("")
    }

    pub const fn bone() -> Icon {
        Icon::new("")
    }

    pub const fn mailchimp() -> Icon {
        Icon::new("")
    }

    pub const fn typo_3() -> Icon {
        Icon::new("")
    }

    pub const fn baby() -> Icon {
        Icon::new("")
    }

    pub const fn plane_departure() -> Icon {
        Icon::new("")
    }

    pub const fn camera_rotate() -> Icon {
        Icon::new("")
    }

    pub const fn folder_plus() -> Icon {
        Icon::new("")
    }

    pub const fn square_reddit() -> Icon {
        Icon::new("")
    }

    pub const fn first_order_alt() -> Icon {
        Icon::new("")
    }

    pub const fn gears() -> Icon {
        Icon::new("")
    }

    pub const fn calendar_minus() -> Icon {
        Icon::new("")
    }

    pub const fn leaf() -> Icon {
        Icon::new("")
    }

    pub const fn screwdriver() -> Icon {
        Icon::new("")
    }

    pub const fn forward_fast() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_pd() -> Icon {
        Icon::new("")
    }

    pub const fn perbyte() -> Icon {
        Icon::new("")
    }

    pub const fn map_location_dot() -> Icon {
        Icon::new("")
    }

    pub const fn umbraco() -> Icon {
        Icon::new("")
    }

    pub const fn chess_knight() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_rotate_left() -> Icon {
        Icon::new("")
    }

    pub const fn bridge_lock() -> Icon {
        Icon::new("")
    }

    pub const fn faucet() -> Icon {
        Icon::new("")
    }

    pub const fn hands_asl_interpreting() -> Icon {
        Icon::new("")
    }

    pub const fn masks_theater() -> Icon {
        Icon::new("")
    }

    pub const fn blog() -> Icon {
        Icon::new("")
    }

    pub const fn square_root_variable() -> Icon {
        Icon::new("")
    }

    pub const fn place_of_worship() -> Icon {
        Icon::new("")
    }

    pub const fn dumpster() -> Icon {
        Icon::new("")
    }

    pub const fn gauge_simple_high() -> Icon {
        Icon::new("")
    }

    pub const fn bowling_ball() -> Icon {
        Icon::new("")
    }

    pub const fn industry() -> Icon {
        Icon::new("")
    }

    pub const fn notdef() -> Icon {
        Icon::new("")
    }

    pub const fn genderless() -> Icon {
        Icon::new("")
    }

    pub const fn person() -> Icon {
        Icon::new("")
    }

    pub const fn square_up_right() -> Icon {
        Icon::new("")
    }

    pub const fn face_laugh_squint() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_to_circle() -> Icon {
        Icon::new("")
    }

    pub const fn dev() -> Icon {
        Icon::new("")
    }

    pub const fn product_hunt() -> Icon {
        Icon::new("")
    }

    pub const fn indian_rupee_sign() -> Icon {
        Icon::new("")
    }

    pub const fn bug_slash() -> Icon {
        Icon::new("")
    }

    pub const fn thermometer() -> Icon {
        Icon::new("")
    }

    pub const fn jenkins() -> Icon {
        Icon::new("")
    }

    pub const fn circle_arrow_up() -> Icon {
        Icon::new("")
    }

    pub const fn gg_circle() -> Icon {
        Icon::new("")
    }

    pub const fn square_js() -> Icon {
        Icon::new("")
    }

    pub const fn users_slash() -> Icon {
        Icon::new("")
    }

    pub const fn mobile_retro() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down_9_1() -> Icon {
        Icon::new("")
    }

    pub const fn hand_lizard() -> Icon {
        Icon::new("")
    }

    pub const fn person_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn file_video() -> Icon {
        Icon::new("")
    }

    pub const fn hornbill() -> Icon {
        Icon::new("")
    }

    pub const fn salesforce() -> Icon {
        Icon::new("")
    }

    pub const fn user_nurse() -> Icon {
        Icon::new("")
    }

    pub const fn mercury() -> Icon {
        Icon::new("")
    }

    pub const fn chart_simple() -> Icon {
        Icon::new("")
    }

    pub const fn guilded() -> Icon {
        Icon::new("")
    }

    pub const fn holly_berry() -> Icon {
        Icon::new("")
    }

    pub const fn microblog() -> Icon {
        Icon::new("")
    }

    pub const fn clock() -> Icon {
        Icon::new("")
    }

    pub const fn user_check() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_trend_up() -> Icon {
        Icon::new("")
    }

    pub const fn heart() -> Icon {
        Icon::new("")
    }

    pub const fn person_through_window() -> Icon {
        Icon::new("")
    }

    pub const fn tags() -> Icon {
        Icon::new("")
    }

    pub const fn tree_city() -> Icon {
        Icon::new("")
    }

    pub const fn mobile_button() -> Icon {
        Icon::new("")
    }

    pub const fn child_reaching() -> Icon {
        Icon::new("")
    }

    pub const fn vnv() -> Icon {
        Icon::new("")
    }

    pub const fn face_laugh_beam() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_rotate_right() -> Icon {
        Icon::new("")
    }

    pub const fn hand_spock() -> Icon {
        Icon::new("")
    }

    pub const fn shop() -> Icon {
        Icon::new("")
    }

    pub const fn mosquito() -> Icon {
        Icon::new("")
    }

    pub const fn sun_plant_wilt() -> Icon {
        Icon::new("")
    }

    pub const fn opencart() -> Icon {
        Icon::new("")
    }

    pub const fn glass_water_droplet() -> Icon {
        Icon::new("")
    }

    pub const fn down_long() -> Icon {
        Icon::new("")
    }

    pub const fn shield_virus() -> Icon {
        Icon::new("")
    }

    pub const fn person_rays() -> Icon {
        Icon::new("")
    }

    pub const fn chess_king() -> Icon {
        Icon::new("")
    }

    pub const fn circle_chevron_left() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_moon() -> Icon {
        Icon::new("")
    }

    pub const fn circle_dot() -> Icon {
        Icon::new("")
    }

    pub const fn building_lock() -> Icon {
        Icon::new("")
    }

    pub const fn monument() -> Icon {
        Icon::new("")
    }

    pub const fn blogger_b() -> Icon {
        Icon::new("")
    }

    pub const fn yin_yang() -> Icon {
        Icon::new("")
    }

    pub const fn atlassian() -> Icon {
        Icon::new("")
    }

    pub const fn minus() -> Icon {
        Icon::new("")
    }

    pub const fn shrimp() -> Icon {
        Icon::new("")
    }

    pub const fn pied_piper() -> Icon {
        Icon::new("")
    }

    pub const fn magento() -> Icon {
        Icon::new("")
    }

    pub const fn stumbleupon_circle() -> Icon {
        Icon::new("")
    }

    pub const fn venus_double() -> Icon {
        Icon::new("")
    }

    pub const fn yahoo() -> Icon {
        Icon::new("")
    }

    pub const fn burst() -> Icon {
        Icon::new("")
    }

    pub const fn shield() -> Icon {
        Icon::new("")
    }

    pub const fn tencent_weibo() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_zero() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_sampling_plus() -> Icon {
        Icon::new("")
    }

    pub const fn amazon_pay() -> Icon {
        Icon::new("")
    }

    pub const fn google() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_left_long() -> Icon {
        Icon::new("")
    }

    pub const fn headphones_simple() -> Icon {
        Icon::new("")
    }

    pub const fn fantasy_flight_games() -> Icon {
        Icon::new("")
    }

    pub const fn comment() -> Icon {
        Icon::new("")
    }

    pub const fn wheelchair() -> Icon {
        Icon::new("")
    }

    pub const fn computer_mouse() -> Icon {
        Icon::new("")
    }

    pub const fn people_carry_box() -> Icon {
        Icon::new("")
    }

    pub const fn chart_pie() -> Icon {
        Icon::new("")
    }

    pub const fn person_snowboarding() -> Icon {
        Icon::new("")
    }

    pub const fn brazilian_real_sign() -> Icon {
        Icon::new("")
    }

    pub const fn sitemap() -> Icon {
        Icon::new("")
    }

    pub const fn locust() -> Icon {
        Icon::new("")
    }

    pub const fn aws() -> Icon {
        Icon::new("")
    }

    pub const fn bath() -> Icon {
        Icon::new("")
    }

    pub const fn bandage() -> Icon {
        Icon::new("")
    }

    pub const fn meteor() -> Icon {
        Icon::new("")
    }

    pub const fn pen_clip() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_short_wide() -> Icon {
        Icon::new("")
    }

    pub const fn city() -> Icon {
        Icon::new("")
    }

    pub const fn dice_three() -> Icon {
        Icon::new("")
    }

    pub const fn spinner() -> Icon {
        Icon::new("")
    }

    pub const fn toolbox() -> Icon {
        Icon::new("")
    }

    pub const fn chalkboard() -> Icon {
        Icon::new("")
    }

    pub const fn square_caret_down() -> Icon {
        Icon::new("")
    }

    pub const fn quinscape() -> Icon {
        Icon::new("")
    }

    pub const fn jedi() -> Icon {
        Icon::new("")
    }

    pub const fn laptop_file() -> Icon {
        Icon::new("")
    }

    pub const fn toilet_paper() -> Icon {
        Icon::new("")
    }

    pub const fn building_shield() -> Icon {
        Icon::new("")
    }

    pub const fn get_pocket() -> Icon {
        Icon::new("")
    }

    pub const fn bacteria() -> Icon {
        Icon::new("")
    }

    pub const fn spa() -> Icon {
        Icon::new("")
    }

    pub const fn divide() -> Icon {
        Icon::new("")
    }

    pub const fn gitlab() -> Icon {
        Icon::new("")
    }

    pub const fn italic() -> Icon {
        Icon::new("")
    }

    pub const fn basketball() -> Icon {
        Icon::new("")
    }

    pub const fn grip_vertical() -> Icon {
        Icon::new("")
    }

    pub const fn steam() -> Icon {
        Icon::new("")
    }

    pub const fn code_commit() -> Icon {
        Icon::new("")
    }

    pub const fn comment_medical() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_right_to_city() -> Icon {
        Icon::new("")
    }

    pub const fn _3() -> Icon {
        Icon::new("3")
    }

    pub const fn calendar_day() -> Icon {
        Icon::new("")
    }

    pub const fn hamsa() -> Icon {
        Icon::new("")
    }

    pub const fn asymmetrik() -> Icon {
        Icon::new("")
    }

    pub const fn calendar() -> Icon {
        Icon::new("")
    }

    pub const fn optin_monster() -> Icon {
        Icon::new("")
    }

    pub const fn window_maximize() -> Icon {
        Icon::new("")
    }

    pub const fn hand_sparkles() -> Icon {
        Icon::new("")
    }

    pub const fn face_frown_open() -> Icon {
        Icon::new("")
    }

    pub const fn torii_gate() -> Icon {
        Icon::new("")
    }

    pub const fn trailer() -> Icon {
        Icon::new("")
    }

    pub const fn vials() -> Icon {
        Icon::new("")
    }

    pub const fn blogger() -> Icon {
        Icon::new("")
    }

    pub const fn page_4() -> Icon {
        Icon::new("")
    }

    pub const fn ruler_combined() -> Icon {
        Icon::new("")
    }

    pub const fn plane_up() -> Icon {
        Icon::new("")
    }

    pub const fn file_arrow_up() -> Icon {
        Icon::new("")
    }

    pub const fn vimeo_v() -> Icon {
        Icon::new("")
    }

    pub const fn person_rifle() -> Icon {
        Icon::new("")
    }

    pub const fn google_drive() -> Icon {
        Icon::new("")
    }

    pub const fn money_bill_1() -> Icon {
        Icon::new("")
    }

    pub const fn shekel_sign() -> Icon {
        Icon::new("")
    }

    pub const fn tent_arrow_turn_left() -> Icon {
        Icon::new("")
    }

    pub const fn react() -> Icon {
        Icon::new("")
    }

    pub const fn face_smile() -> Icon {
        Icon::new("")
    }

    pub const fn rockrms() -> Icon {
        Icon::new("")
    }

    pub const fn mobile_screen_button() -> Icon {
        Icon::new("")
    }

    pub const fn renren() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_from_bracket() -> Icon {
        Icon::new("")
    }

    pub const fn hourglass_half() -> Icon {
        Icon::new("")
    }

    pub const fn building_wheat() -> Icon {
        Icon::new("")
    }

    pub const fn tooth() -> Icon {
        Icon::new("")
    }

    pub const fn user_slash() -> Icon {
        Icon::new("")
    }

    pub const fn person_falling_burst() -> Icon {
        Icon::new("")
    }

    pub const fn images() -> Icon {
        Icon::new("")
    }

    pub const fn osi() -> Icon {
        Icon::new("")
    }

    pub const fn mound() -> Icon {
        Icon::new("")
    }

    pub const fn quora() -> Icon {
        Icon::new("")
    }

    pub const fn weebly() -> Icon {
        Icon::new("")
    }

    pub const fn circle_radiation() -> Icon {
        Icon::new("")
    }

    pub const fn wand_magic() -> Icon {
        Icon::new("")
    }

    pub const fn filter_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn gitkraken() -> Icon {
        Icon::new("")
    }

    pub const fn temperature_empty() -> Icon {
        Icon::new("")
    }

    pub const fn hand_holding_hand() -> Icon {
        Icon::new("")
    }

    pub const fn stumbleupon() -> Icon {
        Icon::new("")
    }

    pub const fn code_branch() -> Icon {
        Icon::new("")
    }

    pub const fn hourglass() -> Icon {
        Icon::new("")
    }

    pub const fn house_medical_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn caret_right() -> Icon {
        Icon::new("")
    }

    pub const fn pen_to_square() -> Icon {
        Icon::new("")
    }

    pub const fn medal() -> Icon {
        Icon::new("")
    }

    pub const fn cc_apple_pay() -> Icon {
        Icon::new("")
    }

    pub const fn hanukiah() -> Icon {
        Icon::new("")
    }

    pub const fn microsoft() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_turn_to_dots() -> Icon {
        Icon::new("")
    }

    pub const fn person_walking_arrow_loop_left() -> Icon {
        Icon::new("")
    }

    pub const fn users() -> Icon {
        Icon::new("")
    }

    pub const fn transgender() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_sa() -> Icon {
        Icon::new("")
    }

    pub const fn hooli() -> Icon {
        Icon::new("")
    }

    pub const fn app_store() -> Icon {
        Icon::new("")
    }

    pub const fn dong_sign() -> Icon {
        Icon::new("")
    }

    pub const fn face_flushed() -> Icon {
        Icon::new("")
    }

    pub const fn hotdog() -> Icon {
        Icon::new("")
    }

    pub const fn martini_glass_empty() -> Icon {
        Icon::new("")
    }

    pub const fn ellipsis_vertical() -> Icon {
        Icon::new("")
    }

    pub const fn beer_mug_empty() -> Icon {
        Icon::new("")
    }

    pub const fn chart_gantt() -> Icon {
        Icon::new("")
    }

    pub const fn object_group() -> Icon {
        Icon::new("")
    }

    pub const fn face_smile_beam() -> Icon {
        Icon::new("")
    }

    pub const fn unlock_keyhole() -> Icon {
        Icon::new("")
    }

    pub const fn viacoin() -> Icon {
        Icon::new("")
    }

    pub const fn sack_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down_up_across_line() -> Icon {
        Icon::new("")
    }

    pub const fn list_check() -> Icon {
        Icon::new("")
    }

    pub const fn house_chimney_window() -> Icon {
        Icon::new("")
    }

    pub const fn pen_ruler() -> Icon {
        Icon::new("")
    }

    pub const fn truck_moving() -> Icon {
        Icon::new("")
    }

    pub const fn paragraph() -> Icon {
        Icon::new("")
    }

    pub const fn houzz() -> Icon {
        Icon::new("")
    }

    pub const fn cable_car() -> Icon {
        Icon::new("")
    }

    pub const fn envelope() -> Icon {
        Icon::new("")
    }

    pub const fn eye() -> Icon {
        Icon::new("")
    }

    pub const fn facebook() -> Icon {
        Icon::new("")
    }

    pub const fn grav() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_by() -> Icon {
        Icon::new("")
    }

    pub const fn sistrix() -> Icon {
        Icon::new("")
    }

    pub const fn itunes_note() -> Icon {
        Icon::new("")
    }

    pub const fn bottle_droplet() -> Icon {
        Icon::new("")
    }

    pub const fn prescription() -> Icon {
        Icon::new("")
    }

    pub const fn font() -> Icon {
        Icon::new("")
    }

    pub const fn map_location() -> Icon {
        Icon::new("")
    }

    pub const fn recycle() -> Icon {
        Icon::new("")
    }

    pub const fn registered() -> Icon {
        Icon::new("")
    }

    pub const fn school_lock() -> Icon {
        Icon::new("")
    }

    pub const fn utensils() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_right_from_square() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_right_from_bracket() -> Icon {
        Icon::new("")
    }

    pub const fn person_walking_arrow_right() -> Icon {
        Icon::new("")
    }

    pub const fn strikethrough() -> Icon {
        Icon::new("")
    }

    pub const fn shower() -> Icon {
        Icon::new("")
    }

    pub const fn square_snapchat() -> Icon {
        Icon::new("")
    }

    pub const fn circle_up() -> Icon {
        Icon::new("")
    }

    pub const fn gear() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down_long() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_nc_eu() -> Icon {
        Icon::new("")
    }

    pub const fn question() -> Icon {
        Icon::new("?")
    }

    pub const fn envelopes_bulk() -> Icon {
        Icon::new("")
    }

    pub const fn d_and_d() -> Icon {
        Icon::new("")
    }

    pub const fn sliders() -> Icon {
        Icon::new("")
    }

    pub const fn the_red_yeti() -> Icon {
        Icon::new("")
    }

    pub const fn stackpath() -> Icon {
        Icon::new("")
    }

    pub const fn file_audio() -> Icon {
        Icon::new("")
    }

    pub const fn road_barrier() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons() -> Icon {
        Icon::new("")
    }

    pub const fn ruler() -> Icon {
        Icon::new("")
    }

    pub const fn sleigh() -> Icon {
        Icon::new("")
    }

    pub const fn readme() -> Icon {
        Icon::new("")
    }

    pub const fn burger() -> Icon {
        Icon::new("")
    }

    pub const fn f() -> Icon {
        Icon::new("F")
    }

    pub const fn dailymotion() -> Icon {
        Icon::new("")
    }

    pub const fn earth_oceania() -> Icon {
        Icon::new("")
    }

    pub const fn skype() -> Icon {
        Icon::new("")
    }

    pub const fn circle_chevron_up() -> Icon {
        Icon::new("")
    }

    pub const fn slideshare() -> Icon {
        Icon::new("")
    }

    pub const fn circle_plus() -> Icon {
        Icon::new("")
    }

    pub const fn telegram() -> Icon {
        Icon::new("")
    }

    pub const fn caravan() -> Icon {
        Icon::new("")
    }

    pub const fn bowl_rice() -> Icon {
        Icon::new("")
    }

    pub const fn eye_slash() -> Icon {
        Icon::new("")
    }

    pub const fn facebook_messenger() -> Icon {
        Icon::new("")
    }

    pub const fn file_import() -> Icon {
        Icon::new("")
    }

    pub const fn mars_and_venus() -> Icon {
        Icon::new("")
    }

    pub const fn weixin() -> Icon {
        Icon::new("")
    }

    pub const fn centos() -> Icon {
        Icon::new("")
    }

    pub const fn check() -> Icon {
        Icon::new("")
    }

    pub const fn diagram_project() -> Icon {
        Icon::new("")
    }

    pub const fn spaghetti_monster_flying() -> Icon {
        Icon::new("")
    }

    pub const fn passport() -> Icon {
        Icon::new("")
    }

    pub const fn sketch() -> Icon {
        Icon::new("")
    }

    pub const fn money_bills() -> Icon {
        Icon::new("")
    }

    pub const fn o() -> Icon {
        Icon::new("O")
    }

    pub const fn tractor() -> Icon {
        Icon::new("")
    }

    pub const fn address_card() -> Icon {
        Icon::new("")
    }

    pub const fn khanda() -> Icon {
        Icon::new("")
    }

    pub const fn uikit() -> Icon {
        Icon::new("")
    }

    pub const fn icons() -> Icon {
        Icon::new("")
    }

    pub const fn angrycreative() -> Icon {
        Icon::new("")
    }

    pub const fn app_store_ios() -> Icon {
        Icon::new("")
    }

    pub const fn dna() -> Icon {
        Icon::new("")
    }

    pub const fn staff_snake() -> Icon {
        Icon::new("")
    }

    pub const fn building_ngo() -> Icon {
        Icon::new("")
    }

    pub const fn business_time() -> Icon {
        Icon::new("")
    }

    pub const fn file_powerpoint() -> Icon {
        Icon::new("")
    }

    pub const fn turkish_lira_sign() -> Icon {
        Icon::new("")
    }

    pub const fn fingerprint() -> Icon {
        Icon::new("")
    }

    pub const fn at() -> Icon {
        Icon::new("@")
    }

    pub const fn paintbrush() -> Icon {
        Icon::new("")
    }

    pub const fn chess_board() -> Icon {
        Icon::new("")
    }

    pub const fn bridge_water() -> Icon {
        Icon::new("")
    }

    pub const fn discord() -> Icon {
        Icon::new("")
    }

    pub const fn helicopter_symbol() -> Icon {
        Icon::new("")
    }

    pub const fn bezier_curve() -> Icon {
        Icon::new("")
    }

    pub const fn battery_three_quarters() -> Icon {
        Icon::new("")
    }

    pub const fn blackberry() -> Icon {
        Icon::new("")
    }

    pub const fn music() -> Icon {
        Icon::new("")
    }

    pub const fn location_pin() -> Icon {
        Icon::new("")
    }

    pub const fn file_shield() -> Icon {
        Icon::new("")
    }

    pub const fn volume_low() -> Icon {
        Icon::new("")
    }

    pub const fn bold() -> Icon {
        Icon::new("")
    }

    pub const fn p() -> Icon {
        Icon::new("P")
    }

    pub const fn clover() -> Icon {
        Icon::new("")
    }

    pub const fn compact_disc() -> Icon {
        Icon::new("")
    }

    pub const fn cheese() -> Icon {
        Icon::new("")
    }

    pub const fn star_half() -> Icon {
        Icon::new("")
    }

    pub const fn jet_fighter_up() -> Icon {
        Icon::new("")
    }

    pub const fn jug_detergent() -> Icon {
        Icon::new("")
    }

    pub const fn square_phone() -> Icon {
        Icon::new("")
    }

    pub const fn u() -> Icon {
        Icon::new("U")
    }

    pub const fn hand_holding() -> Icon {
        Icon::new("")
    }

    pub const fn money_bill_trend_up() -> Icon {
        Icon::new("")
    }

    pub const fn handcuffs() -> Icon {
        Icon::new("")
    }

    pub const fn church() -> Icon {
        Icon::new("")
    }

    pub const fn pause() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_right_to_bracket() -> Icon {
        Icon::new("")
    }

    pub const fn code_fork() -> Icon {
        Icon::new("")
    }

    pub const fn pump_medical() -> Icon {
        Icon::new("")
    }

    pub const fn css_3() -> Icon {
        Icon::new("")
    }

    pub const fn right_long() -> Icon {
        Icon::new("")
    }

    pub const fn ticket_simple() -> Icon {
        Icon::new("")
    }

    pub const fn dice_d_6() -> Icon {
        Icon::new("")
    }

    pub const fn speakap() -> Icon {
        Icon::new("")
    }

    pub const fn facebook_f() -> Icon {
        Icon::new("")
    }

    pub const fn circle_h() -> Icon {
        Icon::new("")
    }

    pub const fn circle_down() -> Icon {
        Icon::new("")
    }

    pub const fn bed_pulse() -> Icon {
        Icon::new("")
    }

    pub const fn monero() -> Icon {
        Icon::new("")
    }

    pub const fn tv() -> Icon {
        Icon::new("")
    }

    pub const fn person_skating() -> Icon {
        Icon::new("")
    }

    pub const fn location_pin_lock() -> Icon {
        Icon::new("")
    }

    pub const fn keyboard() -> Icon {
        Icon::new("")
    }

    pub const fn ellipsis() -> Icon {
        Icon::new("")
    }

    pub const fn reddit() -> Icon {
        Icon::new("")
    }

    pub const fn dashcube() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down_1_9() -> Icon {
        Icon::new("")
    }

    pub const fn bug() -> Icon {
        Icon::new("")
    }

    pub const fn person_walking_luggage() -> Icon {
        Icon::new("")
    }

    pub const fn comment_dollar() -> Icon {
        Icon::new("")
    }

    pub const fn circle_notch() -> Icon {
        Icon::new("")
    }

    pub const fn hand_point_down() -> Icon {
        Icon::new("")
    }

    pub const fn jet_fighter() -> Icon {
        Icon::new("")
    }

    pub const fn file_contract() -> Icon {
        Icon::new("")
    }

    pub const fn car_battery() -> Icon {
        Icon::new("")
    }

    pub const fn person_hiking() -> Icon {
        Icon::new("")
    }

    pub const fn face_meh() -> Icon {
        Icon::new("")
    }

    pub const fn worm() -> Icon {
        Icon::new("")
    }

    pub const fn battery_half() -> Icon {
        Icon::new("")
    }

    pub const fn box_tissue() -> Icon {
        Icon::new("")
    }

    pub const fn hand_holding_heart() -> Icon {
        Icon::new("")
    }

    pub const fn watchman_monitoring() -> Icon {
        Icon::new("")
    }

    pub const fn trash_can() -> Icon {
        Icon::new("")
    }

    pub const fn house_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn mobile_screen() -> Icon {
        Icon::new("")
    }

    pub const fn paypal() -> Icon {
        Icon::new("")
    }

    pub const fn house_medical_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn chess() -> Icon {
        Icon::new("")
    }

    pub const fn truck_pickup() -> Icon {
        Icon::new("")
    }

    pub const fn intercom() -> Icon {
        Icon::new("")
    }

    pub const fn wine_glass() -> Icon {
        Icon::new("")
    }

    pub const fn shield_dog() -> Icon {
        Icon::new("")
    }

    pub const fn vial() -> Icon {
        Icon::new("")
    }

    pub const fn campground() -> Icon {
        Icon::new("")
    }

    pub const fn motorcycle() -> Icon {
        Icon::new("")
    }

    pub const fn comment_sms() -> Icon {
        Icon::new("")
    }

    pub const fn kaggle() -> Icon {
        Icon::new("")
    }

    pub const fn truck_ramp_box() -> Icon {
        Icon::new("")
    }

    pub const fn file_signature() -> Icon {
        Icon::new("")
    }

    pub const fn expand() -> Icon {
        Icon::new("")
    }

    pub const fn store() -> Icon {
        Icon::new("")
    }

    pub const fn hands_holding_child() -> Icon {
        Icon::new("")
    }

    pub const fn florin_sign() -> Icon {
        Icon::new("")
    }

    pub const fn circle_info() -> Icon {
        Icon::new("")
    }

    pub const fn people_arrows() -> Icon {
        Icon::new("")
    }

    pub const fn face_laugh_wink() -> Icon {
        Icon::new("")
    }

    pub const fn ioxhost() -> Icon {
        Icon::new("")
    }

    pub const fn house_crack() -> Icon {
        Icon::new("")
    }

    pub const fn cat() -> Icon {
        Icon::new("")
    }

    pub const fn digg() -> Icon {
        Icon::new("")
    }

    pub const fn person_falling() -> Icon {
        Icon::new("")
    }

    pub const fn sourcetree() -> Icon {
        Icon::new("")
    }

    pub const fn folder_minus() -> Icon {
        Icon::new("")
    }

    pub const fn hips() -> Icon {
        Icon::new("")
    }

    pub const fn explosion() -> Icon {
        Icon::new("")
    }

    pub const fn exclamation() -> Icon {
        Icon::new("!")
    }

    pub const fn truck_front() -> Icon {
        Icon::new("")
    }

    pub const fn bowl_food() -> Icon {
        Icon::new("")
    }

    pub const fn snowplow() -> Icon {
        Icon::new("")
    }

    pub const fn soap() -> Icon {
        Icon::new("")
    }

    pub const fn venus_mars() -> Icon {
        Icon::new("")
    }

    pub const fn text_slash() -> Icon {
        Icon::new("")
    }

    pub const fn caret_up() -> Icon {
        Icon::new("")
    }

    pub const fn tarp() -> Icon {
        Icon::new("")
    }

    pub const fn mars() -> Icon {
        Icon::new("")
    }

    pub const fn braille() -> Icon {
        Icon::new("")
    }

    pub const fn drum_steelpan() -> Icon {
        Icon::new("")
    }

    pub const fn chess_bishop() -> Icon {
        Icon::new("")
    }

    pub const fn fedora() -> Icon {
        Icon::new("")
    }

    pub const fn file_image() -> Icon {
        Icon::new("")
    }

    pub const fn horse_head() -> Icon {
        Icon::new("")
    }

    pub const fn shield_cat() -> Icon {
        Icon::new("")
    }

    pub const fn bluetooth_b() -> Icon {
        Icon::new("")
    }

    pub const fn backward() -> Icon {
        Icon::new("")
    }

    pub const fn file_waveform() -> Icon {
        Icon::new("")
    }

    pub const fn tower_cell() -> Icon {
        Icon::new("")
    }

    pub const fn box_archive() -> Icon {
        Icon::new("")
    }

    pub const fn codepen() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_right_long() -> Icon {
        Icon::new("")
    }

    pub const fn poop() -> Icon {
        Icon::new("")
    }

    pub const fn face_kiss() -> Icon {
        Icon::new("")
    }

    pub const fn ghost() -> Icon {
        Icon::new("")
    }

    pub const fn solar_panel() -> Icon {
        Icon::new("")
    }

    pub const fn github_alt() -> Icon {
        Icon::new("")
    }

    pub const fn temperature_three_quarters() -> Icon {
        Icon::new("")
    }

    pub const fn piggy_bank() -> Icon {
        Icon::new("")
    }

    pub const fn user_group() -> Icon {
        Icon::new("")
    }

    pub const fn x() -> Icon {
        Icon::new("X")
    }

    pub const fn buromobelexperte() -> Icon {
        Icon::new("")
    }

    pub const fn mountain() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_trend_down() -> Icon {
        Icon::new("")
    }

    pub const fn viruses() -> Icon {
        Icon::new("")
    }

    pub const fn digital_ocean() -> Icon {
        Icon::new("")
    }

    pub const fn plug_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn autoprefixer() -> Icon {
        Icon::new("")
    }

    pub const fn street_view() -> Icon {
        Icon::new("")
    }

    pub const fn jar_wheat() -> Icon {
        Icon::new("")
    }

    pub const fn tablets() -> Icon {
        Icon::new("")
    }

    pub const fn file_export() -> Icon {
        Icon::new("")
    }

    pub const fn ice_cream() -> Icon {
        Icon::new("")
    }

    pub const fn freebsd() -> Icon {
        Icon::new("")
    }

    pub const fn image() -> Icon {
        Icon::new("")
    }

    pub const fn hat_cowboy() -> Icon {
        Icon::new("")
    }

    pub const fn cookie() -> Icon {
        Icon::new("")
    }

    pub const fn square_person_confined() -> Icon {
        Icon::new("")
    }

    pub const fn snowflake() -> Icon {
        Icon::new("")
    }

    pub const fn house_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn qq() -> Icon {
        Icon::new("")
    }

    pub const fn text_width() -> Icon {
        Icon::new("")
    }

    pub const fn cloudversify() -> Icon {
        Icon::new("")
    }

    pub const fn cloud() -> Icon {
        Icon::new("")
    }

    pub const fn face_meh_blank() -> Icon {
        Icon::new("")
    }

    pub const fn vihara() -> Icon {
        Icon::new("")
    }

    pub const fn hammer() -> Icon {
        Icon::new("")
    }

    pub const fn _box() -> Icon {
        Icon::new("")
    }

    pub const fn kaaba() -> Icon {
        Icon::new("")
    }

    pub const fn docker() -> Icon {
        Icon::new("")
    }

    pub const fn train_subway() -> Icon {
        Icon::new("")
    }

    pub const fn cookie_bite() -> Icon {
        Icon::new("")
    }

    pub const fn file_circle_minus() -> Icon {
        Icon::new("")
    }

    pub const fn kiwi_bird() -> Icon {
        Icon::new("")
    }

    pub const fn whatsapp() -> Icon {
        Icon::new("")
    }

    pub const fn cc_discover() -> Icon {
        Icon::new("")
    }

    pub const fn suitcase_rolling() -> Icon {
        Icon::new("")
    }

    pub const fn bullhorn() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_turn_right() -> Icon {
        Icon::new("")
    }

    pub const fn suse() -> Icon {
        Icon::new("")
    }

    pub const fn hourglass_end() -> Icon {
        Icon::new("")
    }

    pub const fn window_minimize() -> Icon {
        Icon::new("")
    }

    pub const fn tornado() -> Icon {
        Icon::new("")
    }

    pub const fn person_pregnant() -> Icon {
        Icon::new("")
    }

    pub const fn pagelines() -> Icon {
        Icon::new("")
    }

    pub const fn google_plus() -> Icon {
        Icon::new("")
    }

    pub const fn sterling_sign() -> Icon {
        Icon::new("")
    }

    pub const fn weight_scale() -> Icon {
        Icon::new("")
    }

    pub const fn t() -> Icon {
        Icon::new("T")
    }

    pub const fn bed() -> Icon {
        Icon::new("")
    }

    pub const fn person_swimming() -> Icon {
        Icon::new("")
    }

    pub const fn compass() -> Icon {
        Icon::new("")
    }

    pub const fn folder_open() -> Icon {
        Icon::new("")
    }

    pub const fn php() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_bolt() -> Icon {
        Icon::new("")
    }

    pub const fn feather_pointed() -> Icon {
        Icon::new("")
    }

    pub const fn wand_magic_sparkles() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_tongue() -> Icon {
        Icon::new("")
    }

    pub const fn centercode() -> Icon {
        Icon::new("")
    }

    pub const fn magnifying_glass() -> Icon {
        Icon::new("")
    }

    pub const fn plug_circle_minus() -> Icon {
        Icon::new("")
    }

    pub const fn rust() -> Icon {
        Icon::new("")
    }

    pub const fn gg() -> Icon {
        Icon::new("")
    }

    pub const fn odnoklassniki() -> Icon {
        Icon::new("")
    }

    pub const fn comment_dots() -> Icon {
        Icon::new("")
    }

    pub const fn square_git() -> Icon {
        Icon::new("")
    }

    pub const fn suitcase_medical() -> Icon {
        Icon::new("")
    }

    pub const fn i_cursor() -> Icon {
        Icon::new("")
    }

    pub const fn bookmark() -> Icon {
        Icon::new("")
    }

    pub const fn building_user() -> Icon {
        Icon::new("")
    }

    pub const fn star_of_life() -> Icon {
        Icon::new("")
    }

    pub const fn circle_arrow_left() -> Icon {
        Icon::new("")
    }

    pub const fn water() -> Icon {
        Icon::new("")
    }

    pub const fn trowel_bricks() -> Icon {
        Icon::new("")
    }

    pub const fn think_peaks() -> Icon {
        Icon::new("")
    }

    pub const fn poo_storm() -> Icon {
        Icon::new("")
    }

    pub const fn ember() -> Icon {
        Icon::new("")
    }

    pub const fn cloud_rain() -> Icon {
        Icon::new("")
    }

    pub const fn square_viadeo() -> Icon {
        Icon::new("")
    }

    pub const fn joomla() -> Icon {
        Icon::new("")
    }

    pub const fn font_awesome() -> Icon {
        Icon::new("")
    }

    pub const fn house_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn vk() -> Icon {
        Icon::new("")
    }

    pub const fn bridge_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn clipboard_check() -> Icon {
        Icon::new("")
    }

    pub const fn house_laptop() -> Icon {
        Icon::new("")
    }

    pub const fn firstdraft() -> Icon {
        Icon::new("")
    }

    pub const fn handshake_slash() -> Icon {
        Icon::new("")
    }

    pub const fn oil_well() -> Icon {
        Icon::new("")
    }

    pub const fn virus() -> Icon {
        Icon::new("")
    }

    pub const fn bootstrap() -> Icon {
        Icon::new("")
    }

    pub const fn volume_high() -> Icon {
        Icon::new("")
    }

    pub const fn b() -> Icon {
        Icon::new("B")
    }

    pub const fn algolia() -> Icon {
        Icon::new("")
    }

    pub const fn capsules() -> Icon {
        Icon::new("")
    }

    pub const fn hands_praying() -> Icon {
        Icon::new("")
    }

    pub const fn otter() -> Icon {
        Icon::new("")
    }

    pub const fn right_to_bracket() -> Icon {
        Icon::new("")
    }

    pub const fn kitchen_set() -> Icon {
        Icon::new("")
    }

    pub const fn rainbow() -> Icon {
        Icon::new("")
    }

    pub const fn apple_whole() -> Icon {
        Icon::new("")
    }

    pub const fn boxes_packing() -> Icon {
        Icon::new("")
    }

    pub const fn table_tennis_paddle_ball() -> Icon {
        Icon::new("")
    }

    pub const fn _4() -> Icon {
        Icon::new("4")
    }

    pub const fn yandex_international() -> Icon {
        Icon::new("")
    }

    pub const fn book_quran() -> Icon {
        Icon::new("")
    }

    pub const fn shoe_prints() -> Icon {
        Icon::new("")
    }

    pub const fn voicemail() -> Icon {
        Icon::new("")
    }

    pub const fn house_tsunami() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_down_short_wide() -> Icon {
        Icon::new("")
    }

    pub const fn republican() -> Icon {
        Icon::new("")
    }

    pub const fn toggle_off() -> Icon {
        Icon::new("")
    }

    pub const fn users_rectangle() -> Icon {
        Icon::new("")
    }

    pub const fn volume_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn building_un() -> Icon {
        Icon::new("")
    }

    pub const fn edge() -> Icon {
        Icon::new("")
    }

    pub const fn atom() -> Icon {
        Icon::new("")
    }

    pub const fn user_graduate() -> Icon {
        Icon::new("")
    }

    pub const fn dumpster_fire() -> Icon {
        Icon::new("")
    }

    pub const fn _1() -> Icon {
        Icon::new("1")
    }

    pub const fn location_dot() -> Icon {
        Icon::new("")
    }

    pub const fn rotate() -> Icon {
        Icon::new("")
    }

    pub const fn credit_card() -> Icon {
        Icon::new("")
    }

    pub const fn umbrella() -> Icon {
        Icon::new("")
    }

    pub const fn house_chimney() -> Icon {
        Icon::new("")
    }

    pub const fn fonticons_fi() -> Icon {
        Icon::new("")
    }

    pub const fn eye_low_vision() -> Icon {
        Icon::new("")
    }

    pub const fn mask_ventilator() -> Icon {
        Icon::new("")
    }

    pub const fn ban_smoking() -> Icon {
        Icon::new("")
    }

    pub const fn dice_six() -> Icon {
        Icon::new("")
    }

    pub const fn blender_phone() -> Icon {
        Icon::new("")
    }

    pub const fn rss() -> Icon {
        Icon::new("")
    }

    pub const fn plane_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn hand_peace() -> Icon {
        Icon::new("")
    }

    pub const fn user() -> Icon {
        Icon::new("")
    }

    pub const fn square_virus() -> Icon {
        Icon::new("")
    }

    pub const fn square_share_nodes() -> Icon {
        Icon::new("")
    }

    pub const fn korvue() -> Icon {
        Icon::new("")
    }

    pub const fn puzzle_piece() -> Icon {
        Icon::new("")
    }

    pub const fn trademark() -> Icon {
        Icon::new("")
    }

    pub const fn tablet() -> Icon {
        Icon::new("")
    }

    pub const fn building_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn headphones() -> Icon {
        Icon::new("")
    }

    pub const fn superscript() -> Icon {
        Icon::new("")
    }

    pub const fn paste() -> Icon {
        Icon::new("")
    }

    pub const fn megaport() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_up_down() -> Icon {
        Icon::new("")
    }

    pub const fn tree() -> Icon {
        Icon::new("")
    }

    pub const fn life_ring() -> Icon {
        Icon::new("")
    }

    pub const fn hryvnia_sign() -> Icon {
        Icon::new("")
    }

    pub const fn file_code() -> Icon {
        Icon::new("")
    }

    pub const fn fill() -> Icon {
        Icon::new("")
    }

    pub const fn sheet_plastic() -> Icon {
        Icon::new("")
    }

    pub const fn clipboard_list() -> Icon {
        Icon::new("")
    }

    pub const fn clone() -> Icon {
        Icon::new("")
    }

    pub const fn lines_leaning() -> Icon {
        Icon::new("")
    }

    pub const fn vine() -> Icon {
        Icon::new("")
    }

    pub const fn tent_arrow_down_to_line() -> Icon {
        Icon::new("")
    }

    pub const fn table_cells() -> Icon {
        Icon::new("")
    }

    pub const fn road_lock() -> Icon {
        Icon::new("")
    }

    pub const fn wolf_pack_battalion() -> Icon {
        Icon::new("")
    }

    pub const fn flickr() -> Icon {
        Icon::new("")
    }

    pub const fn left_long() -> Icon {
        Icon::new("")
    }

    pub const fn fort_awesome_alt() -> Icon {
        Icon::new("")
    }

    pub const fn angle_up() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_from_ground_water() -> Icon {
        Icon::new("")
    }

    pub const fn gifts() -> Icon {
        Icon::new("")
    }

    pub const fn parachute_box() -> Icon {
        Icon::new("")
    }

    pub const fn strava() -> Icon {
        Icon::new("")
    }

    pub const fn door_open() -> Icon {
        Icon::new("")
    }

    pub const fn firefox() -> Icon {
        Icon::new("")
    }

    pub const fn house_chimney_user() -> Icon {
        Icon::new("")
    }

    pub const fn superpowers() -> Icon {
        Icon::new("")
    }

    pub const fn gamepad() -> Icon {
        Icon::new("")
    }

    pub const fn indent() -> Icon {
        Icon::new("")
    }

    pub const fn themeco() -> Icon {
        Icon::new("")
    }

    pub const fn yen_sign() -> Icon {
        Icon::new("")
    }

    pub const fn chevron_down() -> Icon {
        Icon::new("")
    }

    pub const fn window_restore() -> Icon {
        Icon::new("")
    }

    pub const fn chevron_left() -> Icon {
        Icon::new("")
    }

    pub const fn gauge_simple() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_rotate() -> Icon {
        Icon::new("")
    }

    pub const fn lari_sign() -> Icon {
        Icon::new("")
    }

    pub const fn codiepie() -> Icon {
        Icon::new("")
    }

    pub const fn cc_mastercard() -> Icon {
        Icon::new("")
    }

    pub const fn diaspora() -> Icon {
        Icon::new("")
    }

    pub const fn goodreads() -> Icon {
        Icon::new("")
    }

    pub const fn prescription_bottle_medical() -> Icon {
        Icon::new("")
    }

    pub const fn users_viewfinder() -> Icon {
        Icon::new("")
    }

    pub const fn wind() -> Icon {
        Icon::new("")
    }

    pub const fn upload() -> Icon {
        Icon::new("")
    }

    pub const fn align_justify() -> Icon {
        Icon::new("")
    }

    pub const fn ello() -> Icon {
        Icon::new("")
    }

    pub const fn joint() -> Icon {
        Icon::new("")
    }

    pub const fn sellcast() -> Icon {
        Icon::new("")
    }

    pub const fn periscope() -> Icon {
        Icon::new("")
    }

    pub const fn cc_diners_club() -> Icon {
        Icon::new("")
    }

    pub const fn truck() -> Icon {
        Icon::new("")
    }

    pub const fn greater_than() -> Icon {
        Icon::new(">")
    }

    pub const fn rotate_right() -> Icon {
        Icon::new("")
    }

    pub const fn mattress_pillow() -> Icon {
        Icon::new("")
    }

    pub const fn school_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn ear_listen() -> Icon {
        Icon::new("")
    }

    pub const fn weibo() -> Icon {
        Icon::new("")
    }

    pub const fn igloo() -> Icon {
        Icon::new("")
    }

    pub const fn dice_two() -> Icon {
        Icon::new("")
    }

    pub const fn apper() -> Icon {
        Icon::new("")
    }

    pub const fn hand_holding_dollar() -> Icon {
        Icon::new("")
    }

    pub const fn drumstick_bite() -> Icon {
        Icon::new("")
    }

    pub const fn stopwatch() -> Icon {
        Icon::new("")
    }

    pub const fn hot_tub_person() -> Icon {
        Icon::new("")
    }

    pub const fn searchengin() -> Icon {
        Icon::new("")
    }

    pub const fn i() -> Icon {
        Icon::new("I")
    }

    pub const fn arrow_right_arrow_left() -> Icon {
        Icon::new("")
    }

    pub const fn hospital_user() -> Icon {
        Icon::new("")
    }

    pub const fn asterisk() -> Icon {
        Icon::new("*")
    }

    pub const fn earth_africa() -> Icon {
        Icon::new("")
    }

    pub const fn cubes_stacked() -> Icon {
        Icon::new("")
    }

    pub const fn bell_concierge() -> Icon {
        Icon::new("")
    }

    pub const fn bots() -> Icon {
        Icon::new("")
    }

    pub const fn mars_stroke() -> Icon {
        Icon::new("")
    }

    pub const fn person_digging() -> Icon {
        Icon::new("")
    }

    pub const fn database() -> Icon {
        Icon::new("")
    }

    pub const fn square_nfi() -> Icon {
        Icon::new("")
    }

    pub const fn hill_avalanche() -> Icon {
        Icon::new("")
    }

    pub const fn baseball() -> Icon {
        Icon::new("")
    }

    pub const fn panorama() -> Icon {
        Icon::new("")
    }

    pub const fn gauge_high() -> Icon {
        Icon::new("")
    }

    pub const fn ear_deaf() -> Icon {
        Icon::new("")
    }

    pub const fn usb() -> Icon {
        Icon::new("")
    }

    pub const fn cruzeiro_sign() -> Icon {
        Icon::new("")
    }

    pub const fn comment_slash() -> Icon {
        Icon::new("")
    }

    pub const fn cart_plus() -> Icon {
        Icon::new("")
    }

    pub const fn first_order() -> Icon {
        Icon::new("")
    }

    pub const fn video_slash() -> Icon {
        Icon::new("")
    }

    pub const fn thumbtack() -> Icon {
        Icon::new("")
    }

    pub const fn android() -> Icon {
        Icon::new("")
    }

    pub const fn wpressr() -> Icon {
        Icon::new("")
    }

    pub const fn expeditedssl() -> Icon {
        Icon::new("")
    }

    pub const fn git() -> Icon {
        Icon::new("")
    }

    pub const fn angle_left() -> Icon {
        Icon::new("")
    }

    pub const fn thumbs_down() -> Icon {
        Icon::new("")
    }

    pub const fn fire_flame_curved() -> Icon {
        Icon::new("")
    }

    pub const fn paw() -> Icon {
        Icon::new("")
    }

    pub const fn unsplash() -> Icon {
        Icon::new("")
    }

    pub const fn dumbbell() -> Icon {
        Icon::new("")
    }

    pub const fn building_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn gem() -> Icon {
        Icon::new("")
    }

    pub const fn html_5() -> Icon {
        Icon::new("")
    }

    pub const fn repeat() -> Icon {
        Icon::new("")
    }

    pub const fn canadian_maple_leaf() -> Icon {
        Icon::new("")
    }

    pub const fn forward_step() -> Icon {
        Icon::new("")
    }

    pub const fn scissors() -> Icon {
        Icon::new("")
    }

    pub const fn building_flag() -> Icon {
        Icon::new("")
    }

    pub const fn person_biking() -> Icon {
        Icon::new("")
    }

    pub const fn square_hacker_news() -> Icon {
        Icon::new("")
    }

    pub const fn rev() -> Icon {
        Icon::new("")
    }

    pub const fn temperature_low() -> Icon {
        Icon::new("")
    }

    pub const fn circle_minus() -> Icon {
        Icon::new("")
    }

    pub const fn airbnb() -> Icon {
        Icon::new("")
    }

    pub const fn server() -> Icon {
        Icon::new("")
    }

    pub const fn book_bookmark() -> Icon {
        Icon::new("")
    }

    pub const fn group_arrows_rotate() -> Icon {
        Icon::new("")
    }

    pub const fn trash_arrow_up() -> Icon {
        Icon::new("")
    }

    pub const fn glide_g() -> Icon {
        Icon::new("")
    }

    pub const fn person_burst() -> Icon {
        Icon::new("")
    }

    pub const fn vimeo() -> Icon {
        Icon::new("")
    }

    pub const fn plug_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn slack() -> Icon {
        Icon::new("")
    }

    pub const fn house_medical_circle_check() -> Icon {
        Icon::new("")
    }

    pub const fn less() -> Icon {
        Icon::new("")
    }

    pub const fn earth_americas() -> Icon {
        Icon::new("")
    }

    pub const fn person_dress() -> Icon {
        Icon::new("")
    }

    pub const fn toilet_portable() -> Icon {
        Icon::new("")
    }

    pub const fn person_military_rifle() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_turn_down() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_a_z() -> Icon {
        Icon::new("")
    }

    pub const fn face_tired() -> Icon {
        Icon::new("")
    }

    pub const fn cedi_sign() -> Icon {
        Icon::new("")
    }

    pub const fn file_circle_plus() -> Icon {
        Icon::new("")
    }

    pub const fn rocketchat() -> Icon {
        Icon::new("")
    }

    pub const fn file_pen() -> Icon {
        Icon::new("")
    }

    pub const fn house_chimney_medical() -> Icon {
        Icon::new("")
    }

    pub const fn person_cane() -> Icon {
        Icon::new("")
    }

    pub const fn section() -> Icon {
        Icon::new("")
    }

    pub const fn evernote() -> Icon {
        Icon::new("")
    }

    pub const fn futbol() -> Icon {
        Icon::new("")
    }

    pub const fn blender() -> Icon {
        Icon::new("")
    }

    pub const fn file_csv() -> Icon {
        Icon::new("")
    }

    pub const fn gopuram() -> Icon {
        Icon::new("")
    }

    pub const fn helmet_un() -> Icon {
        Icon::new("")
    }

    pub const fn microphone() -> Icon {
        Icon::new("")
    }

    pub const fn eraser() -> Icon {
        Icon::new("")
    }

    pub const fn person_drowning() -> Icon {
        Icon::new("")
    }

    pub const fn phone_volume() -> Icon {
        Icon::new("")
    }

    pub const fn chromecast() -> Icon {
        Icon::new("")
    }

    pub const fn maxcdn() -> Icon {
        Icon::new("")
    }

    pub const fn square_gitlab() -> Icon {
        Icon::new("")
    }

    pub const fn stripe_s() -> Icon {
        Icon::new("")
    }

    pub const fn user_gear() -> Icon {
        Icon::new("")
    }

    pub const fn triangle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn phone_flip() -> Icon {
        Icon::new("")
    }

    pub const fn lock() -> Icon {
        Icon::new("")
    }

    pub const fn teamspeak() -> Icon {
        Icon::new("")
    }

    pub const fn folder_closed() -> Icon {
        Icon::new("")
    }

    pub const fn gripfire() -> Icon {
        Icon::new("")
    }

    pub const fn computer() -> Icon {
        Icon::new("")
    }

    pub const fn train_tram() -> Icon {
        Icon::new("")
    }

    pub const fn stack_exchange() -> Icon {
        Icon::new("")
    }

    pub const fn cloudflare() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_up_down_left_right() -> Icon {
        Icon::new("")
    }

    pub const fn _2() -> Icon {
        Icon::new("2")
    }

    pub const fn envelope_open() -> Icon {
        Icon::new("")
    }

    pub const fn pushed() -> Icon {
        Icon::new("")
    }

    pub const fn k() -> Icon {
        Icon::new("K")
    }

    pub const fn lemon() -> Icon {
        Icon::new("")
    }

    pub const fn manat_sign() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_up_wide_short() -> Icon {
        Icon::new("")
    }

    pub const fn square_vimeo() -> Icon {
        Icon::new("")
    }

    pub const fn dropbox() -> Icon {
        Icon::new("")
    }

    pub const fn y_combinator() -> Icon {
        Icon::new("")
    }

    pub const fn hashnode() -> Icon {
        Icon::new("")
    }

    pub const fn figma() -> Icon {
        Icon::new("")
    }

    pub const fn mask() -> Icon {
        Icon::new("")
    }

    pub const fn comments() -> Icon {
        Icon::new("")
    }

    pub const fn tarp_droplet() -> Icon {
        Icon::new("")
    }

    pub const fn square_arrow_up_right() -> Icon {
        Icon::new("")
    }

    pub const fn plane_lock() -> Icon {
        Icon::new("")
    }

    pub const fn file_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn yammer() -> Icon {
        Icon::new("")
    }

    pub const fn battery_full() -> Icon {
        Icon::new("")
    }

    pub const fn cow() -> Icon {
        Icon::new("")
    }

    pub const fn plug_circle_bolt() -> Icon {
        Icon::new("")
    }

    pub const fn mars_double() -> Icon {
        Icon::new("")
    }

    pub const fn face_sad_cry() -> Icon {
        Icon::new("")
    }

    pub const fn filter() -> Icon {
        Icon::new("")
    }

    pub const fn helicopter() -> Icon {
        Icon::new("")
    }

    pub const fn pied_piper_pp() -> Icon {
        Icon::new("")
    }

    pub const fn sailboat() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_down_to_line() -> Icon {
        Icon::new("")
    }

    pub const fn mdb() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_remix() -> Icon {
        Icon::new("")
    }

    pub const fn id_card() -> Icon {
        Icon::new("")
    }

    pub const fn person_harassing() -> Icon {
        Icon::new("")
    }

    pub const fn rebel() -> Icon {
        Icon::new("")
    }

    pub const fn mosquito_net() -> Icon {
        Icon::new("")
    }

    pub const fn trello() -> Icon {
        Icon::new("")
    }

    pub const fn plus_minus() -> Icon {
        Icon::new("")
    }

    pub const fn j() -> Icon {
        Icon::new("J")
    }

    pub const fn colon_sign() -> Icon {
        Icon::new("")
    }

    pub const fn circle() -> Icon {
        Icon::new("")
    }

    pub const fn fire_extinguisher() -> Icon {
        Icon::new("")
    }

    pub const fn virus_covid_slash() -> Icon {
        Icon::new("")
    }

    pub const fn charging_station() -> Icon {
        Icon::new("")
    }

    pub const fn satellite_dish() -> Icon {
        Icon::new("")
    }

    pub const fn volleyball() -> Icon {
        Icon::new("")
    }

    pub const fn skull() -> Icon {
        Icon::new("")
    }

    pub const fn e() -> Icon {
        Icon::new("E")
    }

    pub const fn toilets_portable() -> Icon {
        Icon::new("")
    }

    pub const fn red_river() -> Icon {
        Icon::new("")
    }

    pub const fn radiation() -> Icon {
        Icon::new("")
    }

    pub const fn lyft() -> Icon {
        Icon::new("")
    }

    pub const fn battery_empty() -> Icon {
        Icon::new("")
    }

    pub const fn square_caret_up() -> Icon {
        Icon::new("")
    }

    pub const fn github() -> Icon {
        Icon::new("")
    }

    pub const fn diamond() -> Icon {
        Icon::new("")
    }

    pub const fn grunt() -> Icon {
        Icon::new("")
    }

    pub const fn whmcs() -> Icon {
        Icon::new("")
    }

    pub const fn border_all() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_left_right() -> Icon {
        Icon::new("")
    }

    pub const fn apple() -> Icon {
        Icon::new("")
    }

    pub const fn file() -> Icon {
        Icon::new("")
    }

    pub const fn smoking() -> Icon {
        Icon::new("")
    }

    pub const fn stopwatch_20() -> Icon {
        Icon::new("")
    }

    pub const fn trash() -> Icon {
        Icon::new("")
    }

    pub const fn award() -> Icon {
        Icon::new("")
    }

    pub const fn mix() -> Icon {
        Icon::new("")
    }

    pub const fn stethoscope() -> Icon {
        Icon::new("")
    }

    pub const fn people_robbery() -> Icon {
        Icon::new("")
    }

    pub const fn spell_check() -> Icon {
        Icon::new("")
    }

    pub const fn steam_symbol() -> Icon {
        Icon::new("")
    }

    pub const fn book_tanakh() -> Icon {
        Icon::new("")
    }

    pub const fn linkedin_in() -> Icon {
        Icon::new("")
    }

    pub const fn people_pulling() -> Icon {
        Icon::new("")
    }

    pub const fn python() -> Icon {
        Icon::new("")
    }

    pub const fn quote_left() -> Icon {
        Icon::new("")
    }

    pub const fn alipay() -> Icon {
        Icon::new("")
    }

    pub const fn tower_broadcast() -> Icon {
        Icon::new("")
    }

    pub const fn folder_tree() -> Icon {
        Icon::new("")
    }

    pub const fn highlighter() -> Icon {
        Icon::new("")
    }

    pub const fn truck_droplet() -> Icon {
        Icon::new("")
    }

    pub const fn vuejs() -> Icon {
        Icon::new("")
    }

    pub const fn buffer() -> Icon {
        Icon::new("")
    }

    pub const fn kickstarter() -> Icon {
        Icon::new("")
    }

    pub const fn ravelry() -> Icon {
        Icon::new("")
    }

    pub const fn star_and_crescent() -> Icon {
        Icon::new("")
    }

    pub const fn dyalog() -> Icon {
        Icon::new("")
    }

    pub const fn toggle_on() -> Icon {
        Icon::new("")
    }

    pub const fn horse() -> Icon {
        Icon::new("")
    }

    pub const fn cent_sign() -> Icon {
        Icon::new("")
    }

    pub const fn plane_arrival() -> Icon {
        Icon::new("")
    }

    pub const fn car_rear() -> Icon {
        Icon::new("")
    }

    pub const fn pager() -> Icon {
        Icon::new("")
    }

    pub const fn kip_sign() -> Icon {
        Icon::new("")
    }

    pub const fn tower_observation() -> Icon {
        Icon::new("")
    }

    pub const fn caret_down() -> Icon {
        Icon::new("")
    }

    pub const fn message() -> Icon {
        Icon::new("")
    }

    pub const fn anchor_circle_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn gulp() -> Icon {
        Icon::new("")
    }

    pub const fn truck_monster() -> Icon {
        Icon::new("")
    }

    pub const fn arrows_up_to_line() -> Icon {
        Icon::new("")
    }

    pub const fn face_angry() -> Icon {
        Icon::new("")
    }

    pub const fn square_xmark() -> Icon {
        Icon::new("")
    }

    pub const fn youtube() -> Icon {
        Icon::new("")
    }

    pub const fn dice_four() -> Icon {
        Icon::new("")
    }

    pub const fn gun() -> Icon {
        Icon::new("")
    }

    pub const fn franc_sign() -> Icon {
        Icon::new("")
    }

    pub const fn car_on() -> Icon {
        Icon::new("")
    }

    pub const fn landmark_flag() -> Icon {
        Icon::new("")
    }

    pub const fn hand_scissors() -> Icon {
        Icon::new("")
    }

    pub const fn truck_field() -> Icon {
        Icon::new("")
    }

    pub const fn nfc_directional() -> Icon {
        Icon::new("")
    }

    pub const fn building_circle_arrow_right() -> Icon {
        Icon::new("")
    }

    pub const fn glasses() -> Icon {
        Icon::new("")
    }

    pub const fn bridge() -> Icon {
        Icon::new("")
    }

    pub const fn creative_commons_nc_jp() -> Icon {
        Icon::new("")
    }

    pub const fn face_rolling_eyes() -> Icon {
        Icon::new("")
    }

    pub const fn foursquare() -> Icon {
        Icon::new("")
    }

    pub const fn bicycle() -> Icon {
        Icon::new("")
    }

    pub const fn plane_slash() -> Icon {
        Icon::new("")
    }

    pub const fn toilet_paper_slash() -> Icon {
        Icon::new("")
    }

    pub const fn toilet() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_right() -> Icon {
        Icon::new("")
    }

    pub const fn user_doctor() -> Icon {
        Icon::new("")
    }

    pub const fn bitbucket() -> Icon {
        Icon::new("")
    }

    pub const fn socks() -> Icon {
        Icon::new("")
    }

    pub const fn mountain_city() -> Icon {
        Icon::new("")
    }

    pub const fn vault() -> Icon {
        Icon::new("")
    }

    pub const fn arrow_turn_up() -> Icon {
        Icon::new("")
    }

    pub const fn stapler() -> Icon {
        Icon::new("")
    }

    pub const fn calendar_plus() -> Icon {
        Icon::new("")
    }

    pub const fn child_rifle() -> Icon {
        Icon::new("")
    }

    pub const fn laravel() -> Icon {
        Icon::new("")
    }

    pub const fn id_badge() -> Icon {
        Icon::new("")
    }

    pub const fn cloudscale() -> Icon {
        Icon::new("")
    }

    pub const fn apple_pay() -> Icon {
        Icon::new("")
    }

    pub const fn users_line() -> Icon {
        Icon::new("")
    }

    pub const fn deploydog() -> Icon {
        Icon::new("")
    }

    pub const fn fish_fins() -> Icon {
        Icon::new("")
    }

    pub const fn bridge_circle_exclamation() -> Icon {
        Icon::new("")
    }

    pub const fn stairs() -> Icon {
        Icon::new("")
    }

    pub const fn head_side_cough() -> Icon {
        Icon::new("")
    }

    pub const fn down_left_and_up_right_to_center() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_wide() -> Icon {
        Icon::new("")
    }

    pub const fn truck_medical() -> Icon {
        Icon::new("")
    }

    pub const fn tachograph_digital() -> Icon {
        Icon::new("")
    }

    pub const fn ubuntu() -> Icon {
        Icon::new("")
    }

    pub const fn square_instagram() -> Icon {
        Icon::new("")
    }

    pub const fn table_cells_large() -> Icon {
        Icon::new("")
    }

    pub const fn bity() -> Icon {
        Icon::new("")
    }

    pub const fn face_kiss_beam() -> Icon {
        Icon::new("")
    }

    pub const fn btc() -> Icon {
        Icon::new("")
    }

    pub const fn user_lock() -> Icon {
        Icon::new("")
    }

    pub const fn copy() -> Icon {
        Icon::new("")
    }

    pub const fn twitch() -> Icon {
        Icon::new("")
    }

    pub const fn face_grin_stars() -> Icon {
        Icon::new("")
    }

    pub const fn edge_legacy() -> Icon {
        Icon::new("")
    }

    pub const fn money_bill_wheat() -> Icon {
        Icon::new("")
    }

    pub const fn diagram_predecessor() -> Icon {
        Icon::new("")
    }

    pub const fn plus() -> Icon {
        Icon::new("+")
    }

    pub const fn hotjar() -> Icon {
        Icon::new("")
    }

    pub const fn z() -> Icon {
        Icon::new("Z")
    }

    pub const fn arrows_spin() -> Icon {
        Icon::new("")
    }

    pub const fn confluence() -> Icon {
        Icon::new("")
    }

    pub const fn info() -> Icon {
        Icon::new("")
    }

    pub const fn _9() -> Icon {
        Icon::new("9")
    }

    pub const fn book_medical() -> Icon {
        Icon::new("")
    }

    pub const fn temperature_high() -> Icon {
        Icon::new("")
    }

    pub const fn walkie_talkie() -> Icon {
        Icon::new("")
    }

    pub const fn simplybuilt() -> Icon {
        Icon::new("")
    }

    pub const fn cart_flatbed() -> Icon {
        Icon::new("")
    }

    pub const fn square_h() -> Icon {
        Icon::new("")
    }

    pub const fn road() -> Icon {
        Icon::new("")
    }

    pub const fn couch() -> Icon {
        Icon::new("")
    }

    pub const fn volcano() -> Icon {
        Icon::new("")
    }

    pub const fn ruler_horizontal() -> Icon {
        Icon::new("")
    }
}

/// An icon.
#[derive(Copy, Clone)]
pub struct Icon {
    pub(crate) color: Option<Color32>,
    pub(crate) size: f32,
    pub(crate) inner: &'static str,
}

impl Icon {
    const fn new(inner: &'static str) -> Self {
        Self {
            inner,
            size: 14.0,
            color: None,
        }
    }

    /// Sets the icon size.
    pub fn size(mut self, size: f32) -> Self {
        self.size = size;
        self
    }

    /// Sets the color.
    pub fn color(mut self, color: Color32) -> Self {
        self.color = Some(color);
        self
    }

    /// Returns the font id.
    pub fn font_id(&self) -> FontId {
        FontId::new(self.size, FontFamily::Name("icons".into()))
    }
}

impl From<Icon> for WidgetText {
    fn from(icon: Icon) -> Self {
        let mut text = RichText::new(icon.inner).font(icon.font_id());
        if let Some(color) = icon.color {
            text = text.color(color);
        }
        text.into()
    }
}
