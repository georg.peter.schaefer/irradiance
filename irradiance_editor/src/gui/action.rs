use crate::core::EditorEngine;

/// Trait for undo/redo actions.
pub trait Action {
    /// Returns the description.
    fn description(&self) -> String;
    /// Does the action.
    fn redo(&self, engine: &mut EditorEngine);
    /// Undoes the action.
    fn undo(&self, engine: &mut EditorEngine);
}

/// Action manager for undoing and redoing actions.
#[derive(Default)]
pub struct Actions {
    redos: Vec<Box<dyn Action>>,
    undos: Vec<Box<dyn Action>>,
    pending_changes: bool,
}

impl Actions {
    pub(crate) fn has_pending_changes(&self) -> bool {
        self.pending_changes
    }

    pub(crate) fn has_undos(&self) -> bool {
        !self.undos.is_empty()
    }

    pub(crate) fn next_undo_description(&self) -> Option<String> {
        self.undos.iter().last().map(|action| action.description())
    }

    pub(crate) fn has_redos(&self) -> bool {
        !self.redos.is_empty()
    }

    pub(crate) fn next_redo_description(&self) -> Option<String> {
        self.redos.iter().last().map(|action| action.description())
    }

    /// Pushes an undoable action.
    pub fn push(&mut self, engine: &mut EditorEngine, action: impl Action + 'static) {
        self.pending_changes = true;
        action.redo(engine);
        self.redos.clear();
        self.undos.push(Box::new(action));
        if self.undos.len() > 50 {
            self.undos.remove(0);
        }
    }

    pub(crate) fn undo(&mut self, engine: &mut EditorEngine) {
        if let Some(action) = self.undos.pop() {
            self.pending_changes = true;
            action.undo(engine);
            self.redos.push(action);
        }
    }

    pub(crate) fn redo(&mut self, engine: &mut EditorEngine) {
        if let Some(action) = self.redos.pop() {
            self.pending_changes = true;
            action.redo(engine);
            self.undos.push(action);
        }
    }

    pub(crate) fn clear_pending_changes(&mut self) {
        self.pending_changes = false;
    }
}
