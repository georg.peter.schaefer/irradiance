struct VSInput {
    [[vk::location(0)]] float2 position : POSITION0;
    [[vk::location(1)]] float2 tex_coord: TEXCOORD0;
    [[vk::location(2)]] float4 color : COLOR0;
};

[[vk::push_constant]]
cbuffer push_constants {
    float2 screen_size;
}

struct VSOutput {
    float4 position : SV_POSITION;
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
    [[vk::location(1)]] float4 color : COLOR0;
};

float3 linear_from_srgb(float3 srgb) {
    bool3 cutoff = srgb < float3(10.31475);
    float3 lower = srgb / float3(3294.6);
    float3 higher = pow((srgb + float3(14.025)) / float3(269.025), float3(2.4));
    return lerp(higher, lower, cutoff);
}

float4 linear_from_srgba(float4 srgba) {
    return float4(linear_from_srgb(srgba.rgb), srgba.a / 255.0);
}

VSOutput main(VSInput input) {
    VSOutput output = (VSOutput)0;

    output.position = float4(2.0 * input.position.x / screen_size.x - 1.0, 2.0 * input.position.y / screen_size.y - 1.0, 0.0, 1.0);
    output.tex_coord = input.tex_coord;
    output.color = linear_from_srgba(input.color);

    return output;
}
