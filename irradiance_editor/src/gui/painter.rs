use std::cell::UnsafeCell;
use std::sync::Arc;

use egui::epaint::Primitive;
use egui::{epaint, ImageData, TextureId, TexturesDelta};

use irradiance_runtime::gfx::buffer::{Buffer, BufferUsage};
use irradiance_runtime::gfx::command_buffer::{BufferImageCopy, CommandBufferBuilder, IndexType};
use irradiance_runtime::gfx::device::{Device, DeviceOwned};
use irradiance_runtime::gfx::image::{
    Filter, Format, Image, ImageAspects, ImageLayout, ImageSubresourceLayers, ImageType,
    ImageUsage, ImageView, ImageViewType, Sampler, SamplerAddressMode,
};
use irradiance_runtime::gfx::pipeline::{
    BlendFactor, BlendOp, ColorBlendAttachmentState, ColorMask, CullMode, DescriptorSet,
    DescriptorSetLayout, DescriptorType, DynamicState, GraphicsPipeline, PipelineBindPoint,
    PipelineLayout, PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate, Viewport,
    WriteDescriptorSetImages,
};
use irradiance_runtime::gfx::render_pass::{
    Attachment, AttachmentLoadOp, AttachmentStoreOp, ClearValue, RenderPass,
};
use irradiance_runtime::gfx::{Extent2D, Extent3D, GraphicsContext, Offset2D, Offset3D, Rect2D};
use irradiance_runtime::math::{Vec2, Vec4};

use super::EditorContext;

/// Ui painter.
#[derive(Debug)]
pub struct Painter {
    sampler: Arc<Sampler>,
    index_buffer: UnsafeCell<Arc<Buffer>>,
    vertex_buffer: UnsafeCell<Arc<Buffer>>,
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl Painter {
    /// Creates a new ui painter.
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Self {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(graphics_context.device().clone());
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            descriptor_set_layout.clone(),
        );
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            graphics_context.swapchain().unwrap().image_extent(),
            graphics_context.swapchain().unwrap().image_format(),
            pipeline_layout.clone(),
        );
        let vertex_buffer = UnsafeCell::new(Self::create_vertex_buffer(
            graphics_context.device().clone(),
            vec![Vertex::default()],
        ));
        let index_buffer = UnsafeCell::new(Self::create_index_buffer(
            graphics_context.device().clone(),
            vec![0],
        ));
        let sampler = Self::create_sampler(graphics_context.device().clone());

        descriptor_set_layout.set_debug_utils_object_name("GUI Painter Descriptor Set Layout");
        pipeline_layout.set_debug_utils_object_name("GUI Painter Pipeline Layout");
        graphics_pipeline.set_debug_utils_object_name("GUI Painter Graphics Pipeline");
        unsafe { &*vertex_buffer.get() }.set_debug_utils_object_name("GUI Painter Vertex Buffer");
        unsafe { &*index_buffer.get() }.set_debug_utils_object_name("GUI Painter Index Buffer");

        Self {
            sampler,
            index_buffer,
            vertex_buffer,
            graphics_pipeline,
            pipeline_layout,
            descriptor_set_layout,
            graphics_context,
        }
    }

    /// Draws a frame.
    pub fn draw(
        &self,
        context: &mut EditorContext,
        builder: &mut CommandBufferBuilder,
        pixels_per_point: f32,
        clipped_primitives: &[egui::ClippedPrimitive],
        textures_delta: &TexturesDelta,
        acquired_image: Arc<ImageView>,
    ) {
        builder.begin_label("GUI Painter", Vec4::new(0.5, 0.0, 1.0, 1.0));

        for (id, delta) in &textures_delta.set {
            self.update_texture(context, *id, delta);
        }

        self.draw_primitives(
            context,
            builder,
            pixels_per_point,
            clipped_primitives,
            acquired_image,
        );

        for id in &textures_delta.free {
            context.textures().remove(id);
        }

        builder.end_label();
    }

    fn draw_primitives(
        &self,
        context: &mut EditorContext,
        builder: &mut CommandBufferBuilder,
        pixels_per_point: f32,
        clipped_primitives: &[egui::ClippedPrimitive],
        swapchain_image: Arc<ImageView>,
    ) {
        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let mut callbacks = Vec::new();
        for egui::ClippedPrimitive { primitive, .. } in clipped_primitives {
            match primitive {
                Primitive::Mesh(mesh) => {
                    vertices.extend(mesh.vertices.iter().map(|vertex| Vertex {
                        position: Vec2::new(vertex.pos.x, vertex.pos.y),
                        tex_coord: Vec2::new(vertex.uv.x, vertex.uv.y),
                        color: Vec4::new(
                            vertex.color.r() as _,
                            vertex.color.g() as _,
                            vertex.color.b() as _,
                            vertex.color.a() as _,
                        ),
                    }));
                    indices.extend(&mesh.indices);
                }
                Primitive::Callback(callback) => {
                    callbacks.push(callback.clone());
                }
            }
        }
        if !vertices.is_empty() {
            *unsafe { &mut *self.vertex_buffer.get() } =
                Self::create_vertex_buffer(self.graphics_context.device().clone(), vertices);
        }
        if !indices.is_empty() {
            *unsafe { &mut *self.index_buffer.get() } =
                Self::create_index_buffer(self.graphics_context.device().clone(), indices);
        }

        let extent = swapchain_image.image().extent();
        let screen_size = Vec2::new(
            extent.width as f32 / pixels_per_point as f32,
            extent.height as f32 / pixels_per_point as f32,
        );

        for callback in callbacks {
            if let Some(callback) = callback.callback.clone().downcast::<CallbackFn>().ok() {
                (*callback.callback)(builder);
            }
        }

        builder
            .bind_graphics_pipeline(self.graphics_pipeline.clone())
            .begin_rendering(self.create_render_pass(swapchain_image))
            .set_viewport(
                0,
                vec![Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: extent.width as _,
                    height: extent.height as _,
                    min_depth: 0.0,
                    max_depth: 1.0,
                }],
            )
            .bind_vertex_buffer(unsafe { &mut *self.vertex_buffer.get() }.clone())
            .bind_index_buffer(
                unsafe { &mut *self.index_buffer.get() }.clone(),
                0,
                IndexType::UInt32,
            )
            .push_constants(
                self.pipeline_layout.clone(),
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                screen_size,
            );

        let mut first_index = 0;
        let mut vertex_offset = 0;
        for egui::ClippedPrimitive {
            clip_rect,
            primitive,
        } in clipped_primitives
        {
            if let Primitive::Mesh(mesh) = primitive {
                if let Some((_, _, descriptor_set)) = context.textures().get(&mesh.texture_id) {
                    let clip_min_x = pixels_per_point * clip_rect.min.x;
                    let clip_min_y = pixels_per_point * clip_rect.min.y;
                    let clip_max_x = pixels_per_point * clip_rect.max.x;
                    let clip_max_y = pixels_per_point * clip_rect.max.y;

                    let clip_min_x = clip_min_x.clamp(0.0, extent.width as f32);
                    let clip_min_y = clip_min_y.clamp(0.0, extent.height as f32);
                    let clip_max_x = clip_max_x.clamp(clip_min_x, extent.width as f32);
                    let clip_max_y = clip_max_y.clamp(clip_min_y, extent.height as f32);

                    let clip_min_x = clip_min_x.round() as u32;
                    let clip_min_y = clip_min_y.round() as u32;
                    let clip_max_x = clip_max_x.round() as u32;
                    let clip_max_y = clip_max_y.round() as u32;

                    builder
                        .set_scissor(
                            0,
                            vec![Rect2D {
                                offset: Offset2D {
                                    x: clip_min_x as _,
                                    y: clip_min_y as _,
                                },
                                extent: Extent2D {
                                    width: clip_max_x - clip_min_x,
                                    height: clip_max_y - clip_min_y,
                                },
                            }],
                        )
                        .bind_descriptor_sets(
                            PipelineBindPoint::Graphics,
                            self.pipeline_layout.clone(),
                            0,
                            vec![descriptor_set.clone()],
                        )
                        .draw_indexed(mesh.indices.len() as _, 1, first_index, vertex_offset, 0);

                    first_index += mesh.indices.len() as u32;
                    vertex_offset += mesh.vertices.len() as i32;
                }
            }
        }

        builder.end_rendering();
    }
}

impl Painter {
    fn create_descriptor_set_layout(device: Arc<Device>) -> Arc<DescriptorSetLayout> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
            .expect("Failed to create 'DescriptorSetLayout'")
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Arc<PipelineLayout> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<Vec2>() as _,
            )
            .build()
            .expect("Failed to create 'DescriptorSetLayout'")
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        extent: Extent2D,
        format: Format,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Arc<GraphicsPipeline> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32SFloat, 0)
                    .attribute(1, 0, Format::R32G32SFloat, 8)
                    .attribute(2, 0, Format::R32G32B32A32SFloat, 16)
                    .build(),
            )
            .vertex_shader(
                vertex_shader::Shader::load(device.clone()).expect("Failed to load vertex shader"),
                "main",
            )
            .fragment_shader(
                fragment_shader::Shader::load(device).expect("Failed to load fragment shader"),
                "main",
            )
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: extent.width as _,
                height: extent.height as _,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent,
            })
            .cull_mode(CullMode::None)
            .color_blend_attachment_state(
                ColorBlendAttachmentState::builder()
                    .enable_blend()
                    .src_color_blend_factor(BlendFactor::One)
                    .dst_color_blend_factor(BlendFactor::OneMinusSrcAlpha)
                    .color_blend_op(BlendOp::Add)
                    .src_alpha_blend_factor(BlendFactor::SrcAlpha)
                    .dst_alpha_blend_factor(BlendFactor::OneMinusSrcAlpha)
                    .alpha_blend_op(BlendOp::Add)
                    .color_write_mask(ColorMask {
                        r: true,
                        g: true,
                        b: true,
                        a: true,
                    })
                    .build(),
            )
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .layout(pipeline_layout)
            .color_attachment_format(format)
            .build()
            .expect("Failed to create 'GraphicsPipeline'")
    }

    fn create_vertex_buffer(device: Arc<Device>, vertices: Vec<Vertex>) -> Arc<Buffer> {
        Self::create_buffer(
            device,
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
        )
    }

    fn create_index_buffer(device: Arc<Device>, indices: Vec<u32>) -> Arc<Buffer> {
        Self::create_buffer(
            device,
            indices,
            BufferUsage {
                index_buffer: true,
                ..Default::default()
            },
        )
    }

    fn create_buffer<T>(device: Arc<Device>, data: Vec<T>, usage: BufferUsage) -> Arc<Buffer> {
        Buffer::from_data(device, data)
            .usage(usage)
            .build()
            .expect("Failed to create 'Buffer'")
    }

    fn create_sampler(device: Arc<Device>) -> Arc<Sampler> {
        Sampler::builder(device)
            .mag_filter(Filter::Linear)
            .address_mode_u(SamplerAddressMode::ClampToEdge)
            .address_mode_v(SamplerAddressMode::ClampToEdge)
            .address_mode_w(SamplerAddressMode::ClampToEdge)
            .build()
            .unwrap()
    }

    fn update_texture(
        &self,
        context: &mut EditorContext,
        id: TextureId,
        delta: &epaint::ImageDelta,
    ) {
        let pixels = match &delta.image {
            ImageData::Color(image) => image.pixels.clone(),
            ImageData::Font(image) => image.srgba_pixels(1.0).collect(),
        };
        let staging_buffer = Buffer::from_data(self.graphics_context.device().clone(), pixels)
            .usage(BufferUsage {
                transfer_src: true,
                ..Default::default()
            })
            .build()
            .unwrap();
        staging_buffer.set_debug_utils_object_name("GUI Painter Update Texture Staging Buffer");

        let (image_view, offset, extent) = if let Some(pos) = delta.pos {
            let (_, image_view, _) = context.textures().get(&id).unwrap().clone();
            let offset = Offset3D {
                x: pos[0] as _,
                y: pos[1] as _,
                z: 0,
            };
            let extent = Extent3D {
                width: delta.image.width() as _,
                height: delta.image.height() as _,
                depth: 1,
            };
            (image_view, offset, extent)
        } else {
            let image = Image::builder(self.graphics_context.device().clone())
                .image_type(ImageType::Type2D)
                .format(Format::R8G8B8A8Srgb)
                .extent(Extent3D {
                    width: delta.image.width() as _,
                    height: delta.image.height() as _,
                    depth: 1,
                })
                .usage(ImageUsage {
                    transfer_dst: true,
                    sampled: true,
                    ..Default::default()
                })
                .build()
                .unwrap();
            let image_view =
                ImageView::builder(self.graphics_context.device().clone(), image.clone())
                    .view_type(ImageViewType::Type2D)
                    .build()
                    .unwrap();

            let descriptor_set = DescriptorSet::builder(
                self.graphics_context.device().clone(),
                self.graphics_context
                    .descriptor_pool(&self.descriptor_set_layout)
                    .expect("descriptor pool"),
                self.descriptor_set_layout.clone(),
            )
            .build()
            .unwrap();

            descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
                .dst_binding(0)
                .descriptor_type(DescriptorType::CombinedImageSampler)
                .sampled_image(
                    &self.sampler,
                    &image_view,
                    ImageLayout::ShaderReadOnlyOptimal,
                )
                .build()]);

            image.set_debug_utils_object_name("GUI Painter Texture Image");
            image_view.set_debug_utils_object_name("GUI Painter Texture Image View");
            descriptor_set.set_debug_utils_object_name("GUI Painter Texture Descriptor Set");

            context
                .textures()
                .insert(id, (false, image_view.clone(), descriptor_set));

            (image_view, Offset3D::default(), image.extent())
        };

        staging_buffer
            .copy_to_image(
                self.graphics_context.graphics_queue().clone(),
                self.graphics_context.command_pool().unwrap(),
                image_view.image().clone(),
                BufferImageCopy::builder()
                    .image_subresource(
                        ImageSubresourceLayers::builder()
                            .image_aspects(ImageAspects {
                                color: true,
                                ..Default::default()
                            })
                            .build(),
                    )
                    .image_offset(offset)
                    .image_extent(extent)
                    .build(),
            )
            .unwrap();
    }

    fn create_render_pass(&self, image_view: Arc<ImageView>) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: image_view.image().extent().into(),
            })
            .color_attachment(
                Attachment::builder(image_view)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Float([0.0, 0.0, 0.0, 1.0]))
                    .build(),
            )
            .build()
    }
}

unsafe impl Send for Painter {}
unsafe impl Sync for Painter {}

#[allow(unused)]
#[derive(Default)]
#[repr(C)]
struct Vertex {
    position: Vec2,
    tex_coord: Vec2,
    color: Vec4,
}

mod vertex_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/gui/gui.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod fragment_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/gui/gui.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}

/// Painter callback.
pub struct CallbackFn {
    callback: Box<dyn Fn(&mut CommandBufferBuilder) + Send + Sync>,
}

impl<F: Fn(&mut CommandBufferBuilder) + Send + Sync + 'static> From<F> for CallbackFn {
    fn from(callback: F) -> Self {
        Self {
            callback: Box::new(callback),
        }
    }
}
