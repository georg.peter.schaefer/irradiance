use std::sync::Arc;

use egui::{Context, FontData, FontDefinitions, FontFamily, FullOutput, Id};
use egui_winit::State;
use irradiance_runtime::{
    core::Frame, gfx::GraphicsContext, window::Window, winit::event::WindowEvent,
};

use super::{EditorContext, Painter};

pub struct GuiContext {
    pub(crate) ctx: Context,
    painter: Arc<Painter>,
    wsi_state: State,
    pub(crate) window: Arc<Window>,
}

impl GuiContext {
    pub fn new(window: Arc<Window>, graphics_context: Arc<GraphicsContext>) -> Self {
        let ctx = Context::default();
        let mut fonts = FontDefinitions::default();
        fonts.font_data.insert(
            "icons".to_owned(),
            FontData::from_static(include_bytes!("Font Awesome 6 Free-Solid-900.otf")),
        );
        fonts
            .families
            .entry(FontFamily::Name("icons".into()))
            .or_default()
            .push("icons".to_owned());
        ctx.set_fonts(fonts);

        let painter = Arc::new(Painter::new(graphics_context.clone()));
        ctx.data().insert_temp(Id::new("painter"), painter.clone());

        let mut wsi_state = State::new(window.event_loop());
        wsi_state.set_max_texture_side(
            graphics_context
                .device()
                .physical_device()
                .properties()
                .limits
                .max_image_dimension2_d as _,
        );

        Self {
            ctx,
            painter,
            wsi_state,
            window,
        }
    }

    pub fn on_event(&mut self, event: &WindowEvent) {
        self.wsi_state.on_event(&self.ctx, event);
    }

    pub fn update(
        &mut self,
        editor_context: &mut EditorContext,
        frame: &mut Frame,
        run_ui: impl FnOnce(&Context, &mut EditorContext),
    ) {
        let raw_input = self.wsi_state.take_egui_input(self.window.inner());
        let FullOutput {
            platform_output,
            textures_delta,
            shapes,
            ..
        } = self.ctx.run(raw_input, |ctx| run_ui(ctx, editor_context));

        self.wsi_state
            .handle_platform_output(self.window.inner(), &self.ctx, platform_output);

        let clipped_primitives = self.ctx.tessellate(shapes);
        let swapchain_image = frame.swapchain_image();
        self.painter.draw(
            editor_context,
            frame.builder_mut(),
            self.ctx.pixels_per_point(),
            &clipped_primitives,
            &textures_delta,
            swapchain_image,
        );

        editor_context.evict_textures();
    }
}
