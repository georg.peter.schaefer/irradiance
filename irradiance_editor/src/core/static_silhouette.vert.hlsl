struct VSInput {
    [[vk::location(0)]] float3 position : POSITION0;
};

struct PushConstants {
    float4x4 projection;
    float4x4 view;
    float4x4 model;
};

[[vk::push_constant]]
cbuffer push_constants {
    PushConstants push_constants;
}

struct VSOutput {
    float4 position : SV_POSITION;
};

VSOutput main(VSInput input) {
    VSOutput output = (VSOutput)0;

    output.position =  float4(input.position, 1.0) * push_constants.model * push_constants.view * push_constants.projection;

    return output;
}
