struct VSOutput {
    [[vk::location(0)]] uint upper;
    [[vk::location(1)]] uint lower;
};

struct FSOutput {
    uint2 entity_id : SV_TARGET0;
};

FSOutput main(VSOutput input) {
    FSOutput output = (FSOutput)0;

    output.entity_id = uint2(input.upper, input.lower);

    return output;
}
