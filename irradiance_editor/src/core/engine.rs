use std::{path::PathBuf, rc::Rc, sync::Arc};

use irradiance_runtime::animation::Animations;
use irradiance_runtime::audio::Audio;
use irradiance_runtime::core::Config;
use irradiance_runtime::gfx::physical_device::PhysicalDeviceFeatures;
use irradiance_runtime::physics::Physics;
use irradiance_runtime::{
    asset::{Assets, FilesystemSource},
    core::{Engine, Frame, Screens, ScreensBuilder},
    ecs::{ComponentTypes, ComponentTypesBuilder},
    gfx::{
        instance::InstanceExtensions, physical_device::DeviceExtensions, Extent2D, GraphicsContext,
    },
    input::Input,
    rendering::pbr::BufPbrPipeline,
    scene::Scenes,
    window::Window,
    winit::event::Event,
};

use super::{BufLog, EditorScreen};

/// Editor integrated engine.
pub struct EditorEngine {
    config: Config,
    should_close: bool,
    physics: Physics,
    scenes: Scenes,
    screens: Rc<Screens>,
    audio: Audio,
    animations: Animations,
    buf_pbr_pipeline: Arc<BufPbrPipeline>,
    assets: Arc<Assets>,
    graphics_context: Arc<GraphicsContext>,
    component_types: ComponentTypes,
    input: Input,
    window: Arc<Window>,
    buf_log: Arc<BufLog>,
}

impl EditorEngine {
    /// Creates a new engine.
    pub fn new(
        title: String,
        asset_path: PathBuf,
        component_types: ComponentTypesBuilder,
        screens: ScreensBuilder,
    ) -> Self {
        let buf_log = BufLog::new();
        let window = Window::builder()
            .title(&title)
            .maximized()
            .build()
            .expect("window");
        let input = Input::new(asset_path.clone(), window.clone());
        let graphics_context = Self::create_graphics_context(window.clone());
        let assets = Assets::builder()
            .source(FilesystemSource::new(asset_path.clone()).expect("filesystem source"))
            .build();
        let component_types = component_types.build();
        let buf_pbr_pipeline = BufPbrPipeline::with_size(
            graphics_context.clone(),
            assets.clone(),
            Extent2D {
                width: 1024,
                height: 768,
            },
        );
        let scenes = Default::default();
        let physics = Physics::new(graphics_context.clone(), assets.clone());

        let mut engine = Self {
            config: Default::default(),
            buf_log,
            window,
            input,
            component_types,
            graphics_context,
            assets,
            buf_pbr_pipeline,
            animations: Animations::default(),
            audio: Audio::new(),
            screens: Default::default(),
            scenes,
            physics,
            should_close: false,
        };

        engine.screens = Rc::new(screens.screen::<EditorScreen>().build(&engine));
        engine.scenes = Scenes::new(asset_path, &engine);

        engine
    }

    fn create_graphics_context(window: Arc<Window>) -> Arc<GraphicsContext> {
        #[cfg(target_os = "linux")]
        let instance_extensions = InstanceExtensions {
            khr_surface: true,
            khr_xlib_surface: true,
            ..Default::default()
        };
        #[cfg(target_os = "windows")]
        let instance_extensions = InstanceExtensions {
            khr_surface: true,
            khr_win32_surface: true,
            ..Default::default()
        };

        GraphicsContext::with_swapchain(window)
            .instance_extensions(instance_extensions)
            .device_extensions(DeviceExtensions {
                khr_swapchain: true,
                khr_maintenance1: true,
                ..Default::default()
            })
            .physical_device_features(PhysicalDeviceFeatures {
                depth_clamp: true,
                shader_image_gather_extended: true,
                wide_lines: true,
                sampler_anisotropy: true,
                ..Default::default()
            })
            .build()
            .unwrap()
    }

    pub(crate) fn buf_log(&self) -> &BufLog {
        &self.buf_log
    }

    pub(crate) fn recreate_buf_pbr_pipeline(&mut self, extent: Extent2D) {
        self.buf_pbr_pipeline = self.buf_pbr_pipeline.clone().resize(extent);
    }
}

impl Engine for EditorEngine {
    fn window(&self) -> &Arc<Window> {
        &self.window
    }

    fn input(&self) -> &Input {
        &self.input
    }

    fn input_mut(&mut self) -> &mut Input {
        &mut self.input
    }

    fn component_types(&self) -> &ComponentTypes {
        &self.component_types
    }

    fn graphics_context(&self) -> &Arc<GraphicsContext> {
        &self.graphics_context
    }

    fn assets(&self) -> &Arc<Assets> {
        &self.assets
    }

    fn buf_pbr_pipeline(&self) -> &Arc<BufPbrPipeline> {
        &self.buf_pbr_pipeline
    }

    fn animations(&self) -> &Animations {
        &self.animations
    }

    fn audio(&self) -> &Audio {
        &self.audio
    }

    fn audio_mut(&mut self) -> &mut Audio {
        &mut self.audio
    }

    fn screens(&self) -> &Screens {
        &self.screens
    }

    fn scenes(&self) -> &Scenes {
        &self.scenes
    }

    fn physics(&self) -> &Physics {
        &self.physics
    }

    fn physics_mut(&mut self) -> &mut Physics {
        &mut self.physics
    }

    fn should_close(&self) -> bool {
        self.should_close
    }

    fn close(&mut self) {
        self.should_close = true;
    }

    fn on_event(&mut self, event: &Event<()>) {
        self.input.on_event(event);
    }

    fn update(&mut self, frame: &mut Frame) {
        let screens = self.screens.clone();

        self.input.update(frame);
        screens.update(self, frame);
        screens.draw(self, frame);
        screens.commit(self);

        self.input.reset();
    }

    fn config(&self) -> &Config {
        &self.config
    }

    fn config_mut(&mut self) -> &mut Config {
        &mut self.config
    }
}
