use crate::core::select_pipeline::select_compute_shader::{Mouse, Selection};
use crate::core::select_pipeline::shaders::PushConstants;
use crate::core::EditorEngine;
use crate::ecs::EditorComponentTypes;
use irradiance_runtime::ecs::{Entities, Facade, Transform};
use irradiance_runtime::gfx::buffer::{Buffer, BufferUsage};
use irradiance_runtime::gfx::command_buffer::{CommandBufferBuilder, IndexType};
use irradiance_runtime::gfx::device::{Device, DeviceOwned};
use irradiance_runtime::gfx::image::{
    Format, Image, ImageAspects, ImageLayout, ImageSubresourceRange, ImageType, ImageUsage,
    ImageView, ImageViewType,
};
use irradiance_runtime::gfx::pipeline::{
    ColorBlendAttachmentState, CompareOp, ComputePipeline, CullMode, DescriptorSet,
    DescriptorSetLayout, DescriptorType, DynamicState, FrontFace, GraphicsPipeline,
    PipelineBindPoint, PipelineLayout, PrimitiveTopology, ShaderStages, VertexInput,
    VertexInputRate, Viewport, WriteDescriptorSetBuffers, WriteDescriptorSetImages,
};
use irradiance_runtime::gfx::render_pass::{
    Attachment, AttachmentLoadOp, AttachmentStoreOp, ClearValue, RenderPass,
};
use irradiance_runtime::gfx::sync::{
    AccessMask, BufferMemoryBarrier, ImageMemoryBarrier, PipelineBarrier, PipelineStages,
};
use irradiance_runtime::gfx::{Extent2D, GfxError, GraphicsContext, Rect2D};
use irradiance_runtime::math::{Mat4, Vec4};
use irradiance_runtime::rendering::{mesh, skinned_mesh, AnimatedMesh, Camera, Mesh, StaticMesh};
use std::any::TypeId;
use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::sync::Arc;

pub struct SelectPipeline {
    executed: bool,
    select_buffer: Arc<Buffer>,
    select_pass: SelectPass,
    depth_buffer: Arc<ImageView>,
    entity_id_buffer: Arc<ImageView>,
    entity_id_pass: EntityIdPass,
}

impl SelectPipeline {
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let select_pipeline = Self {
            entity_id_pass: EntityIdPass::new(graphics_context.clone())?,
            entity_id_buffer: Self::create_entity_id_buffer(
                graphics_context.clone(),
                Extent2D {
                    width: 100,
                    height: 100,
                },
            ),
            depth_buffer: Self::create_depth_buffer(
                graphics_context.clone(),
                Extent2D {
                    width: 100,
                    height: 100,
                },
            ),
            select_pass: SelectPass::new(graphics_context.clone())?,
            select_buffer: Self::create_select_buffer(graphics_context)?,
            executed: false,
        };

        select_pipeline
            .entity_id_buffer
            .image()
            .set_debug_utils_object_name("Select Pipeline Entity Id Buffer Image");
        select_pipeline
            .entity_id_buffer
            .set_debug_utils_object_name("Select Pipeline Entity Id Buffer Image View");
        select_pipeline
            .depth_buffer
            .image()
            .set_debug_utils_object_name("Select Pipeline Depth Buffer Image");
        select_pipeline
            .depth_buffer
            .set_debug_utils_object_name("Select Pipeline Depth Buffer Image View");
        select_pipeline
            .select_buffer
            .set_debug_utils_object_name("Select Pipeline Select Buffer");

        Ok(select_pipeline)
    }

    fn create_select_buffer(
        graphics_context: Arc<GraphicsContext>,
    ) -> Result<Arc<Buffer>, GfxError> {
        Buffer::from_data(
            graphics_context.device().clone(),
            vec![Selection::default()],
        )
        .usage(BufferUsage {
            storage_buffer: true,
            ..Default::default()
        })
        .build()
    }

    /// Sets the component displays.
    pub fn set_displays(&mut self, component_types: &EditorComponentTypes, engine: &EditorEngine) {
        self.entity_id_pass.displays = component_types
            .types()
            .flat_map(|component_type| {
                component_types
                    .display(component_type.clone(), engine)
                    .map(|display| (component_type, display))
            })
            .collect();
    }

    pub fn executed(&self) -> bool {
        self.executed
    }

    pub fn selected<'a>(&'a mut self, entities: &'a Entities) -> Option<Facade<'a>> {
        let selected = entities
            .all()
            .into_iter()
            .find(|entity| self.is_selected(entity));
        self.executed = false;
        selected
    }

    fn is_selected(&self, entity: &Facade) -> bool {
        let mut hasher = DefaultHasher::default();
        entity.id().hash(&mut hasher);
        let entity_id = hasher.finish();
        let selection = self.select_buffer.read::<Selection>().unwrap_or_default();
        let id = (selection.upper as u64) << 32 | selection.lower as u64;
        id == entity_id
    }

    pub fn resize(&mut self, extent: Extent2D) {
        self.entity_id_buffer =
            Self::create_entity_id_buffer(self.entity_id_pass.graphics_context.clone(), extent);
        self.depth_buffer =
            Self::create_depth_buffer(self.entity_id_pass.graphics_context.clone(), extent);
    }

    fn create_entity_id_buffer(
        graphics_context: Arc<GraphicsContext>,
        extent: Extent2D,
    ) -> Arc<ImageView> {
        ImageView::builder(
            graphics_context.device().clone(),
            Image::builder(graphics_context.device().clone())
                .image_type(ImageType::Type2D)
                .format(Format::R32G32UInt)
                .extent(extent.into())
                .usage(ImageUsage {
                    color_attachment: true,
                    storage: true,
                    ..Default::default()
                })
                .build()
                .expect("entity id buffer"),
        )
        .view_type(ImageViewType::Type2D)
        .build()
        .expect("entity id buffer")
    }

    fn create_depth_buffer(
        graphics_context: Arc<GraphicsContext>,
        extent: Extent2D,
    ) -> Arc<ImageView> {
        ImageView::builder(
            graphics_context.device().clone(),
            Image::builder(graphics_context.device().clone())
                .image_type(ImageType::Type2D)
                .format(Format::D32SFloat)
                .extent(extent.into())
                .usage(ImageUsage {
                    depth_stencil_attachment: true,
                    ..Default::default()
                })
                .build()
                .expect("depth buffer"),
        )
        .view_type(ImageViewType::Type2D)
        .subresource_range(
            ImageSubresourceRange::builder()
                .aspect_mask(ImageAspects {
                    depth: true,
                    ..Default::default()
                })
                .build(),
        )
        .build()
        .expect("depth buffer")
    }

    pub fn draw(
        &mut self,
        builder: &mut CommandBufferBuilder,
        entities: &Entities,
        cursor_position: [u32; 2],
    ) -> Result<(), GfxError> {
        self.entity_id_pass.draw(
            builder,
            self.entity_id_buffer.clone(),
            self.depth_buffer.clone(),
            entities,
        )?;
        self.select_pass.dispatch(
            builder,
            cursor_position,
            self.entity_id_buffer.clone(),
            self.select_buffer.clone(),
        )?;
        self.executed = true;

        Ok(())
    }
}

struct EntityIdPass {
    animated_graphics_pipeline: Arc<GraphicsPipeline>,
    static_graphics_pipeline: Arc<GraphicsPipeline>,
    animated_pipeline_layout: Arc<PipelineLayout>,
    static_pipeline_layout: Arc<PipelineLayout>,
    joint_descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
    displays: HashMap<TypeId, Mesh>,
}

impl EntityIdPass {
    fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let joint_descriptor_set_layout =
            Self::create_joint_descriptor_set_layout(graphics_context.device().clone())?;
        let static_pipeline_layout =
            Self::create_static_pipeline_layout(graphics_context.device().clone())?;
        let animated_pipeline_layout = Self::create_animated_pipeline_layout(
            graphics_context.device().clone(),
            joint_descriptor_set_layout.clone(),
        )?;
        let static_graphics_pipeline = Self::create_static_graphics_pipeline(
            graphics_context.device().clone(),
            static_pipeline_layout.clone(),
        )?;
        let animated_graphics_pipeline = Self::create_animated_graphics_pipeline(
            graphics_context.device().clone(),
            animated_pipeline_layout.clone(),
        )?;

        joint_descriptor_set_layout
            .set_debug_utils_object_name("Entity Id Pass Joint Descriptor Set Layout");
        static_pipeline_layout.set_debug_utils_object_name("Entity Id Pass Static Pipeline Layout");
        animated_pipeline_layout
            .set_debug_utils_object_name("Entity Id Pass Animated Pipeline Layout");
        static_graphics_pipeline
            .set_debug_utils_object_name("Entity Id Pass Static Graphics Pipeline");
        animated_graphics_pipeline
            .set_debug_utils_object_name("Entity Id Pass Animated Graphics Pipeline");

        Ok(Self {
            animated_graphics_pipeline,
            static_graphics_pipeline,
            animated_pipeline_layout,
            static_pipeline_layout,
            joint_descriptor_set_layout,
            graphics_context,
            displays: Default::default(),
        })
    }

    fn create_joint_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::StorageBuffer,
                1,
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_static_pipeline_layout(device: Arc<Device>) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_animated_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_static_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .build(),
            )
            .vertex_shader(
                static_entity_id_vertex_shader::Shader::load(device.clone())?,
                "main",
            )
            .fragment_shader(entity_id_fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::Less)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R32G32UInt)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    fn create_animated_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<skinned_mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .attribute(1, 0, Format::R32G32B32A32SFloat, 48)
                    .attribute(2, 0, Format::R32G32B32A32SFloat, 64)
                    .build(),
            )
            .vertex_shader(
                animated_entity_id_vertex_shader::Shader::load(device.clone())?,
                "main",
            )
            .fragment_shader(entity_id_fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .depth_test(true, CompareOp::Less)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R32G32UInt)
            .depth_attachment_format(Format::D32SFloat)
            .build()
    }

    fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        entity_id_buffer: Arc<ImageView>,
        depth_buffer: Arc<ImageView>,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder.begin_label("Entity Id Pass", Vec4::new(0.3, 0.3, 0.3, 1.0));

        builder
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(entity_id_buffer.image().clone())
                            .src_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(depth_buffer.image().clone())
                            .src_stage_mask(PipelineStages {
                                early_fragment_tests: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                depth_stencil_attachment_read: true,
                                depth_stencil_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                early_fragment_tests: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                depth_stencil_attachment_read: true,
                                depth_stencil_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::DepthAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        depth: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .bind_graphics_pipeline(self.static_graphics_pipeline.clone())
            .begin_rendering(self.create_render_pass(entity_id_buffer.clone(), depth_buffer))
            .set_viewport(
                0,
                vec![Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: entity_id_buffer.image().extent().width as _,
                    height: entity_id_buffer.image().extent().height as _,
                    min_depth: 0.0,
                    max_depth: 1.0,
                }],
            )
            .set_scissor(
                0,
                vec![Rect2D {
                    offset: Default::default(),
                    extent: entity_id_buffer.image().extent().into(),
                }],
            );

        if let Some((Some(camera), Some(camera_transform))) = entities
            .active::<Camera>()
            .map(|camera| (camera.get::<Camera>(), camera.get::<Transform>()))
        {
            for (entity, transform) in
                entities
                    .components::<(&Transform,)>()
                    .filter(|(entity, _)| {
                        Some(entity.id().to_string())
                            != entities
                                .active::<Camera>()
                                .map(|entity| entity.id().to_string())
                    })
            {
                let static_mesh = entity.get::<StaticMesh>();
                if let Some(mesh) = static_mesh
                    .as_ref()
                    .and_then(|static_mesh| static_mesh.mesh().ok())
                    .or_else(|| {
                        entity
                            .types()
                            .iter()
                            .flat_map(|component_type| self.displays.get(component_type))
                            .next()
                    })
                {
                    builder
                        .bind_vertex_buffer(mesh.vertex_buffer().clone())
                        .bind_index_buffer(mesh.index_buffer().clone(), 0, IndexType::UInt32);

                    let mut hasher = DefaultHasher::new();
                    entity.id().hash(&mut hasher);
                    let entity_id = hasher.finish();

                    for sub_mesh in mesh.sub_meshes() {
                        builder
                            .push_constants(
                                self.static_pipeline_layout.clone(),
                                ShaderStages {
                                    vertex: true,
                                    ..Default::default()
                                },
                                0,
                                PushConstants {
                                    projection: camera.projection(
                                        entity_id_buffer.image().extent().width as f32
                                            / entity_id_buffer.image().extent().height as f32,
                                    ),
                                    view: Mat4::from(*camera_transform).inverse(),
                                    model: Mat4::from(*transform),
                                    upper: (entity_id >> 32) as u32,
                                    lower: entity_id as u32,
                                },
                            )
                            .draw_indexed(sub_mesh.count() as _, 1, sub_mesh.offset() as _, 0, 0);
                    }
                }
            }

            builder.bind_graphics_pipeline(self.animated_graphics_pipeline.clone());

            for (entity, transform, animated_mesh) in
                entities.components::<(&Transform, &AnimatedMesh)>()
            {
                if let Some(mesh) = animated_mesh.mesh().ok() {
                    if let Some(joint_buffer) = animated_mesh.joint_buffer() {
                        let joint_descriptor_set =
                            self.create_joint_descriptor_set(joint_buffer)?;
                        builder
                            .bind_vertex_buffer(mesh.vertex_buffer().clone())
                            .bind_index_buffer(mesh.index_buffer().clone(), 0, IndexType::UInt32)
                            .bind_descriptor_sets(
                                PipelineBindPoint::Graphics,
                                self.animated_pipeline_layout.clone(),
                                0,
                                vec![joint_descriptor_set],
                            );

                        let mut hasher = DefaultHasher::new();
                        entity.id().hash(&mut hasher);
                        let entity_id = hasher.finish();

                        for sub_mesh in mesh.sub_meshes() {
                            builder
                                .push_constants(
                                    self.animated_pipeline_layout.clone(),
                                    ShaderStages {
                                        vertex: true,
                                        ..Default::default()
                                    },
                                    0,
                                    PushConstants {
                                        projection: camera.projection(
                                            entity_id_buffer.image().extent().width as f32
                                                / entity_id_buffer.image().extent().height as f32,
                                        ),
                                        view: Mat4::from(*camera_transform).inverse(),
                                        model: Mat4::from(*transform),
                                        upper: (entity_id >> 32) as u32,
                                        lower: entity_id as u32,
                                    },
                                )
                                .draw_indexed(
                                    sub_mesh.count() as _,
                                    1,
                                    sub_mesh.offset() as _,
                                    0,
                                    0,
                                );
                        }
                    }
                }
            }
        }

        builder.end_label().end_rendering();

        Ok(())
    }

    fn create_render_pass(
        &self,
        entity_id_buffer: Arc<ImageView>,
        depth_buffer: Arc<ImageView>,
    ) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: entity_id_buffer.image().extent().into(),
            })
            .color_attachment(
                Attachment::builder(entity_id_buffer)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Int([0, 0, 0, 255]))
                    .build(),
            )
            .depth_attachment(
                Attachment::builder(depth_buffer)
                    .image_layout(ImageLayout::DepthAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Depth(1.0))
                    .build(),
            )
            .build()
    }

    fn create_joint_descriptor_set(
        &self,
        joint_buffer: Arc<Buffer>,
    ) -> Result<Arc<DescriptorSet>, GfxError> {
        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.joint_descriptor_set_layout)?,
            self.joint_descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Entity Id Pass Animated Descriptor Set");

        descriptor_set.write_buffers(vec![WriteDescriptorSetBuffers::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::StorageBuffer)
            .buffer(&joint_buffer, 0, !0)
            .build()]);

        Ok(descriptor_set)
    }
}

mod shaders {
    use irradiance_runtime::math::Mat4;

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub projection: Mat4,
        pub view: Mat4,
        pub model: Mat4,
        pub upper: u32,
        pub lower: u32,
    }
}

mod static_entity_id_vertex_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/core/static_entity_id.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod animated_entity_id_vertex_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/core/animated_entity_id.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod entity_id_fragment_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/core/entity_id.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}

struct SelectPass {
    compute_pipeline: Arc<ComputePipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl SelectPass {
    fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(graphics_context.device().clone())?;
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            descriptor_set_layout.clone(),
        )?;
        let compute_pipeline = Self::create_compute_pipeline(
            graphics_context.device().clone(),
            pipeline_layout.clone(),
        )?;

        descriptor_set_layout.set_debug_utils_object_name("Select Pass Descriptor Set Layout");
        pipeline_layout.set_debug_utils_object_name("Select Pass Pipeline Layout");
        compute_pipeline.set_debug_utils_object_name("Select Pass Compute Pipeline");

        Ok(Self {
            compute_pipeline,
            pipeline_layout,
            descriptor_set_layout,
            graphics_context,
        })
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::StorageImage,
                1,
                ShaderStages {
                    compute: true,
                    ..Default::default()
                },
            )
            .binding(
                1,
                DescriptorType::StorageBuffer,
                1,
                ShaderStages {
                    compute: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    compute: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<Mouse>() as _,
            )
            .build()
    }

    fn create_compute_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<ComputePipeline>, GfxError> {
        ComputePipeline::builder(device.clone())
            .shader(select_compute_shader::Shader::load(device)?, "main")
            .layout(pipeline_layout)
            .build()
    }

    fn dispatch(
        &self,
        builder: &mut CommandBufferBuilder,
        cursor_position: [u32; 2],
        entity_id_buffer: Arc<ImageView>,
        select_buffer: Arc<Buffer>,
    ) -> Result<(), GfxError> {
        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.descriptor_set_layout)?,
            self.descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Select Pass Descriptor Set");

        descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::StorageImage)
            .image(&entity_id_buffer, ImageLayout::General)
            .build()]);
        descriptor_set.write_buffers(vec![WriteDescriptorSetBuffers::builder()
            .dst_binding(1)
            .descriptor_type(DescriptorType::StorageBuffer)
            .buffer(&select_buffer, 0, select_buffer.size())
            .build()]);

        builder
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(entity_id_buffer.image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                compute_shader: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                shader_storage_read: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::General)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .buffer_memory_barrier(
                        BufferMemoryBarrier::builder(select_buffer.clone())
                            .src_stage_mask(PipelineStages {
                                host: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                memory_read: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                compute_shader: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                shader_storage_write: true,
                                ..Default::default()
                            })
                            .build(),
                    )
                    .build(),
            )
            .bind_compute_pipeline(self.compute_pipeline.clone())
            .bind_descriptor_sets(
                PipelineBindPoint::Compute,
                self.pipeline_layout.clone(),
                0,
                vec![descriptor_set],
            )
            .push_constants(
                self.pipeline_layout.clone(),
                ShaderStages {
                    compute: true,
                    ..Default::default()
                },
                0,
                Mouse {
                    x: cursor_position[0],
                    y: cursor_position[1],
                },
            )
            .dispatch(1, 1, 1)
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .buffer_memory_barrier(
                        BufferMemoryBarrier::builder(select_buffer.clone())
                            .src_stage_mask(PipelineStages {
                                compute_shader: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                shader_storage_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                host: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                memory_read: true,
                                ..Default::default()
                            })
                            .build(),
                    )
                    .build(),
            );

        Ok(())
    }
}

mod select_compute_shader {
    use irradiance_runtime::shader;

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct Mouse {
        pub x: u32,
        pub y: u32,
    }

    #[derive(Default, Debug)]
    #[repr(C)]
    pub struct Selection {
        pub upper: u32,
        pub lower: u32,
        pub _dummy0: u32,
        pub _dummy1: u32,
    }

    shader! {
        path: "src/core/select.compute.hlsl",
        ty: "compute",
        entry_point: "main"
    }
}
