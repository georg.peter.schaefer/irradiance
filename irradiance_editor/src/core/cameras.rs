use approx::relative_ne;
use irradiance_runtime::input::Mouse;
use irradiance_runtime::math::Vec2;
use irradiance_runtime::{
    core::{Engine, Frame},
    ecs::{Entities, SphericalCoordinate, Transform},
    math::{Mat4, Vec3},
    rendering::Camera,
};

#[derive(Default)]
pub struct Cameras;

impl Cameras {
    pub fn update(&self, engine: &mut dyn Engine, frame: &mut Frame, entities: &Entities) {
        let input = engine.input();
        let mouse = engine.input().get::<Mouse>("Mouse");
        if mouse.continuous() {
            for (_, mut transform, mut spherical_coordinate, _) in entities
                .components::<(&mut Transform, &mut SphericalCoordinate, &Camera)>()
                .filter(|(entity, _, _, _)| entity.id() == "__internal_editor_camera__")
            {
                let angular_velocity = 4.0;
                let mouse_delta = Vec2::new(
                    input.axis("Mouse/HorizontalDelta").get(),
                    input.axis("Mouse/VerticalDelta").get(),
                );
                let inclination =
                    spherical_coordinate.inclination() - mouse_delta[1] * angular_velocity;
                let azimuth = spherical_coordinate.azimuth() - mouse_delta[0] * angular_velocity;

                spherical_coordinate.set_inclination(inclination);
                spherical_coordinate.set_azimuth(azimuth);

                let mut position = transform.position();
                let center = spherical_coordinate.to_cartesian() + position;
                let mut up = *spherical_coordinate;
                up.set_inclination(inclination - 0.1);
                let up = (up.to_cartesian() - spherical_coordinate.to_cartesian()).normalize();

                transform.set_orientation(Mat4::look_at(position, center, up).inverse().into());

                let lateral_velocity = 10.0;
                let forward = (center - position).normalize();
                let right = forward.cross(up).normalize();

                let mut direction = Vec3::default();
                if input.button("Keyboard/W").down() {
                    direction += forward;
                }
                if input.button("Keyboard/S").down() {
                    direction -= forward;
                }
                if input.button("Keyboard/D").down() {
                    direction += right
                }
                if input.button("Keyboard/A").down() {
                    direction -= right;
                }
                if relative_ne!(direction.length2(), 0.0) {
                    direction = direction.normalize();
                }

                position += direction * lateral_velocity * frame.time().delta();
                transform.set_position(position);
            }
        }
    }
}
