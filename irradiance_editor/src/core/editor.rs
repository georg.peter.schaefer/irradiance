use std::{path::PathBuf, sync::Arc};

use irradiance_runtime::animation::Animations;
use irradiance_runtime::audio::Audio;
use irradiance_runtime::core::Config;
use irradiance_runtime::input::Mouse;
use irradiance_runtime::physics::Physics;
use irradiance_runtime::{
    asset::Assets,
    core::{Engine, Frame, ScreenExt, Screens, ScreensBuilder},
    ecs::{ComponentExt, ComponentTypes, ComponentTypesBuilder},
    gfx::GraphicsContext,
    input::Input,
    rendering::pbr::BufPbrPipeline,
    scene::Scenes,
    window::Window,
    winit::event::Event,
};

use crate::{
    asset::{EditorAssetExt, EditorAssetTypes, EditorAssetTypesBuilder},
    core::EditorScreen,
    ecs::{EditorComponentExt, EditorComponentTypes, EditorComponentTypesBuilder},
    gui::Gui,
};

use super::EditorEngine;

/// Editor.
pub struct Editor {
    gui: Gui,
    engine: EditorEngine,
}

impl Editor {
    /// Starts building a new editor.
    pub fn builder() -> EditorBuilder {
        EditorBuilder {
            title: "irradiance - Editor".to_owned(),
            assets: None,
            component_types: ComponentTypes::builder(),
            screens: Screens::builder(),
            asset_types: EditorAssetTypes::builder(),
            editor_component_types: EditorComponentTypes::builder(),
        }
    }
}

impl Engine for Editor {
    fn window(&self) -> &Arc<Window> {
        self.engine.window()
    }

    fn input(&self) -> &Input {
        self.engine.input()
    }

    fn input_mut(&mut self) -> &mut Input {
        self.engine.input_mut()
    }

    fn component_types(&self) -> &ComponentTypes {
        self.engine.component_types()
    }

    fn graphics_context(&self) -> &Arc<GraphicsContext> {
        self.engine.graphics_context()
    }

    fn assets(&self) -> &Arc<Assets> {
        self.engine.assets()
    }

    fn buf_pbr_pipeline(&self) -> &Arc<BufPbrPipeline> {
        self.engine.buf_pbr_pipeline()
    }

    fn animations(&self) -> &Animations {
        self.engine.animations()
    }

    fn audio(&self) -> &Audio {
        self.engine.audio()
    }

    fn audio_mut(&mut self) -> &mut Audio {
        self.engine.audio_mut()
    }

    fn screens(&self) -> &Screens {
        self.engine.screens()
    }

    fn scenes(&self) -> &Scenes {
        self.engine.scenes()
    }

    fn physics(&self) -> &Physics {
        self.engine.physics()
    }

    fn physics_mut(&mut self) -> &mut Physics {
        self.engine.physics_mut()
    }

    fn should_close(&self) -> bool {
        self.engine.should_close()
    }

    fn close(&mut self) {
        self.engine.close()
    }

    fn on_event(&mut self, event: &Event<()>) {
        self.engine.on_event(event);

        if self.gui.editor_context.is_screen_editor_hovered()
            && self.engine.input().button("Mouse/Middle").down()
        {
            self.gui.editor_context.set_grab_input(true);
            self.engine
                .input_mut()
                .get_mut::<Mouse>("Mouse")
                .set_continuous(true);
        } else if self.engine.input().button("Mouse/Middle").up() {
            self.gui.editor_context.set_grab_input(false);
            self.engine
                .input_mut()
                .get_mut::<Mouse>("Mouse")
                .set_continuous(false);
        }

        if !self.gui.editor_context.is_grabbing_input() {
            self.gui.on_event(event);
        }
    }

    fn update(&mut self, frame: &mut Frame) {
        self.update_engine(frame);
        self.gui.update(&mut self.engine, frame);
    }

    fn config(&self) -> &Config {
        self.engine.config()
    }

    fn config_mut(&mut self) -> &mut Config {
        self.engine.config_mut()
    }
}

impl Editor {
    fn update_engine(&mut self, frame: &mut Frame) {
        self.engine
            .screens()
            .get_mut::<EditorScreen>("__internal_editor_screen__")
            .expect("editor screen")
            .handle_select(&mut self.gui.editor_context);
        self.gui.begin_screen_frame(frame);
        self.engine.update(frame);
        self.gui.end_screen_frame(frame);
    }
}

/// Type for building the editor.
pub struct EditorBuilder {
    editor_component_types: EditorComponentTypesBuilder,
    asset_types: EditorAssetTypesBuilder,
    screens: ScreensBuilder,
    component_types: ComponentTypesBuilder,
    assets: Option<PathBuf>,
    title: String,
}

impl EditorBuilder {
    /// Sets the title.
    pub fn title(mut self, title: impl Into<String>) -> Self {
        self.title = title.into();
        self
    }

    /// Sets the assets path.
    pub fn assets(mut self, path: impl Into<PathBuf>) -> Self {
        self.assets = Some(path.into());
        self
    }

    /// Registers a component type.
    pub fn component<C: ComponentExt + EditorComponentExt>(mut self) -> Self {
        self.component_types = self.component_types.component::<C>();
        self.editor_component_types = self.editor_component_types.component::<C>();
        self
    }

    /// Adds a screen.
    pub fn screen<S: ScreenExt + 'static>(mut self) -> Self {
        self.screens = self.screens.screen::<S>();
        self
    }

    /// Registers an asset type.
    pub fn asset<A: EditorAssetExt>(mut self) -> Self {
        self.asset_types = self.asset_types.asset::<A>();
        self
    }

    /// Builds the editor.
    pub fn build(self) -> Editor {
        let assets = self.assets.expect("asset path");
        let engine = EditorEngine::new(
            self.title,
            assets.clone(),
            self.component_types,
            self.screens,
        );
        let gui = Gui::new(
            &engine,
            assets,
            self.asset_types,
            self.editor_component_types,
        );

        Editor { engine, gui }
    }
}
