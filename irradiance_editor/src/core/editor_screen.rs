use std::{any::Any, sync::Arc};

use irradiance_runtime::physics::Physics;
use irradiance_runtime::{
    core::{Engine, Frame, Mode, Screen, ScreenExt},
    ecs::{Entities, SphericalCoordinate, Transform},
    gfx::Extent2D,
    math::{Quat, Vec3},
    rendering::Camera,
};

use crate::core::select_pipeline::SelectPipeline;
use crate::core::{EditorEngine, Selected};
use crate::ecs::ComponentDisplayPipeline;
use crate::gui::message::{Inspect, Message};
use crate::gui::EditorContext;

use super::{outline_pipeline::OutlinePipeline, Cameras};

pub struct EditorScreen {
    component_display_pipeline: ComponentDisplayPipeline,
    outline_pipeline: OutlinePipeline,
    select_pipeline: SelectPipeline,
    physics: Physics,
    camera_system: Cameras,
    entities: Arc<Entities>,
    hovered: bool,
}

impl EditorScreen {
    pub fn set_hovered(&mut self, hovered: bool) {
        self.hovered = hovered;
    }

    pub fn set_entities(&mut self, entities: Arc<Entities>) {
        self.entities = entities;
    }

    pub fn set_displays(&mut self, context: &EditorContext, engine: &EditorEngine) {
        self.select_pipeline
            .set_displays(context.component_types(), engine);
        self.outline_pipeline
            .set_displays(context.component_types(), engine);
        self.component_display_pipeline
            .set_displays(context.component_types(), engine);
    }

    pub fn resize(&mut self, extent: Extent2D) {
        self.select_pipeline.resize(extent);
        self.outline_pipeline.resize(extent);
    }

    pub fn handle_select(&mut self, context: &mut EditorContext) {
        if self.select_pipeline.executed() {
            if let Some(selected) = self.select_pipeline.selected(&self.entities) {
                context
                    .message_bus_mut()
                    .post(Message::Inspect(Inspect::Entity(
                        selected.id().to_string(),
                        self.entities.clone(),
                    )));
            } else {
                self.entities
                    .active::<Selected>()
                    .map(|entity| entity.remove::<Selected>());
            }
        }
    }
}

impl Screen for EditorScreen {
    fn name(&self) -> &'static str {
        "__internal_editor_screen__"
    }

    fn mode(&self) -> Mode {
        Mode::Opaque
    }

    fn entities(&self) -> Option<Arc<Entities>> {
        Some(self.entities.clone())
    }

    fn on_enter(&mut self, engine: &mut dyn Engine) {
        *engine.physics_mut() =
            Physics::new(engine.graphics_context().clone(), engine.assets().clone());

        if let Ok(camera) = self.entities.create("__internal_editor_camera__") {
            camera.insert(Transform::new(
                Vec3::new(0.0, 0.0, 20.0),
                Quat::default(),
                Vec3::new(1.0, 1.0, 1.0),
            ));
            camera.insert(SphericalCoordinate::forward(5.0));
            camera.insert(Camera::new(45f32.to_radians(), 0.1, 1000.0));

            unsafe { self.entities.commit() };
        }

        self.entities
            .get("__internal_editor_camera__")
            .expect("internal camera")
            .activate::<Camera>();
    }

    fn update(&mut self, engine: &mut dyn Engine, frame: &mut Frame) {
        if self.hovered && engine.input().button("Mouse/Left").pressed() {
            let cursor_position = [
                engine.input().axis("Mouse/Horizontal").get() as u32,
                engine.input().axis("Mouse/Vertical").get() as u32,
            ];
            self.select_pipeline
                .draw(frame.builder_mut(), &self.entities, cursor_position)
                .expect("valid frame");
        }

        self.camera_system.update(engine, frame, &self.entities);
        self.physics.update_no_step(&self.entities);
        engine
            .animations()
            .update(engine, frame.time(), &self.entities);

        unsafe { self.entities.commit() };
    }

    fn draw(&mut self, engine: &mut dyn Engine, frame: &mut Frame) {
        let swapchain_image = frame.swapchain_image();
        engine
            .buf_pbr_pipeline()
            .draw(frame.builder_mut(), swapchain_image.clone(), &self.entities)
            .expect("valid frame");
        self.physics
            .draw(
                engine.config().clone(),
                engine.buf_pbr_pipeline().clone(),
                frame.builder_mut(),
                swapchain_image.clone(),
                &self.entities,
            )
            .expect("valid frame");

        self.outline_pipeline
            .draw(frame.builder_mut(), swapchain_image.clone(), &self.entities)
            .expect("valid frame");

        self.component_display_pipeline
            .draw(
                frame.builder_mut(),
                engine.buf_pbr_pipeline().g_buffer(),
                swapchain_image,
                &self.entities,
            )
            .expect("valid frame");
    }

    fn on_leave(&mut self, _engine: &mut dyn Engine) {}

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

impl ScreenExt for EditorScreen {
    fn new(engine: &dyn Engine) -> Self {
        Self {
            component_display_pipeline: ComponentDisplayPipeline::new(
                engine.graphics_context().clone(),
            )
            .expect("component display pipeline"),
            outline_pipeline: OutlinePipeline::new(engine.graphics_context().clone())
                .expect("outline pipeline"),
            select_pipeline: SelectPipeline::new(engine.graphics_context().clone())
                .expect("entity id pass"),
            physics: Physics::new(engine.graphics_context().clone(), engine.assets().clone()),
            camera_system: Cameras::default(),
            entities: Entities::new(),
            hovered: false,
        }
    }
}
