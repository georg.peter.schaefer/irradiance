struct Selection {
    uint upper;
    uint lower;
    uint _dummy0;
    uint _dummy1;
};

struct Mouse {
    uint x;
    uint y;
};

[[vk::binding(0, 0)]]
RWTexture2D<uint2> entity_id_buffer;
[[vk::binding(1, 0)]]
RWStructuredBuffer<Selection> selection;

[[vk::push_constant]]
cbuffer mouse {
    Mouse mouse;
}

[numthreads(1, 1, 1)]
void main(uint3 GlobalInvocationID : SV_DispatchThreadID) {
    uint width = 0;
    uint height = 0;
    entity_id_buffer.GetDimensions(width, height);

    if (mouse.x >= 0 && mouse.x < width && mouse.y >= 0 && mouse.y < height) {
        uint2 index = uint2(mouse.x, mouse.y);
        selection[0].upper = entity_id_buffer[index].x;
        selection[0].lower = entity_id_buffer[index].y;
    } else {
        selection[0].upper = 0;
        selection[0].lower = 0;
    }
}
