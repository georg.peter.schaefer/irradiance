struct FSOutput {
    float silhouette : SV_TARGET0;
};

FSOutput main() {
    FSOutput output = (FSOutput)0;

    output.silhouette = 1.0;

    return output;
}