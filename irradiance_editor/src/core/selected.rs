use std::marker::PhantomData;

use irradiance_runtime::{ecs::Active, Component};
use serde::Serialize;

#[derive(Default, Serialize, Copy, Clone, Component)]
#[serde(tag = "type_name")]
pub struct Selected {
    #[serde(skip_serializing, skip_deserializing)]
    _phantom_data: PhantomData<()>,
}

impl Active for Selected {}
