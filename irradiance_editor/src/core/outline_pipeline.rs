use std::any::TypeId;
use std::collections::HashMap;
use std::sync::Arc;

use crate::core::EditorEngine;
use crate::ecs::EditorComponentTypes;
use irradiance_runtime::gfx::device::DeviceOwned;
use irradiance_runtime::gfx::pipeline::WriteDescriptorSetBuffers;
use irradiance_runtime::rendering::{mesh, skinned_mesh, AnimatedMesh, Mesh};
use irradiance_runtime::{
    ecs::{Entities, Transform},
    gfx::{
        buffer::Buffer,
        command_buffer::{CommandBufferBuilder, IndexType},
        device::Device,
        image::{
            BorderColor, Format, Image, ImageAspects, ImageLayout, ImageSubresourceRange,
            ImageType, ImageUsage, ImageView, ImageViewType, Sampler, SamplerAddressMode,
        },
        pipeline::{
            ColorBlendAttachmentState, CullMode, DescriptorSet, DescriptorSetLayout,
            DescriptorType, DynamicState, FrontFace, GraphicsPipeline, PipelineBindPoint,
            PipelineLayout, PrimitiveTopology, ShaderStages, VertexInput, VertexInputRate,
            Viewport, WriteDescriptorSetImages,
        },
        render_pass::{Attachment, AttachmentLoadOp, AttachmentStoreOp, ClearValue, RenderPass},
        sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages},
        Extent2D, GfxError, GraphicsContext, Rect2D,
    },
    math::Mat4,
    rendering::{
        fullscreen_pass::{self, create_fullscreen_vertex_buffer},
        Camera, StaticMesh,
    },
};

use self::shaders::PushConstants;

use super::Selected;

pub struct OutlinePipeline {
    outline_pass: OutlinePass,
    silhouette_buffer: Arc<ImageView>,
    silhouette_pass: SilhouettePass,
}

impl OutlinePipeline {
    pub fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let outline_pipeline = Self {
            silhouette_pass: SilhouettePass::new(graphics_context.clone())?,
            silhouette_buffer: Self::create_silhouette_buffer(
                graphics_context.clone(),
                Extent2D {
                    width: 100,
                    height: 100,
                },
            ),
            outline_pass: OutlinePass::new(graphics_context)?,
        };

        outline_pipeline
            .silhouette_buffer
            .image()
            .set_debug_utils_object_name("Outline Pipeline Silhouette Buffer Image");
        outline_pipeline
            .silhouette_buffer
            .set_debug_utils_object_name("Outline Pipeline Silhouette Buffer Image View");

        Ok(outline_pipeline)
    }

    /// Sets the component displays.
    pub fn set_displays(&mut self, component_types: &EditorComponentTypes, engine: &EditorEngine) {
        self.silhouette_pass.displays = component_types
            .types()
            .flat_map(|component_type| {
                component_types
                    .display(component_type.clone(), engine)
                    .map(|display| (component_type, display))
            })
            .collect();
    }

    pub fn resize(&mut self, extent: Extent2D) {
        self.silhouette_buffer =
            Self::create_silhouette_buffer(self.silhouette_pass.graphics_context.clone(), extent);
    }

    pub fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        final_image: Arc<ImageView>,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        if entities.active::<Selected>().is_some() {
            self.silhouette_pass
                .draw(builder, self.silhouette_buffer.clone(), entities)?;
            self.outline_pass
                .draw(builder, self.silhouette_buffer.clone(), final_image)?;
        }

        Ok(())
    }

    fn create_silhouette_buffer(
        graphics_context: Arc<GraphicsContext>,
        extent: Extent2D,
    ) -> Arc<ImageView> {
        ImageView::builder(
            graphics_context.device().clone(),
            Image::builder(graphics_context.device().clone())
                .image_type(ImageType::Type2D)
                .format(Format::R16SFloat)
                .extent(extent.into())
                .usage(ImageUsage {
                    color_attachment: true,
                    sampled: true,
                    ..Default::default()
                })
                .build()
                .expect("silhouette buffer"),
        )
        .view_type(ImageViewType::Type2D)
        .build()
        .expect("silhouette buffer")
    }
}

struct SilhouettePass {
    animated_graphics_pipeline: Arc<GraphicsPipeline>,
    static_graphics_pipeline: Arc<GraphicsPipeline>,
    animated_pipeline_layout: Arc<PipelineLayout>,
    static_pipeline_layout: Arc<PipelineLayout>,
    joint_descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
    displays: HashMap<TypeId, Mesh>,
}

impl SilhouettePass {
    fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let joint_descriptor_set_layout =
            Self::create_joint_descriptor_set_layout(graphics_context.device().clone())?;
        let static_pipeline_layout =
            Self::create_static_pipeline_layout(graphics_context.device().clone())?;
        let animated_pipeline_layout = Self::create_animated_pipeline_layout(
            graphics_context.device().clone(),
            joint_descriptor_set_layout.clone(),
        )?;
        let static_graphics_pipeline = Self::create_static_graphics_pipeline(
            graphics_context.device().clone(),
            static_pipeline_layout.clone(),
        )?;
        let animated_graphics_pipeline = Self::create_animated_graphics_pipeline(
            graphics_context.device().clone(),
            animated_pipeline_layout.clone(),
        )?;

        joint_descriptor_set_layout
            .set_debug_utils_object_name("Silhouette Pass Joint Descriptor Set Layout");
        static_pipeline_layout
            .set_debug_utils_object_name("Silhouette Pass Static Pipeline Layout");
        animated_pipeline_layout
            .set_debug_utils_object_name("Silhouette Pass Animated Pipeline Layout");
        static_graphics_pipeline
            .set_debug_utils_object_name("Silhouette Pass Static Graphics Pipeline");
        animated_graphics_pipeline
            .set_debug_utils_object_name("Silhouette Pass Animated Graphics Pipeline");

        Ok(Self {
            animated_graphics_pipeline,
            static_graphics_pipeline,
            animated_pipeline_layout,
            static_pipeline_layout,
            joint_descriptor_set_layout,
            graphics_context,
            displays: Default::default(),
        })
    }

    fn create_joint_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::StorageBuffer,
                1,
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_static_pipeline_layout(device: Arc<Device>) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_animated_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .push_constant_range(
                ShaderStages {
                    vertex: true,
                    ..Default::default()
                },
                0,
                std::mem::size_of::<PushConstants>() as _,
            )
            .build()
    }

    fn create_static_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .build(),
            )
            .vertex_shader(
                static_silhouette_vertex_shader::Shader::load(device.clone())?,
                "main",
            )
            .fragment_shader(silhouette_fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R16SFloat)
            .build()
    }

    fn create_animated_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<skinned_mesh::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32B32SFloat, 0)
                    .attribute(1, 0, Format::R32G32B32A32SFloat, 48)
                    .attribute(2, 0, Format::R32G32B32A32SFloat, 64)
                    .build(),
            )
            .vertex_shader(
                animated_silhouette_vertex_shader::Shader::load(device.clone())?,
                "main",
            )
            .fragment_shader(silhouette_fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(Format::R16SFloat)
            .build()
    }

    fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        silhouette_buffer: Arc<ImageView>,
        entities: &Entities,
    ) -> Result<(), GfxError> {
        builder
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(silhouette_buffer.image().clone())
                            .src_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::Undefined)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .bind_graphics_pipeline(self.static_graphics_pipeline.clone())
            .begin_rendering(self.create_render_pass(silhouette_buffer.clone()))
            .set_viewport(
                0,
                vec![Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: silhouette_buffer.image().extent().width as _,
                    height: silhouette_buffer.image().extent().height as _,
                    min_depth: 0.0,
                    max_depth: 1.0,
                }],
            )
            .set_scissor(
                0,
                vec![Rect2D {
                    offset: Default::default(),
                    extent: silhouette_buffer.image().extent().into(),
                }],
            );

        if let Some((Some(camera), Some(camera_transform))) = entities
            .active::<Camera>()
            .map(|camera| (camera.get::<Camera>(), camera.get::<Transform>()))
        {
            if let Some(selected) = entities.active::<Selected>() {
                if let Some(transform) = selected.get::<Transform>() {
                    let static_mesh = selected.get::<StaticMesh>();
                    if let Some(mesh) = static_mesh
                        .as_ref()
                        .and_then(|static_mesh| static_mesh.mesh().ok())
                        .or_else(|| {
                            selected
                                .types()
                                .iter()
                                .flat_map(|component_type| self.displays.get(component_type))
                                .next()
                        })
                    {
                        builder
                            .bind_vertex_buffer(mesh.vertex_buffer().clone())
                            .bind_index_buffer(mesh.index_buffer().clone(), 0, IndexType::UInt32);

                        for sub_mesh in mesh.sub_meshes() {
                            builder
                                .push_constants(
                                    self.static_pipeline_layout.clone(),
                                    ShaderStages {
                                        vertex: true,
                                        ..Default::default()
                                    },
                                    0,
                                    PushConstants {
                                        projection: camera.projection(
                                            silhouette_buffer.image().extent().width as f32
                                                / silhouette_buffer.image().extent().height as f32,
                                        ),
                                        view: Mat4::from(*camera_transform).inverse(),
                                        model: Mat4::from(*transform),
                                    },
                                )
                                .draw_indexed(
                                    sub_mesh.count() as _,
                                    1,
                                    sub_mesh.offset() as _,
                                    0,
                                    0,
                                );
                        }
                    }
                }

                if let (Some(animated_mesh), Some(transform)) =
                    (selected.get::<AnimatedMesh>(), selected.get::<Transform>())
                {
                    if let Some(mesh) = animated_mesh.mesh().ok() {
                        if let Some(joint_buffer) = animated_mesh.joint_buffer() {
                            let joint_descriptor_set =
                                self.create_joint_descriptor_set(joint_buffer)?;
                            builder
                                .bind_graphics_pipeline(self.animated_graphics_pipeline.clone())
                                .bind_vertex_buffer(mesh.vertex_buffer().clone())
                                .bind_index_buffer(
                                    mesh.index_buffer().clone(),
                                    0,
                                    IndexType::UInt32,
                                )
                                .bind_descriptor_sets(
                                    PipelineBindPoint::Graphics,
                                    self.animated_pipeline_layout.clone(),
                                    0,
                                    vec![joint_descriptor_set],
                                );

                            for sub_mesh in mesh.sub_meshes() {
                                builder
                                    .push_constants(
                                        self.animated_pipeline_layout.clone(),
                                        ShaderStages {
                                            vertex: true,
                                            ..Default::default()
                                        },
                                        0,
                                        PushConstants {
                                            projection: camera.projection(
                                                silhouette_buffer.image().extent().width as f32
                                                    / silhouette_buffer.image().extent().height
                                                        as f32,
                                            ),
                                            view: Mat4::from(*camera_transform).inverse(),
                                            model: Mat4::from(*transform),
                                        },
                                    )
                                    .draw_indexed(
                                        sub_mesh.count() as _,
                                        1,
                                        sub_mesh.offset() as _,
                                        0,
                                        0,
                                    );
                            }
                        }
                    }
                }
            }
        }

        builder.end_rendering();

        Ok(())
    }

    fn create_render_pass(&self, silhouette_buffer: Arc<ImageView>) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: silhouette_buffer.image().extent().into(),
            })
            .color_attachment(
                Attachment::builder(silhouette_buffer)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Clear)
                    .store_op(AttachmentStoreOp::Store)
                    .clear_value(ClearValue::Int([0, 0, 0, 255]))
                    .build(),
            )
            .build()
    }

    fn create_joint_descriptor_set(
        &self,
        joint_buffer: Arc<Buffer>,
    ) -> Result<Arc<DescriptorSet>, GfxError> {
        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.joint_descriptor_set_layout)?,
            self.joint_descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Silhouette Pass Animated Descriptor Set");

        descriptor_set.write_buffers(vec![WriteDescriptorSetBuffers::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::StorageBuffer)
            .buffer(&joint_buffer, 0, !0)
            .build()]);

        Ok(descriptor_set)
    }
}

mod shaders {
    use irradiance_runtime::math::Mat4;

    #[allow(unused)]
    #[derive(Debug)]
    #[repr(C)]
    pub struct PushConstants {
        pub projection: Mat4,
        pub view: Mat4,
        pub model: Mat4,
    }
}

mod static_silhouette_vertex_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/core/static_silhouette.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod animated_silhouette_vertex_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/core/animated_silhouette.vert.hlsl",
        ty: "vertex",
        entry_point: "main"
    }
}

mod silhouette_fragment_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/core/silhouette.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}

struct OutlinePass {
    sampler: Arc<Sampler>,
    fullscreen_vertex_buffer: Arc<Buffer>,
    graphics_pipeline: Arc<GraphicsPipeline>,
    pipeline_layout: Arc<PipelineLayout>,
    descriptor_set_layout: Arc<DescriptorSetLayout>,
    graphics_context: Arc<GraphicsContext>,
}

impl OutlinePass {
    fn new(graphics_context: Arc<GraphicsContext>) -> Result<Self, GfxError> {
        let descriptor_set_layout =
            Self::create_descriptor_set_layout(graphics_context.device().clone())?;
        let pipeline_layout = Self::create_pipeline_layout(
            graphics_context.device().clone(),
            descriptor_set_layout.clone(),
        )?;
        let graphics_pipeline = Self::create_graphics_pipeline(
            graphics_context.device().clone(),
            pipeline_layout.clone(),
            graphics_context.swapchain().unwrap().image_format(),
        )?;
        let fullscreen_vertex_buffer = create_fullscreen_vertex_buffer(&graphics_context)?;
        let sampler = Sampler::builder(graphics_context.device().clone())
            .address_mode_u(SamplerAddressMode::ClampToBorder)
            .address_mode_v(SamplerAddressMode::ClampToBorder)
            .address_mode_w(SamplerAddressMode::ClampToBorder)
            .border_color(BorderColor::FloatOpaqueBlack)
            .build()?;

        descriptor_set_layout.set_debug_utils_object_name("Outline Pass Descriptor Set Layout");
        pipeline_layout.set_debug_utils_object_name("Outline Pass Pipeline Layout");
        graphics_pipeline.set_debug_utils_object_name("Outline Pass Graphics Pipeline");
        fullscreen_vertex_buffer
            .set_debug_utils_object_name("Outline Pass Fullscreen Vertex Buffer");
        sampler.set_debug_utils_object_name("Outline Pass Sampler");

        Ok(Self {
            sampler,
            fullscreen_vertex_buffer,
            graphics_pipeline,
            pipeline_layout,
            descriptor_set_layout,
            graphics_context,
        })
    }

    fn create_descriptor_set_layout(
        device: Arc<Device>,
    ) -> Result<Arc<DescriptorSetLayout>, GfxError> {
        DescriptorSetLayout::builder(device)
            .binding(
                0,
                DescriptorType::CombinedImageSampler,
                1,
                ShaderStages {
                    fragment: true,
                    ..Default::default()
                },
            )
            .build()
    }

    fn create_pipeline_layout(
        device: Arc<Device>,
        descriptor_set_layout: Arc<DescriptorSetLayout>,
    ) -> Result<Arc<PipelineLayout>, GfxError> {
        PipelineLayout::builder(device)
            .set_layout(descriptor_set_layout)
            .build()
    }

    fn create_graphics_pipeline(
        device: Arc<Device>,
        pipeline_layout: Arc<PipelineLayout>,
        format: Format,
    ) -> Result<Arc<GraphicsPipeline>, GfxError> {
        GraphicsPipeline::builder(device.clone())
            .vertex_input(
                VertexInput::builder()
                    .binding(
                        0,
                        std::mem::size_of::<fullscreen_pass::Vertex>() as _,
                        VertexInputRate::Vertex,
                    )
                    .attribute(0, 0, Format::R32G32SFloat, 0)
                    .attribute(1, 0, Format::R32G32SFloat, 8)
                    .build(),
            )
            .vertex_shader(
                fullscreen_pass::vertex_shader::Shader::load(device.clone())?,
                "main",
            )
            .fragment_shader(outline_fragment_shader::Shader::load(device)?, "main")
            .topology(PrimitiveTopology::TriangleList)
            .viewport(Viewport {
                x: 0.0,
                y: 0.0,
                width: 1024.0,
                height: 786.0,
                min_depth: 0.0,
                max_depth: 1.0,
            })
            .scissor(Rect2D {
                offset: Default::default(),
                extent: Extent2D {
                    width: 1024,
                    height: 768,
                },
            })
            .dynamic_state(DynamicState::Viewport)
            .dynamic_state(DynamicState::Scissor)
            .cull_mode(CullMode::Back)
            .front_face(FrontFace::CounterClockwise)
            .color_blend_attachment_state(ColorBlendAttachmentState::builder().build())
            .layout(pipeline_layout)
            .color_attachment_format(format)
            .build()
    }

    fn draw(
        &self,
        builder: &mut CommandBufferBuilder,
        silhouette_buffer: Arc<ImageView>,
        final_image: Arc<ImageView>,
    ) -> Result<(), GfxError> {
        let descriptor_set = DescriptorSet::builder(
            self.graphics_context.device().clone(),
            self.graphics_context
                .descriptor_pool(&self.descriptor_set_layout)?,
            self.descriptor_set_layout.clone(),
        )
        .build()?;
        descriptor_set.set_debug_utils_object_name("Outline Pass Descriptor Set");

        descriptor_set.write_images(vec![WriteDescriptorSetImages::builder()
            .dst_binding(0)
            .descriptor_type(DescriptorType::CombinedImageSampler)
            .sampled_image(
                &self.sampler,
                &silhouette_buffer,
                ImageLayout::ShaderReadOnlyOptimal,
            )
            .build()]);

        builder
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(silhouette_buffer.image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                fragment_shader: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                shader_sampled_read: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .pipeline_barrier(
                PipelineBarrier::builder()
                    .image_memory_barrier(
                        ImageMemoryBarrier::builder(final_image.image().clone())
                            .src_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .src_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .dst_stage_mask(PipelineStages {
                                color_attachment_output: true,
                                ..Default::default()
                            })
                            .dst_access_mask(AccessMask {
                                color_attachment_write: true,
                                ..Default::default()
                            })
                            .old_layout(ImageLayout::ColorAttachmentOptimal)
                            .new_layout(ImageLayout::ColorAttachmentOptimal)
                            .subresource_range(
                                ImageSubresourceRange::builder()
                                    .aspect_mask(ImageAspects {
                                        color: true,
                                        ..Default::default()
                                    })
                                    .build(),
                            )
                            .build(),
                    )
                    .build(),
            )
            .bind_graphics_pipeline(self.graphics_pipeline.clone())
            .begin_rendering(self.create_render_pass(final_image.clone()))
            .set_viewport(
                0,
                vec![Viewport {
                    x: 0.0,
                    y: 0.0,
                    width: final_image.image().extent().width as _,
                    height: final_image.image().extent().height as _,
                    min_depth: 0.0,
                    max_depth: 1.0,
                }],
            )
            .set_scissor(
                0,
                vec![Rect2D {
                    offset: Default::default(),
                    extent: final_image.image().extent().into(),
                }],
            )
            .bind_vertex_buffer(self.fullscreen_vertex_buffer.clone())
            .bind_descriptor_sets(
                PipelineBindPoint::Graphics,
                self.pipeline_layout.clone(),
                0,
                vec![descriptor_set],
            )
            .draw(6, 1, 0, 0)
            .end_rendering();

        Ok(())
    }

    fn create_render_pass(&self, final_image: Arc<ImageView>) -> RenderPass {
        RenderPass::builder()
            .render_area(Rect2D {
                offset: Default::default(),
                extent: final_image.image().extent().into(),
            })
            .color_attachment(
                Attachment::builder(final_image)
                    .image_layout(ImageLayout::ColorAttachmentOptimal)
                    .load_op(AttachmentLoadOp::Load)
                    .store_op(AttachmentStoreOp::Store)
                    .build(),
            )
            .build()
    }
}

mod outline_fragment_shader {
    use irradiance_runtime::shader;

    shader! {
        path: "src/core/outline.frag.hlsl",
        ty: "fragment",
        entry_point: "main"
    }
}
