[[vk::binding(0, 0)]]
Texture2D silhouette_buffer_texture;
[[vk::binding(0, 0)]]
SamplerState silhouette_buffer_sampler;

struct VSOutput {
    [[vk::location(0)]] float2 tex_coord : TEXCOORD0;
};

const float4 COLOR = float4(0.86, 0.36, 0.0, 1.0);
const int DEPTH = 2;

float4 main(VSOutput input) : SV_TARGET {
    float width;
    float height;
    silhouette_buffer_texture.GetDimensions(width, height);
    float2 texel_size = float2(1.0 / width, 1.0 / height);

    float silhouette = silhouette_buffer_texture.Sample(silhouette_buffer_sampler, input.tex_coord).r;
    if (silhouette > 0.5) {
        discard;
    } else {
        for (int x = -DEPTH; x <= DEPTH; x++) {
            for (int y = -DEPTH; y <= DEPTH; y++) {
                float2 offset = texel_size * float2(x, y);
                float silhouette = silhouette_buffer_texture.Sample(silhouette_buffer_sampler, input.tex_coord + offset).r;
                if (silhouette > 0.0) {
                    return COLOR;
                }
            }
        }

        discard;
    }
}
