use std::sync::{Arc, Mutex, MutexGuard};

use chrono::Local;
use fern::{colors::ColoredLevelConfig, Dispatch};
use log::LevelFilter;

/// A log.
pub struct BufLog {
    buffer: Mutex<Vec<String>>,
}

impl BufLog {
    /// Creates a new buffered log.
    pub fn new() -> Arc<Self> {
        let log = Arc::new(Self {
            buffer: Default::default(),
        });

        let move_log = log.clone();
        let colors = ColoredLevelConfig::new();
        Dispatch::new()
            .level(LevelFilter::Debug)
            .chain(
                Dispatch::new()
                    .format(move |out, message, record| {
                        out.finish(format_args!(
                            "{}[{}][{}] {}",
                            Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                            record.target(),
                            colors.color(record.level()),
                            message
                        ))
                    })
                    .chain(std::io::stdout()),
            )
            .chain(
                Dispatch::new()
                    .format(move |out, message, record| {
                        out.finish(format_args!(
                            "{}[{}][{}] {}",
                            Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                            record.target(),
                            record.level(),
                            message
                        ))
                    })
                    .chain(fern::Output::call(move |record| {
                        move_log.log(format!("{}", record.args()))
                    })),
            )
            .apply()
            .expect("initialized log");

        log
    }

    fn log(&self, message: String) {
        let mut buffer = self.buffer.lock().unwrap();
        buffer.push(message);
    }

    /// Returns access to the buffer.
    pub fn buffer(&self) -> MutexGuard<'_, Vec<String>> {
        self.buffer.lock().unwrap()
    }
}
