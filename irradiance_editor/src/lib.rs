#![warn(missing_docs)]

//! Editor

pub extern crate egui;

pub mod asset;
pub mod core;
pub mod ecs;
pub mod gui;
