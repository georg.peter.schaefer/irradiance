use std::{ffi::OsStr, path::Path, sync::Arc};

use egui::{style::Margin, Color32, Frame, Grid, PaintCallback, Rect, RichText, TextureId, Ui};
use irradiance_runtime::gfx::command_buffer::CommandBufferBuilder;
use irradiance_runtime::gfx::device::DeviceOwned;
use irradiance_runtime::rendering::pbr::{BloomBuffer, SsaoBuffer};
use irradiance_runtime::{
    asset::{Asset, AssetRef, Assets},
    core::Engine,
    ecs::{Entities, Transform},
    gfx::{
        image::{
            Format, Image, ImageAspects, ImageLayout, ImageSubresourceRange, ImageType, ImageUsage,
            ImageView, ImageViewType,
        },
        sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages},
        Extent2D, GraphicsContext,
    },
    math::{Mat4, Vec2, Vec3, Vec4},
    rendering::{pbr::GBuffer, Camera, DirectionalLight, Material, Mesh, StaticMesh, Texture},
};

use crate::gui::CallbackFn;
use crate::{
    asset::EditorAssetExt,
    core::EditorEngine,
    gui::{
        message::{Message, Recv, Repaint},
        widgets::{AssetSlot, ColorPicker, Slider, Widget},
        Action, EditorContext, Icon, Icons,
    },
};

/// Displays a material.
pub struct MaterialDisplay {
    texture_id: Option<TextureId>,
    entities: Arc<Entities>,
    final_image: Arc<ImageView>,
    bloom_buffer: Arc<BloomBuffer>,
    ssao_buffer: Arc<SsaoBuffer>,
    light_buffer: Arc<ImageView>,
    g_buffer: Arc<GBuffer>,
    size: Vec2,
    state: MaterialDisplayState,
    repaint: bool,
}

impl MaterialDisplay {
    /// Creates a new material display.
    pub fn new(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        material: AssetRef<Material>,
        size: Vec2,
    ) -> Self {
        let device = graphics_context.device();
        let extent = Extent2D {
            width: size[0] as _,
            height: size[1] as _,
        };

        let entities = Entities::new();

        let sun = entities.create("sun").unwrap();
        sun.insert(Transform::new(
            Default::default(),
            Mat4::look_at(
                Vec3::new(1.0, 1.0, 1.0),
                Vec3::zero(),
                Vec3::new(0.0, 1.0, 0.0),
            )
            .inverse()
            .into(),
            Vec3::new(1.0, 1.0, 1.0),
        ));
        sun.insert(DirectionalLight::new(Vec3::new(1.0, 1.0, 1.0), 2.0));

        let camera = entities.create("camera").unwrap();
        camera.insert(Transform::new(
            Vec3::new(0.0, 0.0, 2.8),
            Mat4::look_at(
                Vec3::new(0.0, 0.0, 2.8),
                Vec3::zero(),
                Vec3::new(0.0, 1.0, 0.0),
            )
            .inverse()
            .into(),
            Vec3::new(1.0, 1.0, 1.0),
        ));
        camera.insert(Camera::new(45f32.to_radians(), 0.1, 100.0));
        camera.activate::<Camera>();

        let sphere = entities.create("sphere").unwrap();
        sphere.insert(Transform::new(
            Default::default(),
            Default::default(),
            Vec3::new(1.0, 1.0, 1.0),
        ));
        let mut mesh = Mesh::try_from_bytes(
            assets,
            graphics_context.clone(),
            Path::new(""),
            include_bytes!("sphere.gltf").to_vec(),
        );
        if let Ok(mesh) = mesh.as_mut() {
            for sub_mesh in mesh.sub_meshes_mut() {
                sub_mesh.set_material(material.clone());
            }
        }
        sphere.insert(StaticMesh::new(AssetRef::new_pre_read(mesh)));

        unsafe {
            entities.commit();
        }

        let material_display = Self {
            repaint: true,
            state: MaterialDisplayState::new(material),
            size,
            g_buffer: Arc::new(GBuffer::new(device.clone(), extent).expect("g buffer")),
            light_buffer: ImageView::builder(
                device.clone(),
                Image::builder(device.clone())
                    .image_type(ImageType::Type2D)
                    .format(Format::R32G32B32A32SFloat)
                    .extent(extent.into())
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()
                    .expect("light buffer"),
            )
            .view_type(ImageViewType::Type2D)
            .build()
            .expect("light buffer image view"),
            ssao_buffer: Arc::new(SsaoBuffer::new(device.clone(), extent).expect("ssao buffer")),
            bloom_buffer: Arc::new(BloomBuffer::new(device.clone(), extent).expect("bloom buffer")),
            final_image: ImageView::builder(
                device.clone(),
                Image::builder(device.clone())
                    .image_type(ImageType::Type2D)
                    .format(graphics_context.swapchain().unwrap().image_format())
                    .extent(extent.into())
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()
                    .expect("final image"),
            )
            .view_type(ImageViewType::Type2D)
            .build()
            .expect("final image view"),
            entities,
            texture_id: None,
        };

        material_display
            .light_buffer
            .image()
            .set_debug_utils_object_name("Material Display Light Buffer Image");
        material_display
            .light_buffer
            .set_debug_utils_object_name("Material Display Light Buffer Image View");
        material_display
            .final_image
            .image()
            .set_debug_utils_object_name("Material Display Final Image");
        material_display
            .final_image
            .set_debug_utils_object_name("Material Display Final Image View");

        material_display
    }
}

impl Widget for MaterialDisplay {
    fn handle_messages(&mut self, context: &mut EditorContext, _engine: &mut EditorEngine) {
        match context.message_bus().recv() {
            Some(Repaint::Asset(path)) if Some(path) == self.state.material.path().as_ref() => {
                self.repaint = true
            }
            _ => {}
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.handle_repaint(ui, engine);

        let texture_id = self
            .texture_id
            .get_or_insert_with(|| context.register_texture(true, self.final_image.clone()));
        ui.image(*texture_id, egui::Vec2::new(self.size[0], self.size[1]));
    }
}

impl MaterialDisplay {
    fn handle_repaint(&mut self, ui: &mut Ui, engine: &mut EditorEngine) {
        if self.state.requires_repaint() {
            self.repaint = true;
        }

        if self.repaint {
            self.paint(ui, engine);
            self.repaint = false;
        }
    }

    fn paint(&mut self, ui: &mut Ui, engine: &mut EditorEngine) {
        let g_buffer = self.g_buffer.clone();
        let light_buffer = self.light_buffer.clone();
        let ssao_buffer = self.ssao_buffer.clone();
        let bloom_buffer = self.bloom_buffer.clone();
        let final_image = self.final_image.clone();
        let entities = self.entities.clone();
        let pbr_pipeline = engine.buf_pbr_pipeline().pbr_pipeline().clone();

        ui.painter().add(PaintCallback {
            rect: Rect::NOTHING,
            callback: Arc::new(CallbackFn::from(
                move |builder: &mut CommandBufferBuilder| {
                    pbr_pipeline
                        .draw(
                            builder,
                            &g_buffer,
                            light_buffer.clone(),
                            &ssao_buffer,
                            &bloom_buffer,
                            final_image.clone(),
                            Vec4::new(0.0, 0.0, 0.0, 1.0),
                            &entities,
                        )
                        .unwrap();
                    builder.pipeline_barrier(
                        PipelineBarrier::builder()
                            .image_memory_barrier(
                                ImageMemoryBarrier::builder(final_image.image().clone())
                                    .src_stage_mask(PipelineStages {
                                        color_attachment_output: true,
                                        ..Default::default()
                                    })
                                    .src_access_mask(AccessMask {
                                        color_attachment_write: true,
                                        ..Default::default()
                                    })
                                    .dst_stage_mask(PipelineStages {
                                        fragment_shader: true,
                                        ..Default::default()
                                    })
                                    .dst_access_mask(AccessMask {
                                        shader_sampled_read: true,
                                        ..Default::default()
                                    })
                                    .old_layout(ImageLayout::ColorAttachmentOptimal)
                                    .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                                    .subresource_range(
                                        ImageSubresourceRange::builder()
                                            .aspect_mask(ImageAspects {
                                                color: true,
                                                ..Default::default()
                                            })
                                            .build(),
                                    )
                                    .build(),
                            )
                            .build(),
                    );
                },
            )),
        });
    }
}

/// Inspects a material.
pub struct MaterialInspector {
    metallic_roughness_texture: AssetSlot<Texture>,
    normal_texture: AssetSlot<Texture>,
    base_color_texture: AssetSlot<Texture>,
    roughness: Slider<f32>,
    metallic: Slider<f32>,
    base_color: ColorPicker,
    material_display: MaterialDisplay,
    material: AssetRef<Material>,
    loaded: bool,
}

impl MaterialInspector {
    /// Creates a new material inspector.
    pub fn new(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        material: AssetRef<Material>,
    ) -> Self {
        Self {
            loaded: false,
            material: material.clone(),
            material_display: MaterialDisplay::new(
                graphics_context.clone(),
                assets.clone(),
                material,
                Vec2::new(150.0, 150.0),
            ),
            base_color: Default::default(),
            metallic: Slider::new(0.0..=1.0),
            roughness: Slider::new(0.0..=1.0),
            base_color_texture: AssetSlot::new(graphics_context.clone(), assets.clone()),
            normal_texture: AssetSlot::new(graphics_context.clone(), assets.clone()),
            metallic_roughness_texture: AssetSlot::new(graphics_context, assets),
        }
    }
}

impl Widget for MaterialInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.material_display.handle_messages(context, engine);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(material) = self.material.ok().filter(|_| !self.loaded) {
            self.base_color.set(material.base_color());
            self.metallic.set(material.metallic());
            self.roughness.set(material.roughness());
            self.base_color_texture
                .set(material.base_color_texture().clone());
            self.normal_texture.set(material.normal_texture().clone());
            self.metallic_roughness_texture
                .set(material.metallic_roughness_texture().clone());
            self.loaded = true;
        }

        Frame::default().fill(Color32::BLACK).show(ui, |ui| {
            ui.vertical_centered(|ui| {
                self.material_display.ui(ui, context, engine);
            });
        });
        Frame::default()
            .inner_margin(Margin::same(15.0))
            .show(ui, |ui| {
                Grid::new("material_properties")
                    .num_columns(2)
                    .spacing([40.0, 4.0])
                    .show(ui, |ui| {
                        ui.label("Name");
                        ui.label(RichText::new(self.file_stem()).strong());
                        ui.end_row();

                        ui.end_row();

                        ui.label("Properties");
                        ui.separator();
                        ui.end_row();

                        ui.end_row();

                        ui.label("Base Color");
                        self.base_color.ui(ui, context, engine);
                        ui.end_row();

                        ui.label("Metallic");
                        self.metallic.ui(ui, context, engine);
                        ui.end_row();

                        ui.label("Roughness");
                        self.roughness.ui(ui, context, engine);
                        ui.end_row();

                        ui.end_row();

                        ui.label("Texture Slots");
                        ui.separator();
                        ui.end_row();

                        ui.end_row();

                        ui.label("Base Color");
                        self.base_color_texture.ui(ui, context, engine);
                        ui.end_row();

                        ui.label("Normal");
                        self.normal_texture.ui(ui, context, engine);
                        ui.end_row();

                        ui.label("Metallic Roughness");
                        self.metallic_roughness_texture.ui(ui, context, engine);
                        ui.end_row();

                        if self.base_color.changing() {
                            let mut material = self.material.unwrap().clone();
                            material.set_base_color(self.base_color.get());
                            unsafe {
                                self.material.replace(material);
                            }
                            self.repaint(context);
                        }
                        if self.base_color.changed() {
                            context.actions_mut().push(
                                engine,
                                ChangeBaseColor::new(
                                    self.material.clone(),
                                    self.base_color.get_old(),
                                    self.base_color.get(),
                                ),
                            );
                            self.repaint(context);
                        }

                        if self.metallic.changing() {
                            let mut material = self.material.unwrap().clone();
                            material.set_metallic(self.metallic.get());
                            unsafe {
                                self.material.replace(material);
                            }
                            self.repaint(context);
                        }
                        if self.metallic.changed() {
                            context.actions_mut().push(
                                engine,
                                ChangeMetallic::new(
                                    self.material.clone(),
                                    self.metallic.get_old(),
                                    self.metallic.get(),
                                ),
                            );
                            self.repaint(context);
                        }

                        if self.roughness.changing() {
                            let mut material = self.material.unwrap().clone();
                            material.set_roughness(self.roughness.get());
                            unsafe {
                                self.material.replace(material);
                            }
                            self.repaint(context);
                        }
                        if self.roughness.changed() {
                            context.actions_mut().push(
                                engine,
                                ChangeRoughness::new(
                                    self.material.clone(),
                                    self.roughness.get_old(),
                                    self.roughness.get(),
                                ),
                            );
                            self.repaint(context);
                        }

                        if self.base_color_texture.changed() {
                            context.actions_mut().push(
                                engine,
                                ChangeBaseColorTexture::new(
                                    self.material.clone(),
                                    self.base_color_texture.get(),
                                ),
                            );
                            self.repaint(context);
                        }
                        if self.normal_texture.changed() {
                            context.actions_mut().push(
                                engine,
                                ChangeNormalTexture::new(
                                    self.material.clone(),
                                    self.normal_texture.get(),
                                ),
                            );
                            self.repaint(context);
                        }
                        if self.metallic_roughness_texture.changed() {
                            context.actions_mut().push(
                                engine,
                                ChangeMetallicRoughnessTexture::new(
                                    self.material.clone(),
                                    self.metallic_roughness_texture.get(),
                                ),
                            );
                            self.repaint(context);
                        }
                    });
            });
    }
}

impl MaterialInspector {
    fn file_stem(&self) -> String {
        self.material
            .path()
            .expect("asset path")
            .file_stem()
            .and_then(OsStr::to_str)
            .map(ToOwned::to_owned)
            .expect("file stem")
    }

    fn repaint(&self, context: &mut EditorContext) {
        context
            .message_bus_mut()
            .post(Message::Repaint(Repaint::Asset(
                self.material.path().unwrap_or_default(),
            )));
    }
}

impl EditorAssetExt for Material {
    fn extensions() -> &'static [&'static str] {
        &["material"]
    }

    fn display_name() -> &'static str {
        "Material"
    }

    fn icon() -> Option<Icon> {
        Some(Icons::circle())
    }

    fn display(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        asset: AssetRef<Self>,
        size: Vec2,
    ) -> Box<dyn Widget> {
        Box::new(MaterialDisplay::new(graphics_context, assets, asset, size))
    }

    fn inspect(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        asset: AssetRef<Self>,
    ) -> Box<dyn Widget> {
        Box::new(MaterialInspector::new(graphics_context, assets, asset))
    }

    fn create(_graphics_context: Arc<GraphicsContext>, _assets: Arc<Assets>) -> Option<Vec<u8>> {
        ron::to_string(&Material::default())
            .ok()
            .map(|s| s.into_bytes())
    }

    fn create_entry() -> Option<(Option<Icon>, &'static str, &'static str)> {
        Some((Self::icon(), Self::display_name(), Self::extensions()[0]))
    }
}

struct ChangeBaseColor {
    new: Vec4,
    old: Vec4,
    material: AssetRef<Material>,
}

impl ChangeBaseColor {
    fn new(material: AssetRef<Material>, old: Vec4, new: Vec4) -> Self {
        Self { new, old, material }
    }

    fn replace(&self, base_color: Vec4) {
        let mut material = self.material.unwrap().clone();
        material.set_base_color(base_color);
        unsafe {
            self.material.replace(material);
        }
    }
}

impl Action for ChangeBaseColor {
    fn description(&self) -> String {
        "Change material base color".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeMetallic {
    new: f32,
    old: f32,
    material: AssetRef<Material>,
}

impl ChangeMetallic {
    fn new(material: AssetRef<Material>, old: f32, new: f32) -> Self {
        Self { new, old, material }
    }

    fn replace(&self, metallic: f32) {
        let mut material = self.material.unwrap().clone();
        material.set_metallic(metallic);
        unsafe {
            self.material.replace(material);
        }
    }
}

impl Action for ChangeMetallic {
    fn description(&self) -> String {
        "Change material metallic".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeRoughness {
    new: f32,
    old: f32,
    material: AssetRef<Material>,
}

impl ChangeRoughness {
    fn new(material: AssetRef<Material>, old: f32, new: f32) -> Self {
        Self { new, old, material }
    }

    fn replace(&self, roughness: f32) {
        let mut material = self.material.unwrap().clone();
        material.set_roughness(roughness);
        unsafe {
            self.material.replace(material);
        }
    }
}

impl Action for ChangeRoughness {
    fn description(&self) -> String {
        "Change material roughness".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new);
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old);
    }
}

struct ChangeBaseColorTexture {
    new: AssetRef<Texture>,
    old: AssetRef<Texture>,
    material: AssetRef<Material>,
}

impl ChangeBaseColorTexture {
    fn new(material: AssetRef<Material>, new: AssetRef<Texture>) -> Self {
        Self {
            new,
            old: material.unwrap().base_color_texture().clone(),
            material,
        }
    }

    fn replace(&self, base_color_texture: AssetRef<Texture>) {
        let mut material = self.material.unwrap().clone();
        material.set_base_color_texture(base_color_texture);
        unsafe {
            self.material.replace(material);
        }
    }
}

impl Action for ChangeBaseColorTexture {
    fn description(&self) -> String {
        "Change material base color texture".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeNormalTexture {
    new: AssetRef<Texture>,
    old: AssetRef<Texture>,
    material: AssetRef<Material>,
}

impl ChangeNormalTexture {
    fn new(material: AssetRef<Material>, new: AssetRef<Texture>) -> Self {
        Self {
            new,
            old: material.unwrap().normal_texture().clone(),
            material,
        }
    }

    fn replace(&self, normal_texture: AssetRef<Texture>) {
        let mut material = self.material.unwrap().clone();
        material.set_normal_texture(normal_texture);
        unsafe {
            self.material.replace(material);
        }
    }
}

impl Action for ChangeNormalTexture {
    fn description(&self) -> String {
        "Change material normal texture".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

struct ChangeMetallicRoughnessTexture {
    new: AssetRef<Texture>,
    old: AssetRef<Texture>,
    material: AssetRef<Material>,
}

impl ChangeMetallicRoughnessTexture {
    fn new(material: AssetRef<Material>, new: AssetRef<Texture>) -> Self {
        Self {
            new,
            old: material.unwrap().metallic_roughness_texture().clone(),
            material,
        }
    }

    fn replace(&self, metallic_roughness_texture: AssetRef<Texture>) {
        let mut material = self.material.unwrap().clone();
        material.set_metallic_roughness_texture(metallic_roughness_texture);
        unsafe {
            self.material.replace(material);
        }
    }
}

impl Action for ChangeMetallicRoughnessTexture {
    fn description(&self) -> String {
        "Change material metallic roughness texture".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

pub struct MaterialDisplayState {
    material: AssetRef<Material>,
    metallic_roughness_loaded: bool,
    normal_loaded: bool,
    base_color_loaded: bool,
    material_loaded: bool,
}

impl MaterialDisplayState {
    pub fn new(material: AssetRef<Material>) -> Self {
        Self {
            material_loaded: false,
            base_color_loaded: false,
            normal_loaded: false,
            metallic_roughness_loaded: false,
            material,
        }
    }

    pub fn requires_repaint(&mut self) -> bool {
        let material_loaded = self.material.is_ok();
        let base_color_loaded = self
            .material
            .ok()
            .filter(|material| material.base_color_texture().is_ok())
            .is_some();
        let normal_loaded = self
            .material
            .ok()
            .filter(|material| material.base_color_texture().is_ok())
            .is_some();
        let metallic_roughness_loaded = self
            .material
            .ok()
            .filter(|material| material.base_color_texture().is_ok())
            .is_some();

        let repaint = (!self.material_loaded && material_loaded)
            || (!self.base_color_loaded && base_color_loaded)
            || (!self.normal_loaded && normal_loaded)
            || (!self.metallic_roughness_loaded && metallic_roughness_loaded);

        self.material_loaded = material_loaded;
        self.base_color_loaded = base_color_loaded;
        self.normal_loaded = normal_loaded;
        self.metallic_roughness_loaded = metallic_roughness_loaded;

        repaint
    }
}
