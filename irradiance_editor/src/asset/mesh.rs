use std::{ffi::OsStr, sync::Arc};

use egui::{style::Margin, Color32, Frame, Grid, PaintCallback, Rect, RichText, TextureId, Ui};
use irradiance_runtime::gfx::command_buffer::CommandBufferBuilder;
use irradiance_runtime::gfx::device::DeviceOwned;
use irradiance_runtime::rendering::pbr::{BloomBuffer, SsaoBuffer};
use irradiance_runtime::{
    asset::{AssetRef, Assets},
    core::Engine,
    ecs::{Entities, Transform},
    gfx::{
        image::{
            Format, Image, ImageAspects, ImageLayout, ImageSubresourceRange, ImageType, ImageUsage,
            ImageView, ImageViewType,
        },
        sync::{AccessMask, ImageMemoryBarrier, PipelineBarrier, PipelineStages},
        Extent2D, GraphicsContext,
    },
    math::{Mat4, Vec2, Vec3, Vec4},
    rendering::{pbr::GBuffer, Camera, DirectionalLight, Material, Mesh, StaticMesh},
};

use crate::gui::CallbackFn;
use crate::{
    asset::EditorAssetExt,
    core::EditorEngine,
    gui::{
        message::{Message, Recv, Repaint},
        widgets::{AssetSlot, Widget},
        Action, EditorContext,
    },
};

use super::material::MaterialDisplayState;

/// Displays a mesh.
pub struct MeshDisplay {
    texture_id: Option<TextureId>,
    entities: Arc<Entities>,
    final_image: Arc<ImageView>,
    bloom_buffer: Arc<BloomBuffer>,
    ssao_buffer: Arc<SsaoBuffer>,
    light_buffer: Arc<ImageView>,
    g_buffer: Arc<GBuffer>,
    size: Vec2,
    mesh: AssetRef<Mesh>,
    state: MeshDisplayState,
    repaint: bool,
}

impl MeshDisplay {
    /// Creates a new mesh display.
    pub fn new(
        graphics_context: Arc<GraphicsContext>,
        mesh: AssetRef<Mesh>,
        size: Vec2,
        sub_mesh: Option<usize>,
    ) -> Self {
        let device = graphics_context.device();
        let extent = Extent2D {
            width: size[0] as _,
            height: size[1] as _,
        };

        let entities = Entities::new();

        let sun = entities.create("sun").unwrap();
        sun.insert(Transform::new(
            Default::default(),
            Mat4::look_at(
                Vec3::new(1.0, 1.0, 1.0),
                Vec3::zero(),
                Vec3::new(0.0, 1.0, 0.0),
            )
            .inverse()
            .into(),
            Vec3::new(1.0, 1.0, 1.0),
        ));
        sun.insert(DirectionalLight::new(Vec3::new(1.0, 1.0, 1.0), 2.0));

        let model = entities.create("model").unwrap();
        model.insert::<Transform>(Transform::new(
            Default::default(),
            Default::default(),
            Vec3::new(1.0, 1.0, 1.0),
        ));
        model.insert::<StaticMesh>(StaticMesh::new(mesh.clone()).with_sub_mesh(sub_mesh));

        unsafe {
            entities.commit();
        }

        let mesh_display = Self {
            repaint: true,
            state: MeshDisplayState::new(mesh.clone()),
            mesh,
            size,
            g_buffer: Arc::new(GBuffer::new(device.clone(), extent).expect("g buffer")),
            light_buffer: ImageView::builder(
                device.clone(),
                Image::builder(device.clone())
                    .image_type(ImageType::Type2D)
                    .format(Format::R32G32B32A32SFloat)
                    .extent(extent.into())
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()
                    .expect("light buffer"),
            )
            .view_type(ImageViewType::Type2D)
            .build()
            .expect("light buffer image view"),
            ssao_buffer: Arc::new(SsaoBuffer::new(device.clone(), extent).expect("ssao buffer")),
            bloom_buffer: Arc::new(BloomBuffer::new(device.clone(), extent).expect("bloom buffer")),
            final_image: ImageView::builder(
                device.clone(),
                Image::builder(device.clone())
                    .image_type(ImageType::Type2D)
                    .format(graphics_context.swapchain().unwrap().image_format())
                    .extent(extent.into())
                    .usage(ImageUsage {
                        color_attachment: true,
                        sampled: true,
                        ..Default::default()
                    })
                    .build()
                    .expect("final image"),
            )
            .view_type(ImageViewType::Type2D)
            .build()
            .expect("final image view"),
            entities,
            texture_id: None,
        };

        mesh_display
            .light_buffer
            .image()
            .set_debug_utils_object_name("Mesh Display Light Buffer Image");
        mesh_display
            .light_buffer
            .set_debug_utils_object_name("Mesh Display Light Buffer Image View");
        mesh_display
            .final_image
            .image()
            .set_debug_utils_object_name("Mesh Display Final Image");
        mesh_display
            .final_image
            .set_debug_utils_object_name("Mesh Display Final Image View");

        mesh_display
    }
}

impl Widget for MeshDisplay {
    fn handle_messages(&mut self, context: &mut EditorContext, _engine: &mut EditorEngine) {
        match context.message_bus().recv() {
            Some(Repaint::Asset(path)) if Some(path) == self.state.mesh.path().as_ref() => {
                self.repaint = true
            }
            _ => {}
        }
        self.dispatch_message(context);
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.insert_camera();
        self.handle_repaint(ui, engine);

        let texture_id = self
            .texture_id
            .get_or_insert_with(|| context.register_texture(true, self.final_image.clone()));
        ui.image(*texture_id, egui::Vec2::new(self.size[0], self.size[1]));
    }
}

impl MeshDisplay {
    fn insert_camera(&self) {
        if let (Some(mesh), None) = (self.mesh.ok(), self.entities.active::<Camera>()) {
            let eye = Vec3::new(0.5, 0.5, 1.0)
                * mesh
                    .bounding_box()
                    .min()
                    .distance(mesh.bounding_box().max());
            let camera = self.entities.create("camera").unwrap();
            camera.insert(Transform::new(
                eye,
                Mat4::look_at(eye, Vec3::zero(), Vec3::new(0.0, 1.0, 0.0))
                    .inverse()
                    .into(),
                Vec3::new(1.0, 1.0, 1.0),
            ));
            camera.insert(Camera::new(45f32.to_radians(), 0.1, 100.0));
            camera.activate::<Camera>();

            unsafe {
                self.entities.commit();
            }
        }
    }

    fn handle_repaint(&mut self, ui: &mut Ui, engine: &mut EditorEngine) {
        if self.state.requires_repaint() {
            self.repaint = true;
        }

        if self.repaint {
            self.paint(ui, engine);
            self.repaint = false;
        }
    }

    fn dispatch_message(&mut self, context: &mut EditorContext) {
        if let Some(mesh) = self.mesh.clone().ok() {
            for sub_mesh in mesh.sub_meshes() {
                if self.material_changes(context, sub_mesh.material()) {
                    context
                        .message_bus_mut()
                        .post(Message::Repaint(Repaint::Asset(
                            self.mesh.path().unwrap_or_default(),
                        )));
                }
            }
        }
    }

    fn material_changes(
        &mut self,
        context: &mut EditorContext,
        material: &AssetRef<Material>,
    ) -> bool {
        matches!(context.message_bus().recv(), Some(Repaint::Asset(path)) if Some(path) == material.path().as_ref())
    }

    fn paint(&mut self, ui: &mut Ui, engine: &mut EditorEngine) {
        let g_buffer = self.g_buffer.clone();
        let light_buffer = self.light_buffer.clone();
        let ssao_buffer = self.ssao_buffer.clone();
        let bloom_buffer = self.bloom_buffer.clone();
        let final_image = self.final_image.clone();
        let entities = self.entities.clone();
        let pbr_pipeline = engine.buf_pbr_pipeline().pbr_pipeline().clone();

        ui.painter().add(PaintCallback {
            rect: Rect::NOTHING,
            callback: Arc::new(CallbackFn::from(
                move |builder: &mut CommandBufferBuilder| {
                    pbr_pipeline
                        .draw(
                            builder,
                            &g_buffer,
                            light_buffer.clone(),
                            &ssao_buffer,
                            &bloom_buffer,
                            final_image.clone(),
                            Vec4::new(0.0, 0.0, 0.0, 1.0),
                            &entities,
                        )
                        .unwrap();
                    builder.pipeline_barrier(
                        PipelineBarrier::builder()
                            .image_memory_barrier(
                                ImageMemoryBarrier::builder(final_image.image().clone())
                                    .src_stage_mask(PipelineStages {
                                        color_attachment_output: true,
                                        ..Default::default()
                                    })
                                    .src_access_mask(AccessMask {
                                        color_attachment_write: true,
                                        ..Default::default()
                                    })
                                    .dst_stage_mask(PipelineStages {
                                        fragment_shader: true,
                                        ..Default::default()
                                    })
                                    .dst_access_mask(AccessMask {
                                        shader_sampled_read: true,
                                        ..Default::default()
                                    })
                                    .old_layout(ImageLayout::ColorAttachmentOptimal)
                                    .new_layout(ImageLayout::ShaderReadOnlyOptimal)
                                    .subresource_range(
                                        ImageSubresourceRange::builder()
                                            .aspect_mask(ImageAspects {
                                                color: true,
                                                ..Default::default()
                                            })
                                            .build(),
                                    )
                                    .build(),
                            )
                            .build(),
                    );
                },
            )),
        });
    }
}

/// Inspects a mesh.
pub struct MeshInspector {
    material_slots: Vec<(MeshDisplay, AssetSlot<Material>)>,
    mesh_display: MeshDisplay,
    mesh: AssetRef<Mesh>,
    assets: Arc<Assets>,
    graphics_context: Arc<GraphicsContext>,
}

impl MeshInspector {
    /// Creates a new mesh inspector.
    pub fn new(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        mesh: AssetRef<Mesh>,
    ) -> Self {
        Self {
            graphics_context: graphics_context.clone(),
            assets,
            mesh: mesh.clone(),
            mesh_display: MeshDisplay::new(graphics_context, mesh, Vec2::new(150.0, 150.0), None),
            material_slots: vec![],
        }
    }
}

impl Widget for MeshInspector {
    fn handle_messages(&mut self, context: &mut EditorContext, engine: &mut EditorEngine) {
        self.mesh_display.handle_messages(context, engine);
        for (mesh_display, asset_slot) in &mut self.material_slots {
            mesh_display.handle_messages(context, engine);
            asset_slot.handle_messages(context, engine);
        }
    }

    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        if let Some(mesh) = self.mesh.ok().filter(|_| self.material_slots.is_empty()) {
            self.material_slots = mesh
                .sub_meshes()
                .iter()
                .enumerate()
                .map(|(index, sub_mesh)| {
                    let mut material_slot = AssetSlot::<Material>::new(
                        self.graphics_context.clone(),
                        self.assets.clone(),
                    );
                    material_slot.set(sub_mesh.material().clone());
                    (
                        MeshDisplay::new(
                            self.graphics_context.clone(),
                            self.mesh.clone(),
                            Vec2::new(50.0, 50.0),
                            Some(index),
                        ),
                        material_slot,
                    )
                })
                .collect()
        }

        Frame::default().fill(Color32::BLACK).show(ui, |ui| {
            ui.vertical_centered(|ui| {
                self.mesh_display.ui(ui, context, engine);
            });
        });
        Frame::default()
            .inner_margin(Margin::same(15.0))
            .show(ui, |ui| {
                Grid::new("mesh_properties")
                    .num_columns(2)
                    .spacing([40.0, 4.0])
                    .show(ui, |ui| {
                        ui.label("Name");
                        ui.label(RichText::new(self.file_stem()).strong());
                        ui.end_row();

                        ui.end_row();

                        ui.label("Material Slots");
                        ui.separator();
                        ui.end_row();

                        ui.end_row();

                        for (sub_mesh, material_slot) in &mut self.material_slots {
                            sub_mesh.ui(ui, context, engine);
                            material_slot.ui(ui, context, engine);
                            ui.end_row();
                        }
                    });
            });

        if let Some(mesh) = self.mesh.ok() {
            for (sub_mesh, _) in mesh.sub_meshes().iter().enumerate() {
                if self.material_slots[sub_mesh].1.changed() {
                    context.actions_mut().push(
                        engine,
                        ChangeSubMeshMaterial::new(
                            self.mesh.clone(),
                            sub_mesh,
                            self.material_slots[sub_mesh].1.get(),
                        ),
                    );
                    context
                        .message_bus_mut()
                        .post(Message::Repaint(Repaint::Asset(
                            self.mesh.path().unwrap_or_default(),
                        )));
                }
            }
        }
    }
}

impl MeshInspector {
    fn file_stem(&self) -> String {
        self.mesh
            .path()
            .expect("asset path")
            .file_stem()
            .and_then(OsStr::to_str)
            .map(ToOwned::to_owned)
            .expect("file stem")
    }
}

impl EditorAssetExt for Mesh {
    fn extensions() -> &'static [&'static str] {
        &["mesh"]
    }

    fn display_name() -> &'static str {
        "Mesh"
    }

    fn display(
        graphics_context: Arc<GraphicsContext>,
        _assets: Arc<Assets>,
        asset: AssetRef<Self>,
        size: Vec2,
    ) -> Box<dyn Widget> {
        Box::new(MeshDisplay::new(graphics_context, asset, size, None))
    }

    fn inspect(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        asset: AssetRef<Self>,
    ) -> Box<dyn Widget> {
        Box::new(MeshInspector::new(graphics_context, assets, asset))
    }
}

struct ChangeSubMeshMaterial {
    new: AssetRef<Material>,
    old: AssetRef<Material>,
    sub_mesh: usize,
    mesh: AssetRef<Mesh>,
}

impl ChangeSubMeshMaterial {
    fn new(mesh: AssetRef<Mesh>, sub_mesh: usize, new: AssetRef<Material>) -> Self {
        Self {
            new,
            old: mesh.unwrap().sub_meshes()[sub_mesh].material().clone(),
            sub_mesh,
            mesh,
        }
    }

    fn replace(&self, material: AssetRef<Material>) {
        let mut mesh = self.mesh.unwrap().clone();
        mesh.sub_meshes_mut()[self.sub_mesh].set_material(material);
        unsafe {
            self.mesh.replace(mesh);
        }
    }
}

impl Action for ChangeSubMeshMaterial {
    fn description(&self) -> String {
        "Change sub mesh material".into()
    }

    fn redo(&self, _engine: &mut EditorEngine) {
        self.replace(self.new.clone());
    }

    fn undo(&self, _engine: &mut EditorEngine) {
        self.replace(self.old.clone());
    }
}

pub struct MeshDisplayState {
    mesh: AssetRef<Mesh>,
    materials: Vec<MaterialDisplayState>,
    mesh_loaded: bool,
}

impl MeshDisplayState {
    pub fn new(mesh: AssetRef<Mesh>) -> Self {
        Self {
            mesh_loaded: false,
            materials: vec![],
            mesh,
        }
    }

    pub fn requires_repaint(&mut self) -> bool {
        if let Some(mesh) = self.mesh.ok().filter(|_| self.materials.is_empty()) {
            self.materials = mesh
                .sub_meshes()
                .iter()
                .map(|sub_mesh| MaterialDisplayState::new(sub_mesh.material().clone()))
                .collect();
        }
        let mesh_loaded = self.mesh.is_ok();

        let repaint = (!self.mesh_loaded && mesh_loaded)
            || self
                .materials
                .iter_mut()
                .any(|material| material.requires_repaint());

        self.mesh_loaded = mesh_loaded;

        repaint
    }
}
