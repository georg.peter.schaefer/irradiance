use std::ffi::OsStr;
use std::sync::Arc;
use std::time::Duration;

use egui::style::Margin;
use egui::{Align, Color32, FontSelection, Frame, Grid, Layout, RichText, Sense, Ui, WidgetText};

use irradiance_runtime::animation::Animation;
use irradiance_runtime::asset::{AssetRef, Assets};
use irradiance_runtime::gfx::GraphicsContext;
use irradiance_runtime::math::Vec2;

use crate::asset::EditorAssetExt;
use crate::core::EditorEngine;
use crate::gui::widgets::Widget;
use crate::gui::{EditorContext, Icons};

#[derive(Default)]
struct AnimationDisplay;

impl Widget for AnimationDisplay {
    fn ui(&mut self, ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
        let (rect, _) = ui.allocate_exact_size(
            egui::Vec2::new(50.0, 50.0),
            Sense::focusable_noninteractive(),
        );
        let text = WidgetText::from(Icons::person_walking().size(40.0));
        let galley = text.into_galley(ui, None, 100.0, FontSelection::default());
        let pos = Layout::top_down(Align::Center)
            .align_size_within_rect(galley.size(), rect)
            .min;
        ui.painter()
            .galley_with_color(pos, galley.galley().clone(), Color32::GRAY);
    }
}

struct AnimationInspector {
    animation: AssetRef<Animation>,
}

impl AnimationInspector {
    fn new(animation: AssetRef<Animation>) -> Self {
        Self { animation }
    }

    fn file_stem(&self) -> String {
        self.animation
            .path()
            .expect("asset path")
            .file_stem()
            .and_then(OsStr::to_str)
            .map(ToOwned::to_owned)
            .expect("file stem")
    }
}

impl Widget for AnimationInspector {
    fn ui(&mut self, ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
        Frame::default()
            .inner_margin(Margin::same(15.0))
            .show(ui, |ui| {
                Grid::new("animation_properties")
                    .num_columns(2)
                    .spacing([40.0, 4.0])
                    .show(ui, |ui| {
                        ui.label("Name");
                        ui.label(RichText::new(self.file_stem()).strong());
                        ui.end_row();

                        ui.end_row();

                        ui.label("Properties");
                        ui.separator();
                        ui.end_row();

                        ui.end_row();

                        ui.label("Duration");
                        ui.label(RichText::new(format!(
                            "{:?}",
                            self.animation
                                .ok()
                                .map(|animation| Duration::from_secs_f32(
                                    animation.end - animation.start
                                ))
                                .unwrap_or_default()
                        )));
                        ui.end_row();
                    });
            });
    }
}

impl EditorAssetExt for Animation {
    fn extensions() -> &'static [&'static str] {
        &["anim"]
    }

    fn display_name() -> &'static str {
        "Animation"
    }

    fn display(
        _graphics_context: Arc<GraphicsContext>,
        _assets: Arc<Assets>,
        _asset: AssetRef<Self>,
        _size: Vec2,
    ) -> Box<dyn Widget> {
        Box::new(AnimationDisplay::default())
    }

    fn inspect(
        _graphics_context: Arc<GraphicsContext>,
        _assets: Arc<Assets>,
        asset: AssetRef<Self>,
    ) -> Box<dyn Widget> {
        Box::new(AnimationInspector::new(asset))
    }
}
