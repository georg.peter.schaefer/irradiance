//! Asset extension.

use std::{
    collections::HashMap,
    ffi::OsStr,
    fs,
    io::Error,
    path::{Path, PathBuf},
    sync::Arc,
};

use irradiance_runtime::animation::Animation;
use irradiance_runtime::audio::AudioSource;
use irradiance_runtime::rendering::{Material, Mesh, SkinnedMesh, Texture};
use irradiance_runtime::{
    asset::{Asset, AssetRef, Assets},
    core::Engine,
    gfx::GraphicsContext,
    math::Vec2,
};

use crate::{
    core::EditorEngine,
    gui::{widgets::Widget, Icon},
};

/// Asset extension trait for the editor.
pub trait EditorAssetExt: Asset {
    /// Returns the file extensions of the asset type.
    fn extensions() -> &'static [&'static str];
    /// Returns the display name.
    fn display_name() -> &'static str;
    /// Returns the icon.
    fn icon() -> Option<Icon> {
        None
    }
    /// Returns a widget for displaying the asset.
    fn display(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        asset: AssetRef<Self>,
        size: Vec2,
    ) -> Box<dyn Widget>;
    /// Returns a widget for inspecting the asset.
    fn inspect(
        graphics_context: Arc<GraphicsContext>,
        assets: Arc<Assets>,
        asset: AssetRef<Self>,
    ) -> Box<dyn Widget>;
    /// Creates a default asset and returns it as bytes.
    fn create(_graphics_context: Arc<GraphicsContext>, _assets: Arc<Assets>) -> Option<Vec<u8>> {
        None
    }
    /// Returns the create entry.
    fn create_entry() -> Option<(Option<Icon>, &'static str, &'static str)> {
        None
    }
}

type CreateEntry = (Option<Icon>, &'static str, &'static str);
type CreateFactory = dyn Fn(Arc<GraphicsContext>, Arc<Assets>) -> Option<Vec<u8>>;
type InspectorFactory =
    dyn Fn(Arc<GraphicsContext>, Arc<Assets>, PathBuf) -> Option<Box<dyn Widget>>;
type DisplayFactory =
    dyn Fn(Arc<GraphicsContext>, Arc<Assets>, PathBuf, Vec2) -> Option<Box<dyn Widget>>;

/// Asset type registration.
pub struct EditorAssetTypes {
    create_entries: Vec<Option<CreateEntry>>,
    creates: Vec<Box<CreateFactory>>,
    inspects: Vec<Box<InspectorFactory>>,
    displays: Vec<Box<DisplayFactory>>,
    icons: Vec<Option<Icon>>,
    display_names: Vec<&'static str>,
    extensions: HashMap<&'static str, usize>,
    graphics_context: Arc<GraphicsContext>,
    assets: Arc<Assets>,
}

impl EditorAssetTypes {
    pub(crate) fn builder() -> EditorAssetTypesBuilder {
        EditorAssetTypesBuilder {
            extensions: Default::default(),
            display_names: vec![],
            icons: vec![],
            displays: vec![],
            inspects: vec![],
            creates: vec![],
            create_entries: vec![],
        }
        .asset::<Texture>()
        .asset::<Material>()
        .asset::<Mesh>()
        .asset::<SkinnedMesh>()
        .asset::<AudioSource>()
        .asset::<Animation>()
    }

    /// Returns if an asset type is registered.
    pub fn is_registered(&self, path: &Path) -> bool {
        path.extension()
            .and_then(OsStr::to_str)
            .and_then(|extension| self.extensions.get(extension))
            .is_some()
    }

    /// Returns the display name of an asset.
    pub fn display_name(&self, path: PathBuf) -> Option<&'static str> {
        path.extension()
            .and_then(OsStr::to_str)
            .and_then(|extension| self.extensions.get(extension))
            .and_then(|index| self.display_names.get(*index))
            .cloned()
    }

    /// Returns the icon of an asset.
    pub fn icon(&self, path: PathBuf) -> Option<Icon> {
        path.extension()
            .and_then(OsStr::to_str)
            .and_then(|extension| self.extensions.get(extension))
            .and_then(|index| self.icons.get(*index))
            .cloned()
            .flatten()
    }

    /// Returns a widget for displaying an asset.
    pub fn display(&self, path: PathBuf, size: Vec2) -> Option<Box<dyn Widget>> {
        path.extension()
            .and_then(OsStr::to_str)
            .and_then(|extension| self.extensions.get(extension))
            .and_then(|index| self.displays.get(*index))
            .and_then(|display| {
                display(
                    self.graphics_context.clone(),
                    self.assets.clone(),
                    path,
                    size,
                )
            })
    }

    /// Returns a widget for inspecting an asset.
    pub fn inspect(&self, path: PathBuf) -> Option<Box<dyn Widget>> {
        path.extension()
            .and_then(OsStr::to_str)
            .and_then(|extension| self.extensions.get(extension))
            .and_then(|index| self.inspects.get(*index))
            .and_then(|inspect| inspect(self.graphics_context.clone(), self.assets.clone(), path))
    }

    /// Creates an asset at the given path.
    pub fn create(&self, path: PathBuf) -> Result<(), Error> {
        if let Some(bytes) = path
            .extension()
            .and_then(OsStr::to_str)
            .and_then(|extension| self.extensions.get(extension))
            .and_then(|index| self.creates.get(*index))
            .and_then(|create| create(self.graphics_context.clone(), self.assets.clone()))
        {
            fs::write(path, bytes)?;
        }

        Ok(())
    }

    /// Returns the new sub menu entries for all registered asset types.
    pub fn create_entries(&self) -> Vec<(Option<Icon>, &'static str, &'static str)> {
        let mut entries = self
            .create_entries
            .iter()
            .flatten()
            .cloned()
            .collect::<Vec<_>>();
        entries.sort_by_key(|(_, display_name, _)| *display_name);
        entries
    }
}

/// Type for building asset types.
pub struct EditorAssetTypesBuilder {
    create_entries: Vec<Option<CreateEntry>>,
    creates: Vec<Box<CreateFactory>>,
    inspects: Vec<Box<InspectorFactory>>,
    displays: Vec<Box<DisplayFactory>>,
    icons: Vec<Option<Icon>>,
    display_names: Vec<&'static str>,
    extensions: HashMap<&'static str, usize>,
}

impl EditorAssetTypesBuilder {
    /// Registers an asset type.
    pub fn asset<A: EditorAssetExt>(mut self) -> Self {
        let index = self.displays.len();
        self.extensions
            .extend(A::extensions().iter().map(|extension| (*extension, index)));
        self.display_names.push(A::display_name());
        self.icons.push(A::icon());
        self.displays
            .push(Box::new(|graphics_context, assets, path, size| {
                assets
                    .read::<A, _>(graphics_context.clone(), path)
                    .ok()
                    .map(|asset| A::display(graphics_context, assets, asset, size))
            }));
        self.inspects
            .push(Box::new(|graphics_context, assets, path| {
                assets
                    .read::<A, _>(graphics_context.clone(), path)
                    .ok()
                    .map(|asset| A::inspect(graphics_context, assets, asset))
            }));
        self.creates.push(Box::new(A::create));
        self.create_entries.push(A::create_entry());
        self
    }

    /// Builds the asset types.
    pub fn build(self, engine: &EditorEngine) -> EditorAssetTypes {
        EditorAssetTypes {
            graphics_context: engine.graphics_context().clone(),
            assets: engine.assets().clone(),
            extensions: self.extensions,
            display_names: self.display_names,
            icons: self.icons,
            create_entries: self.create_entries,
            displays: self.displays,
            inspects: self.inspects,
            creates: self.creates,
        }
    }
}
