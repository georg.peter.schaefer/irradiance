use std::ffi::OsStr;
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::Arc;
use std::time::Duration;

use egui::style::Margin;
use egui::{Button, Frame, Grid, ProgressBar, RichText, Ui};

use irradiance_runtime::asset::{AssetRef, Assets};
use irradiance_runtime::audio::AudioSource;
use irradiance_runtime::core::Engine;
use irradiance_runtime::gfx::GraphicsContext;
use irradiance_runtime::math::Vec2;
use irradiance_runtime::rodio::{Sink, Source};

use crate::asset::EditorAssetExt;
use crate::core::EditorEngine;
use crate::gui::widgets::Widget;
use crate::gui::{EditorContext, Icons};

#[derive(Default)]
struct AudioSourceDisplay;

impl Widget for AudioSourceDisplay {
    fn ui(&mut self, ui: &mut Ui, _context: &mut EditorContext, _engine: &mut EditorEngine) {
        ui.centered_and_justified(|ui| ui.label(Icons::file_audio().size(40.0)));
    }
}

struct AudioSourceInspector {
    elapsed: Arc<AtomicU64>,
    playing: bool,
    sink: Option<Sink>,
    audio_source: AssetRef<AudioSource>,
}

impl AudioSourceInspector {
    fn new(audio_source: AssetRef<AudioSource>) -> Self {
        Self {
            elapsed: Arc::new(AtomicU64::new(0)),
            playing: false,
            sink: None,
            audio_source,
        }
    }

    fn file_stem(&self) -> String {
        self.audio_source
            .path()
            .expect("asset path")
            .file_stem()
            .and_then(OsStr::to_str)
            .map(ToOwned::to_owned)
            .expect("file stem")
    }
}

impl Widget for AudioSourceInspector {
    fn ui(&mut self, ui: &mut Ui, _context: &mut EditorContext, engine: &mut EditorEngine) {
        Frame::default()
            .inner_margin(Margin::same(15.0))
            .show(ui, |ui| {
                ui.horizontal(|ui| {
                    if self.playing {
                        if ui
                            .add_enabled(
                                self.audio_source.is_ok(),
                                Button::new(Icons::pause()).frame(false),
                            )
                            .clicked()
                        {
                            self.sink.as_ref().unwrap().pause();
                            self.playing = false;
                        }
                    } else {
                        if ui
                            .add_enabled(
                                self.audio_source.is_ok(),
                                Button::new(Icons::play()).frame(false),
                            )
                            .clicked()
                        {
                            if self.sink.is_none() {
                                let elapsed = self.elapsed.clone();
                                self.sink = Some(engine.audio().play(
                                    self.audio_source.unwrap().clone().periodic_access(
                                        Duration::from_secs(1),
                                        move |_| {
                                            elapsed.fetch_add(1, Ordering::Relaxed);
                                        },
                                    ),
                                ));
                            } else {
                                self.sink.as_ref().unwrap().play();
                            }
                            self.playing = true;
                        }
                    }
                    if ui
                        .add_enabled(
                            self.audio_source.is_ok() && self.sink.is_some(),
                            Button::new(Icons::stop()).frame(false),
                        )
                        .clicked()
                    {
                        self.sink = None;
                        self.playing = false;
                        self.elapsed.store(0, Ordering::Relaxed);
                    }
                    let progress = self
                        .audio_source
                        .ok()
                        .and_then(|source| source.total_duration())
                        .map(|total_duration| {
                            self.elapsed.load(Ordering::Relaxed) as f32
                                / total_duration.as_secs_f32()
                        })
                        .unwrap_or_default();
                    ui.label(RichText::new(format!(
                        "{:?}",
                        Duration::from_secs(self.elapsed.load(Ordering::Relaxed))
                    )));
                    ui.add(ProgressBar::new(progress));

                    if self
                        .sink
                        .as_ref()
                        .map(|sink| sink.empty())
                        .unwrap_or_default()
                    {
                        self.sink = None;
                        self.playing = false;
                        self.elapsed.store(0, Ordering::Relaxed);
                    }
                });
            });
        Frame::default()
            .inner_margin(Margin::same(15.0))
            .show(ui, |ui| {
                Grid::new("audio_source_properties")
                    .num_columns(2)
                    .spacing([40.0, 4.0])
                    .show(ui, |ui| {
                        ui.label("Name");
                        ui.label(RichText::new(self.file_stem()).strong());
                        ui.end_row();

                        ui.end_row();

                        ui.label("Properties");
                        ui.separator();
                        ui.end_row();

                        ui.end_row();

                        ui.label("Duration");
                        ui.label(RichText::new(format!(
                            "{:?}",
                            self.audio_source
                                .ok()
                                .and_then(|source| source.total_duration())
                                .unwrap_or_default()
                        )));
                        ui.end_row();
                    });
            });
    }
}

impl EditorAssetExt for AudioSource {
    fn extensions() -> &'static [&'static str] {
        &["wav", "mp3", "ogg", "flac"]
    }

    fn display_name() -> &'static str {
        "Audio Source"
    }

    fn display(
        _graphics_context: Arc<GraphicsContext>,
        _assets: Arc<Assets>,
        _asset: AssetRef<Self>,
        _size: Vec2,
    ) -> Box<dyn Widget> {
        Box::new(AudioSourceDisplay::default())
    }

    fn inspect(
        _graphics_context: Arc<GraphicsContext>,
        _assets: Arc<Assets>,
        asset: AssetRef<Self>,
    ) -> Box<dyn Widget> {
        Box::new(AudioSourceInspector::new(asset))
    }
}
