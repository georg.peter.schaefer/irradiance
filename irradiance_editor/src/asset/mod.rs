//! Asset extension.

pub use extension::EditorAssetExt;
pub use extension::EditorAssetTypes;
pub use extension::EditorAssetTypesBuilder;
pub use material::MaterialDisplay;
pub use mesh::MeshDisplay;
pub use texture::TextureDisplay;

mod animation;
mod audio_source;
mod extension;
mod material;
mod mesh;
mod skinned_mesh;
mod texture;
