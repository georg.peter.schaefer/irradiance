use std::ffi::OsStr;

use egui::{style::Margin, Color32, Frame, Grid, RichText, Rounding, Sense, TextureId, Ui};
use irradiance_runtime::{
    asset::{AssetRef, Assets},
    gfx::GraphicsContext,
    math::Vec2,
    rendering::Texture,
};

use crate::{
    asset::EditorAssetExt,
    core::EditorEngine,
    gui::{widgets::Widget, EditorContext},
};

/// Displays a texture.
pub struct TextureDisplay {
    texture_id: Option<TextureId>,
    size: Vec2,
    texture: AssetRef<Texture>,
}

impl TextureDisplay {
    /// Creates a new texture display.
    pub fn new(texture: AssetRef<Texture>, size: Vec2) -> Self {
        Self {
            texture,
            size,
            texture_id: None,
        }
    }
}

impl Widget for TextureDisplay {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, _engine: &mut EditorEngine) {
        let size = egui::Vec2::new(self.size[0], self.size[1]);
        if let Some(texture) = self.texture.ok().filter(|_| self.texture_id.is_none()) {
            self.texture_id = Some(context.register_texture(false, texture.image_view().clone()));
        }

        if let Some(texture_id) = self.texture_id {
            ui.image(texture_id, size);
        } else {
            let (rect, _) = ui.allocate_exact_size(size, Sense::focusable_noninteractive());
            ui.painter()
                .rect_filled(rect, Rounding::default(), Color32::BLACK);
        }
    }
}

/// Inspects a texture.
pub struct TextureInspector {
    texture_display: TextureDisplay,
    texture: AssetRef<Texture>,
}

impl TextureInspector {
    /// Creates a new texture inspector.
    pub fn new(texture: AssetRef<Texture>) -> Self {
        Self {
            texture: texture.clone(),
            texture_display: TextureDisplay::new(texture, Vec2::new(150.0, 150.0)),
        }
    }
}

impl Widget for TextureInspector {
    fn ui(&mut self, ui: &mut Ui, context: &mut EditorContext, engine: &mut EditorEngine) {
        Frame::default().fill(Color32::BLACK).show(ui, |ui| {
            ui.vertical_centered(|ui| {
                self.texture_display.ui(ui, context, engine);
            });
        });
        Frame::default()
            .inner_margin(Margin::same(15.0))
            .show(ui, |ui| {
                Grid::new("texture_properties")
                    .num_columns(2)
                    .spacing([40.0, 4.0])
                    .show(ui, |ui| {
                        ui.label("Name");
                        ui.label(RichText::new(self.file_stem()).strong());
                        ui.end_row();
                    });
            });
    }
}

impl TextureInspector {
    fn file_stem(&self) -> String {
        self.texture
            .path()
            .expect("asset path")
            .file_stem()
            .and_then(OsStr::to_str)
            .map(ToOwned::to_owned)
            .expect("file stem")
    }
}

impl EditorAssetExt for Texture {
    fn extensions() -> &'static [&'static str] {
        &["png", "jpg", "tga", "exr"]
    }

    fn display_name() -> &'static str {
        "Texture"
    }

    fn display(
        _graphics_context: std::sync::Arc<GraphicsContext>,
        _assets: std::sync::Arc<Assets>,
        asset: AssetRef<Self>,
        size: Vec2,
    ) -> Box<dyn Widget> {
        Box::new(TextureDisplay::new(asset, size))
    }

    fn inspect(
        _graphics_context: std::sync::Arc<GraphicsContext>,
        _assets: std::sync::Arc<Assets>,
        asset: AssetRef<Self>,
    ) -> Box<dyn Widget> {
        Box::new(TextureInspector::new(asset))
    }
}
