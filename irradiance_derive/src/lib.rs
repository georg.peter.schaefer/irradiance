#![warn(missing_docs)]

//! irradiance procedural derive macros.

use std::str::FromStr;

use proc_macro::TokenStream;

use proc_macro2::{Ident, Span};
use quote::quote;
use syn::{
    parse_macro_input, punctuated::Punctuated, Field, ItemStruct, Path, PathSegment, Token, Type,
    TypePath, Visibility,
};

/// Implements the `Component` trait for a type.
#[proc_macro_derive(Component)]
pub fn component(input: TokenStream) -> TokenStream {
    let item_struct = parse_macro_input!(input as ItemStruct);
    let ident = item_struct.ident.clone();
    let component_ext = gen_from_str(ident.clone(), &item_struct);
    quote! {
        impl irradiance_runtime::ecs::Component for #ident {
            fn as_any(self: std::sync::Arc<Self>) -> std::sync::Arc<dyn std::any::Any + Sync + Send> {
                self
            }

            fn duplicate(&self) -> std::sync::Arc<dyn irradiance_runtime::ecs::Component> {
                std::sync::Arc::new((*self).clone())
            }
        }

        #component_ext
    }.into()
}

fn gen_from_str(ident: Ident, item_struct: &ItemStruct) -> proc_macro2::TokenStream {
    let (deserialize_struct, is_field_asset) = gen_deserialize_struct(item_struct.clone());
    let deserialize_ident = deserialize_struct.ident.clone();
    let derive_tokens =
        proc_macro2::TokenStream::from_str("#[derive(serde::Deserialize)]").expect("token stream");
    let assignments = gen_assignments(&deserialize_struct, is_field_asset);

    quote! {
        impl irradiance_runtime::ecs::ComponentExt for #ident {
            fn type_name() -> &'static str {
                stringify!(#ident)
            }

            fn from_str(value: irradiance_runtime::ron::Value, engine: &dyn irradiance_runtime::core::Engine) -> Option<std::sync::Arc<dyn irradiance_runtime::ecs::Component>> {
                if let Ok(deserialized) = value.into_rust::<#deserialize_ident>() {
                    Some(
                        std::sync::Arc::new(#ident {
                            #assignments
                        })
                    )
                } else {
                    None
                }
            }
        }

        #derive_tokens
        #deserialize_struct
    }
}

fn gen_deserialize_struct(mut item_struct: ItemStruct) -> (ItemStruct, Vec<bool>) {
    item_struct.attrs = vec![];
    item_struct.vis = Visibility::Inherited;
    item_struct.ident = Ident::new(
        &format!("_deserialize_{}", item_struct.ident),
        Span::call_site(),
    );

    let mut is_field_asset = vec![false; item_struct.fields.len()];
    for (index, field) in item_struct.fields.iter_mut().enumerate() {
        match &mut field.ty {
            Type::Path(TypePath { path, .. })
                if path
                    .segments
                    .iter()
                    .any(|segment| segment.ident == "AssetRef") =>
            {
                let mut segments: Punctuated<PathSegment, Token![::]> = Punctuated::default();
                segments.push_value(Ident::new("std", Span::call_site()).into());
                segments.push_punct(<Token![::]>::default());
                segments.push_value(Ident::new("path", Span::call_site()).into());
                segments.push_punct(<Token![::]>::default());
                segments.push_value(Ident::new("PathBuf", Span::call_site()).into());

                *path = Path {
                    leading_colon: None,
                    segments,
                };

                is_field_asset[index] = true;
            }
            _ => {}
        }
    }

    (item_struct, is_field_asset)
}

fn gen_assignments(
    deserialize_struct: &ItemStruct,
    is_field_asset: Vec<bool>,
) -> proc_macro2::TokenStream {
    let assignments = deserialize_struct
        .fields
        .iter()
        .enumerate()
        .map(|(index, field)| gen_assignment(field, is_field_asset[index]))
        .collect::<Vec<_>>();

    quote! {
        #(#assignments),*
    }
}

fn gen_assignment(field: &Field, is_asset: bool) -> proc_macro2::TokenStream {
    let ident = field.ident.clone().expect("ident");

    if is_asset {
        quote! {
            #ident: engine.assets()
                .read(engine.graphics_context().clone(), deserialized.#ident)
                .unwrap_or_default()
        }
    } else {
        quote! {
            #ident: deserialized.#ident
        }
    }
}
